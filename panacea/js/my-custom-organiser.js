 $(document).ready(function(){
	var maxField = 15; //Input fields increment limitation
	var addButton = $('.add_button'); //Add button selector
	var wrapper = $('tbody'); //Input field wrapper
	var fieldHTML = '';
	var start = $("tr[id='team-breakdowns']").length;
	var x = start+0; 
	
		$(addButton).click(function(){ //Once add button is clicked
		
		var fieldHTML = '<tr id="team-breakdowns" class="remove">'+
					  '<td><input type="text" class="required" id="breakdown_description_'+x+'" onclick="add_new_brekdown_row('+x+')"  name="breakdown['+x+'][description]" class="form-control" /></td>'+
					  '<td><input type="text" class="required" id="total_units_'+x+'" name="breakdown['+x+'][units]" class="form-control" /></td>'+
					  '<td><div class="input-group"><span class="input-group-addon">$</span><input type="text" class="required"  name="breakdown['+x+'][cost_per_unit]" id="cost_per_unit_'+x+'" onblur="check_totals_project_units('+x+')" class="form-control" /></div></td>'+
					  '<td><input type="text" class="required" id="no_of_units_'+x+'" autocomplete="off" onkeyup="check_totals_project_cost('+x+')" name="breakdown['+x+'][no_of_units]" class="form-control" /></td>'+
					  '<td><input type="text" name="breakdown['+x+'][totals]" id="total_amount_'+x+'"   class="form-control total_cost" readonly/></td>'+
					  '<td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img title="Remove field" src="https://www.greenearthappeal.org/panacea/img/remove-icon.png"/></span></a></td>'+
					  '</tr>'; //New input field html 
				
			if(x < maxField){ //Check maximum number of input fields
				x++; //Increment field counter
				$(wrapper).append(fieldHTML); // Add field html
			}
		});
		$(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
			e.preventDefault();
			$(this).closest('tr.remove').remove(); //Remove field html
			//$(this).parent('div').remove(); //Remove field html
			total_project_cost();
			x--; //Decrement field counter
		});
	});
	
		$(function() {
       			$( "#project_start_date" ).datepicker({
				dateFormat: 'dd-mm-yy'
               /*  showOn: "button", 
                buttonImage: "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
                buttonImageOnly: true */
            });
         });
		$("#team").change(function(){
			var count = this.value;
			var start = $("div[id='team-person']").length;
			var data_count = $(this).data('count');
				
			if(count < data_count){
				alert('Can not remove Old Persons.');
				return false;	
			}
			else if(count >= data_count && count <= start){
				
				for (i = data_count+1; i <= start; i++) {
					
					$('.team-person-'+i).remove();
					
				}
			  start = data_count;
		    }
			
			add_persons_data(count,start);
		});
		
	function add_persons_data(n,start) {
		var html = '';
		for (i = start+1; i <= n; i++) {
				html += '<div  id="team-person" class="col-sm-12 team-person-'+i+'">'+
				'<div class="panel panel-default">'+
									'<div class="panel-heading">Person '+i+'</div>'+
									'<div class="panel-body">'+
										'<div class="control-group">'+
											'<label class="control-label">First Name: (*)</label>'+
											'<div class="controls">'+
												'<input class="required"  type="text" value="" name="person['+i+'][first_name]" required=""/>'+
											'</div>'+
										'</div>'+
										'<div class="control-group">'+
											'<label class="control-label">Surname: (*)</label>'+
											'<div class="controls">'+
												'<input class="required"  type="text" value="" name="person['+i+'][surname]" required=""/>'+
											'</div>'+
										'</div>'+
										'<div class="control-group">'+
											'<label class="control-label">Qualifications: </label>'+
											'<div class="controls">'+
												'<input type="text" value="" name="person['+i+'][qualifications]" />'+
											'</div>'+
										'</div>'+
										'<div class="control-group">'+
											'<label class="control-label">Responsibilities in the project: (*)</label>'+
											'<div class="controls">'+
												'<textarea name="person['+i+'][project_responsbilites]" style="height:88px;" class="required" required=""></textarea>'+
											'</div>'+
										'</div>'+
										'<div class="control-group">'+
											'<label class="control-label">Experience: </label>'+
											'<div class="controls">'+
												'<textarea name="person['+i+'][experience]" style="height:88px;"></textarea>'+
											'</div>'+
										'</div>'+
										/*'<div class="control-group">'+
											'<label class="control-label">Photo (upload): </label>'+
											'<div class="controls">'+
												'<input type="file" value="" name="photo'+i+'" />'+
											'</div>'+
										'</div>'+*/
									'</div>'+
								  '</div>'+
							'</div>';
			}
		 $('#persons_data').append(html);
	}
	
		$("#locations").change(function(){
			
			var count = this.value;
	    	var start = $("div[id='tree-locations']").length;
			var data_count = $(this).data('location');
			
			if(count < data_count){
				alert('Can not remove old location.');
				return false;	
			}
			else if(count >= data_count && count <= start){
				
				for (i = data_count+1; i <= start; i++) {
					
					$('.tree-locations-'+i).remove();
					
				}
			  start = data_count;
		    } 
			
			add_locations_data(count,start);
		});
		
	function add_locations_data(n,start) {
		var html = '';
		for (i = start+1; i <= n; i++) {
				html += '<div id="tree-locations" class="col-sm-12 tree-locations-'+i+'">'+
						'<div class="panel panel-default">'+
							'<div class="panel-heading">Location '+i+'</div>'+
							'<div class="panel-body">'+
								/*'<div class="control-group">'+
									'<label class="control-label">Country: (*)</label>'+
									'<div class="controls">'+
										'<input type="text" class="required" value="" name="location['+i+'][country]" required=""/>'+
									'</div>'+
								'</div>'+
								'<div class="control-group">'+
									'<label class="control-label">Region: (*)</label>'+
									'<div class="controls">'+
										'<input type="text" class="required" value="" name="location['+i+'][region]" required=""/>'+
									'</div>'+
								'</div>'+
								'<div class="control-group">'+
									'<label class="control-label">Area: (*)</label>'+
									'<div class="controls">'+
										'<input type="text" class="required" value="" name="location['+i+'][area]" />'+
									'</div>'+
								'</div>'+
								'<div class="control-group">'+
									'<label class="control-label">Location1 : (*)</label>'+
									'<div class="controls">'+
										'<input type="text" class="required" value="" name="location['+i+'][location]" required=""/>'+
									'</div>'+
								'</div>'+*/
									'<div class="control-group">'+
									'<label class="control-label">Google Map Pin Location: </label>'+
										'<div class="controls">'+
										'<input id="pin_location'+i+'" type="textbox" value="Kazakhstan">'+
										'<input value="Search" onclick="google_pin_location'+i+'(\'latlang\')" style="width: 80px;" type="button">'+
											'<div class="google_canvas" id="mapCanvas3'+i+'"></div>'+
											 ' <div id="infoPanel">'+
												'<b>Marker status:</b>'+
												'<div id="markerStatus3'+i+'"><i>Click and drag the marker.</i></div>'+
												'<b>Closest matching address:</b>'+
												'<div id="markeraddress3'+i+'"></div>'+
											   '</div>'+
										'</div>'+
								'</div>'+
								'<div class="control-group"> '+
									'<div class="controls">'+
											'<input type="hidden" id="markerinfo3'+i+'" value="Kazakhstan" name="location['+i+'][google_pin_loc]" required=""/>'+
											'<input type="text" id="lat_lan3'+i+'" value="48.019573, 66.92368399999998" name="location['+i+'][lat_lang]" required=""/>'+
									'</div>'+
								'</div>'+
								'<h4>Types of Trees in Location '+i+'</h4><hr>'+
									'<div class="control-group">'+
										'<label class="control-label" >How many species?: (*)</label>'+
										'<div class="controls">'+
											'<select class="required" onChange="showallspecies(this.value,'+i+')" name="no_of_species'+i+'" id="no_of_species" required="">'+
												'<option value="">--Select--</option>';
												for (j = 1; j <= 10; j++) {
													html +='<option value="'+j+'">'+j+'</option>';
												}
												html +='</select>'+
										'</div>'+
									'</div>'+
									'<div id="all_species_data_'+i+'"></div>'	
							'</div>'+
						  '</div>'+
					'</div>';
			}
		 $('#tree_locations_data').append(html);
	}
	function showallspecies(val,id,data_count = 0){
			var count = val;
			var divid = 'tree-species'+id;
	    	var start = $("div[id='"+divid+"']").length;
		 
			//var data_count = $(this).data('count1');
			if(count < data_count){
				alert('Can not remove old species.');
				return false;	
			}
			else if(count >= data_count && count <= start){
				console.log(count);
				console.log(data_count);
				console.log(start);
				for (i = data_count+1; i <= start; i++) {
					console.log('.tree-species-'+id+i) ; 
				
					$('.tree-species-'+id+i).remove();
					
				}
				debugger;	
			  start = data_count;
		    } 
			
			//$('#all_species_data_'+id+'').html('');
			all_species_data(val,start,id);
			for (i = 1; i <= 10; i++) {
			$("#species_field"+i).smartAutoComplete({source: species_lists });  
			}	
		};
	 function all_species_data(n,start,id) {
			var html = '';
				for (i = start+1; i <= n; i++) {
				html += '<div id="tree-species'+id+'" class="col-sm-12 tree-species-'+id+i+'">'+
					'<div class="panel panel-default">'+
						'<div class="panel-heading">Species '+i+'</div>'+
						'<div class="panel-body">'+
							'<div class="control-group">'+
								'<label class="control-label">Name: (*)</label>'+
								'<div class="controls">'+
									'<input type="text" class="required" id="species_field'+i+'" value="" name="location['+id+'][species]['+i+'][species_name]" required=""/>'+
								'</div>'+
							'</div>'+
							'<div class="control-group">'+
								'<label class="control-label">Number of trees: (*)</label>'+
								'<div class="controls">'+
									'<input type="number" value="" name="location['+id+'][species]['+i+'][no_of_trees]" required=""/>'+
								'</div>'+
							'</div>'+
							'<div class="control-group">'+
								'<label class="control-label">Sourced as: (*)</label>'+
								'<div class="controls">'+
									'<select class="required" name="location['+id+'][species]['+i+'][types_of_trees]" required="">'+
										'<option value="">--Select--</option>'+
										'<option value="Purchased as seeds">Purchased as seeds</option>'+
										'<option value="Seedlings">Seedlings</option>'+
										'<option value="Saplings">Saplings</option>'+
									'</select>'+
								'</div>'+
							'</div>'+
								'<div class="control-group">'+
										'<label class="control-label">Reason/benefits of this species: (*)</label>'+
									'<div class="controls">'+
										'<textarea class="required" name="location['+id+'][species]['+i+'][species_benefits]" style="height:88px;" class="required" required=""></textarea>'+
									'</div>'+
								'</div>'+
							'</div>'+
						  '</div>'+
					'</div>';	
			}
			$('#all_species_data_'+id+'').append(html);	
		 }
		$("#org_provide_fund").change(function(){
			$('#privide_funding_data').html('');
				var html = '';
				 html = '<div class="control-group">'+
				'<label class="control-label" required="">How many organisations will be co funding this project?: (*)</label>'+
				'<div class="controls">'+
					'<select class="required" onChange="showOrganisations(this.value)" name="organisation_funding" id="organisation_funding">'+
						'<option value="">--Select--</option>';
						for (i = 1; i <= 10; i++) {
							html +='<option value="'+i+'">'+i+'</option>';
						}
						html +='</select>'+
				'</div>'+
			'</div>';	
			if(this.value=='yes'){
				 $('#privide_funding_data').append(html);
			}else{
				total_project_costs();
				$('#organisation_funding_data').html('');
			}
		});
		function total_project_costs(){
			var  total = 0;
			var  org_sum = 0;
			jQuery( ".total_cost" ).each(function( index ) {
			  total += parseInt(jQuery( this ).val());
			});
			jQuery( "#total_cost" ).val(total);
			jQuery( ".sum_of_org_funding" ).each(function( index ) {
			if(jQuery( this ).val()!=''){
			  org_sum += parseInt(jQuery( this ).val());
			}
			});
			jQuery( "#other_partner_funding" ).val(0);
			jQuery( "#gea_funding" ).val(total); 
		 }
		
		function showOrganisations(val){
			$('#organisation_funding_data').html('');
			add_organisation_funding_data(val,0);
		};
		
		  $("#organisation_funding").change(function(){
			var count = this.value;
			var start = $("div[id='team-organisation']").length;
			var data_count = $(this).data('org');
			if(count < data_count){
				alert('Can not remove old organisations.');
				return false;	
			}
			else if(count >= data_count && count <= start){
				
				for (i = data_count+1; i <= start; i++) {
					
					$('.team-organisation-'+i).remove();
					
				}
			  start = data_count;
		    }
			
			add_organisation_funding_data(count,start);
		});
		 
		 
		 function add_organisation_funding_data(n,start) {
			var html = '';
				for (i = start+1; i <= n; i++) {
				html += '<div id="team-organisation" class="col-sm-12 team-organisation-'+i+'">'+
					'<div class="panel panel-default">'+
						'<div class="panel-heading">Organisation '+i+'</div>'+
						'<div class="panel-body">'+
							'<div class="control-group">'+
								'<label class="control-label">Name: (*)</label>'+
								'<div class="controls">'+
									'<input type="text" class="required"  value="" name="organisation['+i+'][org_name]" required=""/>'+
								'</div>'+
							'</div>'+
							'<div class="control-group">'+
								'<label class="control-label">How much funding is this organisation providing? : (*)</label>'+
								'<div class="controls">'+
								'<div class="input-group total-cost-group"><span class="input-group-addon">$</span>'+
									'<input type="number" class="required sum_of_org_funding" onblur="total_project_cost()"  value="" name="organisation['+i+'][org_funding_provide]" required=""/>'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="control-group">'+
								'<label class="control-label">Website: </label>'+
								'<div class="controls">'+
									'<input type="text" value="" name="organisation['+i+'][org_website]" />'+
								'</div>'+
							'</div>'+
							'<div class="control-group">'+
								'<label class="control-label">Contact Person First Name: (*)</label>'+
								'<div class="controls">'+
									'<input type="text" class="required"  value="" name="organisation['+i+'][person_first_name]" required=""/>'+
								'</div>'+
							'</div>'+
							'<div class="control-group">'+
								'<label class="control-label">Contact Person Surname: (*)</label>'+
								'<div class="controls">'+
									'<input type="text" value="" class="required"  name="organisation['+i+'][person_surname]" required=""/>'+
								'</div>'+
							'</div>'+
							'<div class="control-group">'+
									'<label class="control-label">Email Address: (*)</label>'+
								'<div class="controls">'+
									'<input type="email" value="" class="required"  name="organisation['+i+'][person_email_Address]" required=""/>'+
								'</div>'+
							'</div>'+
						'</div>'+
					  '</div>'+
				'</div>';	
			}
			$('#organisation_funding_data').append(html);	
		 }
