 $(document).ready(function(){
	var maxField = 15; //Input fields increment limitation
	var addButton = $('.add_button'); //Add button selector
	var wrapper = $('#breakdown_table tbody'); //Input field wrapper
	var fieldHTML = '';
	var start = $("tr[id='team-breakdowns']").length;
	var x = start+0; 
	
		$(addButton).click(function(){ //Once add button is clicked
		
		var fieldHTML = '<tr id="team-breakdowns" class="remove">'+
					  '<td><input type="text" class="required" id="breakdown_company_'+x+'" onclick="add_new_brekdown_row('+x+')"  name="breakdown['+x+'][company]" class="form-control" /></td>'+
					  '<td><input type="email" class="required" id="email_'+x+'" name="breakdown['+x+'][email]" class="form-control" /></td>'+
					  '<td><input type="text" class="required"  name="breakdown['+x+'][first_name]" id="first_name_'+x+'" class="form-control" /></td>'+
					  '<td><input type="text" class="required" id="last_name'+x+'" autocomplete="off" name="breakdown['+x+'][last_name]" class="form-control" /></td>'+
					  '<td><input type="number" name="breakdown['+x+'][tree_nums]" id="tree_nums_'+x+'"   class="form-control"/></td>'+
					  '<td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img title="Remove field" src="https://www.greenearthappeal.org/panacea/img/remove-icon.png"/></span></a></td>'+
					  '</tr>'; //New input field html 
				
			if(x < maxField){ //Check maximum number of input fields
				x++; //Increment field counter
				$(wrapper).append(fieldHTML); // Add field html
			}
		});
		$(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
			e.preventDefault();
			$(this).closest('tr.remove').remove(); //Remove field html
			//$(this).parent('div').remove(); //Remove field html
			//total_project_cost();
			x--; //Decrement field counter
		});
	});
