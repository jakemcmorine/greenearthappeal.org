var geocoder;
var map;
var marker1;
var marker2;
var marker3;
var marker4;

function google_pin_location1(lat=false)  {
	// alert('ok');
    geocoder = new google.maps.Geocoder();
   var address = document.getElementById("pin_location1").value;

    if(address=='Kazakhstan' || address=='Kazakhstan'){
		   var zoomout = 1;
	   }else{
		   var zoomout= 10;
	   }
	var latt = document.getElementById('lat_lan31').value;
	latt  = latt.split(',');
   var latlng = new google.maps.LatLng(latt[0],latt[1]);
   if(lat!=''){
	 var argument = {'address': address};
   }else{
	 var argument = {'latLng': latlng};
   }
  geocoder.geocode( argument, function(results, status) {
   if(lat!=''){
	 var position = results[0].geometry.location;
   }else{
	 var position = latlng;
   }
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById("mapCanvas31"), {
     zoom: zoomout,
           streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker1 = new google.maps.Marker({
          map: map,
          position: position,
          draggable: true,
          title: 'You are here'
      });
      updateMarkerPosition31(position);
      geocodePosition31(position);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker1, 'dragstart', function() {
    updateMarkerAddress31('Dragging...');
  });
      
  google.maps.event.addListener(marker1, 'drag', function() {
    updateMarkerStatus31('Dragging...');
    updateMarkerPosition31(marker1.getPosition());
	// alert('oks');
  });
  
  google.maps.event.addListener(marker1, 'dragend', function() {
    updateMarkerStatus31('Drag ended');
    geocodePosition31(marker1.getPosition());
      map.panTo(marker1.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition31(e.latLng);
	// alert('ok');
    geocodePosition31(marker1.getPosition());
    marker1.setPosition(e.latLng);
  map.panTo(marker1.getPosition()); 
  }); 
  
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function geocodePosition31(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress31(responses[0].formatted_address);
    } else {
      updateMarkerAddress31('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus31(str) {
  document.getElementById("markerStatus31").innerHTML = str;
}

function updateMarkerPosition31(latLng) {
  var latlong= latLng.lat()+', '+latLng.lng();
  
  $('#lat_lan31').val(latlong);
}

function updateMarkerAddress31(str) {
  document.getElementById("markeraddress31").innerHTML = str;
  $('#markerinfo31').val(str);
}


function google_pin_location2(lat=false)  {
    geocoder = new google.maps.Geocoder();
   //var address = 'Chandigarh, India';
   var address = document.getElementById("pin_location2").value;
    if(address=='Kazakhstan' || address=='Kazakhstan'){
		   var zoomout = 1;
	   }else{
		   var zoomout= 10;
	   }
var latt = document.getElementById('lat_lan32').value;
	latt  = latt.split(',');
   var latlng = new google.maps.LatLng(latt[0],latt[1]);
  
   if(lat!=''){
	 var argument = {'address': address};
   }else{
	 var argument = {'latLng': latlng};
   }
  geocoder.geocode( argument, function(results, status) {
	  if(lat!=''){
		 var position = results[0].geometry.location;
	   }else{
		 var position = latlng;
	   }
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById("mapCanvas32"), {
     zoom: zoomout,
            streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker2 = new google.maps.Marker({
          map: map,
          position: position,
          draggable: true,
          title: 'You are here'
      });
      updateMarkerPosition32(position);
      geocodePosition32(position);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker2, 'dragstart', function() {
    updateMarkerAddress32('Dragging...');
  });
      
  google.maps.event.addListener(marker2, 'drag', function() {
    updateMarkerStatus32('Dragging...');
    updateMarkerPosition32(marker2.getPosition());
  });
  
  google.maps.event.addListener(marker2, 'dragend', function() {
    updateMarkerStatus32('Drag ended');
    geocodePosition32(marker2.getPosition());
      map.panTo(marker2.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition32(e.latLng);
    geocodePosition32(marker2.getPosition());
    marker2.setPosition(e.latLng);
  map.panTo(marker2.getPosition()); 
  }); 
  
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function geocodePosition32(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress32(responses[0].formatted_address);
    } else {
      updateMarkerAddress32('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus32(str) {
  document.getElementById("markerStatus32").innerHTML = str;
}

function updateMarkerPosition32(latLng) {
  var latlong= latLng.lat()+', '+latLng.lng();
  $('#lat_lan32').val(latlong);
}

function updateMarkerAddress32(str) {
  document.getElementById("markeraddress32").innerHTML = str;
  $('#markerinfo32').val(str);
}


function google_pin_location3(lat=false)  {
    geocoder = new google.maps.Geocoder();
   //var address = 'Chandigarh, India';
   var address = document.getElementById("pin_location3").value;
    if(address=='Kazakhstan' || address=='Kazakhstan'){
		   var zoomout = 1;
	   }else{
		   var zoomout= 10;
	   }
  var latt = document.getElementById('lat_lan33').value;
	latt  = latt.split(',');
   var latlng = new google.maps.LatLng(latt[0],latt[1]);
  
   if(lat!=''){
	 var argument = {'address': address};
   }else{
	 var argument = {'latLng': latlng};
   }
  geocoder.geocode( argument, function(results, status) {
	   if(lat!=''){
		 var position = results[0].geometry.location;
	   }else{
		 var position = latlng;
	   }
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById("mapCanvas33"), {
     zoom: zoomout,
            streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker3 = new google.maps.Marker({
          map: map,
          position: position,
          draggable: true,
          title: 'You are here'
      });
      updateMarkerPosition33(position);
      geocodePosition33(position);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker3, 'dragstart', function() {
    updateMarkerAddress33('Dragging...');
  });
      
  google.maps.event.addListener(marker3, 'drag', function() {
    updateMarkerStatus33('Dragging...');
    updateMarkerPosition33(marker3.getPosition());
  });
  
  google.maps.event.addListener(marker3, 'dragend', function() {
    updateMarkerStatus33('Drag ended');
    geocodePosition33(marker3.getPosition());
      map.panTo(marker3.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition33(e.latLng);
    geocodePosition33(marker3.getPosition());
    marker3.setPosition(e.latLng);
  map.panTo(marker3.getPosition()); 
  }); 
  
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function geocodePosition33(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress33(responses[0].formatted_address);
    } else {
      updateMarkerAddress33('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus33(str) {
  document.getElementById("markerStatus33").innerHTML = str;
}

function updateMarkerPosition33(latLng) {
  var latlong= latLng.lat()+', '+latLng.lng();
  $('#lat_lan33').val(latlong);
}

function updateMarkerAddress33(str) {
  document.getElementById("markeraddress33").innerHTML = str;
  $('#markerinfo33').val(str);
}
function google_pin_location4(lat=false)  {
    geocoder = new google.maps.Geocoder();
   //var address = 'Chandigarh, India';
   var address = document.getElementById("pin_location4").value;
    if(address=='Kazakhstan' || address=='Kazakhstan'){
		   var zoomout = 1;
	   }else{
		   var zoomout= 10;
	   }
  var latt = document.getElementById('lat_lan34').value;
	latt  = latt.split(',');
   var latlng = new google.maps.LatLng(latt[0],latt[1]);
  
   if(lat!=''){
	 var argument = {'address': address};
   }else{
	 var argument = {'latLng': latlng};
   }
  geocoder.geocode( argument, function(results, status) {
	   if(lat!=''){
		 var position = results[0].geometry.location;
	   }else{
		 var position = latlng;
	   }
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById("mapCanvas34"), {
     zoom: zoomout,
            streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker4 = new google.maps.Marker({
          map: map,
          position: position,
          draggable: true,
          title: 'You are here'
      });
      updateMarkerPosition34(position);
      geocodePosition34(position);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker4, 'dragstart', function() {
    updateMarkerAddress34('Dragging...');
  });
      
  google.maps.event.addListener(marker4, 'drag', function() {
    updateMarkerStatus34('Dragging...');
    updateMarkerPosition34(marker.getPosition());
  });
  
  google.maps.event.addListener(marker4, 'dragend', function() {
    updateMarkerStatus34('Drag ended');
    geocodePosition34(marker4.getPosition());
      map.panTo(marker4.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition34(e.latLng);
    geocodePosition34(marker4.getPosition());
    marker4.setPosition(e.latLng);
  map.panTo(marker4.getPosition()); 
  }); 
  
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function geocodePosition34(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress34(responses[0].formatted_address);
    } else {
      updateMarkerAddress34('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus34(str) {
  document.getElementById("markerStatus34").innerHTML = str;
}

function updateMarkerPosition34(latLng) {
  var latlong= latLng.lat()+', '+latLng.lng();
  $('#lat_lan34').val(latlong);
}

function updateMarkerAddress34(str) {
  document.getElementById("markeraddress34").innerHTML = str;
  $('#markerinfo34').val(str);
}

function google_pin_location5(lat=false)  {
    geocoder = new google.maps.Geocoder();
   var address = document.getElementById("pin_location5").value;
    if(address=='Kazakhstan' || address=='Kazakhstan'){
		   var zoomout = 1;
	   }else{
		   var zoomout= 10;
	   }
  var latt = document.getElementById('lat_lan35').value;
	latt  = latt.split(',');
   var latlng = new google.maps.LatLng(latt[0],latt[1]);
  
   if(lat!=''){
	 var argument = {'address': address};
   }else{
	 var argument = {'latLng': latlng};
   }
  geocoder.geocode( argument, function(results, status) {
	   if(lat!=''){
		 var position = results[0].geometry.location;
	   }else{
		 var position = latlng;
	   }
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById("mapCanvas35"), {
     zoom: zoomout,
            streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker4 = new google.maps.Marker({
          map: map,
          position: position,
          draggable: true,
          title: 'You are here'
      });
      updateMarkerPosition35(position);
      geocodePosition35(position);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker4, 'dragstart', function() {
    updateMarkerAddress35('Dragging...');
  });
      
  google.maps.event.addListener(marker4, 'drag', function() {
    updateMarkerStatus35('Dragging...');
    updateMarkerPosition35(marker.getPosition());
  });
  
  google.maps.event.addListener(marker4, 'dragend', function() {
    updateMarkerStatus35('Drag ended');
    geocodePosition35(marker4.getPosition());
      map.panTo(marker4.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition35(e.latLng);
    geocodePosition35(marker4.getPosition());
    marker4.setPosition(e.latLng);
  map.panTo(marker4.getPosition()); 
  }); 
  
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function geocodePosition35(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress35(responses[0].formatted_address);
    } else {
      updateMarkerAddress35('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus35(str) {
  document.getElementById("markerStatus35").innerHTML = str;
}

function updateMarkerPosition35(latLng) {
  var latlong= latLng.lat()+', '+latLng.lng();
  $('#lat_lan35').val(latlong);
}

function updateMarkerAddress35(str) {
  document.getElementById("markeraddress35").innerHTML = str;
  $('#markerinfo35').val(str);
}


function google_pin_location6(lat=false)  {
    geocoder = new google.maps.Geocoder();
   var address = document.getElementById("pin_location6").value;
    if(address=='Kazakhstan' || address=='Kazakhstan'){
		   var zoomout = 1;
	   }else{
		   var zoomout= 10;
	   }
  var latt = document.getElementById('lat_lan36').value;
	latt  = latt.split(',');
   var latlng = new google.maps.LatLng(latt[0],latt[1]);
  
   if(lat!=''){
	 var argument = {'address': address};
   }else{
	 var argument = {'latLng': latlng};
   }
  geocoder.geocode( argument, function(results, status) {
	   if(lat!=''){
		 var position = results[0].geometry.location;
	   }else{
		 var position = latlng;
	   }
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById("mapCanvas36"), {
     zoom: zoomout,
            streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker4 = new google.maps.Marker({
          map: map,
          position: position,
          draggable: true,
          title: 'You are here'
      });
      updateMarkerPosition36(position);
      geocodePosition36(position);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker4, 'dragstart', function() {
    updateMarkerAddress36('Dragging...');
  });
      
  google.maps.event.addListener(marker4, 'drag', function() {
    updateMarkerStatus36('Dragging...');
    updateMarkerPosition36(marker.getPosition());
  });
  
  google.maps.event.addListener(marker4, 'dragend', function() {
    updateMarkerStatus36('Drag ended');
    geocodePosition36(marker4.getPosition());
      map.panTo(marker4.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition36(e.latLng);
    geocodePosition36(marker4.getPosition());
    marker4.setPosition(e.latLng);
  map.panTo(marker4.getPosition()); 
  }); 
  
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function geocodePosition36(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress36(responses[0].formatted_address);
    } else {
      updateMarkerAddress36('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus36(str) {
  document.getElementById("markerStatus36").innerHTML = str;
}

function updateMarkerPosition36(latLng) {
  var latlong= latLng.lat()+', '+latLng.lng();
  $('#lat_lan36').val(latlong);
}

function updateMarkerAddress36(str) {
  document.getElementById("markeraddress36").innerHTML = str;
  $('#markerinfo36').val(str);
}


function google_pin_location7(lat=false)  {
    geocoder = new google.maps.Geocoder();
   var address = document.getElementById("pin_location7").value;
    if(address=='Kazakhstan' || address=='Kazakhstan'){
		   var zoomout = 1;
	   }else{
		   var zoomout= 10;
	   }
  var latt = document.getElementById('lat_lan37').value;
	latt  = latt.split(',');
   var latlng = new google.maps.LatLng(latt[0],latt[1]);
  
   if(lat!=''){
	 var argument = {'address': address};
   }else{
	 var argument = {'latLng': latlng};
   }
  geocoder.geocode( argument, function(results, status) {
	   if(lat!=''){
		 var position = results[0].geometry.location;
	   }else{
		 var position = latlng;
	   }
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById("mapCanvas37"), {
     zoom: zoomout,
            streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker4 = new google.maps.Marker({
          map: map,
          position: position,
          draggable: true,
          title: 'You are here'
      });
      updateMarkerPosition37(position);
      geocodePosition37(position);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker4, 'dragstart', function() {
    updateMarkerAddress37('Dragging...');
  });
      
  google.maps.event.addListener(marker4, 'drag', function() {
    updateMarkerStatus37('Dragging...');
    updateMarkerPosition37(marker.getPosition());
  });
  
  google.maps.event.addListener(marker4, 'dragend', function() {
    updateMarkerStatus37('Drag ended');
    geocodePosition37(marker4.getPosition());
      map.panTo(marker4.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition37(e.latLng);
    geocodePosition37(marker4.getPosition());
    marker4.setPosition(e.latLng);
  map.panTo(marker4.getPosition()); 
  }); 
  
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function geocodePosition37(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress37(responses[0].formatted_address);
    } else {
      updateMarkerAddress37('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus37(str) {
  document.getElementById("markerStatus37").innerHTML = str;
}

function updateMarkerPosition37(latLng) {
  var latlong= latLng.lat()+', '+latLng.lng();
  $('#lat_lan37').val(latlong);
}

function updateMarkerAddress37(str) {
  document.getElementById("markeraddress37").innerHTML = str;
  $('#markerinfo37').val(str);
}


function google_pin_location8(lat=false)  {
    geocoder = new google.maps.Geocoder();
   var address = document.getElementById("pin_location8").value;
    if(address=='Kazakhstan' || address=='Kazakhstan'){
		   var zoomout = 1;
	   }else{
		   var zoomout= 10;
	   }
  var latt = document.getElementById('lat_lan38').value;
	latt  = latt.split(',');
   var latlng = new google.maps.LatLng(latt[0],latt[1]);
  
   if(lat!=''){
	 var argument = {'address': address};
   }else{
	 var argument = {'latLng': latlng};
   }
  geocoder.geocode( argument, function(results, status) {
	   if(lat!=''){
		 var position = results[0].geometry.location;
	   }else{
		 var position = latlng;
	   }
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById("mapCanvas38"), {
     zoom: zoomout,
            streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker4 = new google.maps.Marker({
          map: map,
          position: position,
          draggable: true,
          title: 'You are here'
      });
      updateMarkerPosition38(position);
      geocodePosition38(position);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker4, 'dragstart', function() {
    updateMarkerAddress38('Dragging...');
  });
      
  google.maps.event.addListener(marker4, 'drag', function() {
    updateMarkerStatus38('Dragging...');
    updateMarkerPosition38(marker.getPosition());
  });
  
  google.maps.event.addListener(marker4, 'dragend', function() {
    updateMarkerStatus38('Drag ended');
    geocodePosition38(marker4.getPosition());
      map.panTo(marker4.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition38(e.latLng);
    geocodePosition38(marker4.getPosition());
    marker4.setPosition(e.latLng);
  map.panTo(marker4.getPosition()); 
  }); 
  
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function geocodePosition38(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress38(responses[0].formatted_address);
    } else {
      updateMarkerAddress38('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus38(str) {
  document.getElementById("markerStatus38").innerHTML = str;
}

function updateMarkerPosition38(latLng) {
  var latlong= latLng.lat()+', '+latLng.lng();
  $('#lat_lan38').val(latlong);
}

function updateMarkerAddress38(str) {
  document.getElementById("markeraddress38").innerHTML = str;
  $('#markerinfo38').val(str);
}


function google_pin_location9(lat=false)  {
    geocoder = new google.maps.Geocoder();
   var address = document.getElementById("pin_location9").value;
    if(address=='Kazakhstan' || address=='Kazakhstan'){
		   var zoomout = 1;
	   }else{
		   var zoomout= 10;
	   }
  var latt = document.getElementById('lat_lan39').value;
	latt  = latt.split(',');
   var latlng = new google.maps.LatLng(latt[0],latt[1]);
  
   if(lat!=''){
	 var argument = {'address': address};
   }else{
	 var argument = {'latLng': latlng};
   }
  geocoder.geocode( argument, function(results, status) {
	   if(lat!=''){
		 var position = results[0].geometry.location;
	   }else{
		 var position = latlng;
	   }
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById("mapCanvas39"), {
     zoom: zoomout,
            streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker4 = new google.maps.Marker({
          map: map,
          position: position,
          draggable: true,
          title: 'You are here'
      });
      updateMarkerPosition39(position);
      geocodePosition39(position);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker4, 'dragstart', function() {
    updateMarkerAddress39('Dragging...');
  });
      
  google.maps.event.addListener(marker4, 'drag', function() {
    updateMarkerStatus39('Dragging...');
    updateMarkerPosition39(marker.getPosition());
  });
  
  google.maps.event.addListener(marker4, 'dragend', function() {
    updateMarkerStatus39('Drag ended');
    geocodePosition39(marker4.getPosition());
      map.panTo(marker4.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition39(e.latLng);
    geocodePosition39(marker4.getPosition());
    marker4.setPosition(e.latLng);
  map.panTo(marker4.getPosition()); 
  }); 
  
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function geocodePosition39(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress39(responses[0].formatted_address);
    } else {
      updateMarkerAddress39('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus39(str) {
  document.getElementById("markerStatus39").innerHTML = str;
}

function updateMarkerPosition39(latLng) {
  var latlong= latLng.lat()+', '+latLng.lng();
  $('#lat_lan39').val(latlong);
}

function updateMarkerAddress39(str) {
  document.getElementById("markeraddress39").innerHTML = str;
  $('#markerinfo39').val(str);
}

function google_pin_location10(lat=false)  {
    geocoder = new google.maps.Geocoder();
   var address = document.getElementById("pin_location10").value;
    if(address=='Kazakhstan' || address=='Kazakhstan'){
		   var zoomout = 1;
	   }else{
		   var zoomout= 10;
	   }
  var latt = document.getElementById('lat_lan310').value;
	latt  = latt.split(',');
   var latlng = new google.maps.LatLng(latt[0],latt[1]);
  
   if(lat!=''){
	 var argument = {'address': address};
   }else{
	 var argument = {'latLng': latlng};
   }
  geocoder.geocode( argument, function(results, status) {
	   if(lat!=''){
		 var position = results[0].geometry.location;
	   }else{
		 var position = latlng;
	   }
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById("mapCanvas310"), {
     zoom: zoomout,
            streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker4 = new google.maps.Marker({
          map: map,
          position: position,
          draggable: true,
          title: 'You are here'
      });
      updateMarkerPosition310(position);
      geocodePosition310(position);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker4, 'dragstart', function() {
    updateMarkerAddress310('Dragging...');
  });
      
  google.maps.event.addListener(marker4, 'drag', function() {
    updateMarkerStatus310('Dragging...');
    updateMarkerPosition310(marker.getPosition());
  });
  
  google.maps.event.addListener(marker4, 'dragend', function() {
    updateMarkerStatus310('Drag ended');
    geocodePosition310(marker4.getPosition());
      map.panTo(marker4.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition310(e.latLng);
    geocodePosition310(marker4.getPosition());
    marker4.setPosition(e.latLng);
  map.panTo(marker4.getPosition()); 
  }); 
  
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function geocodePosition310(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress310(responses[0].formatted_address);
    } else {
      updateMarkerAddress310('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus310(str) {
  document.getElementById("markerStatus310").innerHTML = str;
}

function updateMarkerPosition310(latLng) {
  var latlong= latLng.lat()+', '+latLng.lng();
  $('#lat_lan310').val(latlong);
}

function updateMarkerAddress310(str) {
  document.getElementById("markeraddress310").innerHTML = str;
  $('#markerinfo310').val(str);
}