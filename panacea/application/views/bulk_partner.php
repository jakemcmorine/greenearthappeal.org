<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" enctype="multipart/form-data"  class="form-horizontal" action="">
        <fieldset>
            <legend class="text-center"><?php echo $title; ?></legend>
            <div class="controls">
                <?php echo validation_errors(); ?>
            </div>
            <div class="control-group">
                <label class="control-label">Initiative: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('initiative'); ?>" name="initiative" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Opportunity String: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('opportunity_str'); ?>" name="opportunity_str" placeholder="gives diners the opportunity to:" />
                </div>
            </div>
            
            <div class="control-group">
                <label class="control-label">Company Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('company'); ?>" name="company" id="company" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Company Logo (200 x 200 px):</label>
                <div class="controls">
                    <input type="file" name="logo" />
                </div>
            </div> 
            <div class="control-group">
                <label class="control-label">Website:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('website'); ?>" name="website" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Referer ID: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('code'); ?>" name="code" id="code" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">First Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('first_name'); ?>" name="first_name"  id="first_name"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Last Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('last_name'); ?>" name="last_name" id="last_name"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Email: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('email'); ?>" name="email"  id="email"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Username: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('username'); ?>" name="username" id="username"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password: (*)</label>
                <div class="controls">
                    <input type="password" value="" name="password" id="password"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password Conf: (*)</label>
                <div class="controls">
                    <input type="password" value="" name="password2" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Phone:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('phone'); ?>" name="phone" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Tree number (*):</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('tree_nums'); ?>" name="tree_nums" />
                </div>
            </div>
            <!----->
            <div class="control-group">
                <label class="control-label">Cost per tree (*):</label>
                <div class="controls">
                    <input type="text" style="width: 50px;" name="price" value="<?php echo set_value('price'); ?>" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"> Currency (*):</label>
                <div class="controls">
                    <select name="currency">
                        <option value="">--Select--</option>
                        <option value="1" <?php if (set_value('currency') == 1) echo "selected='selected'" ?>>cent</option>
                        <option value="2" <?php if (set_value('currency') == 2) echo "selected='selected'" ?>>pence</option>

                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Tax (*):</label>
                <div class="controls">
                    <input type="text" style="width: 50px;" name="tax" value="<?php echo set_value('tax'); ?>" /> %
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-success" type="submit" name="add_bulk_partner">Save</button>
                    <a href="<?php echo site_url(); ?>" class="btn btn-warning">Cancel</a>
                    <a href="javascript:void(0);" class="btn btn-info" id="dialog-link">Test</a>
                </div>
            </div>
        </fieldset>
    </form>
</div>
<div id="dialog" title="Email Preview">

</div>
<script  src="<?php echo base_url(); ?>js/jquery-1.10.2.js"  type="text/javascript" ></script>
<script  src="<?php echo base_url(); ?>js/jquery-ui.js"  type="text/javascript" /></script>
<link href="<?php echo base_url(); ?>css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
    $("#dialog").dialog({autoOpen: false, draggable: true, width: 800, maxHeight: 350});
    $("#dialog-link").click(function (event) {
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        var email = $("#email").val();
        var username = $("#username").val();
        var password = $("#password").val();
        var name = $("#company").val();
        var code = $("#code").val();

        $.ajax({
            url: '<?php echo site_url('user/preview_template'); ?>',
            method: 'post',
            data: {'first_name': first_name, 'company': name, 'email': email, 'username': username, 'password': password, 'code': code, 'tempid': 11}
        }).done(function (data) {
            $("#dialog").html(data);
        });


        $("#dialog").dialog("open");
        event.preventDefault();
    });
</script>