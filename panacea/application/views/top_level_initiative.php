<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" enctype="multipart/form-data" class="form-horizontal" action="">
        <fieldset>
            <legend class="text-center">Add Top Level Initiative</legend>
            <div class="controls">
                <?php echo validation_errors(); ?>
            </div>
			 <div class="control-group">
                <label class="control-label">Initiative: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('initiative'); ?>" name="initiative" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Opportunity String: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('opportunity_str'); ?>" name="opportunity_str" placeholder="gives diners the opportunity to:" />
                </div>
            </div>
			 <div class="control-group">
                <label class="control-label">Top Level Partner Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('partner_name'); ?>" name="partner_name" />
                </div>
            </div>
			 <div class="control-group">
                <label class="control-label">Company Logo (200 x 200 px):</label>
                <div class="controls">
                    <input type="file" name="logo" />
                </div>
            </div> 
			 <div class="control-group">
                <label class="control-label">Title: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('main_title'); ?>" name="main_title" />
                </div>
            </div> 
			<div class="control-group">
                <label class="control-label">Sub Title: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('sub_title'); ?>" name="sub_title" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Address:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('address'); ?>" name="address" />
                </div>
            </div>
			
            <div class="control-group">
                <label class="control-label">Website:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('website'); ?>" name="website" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Referer ID: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('code'); ?>" name="code" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">First Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('first_name'); ?>" name="first_name" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Last Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('last_name'); ?>" name="last_name" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Email: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('email'); ?>" name="email" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Phone:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('phone'); ?>" name="phone" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Twitter:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('twitter'); ?>" name="twitter" />
                </div>
            </div>
				<div class="control-group">
                <label class="control-label">Twitter Hashtag:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('twitter_hashtag'); ?>" name="twitter_hashtag" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Tree number (*):</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('tree_nums'); ?>" name="tree_nums" />
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-success" type="submit" name="add_toplevel">Save</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>