<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--  datepicker   -->
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<!--  datepicker   -->
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<!--  datepicker   -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/js/iEdit.css">
<script type="text/javascript" src="<?php echo base_url(); ?>/js/iEdit.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/js/script.js"></script>
<div class="container">
	<legend><?php echo $title; ?></legend>
	<?php echo validation_errors(); ?>
</div>
<?php //echo "<pre>"; print_r($persons); die; ?>
<div id="exTab3" class="container">	
<div class="wh_formss">
<ul  class="nav nav-pills">
			<li class="active grayclass">
				<a  href="#stage2" data-toggle="tab">Project Details</a>
			</li>
			</ul>
			<div class="tab-content clearfix">
			  <div class="tab-pane active" id="stage2">
			<form accept-charset="utf-8" id="stage2_form"  class="form-horizontal">
				<div class="panel panel-info">
					<div class="panel-heading text-center">Stage 2 (Details of Trees)</div>
					<div class="panel-body">
					<div class="controls">
						<?php if (isset($error)) echo "<div class='text-error'>$error</div>"; ?>
					</div>
					  <div  class="control-group">
						<label class="control-label" class="required" >How many locations?: (*)</label>
							<div class="controls">
								<select name="locations"  id="locations" data-location="<?php echo count($locations); ?>" required="">
									<option value="">--Select--</option>
									<?php for($i=1;$i<=10;$i++){ ?>
										<option <?php if(count($locations)==$i){ echo 'selected="selected"'; } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>	
						<div id="tree_locations_data">
						<?php if(!empty($locations)){
							$count=1;
							foreach($locations as $location){ ?>
							<div id="tree-locations" class="col-sm-12 tree-locations-<?php echo $count; ?>">
									<div class="panel panel-default">
										<div class="panel-heading">Location <?php echo $count; ?></div>
										<div class="panel-body">
											<div class="control-group">
												<label class="control-label">Country: (*)</label>
												<div class="controls">
													<input type="text" value="<?php echo $location->country; ?>" name="location[<?php echo $count; ?>][country]" required=""/>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Region: (*)</label>
												<div class="controls">
													<input type="text" value="<?php echo $location->region; ?>" name="location[<?php echo $count; ?>][region]" required=""/>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Area: (*)</label>
												<div class="controls">
													<input type="text" value="<?php echo $location->area; ?>" name="location[<?php echo $count; ?>][area]" />
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Location1 : (*)</label>
												<div class="controls">
													<input type="text" value="<?php echo $location->location; ?>" name="location[<?php echo $count; ?>][location]" required=""/>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Google Map Pin Location: (*)</label>
												<div class="controls">
													<input type="text" value="<?php echo $location->google_pin_loc; ?>" name="location[<?php echo $count; ?>][google_pin_loc]" required=""/>
												</div>
											</div>
												<input type="hidden" value="<?php echo $location->id; ?>" name="location[<?php echo $count; ?>][id]" required=""/>
											<h4>Types of Trees in Location <?php echo $count; ?></h4><hr>
												<div class="control-group">
												 <label class="control-label" >How many species?: (*)</label>
													<div class="controls">
														<select data-count1="<?php echo count($location->species); ?>"  onChange="showallspecies(this.value,<?php echo $count; ?>,<?php echo count($location->species); ?>)" name="no_of_species<?php echo $count; ?>" id="no_of_species" required="">
															<option value="">--Select--</option>
															<?php for($i=1;$i<=10;$i++){ ?>
																<option <?php if(count($location->species)==$i){ echo 'selected="selected"'; } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
															<?php } ?>
															</select>
													</div>
												</div>
												<div id="all_species_data_<?php echo $count; ?>"  >
												<?php if(!empty($location->species)){
													$counter=1;
													foreach($location->species as $species){  ?>
														<div id="tree-species<?php echo $count; ?>" class="col-sm-12 tree-species-<?php echo $count; ?><?php echo $counter; ?>">
															<div class="panel panel-default">
																<div class="panel-heading">Species <?php echo $counter; ?></div>
																<div class="panel-body">
																	<div class="control-group">
																		<label class="control-label">Name: (*)</label>
																		<div class="controls">
																			<select class="required" name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][species_name]" required="">
																				<option value="">--Select--</option>
																				<option <?php if($species->species_name=='Species1'){ echo 'selected="selected"'; } ?> value="Species1">Species1</option>
																				<option <?php if($species->species_name=='Species2'){ echo 'selected="selected"'; } ?> value="Species2">Species2</option>
																				<option <?php if($species->species_name=='Species3'){ echo 'selected="selected"'; } ?> value="Species3">Species3</option>
																			</select>
																		</div>
																	</div>
																	<div class="control-group">
																		<label class="control-label">Number of trees: (*)</label>
																		<div class="controls">
																			<input type="number" value="<?php echo $species->no_of_trees; ?>" name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][no_of_trees]" required=""/>
																		</div>
																	</div>
																	<div class="control-group">
																		<label class="control-label">Select any one: (*)</label>
																		<div class="controls">
																			<select class="required" name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][types_of_trees]" required="">
																				<option value="">--Select--</option>
																				<option <?php if($species->species_seeds=='Purchased as seeds'){ echo 'selected="selected"'; } ?> value="Purchased as seeds">Purchased as seeds</option>
																				<option <?php if($species->species_seeds=='Seedlings'){ echo 'selected="selected"'; } ?> value="Seedlings">Seedlings</option>
																				<option  <?php if($species->species_seeds=='Saplings'){ echo 'selected="selected"'; } ?>value="Saplings">Saplings</option>
																			</select>
																		</div>
																	</div>
																	<div class="control-group">
																			<label class="control-label">Reason/benefits of this species: (*)</label>
																		<div class="controls">
																			<textarea name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][species_benefits]" style="height:88px;" class="required" required=""><?php echo $species->species_benefits; ?></textarea>
																		</div>
																	</div>
																	<input type="hidden" value="<?php echo $species->id; ?>" name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][id]" required=""/>
																	<input type="hidden" value="<?php echo $species->location_id; ?>" name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][location_id]" required=""/>
																  </div>
																  </div>
															</div>
													<?php $counter++;  } } ?>
												</div>
										</div>					
										</div>
									  </div>
						 <?php $count++;  } } ?>
						 </div>
						</div>
						<input type="hidden" name="edit_stage_2" value="edit_stage_2">
						<div id="success_msg">
								</div>
								<div class="controls">
								<?php if($stage2_status==0){ ?>
									<button class="btn btn-success" type="button" onclick="confirm_stage('<?php echo $user_id; ?>','2')" name="edit_stage_2">Confirm Stage 2</button>
								<?php } else{ ?>
									<button class="btn btn-success" type="button"  name="edit_stage_2">Stage 2 Already confirmed</button>
								<?php } ?>
 								</div>
						<br>
				 </div>
			</form>
		</div>
	</div>
	</div>
	</div>
	<script  src="<?php echo base_url(); ?>js/jquery-1.10.2.js"  type="text/javascript" ></script>
<script  src="<?php echo base_url(); ?>js/jquery-ui.js"  type="text/javascript" /></script>
<link href="<?php echo base_url(); ?>css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />


<script type="text/javascript">
 function confirm_stage(user_id,stage){
		if(confirm("Do you really want to confirm the Stage 2?")) {
		var user_id = user_id;
 			$.post('<?php echo site_url('user/stage_confirmed/');?>',
			{ 
				 user_id : user_id,
				 stage : stage
			}, 
 			function(data) 
			{
				$("#success_msg").html('Stage 2 confirmed successfully!');
			}); 
			}else {
				return false; // cancel the event
			}
		}
		
</script>
<style>
body {
  padding : 10px ;
  
}
.wh_formss input, textarea, .uneditable-input {
  width: 310px;
}

#exTab1 .tab-content {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}

#exTab2 h3 {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}


#exTab3 .wh_formss {
  margin: 0 auto;
  width: 70%;
}

#exTab3 .nav.nav-pills
{
	margin-bottom:0px!important;
}
/* remove border radius for the tab */

#exTab1 .nav-pills > li > a {
  border-radius: 0;
}

/* change border radius for the tab , apply corners on top*/

#exTab3 .nav-pills > li > a {
  border-radius: 4px 4px 0 0 ;
}

#exTab3 .tab-content {
  border: 1px solid #E5E6E7;
  padding : 5px 15px;
}
.tab-content.clearfix {
  width: 100%;
}
#exTab3 .nav-pills > li > a
{
	padding:13px!important;
}
.call_us
{
	margin-top:50px;
}
.grayclass a{
	 background-color: #eee;
}
.wh_formss .nav .active a{
	 background-color: #08c!important;
}
select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
  border-radius: 4px;
  color: #555;
  display: inline-block;
  font-size: 14px;
  height: 30px;
  line-height: 20px;
  margin-bottom: 10px;
  padding: 4px 6px;
  vertical-align: middle;
}
.panel-info > .panel-heading {
  text-transform: uppercase;
}
.table input {display: block !important; padding: 0 !important; margin: 0 !important; border: 0 !important; width: 100% !important; border-radius: 0 !important; line-height: 1 !important; text-align: center;}
.table td {margin: 0 !important; padding: 0 !important;}
.add_button img {width: 35px !important;}
.remove_button img {width: 22px !important;}
.disabled{
	pointer-events: none;
    cursor: default;
}
#success_msg {
  color: green;
  font-size: 17px;
  margin-bottom: 10px;
  padding: 4px 6px;
}


@media screen and (max-width:992px)
{
	.wh_formss input, textarea, .uneditable-input {
  width: auto!important;
  float:left;
	}
	.control-label{margin-right:10px;}
.form-horizontal .controls {

  margin-left: 15px!important;
}

}

@media screen and (max-width:767px)
{
	#exTab3 .wh_formss {
  float: left!important;
  margin: 0 auto;
  
  width: 100%!important;
}

.container 
{
	float:left!important;
	padding:5px!important;
	width:100%!important;
}
.panel , .tab-pane , form , .panel-body , .google_canvas{
  float: left!important;
  width: 100%!important;
}
.tab-content {
  float: left;
  width: 100%!important;
  padding:0px!important;
}
.control-label {
  margin-right: 0;
  float: left!important;
  width: 100%!important;
  text-align: left!important;
  font-size: 13px!important;
}
.required {
  float: left;
  width: 100%!important;
}

.form-horizontal .controls {
  float: left;
  margin-left: 0!important;
  padding-left: 0;
  width: 100%;
}
.mystage {
  width: auto;
}
.wh_formss input, textarea, .uneditable-input {
  float: left;
  width: 100%!important;
}
.panel-heading
{
	font-size:12px;
}
.panel-body {
  border: medium none;
  padding: 5px;
}
.new_table {
  overflow: scroll;
}
.wh_btns
{
	
	float:left;
	width:100%;
	text-align:center;
	margin-bottom:20px;
}
}
</style>