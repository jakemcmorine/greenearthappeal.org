<form accept-charset="utf-8" id="stage2_form" method="post" class="form-horizontal" enctype="multipart/form-data" action="">
	<div class="panel panel-info">
		<div class="panel-heading text-center">Project Details</div>
		<div class="panel-body">
		<div class="controls">
			<?php if (isset($error)) echo "<div class='text-error'>$error</div>"; ?>
		<div class="mystage">
			<div class="progress progress-striped active">
				<div class="bar stage_bar" style="width: 0%;"></div>
			</div>
		</div>
		</div>
		  <div  class="control-group">
			<label class="control-label" class="required" >How many locations are you planting trees in?: (*)</label>
				<div class="controls">
					<select name="locations" class="required"  id="locations" data-location="<?php echo count($locations); ?>" required="">
						<option value="">--Select--</option>
						<?php for($i=1;$i<=10;$i++){ ?>
							<option <?php if(count($locations)==$i){ echo 'selected="selected"'; } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>	
			<div id="tree_locations_data">
			<?php if(!empty($locations)){
				$count=1;
				foreach($locations as $location){ ?>
				<div id="tree-locations" class="col-sm-12 tree-locations-<?php echo $count; ?>">
						<div class="panel panel-default">
							<div class="panel-heading">Location <?php echo $count; ?></div>
							<div class="panel-body">
								<!--<div class="control-group">
									<label class="control-label">Country: (*)</label>
									<div class="controls">
										<input type="text" class="required" value="<?php //echo $location->country; ?>" name="location[<?php //echo $count; ?>][country]" required=""/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Region: (*)</label>
									<div class="controls">
										<input type="text" class="required" value="<?php //echo $location->region; ?>" name="location[<?php //echo $count; ?>][region]" required=""/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Area: (*)</label>
									<div class="controls">
										<input type="text" class="required" value="<?php //echo $location->area; ?>" name="location[<?php //echo $count; ?>][area]" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Location1 : (*)</label>
									<div class="controls">
										<input type="text" class="required" value="<?php //echo $location->location; ?>" name="location[<?php// echo $count; ?>][location]" required=""/>
									</div>
								</div>-->
								<div class="control-group">
									<label class="control-label">Google Map Pin Location: </label>
										<div class="controls">
										<input id="pin_location<?php echo $count; ?>" type="textbox" value="<?php if(isset($location->google_pin_loc)){ echo $location->google_pin_loc; }else{ echo "Kazakhstan"; } ?>">
										<input value="Search" onclick="google_pin_location<?php echo $count; ?>('lantlang')" style="width: 80px;" type="button">
											<div class="google_canvas" id="mapCanvas3<?php echo $count; ?>"></div>
											  <div id="infoPanel">
												<b>Marker status:</b>
												<div id="markerStatus3<?php echo $count; ?>"><i>Click and drag the marker.</i></div>
												<b>Address:</b>
												<div id="markeraddress3<?php echo $count; ?>"></div>
											   </div>
										</div>
								</div>
								<div class="control-group"> 
									<div class="controls">
											<input type="hidden" id="markerinfo3<?php echo $count; ?>" value="<?php echo $location->google_pin_loc; ?>" name="location[<?php echo $count; ?>][google_pin_loc]" required=""/>
											<input type="text" id="lat_lan3<?php echo $count; ?>" value="<?php if(isset($location->long_latitute)){ echo $location->long_latitute; }else{ echo '48.019573, 66.92368399999998'; }  ?>" name="location[<?php echo $count; ?>][lat_lang]" required=""/>
									</div>
								</div>
									<input type="hidden" value="<?php echo $location->id; ?>" name="location[<?php echo $count; ?>][id]" required=""/>
								<h4>Types of Trees in Location <?php echo $count; ?></h4><hr>
									<div class="control-group">
									 <label class="control-label" >How many species?: (*)</label>
										<div class="controls">
											<select data-count1="<?php echo count($location->species); ?>" class="required"  onChange="showallspecies(this.value,<?php echo $count; ?>,<?php echo count($location->species); ?>)" name="no_of_species<?php echo $count; ?>" id="no_of_species" required="">
												<option value="">--Select--</option>
												<?php for($i=1;$i<=10;$i++){ ?>
													<option <?php if(count($location->species)==$i){ echo 'selected="selected"'; } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
												<?php } ?>
												</select>
										</div>
									</div>
									<div id="all_species_data_<?php echo $count; ?>"  >
									<?php if(!empty($location->species)){
										$counter=1;
										foreach($location->species as $species){  ?>
											<div id="tree-species<?php echo $count; ?>" class="col-sm-12 tree-species-<?php echo $count; ?><?php echo $counter; ?>">
												<div class="panel panel-default">
													<div class="panel-heading">Species <?php echo $counter; ?></div>
													<div class="panel-body">
														<div class="control-group">
															<label class="control-label">Name: (*)</label>
															<div class="controls">
																<!--select class="required" name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][species_name]" required="">
																	<option value="">--Select--</option>
																	<option <?php if($species->species_name=='Species1'){ echo 'selected="selected"'; } ?> value="Species1">Species1</option>
																	<option <?php if($species->species_name=='Species2'){ echo 'selected="selected"'; } ?> value="Species2">Species2</option>
																	<option <?php if($species->species_name=='Species3'){ echo 'selected="selected"'; } ?> value="Species3">Species3</option>
																</select-->
																<input class="required" id="species_field" type="text" value="<?php echo $species->species_name; ?>" name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][species_name]" required=""/>
																
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Number of trees: (*)</label>
															<div class="controls">
																<input class="required" type="number" value="<?php echo $species->no_of_trees; ?>" name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][no_of_trees]" required=""/>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Sourced as: (*)</label>
															<div class="controls">
																<select class="required" name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][types_of_trees]" required="">
																	<option value="">--Select--</option>
																	<option <?php if($species->species_seeds=='Purchased as seeds'){ echo 'selected="selected"'; } ?> value="Purchased as seeds">Purchased as seeds</option>
																	<option <?php if($species->species_seeds=='Seedlings'){ echo 'selected="selected"'; } ?> value="Seedlings">Seedlings</option>
																	<option  <?php if($species->species_seeds=='Saplings'){ echo 'selected="selected"'; } ?>value="Saplings">Saplings</option>
																</select>
															</div>
														</div>
														<div class="control-group">
																<label class="control-label">Reason/benefits of this species: (*)</label>
															<div class="controls">
																<textarea class="required" name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][species_benefits]" style="height:88px;" class="required" required=""><?php echo $species->species_benefits; ?></textarea>
															</div>
														</div>
														<input type="hidden" value="<?php echo $species->id; ?>" name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][id]" required=""/>
														<input type="hidden" value="<?php echo $species->location_id; ?>" name="location[<?php echo $count; ?>][species][<?php echo $counter; ?>][location_id]" required=""/>
													  </div>
													  </div>
												</div>
										<?php $counter++;  } } ?>
									</div>
							</div>					
							</div>
						  </div>
			 <?php $count++;  } } ?>
			 </div>
			</div>
			<input type="hidden" name="edit_stage_2" value="edit_stage_2">
			
				<?php if ($_SESSION['login']['type'] == 'admin' || $_SESSION['login']['type'] == 'grant_admin') { ?>
				<div id="success_msg2"></div>
					<div class="controls wh_btns">
					<?php if($stage2_status==0 || $stage2_status==2){ ?>
						<button class="btn btn-success" type="submit"  name="edit_stage_2">Save</button>
						<button class="btn btn-success" id="confirmstage2" type="button" onclick="confirm_stage2('<?php echo $user_id; ?>','2')" name="edit_stage_2">Confirm Stage 2</button>
						<button class="btn btn-success" type="button" id="contact_form"  name="contact">Contact</button>
						<?php if($stage2_status==2){ ?>
							<button class="btn btn-success" type="button" onclick="lock_stage1('<?php echo $user_id; ?>','2')" id="lock_button"  name="unlock_button">Lock</button>
						<?php }else{  ?>
							<button class="btn btn-success" type="button" onclick="unlock_stage1('<?php echo $user_id; ?>','2')" id="unlock_button"  name="unlock_button">Unlock</button>
						<?php } ?>
					<?php } else{ ?>
						   <?php if ($_SESSION['login']['type'] == 'grant_admin') { ?>
							<button class="btn btn-success" type="submit"  name="edit_stage_2">Save</button>
						   <?php } ?>
							<button class="btn btn-success" type="button"  name="edit_stage_2">Stage 2 Already confirmed</button>
					<?php } ?>
				</div>
			<?php }else{ ?>
				<div class="controls wh_btns">
					<?php if($stage2_status==1){ ?>
						<button class="btn btn-success" type="button" disabled="disabled">Save</button>
					<?php }elseif($stage2_status==2){ ?>
						<button class="btn btn-success" type="button" name="edit_stage_2" id="edit_stage_2">Submit for Approval</button>
					<?php	}else{ ?>
						<button class="btn btn-success" type="button" name="edit_stage_2" <?php if(isset($stage2_status)){ echo 'disabled="disabled"'; } ?> id="edit_stage_2">Save</button>
						<button class="btn btn-success" style="display:none" type="submit" name="edit_stage_2" id="edit_stage_2_submit">Save</button>
					<?php } ?>
				</div>
			<?php } ?>
			<br>
	 </div>
	 
			<!-- Modal -->
				<div class="modal fade" id="step2Modal">
					<div class="modal-dialog modal-sm">
					  <div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <!--<h4 class="modal-title">Modal Header</h4>--->
						</div>
						<div class="modal-body">
							<p>This will submit this information for review, after review this information cannot be changed, are you sure?</p>
						</div>
						<div class="modal-footer">
						  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
						  <button type="submit" onclick="jQuery('#edit_stage_2_submit').trigger('click');jQuery('#step2Modal').modal('hide');" class="btn btn-primary">Yes</button>
						</div>
					  </div>
					</div>
				</div>
			<!-- Modal -->
			<div class="modal fade" id="ttModalsuccess2">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
				  <form action="http://bit.ly/GEA-Review">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <!--<h4 class="modal-title">Modal Header</h4>--->
					</div>
					<div class="modal-body">
						<p>Thank you for your submission – this has been sent for a review. Please wait for further instructions via your designated email address.</p>
					</div>
					<div class="modal-footer">
					  <button type="submit" class="btn btn-primary">OK</button>
					</div>
					</form>
				  </div>
				</div>
			</div>
	 
</form>
<script type="text/javascript" src="<?php echo base_url(); ?>/js/jquery.smart_autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/js/qs_score.js"></script>
 <script type="text/javascript">
 var species_lists = <?php print_r((json_encode($species_lists))); ?>;
	jQuery(document).ready(function(){
        $("#species_field").smartAutoComplete({source: species_lists });
		});
	
  </script>
 
<?php  if($success2){ ?>
<script>
	jQuery(document).ready(function(){
		jQuery("#ttModalsuccess2").modal("show");
	});
	
</script>
<?php } ?>

 
<?php
 if ($_SESSION['login']['type'] == 'grant_user') {
	 if(($stage2_status!='' || $stage2_status=='1') &&  $stage2_status!='2'){ ?>
	<script>
		jQuery(document).ready(function(){
			jQuery("#stage2_form").find("input,textarea,select").attr("disabled", "disabled");
		});
	</script>
 <?php }
 } ?>

<script  src="<?php echo base_url(); ?>js/custom_google_map.js"  type="text/javascript" ></script>
<script>
 function confirm_stage2(user_id,stage){
		if(confirm("Do you really want to confirm the Stage 2?")) {
		var user_id = user_id;
 			$.post('<?php echo site_url('user/stage_confirmed/');?>',
			{ 
				 user_id : user_id,
				 stage : stage
			}, 
 			function(data) 
			{
				$("#success_msg2").html('Stage 2 confirmed successfully!');
				$("#confirmstage2").text('Stage 2 confirmed');
			}); 
			}else {
				return false; // cancel the event
			}
		}
	
	jQuery(document).ready(function(){
		jQuery("#edit_stage_2").on('click',function(e){
			 e.preventDefault();
			jQuery("#step2Modal").modal("show");
		});
	});

</script>
<style>
.progress-striped .bar {
    background-image: none;
}
#success_msg2 {
  color: green;
  font-size: 17px;
  margin-bottom: 10px;
  padding: 4px 6px;
}
.smart_autocomplete_container li {list-style: none; cursor: pointer;}
.smart_autocomplete_container li.smart_autocomplete_highlight {background-color: #149BDF;}
.smart_autocomplete_container { margin: 10px 0; padding: 5px; background-color: #A3CCFF; }
.smart_autocomplete_container {  overflow: scroll!important;  height: 200px;}
</style>