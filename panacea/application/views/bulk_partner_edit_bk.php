<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo site_url("user/edit_bulk_partner/$id")?>">
        <fieldset>
            <legend class="text-center"><?php echo $title;?></legend>
            <div class="controls">
                <?php echo validation_errors(); ?>
                <?php if (isset($error)) echo "<div class='text-error'>$error</div>"; ?>
            </div>
            <div class="control-group">
                <label class="control-label">Partner Landing Page:</label>
                <div class="controls">
                    <a href="<?php echo 'http://www.greenearthappeal.org/?refid='.$code; ?>" target="_blank"><?php echo 'http://www.greenearthappeal.org/?refid='.$code; ?></a>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"></label>
                <div class="controls">
                    <img src="<?php echo base_url().'tickers/ticker_'.$code.'.png'?>" />
                </div>
            </div>
            <!---->
            <div class="control-group">
                <label class="control-label">First Name: (Solicitor)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $first_name2; ?>" name="first_name2" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Email: (Solicitor)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $email2; ?>" name="email2" />
                </div>
            </div>
            <hr />
            <!----->
            
			<div class="control-group">
                <label class="control-label">Company Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $company; ?>" name="company" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Company Logo (200 x 200 px):</label>
                <div class="controls">
                    <?php if ($logo) echo "<img width='200' src='".base_url()."uploads/company/$logo' />";?>
                    <input type="file" name="logo" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Website:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $website; ?>" name="website" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">First Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $first_name; ?>" name="first_name" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Last Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $last_name; ?>" name="last_name" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Email: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $email; ?>" name="email" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Username: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $username; ?>" name="username" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password:</label>
                <div class="controls">
                    <input type="password" value="" name="password" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password Conf:</label>
                <div class="controls">
                    <input type="password" value="" name="password2" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Phone:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $phone ?>" name="phone" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Address: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $address ?>" name="address" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">City: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $city ?>" name="city" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Area: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $state ?>" name="state" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Post Code: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $post_code ?>" name="post_code" />
                </div>
            </div> 
            <?php if ($_SESSION['login']['type']=='admin') :?>
            <div class="control-group">
                <label class="control-label">Tree number (*):</label>
                <div class="controls">
                    <input type="text" value="<?php echo $tree_nums; ?>" name="tree_nums" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Active:</label>
                <div class="controls">
                    <label class="radio inline">
                        <input type="radio" value="1" name="active" <?php echo ($active==1) ? 'checked' : '';?> /> Active
                    </label>
                    <label class="radio inline">
                        <input type="radio" value="0" name="active" <?php echo ($active==0) ? 'checked' : '';?> /> Inactive
                    </label>
                </div>
            </div>
            
            <!---->
            <div class="control-group">
                <label class="control-label">Cost per tree (*):</label>
                <div class="controls">
                    <input type="text" style="width: 50px;" name="price" value="<?php echo $price;?>" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"> Currency (*):</label>
                <div class="controls">
                    <select name="currency">
                        <option value="">--Select--</option>
                        <option value="1" <?php if( $currency==1) echo "selected='selected'" ?>>cent</option>
                        <option value="2" <?php if( $currency==2) echo "selected='selected'" ?>>pence</option>

                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Tax (*):</label>
                <div class="controls">
                    <input type="text" style="width: 50px;" name="tax" value="<?php echo $tax;?>" /> %
                </div>
            </div>
            <!------>
            <hr />
            <p class="controls text-info">Fields for email template is sent to bulk_partner</p>
            <div class="control-group">
                <label class="control-label">Subject (*):</label>
                <div class="controls">
                    <input type="text" style="width: 100%;" name="subject" value="<?php echo $subject;?>" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Message (*):</label>
                <div class="controls">
                    <textarea style="width: 100%; height:200px;" name="message"><?php echo $message;?></textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Text on Certificate (*):</label>
                <div class="controls">
                    <textarea style="width: 100%; height:200px;" name="certificate_text"><?php echo $certificate_text;?></textarea>
                </div>
            </div>
            <?php endif;?>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-success" type="submit" name="edit_bulk_partner">Save</button>
                    <a href="<?php echo site_url('user/index');?>" class="btn btn-warning">Cancel</a>
				</div>
			</div>
		</fieldset>
	</form>
</div>