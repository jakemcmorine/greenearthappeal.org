<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( "#date_from, #to_date" ).datepicker({
        dateFormat: 'yy-mm-dd'
    });
  } );
 
</script>
<style>
.wk_form .control-group {
    float: left;
    width: 46%;
    padding: 0px 2%;
}
.wk_form .form-horizontal .control-label {
    padding-top: 5px;
    text-align: left;
}
.wk_form .form-horizontal .controls {
    margin: 0px;
    width: 100%;
}
.wk_form .controls input, .wk_form .controls select {
    width: 100%;
	padding-left:0px;
	padding-right:0px;
}
.wk_form .form-horizontal {
    margin-top: 20px;
}
</style>
<div class="span7 offset4 wk_form">
    <form accept-charset="utf-8" method="POST" class="form-horizontal" action="" enctype="multipart/form-data">
        <fieldset>
            <legend class="text-center">
                <?php echo $title;?>
            </legend>
            <div class="controls">
                <?php if (isset($error)) echo '<div class="text-error">'.$error.'</div>';?>
            </div>
			<div class="control-group ">
                <label class="control-label">From Date</label>
                <div class="controls">
					 <input type="text" name="date_from" value="<?php echo $this->input->post('date_from'); ?>" Placeholder="Select From Date" id="date_from" required="required">
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">To Date</label>
                <div class="controls">
					 <input type="text" name="to_date" value="<?php echo $this->input->post('to_date'); ?>" Placeholder="Select To Date" id="to_date" required="required">
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Status</label>
                <div class="controls">
					 <select class="form-control" id="status" name="status">
						<?php $selected = $this->input->post('status'); ?>
						<option value="">Select Status</option>
						<option value="1" <?php if($selected =='1'){ echo 'selected=selected'; } ?>>Created</option>
						<option value="2" <?php if($selected =='2'){ echo 'selected=selected'; } ?>>Paid</option>
						<option value="3" <?php if($selected =='3'){ echo 'selected=selected'; } ?>>Unpaid</option>
						<option value="4" <?php if($selected =='4'){ echo 'selected=selected'; } ?>>Verified</option>
					</select>
                </div>
            </div>
			<!--div class="control-group">
                <label class="control-label">Verified</label>
                <div class="controls">
					<select class="form-control" id="verify" name="verify">
						<option value="">Select</option>
						<option value="1">YES</option>
						<option value="0">NO</option>
					</select>
                </div>
            </div-->
			<div class="control-group">
                <label class="control-label">Select Initiative</label>
                <div class="controls">
					<select class="form-control" id="initiative" name="initiative">
						<?php $select = $this->input->post('initiative'); ?>
						<option value="">Select</option>
						<?php foreach($results as $res){ ?>
							<option value="<?php echo $res['id']; ?>" <?php if($select == $res['id']){ echo 'selected=selected'; } ?>><?php echo $res['local_name']; ?></option>
						<?php }  ?>
					</select>
                </div>
            </div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-success" type="submit" name="create_invoice_report">Create Report</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<?php 

	//echo "<pre>"; print_r($invoices); die;
		if(!empty($invoices)){
	
			$filename = 'Report'."_".date("Y-m-d_H-i",time()).".csv";
			$fp = fopen('reports/'.$filename, 'w');
			$count = 1;
			$total_trees = 0;
			$total_amount = 0;
			foreach($invoices as $invoice)
			{
				if($invoice->company !=""){
					$invoice_date = date('Y.m.d',strtotime($invoice->invoice_date));
					 if($count == '1'){
						if($this->input->post('status') == "2")	{
							$rows = array('Company','Amount Trees','Invoice Number','Money','Date Invoice','Paid Date');
						}else{
							$rows = array('Company','Amount Trees','Invoice Number','Money','Date Invoice');
						}
						 fputcsv($fp, $rows);
					 }
					 if($this->input->post('status') == "2"){	
						$paid_date = date('Y.m.d',strtotime($invoice->invoice_paid_date));
						 $rows = array($invoice->company,$invoice->trees, $invoice->invoice_id,$invoice->total,$invoice_date,$paid_date);
					 }else{
						 $rows = array($invoice->company,$invoice->trees, $invoice->invoice_id,$invoice->total,$invoice_date);
					 }
					fputcsv($fp, $rows);
					$total_trees += $invoice->trees;
					$total_amount += $invoice->total;
					$count ++;
				}
			}
			$rows = array('Total',$total_trees,'',$total_amount,'');
			fputcsv($fp, $rows);
			
		
		fclose($fp);
		?>
	<script>
		setTimeout(function(){ 
			window.location.href="https://www.greenearthappeal.org/panacea/reports/<?php echo $filename; ?>";
		 }, 1000);
	</script>
	<?php }	?>

