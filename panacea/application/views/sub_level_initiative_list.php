<fieldset>
    <legend>Sub Level Initiative List</legend>
    <table class="table table-bordered table-striped">
        <tr>
            <th>ID</th>
			<th>Email</th>
			<th>First Name</th>
            <th>Last Name</th>
            <th>Title</th>
            <th>Sub title</th>
			<th>Opportunity String</th>
            <th>Referer ID</th>
            <th>Tree Number</th>
            <th width="205"></th>
        </tr>
        <?php
            if (count($rows)) :
                foreach ($rows AS $row):
					$url_edit = site_url('user/edit_sub_level_initiative/'.$row->id);
					$url_delete = site_url('user/delete_sub_level_initiative/'.$row->id);
        ?>
            <tr>
                <td><?php echo $row->id;?></td>
                <td><?php echo $row->email;?></td>
                <td><?php echo $row->first_name;?></td>
                <td><?php echo $row->last_name;?></td>
                <td><?php echo $row->title;?></td>
                <td><?php echo $row->subtitle;?></td>
				<td><?php echo $row->opportunity_str;?></td>
                <td><?php echo $row->code;?></td>
                <td><?php echo $row->tree_nums;?></td>
                <td>
				<?php if($row->tcc_api!='1'){ ?>
                    <a class="btn btn-info btn-small" href="<?php echo $url_edit;?>">Edit</a>
				<?php } ?>
                    <a class="btn btn-warning btn-small" href="<?php echo $url_delete;?>">Delete</a>
                </td>
            </tr>
        <?php endforeach; endif;?>
    </table>
</fieldset>