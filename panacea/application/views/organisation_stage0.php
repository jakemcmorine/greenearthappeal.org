<form accept-charset="utf-8" id="stage0_form" method="post" class="form-horizontal" enctype="multipart/form-data" action="">
	<div class="panel panel-info">
		<div class="panel-heading text-center">Initial Data</div>
		<div class="panel-body">
		<div class="controls">
			<?php if (isset($error)) echo "<div class='text-error'>$error</div>";  ?>
		</div><hr>
		
		<div class="control-group">
			<label class="control-label">First Name:</label>
			<div class="controls">
				<input type="text" value="<?php  echo $grant_user->first_name; ?>"  name="first_name" id="first_name"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Surname: </label>
			<div class="controls">
				<input type="text" value="<?php  echo $grant_user->last_name; ?>"  name="last_name" id="last_name"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Email Address: </label>
			<div class="controls">
				<input type="text" value="<?php  echo $grant_user->email; ?>"  name="email" id="email"/>
			</div>
		</div>
		
		<!--div class="control-group">
			<label class="control-label">Description : </label>
			<div class="controls">
				<input type="text" value="<?php  // echo $grant_user->description;  ?>"  name="description" id="description"/>
			</div>
		</div-->
		<div class="control-group">
			<label class="control-label">Name of Organisation : </label>
			<div class="controls">
				<input type="text" value="<?php   echo $grant_user->organisation_name;  ?>"  name="description" id="description"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Address  : </label>
			<div class="controls">
				<input type="text" value="<?php   echo $grant_user->address;  ?>"  name="description" id="description"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Website  : </label>
			<div class="controls">
				<input type="text" value="<?php echo $grant_user->website;  ?>"  name="description" id="description"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Logo: </label>
			<div class="controls">
			<?php if(($grant_user->logo!='')) { 
				echo "<img id='result' width='100' src='https://www.greenearthappeal.org/wp-content/uploads/plants_trees/".$grant_user->logo ."' />";
			 } ?>
			 </div>
		</div>
		<div class="control-group">
			<label class="control-label">Organisation Type  : </label>
			<div class="controls">
				<input type="text" value="<?php   echo $grant_user->organisation_type;  ?>"  name="description" id="description"/>
			</div>
		</div>
		<?php if($grant_user->organisation_type=="Non-Governmental Organisation" || $grant_user->organisation_type=="Charity" || $grant_user->organisation_type=="Registered Company"){ ?>
		<div class="control-group">
			<label class="control-label">Registration No.  : </label>
			<div class="controls">
				<input type="text" value="<?php   echo $grant_user->registration_no;  ?>"  name="description" id="description"/>
			</div>
		</div>
		<?php } ?>
			<div class="control-group">
			<label class="control-label">Number of Trees   : </label>
			<div class="controls">
				<input type="number" value="<?php  echo $grant_user->no_of_trees;  ?>"  name="no_of_trees" id="no_of_trees"/>
			</div>
		</div>
			<div class="control-group">
			<label class="control-label">Funding from us ($ USD)  : </label>
			<div class="controls">
				<input type="number" value="<?php echo $grant_user->project_fund;  ?>"  name="project_fund" id="project_fund"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Hear about us : </label>
			<div class="controls">
				<input type="text" value="<?php   echo $grant_user->hear_about_us;  ?>"  name="hear_about_us" id="hear_about_us"/>
			</div>
		</div>
		<?php if($grant_user->hear_about_us=="Search engine"){ ?>
		<div class="control-group">
			<label class="control-label">Search for : </label>
			<div class="controls">
				<input type="text" value="<?php   echo $grant_user->search_for;  ?>"  name="search_for" id="search_for"/>
			</div>
		</div>
		<?php } ?>
		<?php if($grant_user->hear_about_us=="Referral"){ ?>
		<div class="control-group">
			<label class="control-label">Introduced to our organisation : </label>
			<div class="controls">
				<input type="text" value="<?php   echo $grant_user->introduced_you;  ?>"  name="introduced_you" id="introduced_you"/>
			</div>
		</div>
		<?php } ?>
		<?php if($grant_user->hear_about_us=="Other"){ ?>
		<div class="control-group">
			<label class="control-label">Other : </label>
			<div class="controls">
				<input type="text" value="<?php   echo $grant_user->others;  ?>"  name="others" id="others"/>
			</div>
		</div>
		<?php } ?>
		<div class="control-group">
			<label class="control-label">Type of funding: </label>
			<div class="controls">
				<input type="text" value="<?php   echo ucfirst($grant_user->type_of_fund);  ?>"  name="description" id="description"/>
			</div>
		</div>
		
	</div>
	</div>

</form>

<script>
 	 jQuery(document).ready(function(){
		jQuery("#stage0_form").find("input,textarea,select").attr("disabled", "disabled");
	});    
</script>
<style>
.progress-striped .bar {
    background-image: none;
}
#success_msg,#email_msg {
  color: green;
  font-size: 17px;
  margin-bottom: 10px;
  padding: 4px 6px;
}
@import url('https://netdna.bootstrapcdn.com/bootstrap/2.3.2/css/bootstrap.min.css');
 .mystage {
    width:400px;
}
.modal {
    position: fixed;
    top: 0;
    right:inherit;
	background-color: transparent;
    bottom: 0;
    left:50%;
    z-index: 1050;
    display: none;
     -webkit-overflow-scrolling: touch;
	 width:530px;
	 border:none; box-shadow:none;
	 
}
.modal.fade .modal-dialog{
	width:100%; margin:0;
}

/* .new_table > b {
  top: 78px;
  left: -75px;
  position: absolute;
}
.new_table {
  position: relative;
} */
.new_table > b {
  position: relative;
  top: 99px;
  left: -70px;
}
.input-group-addon
{
	border-left:none!important;
	border-top:none!important;
	border-bottom:none!important;
	border-right:1px solid #ddd!important;
	border-radius:0!important;
	padding: 7px 12px!important;
}
.total-cost-group
{
	position:relative;
}


.total-cost-group input {
  padding-left: 35px!important;
}
.total-cost-group .input-group-addon {
  position: absolute;
  margin-top: 1px;
  margin-left: 1px;
}

@media screen and (max-width:767px)
{
	.modal  {width:97%; left:0; right:0; margin:0 auto;}
	
}
</style>