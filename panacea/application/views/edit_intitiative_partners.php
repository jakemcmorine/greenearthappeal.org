<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div class="container">
	<legend><?php echo $title; ?></legend>
	<?php echo validation_errors(); ?>
</div>
<div id="exTab3" class="container">	
<div class="wh_formss">
<ul  class="nav nav-pills">
			<!--li class="active grayclass">
				<a  href="#1b" data-toggle="tab">Social Media</a>
			</li-->
			<li class="active grayclass">
				<a href="#2b" data-toggle="tab">Company Info</a>
			</li>
			<li class="grayclass">
				<a href="#3b" data-toggle="tab">User Info</a>
			</li>
		</ul>

			<div class="tab-content clearfix">
			  <!--div class="tab-pane active" id="1b">
				   <form accept-charset="utf-8" method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo site_url("user/edit_intitiative_partner_clients/$id") ?>">
				  <fieldset>
				  <legend class="text-center">Social Media
				    <p style="font-size:16px;"><?php echo $social_text ?></p></legend></legend>
				   <div class="controls">
								<?php if (isset($error)) echo "<div class='text-error'>$error</div>"; ?>
					  </div>
					 <div class="control-group">
							<label class="control-label">Company Logo (200 x 200 px):</label>
							<div class="controls">
								<?php if ($logo) echo "<img width='200' src='" . base_url() . "uploads/company/{$logo}?". time() ."' />"; ?>
								<input type="file" name="logo" accept="image/*" />
							</div>
					  </div>
						<div class="control-group">
								<label class="control-label">Twitter Profile URL:</label>
								<div class="controls">
									<input type="text" placeholder="https://twitter.com/greenearthapp" value="<?php echo $twitter ?>" name="twitter" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Facebook Page URL:</label>
								<div class="controls">
									<input type="text" placeholder="https://www.facebook.com/GreenEarthAppeal" value="<?php echo $facebook ?>" name="facebook" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Instagram Profile URL:</label>
								<div class="controls">
									<input type="text" placeholder="https://www.instagram.com/greenearthappeal" value="<?php echo $instagram ?>" name="instagram" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Pinterest Profile URL:</label>
								<div class="controls">
									<input type="text" placeholder="https://www.pinterest.com/greenearthappea" value="<?php echo $pinterest ?>" name="pinterest" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Linkedin Page URL:</label>
								<div class="controls">
									<input type="text" placeholder="https://www.linkedin.com/in/geahopartnerships" value="<?php echo $linkedin ?>" name="linkedin" />
								</div>
							</div>
							<input type="hidden" name="social_info" value="social_info">
							<div class="controls">
									<button class="btn btn-success" type="submit" name="edit_intitiative_partner_clients">Save</button>
								</div>
							<fieldset>
					</form>
				</div-->
			<div class="tab-pane active" id="2b">
				<form accept-charset="utf-8" method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo site_url("user/edit_intitiative_partner_clients/$id") ?>">
					<fieldset>
						<legend class="text-center">Company info
						  <p style="font-size:16px;"><?php echo $company_text ?></p></legend></legend>
						<div class="controls">
							<?php if (isset($error)) echo "<div class='text-error'>$error</div>"; ?>
						</div>
						
					   <div class="control-group">
							<label class="control-label">Initiative: (*)</label>
							<div class="controls">
								<select name="initiative" <?php if($_SESSION['login']['type']=='intitiative') { echo "disabled"; } ?> >
									<option value="">--Select--</option>
										<?php if(count($results)) { foreach($results as $result){ 
										$selected = ($result['id'] == trim($initiative)) ? 'selected="selected"' : ''; ?>
										<option value="<?php echo $result['id']; ?>"<?php echo $selected; ?>><?php echo $result['initiative_name']; ?></option>
										<?php } } ?>
								</select>
							</div>
						</div>	
					<div class="control-group">
						<label class="control-label">Partner Landing Page:</label>
						<?php if(empty($website_url)){
								$website_url= "http://www.greenearthappeal.org";
							} ?>
						<div class="controls">
							<a href="<?php echo $website_url.'/?refid='. $code; ?>" target="_blank" style="background: transparent none repeat scroll 0% 0% ! important;"><?php echo $website_url.'/?refid='. $code; ?></a><br>
							<!--a style="background: transparent none repeat scroll 0% 0% ! important;" href="<?php echo 'http://www.greenearthappeal.org/?refid=' . $code; ?>" target="_blank"><?php echo 'http://www.greenearthappeal.org/?refid=' . $code; ?></a-->
							<a class="btn btn-info btn-xs" href="<?php echo $website_url.'/?refid='. $code; ?>" target="_blank">Visit Page</a>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Your Tree Counter:</label>
						<div class="controls">
							<img src="<?php echo base_url() . 'tickers/ticker_' . $code . '.png'; ?>" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Use this URL to link to your counter:</label>
						<div class="controls">
							<a id="copytext" style="background: transparent none repeat scroll 0% 0% ! important;" target="_blank" href="<?php echo base_url() . 'tickers/ticker_' . $code . '.png'; ?>"><?php echo base_url() . 'tickers/ticker_' . $code . '.png'; ?></a></br>
							<button type="button" class="btn btn-info btn-xs" onClick="copyToClipboard('copytext');">Copy URL</button>

						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Use this URL to link to your certificate(Full):</label>
						<div class="controls">
							<a id="copyfullimg" style="background: transparent none repeat scroll 0% 0% ! important;" target="_blank" href="<?php echo base_url() . 'cert/full_' . $code . '.jpg'; ?>"><?php echo base_url() . 'cert/full_' . $code . '.jpg'; ?></a></br>
							<button type="button" class="btn btn-info btn-xs" onClick="copyToClipboard('copyfullimg');">Copy URL</button>

						</div>
				    </div>
					<div class="control-group">
								<label class="control-label">Use this URL to link to your certificate(Thumbnail):</label>
								<div class="controls">
									<a id="copythumbimg" style="background: transparent none repeat scroll 0% 0% ! important;" target="_blank" href="<?php echo base_url() . 'cert/thumb_' . $code . '.jpg'; ?>"><?php echo base_url() . 'cert/thumb_' . $code . '.jpg'; ?></a></br>
									<button type="button" class="btn btn-info btn-xs" onClick="copyToClipboard('copythumbimg');">Copy URL</button>

								</div>
					</div>
			   
					<div class="control-group">
						<label class="control-label">Company Name:</label>
						<div class="controls">
							<input type="text" value="<?php echo $company; ?>" name="company" id="company" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Website:</label>
						<div class="controls">
							<input type="text" value="<?php echo $website; ?>" name="website" />
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Phone:</label>
						<div class="controls">
							<input type="text" value="<?php echo $phone ?>" name="phone" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Address: (*)</label>
						<div class="controls">
							<input type="text" value="<?php echo $address ?>" name="address" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">City: (*)</label>
						<div class="controls">
							<input type="text" value="<?php echo $city ?>" name="city" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Area: (*)</label>
						<div class="controls">
							<input type="text" value="<?php echo $state ?>" name="state" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Post Code: (*)</label>
						<div class="controls">
							<input type="text" value="<?php echo $post_code ?>" name="post_code" />
						</div>
					</div>
					<input type="hidden" name="company_info" value="company_info">
					<div class="control-group">
						<div class="controls">
							<button class="btn btn-success" type="submit" name="edit_intitiative_partner_clients">Save</button>
						</div>
					</div>
				</fieldset>
			</form>
			</div>
			<div class="tab-pane" id="3b">
				   <form accept-charset="utf-8" method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo site_url("user/edit_intitiative_partner_clients/$id") ?>">
				  <fieldset>
				  <legend class="text-center">User Info
				  <p style="font-size:16px;"><?php echo $user_text ?></p></legend>
				   <div class="controls">
								<?php if (isset($error)) echo "<div class='text-error'>$error</div>"; ?>
					  </div>
					  <div class="control-group">
						<label class="control-label">First Name:</label>
						<div class="controls">
							<input type="text" value="<?php echo $first_name; ?>" name="first_name"  id="first_name"/>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Last Name:</label>
						<div class="controls">
							<input type="text" value="<?php echo $last_name; ?>" name="last_name" id="last_name"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Email: (*)</label>
						<div class="controls">
							<input type="text" value="<?php echo $email; ?>" name="email" id="email"/>
						</div>
					</div>
					<div class="control-group"> 
						<label class="control-label"></label>
						<div class="controls">
							<a style="background: transparent none repeat scroll 0% 0% ! important;" target="_blank" href="https://www.greenearthappeal.org/panacea/user/change_client_password">Click here to Change Password</a>
						</div>
					</div>
					
						<input type="hidden" name="user_info" value="user_info">
						<div class="controls">
									<button class="btn btn-success" type="submit" name="edit_intitiative_partner_clients">Save</button>
								</div>
							</fieldset>
					</form>
				</div>
			</div>
  </div>
  </div>
<div id="dialog" title="Email Preview">

</div>
<script  src="<?php echo base_url(); ?>js/jquery-1.10.2.js"  type="text/javascript" ></script>
<script  src="<?php echo base_url(); ?>js/jquery-ui.js"  type="text/javascript" /></script>
<link href="<?php echo base_url(); ?>css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
	function copyToClipboard(elementId) {
	 var aux = document.createElement("input");
	 aux.setAttribute("value", document.getElementById(elementId).innerHTML);
	 document.body.appendChild(aux)
	 aux.select();
	 document.execCommand("copy");
	 document.body.removeChild(aux);
	 //alert('Link copied.');
}
    $("#dialog").dialog({autoOpen: false, draggable: true, width: 800, maxHeight: 350});
    $("#dialog-link").click(function (event) {
        var first = $("#first_name").val();
        var last_name = $("#last_name").val();
        var email = $("#email").val();
        var username = $("#username").val();
        var password = $("#password").val();
        var name = $("#company").val();
        var message = $("#message").val().toString();
        /*var mapObj = {"[first_name]":first_name};
         var message2 = replaceAll(message, mapObj);*/

        message = message.replace("[First_Name]", first);
        message = message.replace('[Email]', email);
        message = message.replace('[username]', username);
        message = message.replace('[Name]', name);

        $("#dialog").html(message);
        /*$.ajax({
         url:'<?php echo site_url('user/preview_template'); ?>',
         method: 'post',
         data: {'first_name':first_name, 'company':name, 'email':email, 'username':username, 'password':password, 'tempid': 11}		
         }).done(function(data) {
         $("#dialog").html(data);
         });
         */

        $("#dialog").dialog("open");
        event.preventDefault();
    });

    /*	function replaceAll(str,mapObj){
     var re = new RegExp(Object.keys(mapObj).join("|"),"gi");
     
     return str.replace(re, function(matched){
     return mapObj[matched];
     });
     }*/
</script>
<style>
body {
  padding : 10px ;
  
}
.wh_formss input, textarea, .uneditable-input {
  width: 310px;
}

#exTab1 .tab-content {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}

#exTab2 h3 {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}


#exTab3 .wh_formss {
  margin: 0 auto;
  width: 70%;
}

#exTab3 .nav.nav-pills
{
	margin-bottom:0px!important;
}
/* remove border radius for the tab */

#exTab1 .nav-pills > li > a {
  border-radius: 0;
}

/* change border radius for the tab , apply corners on top*/

#exTab3 .nav-pills > li > a {
  border-radius: 4px 4px 0 0 ;
}

#exTab3 .tab-content {
  border: 1px solid #E5E6E7;
  padding : 5px 15px;
}
.tab-content.clearfix {
  width: 100%;
}
#exTab3 .nav-pills > li > a
{
	padding:13px!important;
}
.call_us
{
	margin-top:50px;
}
.grayclass a{
	 background-color: #eee;
}
.active a{
	 background-color: #08c!important;
}
.pull-right {
    float: left!important;
    margin-left: 100px!important;
}
</style>