<fieldset>
    <legend><?php echo $title;?></legend>
    <table class="table table-bordered table-striped">
        <tr>
            <th>ID</th>
            <th>Email</th>
            <th>Company</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Trees num</th>
            <!--<th width="105"></th>--->
        </tr>
        <?php
            if (count($rows)) :
                foreach ($rows AS $row):
                    /*if ($row->type == 'restaurant') {
                        $url_edit = site_url('user/edit_restaurant/'.$row->id);
                        $url_delete = site_url('user/delete_restaurant/'.$row->id);
                    } elseif ($row->type == 'bulk_partner') {
                        $url_edit = site_url('user/edit_bulk_partner/'.$row->id);
                        $url_delete = site_url('user/delete_bulk_partner/'.$row->id);
                    } else {
                        $url_edit = site_url('user/edit_admin/'.$row->id);
                        $url_delete = site_url('user/delete_admin/'.$row->id);
                    }*/
        ?>
            <tr>
                <td><?php echo $row->id;?></td>
                <td><?php echo $row->email;?></td>
                <td><?php echo $row->name;?></td>
                <td><?php echo $row->first_name;?></td>
                <td><?php echo $row->last_name;?></td>
                <td><?php echo $row->tree_nums;?></td>
                <!--<td><?php echo $row->type;?></td>
                <td><?php echo $row->active;?></td>
                <td>
                    <a class="btn btn-info btn-small" href="<?php echo $url_edit;?>">Edit</a>
                    <a class="btn btn-warning btn-small" href="<?php echo $url_delete;?>">Delete</a>
                </td>-->
            </tr>
        <?php endforeach; endif;?>
    </table>
</fieldset>