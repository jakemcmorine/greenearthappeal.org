<script  src="<?php echo base_url(); ?>js/jquery-1.10.2.js"  type="text/javascript" ></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--  datepicker   -->
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<!--  datepicker   -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/js/iEdit.css">
<script type="text/javascript" src="<?php echo base_url(); ?>/js/iEdit.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/js/script.js"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCVezk6CcJmErVr8CDwvMRXuXag80Yi8K0"></script>
<div class="container">
	<legend><?php echo 'Tree Planting Grant Application';//echo $title; ?></legend>
	<?php echo validation_errors(); ?>
</div>
<?php //echo "<pre>"; print_r($persons); die; ?>
<div id="exTab3" class="container wh_container">	
<div class="wh_formss">
<ul  class="nav nav-pills">
			<?php if($_SESSION['login']['type'] == 'admin' || $_SESSION['login']['type'] == 'grant_admin'){ ?>
			<li class="grayclass">
				<a id="stage1_0a" href="#stage0" data-toggle="tab">
				Initial Data
				</a>
			</li>

			<?php } ?>
			<?php if($status==1){ ?>
			<li class="grayclass <?php if($status==1 && $stage2_status==1 && $stage3_status==1){ echo 'active';} ?>">
				<a id="stage1_1a" href="#stage1" data-toggle="tab">
				<?php if($status!='' || $status=='1'){ ?>
				<span class="glyphicon glyphicon-lock"></span>
				<?php } ?>
				Project Overview
				</a>
			</li>
			<?php 
			}else{
			?>
			<li class="grayclass <?php if($status==0 || $status==2){  echo 'active'; }?>">
				<a id="stage1_1a" href="#stage1" data-toggle="tab">Project Overview</a>
			</li>
			<?php }?>
			<?php if($status==1 && isset($stage2_status)){ ?>
			<li class="grayclass <?php if($status==1 && $stage2_status==0 && $stage3_status==0){ echo 'active';}elseif($stage2_status==2){  echo 'active'; }?>">	
				<a href="#2b" id="stage2_2b" class="<?php if($status==1){ echo "enable"; }else{ echo "disabled"; } ?>" data-toggle="tab">
			<?php if($stage2_status!='' || $stage2_status=='1' || $stage2_status=='2'){ ?>
				<span class="glyphicon glyphicon-lock"></span>
				<?php } ?>
				Project Details</a>
			</li>
			<?php }elseif($status==1 && $_SESSION['login']['type'] != 'admin'){
				?>
			<li class="grayclass <?php if($status==1 && $stage2_status==0 && $stage3_status==0){ echo 'active';}?>">	
				<a href="#2b" id="stage2_2b" class="<?php if($status==1){ echo "enable"; }else{ echo "disabled"; } ?>" data-toggle="tab">
				Project Details</a>
			</li>	
				<?php
				
			}

			?>
			
			<?php if($stage2_status==1 && isset($stage3_status)){ ?>
			<li class="grayclass <?php if($status==1 && $stage2_status==1 && $stage3_status==0){ echo 'active';} elseif($stage3_status==2){  echo 'active'; }?>">	
				<a href="#3b" id="stage3_3b" class="<?php if($status==1){ echo "enable"; }else{	echo "disabled"; } ?>" data-toggle="tab">
				<?php if($stage3_status!='' || $stage3_status=='1' || $stage3_status=='2'){ ?>
				<span class="glyphicon glyphicon-lock"></span>
				<?php } ?>
				Final Stage</a>
			</li>
			<?php }elseif($stage2_status==1 && $_SESSION['login']['type'] != 'admin'){
				?>
			<li class="grayclass <?php if($status==1 && $stage2_status==1 && $stage3_status==0){ echo 'active';}?>">	
				<a href="#3b" id="stage3_3b" class="<?php if($status==1){ echo "enable"; }else{	echo "disabled"; } ?>" data-toggle="tab">
				Final Stage</a>
			</li>	
				<?php
				
			}

			?>
		
		</ul>
			<div class="tab-content clearfix">
				 <div class="tab-pane" id="stage0">	 
						<?php $this->load->view('organisation_stage0'); ?>
				</div>
			  <div class="tab-pane <?php if($status==1 && $stage2_status==1 && $stage3_status==1){ echo 'active';}elseif($status==0 || $status==2){  echo 'active'; }?>" id="stage1">	 
			    	<?php $this->load->view('organisation_stage1'); ?>
				</div>
			<div class="tab-pane <?php if($status==1 && $stage2_status==0 && $stage3_status==0){ echo 'active';} elseif($stage2_status==2){  echo 'active'; }?>" id="2b">
				  <?php $this->load->view('organisation_stage2'); ?>
			</div>
			<div class="tab-pane <?php if($status==1 && $stage2_status==1 && $stage3_status==0){ echo 'active';}elseif($stage3_status==2){  echo 'active'; }?>" id="3b">
				   <?php $this->load->view('organisation_stage3'); ?>
			</div>
			</div>		
  </div>	
  </div>
<div id="dialog" title="Email Preview">
</div>
	<div class="modal fade" id="contact_message_form">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Contact</h4>
					</div>
					<div class="modal-body">
						<form id="my_conact_form" method="post">		
							<div class="form-group">
								 <label style="width:74px;" for="to">To:</label>
									<input value="<?php echo $user_data->email; ?>" id="email_to" name="email_to" type="text" required="">
							</div>
							<div class="form-group">
								<label style="width:74px;" class="control-label">Subject</label>
								<input value="Grant Application Submission" id="subject_to" name="subject" type="text" required="">
							</div>
							<div class="form-group">
								<label style="width:74px;" class="control-label">Message</label>
								<textarea style="height:135px;" name="message_to" id="message_to" required="">Dear <?php echo '{firstname},'; ?></textarea>
								<input  id="conact_firstname" name="cont" type="hidden" value="<?php echo $user_data->firstname; ?>">
							</div>
							<div class="form-group">
								<label style="width:74px;" class="control-label"></label>
								<button id="myFormSubmit" class="btn btn-success" type="submit">Send</button>
							</div>
							<div id="email_msg">
							</div>
						</form>
					</div>
					<div class="modal-footer">
					<button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
					</div>
				  </div>
				</div>
			</div>
		<!-- modal popup for first_time_user-->	
		<div class="modal fade" data-backdrop="static" data-keyboard="false" id="first_time_user">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
				  <form action="#" id="first_time_grant_user_form">
					<div class="modal-header">
					</div>
					<div class="modal-body">
						<p>Thank you for your interest in the Green Earth Appeal grant program. In order to process your application we require some details about your project. For your application to be successful, it should have as much supporting information as possible. Please ensure you have the authority to submit the information on behalf of your project.</p>
					</div>
					<div class="modal-footer">
					  <button type="submit" class="btn btn-primary">OK</button>
					</div>
					</form>
				  </div>
				</div>
			</div>

		
<?php  if($user_data->first_time_grant_user=='0' && $_SESSION['login']['type'] == 'grant_user'){ ?>
<script>
	jQuery(document).ready(function(){
		jQuery("#first_time_user").modal("show");
	});
	
</script>
<?php }  ?>
<script  src="<?php echo base_url(); ?>js/jquery-ui.js"  type="text/javascript" /></script>
<link href="<?php echo base_url(); ?>css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
<script  src="<?php echo base_url(); ?>js/my-custom-organiser.js"  type="text/javascript" ></script>
<script>
jQuery("#contact_form").click(function(){
		jQuery("#contact_message_form").modal("show");
	});
jQuery( "#first_time_grant_user_form" ).submit(function( e ) {
		var okay = 1;
		$.post('<?php echo site_url('user/firsttime_grant_user_login/');?>',
		{ 
			first_time : okay
		}, 
		function(data) 
		{
			jQuery("#first_time_user").modal("hide");
		}); 
	  e.preventDefault();
	}); 
	jQuery( "#my_conact_form" ).submit(function( e ) {
		var subject = $('#subject_to').val();
		var email = $('#email_to').val();
		var message = $('#message_to').val();
		var firstname = $('#conact_firstname').val();
		var message1 = message.replace('{firstname}', firstname);
		$.post('<?php echo site_url('user/stage_confirmed_email/');?>',
		{ 
			subject : subject,
			email : email,
			message : message1
		}, 
		function(data) 
		{
			jQuery("#email_msg").html('Email sent successfully!');
			setTimeout(function(){ 
				jQuery("#contact_message_form").modal("hide");
			}, 3000);
		}); 
	  e.preventDefault();
	});
$('#stage2_2b').click(function(){ 
	//alert(google_pin_location1);
	setTimeout(function(){ 
	google_pin_location1();
	google_pin_location2();
	google_pin_location3();
	google_pin_location4();
	google_pin_location5();
	google_pin_location6();
	google_pin_location7();
	google_pin_location8();
	google_pin_location9();
	google_pin_location10();
	}, 3000);
	
});
$('#locations').on('change', function (e) {
	google_pin_location1();
	google_pin_location2();
	google_pin_location3();
	google_pin_location4();
	google_pin_location5();
	google_pin_location6();
	google_pin_location7();
	google_pin_location8();
	google_pin_location9();
	google_pin_location10();
});
$('#stage3_3b').click(function(){ 
	codeAddress();
	codeAddress2();
});
$('#stage1_1a').click(function(){ 
	localCommunitymap();
});

$(document).ready(function () {
 // get max inputs
 
	$("form").each(function (index) {
		$form = $(this);
	  $form.find("input.required,select.required,textarea.required").each(function (index) {
		// get max inputs
		var maximum = $form.find("input.required,select.required,textarea.required").length;

		// get number of complete inputs
		var complete = 0;
		$form.find("input.required,select.required,textarea.required").each(function (index) {
			if ($(this).val().length > 0) {
				complete = complete + 1;
			}
		});

		// round the number, getting a percentage
		var current_progress = (Math.round((complete / maximum) * 100) / 1)+'%';
		
		 $form.find(".stage_bar").width(current_progress);
		// update progress bar text
		$form.find(".stage_bar").text(current_progress);
	  });
	}); 
	google_pin_location1();
	google_pin_location2();
	google_pin_location3();
	google_pin_location4();
	google_pin_location5();
	google_pin_location6();
	google_pin_location7();
	google_pin_location8();
	google_pin_location9();
	google_pin_location10();
});
// on change, check completion %
$("form").change(function () {
	$form = $(this);
    // get max inputs
    var maximum = $form.find("input.required,select.required,textarea.required").length;

    // get number of complete inputs
    var complete = 0;
    $form.find("input.required,select.required,textarea.required").each(function (index) {
        if ($(this).val().length > 0) {
            complete = complete + 1;
        }
    });

    // round the number, getting a percentage
    var current_progress = (Math.round((complete / maximum) * 100) / 1)+'%';
	
	 $form.find(".stage_bar").width(current_progress);
    // update progress bar text
    $form.find(".stage_bar").text(current_progress);

});
<!-- Google Analytics -->

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51413481-1', 'auto');
  ga('send', 'pageview');
<!-- Google Analytics -->
</script>
<style>
.grayclassactive a {
    background-color: #eee;
}
body {
  padding : 10px ;
  
}
.wh_formss input, textarea, .uneditable-input {
  width: 310px;
}

#exTab1 .tab-content {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}

#exTab2 h3 {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}


#exTab3 .wh_formss {
  margin: 0 auto;
  width: 70%;
}

#exTab3 .nav.nav-pills
{
	margin-bottom:0px!important;
}
/* remove border radius for the tab */

#exTab1 .nav-pills > li > a {
  border-radius: 0;
}

/* change border radius for the tab , apply corners on top*/

#exTab3 .nav-pills > li > a {
  border-radius: 4px 4px 0 0 ;
}

#exTab3 .tab-content {
  border: 1px solid #E5E6E7;
  padding : 5px 15px;
}
.tab-content.clearfix {
  width: 100%;
}
#exTab3 .nav-pills > li > a
{
	padding:13px!important;
}
.call_us
{
	margin-top:50px;
}
.grayclass a{
	 background-color: #eee;
}
.wh_formss .nav .active a{
	 background-color: #08c!important;
}
select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
  border-radius: 4px;
  color: #555;
  display: inline-block;
  font-size: 14px;
  height: 30px;
  line-height: 20px;
  margin-bottom: 10px;
  padding: 4px 6px;
  vertical-align: middle;
}
.panel-info > .panel-heading {
  text-transform: uppercase;
}
.table input {display: block !important; padding: 0 !important; margin: 0 !important; border: 0 !important; width: 100% !important; border-radius: 0 !important; line-height: 1 !important; text-align: center;}
.table td {margin: 0 !important; padding: 0 !important;}
.add_button img {width: 35px !important;}
.remove_button img {width: 22px !important;}
.disabled{
	pointer-events: none;
    cursor: default;
}



@media screen and (max-width:992px)
{
	.wh_formss input, textarea, .uneditable-input {
  width: auto!important;
  float:left;
	}
	.control-label{margin-right:10px;}
.form-horizontal .controls {

  margin-left: 15px!important;
}

}

@media screen and (max-width:767px)
{
	#exTab3 .wh_formss {
  float: left!important;
  margin: 0 auto;
  
  width: 100%!important;
}

.container 
{
	float:left!important;
	padding:5px!important;
	width:100%!important;
}
.panel , .tab-pane , form , .panel-body , .google_canvas{
  float: left!important;
  width: 100%!important;
}
.tab-content {
  float: left;
  width: 100%!important;
  padding:0px!important;
}
.control-label {
  margin-right: 0;
  float: left!important;
  width: 100%!important;
  text-align: left!important;
  font-size: 13px!important;
}
.required {
  float: left;
  width: 100%!important;
}

.form-horizontal .controls {
  float: left;
  margin-left: 0!important;
  padding-left: 0;
  width: 100%;
}
.mystage {
  width: auto;
}
.wh_formss input, textarea, .uneditable-input {
  float: left;
  width: 100%!important;
}
.panel-heading
{
	font-size:12px;
}
.panel-body {
  border: medium none;
  padding: 5px;
}
.new_table {
  overflow: scroll;
}
.wh_btns
{
	
	float:left;
	width:100%;
	text-align:center;
	margin-bottom:20px;
}
}
</style>
