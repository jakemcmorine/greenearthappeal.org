<h3>List of refid</h3>
<p class="text-success"><?php if (@isset($_SESSION['message'])) echo $_SESSION['message']; unset($_SESSION['message']); ?></p>
<div class="row-fluid"">
<div class="span6">
    <table class="table table-bordered table-striped">
        <tr>
            <th>ID</th>
            <th>Refid Url</th>
            <th>Redirect Url</th>
        </tr>
        <?php
        $num_rows = count($rows);
        if ($num_rows) :

        for ($i = 0;
        $i < ceil($num_rows / 2);
        $i++) :
        $row = $rows[$i];
        ?>
        <tr>
            <td><?php echo $row->id; ?></td>
            <td><?php echo 'site?refid=' . $row->code; ?></td>
            <td>
                <form class="form-horizontal" method="POST"
                      action="<?php echo site_url('user/save_referrer_redirect'); ?>">
                    site/<input name="redirect_url" value="<?php echo @$row->redirect_url; ?>"
                                style="width: 150px;"/>
                    <input type="hidden" name="code" value="<?php echo $row->code; ?>"/>
                    <input type="hidden" name="type" value="refid"/>
                    <?php if (@$row->redirect_id) : ?>
                        <input type="hidden" name="id" value="<?php echo $row->redirect_id; ?>"/>
                    <?php endif; ?>
                    <button type="submit" class="btn btn-small btn-primary">Save</button>
                </form>
            </td>
        </tr>
        <?php endfor;
        endif; ?>
    </table>
</div>
<div class="span6">
    <table class="table table-bordered table-striped">
        <tr>
            <th>ID</th>
            <th>Refid Url</th>
            <th>Redirect Url</th>
        </tr>
        <?php
        if ($num_rows) :
        for ($i = ceil($num_rows/2);
        $i < $num_rows;
        $i++) :
        $row = $rows[$i];
        ?>
                <tr>
                    <td><?php echo $row->id; ?></td>
                    <td><?php echo 'site?refid=' . $row->code; ?></td>
                    <td>
                        <form class="form-horizontal" method="POST"
                              action="<?php echo site_url('user/save_referrer_redirect'); ?>">
                            site/<input name="redirect_url" value="<?php echo @$row->redirect_url; ?>"
                                        style="width: 150px;"/>
                            <input type="hidden" name="code" value="<?php echo $row->code; ?>"/>
                            <input type="hidden" name="type" value="refid"/>
                            <?php if (@$row->redirect_id) : ?>
                                <input type="hidden" name="id" value="<?php echo $row->redirect_id; ?>"/>
                            <?php endif; ?>
                            <button type="submit" class="btn btn-small btn-primary">Save</button>
                        </form>
                    </td>
                </tr>
            <?php endfor; endif; ?>
    </table>
</div>
</div>
<div class="row-fluid">
    <div class="pagination pull-right">
        <?php echo $pagination; ?>
    </div>
</div>

<style>
    .pagination {
        padding-right: 20px;
    }

    .pagination a, .pagination strong {
        padding: 5px 10px;
    }
</style>