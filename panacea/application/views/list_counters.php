<div class="span7 offset4 wk_form">
    <form accept-charset="utf-8" method="POST" class="form-horizontal" action="" enctype="multipart/form-data">
        <fieldset>
            <legend class="text-center">
                <?php echo $title;?>
            </legend>
            <div class="control-group">
				<label class="control-label">CFD Tree Counter:</label>
				<div class="controls">
					<img src="<?php echo base_url() . 'tickers/ticker_cfd_counter.png?'.time(); ?>" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">GEA Tree Counter:</label>
				<div class="controls">
					<img src="<?php echo base_url() . 'tickers/ticker_gea_counter.png?'.time(); ?>" />
				</div>
			</div>
		</fieldset>
	</form>
</div>