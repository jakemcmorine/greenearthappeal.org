<div class="pct_body">
	<fieldset>
		<legend><?php echo $title;?></legend>
		<table class="table table-bordered table-striped table-hover">
			<tr>
				<th>ID</th>
				<th>Email</th>
				<th>Username</th>
				<th>Trees</th>
				<!--th>Month</th-->
				<th>Date Created</th>
			</tr>
			<tr>
				<?php
					if (count($rows)) :
						foreach ($rows AS $row):
							$monthYearInt = $row->month;
							$years = substr($monthYearInt, -4);
							$month_int = str_replace($years,"",$monthYearInt)
				?>
							<td><?php echo $row->id;?></td>
							<td><?php echo $email;?></td>
							<td><?php echo $username;?></td>
							<td><?php echo $row->trees;?></td>
							<!--td><?php //echo $months[$month_int]." ".$years;?></td-->
							<td><?php echo $row->date_added;?></td></tr><tr>
				<?php
						endforeach;
					else :
						echo '<td>No Records Found..</td>';
					endif;
				?>
			</tr>
		</table>
	</fieldset>
</div>