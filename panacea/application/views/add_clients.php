<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="" enctype="multipart/form-data">
        <fieldset>
            <legend class="text-center">
                <?php echo $title;?>
            </legend>
            <div class="controls">
                <?php if (isset($error)) echo '<div class="text-error">'.$error.'</div>';?>
            </div>
			<div class="control-group">
                <label class="control-label">Csv file (*):</label>
                <div class="controls">
                    <input type="file" name="csv_file1" />
                </div>
            </div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-success" type="submit" name="add_clients">Upload</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>