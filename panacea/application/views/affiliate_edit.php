<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="<?php echo site_url('user/edit_affiliate/'.$id)?>">
        <fieldset>
            <legend class="text-center"><?php echo $title;?></legend>
            <div class="controls">
                <?php echo validation_errors(); ?>
            </div>
            <div class="control-group">
                <label class="control-label">First Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $first_name; ?>" name="first_name" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Last Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $last_name; ?>" name="last_name" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Email: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $email; ?>" name="email"  />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Username: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $username; ?>" name="username" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Type: (*)</label>
                <div class="controls">
                    <label class="radio inline">
                        <input type="radio" value="lead" name="type" <?php echo ($type =="lead") ? 'checked' : '';?> /> Per Lead
                    </label>
                    <label class="radio inline">
                        <input type="radio" value="acquisition" name="type" <?php echo ( $type=="acquisition") ? 'checked' : '';?> /> Per Acquisition
                    </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Rate (*):</label>
                <div class="controls">
                    <input type="text" style="width: 50px;" name="rate" value="<?php echo $rate;?>" />
                </div>
            </div>
             <div class="control-group">
                <label class="control-label">Unique code:</label>
                <div class="controls">
                    <input type="text" style="width: 80px;" name="code" value="<?php echo $code;?>" readonly />
                </div>
            </div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-success" type="submit" name="edit_affiliate">Save</button>
                    <a href="<?php echo site_url('user/affiliates_list');?>" class="btn btn-warning">Cancel</a>
				</div>
			</div>
		</fieldset>
	</form>
</div>