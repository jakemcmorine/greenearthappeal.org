<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/js/iEdit.css">
<script type="text/javascript" src="<?php echo base_url(); ?>/js/iEdit.js"></script>
<form accept-charset="utf-8" id="stage3_form" method="post" class="form-horizontal" enctype="multipart/form-data" action="">
	<div class="panel panel-info">
		<div class="panel-heading text-center">Supporting Information</div>
			<div class="panel-body">
			<div class="controls">
				<?php if (isset($error)) echo "<div class='text-error'>$error</div>"; ?>
			</div>
			<h4>Team and Community Section</h4><hr>
				<?php

//echo "<pre>"; print_r($locations); die;
				if(count($persons)>0){				
				$count = 1;
				foreach($persons as $person){ ?>
					<div class="control-group">
							<label class="control-label">Upload a photo of <?php echo $person->first_name.' '.$person->surname; ?>: </label>
							<div class="controls">
								<input type="file" id="files<?php echo $count; ?>" value="" name="person_photos<?php echo $person->id; ?>" />
								<?php 
								 if($person->place_photo->person_photo){ ?>
										<div class='upload_photo'>
											<div class='img-wrap'>
												<span onclick="remove_grant_upload_photo('<?php echo $person->id; ?>','<?php echo $person->place_photo->person_photo; ?>');"class='close'>&times;</span>
												<img id='result' width='140' src="<?php echo base_url() . "uploads/person_photos/".$person->place_photo->person_photo; ?>" />
											</div>
										</div>

								<?php } ?>
								<input type="hidden" value="<?php echo $person->id; ?>" name="person_data[<?php echo $person->id; ?>]" />
								</div>
					</div>
				<?php $count++; } } ?>
						
				<div class="control-group">
						<label class="control-label">Organisation Headquarters Photo : </label>
						<div class="controls">
							<input type="file" value="" name="org_headquarter_photo" />
							<?php if (!empty($org_headquarter_photo)) { ?>
							<div class='headquarter_photo'>
								<div class='img-wrap'>
									<span onclick="remove_grant_headquarters_photo('<?php echo $id; ?>','<?php echo $org_headquarter_photo; ?>','headquarters');"class='close'>&times;</span>
									<img id='result' width='140' src="<?php echo base_url() . "uploads/person_photos/".$org_headquarter_photo; ?>" />
								</div>
							</div>
							<?php } ?>
						</div>
				</div>
				<div class="control-group">
						<label class="control-label">Community Photos: (Select multiple files)</label>
						<div class="controls">
							<input type="file" value="<?php //echo $person->person_photo; ?>" name="community_photos[]" id="community_photos" multiple />
							<?php if (!empty($community_photos)) {
								foreach($community_photos as $photos){  ?>
								<div class="remove_community_photo_<?php echo $photos->id; ?> community_photo">
									<div class='img-wrap'>
										<span onclick="remove_grant_community_photo('<?php echo $photos->id; ?>','<?php echo $photos->community_photo; ?>','community');"class='close'>&times;</span>
										<img id='result' width='140' src="<?php echo base_url() . "uploads/person_photos/".$photos->community_photo; ?>" />
									</div>
								</div>
							<?php	}
							} ?>
						</div>
				</div> 
			   <h4>Location Photo Section</h4><hr>
			   <?php if(count($locations)>0){
				$rr = 0;
				$count = 1;
				foreach($locations as $location){ 
				 $total = count($location->species); ?>
					<div class="control-group">
						<label class="control-label"><?php echo $location->google_pin_loc; ?> where you will be planting <?php
						$cnt = 1;
						foreach($location->species as $specie){ ?>  <?php echo $specie->no_of_trees; if($cnt < $total){ echo " ,"; } $cnt++; } ?> (Select multiple files): </label>
						<div class="controls">
							<input type="file" id="files<?php echo $count; ?>" value="" name="location_photos<?php echo $location->id; ?>[]" multiple />
							<?php foreach($location_photos[$rr] as $photos){ 
								if($location->id==$photos->location_id){  ?>
									<div class="remove_location_photo_<?php echo $photos->id; ?> community_photo">
										<div class='img-wrap'>
											<span onclick="remove_grant_community_photo('<?php echo $photos->id; ?>','<?php echo $photos->location_photo; ?>','location');"class='close'>&times;</span>
											<img id='result' width='140' src="<?php echo base_url() . "uploads/person_photos/".$photos->location_photo; ?>" />
										</div>
									</div>
								<?php }	} ?>
						</div>
						<input type="hidden" value="<?php echo $location->id; ?>" name="location_data[<?php echo $location->id; ?>]" />
					</div>
				
				<?php $count++; $rr++; } } ?>
				<!--div class="control-group">
						<label class="control-label">Where trees will be planted- Photo upload of location trees will be planted : </label>
						<div class="controls">
							<input type="file" value="<?php //echo $person->person_photo; ?>" name="planted_tree_location_photo" />
							<?php if ($planted_tree_location_photo) echo "<img id='result' width='100' src='" . base_url() . "uploads/person_photos/".$planted_tree_location_photo ."' />"; ?>
						</div>
				</div-->
				
				
				 <h4>Video Section</h4><hr>
				 <?php   if(count($locations)>0){ 
				 $count = 1;
				 foreach($locations as $location){  ?>
						<div class="control-group">
							<label class="control-label">Add video link to <?php echo $location->google_pin_loc; ?> : </label>
							<div class="controls">
								<input type="text" value="<?php  if($location->videos){ echo $location->videos->video_link;  }; ?>" name="videos_links<?php echo $location->id; ?>" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Add additional link : </label>
							<div class="controls">
								<input type="text" value="<?php  if($location->videos){ echo $location->videos->additional_link;  }; ?>" name="additional_links<?php echo $location->id; ?>" />
							</div>
						</div>
						<input type="hidden" value="<?php echo $location->id; ?>" name="videos_data[<?php echo $location->id; ?>]" />
				
				 <?php $count++; } } ?>
				<!--div class="control-group">
						<label class="control-label">Locations of picture: </label>
						<div class="controls">
							<input type="text" value="<?php echo $location_of_picture; ?>" name="location_of_picture" />
						</div>
				</div-->
				
					<!--div class="control-group">
						<label class="control-label">Google Map Point Picker: </label>
							<div class="controls">
							<input id="city_country" type="textbox" value="<?php //if(isset($photos_google_point_picker)){ echo $photos_google_point_picker; }else{ echo "Africa"; } ?>">
							<input value="Search" onclick="codeAddress()" style="width: 80px;" type="button">
								<div class="google_canvas" id="mapCanvas"></div>
								  <div id="infoPanel">
									<b>Marker status:</b>
									<div id="markerStatus"><i>Click and drag the marker.</i></div>
									<b>Closest matching address:</b>
									<div id="markeraddress"></div>
								   </div>
							</div>
					</div-->
				<!--div class="control-group">
						<div class="controls">
							<input type="text" id="markerinfo" value="<?php //echo $photos_google_point_picker; ?>" name="photos_google_point_picker" />
						</div>
				</div-->
			
				<!--div class="control-group">
						<label class="control-label">Team - Youtube link to any videos showing the team: </label>
						<div class="controls">
							<input type="text" value="<?php echo $team_youtube_link; ?>" name="team_youtube_link" />
						</div>
				</div>
				<div class="control-group">
						<label class="control-label">Community - Youtube link to any videos showing the community : </label>
						<div class="controls">
							<input type="text" value="<?php echo $community_youtube_link; ?>" name="community_youtube_link" />
						</div>
				</div>
				<div class="control-group">
						<label class="control-label">Where trees will be planted - Youtube link to any videos showing location  : </label>
						<div class="controls">
							<input type="text" value="<?php echo $tree_planted_youtube_link; ?>" name="tree_planted_youtube_link" />
						</div>
				</div>
				<div class="control-group">
						<label class="control-label">Locations of videos: </label>
						<div class="controls">
							<input type="text" value="<?php echo $tree_planted_location; ?>" name="tree_planted_location" />
						</div>
				</div-->
				
				<!--div class="control-group">
						<label class="control-label">Google Map Point Picker: </label>
							<div class="controls">
							<input id="city_country2" type="textbox" value="<?php if(isset($videos_google_point_picker)){ echo $videos_google_point_picker; }else{ echo "Africa"; } ?>">
							<input value="Search" onclick="codeAddress2()" style="width: 80px;" type="button">
								<div class="google_canvas" id="mapCanvas2"></div>
								  <div id="infoPanel">
									<b>Marker status:</b>
									<div id="markerStatus2"><i>Click and drag the marker.</i></div>
									<b>Closest matching address:</b>
									<div id="markeraddress2"></div>
								   </div>
						</div>
				</div-->
				<!--div class="control-group"> 
						<div class="controls">
								<input type="text" id="markerinfo2" value="<?php echo $videos_google_point_picker; ?>" name="videos_google_point_picker" />
						</div>
				</div-->
				<div class="control-group">
						<label class="control-label">Upload additional supporting documentation  : </label>
						<div class="controls">
							<input type="file" value="<?php //echo $person->person_photo; ?>" name="additional_documentation" />
							<?php 
							 if(!empty($additional_documentation)){ 
								$ext = pathinfo($additional_documentation, PATHINFO_EXTENSION); ?>
								<div class="docs"> 
										<div class='img-wrap'><span onclick="remove_grant_headquarters_photo('<?php echo $id; ?>','<?php echo $additional_documentation; ?>','additional');" class='close'>&times;</span>
											<?php if($ext=='PNG' || $ext=='png' || $ext=='jpg' || $ext=='JPG' || $ext=='jpeg'){ ?>
												<img id='additional_documentation' width='140' src="<?php echo base_url() . "uploads/person_photos/".$additional_documentation;?>" />
											<?php }else{ ?>
												<img id='additional_documentation' width='80' src="<?php echo base_url() ."uploads/person_photos/file.png"; ?>" />
											<?php } ?>
										</div>
										<?php if($ext=='doc' || $ext=='docx' || $ext=='pdf'){ ?>
											<h6 style="width: 194px;"><?php echo $additional_documentation; ?></h6>
										<?php } ?>
								</div>
							<?php } ?>
						</div>
				</div>
			 </div>
			</div>
			<input type="hidden" name="edit_stage_3" value="edit_stage_3">
				<?php if ($_SESSION['login']['type'] == 'admin' || $_SESSION['login']['type'] == 'grant_admin') { ?>
					<div id="success_msg3"></div>
					<div class="controls wh_btns">
					<?php if($stage3_status==0 || $stage3_status==2){ ?>
						<button class="btn btn-success" type="submit"  name="edit_stage_3">Save</button>
						<button class="btn btn-success" id="confirmstage3" type="button" onclick="confirm_stage3('<?php echo $user_id; ?>','3')" name="edit_stage_3">Confirm Stage 3</button>
						<button class="btn btn-success" type="button" id="contact_form"  name="contact">Contact</button>
						<?php if($stage3_status==2){ ?>
							<button class="btn btn-success" type="button" onclick="lock_stage1('<?php echo $user_id; ?>','3')" id="lock_button"  name="unlock_button">Lock</button>
						<?php }else{  ?>
							<button class="btn btn-success" type="button" onclick="unlock_stage1('<?php echo $user_id; ?>','3')" id="unlock_button"  name="unlock_button">Unlock</button>
						<?php } ?>
					<?php } else{ ?>
							   <?php if ($_SESSION['login']['type'] == 'grant_admin') { ?>
									<button class="btn btn-success" type="submit"  name="edit_stage_3">Save</button>
							   <?php } ?>
								<button class="btn btn-success" type="button"  name="edit_stage_3">Stage 3 Already confirmed</button>
					<?php } ?>
					</div>
			  <?php }else{ ?>
					<div class="controls wh_btns">
						<?php if($stage3_status==1){ ?>
								<button class="btn btn-success" type="button" disabled="disabled">Complete Application</button>
						<?php }elseif($stage3_status==2){ ?>
								<button class="btn btn-success" type="button" name="edit_stage_3" id="edit_stage_3_submit">Submit for Approval</button>
						<?php	}else{ ?>
								<button class="btn btn-success" type="button" name="edit_stage_3" <?php if(isset($stage3_status)){ echo 'disabled="disabled"'; } ?> id="edit_stage_3">Complete Application</button>
								<button class="btn btn-success" style="display:none" type="submit" name="edit_stage_3" id="edit_stage_3_submit">Complete Application</button>
						<?php }
						?>
					</div>
			<?php } ?>
			<br>
	 </div>
	 
	<!-- Modal -->
				<div class="modal fade" id="step3Modal">
					<div class="modal-dialog modal-sm">
					  <div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <!--<h4 class="modal-title">Modal Header</h4>--->
						</div>
						<div class="modal-body">
							<p>This will submit this information for review, after review this information cannot be changed, are you sure?</p>
						</div>
						<div class="modal-footer">
						  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
						  <button type="submit" onclick="jQuery('#edit_stage_3_submit').trigger('click');jQuery('#step3Modal').modal('hide');" class="btn btn-primary">Yes</button>
						</div>
					  </div>
					</div>
				</div> 
	
</form>
<?php
 if ($_SESSION['login']['type'] == 'grant_user') {
	 if(($stage3_status!='' || $stage3_status=='1') &&  $stage3_status!='2'){ ?>
	<script>
		jQuery(document).ready(function(){
			jQuery("#stage3_form").find("input,textarea,select").attr("disabled", "disabled");
		});
	</script>
 <?php }
 } ?>

<script>
jQuery(document).ready(function(){
		jQuery("#edit_stage_3_submit").on('click',function(e){
			 e.preventDefault();
			jQuery("#step3Modal").modal("show");
		});
	});
	function crop_image(id){
		
		var img = $("#files"+id)[0].files[0];
		
		if(!iEdit.open(img, true, function(res){
			$("#result"+id).attr("src", res);			
			$("#logo-image"+id).val(res);			
		})){
			alert("Whoops! That is not an image!");
		}
	}
	
 $(document).ready(function(){
	$("#community_photos1").change(function(e){
		var img = e.target.files[0];

		if(!iEdit.open(img, true, function(res){
			$("#community-result").attr("src", res);			
			$("#community-photos").val(res);			
		})){
			alert("Whoops! That is not an image!");
		}

	});
	/* codeAddress();
	codeAddress2();
	codeAddress3(); */
}); 

var geocoder;
var map;
var marker;

function codeAddress () {
    geocoder = new google.maps.Geocoder();
   //var address = 'Chandigarh, India';
   var address = document.getElementById('city_country').value;
   //console.log(address);
    if(address=='Africa' || address=='africa'){
	   var zoomout = 2;
   }else{
	   var zoomout= 16;
   }
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById('mapCanvas'), {
    zoom: zoomout,
            streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location,
          draggable: true,
          title: 'My Title'
      });
      updateMarkerPosition(results[0].geometry.location);
      geocodePosition(results[0].geometry.location);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker, 'dragstart', function() {
    updateMarkerAddress('Dragging...');
  });
      
  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerStatus('Dragging...');
    updateMarkerPosition(marker.getPosition());
  });
  
  google.maps.event.addListener(marker, 'dragend', function() {
    updateMarkerStatus('Drag ended');
    geocodePosition(marker.getPosition());
      map.panTo(marker.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition(e.latLng);
    geocodePosition(marker.getPosition());
    marker.setPosition(e.latLng);
  map.panTo(marker.getPosition()); 
  }); 
  
    } else {
     // alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus(str) {
  document.getElementById('markerStatus').innerHTML = str;
}

function updateMarkerPosition(latLng) {
 /*  document.getElementById('markerinfo').innerHTML = [
    latLng.lat(),
    latLng.lng()
  ].join(', '); */
  var latlong= latLng.lat()+', '+latLng.lng();
  //$('#markerinfo').val(latlong);
}

function updateMarkerAddress(str) {
  document.getElementById('markeraddress').innerHTML = str;
  $('#markerinfo').val(str);
}

function codeAddress2()  {
    geocoder = new google.maps.Geocoder();
   //var address = 'Chandigarh, India';
   var address = document.getElementById('city_country2').value;
    if(address=='Africa' || address=='africa'){
	   var zoomout = 2;
   }else{
	   var zoomout= 10;
   }
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById('mapCanvas2'), {
    zoom: zoomout,
            streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location,
          draggable: true,
          title: 'My Title'
      });
      updateMarkerPosition(results[0].geometry.location);
      geocodePosition2(results[0].geometry.location);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker, 'dragstart', function() {
    updateMarkerAddress2('Dragging...');
  });
      
  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerStatus2('Dragging...');
    updateMarkerPosition2(marker.getPosition());
  });
  
  google.maps.event.addListener(marker, 'dragend', function() {
    updateMarkerStatus2('Drag ended');
    geocodePosition2(marker.getPosition());
      map.panTo(marker.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition2(e.latLng);
    geocodePosition2(marker.getPosition());
    marker.setPosition(e.latLng);
  map.panTo(marker.getPosition()); 
  }); 
  
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function geocodePosition2(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress2(responses[0].formatted_address);
    } else {
      updateMarkerAddress2('Cannot determine address at this location.');
    }
  });
}

function updateMarkerStatus2(str) {
  document.getElementById('markerStatus2').innerHTML = str;
}

function updateMarkerPosition2(latLng) {
 /*  document.getElementById('markerinfo').innerHTML = [
    latLng.lat(),
    latLng.lng()
  ].join(', '); */
  var latlong= latLng.lat()+', '+latLng.lng();
 // $('#markerinfo2').val(latlong);
}

function updateMarkerAddress2(str) {
  document.getElementById('markeraddress2').innerHTML = str;
  $('#markerinfo2').val(str);
}
 function confirm_stage3(user_id,stage){
		if(confirm("Do you really want to confirm the Stage 3?")) {
		var user_id = user_id;
 			$.post('<?php echo site_url('user/stage_confirmed/');?>',
			{ 
				 user_id : user_id,
				 stage : stage
			}, 
 			function(data) 
			{
				$("#success_msg3").html('Stage 3 confirmed successfully!');
				$("#confirmstage3").text('Stage 3 confirmed');
			}); 
			}else {
				return false; // cancel the event
			}
		}
	
function remove_grant_headquarters_photo(id,image_name,delete_type){
	
		if(confirm("Do you really want to delete the photo?")) {
 			$.post('<?php echo site_url('user/remove_grant_headquarters_photo/');?>',
			{ 
				 id : id,
				 image_name : image_name,
				 delete_type : delete_type,
			}, 
 			function(data) 
			{
				if(delete_type=="headquarters"){
					$(".headquarter_photo").hide();
				}else{
					$(".docs").hide();
				}
			}); 
			}else {
				return false; // cancel the event
			}
}

function remove_grant_community_photo(id,image_name,delete_type){
	
		if(confirm("Do you really want to delete the photo?")) {
 			$.post('<?php echo site_url('user/remove_grant_community_photo/');?>',
			{ 
				 id : id,
				 image_name : image_name,
				 delete_type : delete_type,
			}, 
 			function(data) 
			{
				if(delete_type=="location"){
					$(".remove_location_photo_"+id).hide();
				}else{
					$(".remove_community_photo_"+id).hide();
				}
					
			}); 
			}else {
				return false; // cancel the event
			}
}
function remove_grant_upload_photo(person_id,image_name){
	
		if(confirm("Do you really want to delete the photo?")) {
 			$.post('<?php echo site_url('user/remove_grant_upload_photo/');?>',
			{ 
				 person_id : person_id,
				 image_name : image_name,
			}, 
 			function(data) 
			{
				$(".upload_photo").hide();
			}); 
			}else {
				return false; // cancel the event
			}
}
	jQuery(document).ready(function(){
		jQuery("#edit_stage_3").on('click',function(e){
			 e.preventDefault();
			jQuery("#step3Modal").modal("show");
			
		});
	});

</script>

<style>
.img-wrap {
    position: relative;
    display: inline-block;
    border: 1px red solid;
    font-size: 0;
	 margin-top: 10px;
}
.img-wrap .close {
    position: absolute;
    top: 2px;
    right: 2px;
    z-index: 100;
    background-color: #FFF;
    padding: 5px 2px 2px;
    color: #000;
    font-weight: bold;
    cursor: pointer;
    opacity: 1;
    text-align: center;
    font-size: 22px;
    line-height: 10px;
    border-radius: 50%;
}
.img-wrap:hover .close {
    opacity: 1;
}
.google_canvas{
    width: 300px;
    height: 300px;
    float: left;
	margin-top: 8px;
  }

  #infoPanel div{
    margin-bottom: 5px;
  }
/*   #result {
    margin-top: 10px;
} */

#success_msg3 {
  color: green;
  font-size: 17px;
  margin-bottom: 10px;
  padding: 4px 6px;
}
.community_photo{
	float: left;
	margin-right: 10px;
}

</style>