<fieldset>
    <legend><?php echo $title;?></legend>
    <div>
    <p>Welcome to the Green Earth Appeal Partners Control Panel.</p>
    <p>To date you have planted <b><?php echo $_SESSION['login']['data']['total_trees'];?> trees</b> as a partner of the Green Earth Appeal.</p>
    <p>You can use this dashboard to upload and edit your clients.</p>
    </div>
</fieldset>