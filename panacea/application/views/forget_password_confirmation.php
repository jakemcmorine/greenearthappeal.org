<div class="container">
    <div class="row-fluid">
        <div class="span3 offset4">
            <p></p>
            <p class="text-warning">A reset password link has been sent to the email address provided.</p>    
        </div>
        
        <div class="clearfix"></div>
    </div>
</div>