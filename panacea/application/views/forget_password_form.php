<div class="container">
    <div class="row-fluid">
        <div class="span3 offset4">
            <?php echo form_open('user/forget_password', array('method'=>'post', 'class'=>'form-inline'));?>
                <fieldset>
                    <legend class="text-center">Forget Password</legend>
                    <?php
                        echo validation_errors();
                        if (isset($error)) echo $error;
                    ?>
                    <!--div class="control-group">
                        <label for="inputUsername" class="control-label">Your username</label>
                        <input type="text" id="username" value="<?php //echo set_value('username'); ?>" name="username" />
                    </div-->
                    <div class="control-group">
                        <label for="inputEmail" class="control-label">Your email address</label>
                        <input type="text" id="identity" value="<?php echo set_value('email'); ?>" name="email" />
                    </div>
                    <div class="control-group">
                        <div class="controls text-center">
                            <input class="btn" type="submit" name="submit" value="Send">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="clearfix"></div>
        <div class="control-group">
            <div class="text-center text-success">If you are interested in becoming a partner of The Green Earth Appeal,
                please <a href="http://www.greenearthappeal.org/contact/contact"><strong>contact us</strong></a></div>
        </div>
    </div>
</div