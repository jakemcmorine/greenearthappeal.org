<div class="wide50col">
<?php foreach($result as $res){ ?>
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="">
        <fieldset>
            <legend class="text-center">Order Trees</legend>
            <div class="controls2">
                <?php echo validation_errors(); ?>
            </div>
			<div class="control-group">
                <label class="control-label2">First Name:</label>
                <div class="controls2">
                    <input type="text" value="<?php echo $res->fname; ?>" name="first_name" />
                </div>
            </div>   
		<div class="control-group">
                <label class="control-label2">Last Name:</label>
                <div class="controls2">
                    <input type="text" value="<?php echo $res->lname; ?>" name="last_name" />
                </div>
            </div>          
            <div class="control-group">
                <label class="control-label2">Email: (*)</label>
                <div class="controls2">
                    <input type="text" value="<?php echo $res->email; ?>" name="email" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label2">Phone:</label>
                <div class="controls2">
                    <input type="text" value="<?php echo $res->phone; ?>" name="phone" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label2">Restaurant Name:</label>
                <div class="controls2">
                    <input type="text" value="<?php echo set_value('restaurant'); ?>" name="restaurant" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label2">Website:</label>
                <div class="controls2">
                    <input type="text" value="<?php echo set_value('website'); ?>" name="website" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label2">Twitter:</label>
                <div class="controls2">
                    <input type="text" value="<?php echo set_value('twitter'); ?>" name="twitter" />
                </div>
            </div>
            <!--div class="control-group">
                <label class="control-label">Facebook:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('facebook'); ?>" name="facebook" />
                </div>
            </div-->
            <!--div class="control-group">
                <label class="control-label">Tree number (*):</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('tree_nums'); ?>" name="tree_nums" />
                </div>
            </div-->
			<div class="control-group">
				<div class="controls2btn">
					<input type="hidden" value="10" name="tree_nums" />
					<input type="hidden" value="<?php echo$res->affiliate_code; ?>" name="code" />
					<button class="btn btn-success" type="submit" name="order_trees_no">Submit</button>
				</div>
			</div>
		</fieldset>
	</form>
<?php } ?>
</div>
