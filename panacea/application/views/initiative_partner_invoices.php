<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<fieldset>
    <legend><?php echo $title;?></legend>
	 <h4 id="my_error"></h4>
	 	<form class="form-inline" action="" method="POST" role="form">
	   <div class="form-group form1 padding-none">
		<div class="input-group">
				<select  class="form-control" id="paid" name="paid">
					<option value="">Select Paid</option>
					<option value="1">YES</option>
					<option value="0">NO</option>
				</select>
	  </div>
	  </div>
	  <div class="form-group form1 padding-none">
	  <button type="submit" class="btn btn-icon btn-primary glyphicon glyphicon-search"></button>
	  </div>
	  </form>
	  	<form class="form-inline" action="" method="POST" role="form">
		  <div class="form-group form1 padding-none">
			<div class="input-group">
					<select class="form-control" id="verify" name="verify">
						<option value="">Select Verified</option>
						<option value="1">YES</option>
						<option value="0">NO</option>
					</select>
			</div>
		  </div>
	  <div class="form-group form1 padding-none">
	  <button type="submit" class="btn btn-icon btn-primary glyphicon glyphicon-search"></button>
	  </div>
	  </form>
    <table id="initiative_partner_invoices" class="table table-striped table-bordered" width="100%" cellspacing="0">
	<thead>
        <tr>
            <th>Invoice Num</th>
            <th>Trees</th>
			<th>Total</th>
            <th>Paid</th>
            <th>Verified</th>
        </tr>
		</thead>
		 <tbody>
        <?php
		    //echo "<pre>"; print_r($rows); die("here");
            if (count($rows)) :
                foreach ($rows AS $row):
					if($utype == 'admin' || $utype == 'intitiative' || $utype == 'initiative_editor') {
						//if($row->confirmed == 0) {
								$currency = '';
								switch ($row->currency) {
									case 1:
										$currency = '$';
										break;
									case 2:
										$currency = '&pound;';
										break;
								}
								$total = $currency . number_format($row->total, 2);
								// paid
								//$paid = $row->state ? '<b class="text-success">Yes</b>' : '<b class="text-error">No</b>';
								$confirm = $row->confirmed ? '<b class="text-success">Yes</b>' : '<b class="text-error">No</b>';
								if($row->state =="1"){
									$paid = '<b class="text-success">Yes</b>';
								}elseif($row->state=="-2" || $row->state=="0"){
									$paid = '<b class="text-error">No</b>';
								}elseif($row->state=="-3"){
									$paid = '<b class="text-warning">Pending</b>';
								}
       						 ?>
                                <tr class="table_data" id="delete_record<?php echo $row->id;?>" >
                                    <td><?php echo $row->id;?></td>
									<td><?php echo $row->trees;?></td>
                                    <td><?php echo $total;?></td>
                                    <td><?php echo $paid; ?></td>
                                    <td><?php echo $confirm;?></td> 
                                </tr>
								
        <?php 			} //	}
			endforeach; endif;					
		?>
		</tbody>
    </table>
</fieldset>
<script>
	
	$(document).ready(function() {
		$('#initiative_partner_invoices').dataTable( {
		 "bFilter" : false,  
		"bLengthChange": false,		 
		  "aoColumnDefs": [
			{ 
			  "bSortable": false, 
			  "aTargets": [ 1,2] // <--  column and turns off sorting
			 } 
			]
		} );
	} );
	
	</script>
	<style>
	form{
		margin:0px!important;
	}
	.form1 
	{
		float:left;
		margin-right:15px;
	}
	</style>