<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/adapters/jquery.js"></script>
<?php $days = array("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"); ?>
<fieldset>
    <form action="" enctype="multipart/form-data" method="post">
        <legend>
            <?php echo $title;?>
            <span class="pull-right"><input type="submit" value="Save all" name="edit_front_emails" class="btn btn-warning" /></span>
        </legend>
		 <div class="controls">
                <?php echo validation_errors(); ?>
          </div>
			<table class="table table-bordered table-striped">
				<?php for($i=0; $i<=6; $i++){
						$j = $i+1;
				?>
				 <tr>
					<td>
						<textarea name="data[<?php echo $j; ?>][name]" style="height: 350px;"><?php echo $rows[$i]['name']; ?></textarea>
					</td>
					<td width="70%">
						<div>
							<label class="clearfix">Off/On:</label>
							<label class="switch">
							  <input type="checkbox" name="data[<?php echo $j; ?>][enable_email]" value="1" <?php if($rows[$i]['enable_email']=='1'){ echo "checked"; } ?>>
							  <span class="slider"></span>
							</label>
							<label class="clearfix">Subject:</label>
							<input type="text" style="width: 95%;" name="data[<?php echo $j; ?>][subject]" value="<?php echo $rows[$i]['subject']; ?>" />
						</div>
						<?php if($j==7){ ?>
						<div>
							<input type="hidden" style="width: 6%;" name="data[<?php echo $j; ?>][time_period]" value="<?php echo $rows[$i]['time_period'];?>" /> 
							<input type="hidden" name="data[<?php echo $j; ?>][days][]"  value="">
						<?php }else{ ?>
							<label class="clearfix">Email Send After: </label>
							<input type="number" style="width: 6%;" name="data[<?php echo $j; ?>][time_period]" value="<?php echo $rows[$i]['time_period'];?>" /> Minutes
						</div>
						<div class="days_section">
							<label class="clearfix">Select Days: </label>
							<div class="day_inner">
							<?php $days_arr = explode(",",$rows[$i]['selected_days']); 
								foreach($days as $day){?>
								<div class="inner_dy">
									<input type="checkbox" name="data[<?php echo $j; ?>][days][]" id="<?php echo $day.$j; ?>" value="<?php echo $day; ?>" <?php if(in_array($day, $days_arr)) {?> checked="checked"<?php } ?>>
									<label for="<?php echo $day.$j; ?>"><?php echo $day; ?></label>
								</div>
							<?php } ?>
							</div>
						</div>
						<?php } ?>
						<div>
							<label>Message:</label>
							<textarea class="message1"  style="width: 95%; height:200px;" name="data[<?php echo $j; ?>][message]"><?php echo $rows[$i]['message']; ?></textarea>
						</div>
					</td>
					<input type="hidden" name="data[<?php echo $j; ?>][id]" value="<?php echo $rows[$i]['id'];?>" />
					<td><?php echo $j; ?></td>
				</tr>
			<?php } ?> 
		</table>
    </form>
</fieldset>
<style>
.days_section .day_inner .inner_dy {
    text-align: center;
    display: flex;
    align-items: center;
	margin-right: 15px;
}

.days_section .day_inner {
    display: flex;
    flex-wrap: wrap;
    -webkit-flex-wrap: wrap;
	align-items: center;
	margin-bottom: 12px;
}

.days_section .day_inner .inner_dy input {
margin-top: 0px;
    margin-right: 5px;

}

.days_section .day_inner .inner_dy label {
    margin-bottom: 0px;
}
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}
.switch input {display:none;}
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<script>
$('.message1').ckeditor({
    height: "300px",
    toolbarStartupExpanded: true,
    width: "100%"
});
</script>