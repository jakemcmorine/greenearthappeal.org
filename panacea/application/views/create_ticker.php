<!DOCTYPE html>
<html>
    <head>
        <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="http://www.greenearthappeal.org/plugins/system/jat3/jat3/base-themes/default/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <!--<link href="<?php echo base_url();?>css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />-->
        <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    	<title>Green Earth Appeal Partner Login</title>
        <style>
            #pct_header, #pct_footer {padding: 10px; background: gray;}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row-fluid">
                <div class="span3 offset4">
             
                </div>
                <div class="clearfix"></div>
                <div class="control-group">
                    <div class="text-center text-success">If you are interested in becoming a partner of The Green Earth Appeal,
                        please <a href="http://www.greenearthappeal.org/contact/contact"><strong>contact us</strong></a></div>
                </div>
            </div>
        </div>
    </body>
</html>