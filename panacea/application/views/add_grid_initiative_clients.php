<style>
.modal {
    position: fixed;
    top: 0;
    right:inherit;
	background-color: #f5f5f5;;
    left:50%;
    z-index: 1050;
    display: none;
	bottom: 1 !important;
     -webkit-overflow-scrolling: touch;
	 width:530px;
	 border:none; box-shadow:none;
	 
}
.modal.fade.in { 
    top: 25%;
}
.modal-footer {
    padding: 10px 21px 0px !important;
}
.modal-header {
    padding: 15px 15px !important;
    border-bottom: none !important; 
}
.modal.fade .modal-dialog{
	width:100%; margin:0;
}

.display_msg {
    font-size: 15px;
    font-weight: 600;
    color: #186c18;
}
.form-horizontal .controls {
    margin-left: 0px;
}
.table input {
    display: block !important;
    padding: 0 !important;
    margin: 0 !important;
    border: 0 !important;
    width: 100% !important;
    border-radius: 0 !important;
    line-height: 1 !important;
    text-align: center;
}
select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
    border-radius: 4px;
    color: #555;
    display: inline-block;
    font-size: 14px;
    height: 30px;
    line-height: 20px;
    margin-bottom: 10px;
    padding: 4px 6px;
    vertical-align: middle;
}
.new_table table th{
text-align:center;
vertical-align:middle !important;
}
#pct_body {width: 70%;margin: 20px auto;}
.new_table table {width: 90%;float: right; margin-bottom:0;}
.new_table table.wk_example {width: 9%;float: left;margin-top: 63px;text-align: right;/*! padding-right: 10px; */}
.new_table table .add_button img {width: 16px;}
.new_table table td {padding:0 !important;}
.wk_upload_btn {width:90%; float:right;}
#pct_body #stage1_form .panel-heading {font-weight:bold; font-size:18px;}
</style>
<form accept-charset="utf-8" id="stage1_form" method="post" class="form-horizontal" enctype="multipart/form-data" action="">
	<div class="panel panel-info">
		<div class="panel-heading text-center"><?php echo $title;?></div>
		<div class="panel-body">
        <fieldset>
            <!--legend class="text-center">
                <?php echo $title;?>
            </legend-->
            <div class="controls">
                <?php if (isset($error)) echo '<div class="text-error">'.$error.'</div>';?>
            </div>
			<div class="control-group">
				<div class="controls">
					<div class="new_table">
					<?php if(empty($client_data)){ ?>
					<table class="wk_example">
					<tr>
						<td><b>Example:</b></td>
					</tr>
					</table>
					<?php } ?>
					<table id="breakdown_table" class="table table-bordered table-condensed">
					  <tbody>
						<tr>
							<th>Company (*)</th>
							<th width="200">Email (*)</th>
							<th>First Name (*)</th>
							<th>Last Name (*)</th>
							<th>Tree Nums (*)</th>
							<th>&nbsp; <a href="javascript:void(0);" class="add_button"  title="Add field"><span><img title="Add more fields" src="<?php echo base_url(); ?>/img/add-icon.png"/></span></a>&nbsp;</th>
						</tr> 
						<?php if(empty($client_data)){ ?>
						<tr id="team-break">
									  <td><input type="text" value="Com1" class="form-control" disabled="disabled"/></td>
									  <td><input type="text" value="email1@gmail.com" class="form-control" disabled="disabled"/></td>
									  <td><input value="fn1" class="form-control" disabled="disabled" type="text"></td>
									  <td><input type="text" value="ln1" class="form-control" disabled="disabled"/></td>
									  <td><input type="text" value="1" class="form-control" disabled="disabled"/></td>
									</tr>
									<input type="hidden" name="upload_client_form" value="upload_client_form">
						<?php }	?>
						<?php if(!empty($client_data)){
							$count=0;
							foreach($client_data as $client){ ?>
								<tr id="team-breakdowns" class="remove">
								  <td>
									<input type="text" class="required" id="breakdown_company_<?php echo $count; ?>" value="<?php echo $client->company; ?>" name="breakdown[<?php echo $count; ?>][company]" class="form-control" required=""/>
								  </td>
								  <td>
									<input type="text" class="required" id="email_<?php echo $count; ?>" value="<?php echo $client->email; ?>"  name="breakdown[<?php echo $count; ?>][email]" class="form-control" required=""/>
								  </td>
								  <td>
									<input type="text" class="required" value="<?php echo $client->first_name; ?>" id="first_name_<?php echo $count; ?>"  name="breakdown[<?php echo $count; ?>][first_name]"  class="form-control" required=""/>
								  </td>
								  <td>
									<input type="text" class="required" value="<?php echo $client->last_name; ?>" name="breakdown[<?php echo $count; ?>][last_name]" id="last_name<?php echo $count; ?>" class="form-control" required=""/>
								  </td>
								    <td>
										<input type="text" id="tree_nums_<?php echo $count; ?>" value="<?php echo $client->tree_nums; ?>"  name="breakdown[<?php echo $count; ?>][tree_nums]" class="form-control"/>
									</td>
									<td>
										<a href="javascript:void(0);" class="remove_button" title="Remove field"><img title="Remove field" src="https://www.greenearthappeal.org/panacea/img/remove-icon.png"/></span></a>
									</td>
								  <input type="hidden" value="<?php echo $client->id; ?>" name="breakdown[<?php echo $count; ?>][id]" class="form-control" />
								</tr>
							
						<?php $count++; } }else{ ?>
								<tr id="team-breakdowns">
								  <td><input type="text" class="required" id="breakdown_company_0" name="breakdown[0][company]"  class="form-control" required=""/></td>
								  <td><input type="email" class="required" id="email_0" name="breakdown[0][email]" class="form-control" required=""/></td>
								  <td><input type="text" class="required" name="breakdown[0][first_name]" onblur="first_name(0)" id="first_name_0" class="form-control" required=""/></td>
								  <td>
									<input type="text" id="last_name0" class="required" name="breakdown[0][last_name]" autocomplete="off"  class="form-control" required=""/>
								  </td>
								  <td>
									<input type="number" id="tree_nums_0"  name="breakdown[0][tree_nums]" class="form-control"/>
								  </td>
								</tr>
						<?php } ?>
					  </tbody>
					</table>
				</div>
				</div>
            </div>
			<div class="control-group wk_upload_btn">
				<div class="controls">
					<button class="btn btn-success" type="submit" name="add_grid_clients">Upload</button>
				</div>
			</div>
		</fieldset>

	</div>
</div>
</form>
<div class="modal fade" id="upload_cleint_success">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form action="https://www.greenearthappeal.org/panacea">
				<div class="modal-header">
				<!--button type="button" class="close" data-dismiss="modal">&times;</button-->
				</div>
				<div class="modal-body">
					<p class="display_msg">Thanks for Uploading - Your records will be processed shortly.</p>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success">Okay</button>
				</div>
			</form>
	  </div>
	</div>
</div>
<script src="<?php echo base_url(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>/js/upload-clients.js"></script>
<?php  if($success){ ?>
<script>
	jQuery(document).ready(function(){
		jQuery("#upload_cleint_success").modal("show");
	});
</script>
<?php  } ?>

