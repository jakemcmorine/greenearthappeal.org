<div class="container">
    <div class="row-fluid">
        <div class="span3 offset4">
            <!--<form accept-charset="utf-8" method="post" class="form-inline" action="<?php echo base_url().'user/login';?>">-->
            <?php echo form_open('user/login', array('method'=>'post', 'class'=>'form-vertical'));?>
                <fieldset>
                    <legend class="text-center">Login to your account</legend>
                    <?php
                        echo validation_errors();
                        if (isset($error)) echo $error;
                    ?>
    				<div class="control-group">
                        <label for="inputEmail" class="control-label">Your Username</label>
                        <input type="text" id="identity" value="<?php echo set_value('username'); ?>" name="username" />
                    </div>
    				<div class="control-group">
                        <label for="inputPassword" class="control-label">Your password</label>
                        <input type="password" id="password" value="<?php echo set_value('password'); ?>" name="password" />
                    </div>
                    <div class="control-group">
                        <div><?php echo $recaptcha_html; ?></div>
                    </div>
    				<!--<div class="control-group">
    					<label class="checkbox">
                            <input type="checkbox" id="remember" value="1" name="remember"> Remember me
    					</label>
    				</div>-->
    				<div class="control-group">
    					<div class="controls text-center">
    						<button class="btn btn-success" type="submit">Login</button>
    					</div>
    				</div>
    				<div class="control-group">
    					<a class="help-block text-center" href="<?php echo site_url('user/forget_password')?>"><small>Forget your password?</small></a>
    				</div>
				</fieldset>
			</form>
        </div>
        <div class="clearfix"></div>
        <div class="control-group">
            <div class="text-center text-success">If you are interested in becoming a partner of The Green Earth Appeal,
                please <a href="http://www.greenearthappeal.org/contact/contact"><strong>contact us</strong></a></div>
        </div>
    </div>
</div>