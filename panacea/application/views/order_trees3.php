<div class="span7 offset2">
<?php foreach($result as $res){ ?>
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="">
        <fieldset>
            <legend class="text-center">Order Trees</legend>
            <div class="controls">
                <?php echo validation_errors(); ?>
            </div>
			<div class="control-group">
                <label class="control-label">First Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $res->fname; ?>" name="first_name" />
                </div>
            </div>   
		<div class="control-group">
                <label class="control-label">Last Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $res->lname; ?>" name="last_name" />
                </div>
            </div>          
            <div class="control-group">
                <label class="control-label">Email: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $res->email; ?>" name="email" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Phone:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $res->phone; ?>" name="phone" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Restaurant Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('restaurant'); ?>" name="restaurant" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Website:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('website'); ?>" name="website" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Twitter:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('twitter'); ?>" name="twitter" />
                </div>
            </div>
            <!--div class="control-group">
                <label class="control-label">Facebook:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('facebook'); ?>" name="facebook" />
                </div>
            </div-->
            <!--div class="control-group">
                <label class="control-label">Tree number (*):</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('tree_nums'); ?>" name="tree_nums" />
                </div>
            </div-->
			<div class="control-group">
				<div class="controls">
					<input type="hidden" value="10" name="tree_nums" />
					<input type="hidden" value="<?php echo$res->affiliate_code; ?>" name="code" />
					<button class="btn btn-success" type="submit" name="order_trees_no">Submit</button>
				</div>
			</div>
		</fieldset>
	</form>
<?php } ?>
</div>
