<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/adapters/jquery.js"></script>
<fieldset>
    <form action="" method="post">
        <legend>
            <?php echo $title;?>
            <span class="pull-right"><input type="submit" value="Save all" name="email_template" class="btn btn-warning" /></span>
        </legend>
        <table class="table table-bordered table-striped">
            <?php
                if (count($rows)) :
                    foreach ($rows AS $row):
            ?>
                <tr>
                    <td>
                        <textarea name="data[<?php echo $row->id;?>][name]" style="height: 250px;"><?php echo $row->name;?></textarea>
                    </td>
                    <td width="70%">
                        <div>
                            <label class="clearfix">Subject:
                            </label>
                            <input type="text" style="width: 95%;" name="data[<?php echo $row->id?>][subject]" value="<?php echo $row->subject;?>" />
                        </div>
						<div>
                            <label class="clearfix">No.of Days:
                            </label>
                            <input type="text" style="width: 5%;" name="data[<?php echo $row->id?>][days]" value="<?php echo $row->days;?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="grant_message" style="width: 95%; height:200px;" name="data[<?php echo $row->id?>][message]"><?php echo $row->message;?></textarea>
                        </div>
                        <input type="hidden" name="data[<?php echo $row->id?>][id]" value="<?php echo $row->id;?>" />
                    </td>
                    <!--td><?php echo $row->id;?></td-->
                    <!--<td><?php echo $row->note;?></td>-->
                </tr>
            <?php endforeach; endif;?>
        </table>
    </form>
</fieldset>


<script>
    $('.btn-sendmail').off('click').on('click', function(e) {
        e && e.preventDefault();
        var template_id = $(this).parentsUntil('td').siblings('input[type=hidden]').val();
        $('#send_mail_modal').find('input[name=template_id]').val(template_id);
    });
</script>
<script>
$('.grant_message').ckeditor({
    height: "300px",
    toolbarStartupExpanded: true,
    width: "100%"
});
</script>