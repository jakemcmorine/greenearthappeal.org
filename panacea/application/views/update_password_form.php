<div class="container">
    <div class="row-fluid">
        <div class="span3 offset4">
            <form method="POST" id="update_pass_form" class="form-inline">
                <fieldset>
                    <legend class="text-center">Update Password</legend>
                    <?php
                        echo validation_errors();
                        if (isset($error)) echo $error;
                    ?>
					 <span id="errors"></span>
                    <div class="control-group">
                        <label for="inputPassword" class="control-label">Your password</label>
                        <input type="password" id="password" name="password" />
                    </div>
					  <div class="control-group">
                        <label for="inputPassword" class="control-label">Verify password</label>
                        <input type="password" id="confirm_password" name="confirm_password" />
                    </div>
                    <div class="control-group">
                        <div class="controls text-center">
                            <input class="btn" type="submit" name="submit" value="Change">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="clearfix"></div>
        <div class="control-group">
            <div class="text-center text-success">If you are interested in becoming a partner of The Green Earth Appeal,
                please <a href="http://www.greenearthappeal.org/contact/contact"><strong>contact us</strong></a></div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script><!-- jQuery Library-->
<script>
$(document).ready(function()
{	
	$('#password').keyup(function()
	{
		$('#errors').html(checkStrength($('#password').val()))
	})	
	 $("#update_pass_form").submit(function(e){
		 var res = checkStrength($('#password').val());
		 if(res=='Strong Password'){
			 $("#update_pass_form").submit();
		 }else{
			  $('#errors').html(res);
		 }
        e.preventDefault();		
    });
		
function checkStrength(password)
	{
		var strength = 0
		
		if (password.length < 8) { 
			$('#errors').removeClass()
			$('#errors').addClass('short')
			return 'Password should be at least 8 characters'; 
		}	
		if (password.length > 7) strength = 1
		
		//If password contains both lower and uppercase characters, increase strength value.
		if (!password.match(/([a-z])|([A-Z])/)) {
			$('#errors').removeClass();
			$('#errors').addClass('weak');
			return 'Enter any letters';	
		}
		//If it has numbers and characters, increase strength value.
		if (!password.match(/([0-9])/)) {
			
			$('#errors').removeClass();
			$('#errors').addClass('good');
			return 'Enter numbers'	;
			
		}
		
		if (!password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
			
			$('#errors').removeClass();
			$('#errors').addClass('good');
			return 'Enter any special character !,%,&,@,#,$,^,*,?,_,~';	
			
		}
		$('#errors').removeClass();
		$('#errors').addClass('strong');
		return 'Strong Password';
	}
});
	</script>
<style>
.short{
	color:#FF0000;
	font-size:larger;
}
.weak{
	color:orange;
	font-size:larger;
}
.good{
	color:#2D98F3;
	font-size:larger;
}
.strong{
	color: limegreen;
	font-size:larger;
}
</style>