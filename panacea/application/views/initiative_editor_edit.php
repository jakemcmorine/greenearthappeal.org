<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="<?php echo site_url('user/edit_initiative_editor/'.$id)?>">
        <fieldset>
            <legend class="text-center"><?php echo $title;?></legend>
            <div class="controls">
                <?php echo validation_errors(); ?>
            </div>
            <div class="control-group">
                <label class="control-label">Email: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $email; ?>" name="email"  />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Username: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $username; ?>" name="username" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password:</label>
                <div class="controls">
                    <input type="password" value="" name="password"  />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password Conf:</label>
                <div class="controls">
                    <input type="password" value="" name="password2" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Active:</label>
                <div class="controls">
                    <label class="radio inline">
                        <input type="radio" value="1" name="active" <?php echo ($active==1) ? 'checked' : '';?> /> Active
                    </label>
                    <label class="radio inline">
                        <input type="radio" value="0" name="active" <?php echo ($active==0) ? 'checked' : '';?> /> Inactive
                    </label>
                </div>
            </div>
            
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-success" type="submit" name="edit_initiative_editor">Save</button>
                    <a href="<?php echo site_url('user/index');?>" class="btn btn-warning">Cancel</a>
				</div>
			</div>
		</fieldset>
	</form>
</div>