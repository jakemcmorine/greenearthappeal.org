<div class="container">
    <div class="row-fluid">
        <div class="span3 offset4">
            <?php echo form_open('user/change_client_password', array('method'=>'post', 'class'=>'form-inline'));?>
                <fieldset>
                    <legend class="text-center">Change Password</legend>
                    <?php

                        echo validation_errors();
                        if (isset($error)) echo $error;
                    ?>
                    <div class="control-group">
					<label class="control-label">Password:</label>
						<div class="controls">
							<input type="password" value="" name="password" id="password"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Password Conf:</label>
						<div class="controls">
							<input type="password" value="" name="password2" />
						</div>
					</div>
                    <div class="control-group">
                        <div class="controls text-center">
                            <input class="btn btn-success" type="submit" name="submit" value="Save">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="clearfix"></div>
    </div>
</div>