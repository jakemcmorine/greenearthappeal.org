<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/adapters/jquery.js"></script>
<fieldset>
    <form action="" method="post">
        <legend>
            <?php echo $title;?>
            <span class="pull-right"><input type="submit" value="Save all" name="email_template" class="btn btn-warning" /></span>
        </legend>
        <table class="table table-bordered table-striped">
            <?php
                if (count($rows)) :
                    foreach ($rows AS $row):
                        /*if ($row->type == 'restaurant') {
                            $url_edit = site_url('user/edit_restaurant/'.$row->id);
                            $url_delete = site_url('user/delete_restaurant/'.$row->id);
                        } elseif ($row->type == 'bulk_partner') {
                            $url_edit = site_url('user/edit_bulk_partner/'.$row->id);
                            $url_delete = site_url('user/delete_bulk_partner/'.$row->id);
                        } else {
                            $url_edit = site_url('user/edit_admin/'.$row->id);
                            $url_delete = site_url('user/delete_admin/'.$row->id);
                        }*/
            ?>
                <tr>
                    <td>
                        <textarea name="data[<?php echo $row->id;?>][name]" style="height: 250px;"><?php echo $row->name;?></textarea>
                    </td>
                    <td width="70%">
                        <div>
                            <label class="clearfix">Subject:
                                <a href="#send_mail_modal" class="btn btn-small btn-success pull-right btn-sendmail"data-toggle="modal">Send Test Email</a>
                            </label>
                            <input type="text" style="width: 95%;" name="data[<?php echo $row->id?>][subject]" value="<?php echo $row->subject;?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="message1" style="width: 95%; height:200px;" name="data[<?php echo $row->id?>][message]"><?php echo $row->message;?></textarea>
                        </div>
                        <input type="hidden" name="data[<?php echo $row->id?>][id]" value="<?php echo $row->id;?>" />
                    </td>
                    <td><?php echo $row->id;?></td>
                    <!--<td><?php echo $row->note;?></td>-->
                </tr>
            <?php endforeach; endif;?>
        </table>
    </form>
</fieldset>

<div class="modal hide fade" id="send_mail_modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Send Test Email</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="POST" action="<?php echo site_url('user/send_test_email'); ?>">
            <!-- <div class="control-group">
                <label class="control-label" for="inputName">Your name (*)</label>
                <div class="controls">
                    <input type="text" id="inputName" placeholder="Your name" name="name" required />
                </div>
            </div> -->
            <div class="control-group">
                <label class="control-label" for="inputEmail">Email (*)</label>
                <div class="controls">
                    <input type="email" id="inputEmail" name="email" placeholder="Email" required>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input type="hidden" name="template_id" />
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $('.btn-sendmail').off('click').on('click', function(e) {
        e && e.preventDefault();
        var template_id = $(this).parentsUntil('td').siblings('input[type=hidden]').val();
        $('#send_mail_modal').find('input[name=template_id]').val(template_id);
    });
</script>
<script>
$('.message1').ckeditor({
    height: "300px",
    toolbarStartupExpanded: true,
    width: "100%"
});
</script>