<!DOCTYPE html>
<html>
    <head>
        <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="https://www.greenearthappeal.org/plugins/system/jat3/jat3/base-themes/default/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <!--<link href="<?php echo base_url();?>css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />-->
        <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    	<title>Green Earth Appeal Partner Login</title>
        <style>
            #pct_header, #pct_footer {padding: 10px; background: gray;}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row-fluid">
                <div class="span3 offset4">
                    <!--<form accept-charset="utf-8" method="post" class="form-inline" action="<?php echo base_url().'user/login';?>">-->
                    <?php echo form_open('user/login', array('method'=>'post', 'class'=>'form-inline'));?>
                        <fieldset>
                            <legend class="text-center">Login to your account</legend>
                            <?php
                                echo validation_errors();
                                if (isset($error)) echo $error;
                            ?>
            				<div class="control-group">
                                <label for="inputEmail" class="control-label">Your email address</label>
                                <input type="text" id="identity" value="<?php echo set_value('email'); ?>" name="email" />
                            </div>
            				<div class="control-group">
                                <label for="inputPassword" class="control-label">Your password</label>
                                <input type="password" id="password" value="<?php echo set_value('password'); ?>" name="password" />
                            </div>
                            
            				<!--<div class="control-group">
            					<label class="checkbox">
                                    <input type="checkbox" id="remember" value="1" name="remember"> Remember me
            					</label>
            				</div>-->
            				<div class="control-group">
            					<div class="controls text-center">
            						<button class="btn" type="submit">Login</button>
            					</div>
            				</div>
                            <!--
            				<div class="control-group">
            					<a class="help-block text-center" href="forgot_password"><small>Forget your password?</small></a>
            				</div>-->
        				</fieldset>
        			</form>
                </div>
                <div class="clearfix"></div>
                <div class="control-group">
                    <div class="text-center text-success">If you are interested in becoming a partner of The Green Earth Appeal,
                        please <a href="https://www.greenearthappeal.org/contact-us/"><strong>contact us</strong></a></div>
                </div>
            </div>
        </div>
    </body>
</html>