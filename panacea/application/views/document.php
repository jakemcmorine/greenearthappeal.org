<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="" enctype="multipart/form-data">
        <fieldset>
            <legend class="text-center"><?php echo $title;?></legend>
            <?php if ($_SESSION['login']['type']=='admin' || $_SESSION['login']['type']=='initiative_editor') :?>
            <div class="controls">
                <?php
                    if (isset($_POST['document']) ) {
                        echo '<div class="text-error">'.validation_errors().'</div>';
                        if (isset($error)) echo '<div class="text-error">'.$error.'</div>';
                    }
                ?>
            </div>
			<div class="control-group">
                <label class="control-label">File 1:</label>
                <div class="controls">
                    <input type="file" name="file1" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">File 2:</label>
                <div class="controls">
                    <input type="file" name="file2" />
                </div>
            </div>
			<div class="control-group">
				<div class="controls">
					<input class="btn btn-success" type="submit" value="Upload" name="document" />
                    <!-- <a class="btn btn-warning btn-medium" href="<?php echo site_url('user/invoices')?>">Cancel</a> -->
				</div>
			</div>
            <hr />
            <?php endif;?>
            
            <?php
                if ( $row->file1 && $row->file1) :
                    $url_download = base_url().'uploads/document/';
            ?>
            <h4>Download attachments</h4>
            <a target="_blank" href="<?php echo $url_download . $row->file1;?>">Download <?php echo $row->file1;?></a><br />
            <a target="_blank" href="<?php echo $url_download . $row->file2;?>">Download <?php echo $row->file2;?></a>
            
            <p>
                <a class="btn btn-info btn-medium" href="<?php echo site_url('user/invoices')?>">Return to Invoices</a>
            </p>
            <?php endif; ?>
		</fieldset>
	</form>
    <!----->
    <?php if ($_SESSION['login']['type'] == 'admin' || $_SESSION['login']['type']=='initiative_editor') :?>
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="">
        <fieldset>
            <legend class="text-center">Fordward to Solicitors</legend>
            
            <div class="controls">
                <?php
                    if ( isset($_POST['forward']) ) {
                        echo '<div class="text-error">'.validation_errors().'</div>';
                        if (isset($error)) echo '<div class="text-error">'.$error.'</div>';
                    }
                ?>
            </div>
			<div class="control-group">
                <label class="control-label">Solicitor Email: (*)</label>
                <div class="controls">
                    <input style="width: 100%;" type="text" name="email" value="<?php echo set_value('email');?>" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Subject: (*)</label>
                <div class="controls">
                    <input style="width: 100%;" type="text" name="subject" value="<?php echo set_value('subject');?>" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Message: (*)</label>
                <div class="controls">
                    <textarea style="width: 100%; height:250px" name="message"><?php echo set_value('message');?></textarea>
                </div>
            </div>
            <div class="control-group">
				<div class="controls">
					<input class="btn btn-success" type="submit" value="Forward" name="forward" />
                    <a class="btn btn-warning btn-medium" href="<?php echo site_url('user/invoices')?>">Cancel</a>
				</div>
			</div>
        </fieldset>
    </form>
    <?php endif;?>
</div>