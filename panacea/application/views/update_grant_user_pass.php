<div class="container">
    <div class="row-fluid">
        <div class="span3 offset4">
            <form method="POST" class="form-inline">
                <fieldset>
                    <legend class="text-center">Update Password</legend>
                    <?php
                        echo validation_errors();
                        if (isset($error)) echo $error;
                    ?>
                    <div class="control-group">
                         <label for="inputPassword" class="control-label">Enter new password</label>
                        <input type="password" id="password" name="password" required=""/>
                    </div>
                    <div class="control-group">
                        <div class="controls text-center">
                            <input class="btn" type="submit" name="submit" value="Save">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="clearfix"></div>
        
    </div>
</div>