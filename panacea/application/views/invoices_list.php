<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<fieldset>
    <legend><?php echo $title;?></legend>
	 <h4 id="my_error"></h4>
	 
	 <form class="form-inline" action="" method="POST" role="form">
		  <div class="form-group form1 padding-none">
			  <div class="input-group">
				<select  class="form-control" id="paid" name="paid">
					<option value="">Select Paid</option>
					<option value="1">YES</option>
					<option value="0">NO</option>
				</select>
			  </div>
		  </div>
		  <div class="form-group form1 padding-none">
			<button type="submit" class="btn btn-icon btn-primary glyphicon glyphicon-search"></button>
		  </div>
	  </form>
	  
	 <form class="form-inline" action="" method="POST" role="form">
		  <div class="form-group form1 padding-none">
			<div class="input-group">
				<select class="form-control" id="verify" name="verify">
					<option value="">Select Verified</option>
					<option value="1">YES</option>
					<option value="0">NO</option>
				</select>
			</div>
		  </div>
		  <div class="form-group form1 padding-none">
			<button type="submit" class="btn btn-icon btn-primary glyphicon glyphicon-search"></button>
		  </div>
	  </form>
	  
    <table id="example" class="table table-striped table-bordered" width="100%" cellspacing="0">
	<button class="btn btn-danger btn pull-right" onclick="delete_action();">Delete All Selected</button>
	<a class="btn btn-warning btn pull-right" style="margin-right:8px;" href="<?php echo site_url('user/view_deleted_invoices/'); ?>">View All Deleted</a>

	<thead>
        <tr>
			<th>Select All/none <input type="checkbox" value="" id="checkAll"></th>
            <th>Invoice Num</th>
            <th>User</th>
            <th>Total</th>
            <th>Trees</th>
            <th>Date Created</th>
            <th>Paid</th>
            <th>Verified</th>
			<?php if($utype == 'solicitor'){ echo '<th>Paid Date</th>'; } ?>
            <th>Type</th>
            <th>Company/Restaurant</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tr>
		</thead>
		 <tbody>
        <?php 
            if (count($rows)) :
                foreach ($rows AS $row):
                    /*if ($row->type == 'restaurant') {
                        $url_edit = site_url('user/edit_restaurant/'.$row->id);
                        $url_delete = site_url('user/delete_restaurant/'.$row->id);
                    } elseif ($row->type == 'bulk_partner') {
                        $url_edit = site_url('user/edit_bulk_partner/'.$row->id);
                        $url_delete = site_url('user/delete_bulk_partner/'.$row->id);
                    } else {
                        $url_edit = site_url('user/edit_admin/'.$row->id);
                        $url_delete = site_url('user/delete_admin/'.$row->id);
                    }*/
					//echo "<pre>";  print_r($row);
					
					if($utype == 'admin' || $utype == 'initiative_editor'  ||($utype == 'solicitor' && $row->state == 1 && $row->confirmed == 0)) {
						//if($row->confirmed == 0) {
								$currency = '';
								switch ($row->currency) {
									case 1:
										$currency = '$';
										break;
									case 2:
										$currency = '&pound;';
										break;
									case 3:
										$currency = '€';
										break;
								}
								$total = $currency . number_format($row->total, 2);
								// paid
								//$paid = $row->state ? '<b class="text-success">Yes</b>' : '<b class="text-error">No</b>';
								if($row->state =="1"){
									$paid = '<b class="text-success">Yes</b>';
								}elseif($row->state=="-2" || $row->state=="0"){
									$paid = '<b class="text-error">No</b>';
								}elseif($row->state=="-3"){
									$paid = '<b class="text-warning">Pending</b>';
								}elseif($row->state=="-4"){
									$paid = '<b class="text-error">Cancelled</b>';
								}
								$confirm = $row->confirmed ? '<b class="text-success">Yes</b>' : '<b class="text-error">No</b>';
       						 ?>
                                <tr class="table_data" id="delete_record<?php echo $row->id;?>" >
								<td><input type="checkbox" value="<?php echo $row->id;?>" name="checkboxlist"  id="check_<?php echo $row->id;?>"></td>
                                    <td><?php echo $row->id;?></td>
                                    <td><?php echo $row->email;?></td>
                                    <td><?php echo $total;?></td>
                                    <td><?php echo $row->trees;?></td>
                                    <td><?php echo $row->date_created;?></td>
                                    <td><?php echo $paid; ?></td>
                                    <td><?php echo $confirm;?></td>
									<?php if($utype == 'solicitor'){ echo '<td>'.$row->date_paid.'</td>'; } ?>
                                    <td><?php echo $row->type;?></td>
                                    <td><?php echo $row->company;?></td>
                                        <?php if ($_SESSION['login']['type']=='admin' || $_SESSION['login']['type']=='initiative_editor') :?>
										<td>
                                        <form method="post" action="<?php echo site_url('user/invoice_action');?>" style="margin:0; padding:0; display:inline;" class="inline">
                                            <!-- // <input type="submit" name="action" value="Delete" class="btn-small btn-warning" /> -->
                                            <input type="hidden" name="id" value="<?php echo $row->id;?>" />
                                            <input type="hidden" name="task" value="delete" />
                                            <button class="btn-small btn-warning btn" type="submit">Delete</button>
                                        </form>
                                            <?php if($row->state == 0) { ?>
                                            <form method="post" action="<?php echo site_url('user/invoice_action');?>" style="margin:0; padding:0; display:inline;" class="inline">
                                                <!-- // <input type="submit" name="action" value="Pay" class="btn-small btn-info" /> -->
                                                <button class="btn-small btn-info btn" type="submit">Pay</button>
                                                <input type="hidden" name="task" value="pay" />
                                                <input type="hidden" name="id" value="<?php echo $row->id;?>" />
                                            </form>
                                            <?php }else{ ?>
											 <form method="post" action="<?php echo site_url('user/invoice_action');?>" style="margin:0; padding:0; display:inline;" class="inline">
                                                <!-- // <input type="submit" name="action" value="Pay" class="btn-small btn-info" /> -->
                                                <button class="btn-small btn-primary btn" type="submit">Refund</button>
                                                <input type="hidden" name="task" value="refund" />
                                                <input type="hidden" name="id" value="<?php echo $row->id;?>" />
                                            </form>
											 <?php } ?>
                                            </td>
                                        <?php endif;?>
                                        <td>
                                        <a href="<?php echo site_url('user/document/'.$row->id); ?>" class="btn btn-small btn-success">Document</a>
                                        <!-- <a href="<?php echo base_url().'uploads/invoices/'.$row->name; ?>" target="_blank" class="btn btn-small btn-info">View</a> -->
                                        <a href="<?php echo site_url('user/view_invoice/'.$row->id); ?>" target="_blank" class="btn btn-small btn-info">View</a>
                                        </td>
                                        
                                        <?php if ($_SESSION['login']['type']=='solicitor') :?>
                                        <td>
                                        <!-- Prepare Letter button -->
                                        <a href="<?php echo site_url('user/prepare_letter/'.$row->id); ?>" target="_blank" class="btn btn-small btn-warning">Prepare Letter</a>
                                        <!-- Confirm buton -->
                                        <form method="post" action="<?php echo site_url('user/confirm_payment');?>" style="margin:0; padding:0; display:inline;" class="inline">
                                            <!-- // <input type="submit" name="action" value="Delete" class="btn-small btn-warning" /> -->
                                            <input type="hidden" name="id" value="<?php echo $row->id;?>" />
                                            <input type="hidden" name="task" value="confirm" />
                                            <button class="btn-small btn-danger btn" type="submit">Confirm</button>
                                        </form>
                                        </td>
                                        <?php endif;?>
                                    
                                </tr>
								
        <?php 			} //	}
			endforeach; endif;					
		?>
		</tbody>
    </table>
</fieldset>
<script>
	function delete_action(){
			var action=$('#action').val();
			var checkValues = $('input[name=checkboxlist]:checked').map(function()
            {
                return $(this).val();
            }).get();
				if(checkValues==""){
					alert('Please check at least one record from list to perform this action.');
					return false;
				}
					if(confirm("Are you sure you want to delete?")) {
					   $.post('<?php echo site_url('user/deleted_invoices');?>',
						{ 
							 id : checkValues
						}, 
						function(data) 
						{  
						 checkValues = checkValues.toString();
							var arr = checkValues.split(','); 
							for(i=0; i < arr.length; i++){
                             $("#delete_record"+arr[i]).remove();								
							}	
							
							$("#my_error").html('All these invoices deleted successfully!');
						}); 
						
					}else{
							return false;
					}
				
			
			
		}
	$(document).ready(function() {
		$("#checkAll").click(function () {    
				 $('input:checkbox').prop('checked', this.checked);    
			 });
			 
			 
		$('#example').dataTable( {
		 "bFilter" : true,  
		//"bLengthChange": true,		 
		 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		  "aoColumnDefs": [
			{ 
			  "bSortable": false, 
			  //"aTargets": [ 1,2,4,7,8,-1 ] // <--  column and turns off sorting
			  "aTargets": [ 0,3,5,8,9,10,11,-1 ] // <--  column and turns off sorting
			 } 
			]
		});
	} );
	
	</script>
	<style>
	#example_length select.input-sm {
	  height: 34px !important;
	}
	#example_length
	{
		margin-top: 20px!important;
	}
	
	form{
		margin:0px!important;
	}
	.form1 
	{
		float:left;
		margin-right:15px;
	}
	</style>