<fieldset>
    <legend>Corporate Partner List</legend>
    <table class="table table-bordered table-striped">
        <tr>
            <th>ID</th>
            <th>Email</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Title</th>
            <th>Sub title</th>
            <th>Address</th>
            <th>Opportunity String</th>
            <th>Referer ID</th>
			
            <th width="205"></th>
        </tr>
        <?php
            if (count($rows)) :
                foreach ($rows AS $row):
					$url_edit = site_url('user/edit_corporate_partner/'.$row->id);
					$url_delete = site_url('user/delete_corporate_partner/'.$row->id);
        ?>
            <tr>
                <td><?php echo $row->id;?></td>
                <td><?php echo $row->email;?></td>
                <td><?php echo $row->first_name;?></td>
                <td><?php echo $row->last_name;?></td>
                <td><?php echo $row->title;?></td>
                <td><?php echo $row->subtitle;?></td>
                <td><?php echo $row->address;?></td>
                <td><?php echo $row->opportunity_str;?></td>
                <td><?php echo $row->code;?></td>
                <td>
                    <a class="btn btn-info btn-small" href="<?php echo $url_edit;?>">Edit</a>
                    <a class="btn btn-warning btn-small" href="<?php echo $url_delete;?>">Delete</a>
                </td>
            </tr>
        <?php endforeach; endif;?>
    </table>
</fieldset>