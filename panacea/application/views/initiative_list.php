<fieldset>
    <legend><?php echo $title; ?></legend>
    <table class="table table-bordered table-striped">
        <tr>
            <th>ID</th>
			<th>Initiative Name</th>
			<th>Local Name</th>
			<th>Certificate Field #1</th>
			<th>Certificate Field #2</th>
			<th>Certificate Field #3</th>
			<th>Certificate Field #4</th>
            <th width="205"></th>
        </tr>
        <?php
            if (count($rows)) :
                foreach ($rows AS $row):
					$url_edit = site_url('user/edit_initiative/'.$row->id);
					$url_delete = site_url('user/delete_initiative/'.$row->id);
        ?>
            <tr>
                <td><?php echo $row->id;?></td>
                <td><?php echo $row->initiative_name;?></td>
                <td><?php echo $row->local_name;?></td>
                <td><?php echo $row->certificate1;?></td>
                <td><?php echo $row->certificate2;?></td>
                <td><?php echo $row->certificate3;?></td>
                <td><?php echo $row->certificate4;?></td>
                <td>
                    <a class="btn btn-info btn-small" href="<?php echo $url_edit;?>">Edit</a>
                    <a class="btn btn-warning btn-small" onclick="return confirm('Are you sure?')" href="<?php echo $url_delete;?>">Delete</a>
                    <button type="button" onclick="copy_initiative('<?php echo $row->id;?>');" class="btn btn-primary btn-small">Copy</button>
                </td>
            </tr>
        <?php endforeach; endif;?>
    </table>
</fieldset>
<script>
 function copy_initiative(initiative_id){
			if(confirm("Do you really want to copy this initiative?")) {
				var initiative_id = initiative_id;
				$.post('<?php echo site_url('user/copy_initiative/');?>',
				{ 
					 initiative_id : initiative_id,
				}, 
				function(data) 
				{
					var res = JSON.parse(data);
					if(res.status=='yes'){
						alert('New initiative created.');
						window.location.reload();
					}
				}); 
			}else {
				return false; // cancel the event
			}
		}
</script>	