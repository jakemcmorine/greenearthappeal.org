<fieldset>
    <legend>Users List</legend>
    <table class="table table-bordered table-striped">
        <tr>
            <th>ID</th>
            <th width="80">Company</th>
            <th>Initiative</th>
            <th>Username</th>
            <th>Contact</th>
            <th>Trees</th>
            <th>Type</th>
            <th>Active</th>
            <th width="165">Action</th>
        </tr>
        <?php
            if (count($rows)) :
                foreach ($rows AS $row):
                    if ($row->type == 'restaurant') {
                        $url_edit = site_url('user/edit_restaurant/'.$row->id);
                        $url_delete = site_url('user/delete_restaurant/'.$row->id);
                    } elseif ($row->type == 'bulk_partner') {
                        $url_edit = site_url('user/edit_bulk_partner/'.$row->id);
                        $url_delete = site_url('user/delete_bulk_partner/'.$row->id);
					} elseif ($row->type == 'solicitor') {
                        $url_edit = site_url('user/edit_solicitor/'.$row->id);
                        $url_delete = site_url('user/delete_user/'.$row->id);
                    } elseif ($row->type == 'editor') {
                        $url_edit = site_url('user/edit_editor/'.$row->id);
                        $url_delete = site_url('user/delete_user/'.$row->id);
                     } elseif ($row->type == 'intitiative') {
                        $url_edit = site_url('user/edit_intitiative_partner/'.$row->id);
                        $url_delete = site_url('user/delete_intitiative_partner/'.$row->id);
                    }  elseif ($row->type == 'initiative_editor') {
                        $url_edit = site_url('user/edit_initiative_editor/'.$row->id);
                        $url_delete = site_url('user/delete_user/'.$row->id);
                    } 
					else {
						
                        //$url_edit = site_url('user/edit_admin/'.$row->id);
                        //$url_delete = site_url('user/delete_admin/'.$row->id);
                        //$url_edit = site_url('user/edit_user/'.$row->id);
                        $url_edit = '';
                        $url_delete = site_url('user/delete_user/'.$row->id);
                    }
					$url_log_as = site_url('user/login_as_user/'.$row->id);
					$url_add_trees = site_url('user/add_restaurant_trees/'.$row->id);
        ?>
            <tr>
                <td><?php echo $row->id;?></td>
                <td><?php echo $row->name;?></td>
                <td><?php 
					if($row->type=="intitiative"){ 
						echo "(".@$row->initiative.") ".@$row->initiative_name;
					}else{
						echo @$row->initiative; 
					} ?>
				</td>
                <td><?php echo $row->email;?></td>
                <td><?php echo @$row->first_name;?> <?php echo @$row->last_name;?></td>
                <?php if($row->type=="intitiative"){  ?>
                <td><?php echo number_format(@$row->tree_nums+ @$row->free_trees + @$row->tree_nums2);?></td>
                <?php }else{ ?>
                <td><?php echo number_format(@$row->tree_nums + @$row->tree_nums2);?></td>
                <?php } ?>
                <td><?php echo $row->type;?></td>
                <td><?php echo $row->active;?></td>
                <td>
                    <?php if ($row->type != 'other') :?>
                    <a class="btn btn-info btn-small" href="<?php echo $url_edit;?>">Edit</a>
                    <a class="btn btn-warning btn-small" onclick="return confirm('Are you sure?')" href="<?php echo $url_delete;?>">Delete</a>
                    <a class="btn btn-danger btn-small" href="<?php echo $url_log_as;?>">Login</a>
                    <!--a class="btn btn-danger btn-success" href="<?php echo $url_add_trees;?>">Add trees</a-->
                    <?php endif;?>
                </td>
            </tr>
        <?php endforeach; endif;?>
    </table>
</fieldset>