<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="">
        <fieldset>
            <legend class="text-center"><?php echo $title;?></legend>
            <div class="controls">
                <?php echo validation_errors(); ?>
            </div>
            <div class="control-group">
                <label class="control-label">Email: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('email'); ?>" name="email" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Username: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('username'); ?>" name="username" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password: (*)</label>
                <div class="controls">
                    <input type="password" value="" name="password" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password Conf: (*)</label>
                <div class="controls">
                    <input type="password" value="" name="password2" />
                </div>
            </div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-success" type="submit">Save</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>