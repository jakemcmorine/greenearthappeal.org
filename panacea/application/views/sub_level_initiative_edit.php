<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" enctype="multipart/form-data" class="form-horizontal" action="<?php echo site_url('user/edit_sub_level_initiative/'.$id)?>">
        <fieldset>
            <legend class="text-center">Edit Sub Level Initiative</legend>
            <div class="controls">
                <?php echo validation_errors(); ?>
            </div>
			<div class="control-group">
                <label class="control-label">Initiative: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $initiative; ?>" name="initiative" />
                </div>
            </div>
			 <div class="control-group">
                <label class="control-label">Select Top Level Initiative: (*)</label>
                <div class="controls">
                    <select name="tli_id">
                        <option value="">--Select--</option>
						<?php if(count($results)) { foreach($results as $result){ 
						$selected = ($result['id'] == $tli_id) ? 'selected="selected"' : ''; ?>
                        <option value="<?php echo $result['id']; ?>"<?php echo $selected; ?>><?php echo $result['opportunity_str']; ?></option>
						<?php } } ?>
                    </select>
                </div>
            </div>
			 <div class="control-group">
                <label class="control-label">Opportunity String: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $opportunity_str; ?>" name="opportunity_str" placeholder="gives diners the opportunity to:" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Sub Level Partner Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $sub_level_partner; ?>" name="partner_name" />
                </div>
            </div>
			 <div class="control-group">
                <label class="control-label">Company Logo (200 x 200 px):</label>
                <div class="controls">
				<?php if ($logo) echo "<img width='200' src='" . base_url() . "uploads/sublevel_initiative/$logo' />"; ?>
                    <input type="file" name="logo" />
                </div>
            </div> 
			 <div class="control-group">
                <label class="control-label">Title: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $title; ?>" name="main_title" />
                </div>
            </div> 
			<div class="control-group">
                <label class="control-label">Sub Title: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $subtitle; ?>" name="sub_title" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Brand:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $brand; ?>" name="brand" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Address:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $address; ?>" name="address" />
                </div>
            </div>
			
            <div class="control-group">
                <label class="control-label">Website:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $website; ?>" name="website" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Referer ID: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $code; ?>" name="code" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">First Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $first_name; ?>" name="first_name" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Last Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $last_name; ?>" name="last_name" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Email: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $email; ?>" name="email" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Phone:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $phone; ?>" name="phone" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Tree number (*):</label>
                <div class="controls">
                    <input type="text" value="<?php echo $tree_nums; ?>" name="tree_nums" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Twitter:</label>
                <div class="controls">
                    <input type="text" value="<?php echo stripslashes($twitter); ?>" name="twitter" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Twitter Hashtag:</label>
                <div class="controls">
                    <input type="text" value="<?php echo stripslashes($twitter_hashtag); ?>" name="twitter_hashtag" />
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-success" type="submit" name="add_toplevel">Save</button>
					 <a href="<?php echo site_url('user/sub_level_initiative_list');?>" class="btn btn-warning">Cancel</a>
                </div>
            </div>
        </fieldset>
    </form>
</div>