<!DOCTYPE html>
<html>
    <head>
        <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="https://www.greenearthappeal.org/plugins/system/jat3/jat3/base-themes/default/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <!--<link href="<?php echo base_url();?>css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />-->
        <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    	<title>Green Earth Appeal</title>
        <style>
            #pct_header, #pct_footer {padding: 10px; background: gray;}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row-fluid">
                <div class="span3 offset4">
                    <!--<form accept-charset="utf-8" method="post" class="form-inline" action="<?php echo base_url().'user/login';?>">-->
                    <?php echo form_open('imacro/autoupload_clients', array('method'=>'post', 'class'=>'form-inline'));?>
                        <fieldset>
                            <legend class="text-center">Add Clients Trees</legend>
                            <?php
							    //$unique_key = "";
                                echo validation_errors();
                                if (isset($error)) echo $error;
                            ?>
								<div class="control-group">
									<h3 style="font-size: 18px;margin-bottom: 0px;">User unique URL</h3>
									<div style="font-size: 16px;" class="user_unique_url">https://carbonfreedining.org/li/<?php echo $unique_key; ?></div>
								</div>
								<div class="control-group">
									<label class="control-label">First Name (*):</label>
									<div class="controls">
										<input type="text" value="<?php echo set_value('first_name'); ?>" name="first_name"  id="first_name"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Last Name (*):</label>
									<div class="controls">
										<input type="text" value="<?php echo set_value('last_name'); ?>" name="last_name" id="last_name"/>
									</div>
								</div>
								 <div class="control-group">
									<label class="control-label">Email Address: (*)</label>
									<div class="controls">
										<input type="text" value="<?php echo set_value('email_address'); ?>" name="email_address"  id="email_address"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Company Name (*):</label>
									<div class="controls">
										<input type="text" value="<?php echo set_value('company'); ?>" name="company" id="company" />
									</div>
								</div>
								 <div class="control-group">
									<label class="control-label">Tree number (*):</label>
									<div class="controls">
										<input type="text" value="<?php echo set_value('tree_nums'); ?>" name="tree_nums" />
									</div>
								</div>
								 <div class="control-group">
									<label class="control-label">Twitter:</label>
									<div class="controls">
										<input type="text" value="<?php echo set_value('twitter1'); ?>" name="twitter1" />
									</div>
								</div>
								 <div class="control-group">
									<label class="control-label">Twitter:</label>
									<div class="controls">
										<input type="text" value="<?php echo set_value('twitter2'); ?>" name="twitter2" />
									</div>
								</div>
								 <div class="control-group">
									<label class="control-label">Twitter:</label>
									<div class="controls">
										<input type="text" value="<?php echo set_value('twitter3'); ?>" name="twitter3" />
									</div>
								</div>
								 <div class="control-group">
									<label class="control-label">Date of Birth:</label>
									<div class="controls">
										<input type="text" value="<?php echo set_value('date_birth'); ?>" name="date_birth" />
									</div>
								</div>
								 <div class="control-group">
									<label class="control-label">Token (*):</label>
									<div class="controls">
										<input type="text" value="<?php echo set_value('token'); ?>" name="token" />
									</div>
								</div>
								 <div class="control-group">
									<label class="control-label">Initiative Partner ID (*):</label>
									<div class="controls">
										<input type="text" value="<?php echo set_value('partner_id'); ?>" name="partner_id" />
									</div>
								</div>
								<input type="hidden" value="<?php echo $unique_key; ?>" name="unique_key" />
									<div class="control-group">
									<div class="controls">
										<button class="btn btn-success" type="submit">Submit</button>
									</div>
								</div>
                            <!--
            				<div class="control-group">
            					<a class="help-block text-center" href="forgot_password"><small>Forget your password?</small></a>
            				</div>-->
        				</fieldset>
        			</form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </body>
</html>