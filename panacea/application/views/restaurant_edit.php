<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="<?php echo site_url('user/edit_restaurant/' . $id) ?>" enctype="multipart/form-data">
        <fieldset>
            <legend class="text-center"><?php echo $title; ?></legend>
            <div class="controls">
                <?php echo validation_errors(); ?>
                <?php if (@$error) echo '<div class="text-error">' . $error . '</div>'; ?>
            </div>
            <div class="control-group">
                <label class="control-label">Initiative: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo  stripslashes($initiative); ?>" name="initiative" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Opportunity String: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo stripslashes($opportunity_str); ?>" name="opportunity_str" placeholder="gives diners the opportunity to:" />
                </div>
            </div>


            <div class="control-group">
                <label class="control-label">Partner Landing Page:</label>
                <div class="controls">
                    <a href="<?php echo 'http://www.greenearthappeal.org/?refid=' . $code; ?>" target="_blank"><?php echo 'http://www.greenearthappeal.org/?refid=' . $code; ?></a>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Counter URL:</label>
                <div class="controls">
                    <?php echo base_url() . 'tickers/ticker_' . $code . '.png'; ?>
                </div>
            </div>

            <?php
            //if($this->data['latest_certificate_snippet'] !='')
            //{
            ?>
            <!--<div class="control-group">
                    <label class="control-label">Syntex for websites :</label>
                    <div class="controls">
                            <textarea name="code_snippet" rows="8" cols="40" disabled="true" style="width: 479px; white-space: normal;">
            <?php echo htmlentities($this->data['latest_certificate_snippet']); ?>
                            </textarea>
                    </div>
            </div>-->
            <?php
            //}
            ?>

            <div class="control-group">
                <label class="control-label"></label>
                <div class="controls">
                    <img src="<?php echo base_url() . 'tickers/ticker_' . $code . '.png?' . time(); ?>" />
                </div>
            </div>
            <!---->
            <!--<div class="control-group">
                <label class="control-label">First Name: (Solicitor)</label>
                <div class="controls">
                    <input type="text" value="<?php //echo $first_name2;  ?>" name="first_name2" />
                </div>
            </div>
                        <div class="control-group">
                <label class="control-label">Email: (Solicitor)</label>
                <div class="controls">
                    <input type="text" value="<?php //echo $email2;  ?>" name="email2" />
                </div>
            </div>
            <hr />-->
            <!----->

            <div class="control-group">
                <label class="control-label">Partner Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $restaurant; ?>" name="restaurant" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Website:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $website; ?>" name="website" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Your Logo (200 x 200 px):</label>
                <div class="controls">
                    <?php if ($logo) echo "<img width='200' src='" . base_url() . "uploads/company/$logo' />"; ?>
                    <input type="file" name="logo" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">First Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $first_name; ?>" name="first_name" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Last Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $last_name; ?>" name="last_name" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Email: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $email; ?>" name="email" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Username: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $username; ?>" name="username" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password:</label>
                <div class="controls">
                    <input type="password" value="" name="password" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password Conf: </label>
                <div class="controls">
                    <input type="password" value="" name="password2" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Phone:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $phone ?>" name="phone" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Address: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $address ?>" name="address" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">City: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $city ?>" name="city" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Area: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $state ?>" name="state" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Post Code: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $post_code ?>" name="post_code" />
                </div>
            </div>
            <?php if ($_SESSION['login']['type'] == 'admin') : ?>
                <div class="control-group">
                    <label class="control-label"></label>
                    <div class="controls">
                        <h4>Only For Admin</h4>
                    </div>
                </div>
				 <div class="control-group">
                    <label class="control-label">Auto input email</label>
                    <div class="controls">
                        <label class="radio inline">
                            <input type="radio" name="auto_input_email" value="1" <?php echo ($auto_input_email == 1) ? 'checked' : ''; ?> /> On
                        </label>
                        <label class="radio inline">
                            <input type="radio" name="auto_input_email" value="0" <?php echo ($auto_input_email == 0) ? 'checked' : ''; ?> /> Off
                        </label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Send Invoice and certificate email:</label>
                    <div class="controls">
                        <label class="radio inline">
                            <input type="radio" name="send_email_immediately" value="1" <?php echo ($send_email_immediately == 1) ? 'checked' : ''; ?> /> Immediately
                        </label>
                        <label class="radio inline">
                            <input type="radio" name="send_email_immediately" value="0" <?php echo ($send_email_immediately == 0) ? 'checked' : ''; ?> /> Next working day
                        </label>
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label">Request for Funds:</label>
                    <div class="controls">
                        <label class="radio inline">
                            <input type="radio" name="request_for_funds" value="1" <?php echo ($request_for_funds == 1) ? 'checked' : ''; ?> /> Yes
                        </label>
                        <label class="radio inline">
                            <input type="radio" name="request_for_funds" value="0" <?php echo ($request_for_funds == 0) ? 'checked' : ''; ?> /> No
                        </label>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Tree number (*):</label>
                    <div class="controls">
                        <input type="text" value="<?php echo $tree_nums; ?>" name="tree_nums" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Is new restaurant: (*)</label>
                    <div class="controls">
                        <label class="radio inline">
                            <input type="radio" value="1" name="new" <?php echo ($new == 1) ? 'checked' : ''; ?> /> New
                        </label>
                        <label class="radio inline">
                            <input type="radio" value="0" name="new" <?php echo ($new == 0) ? 'checked' : ''; ?> /> Exists
                        </label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Active:</label>
                    <div class="controls">
                        <label class="radio inline">
                            <input type="radio" value="1" name="active" <?php echo ($active == 1) ? 'checked' : ''; ?> /> Active
                        </label>
                        <label class="radio inline">
                            <input type="radio" value="0" name="active" <?php echo ($active == 0) ? 'checked' : ''; ?> /> Inactive
                        </label>
                    </div>
                </div>
                <!----->
                <div class="control-group">
                    <label class="control-label">Cost per tree (*):</label>
                    <div class="controls">
                        <input type="text" style="width: 50px;" name="price" value="<?php echo $price; ?>" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"> Currency (*):</label>
                    <div class="controls">
                        <select name="currency">
                            <option value="">--Select--</option>
                            <option value="1" <?php if ($currency == 1) echo "selected='selected'" ?>>cent</option>
                            <option value="2" <?php if ($currency == 2) echo "selected='selected'" ?>>pence</option>

                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Tax (*):</label>
                    <div class="controls">
                        <input type="text" style="width: 50px;" name="tax" value="<?php echo $tax; ?>" /> %
                    </div>
                </div>
                <?php if ($id == 668) { ?>
                    <hr />
                    <p class="controls text-info">Fields for email template is sent to LoopUp</p>
                    <div class="control-group">
                        <label class="control-label">Subject (*):</label>
                        <div class="controls">
                            <input type="text" style="width: 100%;" name="subject" value="<?php echo $subject; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">CC :</label>
                        <div class="controls">
                            <input type="text" style="width: 100%;" name="cc_email" value="<?php echo $cc_email ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Message (*):</label>
                        <div class="controls">
                            <textarea style="width: 100%; height:200px;" name="message" id="message"><?php echo $message; ?></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Text on Certificate (*):<br/>use:<br/>[name]<br/>[total_trees]</label>
                        <div class="controls">
                            <textarea style="width: 100%; height:200px;" name="certificate_text"><?php echo $certificate_text; ?></textarea>
                        </div>
                    </div>
                <?php } ?>
            <?php endif; ?>

            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-success" type="submit" name="edit_restaurant">Save</button>
                    <a href="<?php echo site_url('user/index'); ?>" class="btn btn-warning">Cancel</a>
                </div>
            </div>
        </fieldset>
    </form>
</div>