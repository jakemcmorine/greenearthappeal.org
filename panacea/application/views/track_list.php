<fieldset>
    <legend>Affiliates Tracking</legend>
    <table class="table table-bordered table-striped">
        <tr>
            <th>ID</th>
			<th>Track code</th>
            <th>Email</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Phone</th>
            <th>Dated</th>
            <th width="205"></th>
        </tr>
        <?php
            if (count($rows)) :
                foreach ($rows AS $row):        ?>
            <tr>
                <td><?php echo $row->id;?></td>
                <td><?php echo $row->affiliate_code;?></td>
                <td><?php echo $row->email;?></td>
                <td><?php echo $row->fname;?></td>
                <td><?php echo $row->lname;?></td>
                <td><?php echo $row->phone;?></td>
                <td><?php echo $row->dated;?></td>
            </tr>
        <?php endforeach; endif;?>
    </table>
</fieldset>