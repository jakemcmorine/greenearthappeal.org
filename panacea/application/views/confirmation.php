<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="<?php echo site_url('user/confirm');?>">
        <fieldset>
                <?php
                    switch ( $_SESSION['login']['currency'] ) {
                        case 1:
                            $currency = '$';
                            break;
                        case 2:
                            $currency = '&pound;';
                            break;
                    }
                    $sub_total = $currency . $sub_total;
                    $total_price = $currency . $total_price;
                ?>
            <legend class="text-center"><?php echo $title;?></legend>
            <div class="controls">
                <?php echo ( $total_rows<2 ) ? $total_rows. ' row is inserted' : $total_rows.' rows are inserted'; ?><br />
				<?php echo ( $total_rows_updated<2 ) ? $total_rows_updated. ' row is updated' : $total_rows_updated.' rows are updated'; ?>
            </div>
            <div class="control-group">
                <label class="control-label">Total trees during the partnership:</label>
                <div class="controls">
                    <?php echo $total_trees;?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Current Trees:</label>
                <div class="controls">
                    <?php echo $current_trees;?>
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Sub Total:</label>
                <div class="controls">
                    <?php echo $sub_total;?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Tax:</label>
                <div class="controls">
                    <?php echo $tax;?>%
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Total:</label>
                <div class="controls">
                    <?php echo $total_price;?>
                </div>
            </div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-success" type="submit" name="confirm">Confirm</button>
                    <a class="btn btn-warning" href="<?php echo site_url('user/cancel');?>">Cancel</a>
				</div>
			</div>
		</fieldset>
	</form>
</div>