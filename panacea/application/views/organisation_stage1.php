<form accept-charset="utf-8" id="stage1_form" method="post" class="form-horizontal" enctype="multipart/form-data" action="">
	<div class="panel panel-info">
		<div class="panel-heading text-center">Project Overview</div>
		<div class="panel-body">
		<div class="controls">
			<?php if (isset($error)) echo "<div class='text-error'>$error</div>"; ?>
		<div class="mystage">
			<div class="progress progress-striped active">
				<div class="bar stage_bar" style="width: 0%;"></div>
			</div>
		</div>
		</div><hr>
		<div class="control-group">
			<label class="control-label">Type of Project: (*)</label>
			<div class="controls">
				<select name="type_of_project" class="required" required="">
					<option value="" >--Select--</option>
					<option <?php if($type_of_project=="One off project"){ echo 'selected="selected"'; } ?> value="One off project">One off project</option>
					<option <?php if($type_of_project=="Ongoing project"){ echo 'selected="selected"'; } ?> value="Ongoing project">Ongoing project</option>
				</select>
			</div>
		</div>	
		<div class="control-group">
			<label class="control-label">Proposed Project Start Date: (*)</label>
			<div class="controls">
				<input type="text" value="<?php  echo $project_start_date; ?>"  name="project_start_date" id="project_start_date" class="required" required=""/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Project Aims: (*)</label>
			<div class="controls">
				<textarea name="project_aims" style="height:88px;" placeholder="Please give a description of your project including aims, who will be involved and what the overall benefits will be" class="required" required=""><?php echo $project_aims; ?></textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Local Community: (*)</label>
			<div class="controls">
				<textarea name="local_community" style="height:88px;" placeholder="Please describe what local community support you have for your project or how you plan to get this support" class="required" required=""><?php echo $local_community; ?></textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Organisation Location: </label>
			<div class="controls">
					<input id="city_country_stp1" type="textbox" value="<?php if(isset($location_organisation)){ echo $location_organisation; }else{ echo "Kazakhstan"; } ?>">
					<input value="Search" onclick="localCommunitymap('lantlang')" style="width: 80px;" type="button">
						<div class="google_canvas" id="mapCanvas2_stp1"></div>
					<div id="infoPanel">
						<b>Marker status:</b>
						<div id="markerStatus_stp1"><i>Click and drag the marker.</i></div>
						<b>Address:</b>
						<div id="markeraddress_stp1"></div>
					</div>
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<input type="hidden" id="markerinfo_stp1" value="<?php echo $photos_google_point_picker; ?>" name="location_organisation" />
				<input type="text" name="lan_lat" value="<?php if(isset($long_latitute)){ echo $long_latitute; }else{ echo '48.019573, 66.92368399999998'; }  ?>" id="lat_lan">
			</div>
		</div>
		
		 <div class="control-group">
			<label class="control-label">How many people are in the management team of this project?: (*)</label>
			<div class="controls">
				<select name="team" id="team" class="required" data-count="<?php echo $team ; ?>" required="">
					<option value="" >--Select--</option>
					<?php for($i=1;$i<=10;$i++){ ?>
						<option <?php if($team==$i){ echo 'selected="selected"'; } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>	
		
		<div id="persons_data"><?php if(!empty($persons)){
			$count=1;
			foreach($persons as $person){ ?>
			<div id="team-person" class="col-sm-12 team-person-<?php echo $count ; ?>">
				<div class="panel panel-default">
					<div class="panel-heading">Person <?php echo $count; ?></div>
					<div class="panel-body">
						<div class="control-group">
							<label class="control-label">First Name: (*)</label>
							<div class="controls">
								<input type="text" class="required" value="<?php echo $person->first_name; ?>" name="person[<?php echo $count; ?>][first_name]" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Surname: (*)</label>
							<div class="controls">
								<input type="text" class="required" value="<?php echo $person->surname; ?>" name="person[<?php echo $count; ?>][surname]" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Qualifications: </label>
							<div class="controls">
								<input type="text" value="<?php echo $person->qualifications; ?>" name="person[<?php echo $count; ?>][qualifications]" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Responsibilities in the project: (*)</label>
							<div class="controls">
								<!--input type="text" class="required" value="<?php echo $person->project_responsbilites; ?>" name="person[<?php echo $count; ?>][project_responsbilites]" /-->
								
								<textarea name="person[<?php echo $count; ?>][project_responsbilites]" style="height:88px;" class="required" required=""><?php echo $person->project_responsbilites; ?></textarea>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Experience: </label>
							<div class="controls">
								<!--input type="text" value="<?php echo $person->experience; ?>" name="person[<?php echo $count; ?>][experience]" /-->
								<textarea name="person[<?php echo $count; ?>][experience]" style="height:88px;"><?php echo $person->experience; ?></textarea>
							</div>
						</div>
						<!--<div class="control-group">
							<label class="control-label">Photo (upload): </label>
							<div class="controls">
							<?php //if(isset($person->person_photo)){  ?>
								<img src="<?php  //echo base_url() . "uploads/organisation_stage1/{$person->person_photo}" ; ?>"  height="100" width="100">
							<?php //} ?>
								<input type="file" value="<?php //echo $person->person_photo; ?>" name="photo<?php //echo $count; ?>" />
							</div>
						</div>-->
						<input type="hidden" value="<?php echo $person->id; ?>" name="person[<?php echo $count; ?>][id]" />
					</div>
				  </div>
			</div>
			
		<?php $count++; } } ?></div>
		<h4>Project Funding:</h4><hr>
		 <div class="control-group">
				<label class="control-label">Please provide a full breakdown of the costs associated with the project:</label>
				<div class="controls">
					 <div class="new_table">
						 <table id="breakdown_table" class="table table-bordered table-condensed">
						  <tbody>
							<tr>
							  <th>Description (*)</th>
							  <th>Units (eg. KG,Days,Seeds,metres)(*)</th>
							  <th>Cost Per Unit (*)</th>
							  <th>Number of Units (*)</th>
							  <th>Total ($)</th>
							  <th>&nbsp; <a href="javascript:void(0);" class="add_button"  title="Add field"><span><img title="Add more fields" src="<?php echo base_url(); ?>/img/add-icon.png"/></span></a>&nbsp;</th>
							</tr> 
							<?php if(empty($breakdowns)){ ?>
							<b>Example:</b><tr id="team-break">
										  <td><input type="text" value="Soil" class="form-control" disabled="disabled"/></td>
										  <td><input type="text" value="KG" class="form-control" disabled="disabled"/></td>
										  <td><div class="input-group"><span class="input-group-addon">$</span><input value="3" class="form-control" disabled="disabled" type="text"></div></td>
										  <td><input type="text" value="100" class="form-control" disabled="disabled"/></td>
										  <td><input type="text" value="300" class="form-control" disabled="disabled"/></td>
										</tr>
							<?php }	?>
							<?php if(!empty($breakdowns)){
								$count=0;
								foreach($breakdowns as $breakdown){ ?>
									<tr id="team-breakdowns">
									  <td>
										<input type="text" class="required" id="breakdown_description_<?php echo $count; ?>" value="<?php echo $breakdown->description; ?>" name="breakdown[<?php echo $count; ?>][description]" class="form-control" onclick="add_new_brekdown_row('<?php echo $count; ?>')" required=""/>
									  </td>
									  <td>
										<input type="text" class="required" id="total_units_<?php echo $count; ?>" value="<?php echo $breakdown->units; ?>"  name="breakdown[<?php echo $count; ?>][units]" class="form-control" required=""/>
									  </td>
									  <td>
										<div class="input-group"><span class="input-group-addon">$</span><input type="text" class="required" value="<?php echo $breakdown->cost_per_unit; ?>" id="cost_per_unit_<?php echo $count; ?>"  name="breakdown[<?php echo $count; ?>][cost_per_unit]" onblur="check_totals_project_units('<?php echo $count; ?>')" class="form-control" required=""/>
									  </div>
									  </td>
									  <td>
										<input type="text" class="required" value="<?php echo $breakdown->no_of_units; ?>" onkeyup="check_totals_project_cost('<?php echo $count; ?>')" name="breakdown[<?php echo $count; ?>][no_of_units]" id="no_of_units_<?php echo $count; ?>" class="form-control" required=""/>
									  </td>
										<td>
											<input type="text" id="total_amount_<?php echo $count; ?>" value="<?php echo $breakdown->totals; ?>"  name="breakdown[<?php echo $count; ?>][totals]" class="form-control total_cost" readonly/>
										</td>
									  <input type="hidden" value="<?php echo $breakdown->id; ?>" name="breakdown[<?php echo $count; ?>][id]" class="form-control" />
									</tr>
								
							<?php $count++; } }else{ ?>
									<tr id="team-breakdowns">
									  <td><input type="text" class="required" id="breakdown_description_0" name="breakdown[0][description]" onclick="add_new_brekdown_row(0)" class="form-control" required=""/></td>
									  <td><input type="text" class="required" id="total_units_0" name="breakdown[0][units]" class="form-control" required=""/></td>
									  <td><div class="input-group"><span class="input-group-addon">$</span><input type="text" class="required" name="breakdown[0][cost_per_unit]" onblur="check_totals_project_units(0)" id="cost_per_unit_0" class="form-control" required=""/></div></td>
									  <td>
										<input type="text" id="no_of_units_0" class="required" name="breakdown[0][no_of_units]" autocomplete="off" onkeyup="check_totals_project_cost(0)" class="form-control" required=""/>
									  </td>
									  <td>
										<input type="text" id="total_amount_0"  name="breakdown[0][totals]" class="form-control total_cost" readonly/>
									  </td>
									</tr>
							<?php } ?>
						  </tbody>
						</table>
					</div>
				</div>
			</div>	
			<div class="control-group">
				<label class="control-label">Total cost for project: </label>
				<div class="controls">
				<div class="input-group total-cost-group"><span class="input-group-addon">$</span>
					<input type="number" value="<?php echo $total_cost; ?>" name="total_cost" id="total_cost" readonly/>
					</div>
				</div>
			</div>
			<div class="control-group">
			<label class="control-label">Are other organisations providing funding?: (*)</label>
				<div class="controls">
					<select name="org_provide_fund" class="required" id="org_provide_fund" required="">
						<option  value="" >--Select--</option>
						<option <?php if($org_provide_fund=="yes"){ echo 'selected="selected"'; } ?> value="yes">Yes</option>
						<option <?php if($org_provide_fund=="no"){ echo 'selected="selected"'; } ?> value="no">No</option>
					</select>
				</div>
			</div>	
			
			<div id="privide_funding_data">
			<?php if(!empty($organisation_funding)){ ?>
					<div  class="control-group">
					<label class="control-label" >How many organisations will be co funding this project?: (*)</label>
					<div class="controls">
						<select name="organisation_funding" class="required" required="" id="organisation_funding" data-org="<?php echo $organisation_funding ; ?>">
							<option value="" >--Select--</option>
							<?php for($i=1;$i<=10;$i++){ ?>
								<option <?php if($organisation_funding==$i){ echo 'selected="selected"'; } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php } ?>
						</select>
						</div>
					</div>	
			<?php } ?>
			</div>
			<div id="organisation_funding_data">
			<?php if(!empty($organisations)){
			$count=1;
			foreach($organisations as $organisation){ ?>
					<div id="team-organisation" class="col-sm-12 team-organisation-<?php echo $count ; ?>">
					<div class="panel panel-default">
						<div class="panel-heading">Organisation <?php echo $count; ?></div>
						<div class="panel-body">
							<div class="control-group">
								<label class="control-label">Name: (*)</label>
								<div class="controls">
									<input type="text" class="required" value="<?php echo $organisation->org_name; ?>" name="organisation[<?php echo $count; ?>][org_name]" />
								</div>
							</div>
							<!--div class="control-group">
								<label class="control-label">Funding provided : (*)</label>
								<div class="controls">
									<input type="text" class="required" value="<?php //echo $organisation->funding_provide; ?>" name="organisation[<?php //echo $count; ?>][funding_provide]" />
								</div>
							</div-->
							<div class="control-group">
								<label class="control-label">How much funding is this organisation providing? : (*)</label>
								<div class="controls">
								<div class="input-group total-cost-group"><span class="input-group-addon">$</span>
									<input type="number" class="required sum_of_org_funding" onblur="total_project_cost()" value="<?php echo $organisation->org_funding_provide; ?>" name="organisation[<?php echo $count; ?>][org_funding_provide]" />
									</div>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Website: </label>
								<div class="controls">
									<input type="text" value="<?php echo $organisation->org_website; ?>" name="organisation[<?php echo $count; ?>][org_website]" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Contact Person First Name: (*)</label>
								<div class="controls">
									<input type="text" class="required" value="<?php echo $organisation->person_first_name; ?>" name="organisation[<?php echo $count; ?>][person_first_name]" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Contact Person Surname: (*)</label>
								<div class="controls">
									<input type="text" class="required" value="<?php echo $organisation->person_surname; ?>" name="organisation[<?php echo $count; ?>][person_surname]" />
								</div>
							</div>
							<div class="control-group">
									<label class="control-label">Email Address: (*)</label>
								<div class="controls">
									<input type="text" class="required" value="<?php echo $organisation->person_email_Address; ?>" name="organisation[<?php echo $count; ?>][person_email_Address]" />
								</div>
							</div>
							<input type="hidden" value="<?php echo $organisation->id; ?>" name="organisation[<?php echo $count; ?>][id]" />
						</div>
					  </div>
				</div>
			
			<?php $count++; } } ?></div>
			<div class="control-group">
				<label class="control-label">Funding from other partners: </label>
				<div class="controls">
				<div class="input-group total-cost-group"><span class="input-group-addon">$</span>
					<input type="text" value="<?php echo $other_partner_funding; ?>" name="other_partner_funding" id="other_partner_funding" readonly/>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Funding required from Green Earth Appeal: </label>
				<div class="controls">
				<div class="input-group total-cost-group"><span class="input-group-addon">$</span>
					<input type="text" value="<?php echo $gea_funding; ?>" name="gea_funding" id="gea_funding" readonly/>
					</div>
				</div>
			</div>
			
			<input type="hidden" name="edit_stage_1" value="edit_stage_1">
			<?php if ($_SESSION['login']['type'] == 'admin' || $_SESSION['login']['type'] == 'grant_admin') { ?>
				<div id="success_msg"></div>
				<div class="controls wh_btns">
				<?php if($status==0 || $status==2){ ?>
					<button class="btn btn-success" type="submit"  name="edit_stage_1">Save</button>
					<button class="btn btn-success" id="confirmstage1" type="button" onclick="confirm_stage('<?php echo $user_id; ?>','1')" name="edit_stage_1">Confirm Stage 1</button>
					<button class="btn btn-success" type="button" id="contact_form"  name="contact">Contact</button>
					<?php if($status==2){ ?>
						<button class="btn btn-success" type="button" onclick="lock_stage1('<?php echo $user_id; ?>','1')" id="lock_button"  name="unlock_button">Lock</button>
					<?php }else{  ?>
						<button class="btn btn-success" type="button" onclick="unlock_stage1('<?php echo $user_id; ?>','1')" id="unlock_button"  name="unlock_button">Unlock</button>
					<?php } ?>
				<?php } else{ ?>
				<?php if ($_SESSION['login']['type'] == 'grant_admin') { ?>
					<button class="btn btn-success" type="submit"  name="edit_stage_1">Save</button>
				<?php } ?>
					<button class="btn btn-success" type="button"  name="edit_stage_1">Stage 1 Already confirmed</button>
				<?php } ?>
				</div>
			<?php }else{ ?>
				<div class="controls wh_btns">
					<?php if(isset($status)) { 
					if($status==1){ ?>
						<button class="btn btn-success" type="button" disabled="disabled">Save</button>
						<!--button class="btn btn-success" onclick="show_submit_modal();" type="button">Submitted</button-->
					<?php }elseif($status==2){ ?>
						<button class="btn btn-success" type="button" name="edit_stage_1" id="edit_stage_1">Submit for Approval</button>
						<!--button class="btn btn-success" type="button" name="edit_stage_1" onclick="submit_for_approval1('<?php echo $user_id; ?>','1')">Submit for Approval</button-->
					<?php }else{ ?>
						<button class="btn btn-success" type="button" <?php if(isset($status)){ echo 'disabled="disabled"'; } ?> name="edit_stage_1" id="edit_stage_1">Save</button>
					<?php }	?>
				</div>
			<?php 
			}else{ ?>
				<button class="btn btn-success" type="button" <?php if(isset($status)){ echo 'disabled="disabled"'; } ?> name="edit_stage_1" id="edit_stage_1">Save</button>
				<button class="btn btn-success" style="display:none" type="submit" name="edit_stage_1" id="edit_stage_1_submit">Save</button>
				</div>
			<?php } } ?>

	</div>
	</div>
	<!-- Modal -->
		<div class="modal fade" id="ttModal">
			<div class="modal-dialog modal-sm">
			  <div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <!--<h4 class="modal-title">Modal Header</h4>--->
				</div>
				<div class="modal-body">
					<p>This will submit this information for review, after review this information cannot be changed, are you sure?</p>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				  <button type="submit" onclick="jQuery('#edit_stage_1_submit').trigger('click');jQuery('#ttModal').modal('hide');" class="btn btn-primary">Yes</button>
				</div>
			  </div>
			</div>
		</div>

</form>

<!-- Modal -->
			<div class="modal fade" id="ttModalsuccess">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
				  <form action="">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <!--<h4 class="modal-title">Modal Header</h4>--->
					</div>
					<div class="modal-body">
						<p>Thank you for your submission � this has been sent for a review. Please wait for further instructions via your designated email address.</p>
					</div>
					<div class="modal-footer">
					  <button type="button" onclick="hide_ttModalsuccess()" class="btn btn-primary">OK</button>
					</div>
					</form>
				  </div>
				</div>
			</div>
			<div class="modal fade" id="show_submit_modal">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
				  <form action="">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <!--<h4 class="modal-title">Modal Header</h4>--->
					</div>
					<div class="modal-body">
						<p>Once submitted you can't change anything.</p>
					</div>
					<div class="modal-footer">
					  <button type="button" data-dismiss="modal" class="btn btn-primary">OK</button>
					</div>
					</form>
				  </div>
				</div>
			</div>
			
			<div class="modal fade" id="google_review_model">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
				  <form action="https://bit.ly/GEA-FBReview">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<p>Will you provide a review of our organisation?</p>
					</div>
					<div class="modal-footer">
					  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
					  <button type="submit" class="btn btn-primary">Yes</button>
					</div>
					</form>
				  </div>
				</div>
			</div>

<?php  if($success){ ?>
<script>
	jQuery(document).ready(function(){
		jQuery("#ttModalsuccess").modal("show");
	});
	
</script>
<?php } ?>

<?php 
 if ($_SESSION['login']['type'] == 'grant_user') {
		if(($status!='' || $status=='1') && $status!='2'){ ?>
		<script>
			 jQuery(document).ready(function(){
				jQuery("#stage1_form").find("input,textarea,select").attr("disabled", "disabled");
			});    
		</script>
 <?php } } ?>	
	
<script src="<?php echo base_url(); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript">
function show_submit_modal(){
	jQuery("#show_submit_modal").modal("show");
}
 function hide_ttModalsuccess(){
	 jQuery("#ttModalsuccess").modal("hide");
	 jQuery("#google_review_model").modal("show");
 }
  function add_new_brekdown_row(id){
	  var id_val = parseInt(id)+1;
	  //alert(id_val);
	 // alert(jQuery("#breakdown_description_"+id_val).val());
	  if(typeof jQuery("#breakdown_description_"+id_val).val() == 'undefined'){
		   jQuery(".add_button").trigger( "click" );
		   jQuery("#breakdown_description_"+id).prop('required',true);
		   jQuery("#total_units_"+id).prop('required',true);
		   jQuery("#cost_per_unit_"+id).prop('required',true);
		   jQuery("#no_of_units_"+id).prop('required',true);
	  }	
 }
 
  function check_totals_project_units(id){
	   var cost_per_unit = jQuery("#cost_per_unit_"+id).val();
	   var no_of_units = jQuery("#no_of_units_"+id).val();
	   if(no_of_units==""){
		   no_of_units = 1;
	   }
	   if(isNaN(cost_per_unit)==true){
		  alert('Please enter any number.');
		  return false;
	  }
	  if(isNaN(no_of_units)==true){
		  alert('Please enter any number.');
		  return false;
	  }
	  
	  var total_cost =  (cost_per_unit * no_of_units);
	  jQuery("#total_amount_"+id).val(total_cost);
	 /*  var id_val = parseInt(id)+1;
	  if(typeof jQuery("#total_amount_"+id_val).val() == 'undefined'){
	  if(total_cost>0 ){
		   jQuery(".add_button").trigger( "click" );
		}
	  } */
	  var total_records = $("tr[id='team-breakdowns']").length;
	  //alert(total_records);
	  total_project_cost(total_records);
  }
 function check_totals_project_cost(id){
	 
	  var cost_per_unit = jQuery("#cost_per_unit_"+id).val();
	  var no_of_units = jQuery("#no_of_units_"+id).val();
	  if(isNaN(cost_per_unit)==true){
		  alert('Please enter any number.');
		  return false;
	  }
	  if(isNaN(no_of_units)==true){
		  alert('Please enter any number.');
		  return false;
	  }
	  
	  var total_cost =  (cost_per_unit * no_of_units);
	  jQuery("#total_amount_"+id).val(total_cost);
	  var id_val = parseInt(id)+1;
	  if(typeof jQuery("#total_amount_"+id_val).val() == 'undefined'){
	  if(total_cost>0 ){
		   jQuery(".add_button").trigger( "click" );
		}
	  }	
	  var total_records = $("tr[id='team-breakdowns']").length;
	  total_project_cost(total_records);
 }
 
 
	
function total_project_cost(){
	var  total = 0;
	var  org_sum = 0;
	jQuery( ".total_cost" ).each(function( index ) {
		if(jQuery( this ).val()!=''){
		total += parseInt(jQuery( this ).val());
		}
	});
	jQuery( "#total_cost" ).val(total);
	jQuery( ".sum_of_org_funding" ).each(function( index ) {
		if(jQuery( this ).val()!=''){
			org_sum += parseInt(jQuery( this ).val());
		}
	});
	jQuery( "#other_partner_funding" ).val(org_sum);
	jQuery( "#gea_funding" ).val(total-org_sum); 
 }

 function confirm_stage(user_id,stage){
		if(confirm("Do you really want to confirm the Stage 1?")) {
		var user_id = user_id;
 			$.post('<?php echo site_url('user/stage_confirmed/');?>',
			{ 
				user_id : user_id,
				stage : stage
			}, 
 			function(data) 
			{
				$("#success_msg").html('Stage 1 confirmed successfully!');
				$("#confirmstage1").text('Stage 1 confirmed');
			}); 
			}else {
				return false; // cancel the event
			}
		}
	function submit_for_approval1(user_id,stage){
		if(confirm("Do you really want to submit for approval project overview?")) {
		var user_id = user_id;
 			$.post('<?php echo site_url('user/submit_for_approval/');?>',
			{ 
				user_id : user_id,
				stage : stage
			}, 
 			function(data) 
			{
				$("#success_msg").html('Submit for approval project overview sent successfully!');
				jQuery("#ttModalsuccess").modal("show");
			}); 
			}else {
				return false; // cancel the event
			}
		}
	function unlock_stage1(user_id,stage){
	    if(confirm("Do you really want to unlock this stage for resubmission?")) {
		var user_id = user_id;
 			$.post('<?php echo site_url('user/unlock_stages_for_resubmission/');?>',
			{ 
				user_id : user_id,
				stage : stage
			}, 
 			function(data) 
			{	
				if(stage==1){
					$("#success_msg").html('This stage is ready for grant user to resubmit data.');
				}else if(stage==2){
					$("#success_msg2").html('This stage is ready for grant user to resubmit data.');
				}else if(stage==3){
					$("#success_msg3").html('This stage is ready for grant user to resubmit data.');
				}
			}); 
			}else {
				return false; // cancel the event
			}
		}
	function lock_stage1(user_id,stage){
	    if(confirm("Do you really want to lock this stage?")) {
		var user_id = user_id;
 			$.post('<?php echo site_url('user/lock_stages_for_resubmission/');?>',
			{ 
				user_id : user_id,
				stage : stage
			}, 
 			function(data) 
			{
				
				if(stage==1){
					$("#success_msg").html('Stage has been locked successfully.');
				}else if(stage==2){
					$("#success_msg2").html('Stage has been locked successfully.');
				}else if(stage==3){
					$("#success_msg3").html('Stage has been locked successfully.');
				}
				
			}); 
			}else {
				return false; // cancel the event
			}
		}
		
	jQuery(document).ready(function(){
		jQuery("#edit_stage_1").on('click',function(e){
			 e.preventDefault();
			jQuery("#ttModal").modal("show");
			
		});
		localCommunitymap();
		
	});

var geocoder;
var map;
var marker;
function updateMarkerPosition_stp1(latLng) {
 /*  document.getElementById('markerinfo').innerHTML = [
    latLng.lat(),
    latLng.lng()
  ].join(', '); */
  var latlong= latLng.lat()+', '+latLng.lng();
  $('#lat_lan').val(latlong);
}
function updateMarkerStatus_stp1(str) {
  document.getElementById('markerStatus_stp1').innerHTML = str;
}
function geocodePosition_stp1(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress_stp1(responses[0].formatted_address);
    } else {
      updateMarkerAddress_stp1('Cannot determine address at this location.');
    }
  });
}
function updateMarkerAddress_stp1(str) {
  document.getElementById('markeraddress_stp1').innerHTML = str;
  $('#markerinfo_stp1').val(str);
}
 
function localCommunitymap(lat=false)  {
	
    geocoder = new google.maps.Geocoder();
   var address = document.getElementById('city_country_stp1').value;
   var latt = document.getElementById('lat_lan').value;
	latt  = latt.split(',');
   var latlng = new google.maps.LatLng(latt[0],latt[1]);
      if(address=='Kazakhstan' || address=='Kazakhstan'){
	   var zoomout = 1;
   }else{
	   var zoomout= 10;
   }
   if(lat!=''){
	 var argument = {'address': address};
   }else{
	 var argument = {'latLng': latlng};
   }
   
  geocoder.geocode( argument, function(results, status) {
	 if(lat!=''){
		 var position = results[0].geometry.location;
	   }else{
		 var position = latlng;
	   }
    if (status == google.maps.GeocoderStatus.OK) {
      map = new google.maps.Map(document.getElementById('mapCanvas2_stp1'), {
			zoom: zoomout,
            streetViewControl: false,
          mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              mapTypeIds:[google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.ROADMAP] 
    },
    center: results[0].geometry.location,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
      map.setCenter(results[0].geometry.location);
      marker = new google.maps.Marker({
          map: map,
         // position: results[0].geometry.location,
		  position:  position,
          draggable: true,
          title: 'You are here'
      });
      updateMarkerPosition_stp1(position);
      geocodePosition_stp1(position);
        
      // Add dragging event listeners.
  google.maps.event.addListener(marker, 'dragstart', function() {
    updateMarkerAddress_stp1('Dragging...');
  });
      
  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerStatus_stp1('Dragging...');
    updateMarkerPosition_stp1(marker.getPosition());
  });
  
  google.maps.event.addListener(marker, 'dragend', function() {
    updateMarkerStatus_stp1('Drag ended');
    geocodePosition_stp1(marker.getPosition());
      map.panTo(marker.getPosition()); 
  });
  
  google.maps.event.addListener(map, 'click', function(e) {
    updateMarkerPosition_stp1(e.latLng);
    geocodePosition_stp1(marker.getPosition());
    marker.setPosition(e.latLng);
  map.panTo(marker.getPosition()); 
  }); 
  
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
    }
  });
} 	
</script>

<style>
.progress-striped .bar {
    background-image: none;
}
#success_msg,#email_msg {
  color: green;
  font-size: 17px;
  margin-bottom: 10px;
  padding: 4px 6px;
}
@import url('https://netdna.bootstrapcdn.com/bootstrap/2.3.2/css/bootstrap.min.css');
 .mystage {
    width:400px;
}
.modal {
    position: fixed;
    top: 0;
    right:inherit;
	background-color: transparent;
    bottom: 0;
    left:50%;
    z-index: 1050;
    display: none;
     -webkit-overflow-scrolling: touch;
	 width:530px;
	 border:none; box-shadow:none;
	 
}
.modal.fade .modal-dialog{
	width:100%; margin:0;
}

/* .new_table > b {
  top: 78px;
  left: -75px;
  position: absolute;
}
.new_table {
  position: relative;
} */
.new_table > b {
  position: relative;
  top: 99px;
  left: -70px;
}
.input-group-addon
{
	border-left:none!important;
	border-top:none!important;
	border-bottom:none!important;
	border-right:1px solid #ddd!important;
	border-radius:0!important;
	padding: 7px 12px!important;
}
.total-cost-group
{
	position:relative;
}


.total-cost-group input {
  padding-left: 35px!important;
}
.total-cost-group .input-group-addon {
  position: absolute;
  margin-top: 1px;
  margin-left: 1px;
}

@media screen and (max-width:767px)
{
	.modal  {width:97%; left:0; right:0; margin:0 auto;}
	
}
</style>