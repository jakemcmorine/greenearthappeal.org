<div class="container">
    <div class="row-fluid">
        <div class="span3 offset4">
            <p><div class="text-error"><?php if(isset($error)) { echo $error; } ?></div></p>
            <p class="text-warning">Please login <a href="<?php echo site_url('user/login');?>">here</a>.</p>    
        </div>
        
        <div class="clearfix"></div>
    </div>
</div>