<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/adapters/jquery.js"></script>
<fieldset>
    <form action="" enctype="multipart/form-data" method="post">
        <legend>
            <?php echo $title;?>
            <span class="pull-right"><input type="submit" value="Save all" name="add_initiative" class="btn btn-warning" /></span>
        </legend>
		 <div class="controls">
                <?php echo validation_errors(); ?>
          </div>
        <table class="table table-bordered table-striped">
				<tr>
                    <td>Enter Initiative Name
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Initiative Name: (*):</label>
							 <input type="text" style="width: 95%;" value="<?php echo set_value('initiative_name'); ?>" name="initiative_name" />
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Enter Initiative Certificate Name
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Initiative Certificate Name: (*):</label>
							 <input type="text" style="width: 95%;" value="<?php echo set_value('ini_certificate_name'); ?>" name="ini_certificate_name" />
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Enter Local Name
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Local Name: (*):</label>
							 <input type="text" style="width: 95%;" value="<?php echo set_value('local_name'); ?>" name="local_name" />
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Enter Website URL
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Website: (*):</label>
							 <input type="text" style="width: 95%;" value="<?php echo set_value('website'); ?>" name="website" />
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Enable/Disable Client Activation Email
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Client Activation Email: :</label>
							 <select style="width: 10%;" name="activation_email">
							  <option value="1">Yes</option>
							  <option value="0">No</option>
							</select> 
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Client Email Attached Certificate Type
					</td>
					 <td width="70%">
                        <div>
                           <label class="clearfix">Select Type: </label>
							<select style="width: 12%;" name="cert_type">
							 <option selected value="1">PDF</option>
							 <option value="2">Image</option>
							</select>
                        </div>
				    </td>
					<td></td>
				</tr>
				<tr>
                    <td>Counter Type
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Select Counter Type: </label>
							<select style="width: 24%;" name="ini_counter_type">
							 <option selected value="1">Green Earth Appeal</option>
							 <option value="2">Carbon Free Dining</option>
							</select>
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Enable/Disable Request for Funds
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Request for Funds :</label>
							<input type="radio" name="request_for_funds" value="1">&nbsp;<span>Yes</span>
							<input type="radio" name="request_for_funds" value="0" checked="checked">&nbsp;<span>No</span>
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Enter Sender Email Title
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Email Title: (*):</label>
							 <input type="text" style="width: 95%;" value="<?php echo set_value('sender_email_title'); ?>" name="sender_email_title" />
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Enter Social Tab Text here
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Social Tab Text:</label>
							<textarea style="width: 95%; height:50px;" name="social_text"><?php echo set_value('social_text'); ?></textarea>
                        </div>
				    </td>
				</tr>
				<tr>
                    <td>Enter Company Tab Text here
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Company Tab Text:</label>
							<textarea style="width: 95%; height:50px;" name="company_text"><?php echo set_value('company_text'); ?></textarea>
                        </div>
				    </td>
				</tr>
				<tr>
                    <td>Enter User Tab Text here
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">User Tab Text:</label>
							<textarea style="width: 95%; height:50px;" name="user_text"><?php echo set_value('user_text'); ?></textarea>
                        </div>
				    </td>
				</tr>
				<tr>
					<td>Certificate Layout
					</td>
					 <td width="70%">
						<div>
							<label class="clearfix">Certificate Layout :</label>
							 <select style="width: 15%;" name="certificate_layout">
							  <option  selected="selected" value="1">Layout #1</option>
							  <option value="2">Layout #2</option>
							</select> 
						</div>
					</td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Invoice Company Logo
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Upload:</label>
							 <input type="file" name="invoice_company_logo" accept="image/*" />
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>								<tr>                    <td>Invoice Name					</td>					 <td width="70%">                        <div>                            <label class="clearfix">Invoice Name:</label>							 <input type="text" style="width: 95%;" value="<?php echo set_value('invoice_name'); ?>" name="invoice_name" />                        </div>				    </td>					<td><?php //echo $row->id;?></td>				</tr>
				<tr>
                    <td>Invoice Company Name
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Company Name:</label>
							 <input type="text" style="width: 95%;" value="<?php echo set_value('invoice_company_name'); ?>" name="invoice_company_name" />
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
				 <td>Invoice Company Address
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Company Address:</label>
							<textarea style="width: 95%; height:100px;" name="invoice_company_address"><?php echo set_value('invoice_company_address'); ?></textarea>
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				 <td>Invoice Contact Details
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Contact Details:</label>
							<textarea style="width: 95%; height:100px;" name="invoice_contact_details"><?php echo set_value('invoice_contact_details'); ?></textarea>
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>  
					<td>Invoice Bank Details
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Bank Details:</label>
							<textarea style="width: 95%; height:100px;" name="invoice_bank_details"><?php echo set_value('invoice_bank_details'); ?></textarea>
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Upload certificate Image
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Upload:</label>
							 <input type="file" name="logo" />
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Certificate Field #1</br>
					- fields can be replaced </br>
						[company_name]</br>
						[client_trees]</br>
						[initiative_name]</br>
                        [total_trees]</br>
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Certificate Field #1:</label>
							<textarea style="width: 95%; height:100px;" name="certificate1"><?php echo set_value('certificate1'); ?></textarea>
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Certificate Field #2</br>
					- fields can be replaced</br>
						[company_name]</br>
						[client_trees]</br>
						[initiative_name]</br>
					    [total_trees]</br>
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Certificate Field #2:</label>
							<textarea style="width: 95%; height:100px;" name="certificate2"><?php echo set_value('certificate2'); ?></textarea>
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Certificate Field #3</br>
					- fields can be replaced</br>
						[company_name]</br>
						[client_trees]</br>
						[initiative_name]</br>
						[total_trees]</br>
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Certificate Field #3:</label>
							<textarea style="width: 95%; height:100px;" name="certificate3"><?php echo set_value('certificate3'); ?></textarea>
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Certificate Field #4</br>
					- fields can be replaced</br>
						[company_name]</br>
						[client_trees]</br>
						[initiative_name]</br>
						[total_trees]</br>
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Certificate Field #4:</label>
							<textarea style="width: 95%; height:100px;" name="certificate4"><?php echo set_value('certificate4'); ?></textarea>
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
                <tr>
                    <td>
                        <textarea name="data[1][name]" style="height: 250px;"><?php //echo set_value('data[1][name]'); ?>
Initiative partner account
- when a new Initiativer is added
- fields can be replaced
[initiative_name]
[name]
[first_name]
[username]
[password]
[unique_url]
[ticker_url]</textarea>
                    </td>
                    <td width="70%">
                        <div>
                            <label class="clearfix">Subject:</label>
                            <input type="text" style="width: 95%;" name="data[1][subject]" value="<?php echo set_value('data[1][subject]'); ?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="message1" style="width: 95%; height:200px;" name="data[1][message]"><?php echo set_value('data[1][message]'); ?></textarea>
                        </div>
                    </td>
                    <td>1</td>
                </tr>
				  <tr>
                    <td>
                        <textarea name="data[2][name]" style="height: 250px;"><?php //echo set_value('data[2][name]'); ?>
Initiative Client activation Email
[contact_person]
[activation_url]
[name]: name of company
[initiative_name]
[name]
[first_name]
[unique_url]
[ticker_url]</textarea>
                    </td>
                    <td width="70%">
                        <div>
                            <label class="clearfix">Subject:</label>
                            <input type="text" style="width: 95%;" name="data[2][subject]" value="<?php echo set_value('data[2][subject]'); ?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="message1" style="width: 95%; height:200px;" name="data[2][message]"><?php echo set_value('data[2][message]'); ?></textarea>
                        </div>
                    </td>
                    <td>2</td>
                </tr>
				<tr>
                    <td>
                        <textarea name="data[3][name]" style="height: 250px;"><?php //echo set_value('data[3][name]'); ?>
Email to initiative's partner clients
- when a new initiative confirm his clients on the csv file, emails are sent his clients
- fields can be replaced
[initiative_name]
[contact_person]
[company_name]
[username]
[password]
[unique_url]
[ticker_url]</textarea>
                    </td>
                    <td width="70%">
                        <div>
                            <label class="clearfix">Subject:</label>
                            <input type="text" style="width: 95%;" name="data[3][subject]" value="<?php echo set_value('data[3][subject]'); ?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="message1" style="width: 95%; height:200px;" name="data[3][message]"><?php echo set_value('data[3][message]'); ?></textarea>
                        </div>
                    </td>
                    <td>3</td>
                </tr>
	            <tr>
                    <td>
                        <textarea name="data[4][name]" style="height: 250px;"><?php //echo set_value('data[4][name]'); ?>
Invoice and Certificate for initiative partner
- Email is sent to him when he confirm tree number on the csv file

- Fields can be added
[initiative_name]
[contact_person]
[company_name]
[total_trees]
[current_trees]
[total]: total of money
[ticker_url]
[unique_url]</textarea>
                    </td>
                    <td width="70%">
                        <div>
                            <label class="clearfix">Subject:</label>
                            <input type="text" style="width: 95%;" name="data[4][subject]" value="<?php echo set_value('data[4][subject]'); ?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea  class="message1" style="width: 95%; height:200px;" name="data[4][message]"><?php echo set_value('data[4][message]'); ?></textarea>
                        </div>
                    </td>
                    <td>4</td>
                </tr>
				  <tr>
                    <td>
                        <textarea name="data[5][name]" style="height: 250px;"><?php //echo set_value('data[5][name]'); ?>
Email to Update initiative's partner clients 
- when a new initiative confirm his clients on the csv file, emails are sent his clients
- fields can be replaced
[initiative_name]
[contact_person]
[company_name]
[unique_url]
[ticker_url]</textarea>
                    </td>
                    <td width="70%">
                        <div>
                            <label class="clearfix">Subject:</label>
                            <input type="text" style="width: 95%;" name="data[5][subject]" value="<?php echo set_value('data[5][subject]'); ?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="message1" style="width: 95%; height:200px;" name="data[5][message]"><?php echo set_value('data[5][message]'); ?></textarea>
                        </div>
                    </td>
                    <td>5</td>
                </tr>
				 <tr>
                    <td>
                        <textarea name="data[6][name]" style="height: 250px;"><?php //echo set_value('data[5][name]'); ?>
Monthly Reminder Email
- It will send reminder email to partner to submit figures
- fields can be replaced
[initiative_name]
[contact_person]
[company_name]</textarea>
                    </td>
                    <td width="70%">
                        <div>
                            <label class="clearfix">Subject:</label>
                            <input type="text" style="width: 95%;" name="data[6][subject]" value="<?php echo set_value('data[6][subject]'); ?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="message1" style="width: 95%; height:200px;" name="data[6][message]"><?php echo set_value('data[5][message]'); ?></textarea>
                        </div>
                    </td>
                    <td>6</td>
                </tr>
        </table>
    </form>
</fieldset>
<script>
$('.message1').ckeditor({
    height: "300px",
    toolbarStartupExpanded: true,
    width: "100%"
});

</script>
