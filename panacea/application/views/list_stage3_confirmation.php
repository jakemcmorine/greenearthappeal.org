<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--  datepicker   -->
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<!--  datepicker   -->
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<!--  datepicker   -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/js/iEdit.css">
<script type="text/javascript" src="<?php echo base_url(); ?>/js/iEdit.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/js/script.js"></script>
<div class="container">
	<legend><?php echo $title; ?></legend>
	<?php echo validation_errors(); ?>
</div>
<?php //echo "<pre>"; print_r($persons); die; ?>
<div id="exTab3" class="container">	
<div class="wh_formss">
<ul  class="nav nav-pills">
			<li class="active grayclass">
				<a  href="#stage3" data-toggle="tab">Final Stage</a>
			</li>
			</ul>
			<div class="tab-content clearfix">
			  <div class="tab-pane active" id="stage3">
			<form accept-charset="utf-8" id="stage3_form" method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo site_url("user/edit_grant_user/") ?>">
				<div class="panel panel-info">
					<div class="panel-heading text-center">Stage 3 (Supporting Information)</div>
						<div class="panel-body">
						<div class="controls">
							<?php if (isset($error)) echo "<div class='text-error'>$error</div>"; ?>
						</div>
						<h4>Photographs</h4><hr>
							<?php if(count($persons)>0){				
							$count = 1;
							foreach($persons as $person){ ?>
								<div class="control-group">
										<label class="control-label">Upload a photo of <?php echo $person->first_name; ?>: </label>
										<div class="controls">
											<input type="file" onChange="crop_image(<?php echo $count; ?>)" id="files<?php echo $count; ?>" value="" name="person_photos[<?php echo $count; ?>]" />
											<?php 
											 if($person->place_photo){ echo "<img id='result' width='100' src='" . base_url() . "uploads/person_photos/".$person->place_photo->person_photo ."' />"; } ?>
											<input type="hidden" id="logo-image<?php echo $count; ?>"  name="person_photos[<?php echo $count; ?>][image]" />
											<input type="hidden" value="<?php echo $person->id; ?>" name="person_photos[<?php echo $count; ?>][person_id]" />
											<img style="width:100px;" id="result<?php echo $count; ?>">
										</div>
										</div>
							<?php $count++; } } ?>
							
										
							<div class="control-group">
									<label class="control-label">Community Photos: </label>
									<div class="controls">
										<input type="file" value="<?php //echo $person->person_photo; ?>" name="community_photos" />
										<?php if ($community_photos) echo "<img id='result' width='200' src='" . base_url() . "uploads/person_photos/".$community_photos ."' />"; ?>
									</div>
							</div> 
							<div class="control-group">
									<label class="control-label">Where trees will be planted- Photo upload of location trees will be planted : </label>
									<div class="controls">
										<input type="file" value="<?php //echo $person->person_photo; ?>" name="planted_tree_location_photo" />
										<?php if ($planted_tree_location_photo) echo "<img id='result' width='200' src='" . base_url() . "uploads/person_photos/".$planted_tree_location_photo ."' />"; ?>
									</div>
							</div>
							<div class="control-group">
									<label class="control-label">Locations of picture: </label>
									<div class="controls">
										<input type="text" value="<?php echo $location_of_picture; ?>" name="location_of_picture" />
									</div>
							</div>
							<div class="control-group">
									<label class="control-label">Google Map Point Picker: </label>
										<div class="controls">
										<!--div id="mapCanvas"></div>
									  <div id="infoPanel">
										<b>Marker status:</b>
										<div id="markerStatus"><i>Click and drag the marker.</i></div>
										<b>Current position:</b>
										<div id="markerinfo"></div>
										<b>Closest matching address:</b>
										<div id="markeraddress"></div>
									   </div-->
										<input type="text" value="<?php echo $photos_google_point_picker; ?>" name="photos_google_point_picker" />
									</div>
							</div>
							<h4>Videos</h4><hr>
							<div class="control-group">
									<label class="control-label">Team - Youtube link to any videos showing the team: </label>
									<div class="controls">
										<input type="text" value="<?php echo $team_youtube_link; ?>" name="team_youtube_link" />
									</div>
							</div>
							<div class="control-group">
									<label class="control-label">Community - Youtube link to any videos showing the community : </label>
									<div class="controls">
										<input type="text" value="<?php echo $community_youtube_link; ?>" name="community_youtube_link" />
									</div>
							</div>
							<div class="control-group">
									<label class="control-label">Where trees will be planted - Youtube link to any videos showing location  : </label>
									<div class="controls">
										<input type="text" value="<?php echo $tree_planted_youtube_link; ?>" name="tree_planted_youtube_link" />
									</div>
							</div>
							<div class="control-group">
									<label class="control-label">Locations of videos: </label>
									<div class="controls">
										<input type="text" value="<?php echo $tree_planted_location; ?>" name="tree_planted_location" />
									</div>
							</div>
							<div class="control-group">
									<label class="control-label">Google Map Point Picker: </label>
									<div class="controls">
										<input type="text" value="<?php echo $videos_google_point_picker; ?>" name="videos_google_point_picker" />
									</div>
							</div>
							<div class="control-group">
									<label class="control-label">Upload additional supporting documentation  : </label>
									<div class="controls">
										<input type="file" value="<?php //echo $person->person_photo; ?>" name="additional_documentation" />
									</div>
							</div>
						 </div>
						</div>
						<input type="hidden" name="edit_stage_3" value="edit_stage_3">
							<div id="success_msg">
							</div>
							<div class="controls">
							<?php if($stage3_status==0){ ?>
								<button class="btn btn-success" type="button" onclick="confirm_stage('<?php echo $user_id; ?>','3')" name="edit_stage_2">Confirm Stage 3</button>
							<?php } else{ ?>
								<button class="btn btn-success" type="button"  name="edit_stage_2">Stage 3 Already confirmed</button>
							<?php } ?>
 						</div>
						<br>
				 </div>
			</form>
		</div>
	</div>
	</div>
	</div>
	<script  src="<?php echo base_url(); ?>js/jquery-1.10.2.js"  type="text/javascript" ></script>
<script  src="<?php echo base_url(); ?>js/jquery-ui.js"  type="text/javascript" /></script>
<link href="<?php echo base_url(); ?>css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />


<script type="text/javascript">
 function confirm_stage(user_id,stage){
		if(confirm("Do you really want to confirm the Stage 3?")) {
		var user_id = user_id;
 			$.post('<?php echo site_url('user/stage_confirmed/');?>',
			{ 
				 user_id : user_id,
				 stage : stage
			}, 
 			function(data) 
			{
				$("#success_msg").html('Stage 3 confirmed successfully!');
			}); 
			}else {
				return false; // cancel the event
			}
		}
		
</script>
<style>
body {
  padding : 10px ;
  
}
.wh_formss input, textarea, .uneditable-input {
  width: 310px;
}

#exTab1 .tab-content {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}

#exTab2 h3 {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}


#exTab3 .wh_formss {
  margin: 0 auto;
  width: 70%;
}

#exTab3 .nav.nav-pills
{
	margin-bottom:0px!important;
}
/* remove border radius for the tab */

#exTab1 .nav-pills > li > a {
  border-radius: 0;
}

/* change border radius for the tab , apply corners on top*/

#exTab3 .nav-pills > li > a {
  border-radius: 4px 4px 0 0 ;
}

#exTab3 .tab-content {
  border: 1px solid #E5E6E7;
  padding : 5px 15px;
}
.tab-content.clearfix {
  width: 100%;
}
#exTab3 .nav-pills > li > a
{
	padding:13px!important;
}
.call_us
{
	margin-top:50px;
}
.grayclass a{
	 background-color: #eee;
}
.wh_formss .nav .active a{
	 background-color: #08c!important;
}
select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
  border-radius: 4px;
  color: #555;
  display: inline-block;
  font-size: 14px;
  height: 30px;
  line-height: 20px;
  margin-bottom: 10px;
  padding: 4px 6px;
  vertical-align: middle;
}
.panel-info > .panel-heading {
  text-transform: uppercase;
}
.table input {display: block !important; padding: 0 !important; margin: 0 !important; border: 0 !important; width: 100% !important; border-radius: 0 !important; line-height: 1 !important; text-align: center;}
.table td {margin: 0 !important; padding: 0 !important;}
.add_button img {width: 35px !important;}
.remove_button img {width: 22px !important;}
.disabled{
	pointer-events: none;
    cursor: default;
}
#success_msg {
  color: green;
  font-size: 17px;
  margin-bottom: 10px;
  padding: 4px 6px;
}


@media screen and (max-width:992px)
{
	.wh_formss input, textarea, .uneditable-input {
  width: auto!important;
  float:left;
	}
	.control-label{margin-right:10px;}
.form-horizontal .controls {

  margin-left: 15px!important;
}

}

@media screen and (max-width:767px)
{
	#exTab3 .wh_formss {
  float: left!important;
  margin: 0 auto;
  
  width: 100%!important;
}

.container 
{
	float:left!important;
	padding:5px!important;
	width:100%!important;
}
.panel , .tab-pane , form , .panel-body , .google_canvas{
  float: left!important;
  width: 100%!important;
}
.tab-content {
  float: left;
  width: 100%!important;
  padding:0px!important;
}
.control-label {
  margin-right: 0;
  float: left!important;
  width: 100%!important;
  text-align: left!important;
  font-size: 13px!important;
}
.required {
  float: left;
  width: 100%!important;
}

.form-horizontal .controls {
  float: left;
  margin-left: 0!important;
  padding-left: 0;
  width: 100%;
}
.mystage {
  width: auto;
}
.wh_formss input, textarea, .uneditable-input {
  float: left;
  width: 100%!important;
}
.panel-heading
{
	font-size:12px;
}
.panel-body {
  border: medium none;
  padding: 5px;
}
.new_table {
  overflow: scroll;
}
.wh_btns
{
	
	float:left;
	width:100%;
	text-align:center;
	margin-bottom:20px;
}
}
</style>