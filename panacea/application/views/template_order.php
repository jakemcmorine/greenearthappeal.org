<!DOCTYPE html>
<html>
<head>
	<meta name="author" content="johnn" />
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="http://www.greenearthappeal.org/plugins/system/jat3/jat3/base-themes/default/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <script src="<?php echo base_url();?>/js/bootstrap.min.js"></script>
	<title><?php echo $title; ?></title>
    <style>
        #pct_header, #pct_footer {padding: 10px; background: gray;}
        a {color: #146734}
    </style>
</head>

<body style="padding-top: 10px;">
<div class="container_fluid">
    <div id="header">
        <div class="row-fluid">
            <div class="pull-left">
                <a href="<?php echo site_url();?>">
                    <img alt="logo" src="http://www.greenearthappeal.org/the_sonne/images/Partners/logo-trans.png" />
                </a>
            </div>
            <div class="pull-right">
                <?php if ($_SESSION['login']['type']=='admin' || $_SESSION['login']['type']=='manager') :?>
                <a href="<?php echo site_url('user/add_affiliate')?>" class="btn btn-info">Add Affiliate</a>
                <a href="<?php echo site_url('user/add_bulk_partner');?>" class="btn btn-info">Add a Bulk Partner</a>
                <a href="<?php echo site_url('user/add_restaurant');?>" class="btn btn-info">Add a Restaurant</a>
                <?php endif;?>
                <?php if ($_SESSION['login']['type']=='admin') :?>
                <a href="<?php echo site_url('user/add_solicitor');?>" class="btn btn-info">Add a Solicitor</a>
                <?php endif;?>
                <!------>
                <?php if ($_SESSION['login']['type']=='bulk_partner') :?>
                <a href="<?php echo str_replace('/index.php', '', site_url('uploads/sample-csv/sample_csv.csv'));?>" class="btn btn-warning">Download a sample csv</a>
                <a href="<?php echo site_url('user/edit_bulk_partner')?>" class="btn btn-info">Edit your account</a>
                <a href="<?php echo site_url('user/add_clients')?>" class="btn btn-info">Add Clients</a>
                <a href="<?php echo site_url('user/clients_list')?>" class="btn btn-info">List of Clients</a>
                <?php endif;?>
                <!-------->
                <?php if ($_SESSION['login']['type']=='restaurant') :?>
                <a href="<?php echo site_url('user/edit_restaurant')?>" class="btn btn-info">Edit your account</a>
                <?php endif;?>
                <?php if ($_SESSION['login']) {
					echo "<br /><br />"; ?>
                    <?php if ($_SESSION['login']['type']=='admin') :?>
                    <a href="<?php echo site_url('user/invoices')?>" class="btn btn-success">Invoices History</a>
                    <?php endif;
					if ($_SESSION['login']['type']=='admin' || $_SESSION['login']['type']=='manager') : ?>
                    <a href="<?php echo site_url('user/email_templates');?>" class="btn btn-warning">Email Templates</a>
                    <a href="<?php echo site_url('user/affiliates_list');?>" class="btn btn-danger">Affiliates List</a>
                    <a href="<?php echo site_url('user/track_list');?>" class="btn btn-danger">Affiliates Tracking</a>
					<?php endif;
					if ($_SESSION['login']['type']=='affiliate') :
                    echo '<a href="'. site_url('user/affiliate_index').'" class="btn btn-danger">Home</a> ';
					else:
					echo '<a href="'. base_url().'" class="btn btn-danger">Home</a> ';
					endif;
                    echo '<a href="'. site_url('user/logout').'" class="btn btn-danger">Logout</a>';
                }
                ?>
            </div>
            <?php if ($_SESSION['login']['type']=='bulk_partner') :?>
            <div style="float:right; clear:right; margin-top: 30px" class="text-success">To date you have planted <b><?php echo $_SESSION['login']['data']['total_trees'];?></b> trees</div>
            <?php endif;?>
        </div>
        <div class="clearfix"></div>
    </div>
    <!--  // body here -->
    <div id="pct_body">
        <?php echo $this->load->view($body);?>
        <div class="clearfix"></div>
    </div>
    <div class="call_us">
        <p><img border="0" src="http://www.greenearthappeal.org/the_sonne/images/Partners/call-us.png" alt="Sample image" /></p>
        <p class="text-larger">Send us an 
            <a href="mailto:website@greenearthappeal.org?subject=I%20was%20on%20your%20website....">email</a> or give us a call on <a href="#">020 8798 0476</a>
        </p>
    </div>
    <div id="footer">
        <div class="ja-copyright">
            <small>Copyright &copy; 2013 Green Earth Appeal. All Rights Reserved.</small><br>
            <small>Site Built By Our Digital Partner <a style="color: orange;" href="http://www.nonoodle.co.uk">nonoodle.co.uk</a></small>
        </div>
    </div>
    <style>
        #footer {
            padding: 30px 0;
            background: url("http://www.greenearthappeal.org/templates/ja_puresite/images/bg-body.gif") repeat scroll 0 0 #C9C9C9;
        }
        #header, #footer {
            color: #656667;
            font-size: 16px;
            text-align: center;
        }
        #header {
            background: none repeat scroll 0 0 #FFFFFF;
            border-bottom: 1px solid #E5E6E7;
            padding-bottom: 10px;
            text-align: left;
        }
        .call_us {
            border-top: 1px solid #E5E6E7;
            font-size: 22px;
            padding: 10px 0;
            text-align: center;
        }
</style>
</div>


</body>
</html>