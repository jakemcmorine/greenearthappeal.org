<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="">
        <fieldset>
            <legend class="text-center"><?php echo $title;?></legend>
             <div class="controls">
                <?php
				
                    echo '<div class="text-error">'.validation_errors().'</div>';
                    if (isset($error)) echo '<div class="text-error">'.$error.'</div>';
                ?>
            </div>
			<div class="control-group">
				Hello, Please confirm the number of trees you wish to plant for this month.<!--b>June 2018</b-->   
			</div>
			<div class="control-group">
                <label class="control-label">Your Tree Number(*):</label>
                <div class="controls">
                    <input type="text" name="trees" value="<?php set_value('trees');?>" />
                    <input type="hidden" name="user_id" value="<?php if(isset($partner_id)){ echo $partner_id; };?>" />
                    <input type="hidden" name="add_monthly_figure" value="add_monthly_figure" />
                </div>
            </div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-success"  type="submit" name="add_a_figure">Submit</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>