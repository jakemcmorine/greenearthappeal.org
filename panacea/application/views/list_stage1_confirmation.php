<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--  datepicker   -->
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<!--  datepicker   -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/js/iEdit.css">
<script type="text/javascript" src="<?php echo base_url(); ?>/js/iEdit.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/js/script.js"></script>
<div class="container">
	<legend><?php echo $title; ?></legend>
	<?php echo validation_errors(); ?>
</div>
<?php //echo "<pre>"; print_r($persons); die; ?>
<div id="exTab3" class="container">	
<div class="wh_formss">
<ul  class="nav nav-pills">
			<li class="active grayclass">
				<a  href="#stage1" data-toggle="tab">Project Overview</a>
			</li>
			</ul>
			<div class="tab-content clearfix">
			  <div class="tab-pane active" id="stage1">
			<form accept-charset="utf-8" id="stage1_form"  class="form-horizontal">
						<div class="panel panel-info">
							<div class="panel-heading text-center">Stage 1</div>
							<div class="panel-body">
							 <div class="control-group">
								<label class="control-label">Type of Project: (*)</label>
								<div class="controls">
									<select name="type_of_project" class="required" required="" disabled>
										<option value="">--Select--</option>
									    <option <?php if($type_of_project=="One off project"){ echo 'selected="selected"'; } ?> value="One off project">One off project</option>
									    <option <?php if($type_of_project=="Ongoing project"){ echo 'selected="selected"'; } ?> value="Ongoing project">Ongoing project</option>
									</select>
								</div>
							</div>	
							<div class="control-group">
								<label class="control-label">Proposed Project Start Date: (*)</label>
								<div class="controls">
									<input type="text" value="<?php echo $project_start_date; ?>"  name="project_start_date" id="project_start_date" disabled/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Project Aims: (*)</label>
								<div class="controls">
									<textarea name="project_aims" style="height:88px;" placeholder="Please give a description of your project including aims, who will be involved and what the overall benefits will be" class="required" disabled><?php echo $project_aims; ?></textarea>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Local Community: (*)</label>
								<div class="controls">
									<textarea name="local_community" style="height:88px;" placeholder="Please describe what local community support you have for your project or how you plan to get this support" class="required" required="" disabled><?php echo $local_community; ?></textarea>
								</div>
							</div>
							 <div class="control-group">
								<label class="control-label" class="required" required="">Team: (*)</label>
								<div class="controls">
									<select name="team" id="team" disabled>
										<option value="">--Select--</option>
										<?php for($i=1;$i<=10;$i++){ ?>
											<option <?php if($team==$i){ echo 'selected="selected"'; } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
									    <?php } ?>
									</select>
								</div>
							</div>	
							
							<div id="persons_data"><?php if(!empty($persons)){
								$count=1;
								foreach($persons as $person){ ?>
								<div class="col-sm-10">
									<div class="panel panel-default">
										<div class="panel-heading">Person <?php echo $count; ?></div>
										<div class="panel-body">
											<div class="control-group">
												<label class="control-label">First Name: (*)</label>
												<div class="controls">
													<input type="text" value="<?php echo $person->first_name; ?>" name="person[<?php echo $person->id; ?>][first_name]" disabled />
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Surname: (*)</label>
												<div class="controls">
													<input type="text" value="<?php echo $person->surname; ?>" name="person[<?php echo $person->id; ?>][surname]" disabled />
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Qualifications: </label>
												<div class="controls">
													<input type="text" value="<?php echo $person->qualifications; ?>" name="person[<?php echo $person->id; ?>][qualifications]" disabled />
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Responsibilities in the project: (*)</label>
												<div class="controls">
													<input type="text" value="<?php echo $person->project_responsbilites; ?>" name="person[<?php echo $person->id; ?>][project_responsbilites]" disabled />
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Experience: </label>
												<div class="controls">
													<input type="text" value="<?php echo $person->experience; ?>" name="person[<?php echo $person->id; ?>][experience]" disabled />
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Photo (upload): </label>
												<div class="controls">
												<?php if(isset($person->person_photo)){  ?>
													<img src="<?php  echo base_url() . "uploads/organisation_stage1/{$person->person_photo}" ; ?>"  height="100" width="100">
												<?php } ?>
													
												</div>
											</div>
										</div>
									  </div>
								</div>
								
							<?php $count++; } } ?></div>
							<div class="control-group">
								<label class="control-label">Total cost for project(USD $): </label>
								<div class="controls">
									<input type="number" value="<?php echo $total_cost; ?>" name="total_cost" id="total_cost" disabled />
								</div>
							</div>
							 <div class="control-group">
								<label class="control-label">Are other organisations providing funding?: (*)</label>
									<div class="controls">
										<select name="org_provide_fund" class="required" id="org_provide_fund" required="" disabled >
											<option  value="">--Select--</option>
											<option <?php if($org_provide_fund=="yes"){ echo 'selected="selected"'; } ?> value="yes">Yes</option>
											<option <?php if($org_provide_fund=="no"){ echo 'selected="selected"'; } ?> value="no">No</option>
										</select>
									</div>
								</div>	
								<div id="privide_funding_data"></div>
								<div id="organisation_funding_data">
								<?php if(!empty($organisations)){
								$count=1;
								foreach($organisations as $organisation){ ?>
										<div class="col-sm-10">
										<div class="panel panel-default">
											<div class="panel-heading">Organisation <?php echo $count; ?></div>
											<div class="panel-body">
												<div class="control-group">
													<label class="control-label">Name: (*)</label>
													<div class="controls">
														<input type="text" value="<?php echo $organisation->org_name; ?>" name="organisation[<?php echo $organisation->id; ?>][org_name]" disabled />
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Funding provided : (*)</label>
													<div class="controls">
														<input type="text" value="<?php echo $organisation->funding_provide; ?>" name="organisation[<?php echo $organisation->id; ?>][funding_provide]" disabled />
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Website: </label>
													<div class="controls">
														<input type="text" value="<?php echo $organisation->org_website; ?>" name="organisation[<?php echo $organisation->id; ?>][org_website]" disabled />
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Contact Person First Name: (*)</label>
													<div class="controls">
														<input type="text" value="<?php echo $organisation->person_first_name; ?>" name="organisation[<?php echo $organisation->id; ?>][person_first_name]" disabled />
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Contact Person Surname: (*)</label>
													<div class="controls">
														<input type="text" value="<?php echo $organisation->person_surname; ?>" name="organisation[<?php echo $organisation->id; ?>][person_surname]" disabled />
													</div>
												</div>
												<div class="control-group">
														<label class="control-label">Email Address: (*)</label>
													<div class="controls">
														<input type="text" value="<?php echo $organisation->person_email_Address; ?>" name="organisation[<?php echo $organisation->id; ?>][person_email_Address]" disabled />
													</div>
												</div>
											</div>
										  </div>
									</div>
								
								<?php $count++; } } ?></div>
								 <div class="control-group">
									<label class="control-label">Breakdown of costs: (Use figure from Total cost for project to reconcile)</label>
									<div class="controls">
									 <div class="new_table">
										 <table class="table table-bordered table-condensed">
										  <tbody>
											<tr>
											  <th>Description (*)</th>
											  <th>Units (eg. KG,Days,Seeds,metres)(*)</th>
											  <th>Cost Per Unit (*)</th>
											  <th>Number of Units (*)</th>
											</tr>
											<?php if(!empty($breakdowns)){
												$count=1;
												foreach($breakdowns as $breakdown){ ?>
													<tr>
													  <td><input type="text" value="<?php echo $breakdown->description; ?>" name="breakdown[<?php echo $breakdown->id; ?>][description]" class="form-control" disabled /></td>
													  <td><input type="text" value="<?php echo $breakdown->units; ?>"  name="breakdown[<?php echo $breakdown->id; ?>][units]" class="form-control" disabled /></td>
													  <td><input type="text" value="<?php echo $breakdown->cost_per_unit; ?>"  name="breakdown[<?php echo $breakdown->id; ?>][cost_per_unit]" class="form-control" disabled /></td>
													  <td><input type="text" value="<?php echo $breakdown->no_of_units; ?>" name="breakdown[<?php echo $breakdown->id; ?>][no_of_units]" class="form-control" disabled /></td>
													</tr>
												
											<?php $count++; } } ?>
										  </tbody>
										</table>
									</div>
									</div>
								</div>	
								<input type="hidden" name="edit_stage_1" value="edit_stage_1">
								<div id="success_msg">
								</div>
								<div class="controls">
								<?php if($status==0){ ?>
									<button class="btn btn-success" type="button" onclick="confirm_stage('<?php echo $user_id; ?>','1')" name="edit_stage_1">Confirm Stage 1</button>
								<?php } else{ ?>
									<button class="btn btn-success" type="button"  name="edit_stage_1">Stage 1 Already confirmed</button>
								<?php } ?>
 								</div>
						</div>
						</div>
					</form>
				</div>
		  </div>
  </div>
<script  src="<?php echo base_url(); ?>js/jquery-1.10.2.js"  type="text/javascript" ></script>
<script  src="<?php echo base_url(); ?>js/jquery-ui.js"  type="text/javascript" /></script>
<link href="<?php echo base_url(); ?>css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />


<script type="text/javascript">
 function confirm_stage(user_id,stage){
		if(confirm("Do you really want to confirm the Stage 1?")) {
		var user_id = user_id;
 			$.post('<?php echo site_url('user/stage_confirmed/');?>',
			{ 
				 user_id : user_id,
				 stage : stage
			}, 
 			function(data) 
			{
				$("#success_msg").html('Stage 1 confirmed successfully!');
			}); 
			}else {
				return false; // cancel the event
			}
		}
		
</script>
<style>
body {
  padding : 10px ;
  
}
.wh_formss input, textarea, .uneditable-input {
  width: 310px;
}

#exTab1 .tab-content {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}

#exTab2 h3 {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}


#exTab3 .wh_formss {
  margin: 0 auto;
  width: 70%;
}

#exTab3 .nav.nav-pills
{
	margin-bottom:0px!important;
}
/* remove border radius for the tab */

#exTab1 .nav-pills > li > a {
  border-radius: 0;
}

/* change border radius for the tab , apply corners on top*/

#exTab3 .nav-pills > li > a {
  border-radius: 4px 4px 0 0 ;
}

#exTab3 .tab-content {
  border: 1px solid #E5E6E7;
  padding : 5px 15px;
}
.tab-content.clearfix {
  width: 100%;
}
#exTab3 .nav-pills > li > a
{
	padding:13px!important;
}
.call_us
{
	margin-top:50px;
}
.grayclass a{
	 background-color: #eee;
}
.wh_formss .nav .active a{
	 background-color: #08c!important;
}
select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
  border-radius: 4px;
  color: #555;
  display: inline-block;
  font-size: 14px;
  height: 30px;
  line-height: 20px;
  margin-bottom: 10px;
  padding: 4px 6px;
  vertical-align: middle;
}
.panel-info > .panel-heading {
  text-transform: uppercase;
}
.table input {display: block !important; padding: 0 !important; margin: 0 !important; border: 0 !important; width: 100% !important; border-radius: 0 !important; line-height: 1 !important; text-align: center;}
.table td {margin: 0 !important; padding: 0 !important;}
.add_button img {width: 35px !important;}
.remove_button img {width: 22px !important;}
.disabled{
	pointer-events: none;
    cursor: default;
}
#success_msg {
  color: green;
  font-size: 17px;
  margin-bottom: 10px;
  padding: 4px 6px;
}


@media screen and (max-width:992px)
{
	.wh_formss input, textarea, .uneditable-input {
  width: auto!important;
  float:left;
	}
	.control-label{margin-right:10px;}
.form-horizontal .controls {

  margin-left: 15px!important;
}

}

@media screen and (max-width:767px)
{
	#exTab3 .wh_formss {
  float: left!important;
  margin: 0 auto;
  
  width: 100%!important;
}

.container 
{
	float:left!important;
	padding:5px!important;
	width:100%!important;
}
.panel , .tab-pane , form , .panel-body , .google_canvas{
  float: left!important;
  width: 100%!important;
}
.tab-content {
  float: left;
  width: 100%!important;
  padding:0px!important;
}
.control-label {
  margin-right: 0;
  float: left!important;
  width: 100%!important;
  text-align: left!important;
  font-size: 13px!important;
}
.required {
  float: left;
  width: 100%!important;
}

.form-horizontal .controls {
  float: left;
  margin-left: 0!important;
  padding-left: 0;
  width: 100%;
}
.mystage {
  width: auto;
}
.wh_formss input, textarea, .uneditable-input {
  float: left;
  width: 100%!important;
}
.panel-heading
{
	font-size:12px;
}
.panel-body {
  border: medium none;
  padding: 5px;
}
.new_table {
  overflow: scroll;
}
.wh_btns
{
	
	float:left;
	width:100%;
	text-align:center;
	margin-bottom:20px;
}
}
</style>