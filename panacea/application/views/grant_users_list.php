<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<fieldset>
    <legend><?php echo $title; ?></legend>
	<form class="form-inline" action="" method="POST" role="form">
	   <div class="form-group form1 padding-none">
		<div class="input-group">
				<select  class="form-control" id="stage" name="stage">
					<option value="">Select Stage</option>
					<option value="registered" <?php if(isset($_POST['stage']) && $_POST['stage']=='registered'){ echo 'selected'; } ?>>Registered</option>
					<option value="password" <?php if(isset($_POST['stage']) && $_POST['stage']=='password'){ echo 'selected'; } ?>>Password/Activated</option>
					<option value="proj_overview" <?php if(isset($_POST['stage']) && $_POST['stage']=='proj_overview'){ echo 'selected'; } ?>>Project Overview</option>
					<option value="proj_details" <?php if(isset($_POST['stage']) && $_POST['stage']=='proj_details'){ echo 'selected'; } ?>>Project Details</option>
					<option value="supporting_info" <?php if(isset($_POST['stage']) && $_POST['stage']=='supporting_info'){ echo 'selected'; } ?>>Supporting Information</option>
				</select>
	  </div>
	  </div>
	  <div class="form-group form1 padding-none">
	  <button type="submit" class="btn btn-icon btn-primary glyphicon glyphicon-search"></button>
	  </div>
	  </form>
    <table id="grant_user_list"  class="table table-bordered table-striped">
	<thead>
        <tr>
            <th>ID</th>
			<th>Email</th>
			<th>Organisation Name</th>
			<th>Cost Per Tree</th>
			<th>Created Date</th>
            <th width="300">Actions</th>
        </tr>
	</thead>
	<tbody>
        <?php //echo "<pre>"; print_r($rows); die;
            if (count($rows)) :
                foreach ($rows AS $row):
					$url_edit = site_url('user/edit_grant_user/'.$row->user_id);
					$url_delete = site_url('user/delete_grant_user/'.$row->user_id);
					$disable_email = site_url('user/disable_grant_reminder_email/'.$row->user_id);
					$enable_email = site_url('user/enable_grant_reminder_email/'.$row->user_id);
        ?>
            <tr class="table_data">
                <td><?php echo $row->user_id;?></td>
                <td><?php echo $row->email;?></td>
                <td><?php echo $row->organisation_name;?></td>
                <td><?php echo round($row->project_fund/$row->no_of_trees, 2); ?></td>
                <td><?php echo $row->date_created;?></td>
                <td>
				  <?php if($row->reminder_email=='1'){ ?>
					   <a class="btn btn-warning btn-small" href="<?php echo $disable_email;?>">Unsubscribe</a>
				 <?php }else{ ?>
				       <a class="btn btn-success btn-small" href="<?php echo $enable_email;?>">Subscribe</a>
				 <?php } ?>
                    <a class="btn btn-info btn-small" href="<?php echo $url_edit;?>">Confirm Stages</a>
                    <a class="btn btn btn-danger btn-small" href="<?php echo $url_delete;?>">Delete</a>
                </td>
            </tr>
        <?php endforeach; endif;?>
	</tbody>
    </table>
</fieldset>
<script>
	$(document).ready(function() {
		$('#grant_user_list').dataTable( {
		 "bFilter" : false,  
		"bLengthChange": false,		 
		  "aoColumnDefs": [
			{ 
			  "bSortable": false, 
			  "aTargets": [5] // <--  column and turns off sorting
			 } 
			]
		} );
	} );
	
	</script>
	<style>
	form{
		margin:0px!important;
	}
	.form1 
	{
		float:left;
		margin-right:15px;
	}
	</style>