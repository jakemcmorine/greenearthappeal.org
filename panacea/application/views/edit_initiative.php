<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/adapters/jquery.js"></script>
<fieldset>
    <form action="" enctype="multipart/form-data" method="post">
        <legend>
            <?php echo $title;?>
            <span class="pull-right"><input type="submit" value="Save all" name="edit_initiative" class="btn btn-warning" /></span>
        </legend>
		 <div class="controls">
                <?php echo validation_errors(); ?>
          </div>
        <table class="table table-bordered table-striped">
				<tr>
                    <td>Enter Initiative Name
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Initiative Name: (*):</label>
							 <input type="text" style="width: 95%;" value="<?php echo stripslashes($initiative_name); ?>" name="initiative_name" />
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Enter Initiative Certificate Name
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Initiative Certificate Name: (*):</label>
							 <input type="text" style="width: 95%;" value="<?php echo stripslashes($ini_certificate_name); ?>" name="ini_certificate_name" />
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Enter Local Name
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Local Name: (*):</label>
							 <input type="text" style="width: 95%;" value="<?php echo stripslashes($local_name); ?>" name="local_name" />
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Enter Website URL
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Website: (*):</label>
							 <input type="text" style="width: 95%;" value="<?php echo $website; ?>" name="website" />
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Enable/Disable Client Activation Email
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Client Activation Email: </label>
							 <select style="width: 10%;" name="activation_email">
							  <option  <?php echo $sel= ($activation_email=='1')? "selected" : "";  ?> value="1">Yes</option>
							  <option  <?php echo $sel= ($activation_email=='0')? "selected" : "";  ?> value="0">No</option>
							</select> 
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Client Email Attached Certificate Type
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Select Type: </label>
							<select style="width: 12%;" name="cert_type">
							 <option <?php echo $certtype= ($cert_type=='1')? "selected" : "";  ?>  value="1">PDF</option>
							 <option <?php echo $certtype= ($cert_type=='2')? "selected" : "";  ?>  value="2">Image</option>
							</select>
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Counter Type
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Select Counter Type: </label>
							<select style="width: 28%;" name="ini_counter_type">
							 <option <?php echo $countertype= ($ini_counter_type=='1')? "selected" : "";  ?>  value="1">Green Earth Appeal</option>
							 <option <?php echo $countertype= ($ini_counter_type=='2')? "selected" : "";  ?> value="2">Carbon Free Dining</option>
							 <option <?php echo $countertype= ($ini_counter_type=='3')? "selected" : "";  ?> value="3">Non Partner</option>
							</select>
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
			   <tr>
                    <td>Enable/Disable Request for Funds
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Request for Funds :</label>
							<input type="radio" name="request_for_funds" value="1" <?php echo $sel= ($request_for_funds=='1')? "checked" : "";  ?>>&nbsp;<span>Yes</span>
							<input type="radio" name="request_for_funds" value="0" <?php echo $sel= ($request_for_funds=='0')? "checked" : "";  ?>>&nbsp;<span>No</span>
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Total Tree Counter
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Tree Counter URL:	<a id="copytext" style="background: transparent none repeat scroll 0% 0% ! important;" target="_blank" href="<?php echo base_url() . 'tickers/initiative_tickers/ticker_' . $id . '.png'; ?>"><?php echo base_url() . 'tickers/initiative_tickers/ticker_' . $id . '.png'; ?></a></br>
							<button type="button" class="btn btn-info btn-xs" onClick="copyToClipboard('copytext');">Copy URL</button></label>
							<img src="<?php echo base_url() . 'tickers/initiative_tickers/ticker_' . $id . '.png?'.time(); ?>" />
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				 <tr>
                    <td>Counter String
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Tree Counter String:	<a id="copytextstringurl" style="background: transparent none repeat scroll 0% 0% ! important;" target="_blank" href="https://carbonfreedining.org/initiative-trees/<?php echo $token; ?>">https://carbonfreedining.org/initiative-trees/<?php echo $token; ?></a><br>
							<button type="button" class="btn btn-info btn-xs" onClick="copyToClipboard('copytextstringurl');">Copy URL</button></label>
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Certificate PDF URL
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix"><a id="copyfullimg" style="background: transparent none repeat scroll 0% 0% ! important;" target="_blank" href="<?php echo base_url() . 'uploads/certs/initiatives/The_Green_Earth_Appeal_Certificate_Initiative_' . $id . '.pdf'; ?>"><?php echo base_url() . 'uploads/certs/initiatives/The_Green_Earth_Appeal_Certificate_Initiative_' . $id . '.pdf'; ?></a></br>
							<button type="button" class="btn btn-info btn-xs" onClick="copyToClipboard('copyfullimg');">Copy URL</button>
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Enter Sender Email Title
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Email Title: (*):</label>
							 <input type="text" style="width: 95%;" value="<?php echo $sender_email_title; ?>" name="sender_email_title" />
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
					<tr>
                    <td>Enter Social Tab Text here
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Social Tab Text:</label>
							<textarea style="width: 95%; height:50px;" name="social_text"><?php echo $social_text; ?></textarea>
                        </div>
				    </td>
				</tr>
				<tr>
                    <td>Enter Company Tab Text here
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Company Tab Text:</label>
							<textarea style="width: 95%; height:50px;" name="company_text"><?php echo $company_text; ?></textarea>
                        </div>
				    </td>
				</tr>
				<tr>
                    <td>Enter User Tab Text here
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">User Tab Text:</label>
							<textarea style="width: 95%; height:50px;" name="user_text"><?php echo $user_text; ?></textarea>
                        </div>
				    </td>
				</tr>
				<tr>
                    <td>Certificate Layout
					</td>
					 <td width="70%">
						<div>
							<label class="clearfix">Certificate Layout :</label>
							 <select style="width: 15%;" name="certificate_layout">
							  <option  <?php echo $sel= ($certificate_layout=='1')? "selected" : "";  ?> value="1">Layout #1</option>
							  <option  <?php echo $sel= ($certificate_layout=='2')? "selected" : "";  ?> value="2">Layout #2</option>
							</select> 
						</div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Invoice Company Logo
					</td>
					 <td width="70%">
                        <div>
							<label class="clearfix">Enable/Disable Invoice Dynamic fields:</label>
							<label class="switch">
							  <input type="checkbox" name="enable_invoice_data" value="1" <?php if($enable_invoice_data=='1'){ echo "checked"; } ?>>
							  <span class="slider"></span>
							</label>
                            <label class="clearfix">Upload:</label>
							<?php if ($invoice_company_logo) echo "<img width='200' src='" . base_url() . "libraries/imgs/{$invoice_company_logo}?". time() ."' />"; ?>
							 <input type="file" name="invoice_company_logo" accept="image/*" />
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>								<tr>                    <td>Invoice Name					</td>					 <td width="70%">                        <div>                            <label class="clearfix">Invoice Name:</label>							 <input type="text" style="width: 95%;" value="<?php echo $invoice_name; ?>" name="invoice_name" />                        </div>				    </td>					<td><?php //echo $row->id;?></td>				</tr>
				<tr>
                    <td>Invoice Company Name
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Company Name:</label>
							 <input type="text" style="width: 95%;" value="<?php echo $invoice_company_name; ?>" name="invoice_company_name" />
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>
				 <td>Invoice Company Address
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Company Address:</label>
							<textarea style="width: 95%; height:100px;" name="invoice_company_address"><?php echo stripslashes($invoice_company_address); ?></textarea>
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				 <td>Invoice Contact Details
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Contact Details:</label>
							<textarea style="width: 95%; height:100px;" name="invoice_contact_details"><?php echo stripslashes($invoice_contact_details); ?></textarea>
                        </div>
				    </td>
					<td><?php //echo $row->id;?></td>
				</tr>
				<tr>  
					<td>Invoice Bank Details
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Bank Details:</label>
							<textarea style="width: 95%; height:100px;" name="invoice_bank_details"><?php echo stripslashes($invoice_bank_details); ?></textarea>
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Upload certificate Image
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Upload:</label>
							<?php if ($bk_image) echo "<img width='200' src='" . base_url() . "libraries/imgs/{$bk_image}?". time() ."' />"; ?>
							 <input type="file" name="logo" accept="image/*" />
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Certificate Field #1</br>
					- fields can be replaced</br>
						[company_name]</br>
						[client_trees]</br>
						[initiative_name]</br>
						[total_trees]</br>
						[cfd_counter]</br>
						[gea_counter]</br>
						<?php if($id=="31"){
							echo "[full_name]"."</br>";
							echo "[name]"."</br>";
							echo "[first_name]"."</br>";
							echo "[last_name]"."</br>";
						} ?>
						<?php if($id=="19"){
							echo "[calculated_cups]"."</br>";
						} ?>
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Certificate Field #1:</label>
							<textarea style="width: 95%; height:100px;" name="certificate1"><?php echo stripslashes($certificate1); ?></textarea>
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Certificate Field #2</br>
					- fields can be replaced</br>
						[company_name]</br>
						[client_trees]</br>
						[initiative_name]</br>
						[total_trees]</br>
						[cfd_counter]</br>
						[gea_counter]</br>
						<?php if($id=="31"){
							echo "[full_name]"."</br>";
							echo "[name]"."</br>";
							echo "[first_name]"."</br>";
							echo "[last_name]"."</br>";
						} ?>
						<?php if($id=="19"){
							echo "[calculated_cups]"."</br>";
						} ?>
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Certificate Field #2:</label>
							<textarea style="width: 95%; height:100px;" name="certificate2"><?php echo stripslashes($certificate2); ?></textarea>
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Certificate Field #3</br>
					- fields can be replaced</br>
						[company_name]</br>
						[client_trees]</br>
						[initiative_name]</br>
					    [total_trees]</br>
						[cfd_counter]</br>
						[gea_counter]</br>
					   <?php if($id=="31"){
							echo "[full_name]"."</br>";
							echo "[name]"."</br>";
							echo "[first_name]"."</br>";
							echo "[last_name]"."</br>";
						} ?>
						<?php if($id=="19"){
							echo "[calculated_cups]"."</br>";
						} ?>
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Certificate Field #3:</label>
							
							<textarea style="width: 95%; height:100px;" name="certificate3"><?php echo stripslashes($certificate3); ?></textarea>
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				<tr>
                    <td>Certificate Field #4</br>
					- fields can be replaced</br>
						[company_name]</br>
						[client_trees]</br>
						[initiative_name]</br>
						[total_trees]</br>
						[cfd_counter]</br>
						[gea_counter]</br>
						<?php if($id=="31"){
							echo "[full_name]"."</br>";
							echo "[name]"."</br>";
							echo "[first_name]"."</br>";
							echo "[last_name]"."</br>";
						} ?>
						<?php if($id=="19"){
							echo "[calculated_cups]"."</br>";
						} ?>
					</td>
					 <td width="70%">
                        <div>
                            <label class="clearfix">Certificate Field #4:</label>
							<textarea style="width: 95%; height:100px;" name="certificate4"><?php echo stripslashes($certificate4); ?></textarea>
                        </div>
				    </td>
					 <td><?php //echo $row->id;?></td>
				</tr>
				 <?php //echo "<pre>"; print_r($rows); die; 
               /*  if (count($rows)) :
                    foreach ($rows AS $row): */
				?>
                <tr>
                    <td>
                        <textarea name="data[1][name]" style="height: 350px;"><?php echo $rows[0]['name']; ?></textarea>
                    </td>
                    <td width="70%">
                        <div>
						 <label class="clearfix">Off/On:</label>
							<label class="switch">
							  <input type="checkbox" name="data[1][enable_email]" value="1" <?php if($rows[0]['enable_email']=='1'){ echo "checked"; } ?>>
							  <span class="slider"></span>
							</label>
                            <label class="clearfix">Subject:</label>
                            <input type="text" style="width: 95%;" name="data[1][subject]" value="<?php echo $rows[0]['subject']; ?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="message1" style="width: 95%; height:200px;" name="data[1][message]"><?php echo $rows[0]['message']; ?></textarea>
                        </div>
                    </td>
					<input type="hidden" name="data[1][id]" value="<?php echo $rows[0]['id'];?>" />
                    <td>1</td>
                </tr>
				   <tr>
                    <td>
                        <textarea name="data[2][name]" style="height: 350px;"><?php echo $rows[1]['name']; ?></textarea>
                    </td>
                    <td width="70%">
                        <div>
							<label class="clearfix">Off/On:</label>
							<label class="switch">
							  <input type="checkbox" name="data[2][enable_email]" value="1" <?php if($rows[1]['enable_email']=='1'){ echo "checked"; } ?>>
							  <span class="slider"></span>
							</label>
                            <label class="clearfix">Subject:</label>
                            <input type="text" style="width: 95%;" name="data[2][subject]" value="<?php echo $rows[1]['subject']; ?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="message1" style="width: 95%; height:200px;" name="data[2][message]"><?php echo $rows[1]['message']; ?></textarea>
                        </div>
                    </td>
					<input type="hidden" name="data[2][id]" value="<?php echo $rows[1]['id'];?>" />
                    <td>2</td>
                </tr>
				   <tr>
                    <td>
                        <textarea name="data[3][name]" style="height: 350px;"><?php echo $rows[2]['name']; ?></textarea>
                    </td>
                    <td width="70%">
                        <div>
							<label class="clearfix">Off/On:</label>
							<label class="switch">
							  <input type="checkbox" name="data[3][enable_email]" value="1" <?php if($rows[2]['enable_email']=='1'){ echo "checked"; } ?>>
							  <span class="slider"></span>
							</label>
                            <label class="clearfix">Subject:</label>
                            <input type="text" style="width: 95%;" name="data[3][subject]" value="<?php echo $rows[2]['subject']; ?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="message1" style="width: 95%; height:200px;" name="data[3][message]"><?php echo $rows[2]['message']; ?></textarea>
                        </div>
                    </td>
					<input type="hidden" name="data[3][id]" value="<?php echo $rows[2]['id'];?>" />
                    <td>3</td>
                </tr>
				   <tr>
                    <td>
                        <textarea name="data[4][name]" style="height: 350px;"><?php echo $rows[3]['name']; ?></textarea>
                    </td>
                    <td width="70%">
                        <div>
							<label class="clearfix">Off/On:</label>
							<label class="switch">
							  <input type="checkbox" name="data[4][enable_email]" value="1" <?php if($rows[3]['enable_email']=='1'){ echo "checked"; } ?>>
							  <span class="slider"></span>
							</label>
                            <label class="clearfix">Subject:</label>
                            <input type="text" style="width: 95%;" name="data[4][subject]" value="<?php echo $rows[3]['subject']; ?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="message1"  style="width: 95%; height:200px;" name="data[4][message]"><?php echo $rows[3]['message']; ?></textarea>
                        </div>
                    </td>
					<input type="hidden" name="data[4][id]" value="<?php echo $rows[3]['id'];?>" />
                    <td>4</td>
                </tr>
				   <tr>
                    <td>
                        <textarea name="data[5][name]" style="height: 350px;"><?php echo $rows[4]['name']; ?></textarea>
                    </td>
                    <td width="70%">
                        <div>
							<label class="clearfix">Off/On:</label>
							<label class="switch">
							  <input type="checkbox" name="data[5][enable_email]" value="1" <?php if($rows[4]['enable_email']=='1'){ echo "checked"; } ?>>
							  <span class="slider"></span>
							</label>
                            <label class="clearfix">Subject:</label>
                            <input type="text" style="width: 95%;" name="data[5][subject]" value="<?php echo $rows[4]['subject']; ?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="message1"  style="width: 95%; height:200px;" name="data[5][message]"><?php echo $rows[4]['message']; ?></textarea>
                        </div>
                    </td>
					<input type="hidden" name="data[5][id]" value="<?php echo $rows[4]['id'];?>" />
                    <td>5</td>
                </tr>
				 <tr>
                    <td>
                        <textarea name="data[6][name]" style="height: 350px;"><?php echo $rows[5]['name']; ?></textarea>
                    </td>
                    <td width="70%">
                        <div>
							<label class="clearfix">Off/On:</label>
							<label class="switch">
							  <input type="checkbox" name="data[6][enable_email]" value="1" <?php if($rows[5]['enable_email']=='1'){ echo "checked"; } ?>>
							  <span class="slider"></span>
							</label>
                            <label class="clearfix">Subject:</label>
                            <input type="text" style="width: 95%;" name="data[6][subject]" value="<?php echo $rows[5]['subject']; ?>" />
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea class="message1"  style="width: 95%; height:200px;" name="data[6][message]"><?php echo $rows[5]['message']; ?></textarea>
                        </div>
                    </td>
					<input type="hidden" name="data[6][id]" value="<?php echo $rows[5]['id'];?>" />
                    <td>6</td>
                </tr>
				 <?php //endforeach; endif;?>
				 
        </table>
    </form>
</fieldset>
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<script>
$('.message1').ckeditor({
    height: "300px",
    toolbarStartupExpanded: true,
    width: "100%"
});

function copyToClipboard(elementId) {
	 var aux = document.createElement("input");
	 aux.setAttribute("value", document.getElementById(elementId).innerHTML);
	 document.body.appendChild(aux)
	 aux.select();
	 document.execCommand("copy");
	 document.body.removeChild(aux);
	 //alert('Link copied.');
}
</script>
