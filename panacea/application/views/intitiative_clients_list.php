<fieldset>
    <legend><?php echo $title;?></legend>
    <table class="table table-bordered table-striped">
        <tr>
            <th>ID</th>
            <th>Email</th>
            <th>Company</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Trees num</th>
            <th>Free Trees</th>
            <th width="105">Action</th>
        </tr>
        <?php
            if (count($rows)) :
                foreach ($rows AS $row):
        ?>
            <tr>
                <td><?php echo $row->id;?></td>
                <td><?php echo $row->email;?></td>
                <td><?php echo stripslashes($row->name);?></td>
                <td><?php echo stripslashes($row->first_name);?></td>
                <td><?php echo stripslashes($row->last_name);?></td>
                <td><?php echo $row->tree_nums;?></td>
                <td><?php echo $row->free_trees;?></td>
				<td>
                    <a class="btn btn-info btn-small" href="<?php echo site_url('user/edit_intitiative_partner_clients').'/'.$row->id; ?>">Edit</a>
                    <a class="btn btn-warning btn-small" onclick="return confirm('Are you sure?')" href="<?php echo site_url('user/delete_intitiative_partner_clients').'/'.$row->id; ?>">Delete</a>
                </td>
                <!--<td><?php echo $row->type;?></td>
                <td><?php echo $row->active;?></td>
                <td>
                    <a class="btn btn-info btn-small" href="<?php echo $url_edit;?>">Edit</a>
                    <a class="btn btn-warning btn-small" href="<?php echo $url_delete;?>">Delete</a>
                </td>-->
            </tr>
        <?php endforeach; endif;?>
    </table>
</fieldset>