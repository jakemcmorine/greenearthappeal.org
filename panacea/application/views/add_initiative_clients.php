<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="" enctype="multipart/form-data">
        <fieldset>
            <legend class="text-center">
                <?php echo $title;?>
            </legend>
           
		  <div class="control-group"> 
                <label class="control-label"></label>
                <div class="controls">
                    <a href="<?php echo str_replace('/index.php', '', site_url('uploads/sample-csv/inititiative_sample_csv.csv')); ?>"
                       class="btn btn-warning">Download a sample csv</a>
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Csv file (*):</label>
                <div class="controls">
                    <input type="file" name="csv_file1" id="csv" accept=".csv" />
                </div>
            </div>
			<div class="controls">
                <?php if (isset($error)) echo '<div class="text-error">'.$error.'</div>';?>
            </div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-success" type="submit" name="add_initiative_clients">Upload</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<div class="modal fade" id="upload_cleint_success">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form action="https://www.greenearthappeal.org/panacea">
				<div class="modal-header">
				<!--button type="button" class="close" data-dismiss="modal">&times;</button-->
				</div>
				<div class="modal-body">
					<p class="display_msg">Thanks for Uploading - Your records will be processed shortly.</p>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success">Okay</button>
				</div>
			</form>
	  </div>
	</div>
</div>
<?php  if($success){ ?>
	<script>
		jQuery(document).ready(function(){
			jQuery("#upload_cleint_success").modal("show");
		});
	</script>
<?php  } ?>
<style>
.modal {
    position: fixed;
    top: 0;
    right:inherit;
	background-color: #f5f5f5;;
    left:50%;
    z-index: 1050;
    display: none;
     -webkit-overflow-scrolling: touch;
	 width:530px;
	 border:none; box-shadow:none;
	 
}
.modal.fade.in {
    top: 25%;
}
.modal-footer {
    padding: 10px 21px 0px !important;
}
.modal-header {
    padding: 15px 15px !important;
    border-bottom: none !important; 
}
.modal.fade .modal-dialog{
	width:100%; margin:0;
}

.display_msg {
    font-size: 15px;
    font-weight: 600;
    color: #186c18;
}
</style>