<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/js/iEdit.css">
<script type="text/javascript" src="<?php echo base_url(); ?>/js/iEdit.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/js/script.js"></script>
<?php if(empty($api_result)) { ?>
<div class="pull-right" style="margin-top: 16px;">
		<a href="javascript:void(0);" class="btn btn-info" id="dialog_partner_add_api">Add to API</a>
</div>
<?php } ?>
<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo site_url("user/edit_intitiative_partner/$id") ?>">
        <fieldset>
            <legend class="text-center"><?php echo $title; ?></legend>
            <div class="controls">
                <?php echo validation_errors(); ?>
                <?php if (isset($error)) echo "<div class='text-error'>$error</div>"; ?>
            </div>
            
           <div class="control-group">
                <label class="control-label">Initiative: (*)</label>
                <div class="controls">
                    <select name="initiative" <?php if($_SESSION['login']['type']=='intitiative_client' || $_SESSION['login']['type']=='intitiative') { echo "disabled"; } ?> >
						<option value="">--Select--</option>
							<?php if(count($results)) { foreach($results as $result){ 
							$selected = ($result['id'] == trim($initiative)) ? 'selected="selected"' : ''; ?>
							<option value="<?php echo $result['id']; ?>"<?php echo $selected; ?>><?php echo $result['local_name']; ?></option>
							<?php } } ?>
                    </select>
                </div>
            </div>
			 <div class="control-group">
                <label class="control-label">Send Invoices: </label>
                <div class="controls">
                    <select name="send_invoices">
                         <option <?php echo $sel= ($send_invoices=='onupload')? "selected" : "";  ?> value="onupload">On Upload</option>
						 <option <?php echo $sel= ($send_invoices=='monthly')? "selected" : "";  ?> value="monthly">Monthly</option>
                    </select>
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Monthly Reminder Email: </label>
                <div class="controls">
                    <select name="monthly_reminder_email">
                         <option <?php echo $selected = ($monthly_reminder_email=='1')? "selected" : "";  ?> value="1">Yes</option>
						 <option <?php echo $selected = ($monthly_reminder_email=='0')? "selected" : "";  ?> value="0">No</option>
                    </select>
                </div>
            </div>
			 <div class="control-group">
                <label class="control-label">Free Trial: </label>
                <div class="controls">
                    <select name="free_trial">
						 <option <?php echo $sel= ($free_trial=='0')? "selected" : "";  ?> value="0">No</option>
                         <option <?php echo $sel= ($free_trial=='1')? "selected" : "";  ?> value="1">Yes</option>
                    </select>
                </div>
            </div>
			 <div class="control-group">
                <label class="control-label">Grid Upload: </label>
                <div class="controls">
                    <select name="grid_upload">
						 <option <?php echo $sel= ($grid_upload=='0')? "selected" : "";  ?> value="0">No</option>
                         <option <?php echo $sel= ($grid_upload=='1')? "selected" : "";  ?> value="1">Yes</option>
                    </select>
                </div>
             </div>
			 
            <!--div class="control-group">
                <label class="control-label">Opportunity String: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php //echo $opportunity_str; ?>" name="opportunity_str" placeholder="gives diners the opportunity to:" />
                </div>
            </div-->

            <div class="control-group">
                <label class="control-label">Partner Landing Page:</label>
					<?php if(empty($website_url)){
						$website_url= "http://www.greenearthappeal.org";
					} ?>
                <div class="controls">
                    <!--a href="<?php echo 'http://www.greenearthappeal.org/?refid=' . $code; ?>" target="_blank"><?php echo 'http://www.greenearthappeal.org/?refid=' . $code; ?></a-->
					<a href="<?php echo $website_url.'/?refid='. $code; ?>" target="_blank"><?php echo $website_url.'/?refid='. $code; ?></a>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Your Tree Counter:</label>
                <div class="controls">
                    <img src="<?php echo base_url() . 'tickers/ticker_' . $code . '.png?'.time(); ?>" />
                </div>
            </div>
			<div class="control-group">
						<label class="control-label">Use this URL to link to your counter:</label>
						<div class="controls">
							<a id="copytext" style="background: transparent none repeat scroll 0% 0% ! important;" target="_blank" href="<?php echo base_url() . 'tickers/ticker_' . $code . '.png'; ?>"><?php echo base_url() . 'tickers/ticker_' . $code . '.png'; ?></a></br>
							<button type="button" class="btn btn-info btn-xs" onClick="copyToClipboard('copytext');">Copy URL</button>

						</div>
			</div>
			<?php  if(file_exists($_SERVER["DOCUMENT_ROOT"].'/panacea/tickers/cfd-tickers/cfd_green_ticker_' . $code . '.png')){ ?>
			<div class="control-group">
                <label class="control-label">Your Tree Counter(Green):</label>
                <div class="controls">
                    <img src="<?php echo base_url() . 'tickers/cfd-tickers/cfd_green_ticker_' . $code . '.png?'.time(); ?>" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Your Tree Counter(Black):</label>
                <div class="controls">
                    <img src="<?php echo base_url() . 'tickers/cfd-tickers/cfd_black_ticker_' . $code . '.png?'.time(); ?>" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Your Tree Counter(White):</label>
                <div class="controls">
                    <img style="background: #eee;" src="<?php echo base_url() . 'tickers/cfd-tickers/cfd_white_ticker_' . $code . '.png?'.time(); ?>" />
                </div>
            </div>
			<?php  } ?>
			<div class="control-group">
						<label class="control-label">Use this URL to link to your certificate(Full):</label>
						<div class="controls">
							<a id="copyfullimg" style="background: transparent none repeat scroll 0% 0% ! important;" target="_blank" href="<?php echo base_url() . 'cert/full_' . $code . '.jpg'; ?>"><?php echo base_url() . 'cert/full_' . $code . '.jpg'; ?></a></br>
							<button type="button" class="btn btn-info btn-xs" onClick="copyToClipboard('copyfullimg');">Copy URL</button>

						</div>
			</div>
			<div class="control-group">
						<label class="control-label">Use this URL to link to your certificate(Thumbnail):</label>
						<div class="controls">
							<a id="copythumbimg" style="background: transparent none repeat scroll 0% 0% ! important;" target="_blank" href="<?php echo base_url() . 'cert/thumb_' . $code . '.jpg'; ?>"><?php echo base_url() . 'cert/thumb_' . $code . '.jpg'; ?></a></br>
							<button type="button" class="btn btn-info btn-xs" onClick="copyToClipboard('copythumbimg');">Copy URL</button>

						</div>
			</div>
            <!---->
            <?php /* ?><div class="control-group">
              <label class="control-label">First Name: (Solicitor)</label>
              <div class="controls">
              <input type="text" value="<?php echo $first_name2; ?>" name="first_name2" />
              </div>
              </div>
              <div class="control-group">
              <label class="control-label">Email: (Solicitor)</label>
              <div class="controls">
              <input type="text" value="<?php echo $email2; ?>" name="email2" />
              </div>
              </div>
              <hr /><?php */ ?>
            <!----->

            <div class="control-group">
                <label class="control-label">Company Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $company; ?>" name="company" id="company" />
                </div>
            </div>
            <div class="control-group">
				<label class="control-label">Company Logo:</label>
				<div class="controls">
					<?php if ($logo) echo "<img id='result' width='200' src='" . base_url() . "uploads/company/{$logo}?". time() ."' />"; ?>
					<input id="file"  type="file" name="logo" accept="image/*" />
					<input type="hidden" id="logo-image"  name="logo-image" />
					<!--img id="result"-->
				</div>
		   </div>
            <div class="control-group">
                <label class="control-label">Website:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $website; ?>" name="website" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">First Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $first_name; ?>" name="first_name"  id="first_name"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Last Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $last_name; ?>" name="last_name" id="last_name"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Email: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $email; ?>" name="email" id="email"/>
                </div>
            </div>
            <!--div class="control-group">
                <label class="control-label">Username: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $username; ?>" name="username" id="username"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password:</label>
                <div class="controls">
                    <input type="password" value="" name="password" id="password"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password Conf:</label>
                <div class="controls">
                    <input type="password" value="" name="password2" />
                </div>
            </div-->
            <div class="control-group">
                <label class="control-label">Phone:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $phone ?>" name="phone" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Address: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $address ?>" name="address" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">City: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $city ?>" name="city" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Area: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $state ?>" name="state" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Post Code: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo $post_code ?>" name="post_code" />
                </div>
            </div>
			<?php if ($_SESSION['login']['type'] == 'intitiative_client') { ?>
			<div class="control-group">
                <label class="control-label">Twitter Handle:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $twitter ?>" name="twitter" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Facebook Page:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $facebook ?>" name="facebook" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Instagram User:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $instagram ?>" name="instagram" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Pinterest User:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $pinterest ?>" name="pinterest" />
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Linkedin Profile:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $linkedin ?>" name="linkedin" />
                </div>
            </div>
			
			<?php } ?>
            <?php if ($_SESSION['login']['type'] == 'admin') : ?>
                <div class="control-group">
                    <label class="control-label"></label>
                    <div class="controls">
                        <h4>Only For Admin</h4>
                    </div>
                </div>
				<div class="control-group">
					<label class="control-label">Dashboard User: </label>
					<div class="controls">
						<input type="text" value="<?php echo $dashboard_user_email; ?>" name="dashboard_user_email"  id="dashboard_user_email"/>
					</div>
				</div>
				<div class="control-group">
                <label class="control-label">Sender Email:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $sender_email; ?>" name="sender_email" id="sender_email" />
                </div>
				</div>
				<div class="control-group">
                <label class="control-label">Reply-to Email:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $replyto_email; ?>" name="replyto_email" id="replyto_email" />
                </div>
				</div>
				<div class="control-group">
                <label class="control-label">Tech Support Email:</label>
                <div class="controls">
                    <input type="text" value="<?php echo $tech_support_email; ?>" name="tech_support_email" id="tech_support_email" />
                </div>
				</div>
				<div class="control-group">
					<label class="control-label">Counter Type: </label>
					<div class="controls">
						<select name="counter_type">
							 <option <?php echo $countertype= ($counter_type=='0')? "selected" : "";  ?>  value="0">Select</option>
							 <option <?php echo $countertype= ($counter_type=='1')? "selected" : "";  ?>  value="1">Green Earth Appeal</option>
							 <option <?php echo $countertype= ($counter_type=='2')? "selected" : "";  ?> value="2">Carbon Free Dining</option>
							 <option <?php echo $countertype= ($counter_type=='3')? "selected" : "";  ?> value="3">Non Partner</option>
						</select>
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label">HubSpot Company ID:</label>
                    <div class="controls">
                        <input type="text" value="<?php echo $hs_company_id; ?>" name="hs_company_id" />
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label">HubSpot Deal ID:</label>
                    <div class="controls">
                        <input type="text" value="<?php echo $hs_deal_id; ?>" name="hs_deal_id" />
                    </div>
                </div>
				<div class="control-group">
					<label class="control-label">Upload certificate Image : </label>
					<div class="controls">
						<?php if ($partner_bk_image) echo "<img width='200' src='" . base_url() . "libraries/imgs/{$partner_bk_image}?". time() ."' />"; ?>
							 <input type="file" name="logo" accept="image/*" />
					</div>
				</div>
				<div class="control-group">
                    <label class="control-label"> No.of restaurants under partner:</label>
                    <div class="controls">
                        <select name="no_of_restaurants">
                            <option value="1">--Select--</option>
                            <option value="1" <?php if ($no_of_restaurants == 1) echo "selected='selected'" ?>>Single</option>
                            <option value="2" <?php if ($no_of_restaurants == 2) echo "selected='selected'" ?>>Multiple</option>

                        </select>
                    </div>
                </div>
				 <div class="control-group">
                    <label class="control-label">Attach certificate in emails</label>
                    <div class="controls">
                        <label class="radio inline">
                            <input type="radio" name="attach_cert_inemails" value="1" <?php echo ($attach_cert_inemails == 1) ? 'checked' : ''; ?> /> Yes
                        </label>
                        <label class="radio inline">
                            <input type="radio" name="attach_cert_inemails" value="0" <?php echo ($attach_cert_inemails == 0) ? 'checked' : ''; ?> /> No
                        </label>
                   </div>
                </div>
                <div class="control-group">
                    <label class="control-label">When will emails be sent?</label>
                    <div class="controls">
                        <label class="radio inline">
                            <input type="radio" name="send_email_immediately" value="1" <?php echo ($send_email_immediately == 1) ? 'checked' : ''; ?> /> Immediately
                        </label>
                        <label class="radio inline">
                            <input type="radio" name="send_email_immediately" value="0" <?php echo ($send_email_immediately == 0) ? 'checked' : ''; ?> /> Following morning
                        </label>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Tree number (*):</label>
                    <div class="controls">
                        <input type="text" value="<?php echo $tree_nums; ?>" name="tree_nums" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Active:</label>
                    <div class="controls">
                        <label class="radio inline">
                            <input type="radio" value="1" name="active" <?php echo ($active == 1) ? 'checked' : ''; ?> /> Active
                        </label>
                        <label class="radio inline">
                            <input type="radio" value="0" name="active" <?php echo ($active == 0) ? 'checked' : ''; ?> /> Inactive
                        </label>
                    </div>
                </div>

                <!---->
                <div class="control-group">
                    <label class="control-label">Cost per tree (*):</label>
                    <div class="controls">
                        <input type="text" style="width: 50px;" name="price" value="<?php echo $price; ?>" />
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label">Sustainable Credit Price:</label>
                    <div class="controls">
                        <input type="text" style="width: 50px;" name="suscredit_price" value="<?php echo $suscredit_price; ?>" />
						<?php if ($currency == 2) echo "Pence"; ?>
						<?php if ($currency == 3) echo "Cents"; ?>
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label">Delay payment by:</label>
                    <div class="controls">
                        <input type="number" style="width: 50px;" name="delay_payment_by" value="<?php echo $delay_payment_by; ?>" /> days
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Currency (*):</label>
                    <div class="controls">
                        <select name="currency">
                            <option value="">--Select--</option>
                            <option value="1" <?php if ($currency == 1) echo "selected='selected'" ?>>USD</option>
                            <option value="2" <?php if ($currency == 2) echo "selected='selected'" ?>>GBP</option>
                            <option value="3" <?php if ($currency == 3) echo "selected='selected'" ?>>EUR</option>

                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Tax (*):</label>
                    <div class="controls">
                        <input type="text" style="width: 50px;" name="tax" value="<?php echo $tax; ?>" /> %
                    </div>
                </div>
                <!------>
                <p class="controls text-info">Fields for email template is sent to initiative partner</p>
                <div class="control-group">
                    <label class="control-label">Subject (*):</label>
                    <div class="controls">
                        <input type="text" style="width: 100%;" name="subject" value="<?php echo $subject; ?>" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">CC :</label>
                    <div class="controls">
                        <input type="text" style="width: 100%;" name="cc_email" value="<?php echo $cc_email ?>" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Message (*):</label>
                    <div class="controls">
                        <textarea style="width: 100%; height:200px;" name="message" id="message"><?php echo $message; ?></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        Text on Certificate (*):<br/>
                        use:<br/>
                        [name]<br/>
                        [total_trees]<br/>
                        [initiative]
                    </label>
                    <div class="controls">
                        <textarea style="width: 100%; height:200px;" name="certificate_text"><?php echo $certificate_text; ?></textarea>
                    </div>
                </div>
            <?php endif; ?>
            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-success" type="submit" name="edit_intitiative_partner">Save</button>
                    <a href="<?php echo site_url('user/index'); ?>" class="btn btn-warning">Cancel</a>
                    <a href="javascript:void(0);" class="btn btn-info" id="dialog-link">Test</a>
                </div>
            </div>
        </fieldset>
    </form>
	<hr />
		<div class="control-group">
		<label class="control-label"></label>
		<div class="controls">
			<h4>Change password:</h4>
		</div>
	</div>
		<form accept-charset="utf-8" method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo site_url("user/edit_intitiative_partner/$id") ?>">
				<div class="control-group">
					<label class="control-label">Username: (*)</label>
					<div class="controls">
						<input type="text" value="<?php echo $username; ?>" name="username" id="username"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Password: (*)</label>
					<div class="controls">
						<input type="password" value="" name="password" id="password" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Password Conf: (*)</label>
					<div class="controls">
						<input type="password" value="" name="password2" required />
					</div>
				</div>
			
			   <div class="control-group">
					<div class="controls">
					<button class="btn btn-success" type="submit" name="update_partner_password">Update Password</button>
					</div>
				</div>
		 </form>
				
		
		<?php if(!empty($api_result)) { ?>
			 <h5>API configuration</h5>
			 <form accept-charset="utf-8" method="POST" class="form-horizontal" id="update_api_form" enctype="multipart/form-data" action="<?php echo site_url("user/update_ls_api_credentials/$id") ?>">
			  <fieldset>
			 <div class="control-group">
				<label class="control-label">Company ID: (*)</label>
				<div class="controls">
					<input value="<?php echo $api_result['2']; ?>" name="ls_api_company_id" id="ls_api_company_id" type="number" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Master ID: (*)</label>
				<div class="controls">
					<input value="<?php echo $api_result['3']; ?>" name="ls_api_master_id" id="ls_api_master_id" type="number" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Username: (*)</label>
				<div class="controls">
					<input value="<?php echo $api_result['5']; ?>" name="ls_api_username" id="ls_api_username" autocomplete="off" type="text" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Password: (*)</label>
				<div class="controls">
					<input value="<?php echo $api_result['6']; ?>" name="ls_api_password" id="ls_api_password" type="text" autocomplete="off" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Server: (*)</label>
				<div class="controls">
					<input value="<?php echo $api_result['7']; ?>" name="ls_api_server" id="ls_api_server" type="text" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Product ID: (*)</label>
				<div class="controls">
					<input value="<?php echo $api_result['9']; ?>" name="ls_api_product_id" id="ls_api_product_id" type="text" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Active: (*)</label>
				<div class="controls">
					<select name="api_active">
						 <option <?php echo $sel= ($api_result['10']=='0')? "selected" : "";  ?> value="0">Disabled</option>
						 <option <?php echo $sel= ($api_result['10']=='1')? "selected" : "";  ?> value="1">Enabled</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Sustainable Credit Partner: (*)</label>
				<div class="controls"> 
					<select name="api_is_suscredit">
						<option <?php echo $sel= ($api_result['11']=='1')? "selected" : "";  ?> value="1">Yes</option>
						<option <?php echo $sel= ($api_result['11']=='0')? "selected" : "";  ?> value="0">No</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-success" type="submit" name="update_api_save">Update API</button>
				</div>
			</div>
			 </fieldset>
		</form>
	<?php } ?>
</div>
<div id="dialog" title="Email Preview">

</div>
<?php if(empty($api_result)) { ?>
<div class="modal fade" id="partner_add_api">
	<div class="modal-dialog modal-sm">
	  <div class="modal-content">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <h4 class="modal-title text-center">Add to API</h4>
		</div>
		<div class="modal-body">
		<form accept-charset="utf-8" method="POST" class="form-horizontal" id="add_api_form" enctype="multipart/form-data" action="">
			<fieldset>
				<div class="control-group">
					<label class="control-label">Company ID: (*)</label>
					<div class="controls">
						<input value="" name="api_company_id" id="company_id" type="number" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Master ID: (*)</label>
					<div class="controls">
						<input value="" name="master_id" id="master_id" type="number" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Username: (*)</label>
					<div class="controls">
						<input value="" name="api_username" id="api_username" autocomplete="off" type="text" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Password: (*)</label>
					<div class="controls">
						<input value="" name="api_password" id="api_password" type="text" autocomplete="off" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Server: (*)</label>
					<div class="controls">
						<input value="" name="api_server" id="api_server" type="text" required/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Product ID: (*)</label>
					<div class="controls">
						<input value="" name="api_product_id" id="api_product_id" type="text" required/>
					</div>
				</div>
				<input type="hidden" value="<?php echo $company; ?>" name="api_company_name" id="api_company_name" >
				<input type="hidden" value="<?php echo $id; ?>" name="pan_id" id="pan_id" >
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-success" type="submit" name="add_api_save">Save</button>
					</div>
				</div>
			</fieldset>
		</form>
		</div>
		<div class="modal-footer">
			<div class="text-center" style="color:green;" id="api_msg">
			</div>
		</div>
	  </div>
	</div>
</div>
<?php } ?>
<style>	
#partner_add_api.fade{
	display:none;
}		
</style>			

<script  src="https://code.jquery.com/jquery-1.11.3.min.js"  type="text/javascript" ></script>
<script  src="<?php echo base_url(); ?>js/jquery-ui.js"  type="text/javascript" /></script>
<link href="<?php echo base_url(); ?>css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


<script type="text/javascript">
	function copyToClipboard(elementId) {
	 var aux = document.createElement("input");
	 aux.setAttribute("value", document.getElementById(elementId).innerHTML);
	 document.body.appendChild(aux)
	 aux.select();
	 document.execCommand("copy");
	 document.body.removeChild(aux);
	 //alert('Link copied.');
}
    $("#dialog").dialog({autoOpen: false, draggable: true, width: 800, maxHeight: 350});
    $("#dialog-link").click(function (event) {
        var first = $("#first_name").val();
        var last_name = $("#last_name").val();
        var email = $("#email").val();
        var username = $("#username").val();
        var password = $("#password").val();
        var name = $("#company").val();
        var message = $("#message").val().toString();
        /*var mapObj = {"[first_name]":first_name};
         var message2 = replaceAll(message, mapObj);*/

        message = message.replace("[First_Name]", first);
        message = message.replace('[Email]', email);
        message = message.replace('[username]', username);
        message = message.replace('[Name]', name);

        $("#dialog").html(message);
        /*$.ajax({
         url:'<?php echo site_url('user/preview_template'); ?>',
         method: 'post',
         data: {'first_name':first_name, 'company':name, 'email':email, 'username':username, 'password':password, 'tempid': 11}		
         }).done(function(data) {
         $("#dialog").html(data);
         });
         */

        $("#dialog").dialog("open");
        event.preventDefault();
    });

    /*	function replaceAll(str,mapObj){
     var re = new RegExp(Object.keys(mapObj).join("|"),"gi");
     
     return str.replace(re, function(matched){
     return mapObj[matched];
     });
     }*/
	jQuery("#dialog_partner_add_api").click(function(){
		jQuery("#partner_add_api").modal("show");
	});
	 jQuery( "#add_api_form" ).submit(function( e ) {
		var ajaxurl = '<?php echo site_url('user/add_lightspeed_api_partner_credentials/');?>';
		e.preventDefault();
		  $.ajax({
				url: ajaxurl,
				type: "post",
				data: $(this).serialize(),
				success: function(data) {
					jQuery("#api_msg").html('Record added successfuly!');
					setTimeout(function(){ 
						jQuery("#partner_add_api").modal("hide");
					}, 3000);
				}
			});
		e.preventDefault();
	});
</script>