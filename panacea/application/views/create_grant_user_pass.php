<div class="container">
    <div class="row-fluid">
        <div class="span3 offset4">
            <form method="POST" class="form-inline">
                <fieldset>
                    <legend class="text-center">Create Password</legend>
                    <?php
                        echo validation_errors();
                        if (isset($error)) echo $error;
                    ?>
                </fieldset>
            </form>
        </div>
        <div class="clearfix"></div>
        
    </div>
</div>