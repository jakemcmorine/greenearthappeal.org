<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="">
        <fieldset>
            <legend class="text-center"><?php echo $title;?></legend>
            <div class="control-group">
            <?php
                $str = '';
                if ($has_figure && !isset($_SESSION['login']['from_admin']) ) {
                    echo '<p>Thank you for submitting your <b>'.$prev_month.'</b> tree figures. We have sent you an invoice for last month\'s trees as well as a certificate for the total number of trees you have financed through The Green Earth Appeal.
                        </p><p>Thank you for your ongoing support.</p>';
                    $str = 'disabled="true"';
                } else {
                    echo 'Hello <b>' .$restaurant. '</b>, please confirm the number of trees you wish to plant for <b>' .$prev_month. '</b>';
                }
            ?>
            </div>
            <div class="controls">
                <?php
                    echo '<div class="text-error">'.validation_errors().'</div>';
                    if (isset($error)) echo '<div class="text-error">'.$error.'</div>';
                ?>
            </div>
			<div class="control-group">
                <label class="control-label">Your Tree Number(*):</label>
                <div class="controls">
                    <input type="text" <?php echo $str;?> name="trees" value="<?php set_value('trees');?>" />
                </div>
            </div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-success" <?php echo $str;?>  type="submit" name="add_a_figure">Submit</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>