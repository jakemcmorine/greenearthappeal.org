<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="">
        <fieldset>
            <legend class="text-center">Add a restaurant</legend>
            <div class="controls">
                <?php echo validation_errors(); ?>
            </div>
            <div class="control-group">
                <label class="control-label">Initiative: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('initiative'); ?>" name="initiative" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Opportunity String: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('opportunity_str'); ?>" name="opportunity_str" placeholder="gives diners the opportunity to:" />
                </div>
            </div>
            
            <div class="control-group">
                <label class="control-label">Restaurant Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('restaurant'); ?>" name="restaurant" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Website:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('website'); ?>" name="website" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Referer ID: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('code'); ?>" name="code" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">First Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('first_name'); ?>" name="first_name" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Last Name:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('last_name'); ?>" name="last_name" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Email: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('email'); ?>" name="email" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Username: (*)</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('username'); ?>" name="username" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password: (*)</label>
                <div class="controls">
                    <input type="password" value="" name="password" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Password Conf: (*)</label>
                <div class="controls">
                    <input type="password" value="" name="password2" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Phone:</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('phone'); ?>" name="phone" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Tree number (*):</label>
                <div class="controls">
                    <input type="text" value="<?php echo set_value('tree_nums'); ?>" name="tree_nums" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Cost per tree (*):</label>
                <div class="controls">
                    <input type="text" style="width: 50px;" name="price" value="<?php echo set_value('price'); ?>" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"> Currency (*):</label>
                <div class="controls">
                    <select name="currency">
                        <option value="">--Select--</option>
                        <option value="1" <?php if (set_value('currency') == 1) echo "selected='selected'" ?>>cent</option>
                        <option value="2" <?php if (set_value('currency') == 2) echo "selected='selected'" ?>>pence</option>

                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Tax (*):</label>
                <div class="controls">
                    <input type="text" style="width: 50px;" name="tax" value="<?php echo set_value('tax'); ?>" /> %
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Is new restaurant: (*)</label>
                <div class="controls">
                    <label class="radio inline">
                        <input type="radio" value="1" name="new" <?php if (isset($_POST['new'])) echo ($_POST['new'] == 1) ? 'checked' : ''; ?> /> New
                    </label>
                    <label class="radio inline">
                        <input type="radio" value="0" name="new" <?php if (isset($_POST['new'])) echo ($_POST['new'] == 0) ? 'checked' : ''; ?> /> Exists
                    </label>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-success" type="submit" name="add_restaurant">Save</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>