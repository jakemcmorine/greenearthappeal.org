<!DOCTYPE html>
<html>
<head>
    <meta name="author" content="johnn"/>
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>img/favicon.png"/>
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!--link href="https://www.greenearthappeal.org/plugins/system/jat3/jat3/base-themes/default/images/favicon.ico"
          rel="shortcut icon" type="image/x-icon"/-->

    <script src="<?php echo base_url(); ?>/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url(); ?>/js/bootstrap.min.js"></script>
    <title><?php echo $title; ?></title>
    <style>
        #pct_header, #pct_footer {
            padding: 10px;
            background: gray;
        }
        a{
            color: #146734
        }
		.btn-small {
		  padding: 2px 9px!important;
		}
    </style>
</head>

<body style="padding-top: 10px;">
<div id="cover" style="display:none;"></div>
<div class="container">
    <div id="header">
        <div class="row-fluid">
            <div class="pull-left">
                <a href="<?php echo site_url(); ?>">
                    <img alt="logo" src="https://www.greenearthappeal.org/the_sonne/images/Partners/logo-trans.png"/>
                </a>
            </div>
            <div class="pull-right">
			<?php //echo "<pre>"; print_r($_SESSION['login']); die; 
				echo "<br /><br />";
			if ($_SESSION['login']['type'] == 'affiliate') :
                        echo '<a href="' . site_url('user/affiliate_index') . '" style="float: left; margin-right: 6px;" class="btn btn-danger">Home</a> ';
                    else:
                        echo '<a href="' . base_url() . '" style="float: left; margin-right: 6px;" class="btn btn-danger">Home</a> ';
                    endif;  
				if ($_SESSION['login']['type'] == 'initiative_editor'){ ?>
				 <a href="<?php echo site_url('user/initiative_editor_invoices') ?>" class="btn btn-success">Invoices History</a>
				 <a href="<?php echo site_url('user/add_restaurant') ?>" class="btn btn-info">Add Restaurant </a>
				 <a href="<?php echo site_url('user/add_initiative') ?>" class="btn btn-info">Add Initiative </a>
				 <a href="<?php echo site_url('user/add_initiative_partner') ?>" class="btn btn-info">Add Initiative Partner</a>
				 <a href="<?php echo site_url('user/initiative_list') ?>" class="btn btn-info">Initiative List</a>
				<?php }
			if ($_SESSION['login']['type'] == 'admin' || $_SESSION['login']['type'] == 'manager'){ ?>
			  <div class="dropdown" style="float: left; margin-right: 6px;">
				<button class="btn btn-info dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">Add
				<span class="caret"></span></button>
				<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
				 <?php if ($_SESSION['login']['type'] == 'admin' || $_SESSION['login']['type'] == 'manager') : ?>
					  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('user/add_affiliate') ?>">Affiliate</a></li>
					   <li class="dropdown-submenu">
						<a role="menuitem" tabindex="-1" href="<?php echo site_url('user/add_initiative'); ?>">Initiative</span></a>
						<ul class="dropdown-menu">
						  <li> <a href="<?php echo site_url('user/initiative_list') ?>">Initiative List</a></li>
						</ul>
					  </li>
					  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('user/add_initiative_partner'); ?>">Initiative Partner</a></li>
					  <!--li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('user/list_grant_users'); ?>">Grant Applications</a></li-->
					   <li class="dropdown-submenu">
						<a role="menuitem" tabindex="-1" href="<?php echo site_url('user/list_grant_users'); ?>">Grant Applications</span></a>
						<ul class="dropdown-menu">
						  <li> <a href="<?php echo site_url('user/grant_email_templates') ?>">Grant Email Templates</a></li>
						</ul>
					  </li>
					  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('user/add_bulk_partner'); ?>">Bulk Partner</a></li>
					  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('user/add_restaurant'); ?>">Restaurant</a></li>
					  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('user/add_editor'); ?>">Editor</a></li>  
					  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('user/add_initiative_editor'); ?>">Initiative Editor</a></li>  					  
                <?php endif; ?>
				 <?php if ($_SESSION['login']['type'] == 'admin') : ?>
				  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('user/add_solicitor'); ?>">Solicitor</a></li>
				  <li class="dropdown-submenu">
					<a role="menuitem" tabindex="-1" href="<?php echo site_url('user/add_corporate_partner'); ?>">Corporate Partner</span></a>
					<ul class="dropdown-menu">
					  <li> <a href="<?php echo site_url('user/corporate_partner_list') ?>">Corporate Partner List</a></li>
					
					</ul>
				  </li>
				   <li class="dropdown-submenu">
					<a role="menuitem" tabindex="-1" href="<?php echo site_url('user/add_toplevel_initiative'); ?>">Top Level Initiative</span></a>
					<ul class="dropdown-menu">
					  <li> <a href="<?php echo site_url('user/top_level_initiative_list') ?>">Top Level Initiative List</a></li>
					</ul>
				  </li>
				  <li class="dropdown-submenu">
					<a role="menuitem" tabindex="-1" href="<?php echo site_url('user/add_sublevel_initiative'); ?>">Sub Level Initiative</span></a>
					<ul class="dropdown-menu">
					  <li> <a href="<?php echo site_url('user/sub_level_initiative_list') ?>">Sub Level Initiative List</a></li>
					</ul>
				  </li>
				  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('user/create_invoice_report'); ?>">Create Report</a></li>
				  <li class="dropdown-submenu">
						<a role="menuitem" tabindex="-1" href="#">EU Ambassador Emails</span></a>
						<ul class="dropdown-menu">
						  <li> <a href="<?php echo site_url('euambassador/email_templates'); ?>">EU Ambassador Emails</a></li>
						  <li> <a href="<?php echo site_url('euambassador/front_of_house_emails') ?>">Front of House Emails</a></li>
						</ul>
					</li>
				   <li class="dropdown-submenu">
						<a role="menuitem" tabindex="-1" href="#">JS3 Global Emails</span></a>
						<ul class="dropdown-menu">
						  <li> <a href="<?php echo site_url('user/cv_thankyou_email/') ?>">CV Thankyou Email</a></li>
						  <li> <a href="<?php echo site_url('user/global_email_template/1A') ?>">Set 1A</a></li>
						  <li> <a href="<?php echo site_url('user/global_email_template/1B') ?>">Set 1B</a></li>
						  <li> <a href="<?php echo site_url('user/global_email_template/2A') ?>">Set 2A</a></li>
						  <li> <a href="<?php echo site_url('user/global_email_template/2B') ?>">Set 2B</a></li>
						</ul>
					</li>
					<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('user/all_counters'); ?>">Counters</a></li>
                <?php endif; ?>
				</ul>
			  </div>
			<?php } ?>
                <?php /* if ($_SESSION['login']['type'] == 'admin' || $_SESSION['login']['type'] == 'manager') : ?>
                    <a href="<?php echo site_url('user/add_affiliate') ?>" class="btn btn-info">Add Affiliate</a>
                    <a href="<?php echo site_url('user/add_bulk_partner'); ?>" class="btn btn-info">Add a Bulk Partner</a>
                    <a href="<?php echo site_url('user/add_restaurant'); ?>" class="btn btn-info">Add a Restaurant</a>
                    <a href="<?php echo site_url('user/add_editor'); ?>" class="btn btn-info">Add a Editor</a>
                <?php endif; */ ?>
                <?php if ($_SESSION['login']['type'] == 'admin') : ?>
                    <a href="<?php echo site_url('user/manage_ref'); ?>" class="btn btn-primary">Manage ref</a>
                    <a href="<?php echo site_url('user/manage_refid'); ?>" class="btn btn-primary">Manage refid</a>
                <?php endif; ?>
                <!------>
                <?php if ($_SESSION['login']['type'] == 'bulk_partner') : ?>
                    <a href="<?php echo str_replace('/index.php', '', site_url('uploads/sample-csv/sample_csv.csv')); ?>"
                       class="btn btn-warning">Download a sample csv</a>
                    <a href="<?php echo site_url('user/edit_bulk_partner') ?>" class="btn btn-info">Edit your
                        account</a>
                    <a href="<?php echo site_url('user/add_clients') ?>" class="btn btn-info">Add Clients</a>
                    <a href="<?php echo site_url('user/clients_list') ?>" class="btn btn-info">List of Clients</a>
                <?php endif; ?>
                <!-------->
				 <!------>
                <?php if ($_SESSION['login']['type'] == 'intitiative') : ?>
                    <!--a href="<?php echo str_replace('/index.php', '', site_url('uploads/sample-csv/inititiative_sample_csv.csv')); ?>"
                       class="btn btn-warning">Download a sample csv</a-->
                    <!--a href="<?php echo site_url('user/edit_intitiative_partner') ?>" class="btn btn-info">Edit your
                        account</a-->
						 <a href="<?php echo site_url('user/initiative_partner_invoices') ?>" class="btn btn-success">Invoices History</a>
						 <a href="<?php echo site_url('user/edit_intitiative_partners') ?>" class="btn btn-info">Edit your
                        account</a>
						
                    <a href="<?php echo site_url('user/add_intitiative_clients') ?>" class="btn btn-info">Add Clients</a>
                    <a href="<?php echo site_url('user/intitiative_clients_list') ?>" class="btn btn-info">List of Clients</a>
                <?php endif; ?>
                <!-------->
                <?php if ($_SESSION['login']['type'] == 'restaurant') : ?>
                    <a href="<?php echo site_url('user/edit_restaurant') ?>" class="btn btn-info">Edit your account</a>
                    <a href="<?php echo site_url('user/arcive_figures'); ?>" class="btn btn-info">Previous Months
                        Figures</a>
                <?php endif; ?>
                <?php if ($_SESSION['login']) {
                   // echo "<br /><br />"; ?>
                    <?php if ($_SESSION['login']['type'] == 'admin') : ?>
                        <a href="<?php echo site_url('user/invoices') ?>" class="btn btn-success">Invoices History</a>
                    <?php endif;
                    if ($_SESSION['login']['type'] == 'admin' || $_SESSION['login']['type'] == 'manager') : ?>
                        <a href="<?php echo site_url('user/email_templates'); ?>" class="btn btn-warning">Email
                            Templates</a>
                        <a href="<?php echo site_url('user/affiliates_list'); ?>" class="btn btn-danger">Affiliates
                            List</a>
                        <a href="<?php echo site_url('user/track_list'); ?>" class="btn btn-danger">Affiliates
                            Tracking</a>
                    <?php endif;
                    
                    echo '<a href="' . site_url('user/logout') . '" class="btn btn-danger">Logout</a>';
                }
                ?>
                <?php if ($_SESSION['login']['type'] == 'restaurant') : ?>
                    <a href="<?php echo base_url("uploads/certs/" . $this->data['latest_certificate_url']); ?>"
                       class="btn btn-info" target="_blank"><img src="<?php echo base_url('img/dlc.png'); ?>"
                                                                 Title="Download Latest Certificate"
                                                                 alt="Download Latest Certificate"
                                                                 style="height:20px; width:20px;"> Latest
                        Certificate</a>
                <?php endif; ?>

                <?php if ($_SESSION['login']['type'] == 'bulk_partner') : ?>
                    <a href="<?php echo site_url('user/bulk_partner_figures'); ?>" class="btn btn-info">Previous Months
                        Figures</a>
                    <a href="<?php echo base_url("uploads/certs/" . $this->data['latest_certificate_url']); ?>"
                       class="btn btn-info" target="_blank"><img src="<?php echo base_url('img/dlc.png'); ?>"
                                                                 Title="Download Latest Certificate"
                                                                 alt="Download Latest Certificate"
                                                                 style="height:20px; width:20px;"> Latest
                        Certificate</a>
                <?php endif; ?>
				
				<?php if ($_SESSION['login']['type'] == 'intitiative') : ?>
				</br>
				</br>
				
                    <a href="<?php echo site_url('user/intitiative_partner_figures'); ?>" class="btn btn-info">Previous Months
                        Figures</a>
                    <a href="<?php echo base_url("uploads/certs/" . $this->data['latest_certificate_url']); ?>"
                       class="btn btn-info" target="_blank"><img src="<?php echo base_url('img/dlc.png'); ?>"
                                                                 Title="Download Latest Certificate"
                                                                 alt="Download Latest Certificate"
                                                                 style="height:20px; width:20px;"> Latest
                        Certificate</a>
                <?php endif; ?>
            </div>
            <?php if ($_SESSION['login']['type'] == 'bulk_partner') : ?>
                <div style="float:right; clear:right; margin-top: 30px" class="text-success">To date you have planted
                    <b><?php echo $_SESSION['login']['data']['total_trees']; ?></b> trees
                </div>
            <?php endif; ?>
			 <?php if ($_SESSION['login']['type'] == 'intitiative') : ?>
                <div style="float:right; clear:right; margin-top: 30px" class="text-success">To date you have planted
                    <b><?php echo $_SESSION['login']['data']['total_trees']; ?></b> trees
                </div>
            <?php endif; ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <!--  // body here -->
    <div id="pct_body">
        <?php  echo $this->load->view($body); ?>
        <div class="clearfix"></div>
    </div>
    <div class="call_us">
        <p><img border="0" src="https://www.greenearthappeal.org/the_sonne/images/Partners/call-us.png"
                alt="Sample image"/></p>

        <p class="text-larger">Send us an
            <a href="mailto:website@greenearthappeal.org?subject=I%20was%20on%20your%20website....">email</a> or give us
            a call on <a href="#">020 8798 0476</a>
        </p>
    </div>
    <div id="footer">
        <div class="ja-copyright">
            <small>Copyright &copy; <?php echo date("Y"); ?> Green Earth Appeal. All Rights Reserved.</small>
            <br>
            <small>Site Built By Our Digital Partner <a style="color: orange;" href="http://www.nonoodle.co.uk" target="_blank">nonoodle.co.uk</a>
            </small>
        </div>
    </div>
    <style>
        #footer {
            padding: 30px 0;
            /* background: url("https://www.greenearthappeal.org/templates/ja_puresite/images/bg-body.gif") repeat scroll 0 0 #C9C9C9; */
        }

        #header, #footer {
            color: #656667;
            font-size: 16px;
            text-align: center;
        }

        #header {
            background: none repeat scroll 0 0 #FFFFFF;
            border-bottom: 1px solid #E5E6E7;
            padding-bottom: 10px;
            text-align: left;
        }

        .call_us {
            border-top: 1px solid #E5E6E7;
            font-size: 22px;
            padding: 10px 0;
            text-align: center;
        }
    </style>
</div>

</body>
</html>