<fieldset>
    <legend>Submission List</legend>
    <table class="table table-bordered table-striped">
        <tr>
            <th>ID</th>
            <th width="205"></th>
        </tr>
        <?php
            if (count($rows)) :
                foreach ($rows AS $row):
        ?>
            <tr>
                <td><?php echo $row->id;?></td>
            </tr>
        <?php endforeach; endif;?>
    </table>
</fieldset>