<div class="span7 offset2">
    <form accept-charset="utf-8" method="post" class="form-horizontal" action="<?php echo site_url('user/confirm_figure');?>">
        <fieldset>
            <legend class="text-center"><?php echo $title;?></legend>
            <?php
                switch ( $_SESSION['login']['currency'] ) {
                    case 1:
                        $currency = '$';
                        break;
                    case 2:
                        $currency = '&pound;';
                        break;
                }
                $sub_total = $currency . $sub_total;
                $total_price = $currency . $total;
            ?>
			<div class="control-group">
                <label class="control-label">Total trees during the partnership:</label>
                <div class="controls" style="padding-top: 15px">
                    <?php echo $total_trees;?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Current Trees:</label>
                <div class="controls" style="padding-top:5px">
                    <?php echo $current_trees;?>
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Sub Total:</label>
                <div class="controls" style="padding-top:5px">
                    <?php echo $sub_total;?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Tax:</label>
                <div class="controls" style="padding-top:5px">
                    <?php echo $tax. '%';?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Total:</label>
                <div class="controls" style="padding-top:5px">
                    <?php echo $total_price;?>
                </div>
            </div>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-success" type="submit" name="confirm">Confirm</button>
                    <a class="btn btn-warning" href="<?php echo site_url('user/cancel_figure');?>">Cancel</a>
				</div>
			</div>
		</fieldset>
	</form>
</div>