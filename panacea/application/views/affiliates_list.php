<fieldset>
    <legend>Affiliates List</legend>
    <table class="table table-bordered table-striped">
        <tr>
            <th>ID</th>
            <th>Email</th>
            <th>Username</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Type</th>
            <th>Rate</th>
            <th>Unique code</th>
            <th width="205"></th>
        </tr>
        <?php
            if (count($rows)) :
                foreach ($rows AS $row):
					$url_edit = site_url('user/edit_affiliate/'.$row->id);
					$url_delete = site_url('user/delete_affiliate/'.$row->id);
        ?>
            <tr>
                <td><?php echo $row->id;?></td>
                <td><?php echo $row->email;?></td>
                <td><?php echo $row->username;?></td>
                <td><?php echo $row->first_name;?></td>
                <td><?php echo $row->last_name;?></td>
                <td><?php echo ucfirst($row->type);?></td>
                <td><?php echo $row->rate;?></td>
                <td><?php echo $row->code;?></td>
                <td>
                    <a class="btn btn-info btn-small" href="<?php echo $url_edit;?>">Edit</a>
                    <a class="btn btn-warning btn-small" href="<?php echo $url_delete;?>">Delete</a>
                </td>
            </tr>
        <?php endforeach; endif;?>
    </table>
</fieldset>