<fieldset>
    <legend><?php echo $title;?></legend>
    <div>
    <p>Welcome to the Green Earth Appeal Partners Control Panel.</p>
    <p>To date you have planted <b><?php echo $_SESSION['login']['data']['total_trees'];?> trees</b> as a partner of the Green Earth Appeal.</p>
    <p>You can use this dashboard to upload your clients� information and we will:</p>
    <ul>
        <li>Send each client an email which will:
            <ul>
                <li>Confirm that the designated number of trees have been planted on their behalf</li>
                <li>Have a certificate attached</li>
                <li>Have the URL of their unique landing page on our website which will contain your clients� logo
                    (please note that initially the page will show their company name, until they, or you upload their logo) � <b><a href="#">click here</a></b> for an example of
                    <br />a) What it initially will look like
                    <br />b) What it will look like once their logo has been added.
                </li>
                <li>Have username and Password to be able to log in and upload their own logo to our system, for their unique landing page</li>
            </ul>
        </li>
        <li>Send you a certificate for the total number of trees planted through our partnership with you.</li>
    </ul>
    </div>
</fieldset>