<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "user";
$route['reminder_email_cronjob'] = "welcome/reminder_email_cronjob";
$route['send_monthly_invoice'] = "welcome/send_monthly_invoice";
$route['reminder_emails_to_restaurants'] = "welcome/reminder_emails_to_restaurants";
$route['monthly_auto_emails_to_restaurants'] = "welcome/monthly_auto_emails_to_restaurants";
$route['restaurant_invoice_email_on_nextday'] = "welcome/restaurant_invoice_email_on_nextday";
$route['js3_global_automated_email_cronjob'] = "imacro/js3_global_automated_email_cronjob";
$route['js3_global_automated_email_cronjob_new'] = "imacro/js3_global_automated_email_cronjob_new";
$route['404_override'] = '';
$route['delete_client/(:any)'] = 'imacro/delete_client/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */