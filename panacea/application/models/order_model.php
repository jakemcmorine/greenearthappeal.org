<?php
    class Order_model extends CI_Model {
        function __construct() {
            parent::__construct();
            $this->load->database();
        }
      function get_affiliate_list($code){
			// load affiliates 
            $query = "SELECT * FROM pct_affiliate_track WHERE `id` = '".$code."' ";
            $rows1 = $this->db->query($query)->result();
			
            return array_merge($rows1);	
	}  
	public function insert_restaurant($ranCode) {
            $email      = mysql_escape_string($this->input->post('email'));
            $restaurant = mysql_escape_string($this->input->post('restaurant'));
            $first_name = mysql_escape_string($this->input->post('first_name'));
            $last_name  = mysql_escape_string($this->input->post('last_name'));
            $phone      = mysql_escape_string($this->input->post('phone'));
            $tree_nums  = mysql_escape_string($this->input->post('tree_nums'));
            $website    = mysql_escape_string($this->input->post('website'));
            $twitter    = mysql_escape_string($this->input->post('twitter'));
            $facebook    = mysql_escape_string($this->input->post('facebook'));
            $code       = mysql_escape_string($this->input->post('code'));
            
            $data       = array();
            $data['email']      = $email;
            //$data['username']   = $username;
           // $data['password']   = md5($password);
            $data['type']       = 'restaurant';
            $data['active']     = 1;
            $this->db->insert_batch('pct_users', array($data));
            
            $user_id    = $this->db->insert_id();
            $data       = array();
            $data['user_id']        = $user_id;
            $data['website']        = $website;
            $data['twitter']        = $twitter;
            //$data['facebook']        = $facebook;
            //
            $data['restaurant']     = $restaurant;
            $data['first_name']     = $first_name;
            $data['last_name']      = $last_name;
            $data['phone']          = $phone;
            $data['tree_nums']      = $tree_nums;
            $this->db->insert_batch('pct_restaurants', array($data));
            
            // insert referer code
            $data2 = array();
            $data2['code']           = $ranCode;
            $data2['user_id']        = $user_id;
            $this->insert_referer($data2);
	return $user_id;	
        }
	function insert_referer($data) {
            //$data['code'] = str_replace(' ', '-', $data['code']);
            $query = "INSERT INTO pct_referer
                        SET user_id='{$data['user_id']}',
                            code='{$data['code']}'";
            $this->db->query($query);
        }

        
    }
?>
