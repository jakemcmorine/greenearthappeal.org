<?php
class Imacro_model extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	/**
	 * =====================================================================
	* function for Main insert_autoupload_clients
	*/
	function insert_autoupload_clients() {
		
		$first_name 	= mysql_escape_string($this->input->post('first_name'));
		$last_name 		= mysql_escape_string($this->input->post('last_name'));
		$job_title 		= mysql_escape_string($this->input->post('job_title'));
		$company_number = mysql_escape_string($this->input->post('company_number'));
		$city 			= mysql_escape_string($this->input->post('city'));
		$country 		= mysql_escape_string($this->input->post('country'));
		$linkedin_profile_url = mysql_escape_string($this->input->post('linkedin_profile_url'));
		$phone_number 	= mysql_escape_string($this->input->post('phone_number'));
		$website 		= mysql_escape_string($this->input->post('website'));
		$email_address 	= mysql_escape_string($this->input->post('email_address'));
		$company 		= mysql_escape_string($this->input->post('company'));
		$tree_nums 		= mysql_escape_string($this->input->post('tree_nums'));
		$twitter1 		= mysql_escape_string($this->input->post('twitter1'));
		$twitter2 		= mysql_escape_string($this->input->post('twitter2'));
		$twitter3 		= mysql_escape_string($this->input->post('twitter3'));
		$date_birth 	= mysql_escape_string($this->input->post('date_birth'));
		$partner_id 	= mysql_escape_string($this->input->post('partner_id'));
		$unique_key 	= mysql_escape_string($this->input->post('unique_key'));
		$delete_key 	= mysql_escape_string($this->input->post('delete_key'));
		$token 			= mysql_escape_string($this->input->post('token'));
		
		
		if($company_number == "-"){
			$company_number = $this->randomKey(10);   				//to generate random comp number
		}
		if($email_address=="" || $email_address=="-"){
			$random_no = strtolower($this->randomKey(6));
			$email_address ='noemail'.$random_no.'@noemail.com';     //if email address is empty
		}
		// to make company name in first case upper if all the letter are upper
		
		if($company !=""){
			
			$array = explode(' ', $company);
			$count = count($array);
			$i=0;
			foreach ($array as $testcase) {
				if (ctype_upper($testcase)) {
					 $i++;
				}
			} 
			if($count == $i){
			  $company = strtolower($company);
			  $company = ucwords($company);
			} 
		}
		
		$data = array();			
		$data['first_name']  = $first_name;
		$data['last_name']  = $last_name;
		$data['job_title']  = $job_title;
		$data['company_number']  = $company_number;
		$data['city']  = $city;
		$data['country']  = $country;
		$data['linkedin_profile_url']  = $linkedin_profile_url;
		$data['phone_number']  = $phone_number;
		$data['website']  = $website;
		$data['email_address']  = $email_address;
		$data['company']  = $company;
		$data['tree_nums']  = $tree_nums;
		$data['twitter1']  = $twitter1;
		$data['twitter2']  = $twitter2;
		$data['twitter3']  = $twitter3;
		$data['date_birth']  = $date_birth;
		$data['unique_key']  = $unique_key;
		$data['delete_key']  = $delete_key;
		$data['partner_id']  = $partner_id;
		$data['hs_contact_owner_key']  = $token;
		//echo "<pre>"; print_r($data); die("here");
		$insertid = $this->db->insert_batch('pct_imacroData', array($data));    
		return $this->db->insert_id();		 
	}
	
	/**
	 * ============================================================================================================================================
	 * function for Main insert ichanged world client data api
	*/
	function insert_ichangedworld_clients($post_data) {
		
		$first_name 		= mysql_escape_string( $post_data['first_name'] );
		$last_name 			= mysql_escape_string( $post_data['last_name'] );
		$email_address 		= mysql_escape_string( $post_data['email'] );
		$company 			= mysql_escape_string( $post_data['company_name'] );
		$phone_number 		= mysql_escape_string( $post_data['phone'] );
		$tree_nums 			= mysql_escape_string( $post_data['tree_nums'] );
		$unique_key 		= mysql_escape_string( $post_data['unique_key'] );
		$delete_key 		= mysql_escape_string( $post_data['delete_key'] );
		$form_type 			= mysql_escape_string( $post_data['form_type'] );
		$partner_id 		= mysql_escape_string( $post_data['partner_id'] );
				

		$company_number = 	$this->randomKey(10);   				//to generate random comp number

		/* if($email_address=="" || $email_address=="-"){
			$random_no = strtolower($this->randomKey(6));
			$email_address ='noemail'.$random_no.'@noemail.com';     //if email address is empty
		} */
		
		// to make company name in first case upper if all the letter are upper
		if($company !=""){
				$array = explode(' ', $company);
				$count = count($array);
				$i=0;
				 foreach ($array as $testcase) {
					if (ctype_upper($testcase)) {
						 $i++;
					}
				} 
				if($count == $i){
				  $company = strtolower($company);
				  $company = ucwords($company);
				} 
		}
		$data = array();			
		$data['first_name']  		= $first_name;
		$data['last_name']  		= $last_name;
		$data['company_number']  	= $company_number;
		$data['phone_number']  		= $phone_number;
		$data['email_address']  	= $email_address;
		$data['company']  			= $company;
		$data['tree_nums']  		= $tree_nums;
		$data['unique_key']  		= $unique_key;
		$data['delete_key'] 		= $delete_key;
		$data['partner_id']  		= $partner_id;
		$data['form_type']  		= $form_type;
		$data['hs_contact_owner_key']  = "abcde12345";
		//echo "<pre>"; print_r($data); die("here");
			
		$query = 'SELECT * FROM pct_imacroData WHERE email_address = "'.$email_address.'"';
		$res = $this->db->query($query)->row();
		
		if(!empty($res)){
			
			$imacroData['tree_nums']       =	$res->tree_nums + $tree_nums;
			$imacroData['id']   		   =	$res->id;
			$imacroData['company']   	   =	$company;
			$imacroData['phone_number']    =    $phone_number;
			
		    $this->db->update_batch('pct_imacroData', array($imacroData), 'id');	
			return $res->id;	
			
		}else{
			
			$insertid = $this->db->insert_batch('pct_imacroData', array($data));    
			return $this->db->insert_id();		 
		}
		 
	}
	
	/**
	 * ============================================================================================================================================
	* function to insert CFD hubspot form client data api
	*/
	function insert_cfdhs_clients($post_data) {
		
		$first_name 		= mysql_escape_string( $post_data['first_name'] );
		$last_name 			= mysql_escape_string( $post_data['last_name'] );
		$email_address 		= mysql_escape_string( $post_data['email'] );
		$company 			= mysql_escape_string( $post_data['company_name'] );
		$phone_number 		= mysql_escape_string( $post_data['phone'] );
		$tree_nums 			= mysql_escape_string( $post_data['tree_nums'] );
		$unique_key 		= mysql_escape_string( $post_data['unique_key'] );
		$delete_key 		= mysql_escape_string( $post_data['delete_key'] );
		$form_type 			= mysql_escape_string( $post_data['form_type'] );
		$partner_id 		= mysql_escape_string( $post_data['partner_id'] );
				

		$company_number = 	$this->randomKey(10);   				//to generate random comp number

		/* if($email_address=="" || $email_address=="-"){
			$random_no = strtolower($this->randomKey(6));
			$email_address ='noemail'.$random_no.'@noemail.com';     //if email address is empty
		} */
		
		// to make company name in first case upper if all the letter are upper
		if($company !=""){
				$array = explode(' ', $company);
				$count = count($array);
				$i=0;
				 foreach ($array as $testcase) {
					if (ctype_upper($testcase)) {
						 $i++;
					}
				} 
				if($count == $i){
				  $company = strtolower($company);
				  $company = ucwords($company);
				} 
		}
		$data = array();			
		$data['first_name']  		= $first_name;
		$data['last_name']  		= $last_name;
		$data['company_number']  	= $company_number;
		$data['phone_number']  		= $phone_number;
		$data['email_address']  	= $email_address;
		$data['company']  			= $company;
		$data['tree_nums']  		= $tree_nums;
		$data['unique_key']  		= $unique_key;
		$data['delete_key'] 		= $delete_key;
		$data['partner_id']  		= $partner_id;
		$data['form_type']  		= $form_type;
		$data['hs_contact_owner_key']  = "abcde12345";
		//echo "<pre>"; print_r($data); die("here");
			
		$query = 'SELECT * FROM pct_imacroData WHERE email_address = "'.$email_address.'"';
		$res = $this->db->query($query)->row();
		
		if(!empty($res)){
			
			$imacroData['tree_nums']       =	$res->tree_nums + $tree_nums;
			$imacroData['id']   		   =	$res->id;
			$imacroData['first_name']      =	$first_name;
			$imacroData['last_name']   	   =	$last_name;
			$imacroData['company']   	   =	$company;
			$imacroData['phone_number']    =    $phone_number;
			
		    $this->db->update_batch('pct_imacroData', array($imacroData), 'id');	
			return $res->id;	
			
		}else{
			
			$insertid = $this->db->insert_batch('pct_imacroData', array($data));    
			return $this->db->insert_id();		 
		}
		 
	}
	function load_global_email_template($template_id, $set_id) {
            $query = "SELECT * FROM pct_global_email_templates where set_id='".$set_id."' AND template_id='".$template_id."'";
         
            return $this->db->query($query)->row_array();
       }
	function check_names_spaces($name){
		
		$name =  str_replace(" ","-", $name);
		$name =  str_replace(")","", $name);
		$name =  str_replace("(","", $name);
		$name =  ucfirst(strtolower($name));
		return $name;
	}
	
	function check_certurl_names_spaces($name){
		$name =  $this->removeAccents($name); 	// to remove Accents if exists
		$name =  str_replace(" ","-", $name);
		$name =  ucfirst(strtolower($name));
		return $name;
	}
	
	
	function removeAccents($str) { 
	  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
	  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
	  return str_replace($a, $b, $str);
	}
	
	function filter_company_name($input)       
		{
			$input =  preg_replace("/\([^)]+\)/","",$input);

			$search  = array("ltd",".ltd","ltd.","limited",".","(",")");
			$replace = array("","","","","","","");

			$output = str_ireplace($search, $replace, $input);

			$output = trim(preg_replace('/\s+/', ' ', $output));

			$output = explode(" ", $output );

			$upper = $lower = 0;
			$newoutput = array();
			foreach ($output as $word) { 
				  
				if (ctype_upper($word)) { 
					$upper += 1;
					$company =  ucfirst(strtolower($word));
					if(strlen($word) < 4 ){
						$company = $word;
					}
				} else { 
					$lower += 1;
				   $company =  ucfirst(strtolower($word));
				} 
				$newoutput[] = $company;
			}
			return $newoutput = implode(" ", $newoutput );
		}
	
	
	
	/**
	 * ============================================================================================================================================
	* function for New insert_autoupload_clients
	*/
	function new_insert_autoupload_clients() {
		
		$first_name = mysql_escape_string($this->input->post('first_name'));
		$last_name = mysql_escape_string($this->input->post('last_name'));
		$job_title = mysql_escape_string($this->input->post('job_title'));
		$company_number = mysql_escape_string($this->input->post('company_number'));
		$search_id = mysql_escape_string($this->input->post('search_id'));
		$city = mysql_escape_string($this->input->post('city'));
		$country = mysql_escape_string($this->input->post('country'));
		$linkedin_profile_url = mysql_escape_string($this->input->post('linkedin_profile_url'));
		$phone_number = mysql_escape_string($this->input->post('phone_number'));
		$website = mysql_escape_string($this->input->post('website'));
		$email_address = mysql_escape_string($this->input->post('email_address'));
		$company = mysql_escape_string($this->input->post('company'));
		$tree_nums = mysql_escape_string($this->input->post('tree_nums'));
		$twitter1 = mysql_escape_string($this->input->post('twitter1'));
		$twitter2 = mysql_escape_string($this->input->post('twitter2'));
		$twitter3 = mysql_escape_string($this->input->post('twitter3'));
		$date_birth = mysql_escape_string($this->input->post('date_birth'));
		$partner_id = mysql_escape_string($this->input->post('partner_id'));
		$unique_key = mysql_escape_string($this->input->post('unique_key')); 
		$delete_key = mysql_escape_string($this->input->post('delete_key'));
		$create_hs_immediately = $this->input->post('create_hs_immediately');
		$token = trim($this->input->post('token')); 
		      
		$first_name = $this->check_names_spaces($first_name);    
		$last_name  = $this->check_names_spaces($last_name);
		
		$cert_firstname = $this->check_certurl_names_spaces($first_name);
		$cert_lastname  = $this->check_certurl_names_spaces($last_name); 
		
		$certificate_key = strtolower($cert_firstname.'-'.$cert_lastname);
		
        $check_Cert_key = $this->db->query('SELECT * from pct_imacroData WHERE certificate_key="'.$certificate_key.'"')->row_array();
		
		if($check_Cert_key){
			$certificate_key = rand(10,99).'-'.$cert_firstname.'-'.$cert_lastname;     
		}
		
		if($company_number == "-"){
			 $company_number_random = $this->randomKey(10);   				//to generate random comp number
		}
		if($email_address=="" || $email_address=="-"){
			$random_no = strtolower($this->randomKey(6));
			$email_address ='noemail'.$random_no.'@noemail.com';     //if email address is empty
		}
		// to make company name in first case upper if all the letter are upper
		if($company !=""){
		    
			/* $array = explode(' ', $company);
					$count = count($array);
					$i=0;
					 foreach ($array as $testcase) {
						if (ctype_upper($testcase)) {
							 $i++;
						}
					} 
					if($count == $i){
					  $company = strtolower($company);
					  $company = ucwords($company);
					}  */
				
				 $company = $this->filter_company_name($company);
		    
		}
		$job_title = $this->filter_company_name($job_title);
		
		$data = array();			
		$data['first_name']  		= $first_name;
		$data['last_name']  		= $last_name;
		$data['job_title']  		= $job_title;
		$data['city']  				= ucwords(strtolower($city));
		$data['search_id']  		= $search_id;
		$data['country']  			= ucwords(strtolower($country));
		$data['linkedin_profile_url']  = $linkedin_profile_url;
		$data['phone_number']  		= $phone_number;
		$data['website']  			= $website;
		$data['email_address']  	= $email_address;
		$data['company']  			= $company;
		$data['tree_nums']  		= $tree_nums;
		$data['twitter1']  			= $twitter1;
		$data['twitter2']  			= $twitter2;
		$data['twitter3']  			= $twitter3;
		$data['date_birth']  		= $date_birth;
		$data['unique_key']  		= $unique_key;
		$data['delete_key']  		= $delete_key;
		$data['partner_id'] 		= $partner_id;
		$data['certificate_key']  	= $certificate_key;
		
		if($create_hs_immediately == 1){  
			$data['create_hs_immediately'] = "1";
		}
		if($token == "45690l55fsb89cd1dd437087vdc4g"){
			$data['form_type']  = "4";                            //for new sus hubspot users
			$data['company_number']  	= $company_number;
		}else{
			$data['company_number']  	= $company_number_random;    
		}
		//echo "<pre>"; print_r($data); die("here");
		 $insertid = $this->db->insert_batch('pct_imacroData', array($data));    
		 return $this->db->insert_id();		 
	}       
	/**
	 * ============================================================================================================================================
	* function for New insert_autoupload_clients
	*/
	function newlinkedin_insert_autoupload_clients($hubspot_contact_id) {
		
		$first_name = mysql_escape_string($this->input->post('first_name'));
		$last_name = mysql_escape_string($this->input->post('last_name'));
		$job_title = mysql_escape_string($this->input->post('job_title'));
		$search_id = mysql_escape_string($this->input->post('search_id'));
		$company_number = mysql_escape_string($this->input->post('company_number'));
		$city = mysql_escape_string($this->input->post('city'));
		$country = mysql_escape_string($this->input->post('country'));
		$linkedin_profile_url = mysql_escape_string($this->input->post('linkedin_profile_url'));
		$phone_number = mysql_escape_string($this->input->post('phone_number'));
		$website = mysql_escape_string($this->input->post('website'));
		$email_address = mysql_escape_string($this->input->post('email_address'));
		$company = mysql_escape_string($this->input->post('company'));
		$tree_nums = mysql_escape_string($this->input->post('tree_nums'));
		$twitter1 = mysql_escape_string($this->input->post('twitter1'));
		$twitter2 = mysql_escape_string($this->input->post('twitter2'));
		$twitter3 = mysql_escape_string($this->input->post('twitter3'));
		$date_birth = mysql_escape_string($this->input->post('date_birth'));
		
	
		$data = array();			
		$data['first_name']  			= $first_name;
		$data['last_name']  			= $last_name;
		$data['job_title']  			= $job_title;
		$data['search_id']  			= $search_id;
		$data['city']  					= $city;
		$data['country']  				= $country;
		$data['linkedin_profile_url']   = $linkedin_profile_url;
		$data['phone_number']  			= $phone_number;
		$data['website']  				= $website;
		$data['email_address']  		= $email_address;
		$data['company'] 		 		= $company;
		$data['company_number'] 		= $company_number;
		$data['tree_nums']  			= $tree_nums;
		$data['twitter1']  				= $twitter1;
		$data['twitter2']  				= $twitter2;
		$data['twitter3']  				= $twitter3;
		$data['date_birth']  			= $date_birth;
		$data['hubspot_contact_id']  	= $hubspot_contact_id;
		
		//echo "<pre>"; print_r($data); die("here");
		 $insertid = $this->db->insert_batch('pct_js3global_lead_clients', array($data));    
		 return $this->db->insert_id();		 
	}
	function randomKey($length) {  //create no of random digit alphanumeric code
			$key='';
			$pool = array_merge(range(0,9),range('A', 'Z'));

			for($i=0; $i < $length; $i++) {
				$key .= $pool[mt_rand(0, count($pool) - 1)];
			}
			return $key;
		}
	 function load_initiative($email) {
             $query = "SELECT pi.*,pi.website as website_url,bp.*, u.id AS id, u.email AS email, u.username AS username, u.dashboard_user_email AS dashboard_user_email ,u.active AS active, u.tax AS tax, u.price AS price, u.currency AS currency, r.code AS code
					FROM pct_initiative_partner AS bp
					INNER JOIN pct_users AS u
					ON u.id=bp.user_id
					INNER JOIN pct_referer AS r
					ON r.user_id=bp.user_id 
					INNER JOIN pct_initiative AS pi
					ON pi.id=bp.initiative 
					WHERE u.email='$email' AND u.type='intitiative_client'";
            $row = $this->db->query($query);
            $data = $row->row_array();
            return $data;
            
       }
}