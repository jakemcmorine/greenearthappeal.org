<?php
class Ambassador_model extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	/**
	 * ============================================================================================================================================
	* function to insert CFD hubspot form client data api
	*/
	function create_ambassador($post_data) {
		
		$email 			= $post_data['email'];
		$password 		= $post_data['password'];
		$type 			= $post_data['type'];
				
		$data   = array();
		$data['email']      = $email;
		$data['username']   = $email;
		$data['password']   = md5($password);
		if(isset($post_data['channel_manager_id'])){
			$data['channel_manager_id'] = $post_data['channel_manager_id']; 
		}
		$data['type']       = $type; 
		$data['active']     = 1;
		$this->db->insert_batch('pct_users', array($data));
		$user_id    = $this->db->insert_id(); 
		
		//$code = $this->randomKey(8);            //generate code for referid
		$code  =  $post_data['pct_referer'];
		
		// insert referer code
		$pct_refererquery = 'INSERT INTO pct_referer SET user_id="'.$user_id.'",code="'.$code.'"';
		$this->db->query($pct_refererquery);
		
		return $user_id;
		 
	}
	
	/**
	 * ============================================================================================================================================
	* function to update EU Ambassador Email templates
	*/
	function update_euamb_email_templates() {
			
			$records   = $_POST['data'];
			//echo "<pre>"; print_r($records); die;
			$emails = array();
			foreach($records as $key => $val){
				
				if(!empty($records[$key]['id'])){
					
					$emails['name']         = $records[$key]['name'];
					$emails['subject']      = $records[$key]['subject'];
					$emails['message']      = $records[$key]['message'];	
					$emails['time_period']  = $records[$key]['time_period'];								
					$emails['id']     	    = $records[$key]['id'];
					$emails['selected_days']  = implode(',', $records[$key]['days']);
					if(isset($records[$key]['enable_email']) && $records[$key]['enable_email']=='1'){
						$emails['enable_email']   = '1';
					}else{
						$emails['enable_email']   = '0';
					}
					$this->db->update_batch('pct_euamb_email_templates', array($emails), 'id');
					
				}else{
					
					$selected_days  = implode(',', $records[$key]['days']);
					$query = "INSERT INTO pct_euamb_email_templates SET
						name = '{$records[$key]['name']}',
						subject = '{$records[$key]['subject']}',
						template_id = '{$key}',
						enable_email = '{$records[$key]['enable_email']}',
						time_period = '{$records[$key]['time_period']}',
						selected_days = '{$selected_days}',
						message = '{$records[$key]['message']}'";
						$this->db->query($query);   
						
				} 
				
			}
    }
	   
		/**
		 * ============================================================================================================================================
		* function to update EU Ambassador Front of house Email templates
		*/
		function update_eu_fronthouse_email_templates() {
				
					$records   = $_POST['data'];
					//echo "<pre>"; print_r($records); die;
					$emails = array();
					foreach($records as $key => $val){
						if(!empty($records[$key]['id'])){
							$emails['name']         = $records[$key]['name'];
							$emails['subject']      = $records[$key]['subject'];
							$emails['message']      = $records[$key]['message'];	
							$emails['time_period']  = $records[$key]['time_period'];								
							$emails['id']     	    = $records[$key]['id'];
							$emails['selected_days']  = implode(',', $records[$key]['days']);
							if(isset($records[$key]['enable_email']) && $records[$key]['enable_email']=='1'){
								$emails['enable_email']   = '1';
							}else{
								$emails['enable_email']   = '0';
							}
							$this->db->update_batch('pct_euamb_frontofhouse_emails_templates', array($emails), 'id');
						}else{
							 $selected_days  = implode(',', $records[$key]['days']);
							 $query = "INSERT INTO pct_euamb_frontofhouse_emails_templates SET
								name = '{$records[$key]['name']}',
								subject = '{$records[$key]['subject']}',
								template_id = '{$key}',
								enable_email = '{$records[$key]['enable_email']}',
								time_period = '{$records[$key]['time_period']}',
								selected_days = '{$selected_days}',
								message = '{$records[$key]['message']}'";
								$this->db->query($query);   
						} 
					}
		   }
	  	/**
        * ============================================================================================================================================
        * function to load EU Ambassador Email templates
        */
	   function load_eu_ambassdor_template($template_id = "") {
		   
               $query = "SELECT * FROM pct_euamb_email_templates WHERE id=$template_id";
               return $this->db->query($query)->row();
        }
		
		function randomKey($length) {  //create no of random digit alphanumeric code
			$key='';
			$pool = array_merge(range(0,9),range('A', 'Z'));

			for($i=0; $i < $length; $i++) {
				$key .= $pool[mt_rand(0, count($pool) - 1)];
			}
			return $key;
	}
}