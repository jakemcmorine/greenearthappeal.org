<?php
class User_model extends CI_Model {
        function __construct() {
            parent::__construct();
            $this->load->database();
        }
		function get_all_stage1_user_data($user_id) {
			
			  $query = "SELECT activation_key from pct_users where id='$user_id'";
              $rows0 = $this->db->query($query)->row();
			  $query = "SELECT * from pct_grant_user_stage1 where user_id='$user_id'";
              $rows1 = $this->db->query($query)->row_array();
			  $query = "SELECT * from pct_person_stage1 where user_id='$user_id'";
              $rows2['persons'] = $this->db->query($query)->result();
			  $query = "SELECT * from  pct_organisation_stage1 where user_id='$user_id'";
              $rows3['organisations'] = $this->db->query($query)->result();
			  $query = "SELECT * from  pct_breakdowns_stage1 where user_id='$user_id'";
              $rows4['breakdowns'] = $this->db->query($query)->result();
			  $query = "SELECT * from  pct_users where id='$user_id'";
              $rows5['user_data'] = $this->db->query($query)->row();
			  $query = "SELECT species from  pct_species";
              $rows6['species_list'] = $this->db->query($query)->result();
				$rows6['species_lists']=array();
				foreach($rows6['species_list'] as $species){
					 $rows6['species_lists'][] =  $species->species;
				} 
				
				$query = "SELECT u.*,g.* FROM pct_users AS u INNER JOIN pct_grant_users AS g
                        ON g.user_id=u.id WHERE u.id='$user_id'";
				$rows7['grant_user'] = $this->db->query($query)->row();
				/* if(isset($rows0->activation_key)){
					$url ='https://www.greenearthappeal.org/get_grant_user_api.php/?activation_key='.$rows0->activation_key;
					$res = file_get_contents($url);
					if(!empty($res)){
					 $rows7['grant_user'] = json_decode($res);
					}
				} */
				// echo "<pre>"; print_r($rows7['grant_user']); die;
			return array_merge($rows1, $rows2, $rows3, $rows4,$rows5,$rows6,$rows7);
			
		}
		
		function get_inbound_api_data() {
			  $query = "SELECT * from  panacea_api_data where processed='0'";
              $result = $this->db->query($query)->result_array();
			  return  $result;
		}
		function check_inbound_api_userid($post) {
			   $api_user_id  = $post['api_keys']['api_user_id'];
			   $api_user_key = $post['api_keys']['api_user_key'];
			  $query = "SELECT * from  panacea_api_credentials where api_user_id='$api_user_id' AND api_user_key='$api_user_key'";
              $result = $this->db->query($query)->row_array();
			  return  $result;
		}
		
		function check_inbound_order_id($post) {
			   $order_id  	 = $post['api_keys']['order_id'];
			   $api_user_id  = $post['api_keys']['api_user_id'];
			  $query = "SELECT * from  panacea_api_data where panacea_id='$api_user_id' AND order_id='$order_id'";
              $result = $this->db->query($query)->row_array();
			  return  $result;
		}
		
		function get_all_stage2_user_data($user_id) {
			
			  $query = "SELECT * from pct_grant_user_stage2 where user_id='$user_id'";
              $rows1 = $this->db->query($query)->row_array();
			  $query = "SELECT * from pct_stage2_locations where user_id='$user_id'";
              $rows2['locations'] = $this->db->query($query)->result();
			  foreach($rows2['locations'] as $key => $val){
				   $location_id = $val->id;
				   $query = "SELECT * from  pct_stage2_species where location_id='$location_id'";
				  $rows2['locations'][$key]->species = $this->db->query($query)->result();
			  }
			return array_merge($rows1, $rows2);
			
		}
        function get_all_stage3_user_data($user_id,$data1,$data2) {
			
			  $query = "SELECT * from pct_grant_user_stage3 where user_id='$user_id'";
              $rows1 = $this->db->query($query)->row_array();
			  foreach($data1['persons'] as $key => $_person){
					$query = "SELECT * from pct_stage3_person_photos where user_id='$user_id' AND person_id= '$_person->id'";
				    $data1['persons'][$key]->place_photo = $this->db->query($query)->row();
			  }
			  
			   foreach($data2['locations'] as $key => $location){
					$query = "SELECT * from pct_stage3_location_photos where user_id='$user_id' AND location_id= '$location->id'";
					$data2['location_photos'][$key] = $this->db->query($query)->result();
					
					$query3 = "SELECT * from pct_stage3_videos_links where user_id='$user_id' AND location_id= '$location->id'";
					$data2['locations'][$key]->videos = $this->db->query($query3)->row();
					
			  } 
			  $query4 = "SELECT * from pct_stage3_community_photos where user_id='$user_id'";
			  $data2['community_photos'] = $this->db->query($query4)->result();
			 
			//echo "<pre>"; print_r($data2['location_photos']); die;
			return array_merge($rows1, $data1,$data2);
			
		}
		
		function list_grant_users($stage="") {
			
			if($stage=='password'){
				$query = "SELECT u.*,g.* FROM pct_users AS u INNER JOIN pct_grant_users AS g
                        ON g.user_id=u.id
                    WHERE u.type='grant_user' AND password!=''";
			}elseif($stage=='proj_overview'){
				$query = "SELECT u.*,g.* FROM pct_users AS u INNER JOIN pct_grant_users AS g
                        ON g.user_id=u.id INNER JOIN pct_grant_user_stage1 AS stage1 ON stage1.user_id=u.id
                    WHERE u.type='grant_user' AND u.id NOT IN (SELECT user_id from pct_grant_user_stage2) AND password!=''";
			}elseif($stage=='proj_details'){
				$query = "SELECT u.*,g.* FROM pct_users AS u INNER JOIN pct_grant_users AS g
                        ON g.user_id=u.id INNER JOIN pct_grant_user_stage2 AS stage2 ON stage2.user_id=u.id
                    WHERE u.type='grant_user' AND u.id NOT IN (SELECT user_id from pct_grant_user_stage3) AND password!=''";
			}elseif($stage=='supporting_info'){
				$query = "SELECT u.*,g.* FROM pct_users AS u INNER JOIN pct_grant_users AS g
                        ON g.user_id=u.id INNER JOIN pct_grant_user_stage3 AS stage3 ON stage3.user_id=u.id
                    WHERE u.type='grant_user' AND password!=''";
			}else{
				$query = "SELECT u.*,g.* FROM pct_users AS u INNER JOIN pct_grant_users AS g
                        ON g.user_id=u.id
                    WHERE u.type='grant_user'";	
			}
			
            $rows = $this->db->query($query)->result();
			//echo "<pre>"; print_r($rows); die;
			return $rows;
			
		 }
        function get_list_users() {
            // load restaurants
            $query = "SELECT re.*, re.restaurant AS name, (SELECT SUM(trees) FROM pct_figures WHERE user_id=u.id) AS tree_nums2, u.id AS id, u.email AS email, u.username AS username, u.active AS active, u.type AS type
                        FROM pct_restaurants AS re
                        INNER JOIN pct_users AS u
                        ON u.id=re.user_id
                        WHERE u.type IN ('admin', 'restaurant', 'bulk_partner', 'solicitor') and active = '1'";
            $rows1 = $this->db->query($query)->result();
            // load bulk partners
            $query = "SELECT bp.*, bp.company AS name, (SELECT SUM(trees) FROM pct_invoices WHERE user_id=u.id) AS tree_nums2, u.id AS id, u.email AS email, u.username AS username,  u.active AS active, u.type AS type
                        FROM pct_bulk_partners AS bp
                        INNER JOIN pct_users AS u
                        ON u.id=bp.user_id
                        WHERE u.type IN ('admin', 'restaurant', 'bulk_partner') and active = '1'";
            $rows2 = $this->db->query($query)->result();
			
			$query = "SELECT pi.initiative_name, bp.*, bp.company AS name, (SELECT (SUM(free_trees)+ SUM(tree_nums)) FROM pct_initiative_partner WHERE bulker_id=u.id) As tree_nums2 , u.id AS id, u.email AS email, u.username AS username,  u.active AS active, u.type AS type
                        FROM pct_initiative_partner AS bp
                        INNER JOIN pct_users AS u
                        ON u.id=bp.user_id
						INNER JOIN pct_initiative AS pi
                        ON pi.id=bp.initiative
                        WHERE u.type IN ('admin', 'intitiative') and active = '1'";
						
            $rows6 = $this->db->query($query)->result();
            // load solicitors
            $query = "SELECT u.* FROM pct_users AS u
                        WHERE u.type='solicitor'";
            $rows3 = $this->db->query($query)->result();

            // load editors
            $query = "SELECT u.* FROM pct_users AS u
                    WHERE u.type='editor'";
            $rows4 = $this->db->query($query)->result();
            
            // load other
            $query = "SELECT u.* FROM pct_users AS u
                    WHERE u.type='other'";
            $rows5 = $this->db->query($query)->result();
			
			 $query = "SELECT u.* FROM pct_users AS u
                    WHERE u.type='initiative_editor'";
            $rows7 = $this->db->query($query)->result();
			
            return array_merge($rows1, $rows2, $rows3, $rows4, $rows5 ,$rows6,$rows7);
        }
		
		function get_initiative_users_list($user_id) {
            
            // load bulk partners
   
		 	 $query = "SELECT pi.initiative_name, bp.*, bp.company AS name, (SELECT (SUM(free_trees)+ SUM(tree_nums)) FROM pct_initiative_partner WHERE bulker_id=u.id) As tree_nums2 , u.id AS id, u.email AS email, u.username AS username,  u.active AS active, u.type AS type
                        FROM pct_initiative_partner AS bp
                        INNER JOIN pct_users AS u
                        ON u.id=bp.user_id
						INNER JOIN pct_initiative AS pi
                        ON pi.id=bp.initiative
                        WHERE u.type IN ('intitiative') AND pi.user_id=$user_id";
						
			//$query1 = "SELECT *,bp.user_id as id,pi.initiative_name as initiative, (SELECT (SUM(free_trees)+ SUM(tree_nums)) FROM pct_initiative_partner WHERE bulker_id=u.id) As tree_nums2 from pct_initiative pi INNER JOIN pct_users AS u  ON u.id=pi.user_id left join pct_initiative_partner bp on bp.initiative = pi.id  WHERE pi.user_id = $user_id";
						
           $rows6 = $this->db->query($query)->result();
		  
		   foreach($rows6 as $key => $_row){
			   
			 $rows6[$key]->type = 'intitiative';
			
		   }
		  $query = "SELECT re.*, re.restaurant AS name, (SELECT SUM(trees) FROM pct_figures WHERE user_id=u.id) AS tree_nums2, u.id AS id, u.email AS email, u.username AS username, u.active AS active, u.type AS type
			FROM pct_restaurants AS re
			INNER JOIN pct_users AS u
			ON u.id=re.user_id
			WHERE u.type='restaurant' AND re.initiative_editor_id=$user_id";
            $rows2 = $this->db->query($query)->result(); 
		    //echo "<pre>"; print_r($rows2); die;
		   return array_merge($rows6,$rows2);
        }
		
		function get_list_users_manager() {
            // load restaurants
           $query = "SELECT re.*, re.restaurant AS name, (SELECT SUM(trees) FROM pct_figures WHERE user_id=u.id) AS tree_nums2, u.id AS id, u.email AS email, u.username AS username, u.active AS active, u.type AS type
                        FROM pct_restaurants AS re
                        INNER JOIN pct_users AS u
                        ON u.id=re.user_id
                        WHERE u.type IN ('restaurant', 'bulk_partner', 'solicitor')";
            $rows1 = $this->db->query($query)->result();
            // load bulk partners
            $query = "SELECT bp.*, bp.company AS name, (SELECT SUM(trees) FROM pct_invoices WHERE user_id=u.id) AS tree_nums2, u.id AS id, u.email AS email, u.username AS username,  u.active AS active, u.type AS type
                        FROM pct_bulk_partners AS bp
                        INNER JOIN pct_users AS u
                        ON u.id=bp.user_id
                        WHERE u.type IN ('admin', 'restaurant', 'bulk_partner')";
            $rows2 = $this->db->query($query)->result();
			
            return array_merge($rows1, $rows2);
        }
        
		
		function get_company_name($user_id) {
			
			 $query = "SELECT * FROM pct_users where id='".$user_id."'";
             $rows1 = $this->db->query($query)->row_array();
			 if($rows1['type']=="intitiative"){
				  $query = "SELECT * FROM pct_initiative_partner where user_id='".$user_id."'";
				  $company_name = $this->db->query($query)->row_array();
				  return $company_name['company'];
			 }elseif($rows1['type']=="restaurant"){
				  $query = "SELECT * FROM pct_restaurants where user_id='".$user_id."'";
				  $company_name = $this->db->query($query)->row_array();
				  return $company_name['restaurant'];
				 
			 }elseif($rows1['type']=="bulk_partner"){
				 $query = "SELECT * FROM pct_bulk_partners where user_id='".$user_id."'";
				  $company_name = $this->db->query($query)->row_array();
				  return $company_name['company'];
			 }
			 //echo "<pre>"; print_r($rows1); die;
		}
		
		
		function list_track(){
			// load affiliates 
            $query = "SELECT * FROM pct_affiliate_track";
            $rows1 = $this->db->query($query)->result();
			
            return array_merge($rows1);	
		}
		
		
		function get_submission_list(){
			// load affiliates 
            $query = "SELECT * FROM pct_affiliate_track WHERE affiliate_code = '".$_SESSION['login']['affcode']."' ";
            $rows1 = $this->db->query($query)->result();
			
            return array_merge($rows1);	
		}
		
		
		function list_affiliates(){
			// load affiliates 
            $query = "SELECT * FROM pct_affiliates";
            $rows1 = $this->db->query($query)->result();
			
            return array_merge($rows1);	
		}
		
		
	     function get_parter_data($user_id) {
            $sql = "SELECT * FROM pct_initiative_partner WHERE user_id=$user_id";
            $user = $this->db->query($sql)->row_array();
            return $user;
			
		  }
        function get_user($id) {
            $sql = "SELECT * FROM pct_users WHERE id=$id";
            $user = $this->db->query($sql)->row_array();
            $user2 = array();
            
			 if ( $user['type']=='intitiative' ) {
                $sql2 = "SELECT * FROM pct_initiative_partner WHERE user_id=$id";
                $user2 = $this->db->query($sql2)->row_array();
            } elseif ( $user['type']=='bulk_partner' ) {
                $sql2 = "SELECT * FROM pct_bulk_partners WHERE user_id=$id";
                $user2 = $this->db->query($sql2)->row_array();
            } elseif ( $user['type']=='restaurant' ) {
                $sql2 = "SELECT * FROM pct_restaurants WHERE user_id=$id";
                $user2 = $this->db->query($sql2)->row_array();
            } else if ( $user['type'] == 'other') {
                if ($id == 802) {
                    // the sonne
                    $user2 = array(
                        'address' => 'Antiguo Camino Minera del Norte Km 2.5',
                        'city' => 'Col. Centro',
                        'state' => 'Santa Catarina',
                        'post_code' => 'N.L.',
                        'first_name' => 'Fruit Tree Planting Donations',
                        'company' => 'Sonne Energeticos'
                    );
                } else {
                    // paper cup
                    $user2 = array(
                        'address' => 'Unit 3 Mearley Brooke Commercial Centre',
                        'city' => 'Citheroe',
                        'state' => 'Lancs',
                        'post_code' => 'BB7 1PL',
                        'first_name' => 'Sarah',
                        'company' => 'The Printed Cup Company'
                    );
                }
            }
            return array_merge($user, $user2);
        }
        
        function update_user_state($id, $first=0) {
            // 
            $query = "UPDATE pct_users SET first=$first WHERE id=$id";
            $this->db->query($query);
        }
        
		
		function get_solicitor_user() {
			$user =array();
            $sql = "SELECT * FROM pct_users WHERE type='solicitor' AND active = 1";
            $user = $this->db->query($sql)->row_array();
            return $user;
        }
		
		 /**
         * ============================================================================================================================================
        * function for insert stage1 grant user data 
        */
		function add_grant_user_stage1() {  
		//echo "<pre>"; print_r($_FILES); die;
			$user_id = $_SESSION['login']['id'];
			$type_of_project = mysql_escape_string($this->input->post('type_of_project'));
			$project_start_date = mysql_escape_string($this->input->post('project_start_date'));
			$project_aims = mysql_escape_string($this->input->post('project_aims'));
			$local_community = mysql_escape_string($this->input->post('local_community'));
			$location_organisation = mysql_escape_string($this->input->post('location_organisation'));
            $team = mysql_escape_string($this->input->post('team'));
            $total_cost = mysql_escape_string($this->input->post('total_cost'));
            $org_provide_fund = mysql_escape_string($this->input->post('org_provide_fund'));
            $organisation_funding = mysql_escape_string($this->input->post('organisation_funding'));
            $other_partner_funding = mysql_escape_string($this->input->post('other_partner_funding'));
            $gea_funding = mysql_escape_string($this->input->post('gea_funding'));
 		
     		$data = array();	
			$data['user_id']                 = $user_id;
            $data['type_of_project']         = $type_of_project;
            $data['project_start_date']      = $project_start_date;
            $data['project_aims']            = $project_aims;
            $data['local_community']         = $local_community;
			$data['location_organisation']   = $location_organisation;
			$data['long_latitute']   		 = $_POST['lan_lat'];
            $data['team']                    = $team;
            $data['total_cost']              = $total_cost;
			$data['org_provide_fund']        = $org_provide_fund;
			$data['organisation_funding']    = $organisation_funding;
			$data['other_partner_funding']   = $other_partner_funding;
			$data['gea_funding']    		 = $gea_funding;
			$this->db->insert_batch('pct_grant_user_stage1', array($data));
			
		   $persons   = $_POST['person'];
		   $person = array();
			foreach($persons as $key => $val){
				
					$photo = '';
					  // check upload
					if ($_FILES['photo'.$key]['size']) {
							$config['file_name'] = date('Yhs').'-'.$_FILES['photo'.$key]['name'];
							$config['upload_path'] = './uploads/organisation_stage1/';
							$config['allowed_types'] = 'jpg|jpeg|png|gif';
							$config['max_size'] = '2048000';
							$this->load->library('upload');
							$this->upload->initialize($config);
							$this->upload->do_upload('photo'.$key);
							$photo = $this->upload->data();
							$photo = $photo['file_name'];
							
					} 
				
				$person['user_id']           		 = $user_id;
				$person['first_name']         		 = $persons[$key]['first_name'];
				$person['surname']                   = $persons[$key]['surname'];
				$person['qualifications']            = $persons[$key]['qualifications'];
				$person['project_responsbilites']    = $persons[$key]['project_responsbilites'];
				$person['experience']                = $persons[$key]['experience'];
				//$person['person_photo']              = $photo;
				$this->db->insert_batch('pct_person_stage1', array($person));
				
			}
			
			$organisations   = $_POST['organisation'];
			$organisation = array();
			foreach($organisations as $key => $val){
				$organisation['user_id']           		=  $user_id;
				$organisation['org_name']         	   	= $organisations[$key]['org_name'];
				//$organisation['funding_provide']       	= $organisations[$key]['funding_provide'];
				$organisation['org_funding_provide']    = $organisations[$key]['org_funding_provide'];
				$organisation['org_website']            = $organisations[$key]['org_website'];
				$organisation['person_first_name']      = $organisations[$key]['person_first_name'];
				$organisation['person_surname']         = $organisations[$key]['person_surname'];
				$organisation['person_email_Address']   = $organisations[$key]['person_email_Address'];
			   $this->db->insert_batch('pct_organisation_stage1', array($organisation));
				
			}
			$breakdowns   = $_POST['breakdown'];
			$breakdown = array();
			//echo "<pre>"; print_r($breakdowns); die;
			foreach($breakdowns as $key => $val){
				$breakdown['user_id']           	 = $user_id;
				if($breakdowns[$key]['description']!=''){	
					$breakdown['description']         	 = $breakdowns[$key]['description'];
					$breakdown['units']      		     = $breakdowns[$key]['units'];
					$breakdown['cost_per_unit']          = $breakdowns[$key]['cost_per_unit'];
					$breakdown['no_of_units']     		 = $breakdowns[$key]['no_of_units'];
					$breakdown['totals']     		     = $breakdowns[$key]['totals'];
					
					$this->db->insert_batch('pct_breakdowns_stage1', array($breakdown));
				}
			  
			}
         return $user_id;
        }
		
		 /**
         * ============================================================================================================================================
        * function for insert stage1 grant user data 
        */
		
		 function update_grant_user_stage1($user_id="") {  
		 
			 if($user_id==''){
				 $user_id = $_SESSION['login']['id'];
			 }
			//echo "<pre>"; print_r($_POST); die('here');
			$type_of_project = mysql_escape_string($this->input->post('type_of_project'));
			$project_start_date = mysql_escape_string($this->input->post('project_start_date'));
			$project_aims = mysql_escape_string($this->input->post('project_aims'));
			$local_community = mysql_escape_string($this->input->post('local_community'));
			$location_organisation = mysql_escape_string($this->input->post('location_organisation'));
            $team = mysql_escape_string($this->input->post('team'));
            $total_cost = mysql_escape_string($this->input->post('total_cost'));
            $org_provide_fund = mysql_escape_string($this->input->post('org_provide_fund'));
            $organisation_funding = mysql_escape_string($this->input->post('organisation_funding'));
			$other_partner_funding = mysql_escape_string($this->input->post('other_partner_funding'));
            $gea_funding = mysql_escape_string($this->input->post('gea_funding'));
 		
     		$data = array();	
            $data['type_of_project']         = $type_of_project;
            $data['project_start_date']      = $project_start_date;
            $data['project_aims']            = $project_aims;
            $data['local_community']         = $local_community;
			$data['location_organisation']   = $location_organisation;
			$data['long_latitute']   		 = $_POST['lan_lat'];
            $data['team']                    = $team;
            $data['total_cost']              = $total_cost;
			$data['org_provide_fund']        = $org_provide_fund;
			$data['organisation_funding']    = $organisation_funding;
			$data['other_partner_funding']   = $other_partner_funding;
			$data['gea_funding']    		 = $gea_funding;
			$data['user_id']  = $user_id;
			if ($_SESSION['login']['type'] == 'grant_user') {
				$data['status']  = '0';               //after update change it from 2 to 0
			}
			
			$this->db->update_batch('pct_grant_user_stage1', array($data), 'user_id');		
			$persons   = $_POST['person'];
		 
			foreach($persons as $key => $val){
				$person = array();
					$photo = '';
					  // check upload
					if ($_FILES['photo'.$key]['size']) {
							$config['file_name'] = date('Yhs').'-'.$_FILES['photo'.$key]['name'];
							$config['upload_path'] = './uploads/organisation_stage1/';
							$config['allowed_types'] = 'jpg|jpeg|png|gif';
							$config['max_size'] = '2048000';
							$this->load->library('upload');
							$this->upload->initialize($config);
							$this->upload->do_upload('photo'.$key);
							$photo = $this->upload->data();
							$photo = $photo['file_name'];
							
					} 
				
				

				$person['first_name']         		 = $persons[$key]['first_name'];
				$person['surname']                   = $persons[$key]['surname'];
				$person['qualifications']            = $persons[$key]['qualifications'];
				$person['project_responsbilites']    = $persons[$key]['project_responsbilites'];
				$person['experience']                = $persons[$key]['experience'];
				if($photo!=""){
					//$person['person_photo']          = $photo;
				}
			 	if(isset($persons[$key]['id'])){
					$person['id']   = $persons[$key]['id'];
					$this->db->update_batch('pct_person_stage1', array($person), 'id');	
					
				}else{
					$person['user_id'] = $user_id;
					$this->db->insert_batch('pct_person_stage1', array($person));	
				} 			
			}
			$organisations   = $_POST['organisation'];
	
			foreach($organisations as $key => $val){
				$organisation = array();
				$organisation['org_name']         	   	= $organisations[$key]['org_name'];
				//$organisation['funding_provide']       	= $organisations[$key]['funding_provide'];
				$organisation['org_funding_provide']    = $organisations[$key]['org_funding_provide'];
				$organisation['org_website']            = $organisations[$key]['org_website'];
				$organisation['person_first_name']      = $organisations[$key]['person_first_name'];
				$organisation['person_surname']         = $organisations[$key]['person_surname'];
				$organisation['person_email_Address']   = $organisations[$key]['person_email_Address'];
			   
			   
			   if(isset($organisations[$key]['id'])){
					$organisation['id']   = $organisations[$key]['id'];
					$this->db->update_batch('pct_organisation_stage1', array($organisation), 'id');
			   
				}else{
					$organisation['user_id'] = $user_id;
					$this->db->insert_batch('pct_organisation_stage1', array($organisation));	
				} 		
				
			}
			$breakdowns   = $_POST['breakdown'];
			//echo "<pre>"; print_r($breakdowns); die;
			foreach($breakdowns as $key => $val){
				$breakdown = array();
				if($breakdowns[$key]['description']!=''){	
					$breakdown['id']           	 		 = $breakdowns[$key]['id'];
					$breakdown['description']         	 = $breakdowns[$key]['description'];
					$breakdown['units']      		     = $breakdowns[$key]['units'];
					$breakdown['cost_per_unit']          = $breakdowns[$key]['cost_per_unit'];
					$breakdown['no_of_units']     		 = $breakdowns[$key]['no_of_units'];
					$breakdown['totals']     		     = $breakdowns[$key]['totals'];

					if(isset($breakdowns[$key]['id'])){
						$breakdown['id']   = $breakdowns[$key]['id'];
						$this->db->update_batch('pct_breakdowns_stage1', array($breakdown), 'id');
				   
					}else{
						$breakdown['user_id'] = $user_id;
						$this->db->insert_batch('pct_breakdowns_stage1', array($breakdown));	
				} 	
				}
				
			}
         
        }
		 /**
         * ============================================================================================================================================
        * function for insert stage2 grant user data 
        */
		function add_grant_user_stage2() {  
			//echo "<pre>"; print_r($_FILES); die;

			if($user_id==''){
				 $user_id = $_SESSION['login']['id'];
			 }
			$locations = mysql_escape_string($this->input->post('locations'));
		
     		$data = array();	
			$data['user_id']                 = $user_id;
            $data['locations']         		 = $locations;
     
			$this->db->insert_batch('pct_grant_user_stage2', array($data));
			
		   $locations   = $_POST['location'];
	
			foreach($locations as $key => $val){
				
				$location['user_id']           		   =  $user_id;
				//$location['country']         	       = $locations[$key]['country'];
				//$location['region']                    = $locations[$key]['region'];
				//$location['area']            		   = $locations[$key]['area'];
				//$location['location']                  = $locations[$key]['location'];
				$location['google_pin_loc']            = $locations[$key]['google_pin_loc'];
				$location['long_latitute']             = $locations[$key]['lat_lang'];
				//echo '<pre>';print_r($location).'</br>'; die;
				$this->db->insert_batch('pct_stage2_locations', array($location));
			    $location_id = $this->db->insert_id();
				if(!empty($locations[$key]['species'])){
					$species = $locations[$key]['species'];
					foreach($species as $key => $val){
						$specie['user_id']           		 =  $user_id;
						$specie['location_id']           	 =  $location_id;
						$specie['species_name']         	 = $species[$key]['species_name'];
						$specie['no_of_trees']               = $species[$key]['no_of_trees'];
						$specie['species_seeds']             = $species[$key]['types_of_trees'];
						$specie['species_benefits']          = $species[$key]['species_benefits'];	
						$this->db->insert_batch('pct_stage2_species', array($specie));
					}
				}
				
			}
			
         return $user_id;
        }
		
		 /**
         * ============================================================================================================================================
        * function for update stage2 grant user data 
        */
		function update_grant_user_stage2($user_id="") { 
            //echo "<pre>"; print_r($_POST); die;
			if($user_id==''){
				 $user_id = $_SESSION['login']['id'];
			 }
			$locations = mysql_escape_string($this->input->post('locations'));
		
     		$data = array();	
			$data['user_id']                 = $user_id;
            $data['locations']         		 = $locations;
			if ($_SESSION['login']['type'] == 'grant_user') {
				$data['stage2_status']  = '0';               //after update change it from 2 to 0
			}
			$this->db->update_batch('pct_grant_user_stage2', array($data),'user_id');		
			
		   $locations   = $_POST['location'];
	
			foreach($locations as $key => $val){
			$location = array();
				// $location['country']         	       = $locations[$key]['country'];
				// $location['region']                    = $locations[$key]['region'];
				// $location['area']            		   = $locations[$key]['area'];
				//$location['location']                  = $locations[$key]['location'];
				$location['google_pin_loc']            = $locations[$key]['google_pin_loc'];
				$location['long_latitute']             =  $locations[$key]['lat_lang'];
				//echo '<pre>';print_r($location).'</br>'; die;
				 if(isset($locations[$key]['id'])){
					$location['id']   = $locations[$key]['id'];
					$this->db->update_batch('pct_stage2_locations', array($location), 'id');
			   
				}else{
					$location['user_id'] = $user_id;
					$this->db->insert_batch('pct_stage2_locations', array($location));	
					$location_id = $this->db->insert_id();
				} 		
				
			    
				if(!empty($locations[$key]['species'])){
					$species = $locations[$key]['species'];
					foreach($species as $key => $val){
						$specie = array();
						if(isset($location_id)){
							$specie['location_id']           	 =  $location_id;
						}
						$specie['species_name']         	 = $species[$key]['species_name'];
						$specie['no_of_trees']               = $species[$key]['no_of_trees'];
						$specie['species_seeds']             = $species[$key]['types_of_trees'];
						$specie['species_benefits']          = $species[$key]['species_benefits'];	
						
						if(isset($species[$key]['id'])){
							$specie['id']   = $species[$key]['id'];
							$this->db->update_batch('pct_stage2_species', array($specie), 'id');
					   
						}else{
							$specie['user_id'] = $user_id;
							if(isset($location['id'])){
								$specie['location_id']      =  $location['id'];
							}
							$this->db->insert_batch('pct_stage2_species', array($specie));	
						} 		
					}
				}
				
			}
			
         return $user_id;
        }
		
		 /**
         * ============================================================================================================================================
        * function for insert stage3 grant user data 
        */
		function add_grant_user_stage3($user_id="") {  
		// echo "<pre>"; print_r($_POST);  print_r($_FILES); die;
			if($user_id==''){
				 $user_id = $_SESSION['login']['id'];
			 }
			$data = array();	
			$data['user_id']   = $user_id;
					
			$location_photos  = $_POST['location_data'];
			foreach($location_photos as $key => $val){
						  // check upload
					if ($_FILES['location_photos'.$key]['size']) {
						$config['upload_path'] ='./uploads/person_photos/';
						$config['allowed_types'] ='jpg|jpeg|png|gif|pdf';
						$config['max_size']     = '2048000';
						$config['overwrite'] = TRUE;
						$fileCount = count($_FILES['location_photos'.$key]["name"]);
							for($i=0; $i < $fileCount; $i++){
								$_FILES['files']['name'] = date('Yhs').'-'.$_FILES["location_photos".$key]['name'][$i];
								$_FILES['files']['type'] = $_FILES["location_photos".$key]['type'][$i];
								$_FILES['files']['tmp_name'] = $_FILES["location_photos".$key]['tmp_name'][$i];
								$_FILES['files']['error'] = $_FILES["location_photos".$key]['error'][$i];
								$_FILES['files']['size'] = $_FILES["location_photos".$key]['size'][$i]; 
								
							$this->load->library('upload', $config);
							$this->upload->initialize($config);
							if (!$this->upload->do_upload('files'))      
								{  
									$data22222 = array('error' => $this->upload->display_errors());    
									//echo $data['error']; 
								}else{
									$data3 = $this->upload->data();
								}
								//echo '<pre>'; print_r($data); 
								if($data3['file_name']!=""){
									$location['location_photo']   = $data3['file_name'];
									$location['user_id']          = $user_id;
									$location['location_id']      = $key;
								   $this->db->insert_batch('pct_stage3_location_photos', array($location));
								}
							}
						
					} 
						
			}

			  if(!empty($_FILES['community_photos']['size'][0])){
				 
					$config['upload_path'] ='./uploads/person_photos/';
					$config['allowed_types'] ='jpg|jpeg|png|gif|pdf';
					$config['max_size']     = '2048000';
					$config['overwrite'] = TRUE;
					$fileCount = count($_FILES["community_photos"]["name"]);
					for($i=0; $i < $fileCount; $i++){
						
					 	$_FILES['files']['name'] = date('Yhs').'-'.$_FILES["community_photos"]['name'][$i];
						$_FILES['files']['type'] = $_FILES["community_photos"]['type'][$i];
						$_FILES['files']['tmp_name'] = $_FILES["community_photos"]['tmp_name'][$i];
						$_FILES['files']['error'] = $_FILES["community_photos"]['error'][$i];
						$_FILES['files']['size'] = $_FILES["community_photos"]['size'][$i]; 
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if (!$this->upload->do_upload('files'))      
							{  
								$data22222 = array('error' => $this->upload->display_errors());    
								//echo $data['error'];
							}else{
								$data1 = $this->upload->data();
							}
							if($data1['file_name']!=""){
								$community['community_photo']   = $data1['file_name'];
								$community['user_id']            = $user_id;
								$this->db->insert_batch('pct_stage3_community_photos', array($community));
							}
					}
			 }

			if ($_FILES['org_headquarter_photo']['size']) {
					$config['file_name'] = date('Yhs').'-'.$_FILES['org_headquarter_photo']['name'];
					$config['upload_path'] = './uploads/person_photos/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
					$config['max_size'] = '2048000';
					$this->load->library('upload');
					$this->upload->initialize($config);
					$this->upload->do_upload('org_headquarter_photo');
					$photo = $this->upload->data();
					
					$data['org_headquarter_photo']   = $photo['file_name'];
			 }
			  if ($_FILES['additional_documentation']['size']) {
					$config['file_name'] = date('Yhs').'-'.$_FILES['additional_documentation']['name'];
					$config['upload_path'] = './uploads/person_photos/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx|xlsx|eml|msg';
					$config['max_size'] = '2048000';
					$this->load->library('upload');
					$this->upload->initialize($config);
					$this->upload->do_upload('planted_tree_location_photo');
					$photo = $this->upload->data();
					
					$data['additional_documentation']   = $photo['file_name'];
			}  
			//echo "<pre>"; print_r($data); die;
			 $this->db->insert_batch('pct_grant_user_stage3', array($data));
			 
			
			/* if ($_FILES['community_photos']['size']) {
					$config['file_name'] = date('Yhs').'-'.$_FILES['community_photos']['name'];
					$config['upload_path'] = './uploads/person_photos/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
					$config['max_size'] = '10000';
					$this->load->library('upload');
					$this->upload->initialize($config);
					$this->upload->do_upload('community_photos');
					$photo = $this->upload->data();
					
					$community['community_photo']   = $photo['file_name'];
					$community['user_id']            = $user_id;
					$this->db->insert_batch('pct_stage3_community_photos', array($community));	
			 } */
			
			 
			$photo = '';
			$person_photos  = $_POST['person_data'];
			foreach($person_photos as $key => $val){
					$photo = '';
					  // check upload
					if ($_FILES['person_photos'.$key]['size']>0) {
							$config['file_name'] = date('Yhs').'-'.$_FILES['person_photos'.$key]['name'];
							$config['upload_path'] = './uploads/person_photos/';
							$config['allowed_types'] = 'jpg|jpeg|png|gif';
							$config['max_size'] = '2048000';
							$this->load->library('upload');
							$this->upload->initialize($config);
							$this->upload->do_upload('person_photos'.$key);
							$photo = $this->upload->data();
							$photo = $photo['file_name'];
							
					} 
				$person['person_photo']     = $photo;
				$person['user_id']          = $user_id;
				$person['person_id']        = $key;
				$this->db->insert_batch('pct_stage3_person_photos', array($person));
			}
			  
		/* 	$location_photo = '';
			$location_photos  = $_POST['location_data'];
			foreach($location_photos as $key => $val){
					
					  // check upload
					if ($_FILES['location_photos'.$key]['size']) {
							$config['file_name'] = date('Yhs').'-'.$_FILES['location_photos'.$key]['name'];
							$config['upload_path'] = './uploads/person_photos/';
							$config['allowed_types'] = 'jpg|jpeg|png|gif';
							$config['max_size'] = '10000';
							$this->load->library('upload');
							$this->upload->initialize($config);
							$this->upload->do_upload('location_photos'.$key);
							$photo = $this->upload->data();
							$location_photo = $photo['file_name'];
							
					} 
				$location['location_photo']   = $location_photo;
				$location['user_id']          = $user_id;
				$location['location_id']      = $key;
				$this->db->insert_batch('pct_stage3_location_photos', array($location));
			}
			 */
		    $videos_data  = $_POST['videos_data'];
			foreach($videos_data as $key => $val){
				
				$videos['user_id']          = $user_id;
				$videos['location_id']      = $key;
				$videos['video_link']       = $_POST['videos_links'.$key];
				$videos['additional_link']  = $_POST['additional_links'.$key];
				$this->db->insert_batch('pct_stage3_videos_links', array($videos));
				
				}
					
		/* 	$image_data1 = $this->input->post('community_photos');
			
			if(!empty($image_data1)){
				$file_name1 = date('Yhs').'_photo.png';
				$upload_path = './uploads/person_photos/'.$file_name1;
				list($type, $image_data1) = explode(';', $image_data1);
				list(, $image_data1)      = explode(',', $image_data1);
				$image_data1 = base64_decode($image_data1);
				file_put_contents($upload_path, $image_data1);
				
				$data['community_photo']   = $file_name1;
				
			} */
			
		/* 	if ($_FILES['planted_tree_location_photo']['size']) {
					$config['file_name'] = date('Yhs').'-'.$_FILES['planted_tree_location_photo']['name'];
					$config['upload_path'] = './uploads/person_photos/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
					$config['max_size'] = '10000';
					$this->load->library('upload');
					$this->upload->initialize($config);
					$this->upload->do_upload('planted_tree_location_photo');
					$photo = $this->upload->data();
					
					$data['planted_tree_location_photo']   = $photo['file_name'];
			 }  */
			
				
			
			
			
			/*    if(!empty($_POST['person_photos'])){
				   $person_photos = $_POST['person_photos'];
				  foreach($person_photos as $key => $val){
						$image_data = $person_photos[$key]['image'];
						if(!empty($image_data)){
							$file_name = date('Yhs').$key.'_photo.png';
							$upload_path = './uploads/person_photos/'.$file_name;
							$image_data = $person_photos[$key]['image'];
							list($type, $image_data) = explode(';', $image_data);
							list(, $image_data)      = explode(',', $image_data);
							$image_data = base64_decode($image_data);
							file_put_contents($upload_path, $image_data);
						
							$photos['user_id']           	= $user_id;
							$photos['person_id']            = $person_photos[$key]['person_id'];
							$photos['person_photo']         = $file_name;
								
					        $this->db->insert_batch('pct_stage3_person_photos', array($photos));
							
						}
				   }
			  }  */
		   
			return $user_id;
		
		}
		
		 /**
         * ============================================================================================================================================
        * function for update update_grant_user_stage3 grant user data 
        */
		function update_grant_user_stage3($user_id="") {   
			//echo "<pre>"; print_r($_POST['community_photos']); 
			//echo "<pre>"; print_r($_FILES['community_photos']); 
			if($user_id==''){
				 $user_id = $_SESSION['login']['id'];
			 }
			 if(!empty($_FILES['community_photos']['size'][0])){
				 
					$config['upload_path'] ='./uploads/person_photos/';
					$config['allowed_types'] ='jpg|jpeg|png|gif|pdf';
					$config['max_size']     = '2048000';
					$config['overwrite'] = TRUE;
					$fileCount = count($_FILES["community_photos"]["name"]);
					for($i=0; $i < $fileCount; $i++){
						$data1['file_name']="";
					 	$_FILES['files']['name'] = date('Yhs').'-'.$_FILES["community_photos"]['name'][$i];
						$_FILES['files']['type'] = $_FILES["community_photos"]['type'][$i];
						$_FILES['files']['tmp_name'] = $_FILES["community_photos"]['tmp_name'][$i];
						$_FILES['files']['error'] = $_FILES["community_photos"]['error'][$i];
						$_FILES['files']['size'] = $_FILES["community_photos"]['size'][$i]; 
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if (!$this->upload->do_upload('files'))      
							{  
								$data22222 = array('error' => $this->upload->display_errors());    
								//echo $data['error'];
							}else{
								$data1 = $this->upload->data();
							}
							if($data1['file_name']!=""){
								$community['community_photo']   = $data1['file_name'];
								$community['user_id']            = $user_id;
								$this->db->insert_batch('pct_stage3_community_photos', array($community));
							}
					}
			 }
		
			/* $community_photos  = $_FILES['community_photos'];
			echo "<pre>"; print_r($community_photos); die; */
			$data = array();	
			$data['user_id']  = $user_id;
			if ($_FILES['org_headquarter_photo']['size']>0) {
					$config['file_name'] = date('Yhs').'-'.$_FILES['org_headquarter_photo']['name'];
					$config['upload_path'] = './uploads/person_photos/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
					$config['max_size'] = '2048000';
					$this->load->library('upload');
					$this->upload->initialize($config);
					$this->upload->do_upload('org_headquarter_photo');
					$photo = $this->upload->data();
					if($photo!=""){
						$data['org_headquarter_photo']   = $photo['file_name'];
					}
			 }
			  if ($_FILES['additional_documentation']['size']>0) {
					$config['file_name'] = date('Yhs').'-'.$_FILES['additional_documentation']['name'];
					$config['upload_path'] = './uploads/person_photos/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx|xlsx|eml|msg';
					$config['max_size'] = '2048000';
					$this->load->library('upload');
					$this->upload->initialize($config);
					$this->upload->do_upload('planted_tree_location_photo');
					$photo = $this->upload->data();
					if($photo!=""){
						$data['additional_documentation']   = $photo['file_name'];
					}
			}  
			$data['location_of_picture']   = 'test';
			if ($_SESSION['login']['type'] == 'grant_user') {
				$data['stage3_status']  = '0';               //after update change it from 2 to 0
			}
			$this->db->update_batch('pct_grant_user_stage3', array($data), 'user_id');	
			$photo = '';
			$person_photos  = $_POST['person_data'];
			foreach($person_photos as $key => $val){
					$photo = '';
					  // check upload
					if ($_FILES['person_photos'.$key]['size']>0) {
							$config['file_name'] = date('Yhs').'-'.$_FILES['person_photos'.$key]['name'];
							$config['upload_path'] = './uploads/person_photos/';
							$config['allowed_types'] = 'jpg|jpeg|png|gif';
							$config['max_size'] = '2048000';
							$this->load->library('upload');
							$this->upload->initialize($config);
							$this->upload->do_upload('person_photos'.$key);
							$photo = $this->upload->data();
							$photo = $photo['file_name'];
							
					} 
					$sql = "SELECT * FROM  pct_stage3_person_photos WHERE person_id='".$key. "'";
					$query = $this->db->query($sql);
					$query1 = $query->result_array();
					
					if(!empty($query1)){
						if($photo!=""){
							$person['person_photo']  = $photo;
							$person['person_id']   = $key;
							$this->db->update_batch('pct_stage3_person_photos', array($person), 'person_id');
						}
					
					}else{
						$person['person_photo']     = $photo;
						$person['user_id']          = $user_id;
						$person['person_id']        = $key;
						$this->db->insert_batch('pct_stage3_person_photos', array($person));
					}
			}
				$location_photos  = $_POST['location_data'];
				foreach($location_photos as $key => $val){
							  // check upload
						if ($_FILES['location_photos'.$key]['size']>0) {
							$config['upload_path'] ='./uploads/person_photos/';
							$config['allowed_types'] ='jpg|jpeg|png|gif|pdf';
							$config['max_size']     = '2048000';
							$config['overwrite'] = TRUE;
							$fileCount = count($_FILES['location_photos'.$key]["name"]);
								for($i=0; $i < $fileCount; $i++){
									$data3['file_name']="";
									$_FILES['files']['name'] = date('Yhs').'-'.$_FILES["location_photos".$key]['name'][$i];
									$_FILES['files']['type'] = $_FILES["location_photos".$key]['type'][$i];
									$_FILES['files']['tmp_name'] = $_FILES["location_photos".$key]['tmp_name'][$i];
									$_FILES['files']['error'] = $_FILES["location_photos".$key]['error'][$i];
									$_FILES['files']['size'] = $_FILES["location_photos".$key]['size'][$i]; 
									
								$this->load->library('upload', $config);
								$this->upload->initialize($config);
								if (!$this->upload->do_upload('files'))      
									{  
										$data22222 = array('error' => $this->upload->display_errors());    
										//echo $data['error']; 
									}else{
										$data3 = $this->upload->data();
									}
									//echo '<pre>'; print_r($data); 
									if($data3['file_name']!=""){
										$location['location_photo']   = $data3['file_name'];
										$location['user_id']          = $user_id;
										$location['location_id']      = $key;
									   $this->db->insert_batch('pct_stage3_location_photos', array($location));
									}
								}
							
						} 
							
				}
				
			$videos_data  = $_POST['videos_data'];
			foreach($videos_data as $key => $val){
				
					$sql = "SELECT * FROM  pct_stage3_videos_links WHERE location_id='".$key."' AND user_id='".$user_id."'";
					$query = $this->db->query($sql);
					$query1 = $query->result_array();
					
					if(!empty($query1)){
						$videos['location_id']      = $key;
						$videos['video_link']       = $_POST['videos_links'.$key];
						$videos['additional_link']  = $_POST['additional_links'.$key];
						$this->db->update_batch('pct_stage3_videos_links', array($videos), 'location_id');
					}else{
						$videos['location_id']      = $key;
						$videos['user_id']          = $user_id;
						$videos['video_link']       = $_POST['videos_links'.$key];
						$videos['additional_link']  = $_POST['additional_links'.$key];
						$this->db->insert_batch('pct_stage3_videos_links', array($videos));
					}
				}
		}
		 /**
         * ============================================================================================================================================
        * function for update update_grant_user_stage3 grant user data 
        */
		function update_grant_user_stage3111() {          //old stage 3
		
			$user_id = $_SESSION['login']['id'];
			$location_of_picture = mysql_escape_string($this->input->post('location_of_picture'));
			$photos_google_point_picker = mysql_escape_string($this->input->post('photos_google_point_picker'));
			$team_youtube_link = mysql_escape_string($this->input->post('team_youtube_link'));
			$community_youtube_link = mysql_escape_string($this->input->post('community_youtube_link'));
			$tree_planted_youtube_link = mysql_escape_string($this->input->post('tree_planted_youtube_link'));
			$tree_planted_location = mysql_escape_string($this->input->post('tree_planted_location'));
			$videos_google_point_picker = mysql_escape_string($this->input->post('videos_google_point_picker'));

			$data = array();	
			$data['user_id']                      = $user_id;
            $data['location_of_picture']          = $location_of_picture;
            //$data['photos_google_point_picker']   = $photos_google_point_picker;
            $data['team_youtube_link']            = $team_youtube_link;
            $data['community_youtube_link']       = $community_youtube_link;
            $data['tree_planted_youtube_link']    = $tree_planted_youtube_link;
            $data['tree_planted_location']        = $tree_planted_location;
			$data['videos_google_point_picker']   = $videos_google_point_picker;
			
			$image_data1 = $this->input->post('community_photos');
			if(!empty($image_data1)){
				$file_name1 = date('Yhs').'_photo.png';
				$upload_path = './uploads/person_photos/'.$file_name1;
				list($type, $image_data1) = explode(';', $image_data1);
				list(, $image_data1)      = explode(',', $image_data1);
				$image_data1 = base64_decode($image_data1);
				file_put_contents($upload_path, $image_data1);
				
				$data['community_photo']   = $file_name1;
				
			}
			if ($_FILES['planted_tree_location_photo']['size']) {
					$config['file_name'] = date('Yhs').'-'.$_FILES['planted_tree_location_photo']['name'];
					$config['upload_path'] = './uploads/person_photos/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
					$config['max_size'] = '2048000';
					$this->load->library('upload');
					$this->upload->initialize($config);
					$this->upload->do_upload('planted_tree_location_photo');
					$photo = $this->upload->data();
					
					$data['planted_tree_location_photo']   = $photo['file_name'];
			 } 
			  if ($_FILES['additional_documentation']['size']) {
					$config['file_name'] = date('Yhs').'-'.$_FILES['additional_documentation']['name'];
					$config['upload_path'] = './uploads/person_photos/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx|xlsx|eml|msg';
					$config['max_size'] = '2048000';
					$this->load->library('upload');
					$this->upload->initialize($config);
					$this->upload->do_upload('planted_tree_location_photo');
					$photo = $this->upload->data();
					
					$data['planted_tree_location_photo']   = $photo['file_name'];
			} 
			$this->db->update_batch('pct_grant_user_stage3', array($data), 'user_id');	
			
		
			
			   if(!empty($_POST['person_photos'])){
				   $person_photos = $_POST['person_photos'];
				  foreach($person_photos as $key => $val){
						$image_data = $person_photos[$key]['image'];
						if(!empty($image_data)){
							$file_name = date('Yhs').$key.'_photo.png';
							$upload_path = './uploads/person_photos/'.$file_name;
							$image_data = $person_photos[$key]['image'];
							list($type, $image_data) = explode(';', $image_data);
							list(, $image_data)      = explode(',', $image_data);
							$image_data = base64_decode($image_data);
							file_put_contents($upload_path, $image_data);
						
							$photos['person_photo']         = $file_name;
							$sql = "SELECT * FROM  pct_stage3_person_photos WHERE person_id='". $person_photos[$key]['person_id']. "'";
							$query = $this->db->query($sql);
							$query1 = $query->result_array();
							
							if(!empty($query1)){
								$photos['person_id']   = $person_photos[$key]['person_id'];
								$this->db->update_batch('pct_stage3_person_photos', array($photos), 'person_id');
							}else{
								
								$photos['user_id']           	= $user_id;
								$photos['person_id']            = $person_photos[$key]['person_id'];
								$this->db->insert_batch('pct_stage3_person_photos', array($photos));
							}
							
						}
				   }
			  } 
		
		}
		/**
         * ============================================================================================================================================
        * function for Main insert_autoupload_clients
        */
        function insert_autoupload_clients() {
			
			$first_name = mysql_escape_string($this->input->post('first_name'));
			$last_name = mysql_escape_string($this->input->post('last_name'));
			$email_address = mysql_escape_string($this->input->post('email_address'));
			$company = mysql_escape_string($this->input->post('company'));
			$tree_nums = mysql_escape_string($this->input->post('tree_nums'));
            $twitter1 = mysql_escape_string($this->input->post('twitter1'));
            $twitter2 = mysql_escape_string($this->input->post('twitter2'));
            $twitter3 = mysql_escape_string($this->input->post('twitter3'));
            $date_birth = mysql_escape_string($this->input->post('date_birth'));
            $partner_id = mysql_escape_string($this->input->post('partner_id'));
			$unique_key = mysql_escape_string($this->input->post('unique_key'));

            $data = array();			
            $data['first_name']  = $first_name;
            $data['last_name']  = $last_name;
            $data['email_address']  = $email_address;
            $data['company']  = $company;
            $data['tree_nums']  = $tree_nums;
            $data['twitter1']  = $twitter1;
            $data['twitter2']  = $twitter2;
            $data['twitter3']  = $twitter3;
            $data['date_birth']  = $date_birth;
			$data['unique_key']  = $unique_key;
            $data['partner_id']  = $partner_id;
             $this->db->insert_batch('pct_imacroData', array($data));            
        }
		 /**
         * ============================================================================================================================================
        * function for Main initiative
        */
        function insert_initiative($logo, $invoice_company_logo) {
			
			$initiative_name = mysql_escape_string($this->input->post('initiative_name'));
			$local_name = mysql_escape_string($this->input->post('local_name'));
			$ini_certificate_name = mysql_escape_string($this->input->post('ini_certificate_name'));
			$website = mysql_escape_string($this->input->post('website'));
			$sender_email_title = mysql_escape_string($this->input->post('sender_email_title'));
			//$request_for_funds = mysql_escape_string($this->input->post('request_for_funds'));
			$activation_email = mysql_escape_string($this->input->post('activation_email'));
			$ini_counter_type = mysql_escape_string($this->input->post('ini_counter_type'));
			$cert_type 		= mysql_escape_string($this->input->post('cert_type'));
			$certificate_layout = mysql_escape_string($this->input->post('certificate_layout'));
            $certificate1 = mysql_escape_string($this->input->post('certificate1'));
            $certificate2 = mysql_escape_string($this->input->post('certificate2'));
            $certificate3 = mysql_escape_string($this->input->post('certificate3'));
            $certificate4 = mysql_escape_string($this->input->post('certificate4'));
            $social_text = mysql_escape_string($this->input->post('social_text'));
            $company_text = mysql_escape_string($this->input->post('company_text'));
            $user_text = mysql_escape_string($this->input->post('user_text'));
			
			$invoice_company_name 		= mysql_escape_string($this->input->post('invoice_company_name'));
			$invoice_company_address 	= mysql_escape_string($this->input->post('invoice_company_address'));
			$invoice_contact_details 	= mysql_escape_string($this->input->post('invoice_contact_details'));
			$invoice_bank_details 		= mysql_escape_string($this->input->post('invoice_bank_details'));						$invoice_name 				= mysql_escape_string($this->input->post('invoice_name'));

            $data = array();			
            $data['initiative_name']  = $initiative_name;
            $data['local_name']  = $local_name;
            $data['ini_certificate_name']  = $ini_certificate_name;
            $data['website']          = $website;
            $data['sender_email_title'] = $sender_email_title;
           // $data['request_for_funds'] = $request_for_funds;
            $data['activation_email'] = $activation_email;
            $data['ini_counter_type'] = $ini_counter_type;
            $data['cert_type'] 			= $cert_type;
            $data['token'] 				= strtolower($this->randomKey(20));
            $data['certificate_layout'] = $certificate_layout;
            $data['social_text']       = $social_text;
            $data['company_text']      = $company_text;
            $data['user_text']         = $user_text;
			$data['certificate1']      = $certificate1;
			$data['certificate2']      = $certificate2;
			$data['certificate3']      = $certificate3;
			$data['certificate4']      = $certificate4;
			$data['certificate4']      = $certificate4;
			$data['user_id']     	   = $_SESSION['login']['id'];
			
			$data['invoice_company_name']      		= $invoice_company_name;
			$data['invoice_company_address']      	= $invoice_company_address;
			$data['invoice_contact_details']      	= $invoice_contact_details;
			$data['invoice_bank_details']      		= $invoice_bank_details;						$data['invoice_name']      				= $invoice_name;
			
			if ($logo) {
				$data['bk_image']       = $logo;
			}
			
			if ($invoice_company_logo) {
				$data['invoice_company_logo']   = $invoice_company_logo;
			}
			
            $this->db->insert_batch('pct_initiative', array($data));
			$initiative_id = $this->db->insert_id();
			
			//echo "<pre>"; print_r($_POST); die;
			$records   = $_POST['data'];
			$emails = array();
			foreach($records as $key => $val){
				$emails['name']         = $records[$key]['name'];
				$emails['subject']      = $records[$key]['subject'];
				$emails['message']      = $records[$key]['message'];
				$emails['template_id']  = $key;
				$emails['user_id'] 		= $initiative_id;
				
			  $this->db->insert_batch('pct_initiative_email_templates', array($emails));
				
			}
            
        }
		
		function randomKey($length) {  //create no of random digit alphanumeric code
			$key='';
			$pool = array_merge(range(0,9),range('A', 'Z'));

			for($i=0; $i < $length; $i++) {
				$key .= $pool[mt_rand(0, count($pool) - 1)];
			}
			return $key;
		}
		
		 function update_initiative($id, $logo, $invoice_company_logo) {

			$initiative_name = mysql_escape_string($this->input->post('initiative_name'));
			$ini_certificate_name = mysql_escape_string($this->input->post('ini_certificate_name'));
			$local_name = mysql_escape_string($this->input->post('local_name'));
			$website = mysql_escape_string($this->input->post('website'));
			$sender_email_title = mysql_escape_string($this->input->post('sender_email_title'));
			$ini_counter_type = mysql_escape_string($this->input->post('ini_counter_type'));
			$cert_type = mysql_escape_string($this->input->post('cert_type'));
			$request_for_funds = mysql_escape_string($this->input->post('request_for_funds'));
			$activation_email = mysql_escape_string($this->input->post('activation_email'));
			$certificate_layout = mysql_escape_string($this->input->post('certificate_layout'));
            $certificate1 = mysql_escape_string($this->input->post('certificate1'));
            $certificate2 = mysql_escape_string($this->input->post('certificate2'));
            $certificate3 = mysql_escape_string($this->input->post('certificate3'));
            $certificate4 = mysql_escape_string($this->input->post('certificate4'));
			$social_text = mysql_escape_string($this->input->post('social_text'));
			$company_text = mysql_escape_string($this->input->post('company_text'));
			$user_text = mysql_escape_string($this->input->post('user_text'));
			
			$enable_invoice_data 		= $this->input->post('enable_invoice_data');
			$invoice_company_name 		= mysql_escape_string($this->input->post('invoice_company_name'));
			$invoice_company_address 	= mysql_escape_string($this->input->post('invoice_company_address'));
			$invoice_contact_details 	= mysql_escape_string($this->input->post('invoice_contact_details'));
			$invoice_bank_details 		= mysql_escape_string($this->input->post('invoice_bank_details'));						$invoice_name 		= mysql_escape_string($this->input->post('invoice_name'));

			$data = array();			
            $data['initiative_name']  		= $initiative_name;
            $data['local_name']  			= $local_name;
            $data['ini_certificate_name']  	= $ini_certificate_name;
            $data['website'] 		  		= $website;
			$data['sender_email_title'] 	= $sender_email_title;
			$data['request_for_funds'] 		= $request_for_funds;
            $data['activation_email']  		= $activation_email;
            $data['ini_counter_type']  		= $ini_counter_type;
            $data['cert_type']  			= $cert_type;
            $data['certificate_layout']  	= $certificate_layout;
			$data['social_text']       		= $social_text;
            $data['company_text']      		= $company_text;
            $data['user_text']          	= $user_text;
			$data['certificate1']      		= $certificate1;
			$data['certificate2']      		= $certificate2;
			$data['certificate3']      		= $certificate3;
			$data['certificate4']      		= $certificate4;
			
			$data['invoice_company_name']      		= $invoice_company_name;
			$data['invoice_company_address']      	= $invoice_company_address;
			$data['invoice_contact_details']      	= $invoice_contact_details;
			$data['invoice_bank_details']      		= $invoice_bank_details;						$data['invoice_name']      				= $invoice_name;
			
			if ($logo) {
				$data['bk_image']       	= $logo;
			}
			if ($invoice_company_logo) {
				$data['invoice_company_logo']   = $invoice_company_logo;
			}
			if(isset($enable_invoice_data) && $enable_invoice_data == '1'){
				$data['enable_invoice_data']   = '1';
			}else{
				$data['enable_invoice_data']   = '0';
			}
            $data['id']     	   = $id;
			$this->db->update_batch('pct_initiative', array($data), 'id');
			$records   = $_POST['data'];
			//echo "<pre>"; print_r($records); die;
			$emails = array();
			foreach($records as $key => $val){
				if(!empty($records[$key]['id'])){
					$emails['name']         = $records[$key]['name'];
					$emails['subject']      = $records[$key]['subject'];
					$emails['message']      = $records[$key]['message'];	
					$emails['id']     	    = $records[$key]['id'];
					if(isset($records[$key]['enable_email']) && $records[$key]['enable_email']=='1'){
						$emails['enable_email']   = '1';
					}else{
						$emails['enable_email']   = '0';
					}
				   $this->db->update_batch('pct_initiative_email_templates', array($emails), 'id');
				}else{
					/* $emails['name']         = $records[$key]['name'];
					$emails['subject']      = $records[$key]['subject'];
					$emails['message']      = $records[$key]['message'];	
					$emails['template_id']  = $key;
					$emails['user_id'] 		= $id; */
					if(isset($records[$key]['enable_email']) && $records[$key]['enable_email']=='1'){
						$enable_email   = '1';
					}else{
						$enable_email   = '0';
					}
					
					 $query = "INSERT INTO pct_initiative_email_templates SET
                        name = '{$records[$key]['name']}',
                        subject = '{$records[$key]['subject']}',
                        template_id = '{$key}',
                        message = '{$records[$key]['message']}',
						user_id = {$id},
						enable_email = '{$enable_email}'";
						$this->db->query($query);   
				    //$this->db->insert_batch('pct_initiative_email_templates', array($emails));
				} 
			
			}
            
        }
		/**
         * ============================================================================================================================================
        * function to update Global Email templates
        */
		function update_cv_thankyou_template() {
			
			$id           = $this->input->post('cv_id');
			$cv_message   = $this->input->post('cv_message');
			$cv_subject      = $this->input->post('cv_subject');
			$cv_name      = $this->input->post('cv_name');
			$cv = array();
			$cv['id']      		= $id;	
			$cv['message']      = $cv_message;	
			$cv['name']     	= $cv_name;	
			$cv['subject']     	= $cv_subject;	
			$this->db->update_batch('pct_global_email_templates', array($cv), 'id');
		}
		/**
         * ============================================================================================================================================
        * function to update Global Email templates
        */
		function update_global_email_templates($set_id) {
			
				$records   = $_POST['data'];
				//echo "<pre>"; print_r($records); die;
				$emails = array();
				foreach($records as $key => $val){
					if(!empty($records[$key]['id'])){
						$emails['name']         = $records[$key]['name'];
						$emails['subject']      = $records[$key]['subject'];
						$emails['message']      = $records[$key]['message'];	
						$emails['time_period']  = $records[$key]['time_period'];		
						$emails['id']     	    = $records[$key]['id'];
						if(isset($records[$key]['enable_email']) && $records[$key]['enable_email']=='1'){
							$emails['enable_email']   = '1';
						}else{
							$emails['enable_email']   = '0';
						}
					    $this->db->update_batch('pct_global_email_templates', array($emails), 'id');
					}else{
						 $query = "INSERT INTO pct_global_email_templates SET
							name = '{$records[$key]['name']}',
							subject = '{$records[$key]['subject']}',
							template_id = '{$key}',
							time_period = '{$records[$key]['time_period']}',
							enable_email = '{$records[$key]['enable_email']}',
							set_id = '{$set_id}',
							message = '{$records[$key]['message']}'";
							$this->db->query($query);   
					} 
				}
        }
		 /**
         * ============================================================================================================================================
        * function for sub level initiative
        */
        function insert_sublevel_initiative($logo_name='') {
			
			$initiative = mysql_escape_string($this->input->post('initiative'));
            $main_title      = mysql_escape_string($this->input->post('main_title'));
            $sub_title      = mysql_escape_string($this->input->post('sub_title'));
			$address      = mysql_escape_string($this->input->post('address'));
            $email      = mysql_escape_string($this->input->post('email'));
            $partner_name = mysql_escape_string($this->input->post('partner_name'));
			$tree_nums  = mysql_escape_string($this->input->post('tree_nums'));
			$code       = mysql_escape_string($this->input->post('code'));
			$first_name = mysql_escape_string($this->input->post('first_name'));
            $last_name  = mysql_escape_string($this->input->post('last_name'));
            $phone      = mysql_escape_string($this->input->post('phone'));
			$twitter      = mysql_escape_string($this->input->post('twitter'));
			$twitter_hashtag = mysql_escape_string($this->input->post('twitter_hashtag'));
            $website    = mysql_escape_string($this->input->post('website'));	
			$opportunity_str  = mysql_escape_string($this->input->post('opportunity_str'));
			$tli_id  = mysql_escape_string($this->input->post('tli_id'));
			$brand  = mysql_escape_string($this->input->post('brand'));
            $data       = array();
			    						
            $data['initiative']      = $initiative;
			$data['email']           = $email;
			$data['sub_level_partner']   = $partner_name;
            $data['title']          = $main_title;
            $data['subtitle']       = $sub_title;
            $data['tree_nums']      = $tree_nums;
            $data['logo']     	    = $logo_name;
            $data['code']     	    = $code;
			$data['first_name']     = $first_name;
            $data['last_name']      = $last_name;
			$data['address']        = $address;
            $data['phone']          = $phone;
			$data['twitter']        = $twitter;
			$data['twitter_hashtag']   = $twitter_hashtag;
			$data['website']        = $website;
			$data['brand']         = $brand;
			
            $data['opportunity_str']  = $opportunity_str;
            $this->db->insert_batch('pct_sublevel_initiative', array($data));
            
        }
		 function update_sublevel_initiative($id,$logo_name='') {

            $initiative = mysql_escape_string($this->input->post('initiative'));
            $main_title      = mysql_escape_string($this->input->post('main_title'));
            $sub_title      = mysql_escape_string($this->input->post('sub_title'));
			$address      = mysql_escape_string($this->input->post('address'));
            $email      = mysql_escape_string($this->input->post('email'));
            $partner_name = mysql_escape_string($this->input->post('partner_name'));
			$tree_nums  = mysql_escape_string($this->input->post('tree_nums'));
			$code       = mysql_escape_string($this->input->post('code'));
			$first_name = mysql_escape_string($this->input->post('first_name'));
            $last_name  = mysql_escape_string($this->input->post('last_name'));
            $phone      = mysql_escape_string($this->input->post('phone'));
			$twitter      = mysql_escape_string($this->input->post('twitter'));
			$twitter_hashtag = mysql_escape_string($this->input->post('twitter_hashtag'));
            $website    = mysql_escape_string($this->input->post('website'));	
			$opportunity_str  = mysql_escape_string($this->input->post('opportunity_str'));
			$tli_id  = mysql_escape_string($this->input->post('tli_id'));
			$brand  = mysql_escape_string($this->input->post('brand'));
            $data       = array();
            $data['initiative']      = $initiative;
			$data['email']           = $email;
			$data['sub_level_partner']   = $partner_name;
            $data['title']          = $main_title;
            $data['subtitle']       = $sub_title;
            $data['tree_nums']      = $tree_nums;
            $data['code']     	    = $code;
			$data['first_name']     = $first_name;
            $data['last_name']      = $last_name;
			$data['address']        = $address;
            $data['phone']          = $phone;
			$data['twitter']        = $twitter;
			$data['twitter_hashtag']   = $twitter_hashtag;
			$data['website']        = $website;
            $data['opportunity_str']  = $opportunity_str;
			$data['brand']         = $brand;
			
			$data['tli_id']        = $tli_id;
			if($logo_name){
				$data['logo']      = $logo_name;
			}
            $data['id']     	   = $id;
			$this->db->update_batch('pct_sublevel_initiative', array($data), 'id');
            
        }
		function list_sub_level_initiative(){
			// load affiliates 
            $query = "SELECT * FROM pct_sublevel_initiative";
            $rows1 = $this->db->query($query)->result();
			
            return array_merge($rows1);	
		}
		 /**
         * ============================================================================================================================================
        * function for Top level initiative
        */
        function insert_toplevel_initiative($logo_name='') {
			
			$initiative = mysql_escape_string($this->input->post('initiative'));
            $main_title      = mysql_escape_string($this->input->post('main_title'));
            $sub_title      = mysql_escape_string($this->input->post('sub_title'));
			$address      = mysql_escape_string($this->input->post('address'));
            $email      = mysql_escape_string($this->input->post('email'));
            $partner_name = mysql_escape_string($this->input->post('partner_name'));
			$tree_nums  = mysql_escape_string($this->input->post('tree_nums'));
			$code       = mysql_escape_string($this->input->post('code'));
			$first_name = mysql_escape_string($this->input->post('first_name'));
            $last_name  = mysql_escape_string($this->input->post('last_name'));
            $phone      = mysql_escape_string($this->input->post('phone'));
            $twitter      = mysql_escape_string($this->input->post('twitter'));
            $twitter_hashtag = mysql_escape_string($this->input->post('twitter_hashtag'));
            $website    = mysql_escape_string($this->input->post('website'));	
			$opportunity_str  = mysql_escape_string($this->input->post('opportunity_str'));
            $data       = array();
			    			
            $data['initiative']      = $initiative;
			$data['email']           = $email;
			$data['top_level_partner']   = $partner_name;
            $data['title']          = $main_title;
            $data['subtitle']       = $sub_title;
            $data['tree_nums']      = $tree_nums;
            $data['logo']     	    = $logo_name;
            $data['code']     	    = $code;
			$data['first_name']     = $first_name;
            $data['last_name']      = $last_name;
			$data['address']        = $address;
            $data['phone']          = $phone;
            $data['twitter']        = $twitter;
            $data['twitter_hashtag']   = $twitter_hashtag;
			$data['website']        = $website;
            $data['opportunity_str']  = $opportunity_str;
            $this->db->insert_batch('pct_toplevel_initiative', array($data));
            
        }
		 function update_toplevel_initiative($id,$logo_name='') {

            $initiative = mysql_escape_string($this->input->post('initiative'));
            $main_title      = mysql_escape_string($this->input->post('main_title'));
            $sub_title      = mysql_escape_string($this->input->post('sub_title'));
			$address      = mysql_escape_string($this->input->post('address'));
            $email      = mysql_escape_string($this->input->post('email'));
            $partner_name = mysql_escape_string($this->input->post('partner_name'));
			$tree_nums  = mysql_escape_string($this->input->post('tree_nums'));
			$code       = mysql_escape_string($this->input->post('code'));
			$first_name = mysql_escape_string($this->input->post('first_name'));
            $last_name  = mysql_escape_string($this->input->post('last_name'));
            $phone      = mysql_escape_string($this->input->post('phone'));
			$twitter      = mysql_escape_string($this->input->post('twitter'));
			$twitter_hashtag  = mysql_escape_string($this->input->post('twitter_hashtag'));
            $website    = mysql_escape_string($this->input->post('website'));	
			$opportunity_str  = mysql_escape_string($this->input->post('opportunity_str'));
            $data       = array();
            $data['initiative']      = $initiative;
			$data['email']           = $email;
			$data['top_level_partner']   = $partner_name;
            $data['title']          = $main_title;
            $data['subtitle']       = $sub_title;
            $data['tree_nums']      = $tree_nums;
            $data['code']     	    = $code;
			$data['first_name']     = $first_name;
            $data['last_name']      = $last_name;
			$data['address']        = $address;
            $data['phone']          = $phone;
			$data['twitter']        = $twitter;
			$data['twitter_hashtag']   = $twitter_hashtag;
			$data['website']        = $website;
            $data['opportunity_str']  = $opportunity_str;
			if($logo_name){
				$data['logo']      = $logo_name;
			}
            $data['id']     	   = $id;
			$this->db->update_batch('pct_toplevel_initiative', array($data), 'id');
            
        }
		function list_initiative(){
			// load affiliates 
            $query = "SELECT * FROM pct_initiative ORDER BY ID ASC";
            $rows1 = $this->db->query($query)->result();
			
            return array_merge($rows1);	
		}
		
		function list_editor_initiatives($user_id){
			// load affiliates 
            $query = "SELECT * FROM pct_initiative where user_id=$user_id";
            $rows1 = $this->db->query($query)->result();
            return array_merge($rows1);	
		}
		 function list_top_level_initiative(){
			// load affiliates 
            $query = "SELECT * FROM pct_toplevel_initiative";
            $rows1 = $this->db->query($query)->result();
			
            return array_merge($rows1);	
		}
		function load_main_initiative($id) {
            $query = "SELECT * FROM pct_initiative WHERE id=$id";
            $row = $this->db->query($query);
            $data = $row->row_array();
            if ($data) return $data;
            else show_error('This record does not exists', 500 );
        }
		function load_sub_level_initiative($id) {
            $query = "SELECT * FROM pct_sublevel_initiative WHERE id=$id";
            $row = $this->db->query($query);
            $data = $row->row_array();
            if ($data) return $data;
            else show_error('This record does not exists', 500 );
        }
		function load_top_level_initiative($id) {
            $query = "SELECT * FROM pct_toplevel_initiative WHERE id=$id";
            $row = $this->db->query($query);
            $data = $row->row_array();
            if ($data) return $data;
            else show_error('This record does not exists', 500 );
        }
		
         /**
         * ============================================================================================================================================
        * function for corporate partner
        */
        function insert_corporate_partner($logo_name='') {
			
            $initiative = mysql_escape_string($this->input->post('initiative'));
            $opportunity_str = mysql_escape_string($this->input->post('opportunity_str'));
            
            $main_title      = mysql_escape_string($this->input->post('main_title'));
            $sub_title      = mysql_escape_string($this->input->post('sub_title'));
            $address      = mysql_escape_string($this->input->post('address'));
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $partner_name = mysql_escape_string($this->input->post('partner_name'));
            $first_name = mysql_escape_string($this->input->post('first_name'));
            $last_name  = mysql_escape_string($this->input->post('last_name'));
            $phone      = mysql_escape_string($this->input->post('phone'));
            $tree_nums  = mysql_escape_string($this->input->post('tree_nums'));

            $website    = mysql_escape_string($this->input->post('website'));
            $code       = mysql_escape_string($this->input->post('code'));
            
            $data       = array();
            
            $data['email']      = $email;
            // $data['username']   = $username;
            // $data['password']   = md5($password);
            // $data['type']       = 'restaurant';
            // $data['active']     = 1;
            
            // $data['price']      = $price;
            // $data['tax']        = $tax;
            // $data['currency']   = $currency;
            //$this->db->insert_batch('pct_users', array($data));
            
           // $user_id    = $this->db->insert_id();
		 			
            $data       = array();
			$data['email']      = $email;
            $data['title']      = $main_title;
            $data['subtitle']      = $sub_title;
            $data['address']      = $address;
            $data['initiative']      = $initiative;
            $data['opportunity_str'] = $opportunity_str;
            $data['code'] = $code;
            
            //$data['user_id']        = $user_id;
            $data['website']        = $website;
            //
            $data['corporate_partner']     = $partner_name;
            $data['first_name']     = $first_name;
            $data['last_name']      = $last_name;
            $data['phone']          = $phone;
            $data['tree_nums']      = $tree_nums;
            $data['logo']      = $logo_name;
            //$data['new']            = $new;
            $this->db->insert_batch('pct_corporate_partner', array($data));
            
            // insert referer code
            // $data2 = array();
            // $data2['code']           = $code;
            // $data2['user_id']        = $user_id;
            // $this->insert_referer($data2);
        }
		
		  /**
         * ============================================================================================================================================
        * function for update corporate partner
        */
        function update_corporate_partner($id,$logo_name) {
			
            $initiative = mysql_escape_string($this->input->post('initiative'));
            $opportunity_str = mysql_escape_string($this->input->post('opportunity_str'));
            
            $main_title      = mysql_escape_string($this->input->post('main_title'));
            $sub_title      = mysql_escape_string($this->input->post('sub_title'));
            $address      = mysql_escape_string($this->input->post('address'));
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $partner_name = mysql_escape_string($this->input->post('partner_name'));
            $first_name = mysql_escape_string($this->input->post('first_name'));
            $last_name  = mysql_escape_string($this->input->post('last_name'));
            $phone      = mysql_escape_string($this->input->post('phone'));
            $tree_nums  = mysql_escape_string($this->input->post('tree_nums'));
            $website    = mysql_escape_string($this->input->post('website'));
            $code       = mysql_escape_string($this->input->post('code'));
            
            $data       = array();
            
            $data['email']      = $email;
		 			
            $data       = array();
			$data['email']      = $email;
            $data['title']      = $main_title;
            $data['subtitle']      = $sub_title;
            $data['address']      = $address;
            $data['initiative']      = $initiative;
            $data['opportunity_str'] = $opportunity_str;
            $data['code'] = $code;
            //$data['user_id']        = $user_id;
            $data['website']        = $website;
            //
            $data['corporate_partner']     = $partner_name;
            $data['first_name']     = $first_name;
            $data['last_name']      = $last_name;
            $data['phone']          = $phone;
            $data['tree_nums']      = $tree_nums;
            $data['id']      = $id;
	
			if($logo_name){
				$data['logo']      = $logo_name;
			}
            $this->db->update_batch('pct_corporate_partner', array($data), 'id');
        }
        function list_corporate_partner(){
			// load affiliates 
            $query = "SELECT * FROM pct_corporate_partner";
            $rows1 = $this->db->query($query)->result();
			
            return array_merge($rows1);	
		}
		
		function load_corporate_partner($id) {
            $query = "SELECT * FROM pct_corporate_partner WHERE id=$id";
            $row = $this->db->query($query);
            $data = $row->row_array();
            if ($data) return $data;
            else show_error('This record does not exists', 500 );
        }
		
		
        /**
         * ============================================================================================================================================
        * function for restaurant
        */
        function insert_restaurant() {
            $initiative = mysql_escape_string($this->input->post('initiative'));
            $opportunity_str = mysql_escape_string($this->input->post('opportunity_str'));
            
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $new        = mysql_escape_string($this->input->post('new'));
            $password   = mysql_escape_string($this->input->post('password'));
            $restaurant = mysql_escape_string($this->input->post('restaurant'));
            $first_name = mysql_escape_string($this->input->post('first_name'));
            $last_name  = mysql_escape_string($this->input->post('last_name'));
            $phone      = mysql_escape_string($this->input->post('phone'));
            $tree_nums  = mysql_escape_string($this->input->post('tree_nums'));
            //
            $price      = mysql_escape_string($this->input->post('price'));
            $currency   = mysql_escape_string($this->input->post('currency'));
            $tax        = mysql_escape_string($this->input->post('tax'));
            $website    = mysql_escape_string($this->input->post('website'));
            $code       = mysql_escape_string($this->input->post('code'));
            
            $data       = array();
            
            $data['email']      = $email;
            $data['username']   = $username;
            $data['password']   = md5($password);
            $data['type']       = 'restaurant';
            $data['active']     = 1;
            //
            $data['price']      = $price;
            $data['tax']        = $tax;
            $data['currency']   = $currency;
            $this->db->insert_batch('pct_users', array($data));
            
            $user_id    = $this->db->insert_id();
            $data       = array();
            $data['initiative']      = $initiative;
            $data['opportunity_str'] = stripslashes($opportunity_str);
            
            $data['user_id']        = $user_id;
            $data['initiative_editor_id'] = $_SESSION['login']['id'];
            $data['website']        = $website;
            //
            $data['restaurant']     = stripslashes($restaurant);
            $data['first_name']     = $first_name;
            $data['last_name']      = $last_name;
            $data['phone']          = $phone;
            $data['tree_nums']      = $tree_nums;
            $data['new']            = $new;
            $this->db->insert_batch('pct_restaurants', array($data));
            
            // insert referer code
            $data2 = array();
            $data2['code']           = $code;
            $data2['user_id']        = $user_id;
            $this->insert_referer($data2);
        }
        
        // for admin and restaurant partners
        function update_restaurant($id, $logo) {
            $initiative = mysql_escape_string($this->input->post('initiative'));
            $opportunity_str = mysql_escape_string($this->input->post('opportunity_str'));
            
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $new        = mysql_escape_string($this->input->post('new'));
            $send_email_immediately = mysql_real_escape_string($this->input->post('send_email_immediately'));
            $request_for_funds = mysql_real_escape_string($this->input->post('request_for_funds'));
            $auto_input_email = mysql_real_escape_string($this->input->post('auto_input_email'));
            $password   = mysql_escape_string($this->input->post('password'));
            $restaurant = mysql_escape_string($this->input->post('restaurant'));
            $first_name = mysql_escape_string($this->input->post('first_name'));
            $last_name  = mysql_escape_string($this->input->post('last_name'));
            $phone      = mysql_escape_string($this->input->post('phone'));
            $tree_nums  = mysql_escape_string($this->input->post('tree_nums'));
            $active     = mysql_escape_string($this->input->post('active'));
            
            // expend
            $address    = mysql_escape_string($this->input->post('address'));
            $city       = mysql_escape_string($this->input->post('city'));
            $state      = mysql_escape_string($this->input->post('state'));
            $post_code  = mysql_escape_string($this->input->post('post_code'));
            $website    = mysql_escape_string($this->input->post('website'));
            
            //
            $price      = mysql_escape_string($this->input->post('price'));
            $currency   = mysql_escape_string($this->input->post('currency'));
            $tax        = mysql_escape_string($this->input->post('tax'));
            //
			$subject    = $this->input->post('subject');
			$cc_email    = $this->input->post('cc_email');
            $message    = $this->input->post('message');
            $certificate_text        = $this->input->post('certificate_text');
            $first_name2    = mysql_escape_string($this->input->post('first_name2'));
            $email2         = mysql_escape_string($this->input->post('email2'));
            
            
            $data       = array();
            $data['email']      = $email;
            $data['username']   = $username;
            $data['id']         = $id;
            if ($password) {
                $data['password']   = md5($password);   
            }
            if ($_SESSION['login']['type']=='admin') {
                $data['active']     = $active;
                //
                $data['price']      = $price;
                $data['tax']        = $tax;
                $data['currency']   = $currency;
            }
            
            $this->db->update_batch('pct_users', array($data), 'id');
            
            // update pct_restaurant
            $data       = array();
            $data['initiative']     = $initiative;
            $data['opportunity_str'] = stripslashes($opportunity_str);
            
            $data['restaurant']     = stripslashes($restaurant);
            $data['first_name']     = $first_name;
            $data['last_name']      = $last_name;
            $data['phone']          = $phone;
            // expend
            $data['address']        = $address;
            $data['city']           = $city;
            $data['state']          = $state;
            $data['post_code']      = $post_code;
            $data['website']        = $website;
            $data['user_id']        = $id;
            //
            $data['first_name2']     = $first_name2;
            $data['email2']     = $email2;
            
            if ($logo) {
                $data['logo']       = $logo;
            }
            if ($_SESSION['login']['type']=='admin') {
                $data['tree_nums']  = $tree_nums;
                $data['new']        = $new;
                $data['send_email_immediately'] = $send_email_immediately;
                $data['request_for_funds'] = $request_for_funds;
                $data['auto_input_email'] = $auto_input_email;
				$data['subject']      = $subject;
                $data['message']      = $message;
				$data['cc_email']      = $cc_email;
                $data['certificate_text']      = $certificate_text;
            }
            $this->db->update_batch('pct_restaurants', array($data), 'user_id');
        }
        
        
        function load_restaurant($id) {
            $query = "SELECT re.*, u.id AS id, u.email AS email, u.username AS username, u.active AS active, u.tax AS tax, u.currency AS currency, u.price AS price, r.code AS code
                        FROM pct_restaurants AS re
                        INNER JOIN pct_users AS u
                        ON u.id=re.user_id
                        INNER JOIN pct_referer AS r
                        ON r.user_id=re.user_id
                        WHERE u.id=$id";
            $row = $this->db->query($query);
            $data = $row->row_array();
            if ($data) return $data;
            else show_error('This record does not exists', 500 );
        }
		function get_restaurant_using_referid($id) {
            $query = "SELECT re.*, u.id AS id, u.email AS email, u.username AS username, u.active AS active, u.tax AS tax, u.currency AS currency, u.price AS price, r.code AS code
                        FROM pct_restaurants AS re
                        INNER JOIN pct_users AS u
                        ON u.id=re.user_id
                        INNER JOIN pct_referer AS r
                        ON r.user_id=re.user_id
                        WHERE u.id=$id";
            $row = $this->db->query($query);
            $data = $row->row_array();
            if ($data) return $data;
            else show_error('This record does not exists', 500 );
        }
		
        
        function load_figures($user_id) {
            $query = "SELECT SUM(trees) AS trees FROM pct_figures WHERE user_id=$user_id";
            $row = $this->db->query($query)->row_array();
            return $row['trees'];
        }
        
        function insert_a_figure($data) {
            $query = "INSERT INTO pct_figures SET
                        user_id = {$data['user_id']},
                        trees = {$data['trees']},
                        month = {$data['month']},
                        date_added = NOW(),
						latest_certificate = '{$data["latest_certificate"]}',
						code_snippet = '{$data["code_snippet"]}'";
            return $this->db->query($query);          
        }
		
		function get_latest_certificate_data($field_name, $user_id)
		{
			$sql = "SELECT $field_name FROM pct_figures WHERE user_id=$user_id ORDER BY id DESC LIMIT 0,1";
            $query = $this->db->query($sql);
            if ( $query->num_rows() > 0) {
				$row = $query->row_array();
				return $row[$field_name];
            } else {
                return "No Certificate";
            }
		}
        
        function has_figure($month, $user_id) {
            $sql = "SELECT * FROM pct_figures WHERE month='$month' AND user_id=$user_id";
            $query = $this->db->query($sql);
            if ( $query->num_rows() > 0) {
                return true;
            } else {
                return false;
            }
        }
        
        function update_restaurant_state($user_id, $new=0) {
            // 
            $query = "UPDATE pct_restaurants SET new=$new WHERE user_id=$user_id";
            return $this->db->query($query);
        }
             /**
         * ============================================================================================================================================
         * function for initiative 
        */
		
        function insert_initiative_partner($logo) {
            $initiative = mysql_escape_string($this->input->post('initiative'));
           // $opportunity_str = mysql_escape_string($this->input->post('opportunity_str'));
            
            $dashboard_user_email      = mysql_escape_string($this->input->post('dashboard_user_email'));
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $company    = $this->input->post('company');
            $password   = mysql_escape_string($this->input->post('password'));
            $website    = mysql_escape_string($this->input->post('website'));
            $first_name = $this->input->post('first_name');
            $send_invoices = mysql_escape_string($this->input->post('send_invoices'));
            $monthly_reminder_email = mysql_escape_string($this->input->post('monthly_reminder_email'));
            $free_trial = mysql_escape_string($this->input->post('free_trial'));
            $last_name  = $this->input->post('last_name');
            $phone      = mysql_escape_string($this->input->post('phone'));
            $tree_nums  = mysql_escape_string($this->input->post('tree_nums'));
            //
            $price      = mysql_escape_string($this->input->post('price'));
            $currency   = mysql_escape_string($this->input->post('currency'));
            $tax        = mysql_escape_string($this->input->post('tax'));
            $code        = mysql_escape_string($this->input->post('code'));
            $tech_support_email        = mysql_escape_string($this->input->post('tech_support_email'));
            $delay_payment_by        = mysql_escape_string($this->input->post('delay_payment_by'));
            
            
            $data       = array();
            $data['email']      = $email;
            $data['dashboard_user_email'] = $dashboard_user_email;
            $data['username']   = $username;
            $data['password']   = md5($password);
            $data['type']       = 'intitiative';
            $data['active']     = 1;
            
            $data['price']      = $price;
            $data['tax']        = $tax;
            $data['currency']   = $currency;
            $this->db->insert_batch('pct_users', array($data));
            
            $user_id    = $this->db->insert_id();
            $data       = array();
            $data['initiative']     = $initiative;
            //$data['opportunity_str'] = $opportunity_str;
            
            $data['user_id']        = $user_id;
            $data['company']        = $company;
            $data['first_name']     = $first_name;
            $data['send_invoices']  = $send_invoices;
            $data['monthly_reminder_email']  = $monthly_reminder_email;
            $data['free_trial']  	= $free_trial;
            $data['last_name']      = $last_name;
            $data['phone']          = $phone;
            $data['tree_nums']      = $tree_nums;
            $data['website']        = $website;
            $data['delay_payment_by']       = $delay_payment_by;
            $data['tech_support_email']   = $tech_support_email;
			if ($logo) {
                $data['logo']       = $logo;
            }
            $this->db->insert_batch('pct_initiative_partner', array($data));
            
            // insert referer code
            $data2 = array();
            $data2['user_id']        = $user_id;
            $data2['code']           = $code;
            $this->insert_referer($data2);
        }
        /**
         * ============================================================================================================================================
         * function for bulk partner 
        */
        function insert_bulk_partner() {
            $initiative = mysql_escape_string($this->input->post('initiative'));
            $opportunity_str = mysql_escape_string($this->input->post('opportunity_str'));
            
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $company    = mysql_escape_string($this->input->post('company'));
            $password   = mysql_escape_string($this->input->post('password'));
            $website    = mysql_escape_string($this->input->post('website'));
            $first_name = mysql_escape_string($this->input->post('first_name'));
            $last_name  = mysql_escape_string($this->input->post('last_name'));
            $phone      = mysql_escape_string($this->input->post('phone'));
            $tree_nums  = mysql_escape_string($this->input->post('tree_nums'));
            
            //
            $price      = mysql_escape_string($this->input->post('price'));
            $currency   = mysql_escape_string($this->input->post('currency'));
            $tax        = mysql_escape_string($this->input->post('tax'));
            $code        = mysql_escape_string($this->input->post('code'));
            
            
            $data       = array();
            $data['email']      = $email;
            $data['username']   = $username;
            $data['password']   = md5($password);
            $data['type']       = 'bulk_partner';
            $data['active']     = 1;
            
            $data['price']      = $price;
            $data['tax']        = $tax;
            $data['currency']   = $currency;
            $this->db->insert_batch('pct_users', array($data));
            
            $user_id    = $this->db->insert_id();
            $data       = array();
            $data['initiative']     = $initiative;
            $data['opportunity_str'] = $opportunity_str;
            
            $data['user_id']        = $user_id;
            $data['company']        = $company;
            $data['first_name']     = $first_name;
            $data['last_name']      = $last_name;
            $data['phone']          = $phone;
            $data['tree_nums']      = $tree_nums;
            $data['website']        = $website;
            $this->db->insert_batch('pct_bulk_partners', array($data));
            
            // insert referer code
            $data2 = array();
            $data2['user_id']        = $user_id;
            $data2['code']           = $code;
            $this->insert_referer($data2);
        }
        
        // for admin and restaurant partners
        function update_bulk_partner($id, $logo) {
            $initiative = mysql_escape_string($this->input->post('initiative'));
            $opportunity_str = mysql_escape_string($this->input->post('opportunity_str'));
            
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $company    = mysql_escape_string($this->input->post('company'));
            $password   = mysql_escape_string($this->input->post('password'));
            $website    = mysql_escape_string($this->input->post('website'));
            $first_name = mysql_escape_string($this->input->post('first_name'));
            $last_name  = mysql_escape_string($this->input->post('last_name'));
            $phone      = mysql_escape_string($this->input->post('phone'));
            $tree_nums  = mysql_escape_string($this->input->post('tree_nums'));
            $active     = mysql_escape_string($this->input->post('active'));
            $send_email_immediately = mysql_escape_string($this->input->post('$send_email_immediately'));
            
            // expend
            $address    = mysql_escape_string($this->input->post('address'));
            $city       = mysql_escape_string($this->input->post('city'));
            $state      = mysql_escape_string($this->input->post('state'));
            $post_code  = mysql_escape_string($this->input->post('post_code'));
            //
            $price      = mysql_escape_string($this->input->post('price'));
            $currency   = mysql_escape_string($this->input->post('currency'));
            $tax        = mysql_escape_string($this->input->post('tax'));
            $subject    = $this->input->post('subject');
			$cc_email    = $this->input->post('cc_email');
			$cc_people_email    = $this->input->post('cc_people_email');
            $message    = $this->input->post('message');
            $certificate_text        = $this->input->post('certificate_text');
            
            // 
            $email2        = mysql_escape_string($this->input->post('email2'));
            $first_name2   = mysql_escape_string($this->input->post('first_name2'));
            
            
            $data       = array();
            $data['email']      = $email;
            $data['username']   = $username;
            $data['id']         = (int)$id;
            if ($password) {
                $data['password']   = md5($password);   
            }
            
            
            // $data['active'] = (int)$active;
            if ($_SESSION['login']['type']=='admin') {
                $data['active']     = (int) $active;
                //
                $data['price']      = $price;
                $data['tax']        = $tax;
                $data['currency']   = $currency;
            }
            $this->db->update_batch('pct_users', array($data), 'id');
            
            // update pct_bulk_partner
            $data       = array();
            $data['initiative']     = $initiative;
            $data['opportunity_str'] = $opportunity_str;
            
            $data['company']        = $company;
            $data['first_name']     = $first_name;
            $data['last_name']      = $last_name;
            $data['phone']          = $phone;
            
            // expend
            $data['address']        = $address;
            $data['city']           = $city;
            $data['state']          = $state;
            $data['post_code']      = $post_code;
            
            //
            $data['first_name2']     = $first_name2;
            $data['email2']     = $email2;
            
            if ($_SESSION['login']['type']=='admin') {
                $data['tree_nums']      = $tree_nums;
                //
                $data['subject']      = $subject;
                $data['message']      = $message;
				$data['cc_email']      = $cc_email;
				$data['cc_people_email']      = $cc_people_email;
                $data['certificate_text']      = $certificate_text;
                $data['send_email_immediately'] = $send_email_immediately;
            }
            
            $data['user_id']        = $id;
            $data['website']        = $website;
            if ($logo) {
                $data['logo']       = $logo;
            }
            $this->db->update_batch('pct_bulk_partners', array($data), 'user_id');
        }
        
        function load_bulk_partner($id) {
            $query = "SELECT bp.*, u.id AS id, u.email AS email, u.username AS username, u.active AS active, u.tax AS tax, u.price AS price, u.currency AS currency, r.code AS code, (SELECT SUM(trees) FROM pct_invoices WHERE user_id=u.id) AS tree_nums2
                        FROM pct_bulk_partners AS bp
                        INNER JOIN pct_users AS u
                        ON u.id=bp.user_id
                        INNER JOIN pct_referer AS r
                        ON r.user_id=bp.user_id
                        WHERE u.id=$id";
            $row = $this->db->query($query);
            $data = $row->row_array();
            if ($data) return $data;
            else show_error('This record does not exists', 500 );
        }
		function check_random_number_exist($random_token) {
            $query = "SELECT * from pct_partner_hubspot_monthly_figures WHERE random_token='".$random_token."' AND status='0'";
            $row = $this->db->query($query);
            $data = $row->row_array();
            if ($data) return $data;
            else show_error('This record does not exists', 500 );
        }
		
		 function update_intitiative_partner_password($id) {
			 
			    $username   = mysql_escape_string($this->input->post('username'));
                $password   = mysql_escape_string($this->input->post('password'));
				
				$data       = array();
				$data['username']   = $username;
				$data['id']         = (int)$id;
				if ($password) {
					$data['password']   = md5($password);   
				} 
				$this->db->update_batch('pct_users', array($data), 'id');
		 }
		 // for admin and restaurant partners
        function update_intitiative_partner($id, $logo, $cert) {
			 if ($_SESSION['login']['type'] == 'admin') {
				$initiative = mysql_escape_string($this->input->post('initiative'));
				$send_invoices = mysql_escape_string($this->input->post('send_invoices'));
				$monthly_reminder_email = mysql_escape_string($this->input->post('monthly_reminder_email'));
				$free_trial = mysql_escape_string($this->input->post('free_trial'));
				$tech_support_email = mysql_escape_string($this->input->post('tech_support_email'));
				$dashboard_user_email = mysql_escape_string($this->input->post('dashboard_user_email'));
				$no_of_restaurants = mysql_escape_string($this->input->post('no_of_restaurants'));
				$sender_email = mysql_escape_string($this->input->post('sender_email'));
				$replyto_email = mysql_escape_string($this->input->post('replyto_email'));
				$attach_cert_inemails = mysql_escape_string($this->input->post('attach_cert_inemails'));
				$send_email_immediately = mysql_escape_string($this->input->post('send_email_immediately'));
				$delay_payment_by = mysql_escape_string($this->input->post('delay_payment_by'));
				$grid_upload = mysql_escape_string($this->input->post('grid_upload'));
				$counter_type = $this->input->post('counter_type');
				$hs_company_id = $this->input->post('hs_company_id');
				$hs_deal_id = $this->input->post('hs_deal_id');
			 }
            //$opportunity_str = mysql_escape_string($this->input->post('opportunity_str'));
            
            $email      = mysql_escape_string($this->input->post('email'));
           // $username   = mysql_escape_string($this->input->post('username'));
            $company    = $this->input->post('company');
            //$password   = mysql_escape_string($this->input->post('password'));
            $website    = mysql_escape_string($this->input->post('website'));
            $first_name = $this->input->post('first_name');
            $last_name  = $this->input->post('last_name');
            $phone      = mysql_escape_string($this->input->post('phone'));
            $tree_nums  = mysql_escape_string($this->input->post('tree_nums'));
            $active     = mysql_escape_string($this->input->post('active'));
            
            
            // expend
            $address    = $this->input->post('address');
            $city       = mysql_escape_string($this->input->post('city'));
            $state      = mysql_escape_string($this->input->post('state'));
            $post_code  = mysql_escape_string($this->input->post('post_code'));
            //
            $price      = mysql_escape_string($this->input->post('price'));
            $currency   = mysql_escape_string($this->input->post('currency'));
            $tax        = mysql_escape_string($this->input->post('tax'));
            $subject    = $this->input->post('subject');
			$cc_email    = $this->input->post('cc_email');
            $message    = $this->input->post('message');
            $certificate_text        = $this->input->post('certificate_text');
            
            // 
            $email2        = mysql_escape_string($this->input->post('email2'));
            $first_name2   = mysql_escape_string($this->input->post('first_name2'));
			$suscredit_price      = mysql_escape_string($this->input->post('suscredit_price'));
            
			 $data       = array();
        
            $data['email']      = $email;
           // $data['username']   = $username;
            $data['id']         = (int)$id;
            /* if ($password) {
                $data['password']   = md5($password);   
            } */
            
            // $data['active'] = (int)$active;
            if ($_SESSION['login']['type']=='admin') {
                $data['active']     = (int) $active;
                //
                $data['price']      = $price;
                $data['suscredit_price']      = $suscredit_price;
                $data['dashboard_user_email']      = $dashboard_user_email;
                $data['tax']        = $tax;
                $data['currency']   = $currency;
            }
            $this->db->update_batch('pct_users', array($data), 'id');
            
            // update pct_initiative_partner
            $data       = array();
			 if ($_SESSION['login']['type'] == 'intitiative_client') {
				 
				$twitter    = mysql_escape_string($this->input->post('twitter'));
				$facebook       = mysql_escape_string($this->input->post('facebook'));
				$instagram      = mysql_escape_string($this->input->post('instagram'));
				$pinterest  = mysql_escape_string($this->input->post('pinterest'));
				$linkedin  = mysql_escape_string($this->input->post('linkedin'));
				$data['twitter']      = $twitter;
				$data['facebook']      = $facebook;
				$data['instagram']      = $instagram;
				$data['pinterest']      = $pinterest;
				$data['linkedin']      = $linkedin;
					      
			}
           
            //$data['opportunity_str'] = $opportunity_str;
            
            $data['company']        = $company;
            $data['first_name']     = $first_name;
            $data['last_name']      = $last_name;
            $data['phone']          = $phone;
            
            // expend
            $data['address']        = $address;
            $data['city']           = $city;
            $data['state']          = $state;
            $data['post_code']      = $post_code;
            
            //
            $data['first_name2']     = $first_name2;
            $data['email2']     = $email2;
			$data['monthly_reminder_email']  = $monthly_reminder_email;
            
            if ($_SESSION['login']['type']=='admin') {
				$data['initiative']     = $initiative;
				$data['send_invoices']  = $send_invoices;
				$data['free_trial']  	= $free_trial;
                $data['tree_nums']      = $tree_nums;
                $data['grid_upload']      = $grid_upload;
                $data['counter_type']     = $counter_type;
                $data['hs_company_id']    = $hs_company_id;
                $data['hs_deal_id']    	= $hs_deal_id;
                //
                $data['subject']      = $subject;
                $data['message']      = $message;
				$data['cc_email']      = $cc_email;
                $data['certificate_text']      = $certificate_text;
                $data['send_email_immediately'] = $send_email_immediately;
			    $data['tech_support_email']   = $tech_support_email;
			    $data['sender_email']   = $sender_email;
			    $data['replyto_email']   = $replyto_email; 
			    $data['attach_cert_inemails']   = $attach_cert_inemails; 
				$data['no_of_restaurants']      = $no_of_restaurants;
            }
            
            $data['user_id']        = $id;
            $data['website']        = $website;
            $data['delay_payment_by']   = $delay_payment_by;
            if ($logo) {
                $data['logo']  = $logo;
            }
			if ($cert) {
				$data['partner_bk_image']   = $cert;
			}
            $this->db->update_batch('pct_initiative_partner', array($data), 'user_id');
        }
		
		function update_clients_password($id) {
			 $password   = mysql_escape_string($this->input->post('password'));
			$data['id']         = $id;
				if ($password) {
					$data['password']   = md5($password);   
				}        
				$this->db->update_batch('pct_users', array($data), 'id');
			
		}
		function update_intitiative_partner_clients($id, $logo) {
		//echo "<pre>"; print_r($_POST); die;
           
            $email      = mysql_escape_string($this->input->post('email'));
            //$username   = mysql_escape_string($this->input->post('username'));
            $company    = mysql_escape_string($this->input->post('company'));
            //$password   = mysql_escape_string($this->input->post('password'));
            $website    = mysql_escape_string($this->input->post('website'));
            $first_name = mysql_escape_string($this->input->post('first_name'));
            $last_name  = mysql_escape_string($this->input->post('last_name'));
            $phone      = mysql_escape_string($this->input->post('phone'));
            $active     = mysql_escape_string($this->input->post('active'));
                
            // expend
            $address    = mysql_escape_string($this->input->post('address'));
            $city       = mysql_escape_string($this->input->post('city'));
            $state      = mysql_escape_string($this->input->post('state'));
            $post_code  = mysql_escape_string($this->input->post('post_code'));
            //
            $price      = mysql_escape_string($this->input->post('price'));
            $currency   = mysql_escape_string($this->input->post('currency'));
            $tax        = mysql_escape_string($this->input->post('tax'));
            $subject    = $this->input->post('subject');
			$cc_email    = $this->input->post('cc_email');
            $message    = $this->input->post('message');
            $certificate_text        = $this->input->post('certificate_text');
            
            // 
            $email2        = mysql_escape_string($this->input->post('email2'));
            $first_name2   = mysql_escape_string($this->input->post('first_name2'));
            
			 $data     = array();
			  if (isset($_POST['user_info'])) {
				$data['email']      = $email;

				$data['id']         = $id;
				/* if ($password) {
					$data['password']   = md5($password);   
				}   */        
				$this->db->update_batch('pct_users', array($data), 'id');
			  }
			
            
            // update pct_initiative_partner
            $data       = array();
			  if (isset($_POST['social_info'])) {
				$twitter    = mysql_escape_string($this->input->post('twitter'));
				$facebook       = mysql_escape_string($this->input->post('facebook'));
				$instagram      = mysql_escape_string($this->input->post('instagram'));
				$pinterest  = mysql_escape_string($this->input->post('pinterest'));
				$linkedin  = mysql_escape_string($this->input->post('linkedin'));
				$instagram_token  = mysql_escape_string($this->input->post('instagram_token'));
				$data['twitter']      = $twitter;
				$data['facebook']      = $facebook;
				$data['instagram']      = $instagram;
				$data['pinterest']      = $pinterest;
				$data['linkedin']      = $linkedin;
				$data['instagram_token']   = $instagram_token;
					      
			}
			  if (isset($_POST['company_info'])) {
					$data['company']        = $company;
					$data['phone']          = $phone;
					// expend
					$data['address']        = $address;
					$data['city']           = $city;
					$data['state']          = $state;
					$data['post_code']      = $post_code;
					//$data['email2']     = $email2;
					$data['website']        = $website;
			  }
			   if (isset($_POST['user_info'])) {
			  		$data['first_name']     = $first_name;
					$data['last_name']      = $last_name;
			   }
				  $data['user_id']        = $id;
				 $email_id = $_SESSION['login']['email'];
				 $query = "SELECT * from pct_sublevel_initiative where email='$email_id'";
                 $rows1 = $this->db->query($query)->row_array();
				// echo "<pre>"; print_r($rows1); die('here');
				
				if ($logo) {
					$data['logo']       = $logo;
				}
                $this->db->update_batch('pct_initiative_partner', array($data), 'user_id');
				 if ($logo) {
					 if(!empty($rows1)){
						 $id         = $rows1['id'];
						 $qru = "UPDATE pct_sublevel_initiative SET logo = '".$logo."' WHERE id = '".$id."' ";
					     $this->db->query($qru);
					 }
				 }
				  if($twitter){
							$id         = $rows1['id'];
							$twitter_hashtag = '@'.substr(strrchr($twitter, "/"), 1);
							$qru1 = "UPDATE pct_sublevel_initiative SET  twitter_hashtag='".$twitter_hashtag."' WHERE id = '".$id."' ";
					        $this->db->query($qru1);
				 }
        }
		
	 function get_last_uploaded_clients($id) {
		 $query = "SELECT pc.*,u.email,bp.first_name,bp.company,bp.last_name
					FROM pct_initiative_clients AS pc
					INNER JOIN pct_users AS u
					ON u.id=pc.user_id
					INNER JOIN pct_initiative_partner AS bp
					ON pc.user_id=bp.user_id
					WHERE pc.partner_id=$id AND pc.current='1'";
            $row = $this->db->query($query);
            $data = $row->result();
            return $data;
            //else show_error('This record does not exists', 500 );
        }
				
		function load_initiative($id) { 
			
		 $query = "SELECT pi.*,pi.website as website_url,bp.*, u.id AS id,u.id AS pan_user_id,u.date_created AS user_register_date, u.email AS email, u.username AS username, u.dashboard_user_email AS dashboard_user_email ,u.active AS active, u.tax AS tax, u.price AS price, u.suscredit_price AS suscredit_price,u.currency AS currency, r.code AS code,(SELECT SUM(free_trees) FROM pct_initiative_partner WHERE bulker_id=u.id) AS free_trees1, (SELECT SUM(tree_nums) FROM pct_initiative_partner WHERE bulker_id=u.id) AS tree_nums2
					FROM pct_initiative_partner AS bp
					INNER JOIN pct_users AS u
					ON u.id=bp.user_id
					INNER JOIN pct_referer AS r
					ON r.user_id=bp.user_id 
					INNER JOIN pct_initiative AS pi
					ON pi.id=bp.initiative 
					WHERE u.id=$id";
            $row = $this->db->query($query);
            $data = $row->row_array();
            if ($data) return $data;
            else show_error('This record does not exists', 500 );
        }
			function get_initiative_clients_list($initiative_id) {
            // load clients
            $query = "SELECT bp.*, bp.company AS name, u.id AS id, u.email AS email
                        FROM pct_initiative_partner AS bp
                        INNER JOIN pct_users AS u
                        ON u.id=bp.user_id
                        WHERE bp.bulker_id=$initiative_id";
            return $this->db->query($query)->result();
        }
		  function load_initiative_clients($initiative_id) {
            $query = "SELECT * FROM pct_initiative_partner
                        WHERE bulker_id=$initiative_id
                        ";
            return $this->db->query($query)->result();
        }
         
		  /**
        * functions for  insert_initiative_client_email_records Clients
        */
        function insert_initiative_client_email_records($rows,$client_id) {
           $client['all_email_data'] = json_encode($rows);
           $client['client_id'] = $client_id;
		  // echo "<pre>"; print_r($client); die;
            $this->db->insert_batch('pct_client_activation_emaildata', array($client));
            
        }
		
		 function insert_grant_app_user($data) {
			 
			 $user = array();
            $user['email']      = $data['user_email'];
            $user['username']   = $data['user_email']; 	
            $user['activation_key'] = $data['activation_key'];
            $user['firstname'] = $data['first_name'];
            $user['type']       = 'grant_user';
            $user['active']     = 1;
            $this->db->insert_batch('pct_users', array($user));
			$user_id    = $this->db->insert_id(); 
				
			$grant_users = array();
			$grant_users['first_name']     = $data['first_name'];
			$grant_users['last_name']      = $data['last_name'];
			$grant_users['gea_user_id']     = $data['user_id'];
			$grant_users['user_id']        = $user_id;
			$grant_users['organisation_name']     = $data['organisation_name'];
			$grant_users['address']     = $data['address'];
			$grant_users['website']     = $data['website'];
			$grant_users['organisation_type']     = $data['organisation_type'];
			$grant_users['registration_no']     = $data['re_no'];
			$grant_users['no_of_trees']     = $data['no_of_trees'];
			$grant_users['project_fund']     = $data['project_fund'];
			$grant_users['hear_about_us']     = $data['hear_about_us'];
			$grant_users['search_for']     = $data['search_for'];
			$grant_users['introduced_you']     = $data['introduced_you'];
			$grant_users['others']     = $data['others'];
			$grant_users['type_of_fund']     = $data['type_of_fund'];
			$grant_users['logo']     = $data['logo'];
			$grant_users['reg_certificate']     = $data['re_certificate'];
			//echo "<pre>"; print_r($grant_users); die;
			 $this->db->insert_batch('pct_grant_users', array($grant_users));
		 }
        /**
        * functions for  initiative Clients
        */
        function insert_initiative_client($data) {
            // data for user
            $user = array();
            $user['email']      = $data['email'];
            $user['username']   = $data['username'];
			if($data['auto_login_link']==1){			  //for client autologin
               $user['activation_key'] = $data['activation_key'];
			}
			if($data['activation_email']==1){	
              $user['activation_key'] = $data['activation_key'];
			}else{
				$user['password']   = md5($data['password']);
			}
            $user['type']       = 'intitiative_client';
            $user['active']     = 1;
            $this->db->insert_batch('pct_users', array($user));
            
            // data for client
            $user_id    = $this->db->insert_id();
            $client       = array();
            $client['user_id']        = $user_id;
            $client['company']        = $data['company'];
            $client['first_name']     = $data['first_name'];
            $client['last_name']      = $data['last_name'];
            $client['phone']          = isset($data['phone']) ? $data['phone'] : '';
            $client['city']           = isset($data['city']) ? $data['city'] : '';
            $client['company_number'] = isset($data['company_number']) ? $data['company_number'] : '';
            $client['tree_nums']      = $data['tree_nums'];
		    $client['free_trees']     = $data['free_trees'];
            $client['website']        = $data['website'];
            $client['bulker_id']      = $_SESSION['login']['id'];
            $client['initiative']      = $data['initiative_id'];
			$client['figure_history_key'] = $data['figure_history_key'];
            $this->db->insert_batch('pct_initiative_partner', array($client));
            
            $data2 = array();
            $data2['code'] = $data['referer_id'];
            $data2['user_id'] = $user_id;
            $this->insert_referer($data2);
            return $user_id;
        }
		
		/**
        * functions for  insert sus lightspeed credits
        */
        function insert_lightspeed_suscredit($row) {
			
			$suscredits = floor($row['rest_donates']/$row['suscredit_price']);
			$client['partner_id'] 	= $row['panid'];
			$client['date'] 		= $row['date'];
			$client['suscredits']   = $suscredits;
			$client['processed'] 	= '1';
            $this->db->insert_batch('pct_partner_sustainable_credits', array($client));
			
			$this->update_sustainabale_hubspot_credits($row['panid']);  // update total credits on the hubspot
			
			
        }

		
		function update_hubspot_restaurants_total_trees($email, $total_trees){
			
			/* database connection */
			$db_name = "carbonfd_cfddb";
			$db_host = "localhost";
			$db_user = "carbonfd_cfddbus";
			$db_pass = "vVZKUCXr@wAU";

			$con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
			
			$getquery 	= "Select `hs_company_id` from `cfwp_ls_trial_signup_data` where email = '{$email}'";
			$result 	= mysqli_query($con, $getquery); 
			$companyid 	= $result->fetch_row();
			
						
			if($companyid && $companyid[0] != "" ){
				
				$company_arr = array(
						'properties' => array(
						array(
							'name' => 'total_trees_planted',
							'value' => $total_trees
						)
					));
 
				$update  = $this->cfd_update_hubspot_company_properties($company_arr, $companyid[0]);

			}else{
				
				$hs_contact  = $this->cfd_get_hubspot_contact_using_email($email);
				
				
				if(isset($hs_contact['vid']) && isset($hs_contact['properties']['associatedcompanyid']['value'])){
					 
					$companyid  = $hs_contact['properties']['associatedcompanyid']['value'];
					
					$company_arr = array(
						'properties' => array(
						array(
							'name' => 'total_trees_planted',
							'value' => $total_trees
						)
					));
 
					$update  = $this->cfd_update_hubspot_company_properties($company_arr, $companyid);
				}
				
			}
			
		}
		
		function cfd_get_hubspot_contact_using_email($email){
			
			$hapikey = "71c60211-d5ee-435f-9a0b-c4ee06e1e716";    //live hubspot API key	

			$check_email = 'https://api.hubapi.com/contacts/v1/contact/email/' .$email .'/profile?hapikey=' . $hapikey;
			$ch_email = curl_init($check_email);
			curl_setopt($ch_email, CURLOPT_TIMEOUT, 5);
			curl_setopt($ch_email, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($ch_email, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch_email);
			$status_code = @curl_getinfo($ch_email, CURLINFO_HTTP_CODE);
			$curl_errors = curl_error($ch_email);
			curl_close($ch_email);
			$res = json_decode($response, true);
			return $res;
						
		}
		
		
		
		function update_sustainabale_hubspot_credits($partner_id){
			
			$query = $this->db->query("SELECT SUM(suscredits) as suscredits FROM pct_partner_sustainable_credits WHERE partner_id='$partner_id'");
			$credits = $query->row_array();		
	
			/* database connection */
			$db_name = "carbonfd_cfddb";
			$db_host = "localhost";
			$db_user = "carbonfd_cfddbus";
			$db_pass = "vVZKUCXr@wAU";

			$con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
			
			$getquery 	= "Select `hs_company_id` from `cfwp_ls_trial_signup_data` where panacea_id = '{$partner_id}'";
			$result 	= mysqli_query($con, $getquery); 
			$companyid 	= $result->fetch_row();
			
			if($companyid && isset($credits['suscredits']) && $credits['suscredits'] > 0 ){
				
				$company_arr = array(
						'properties' => array(
						array(
							'name' => 'total_sustainable_credits_available',
							'value' => $credits['suscredits']
						)
					));
 
				$update  = $this->cfd_update_hubspot_company_properties($company_arr, $companyid[0]);
				//echo "<pre>"; print_r($update); die("eeeee"); 
			}
			
		}
		
		
		function cfd_update_hubspot_company_properties($arr, $hscompany_id){

			$hapikey = "71c60211-d5ee-435f-9a0b-c4ee06e1e716";    //live hubspot API key				

			$post_json = json_encode($arr);
			$endpoint = 'https://api.hubapi.com/companies/v2/companies/'.$hscompany_id.'?hapikey=' . $hapikey;
			$ch = @curl_init();
			@curl_setopt($ch, CURLOPT_POST, true);
			@curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
			@curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			@curl_setopt($ch, CURLOPT_URL, $endpoint);
			@curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response 	 = @curl_exec($ch);
			$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$curl_errors = curl_error($ch);
			@curl_close($ch);
			return $response;
		}
		
		/**
        * functions for  monthly_figure Clients
        */
        function insert_monthly_figure_initiative_client($data) {
            // data for user
            $user = array();
            $user['email']      = $data['email'];
            $user['username']   = $data['username'];
			if($data['auto_login_link']==1){			  //for client autologin
               $user['activation_key'] = $data['activation_key'];
			}
			if($data['activation_email']==1){	
              $user['activation_key'] = $data['activation_key'];
			}else{
				$user['password']   = md5($data['password']);
			}
            $user['type']       = 'intitiative_client';
            $user['active']     = 1;
            $this->db->insert_batch('pct_users', array($user));
            
            // data for client
            $user_id    = $this->db->insert_id();
            $client       = array();
            $client['user_id']        = $user_id;
            $client['company']        = $data['company'];
            $client['first_name']     = $data['first_name'];
            $client['last_name']      = $data['last_name'];
            $client['phone']          = isset($data['phone']) ? $data['phone'] : '';
            $client['city']           = isset($data['city']) ? $data['city'] : '';
            $client['tree_nums']      = $data['tree_nums'];
		    $client['free_trees']     = $data['free_trees'];
            $client['website']        = $data['website'];
            $client['bulker_id']      = $_SESSION['restaurant']['id'];
            $client['initiative']      = $data['initiative_id'];
			$client['figure_history_key'] = $data['figure_history_key'];
            $this->db->insert_batch('pct_initiative_partner', array($client));
            
            $data2 = array();
            $data2['code'] = $data['referer_id'];
            $data2['user_id'] = $user_id;
            $this->insert_referer($data2);
            return $user_id;
        }
		/**
        * functions for initiative Clients
        */
        function update_initiative_client($data) {
            // data for user
           		//echo "<pre>"; print_r($data);	
				$qr = 'SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email="'.$data['email'].'" AND bp.company="'.$data['company'].'" AND  u.type="intitiative_client"';
				$query = $this->db->query($qr);
				if ($query->num_rows()){
					$rowq = $query->row_array();
					$tr = $rowq['tree_nums'];
					$f_tr = $tr + $data['tree_nums'];
					
					$ft = $rowq['free_trees'];
					$fre_trees = $ft + $data['free_trees'];
					$qru = "UPDATE pct_initiative_partner SET tree_nums = '".$f_tr."',free_trees='".$fre_trees."',figure_history_key='".$data['figure_history_key']."' WHERE user_id = '".$rowq['user_id']."' ";
					$this->db->query($qru);
					$qru1 = "UPDATE pct_users SET activation_key = '".$data['activation_key']."' WHERE id = '".$rowq['user_id']."' ";
					$this->db->query($qru1);
					return $rowq['user_id'];
				}
        }
		/**
        * functions for initiative Clients
        */
        function update_mfc_invite_client($data) {
            // data for user
           		//echo "<pre>"; print_r($data);	
				$qr = 'SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email="'.$data['email'].'" AND bp.company="'.$data['company'].'" AND  u.type="intitiative_client" AND bp.bulker_id="' . $_SESSION['login']['id'] . '"';
				$query = $this->db->query($qr);
				if ($query->num_rows()){
					$rowq = $query->row_array();
					$tr = $rowq['tree_nums'];
					$f_tr = $tr + $data['tree_nums'];
					
					$ft = $rowq['free_trees'];
					$fre_trees = $ft + $data['free_trees'];
					$qru = "UPDATE pct_initiative_partner SET tree_nums = '".$f_tr."',free_trees='".$fre_trees."',figure_history_key='".$data['figure_history_key']."' WHERE user_id = '".$rowq['user_id']."' ";
					$this->db->query($qru);
					$qru1 = "UPDATE pct_users SET activation_key = '".$data['activation_key']."' WHERE id = '".$rowq['user_id']."' ";
					$this->db->query($qru1);
					return $rowq['user_id'];
				}
        }
		/**
        * functions for initiative Clients
        */
        function ichanged_update_initiative_client($data) {
            // data for user
           		//echo "<pre>"; print_r($data);	
				$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='".$data['email']."' AND  u.type='intitiative_client'";
				$query = $this->db->query($qr);
				if ($query->num_rows()){
					$rowq = $query->row_array();
					$tr = $rowq['tree_nums'];
					$f_tr = $tr + $data['tree_nums'];
					$company = $data['company'];
					
					$ft = $rowq['free_trees'];
					$fre_trees = $ft + $data['free_trees'];
					$qru = "UPDATE pct_initiative_partner SET tree_nums = '".$f_tr."',free_trees='".$fre_trees."',company='".$company."',figure_history_key='".$data['figure_history_key']."' WHERE user_id = '".$rowq['user_id']."' ";
					$this->db->query($qru);
					$qru1 = "UPDATE pct_users SET activation_key = '".$data['activation_key']."' WHERE id = '".$rowq['user_id']."' ";
					$this->db->query($qru1);
					return $rowq['user_id'];
				}
        }
		
		
        /**
        * functions for Clients
        */
        function insert_client($data) {
            // data for user
            $user = array();
            $user['email']      = $data['email'];
            $user['username']   = $data['username'];
            $user['password']   = md5($data['password']);
            $user['type']       = 'client';
            $user['active']     = 1;
            $this->db->insert_batch('pct_users', array($user));
            
            // data for client
            $user_id    = $this->db->insert_id();
            $client       = array();
            $client['user_id']        = $user_id;
            $client['company']        = $data['company'];
            $client['first_name']     = $data['first_name'];
            $client['last_name']      = $data['last_name'];
            $client['phone']          = isset($data['phone']) ? $data['phone'] : '';
            $client['tree_nums']      = $data['tree_nums'];
            $client['website']        = $data['website'];
            $client['bulker_id']      = $_SESSION['login']['id'];

            $this->db->insert_batch('pct_bulk_partners', array($client));
            
            $data2 = array();
            $data2['code'] = $data['referer_id'];
            $data2['user_id'] = $user_id;
            $this->insert_referer($data2);
            return $user_id;
        }
		
		
		/**
        * functions for Clients
        */
        function update_client($data) {
            // data for user
           		//echo "<pre>"; print_r($data);	
				$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_bulk_partners AS bp ON u.id=bp.user_id WHERE u.username='".$data['username']."' ";
				$query = $this->db->query($qr);
				if ($query->num_rows()){
					$rowq = $query->row_array();
					$tr = $rowq['tree_nums'];
					$f_tr = $tr + $data['tree_nums'];
					
					$qru = "UPDATE pct_bulk_partners SET tree_nums = '".$f_tr."' WHERE user_id = '".$rowq['user_id']."' ";
					$this->db->query($qru);
					return $rowq['user_id'];
				}
        }
		
        
        /**
        function load_clients($bulker_id) {
            $query = "SELECT SUM(bp.tree_nums) AS total FROM pct_bulk_partners AS bp
                        INNER JOIN pct_users AS u
                        ON u.id= bp.user_id
                        WHERE ( bp.bulker_id=$bulker_id AND u.active=0)
                        OR ( u.id=$bulker_id )
                        ";
            $row = $this->db->query($query)->row_array();
            return $row['total'];
        }*/
        
        function load_clients($bulker_id) {
            $query = "SELECT * FROM pct_bulk_partners
                        WHERE bulker_id=$bulker_id
                        ";
            return $this->db->query($query)->result();
        }
        
		
        function cancel_clients($bulker_id) {
            $query = "DELETE u.*, bp.* FROM pct_users AS u, pct_bulk_partners AS bp
                        WHERE u.id=bp.user_id
                        AND u.active=0
                        AND bp.bulker_id=$bulker_id";
            return $this->db->query($query);
        }
        
        /**function update_clients_state($bulker_id) {
            $query = "UPDATE pct_users SET active=1
                        WHERE active=0
                        AND id IN (SELECT user_id FROM pct_bulk_partners WHERE bulker_id=$bulker_id)";
            return $this->db->query($query);
        }*/
        
        function get_clients_list($bulker_id) {
            // load clients
            $query = "SELECT bp.*, bp.company AS name, u.id AS id, u.email AS email
                        FROM pct_bulk_partners AS bp
                        INNER JOIN pct_users AS u
                        ON u.id=bp.user_id
                        WHERE bp.bulker_id=$bulker_id";
            return $this->db->query($query)->result();
        }
	
	
        function get_intitiative_clients_list($bulker_id) {
            // load clients
            $query = "SELECT bp.*, bp.company AS name, u.id AS id, u.email AS email
                        FROM pct_initiative_partner AS bp
                        INNER JOIN pct_users AS u
                        ON u.id=bp.user_id
                        WHERE bp.bulker_id=$bulker_id order by u.id DESC";
            return $this->db->query($query)->result();
        }
        
		
        /**
         * ============================================================================================================================================
        * functions for Invoices
        */
        function insert_invoice($data) {
            // data for user
            $query = "INSERT INTO pct_invoices
                        SET user_id = '{$data['user_id']}',
                            total = '{$data['total']}',
                            trees = '{$data['trees']}',
                            date_created = NOW(),
                            state = 0";
            if ( $this->db->query($query) ) {
                 $invoiceid = $this->db->insert_id();
				 $query1 = "INSERT INTO pct_invoices_logs
							 SET user_id = '{$data['user_id']}',
								invoice_id = '{$invoiceid}',
								event = 'created',
								status = 0,
								created_date = NOW()";
				$this->db->query($query1);
				return $invoiceid;
            } else {
                return false;   
            }
        }
		   function insert_initiative_invoice($data) {
            // data for user
            $query = "INSERT INTO pct_invoices
                        SET user_id = '{$data['user_id']}',
                            total = '{$data['total']}',
                            trees = '{$data['trees']}',
                            figure_history_key = '{$data['figure_history_key']}',
                            date_created = NOW(),
                            state = 0";
            if ( $this->db->query($query) ) {
				$invoiceid = $this->db->insert_id();
				 $query1 = "INSERT INTO pct_invoices_logs
							 SET user_id = '{$data['user_id']}',
								invoice_id = '{$invoiceid}',
								event = 'created',
								status = 0,
								created_date = NOW()";
				$this->db->query($query1);
				return $invoiceid;
            } else {
                return false;   
            }
        }
		   function insert_initiative_monthly_invoice_data($data) {
            // data for user
             $this->db->insert_batch('pct_initiative_monthly_invoice_data', array($data));
			  return $this->db->insert_id();
			}
		  function insert_initiative_monthly_invoice_records($data) {
            // data for user
             $this->db->insert_batch('pct_initiative_monthly_invoice_records', array($data));
			  return $this->db->insert_id();
			}
        
		 /**
         * ============================================================================================================================================
        * functions for client latest cerificate
        */
        function insert_client_latest_certificate($data) {
			
           $this->db->insert_batch('pct_initiative_client_certificates', array($data));
		   
        }
	     /**
         * ============================================================================================================================================
        * functions for create invoice reports
        */
        function get_invoices_reports($status="", $date_from="", $to_date="", $initiative="") {
		
			$date_from = $date_from." 00:00:00";
			$to_date = date('Y-m-d',strtotime($to_date . "+1 days"));
			$to_date = $to_date." 00:00:00";
			
			
			 $initiative_check = "";
			 $initiative_check_inv = "";
			 if($initiative !=''){
				
				$initiative_check = "INNER JOIN pct_initiative_partner AS pi
						ON pi.user_id = il.user_id";
				$initiative = "AND pi.initiative = '".$initiative."'";
				$initiative_check_inv = "LEFT JOIN pct_initiative_partner AS pi
						ON pi.user_id = i.user_id";
				
			} 
			if($status == '1'){
				//$event = "AND il.event = 'created' AND il.status = '0'";
				$query = "SELECT i.id as invoice_id,i.state,i.trees,i.user_id, i.total,i.date_created as invoice_date,i.date_paid as invoice_paid_date
						FROM pct_invoices AS i
						$initiative_check_inv
						WHERE i.date_created between '".$date_from."' and '".$to_date."'
						AND i.deleted='0' AND i.state ='0'
						$initiative
						ORDER BY i.date_created DESC";
			}elseif($status == '2'){
				//$event = "AND il.event = 'paid'";
				$query = "SELECT i.id as invoice_id,i.state,i.trees,i.user_id, i.total,i.date_created as invoice_date,i.date_paid as invoice_paid_date
						FROM pct_invoices AS i
						$initiative_check_inv
						WHERE i.date_created between '".$date_from."' and '".$to_date."'
						AND i.deleted='0' AND i.state ='1'
						$initiative
						ORDER BY i.date_created DESC";
			}elseif($status == '3'){
				//$event = "AND il.event = 'created' AND il.status = '0'";
				
			 $query = "SELECT i.id as invoice_id,i.state,i.trees,i.user_id, i.total,i.date_created as invoice_date,i.date_paid as invoice_paid_date
						FROM pct_invoices AS i
						$initiative_check_inv
						WHERE i.date_created between '".$date_from."' and '".$to_date."'
						AND i.deleted='0' AND (i.state ='-2' OR i.state ='0')
						$initiative
						ORDER BY i.date_created DESC";
					  //$result = $this->db->query($query)->result();
				     
			}elseif($status == '4'){
				 //$event = "AND il.event = 'verified'";
				 $query = "SELECT i.id as invoice_id,i.state,i.trees,i.user_id, i.total,i.date_created as invoice_date,i.date_paid as invoice_paid_date
						FROM pct_invoices AS i
						$initiative_check_inv
						WHERE i.date_created between '".$date_from."' and '".$to_date."'
						AND i.deleted='0' AND i.state ='1' AND i.confirmed = '1'
						$initiative
						ORDER BY i.date_created DESC";
			}else{
				$event = "";
				$query = "SELECT i.id as invoice_id,i.state,i.trees,i.user_id, i.total,i.date_created as invoice_date,i.date_paid as invoice_paid_date
						FROM pct_invoices AS i
						$initiative_check_inv
						WHERE i.date_created between '".$date_from."' and '".$to_date."'
						AND i.deleted='0'
						$initiative
						ORDER BY i.date_created DESC";
				
			}
			return $result = $this->db->query($query)->result();
			 //echo "<pre>"; print_r($result); die;
			
		/* 	  $query = "SELECT il.*,i.trees, i.total,i.date_created as invoice_date,i.date_paid as invoice_paid_date
						FROM pct_invoices_logs AS il
						INNER JOIN pct_invoices AS i
						ON i.id = il.invoice_id
						$initiative_check
						WHERE il.record_date between '".$date_from."' and '".$to_date."'
						$event
						$initiative
						AND i.deleted='0'
						ORDER BY il.record_date DESC";
			return $result = $this->db->query($query)->result(); */
			//echo "<pre>"; print_r($result); die;
		   
        }
		
		
        function get_invoices($paid="",$verify="") {
            // load invoices
			if($verify=='' && $paid==''){
				$query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        INNER JOIN pct_users AS u
                        ON u.id=i.user_id
                        WHERE i.state<>-2
						AND i.deleted='0'
                        ORDER BY i.date_created DESC";
			}elseif($paid!=''){
				$query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        INNER JOIN pct_users AS u
                        ON u.id=i.user_id
                        WHERE i.state='$paid'
						AND i.deleted='0'
                        ORDER BY i.date_created DESC";
			}elseif($verify!=''){
				$query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        INNER JOIN pct_users AS u
                        ON u.id=i.user_id
                        WHERE i.state<>-2
						AND i.confirmed='$verify'
						AND i.deleted='0'
                        ORDER BY i.date_created DESC";
			}
            return $this->db->query($query)->result();
        }
		
		function get_invoice_company($type, $user_id) {
					$res = array();
					if($type == 'intitiative'){
					  $sql = "SELECT company FROM pct_initiative_partner WHERE user_id=$user_id";
					  $res = $this->db->query($sql)->row();
					 }elseif($type == 'bulk_partner'){
						 $sql = "SELECT company FROM pct_bulk_partners WHERE user_id=$user_id";
					     $res = $this->db->query($sql)->row();
					 }elseif($type == 'restaurant'){
						 $sql = "SELECT restaurant as company FROM pct_restaurants WHERE user_id=$user_id";
					     $res = $this->db->query($sql)->row();
					 }
					 return $res;
		}
		function get_initiative_partner_invoices($paid="",$verify="") {
            // load invoices
			$user_id = $_SESSION['login']['id'];
			if($verify=='' && $paid==''){
				 $query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        LEFT JOIN pct_users AS u
                        ON u.id=i.user_id
						WHERE i.state<>-2
                       AND i.user_id='$user_id'
						AND i.deleted='0'
                        ORDER BY i.date_created DESC";
			}elseif($paid!=''){
				$query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        INNER JOIN pct_users AS u
                        ON u.id=i.user_id
                        WHERE i.state='$paid'
						AND i.user_id='$user_id'
						AND i.deleted='0'
                        ORDER BY i.date_created DESC";
			}elseif($verify!=''){
				$query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        INNER JOIN pct_users AS u
                        ON u.id=i.user_id
						WHERE i.state<>-2
                        AND i.user_id='$user_id'
						AND i.confirmed='$verify'
						AND i.deleted='0'
                        ORDER BY i.date_created DESC";
			}
            return $this->db->query($query)->result();
        }
		function get_initiative_editor_invoices($paid="",$verify="") {
            // load invoices
			$user_id = $_SESSION['login']['id'];
			$ini_query = "SELECT id from pct_initiative where user_id='$user_id'";
			$response = $this->db->query($ini_query)->result_array();
		   
			$ini_ids = implode("','", array_column($response, "id"));
			
		    $ini_part_query = "SELECT * from pct_initiative_partner where initiative IN ('".$ini_ids."') and bulker_id=0 Group by user_id";
			$resp = $this->db->query($ini_part_query)->result_array();
			$partner_ids = implode("','", array_column($resp, "user_id"));
			//echo "<pre>"; print_r($partner_ids); die;
			
			
			if($verify=='' && $paid==''){
				 $query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        LEFT JOIN pct_users AS u
                        ON u.id=i.user_id
						WHERE i.state<>-2
                        AND i.user_id IN ('".$partner_ids."')
						AND i.deleted='0'
                        ORDER BY i.date_created DESC";
			}elseif($paid!=''){
				$query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        INNER JOIN pct_users AS u
                        ON u.id=i.user_id
                        WHERE i.state='$paid'
						AND i.user_id IN ('".$partner_ids."')
						AND i.deleted='0'
                        ORDER BY i.date_created DESC";
			}elseif($verify!=''){
				$query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        INNER JOIN pct_users AS u
                        ON u.id=i.user_id
						WHERE i.state<>-2
                        AND i.user_id IN ('".$partner_ids."')
						AND i.confirmed='$verify'
						AND i.deleted='0'
                        ORDER BY i.date_created DESC";
			}
			   return $fff = $this->db->query($query)->result();
			/* echo "<pre>"; print_r($fff); die; 
            return $this->db->query($query)->result(); */
        }
		
        
		function get_deleted_invoices() {
            // load invoices
            $query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        INNER JOIN pct_users AS u
                        ON u.id=i.user_id
                        WHERE i.deleted='1'
                        ORDER BY i.date_created DESC";
            return $this->db->query($query)->result();
        }
        function get_invoice($id) {
            // load invoices
            $query = "SELECT * FROM pct_invoices
                        WHERE id=$id";
            return $this->db->query($query)->row();
        }
        
        function invoice_pay($id) {
            $query = "UPDATE pct_invoices SET state=1, date_paid=NOW() WHERE id=$id";
			$updated_id = $this->db->query($query);
			  $query1 = "SELECT * FROM pct_invoices WHERE id=$id";
              $res = $this->db->query($query1)->row();
			  $query2 = "INSERT INTO pct_invoices_logs
							 SET user_id = '{$res->user_id}',
								invoice_id = '{$res->id}',
								event = 'paid',
								status = '1',
								paid_date = NOW()";
			  $this->db->query($query2);
            return $updated_id;
        }
		function invoice_refund($id) {
            $query = "UPDATE pct_invoices SET state=0, date_paid='' WHERE id=$id";
            return $this->db->query($query);
        }
        
        function invoice_delete($id) {
            $query = "UPDATE pct_invoices SET state=-2, deleted='0' WHERE id=$id";
            return $this->db->query($query);
        }
		function invoices_deleted($id) {
            $query = "UPDATE pct_invoices SET state=-2,deleted=1 WHERE id=$id";
            return $this->db->query($query);
        }
		function invoices_undeleted($id) {
            $query = "UPDATE pct_invoices SET state=0, deleted='0' WHERE id=$id";
            return $this->db->query($query);
        }
		
        
        function load_invoice_trees($user_id) {
            $sql = "SELECT SUM(trees) AS total FROM pct_invoices WHERE user_id=$user_id AND state<>-2";
            return $this->db->query($sql)->row();
        }
	    function load_restaurant_figures_trees($user_id) {
            $sql = "SELECT SUM(trees) AS total FROM pct_figures WHERE user_id=$user_id";
            return $this->db->query($sql)->row();
        }

        /**
         * ============================================================================================================================================
         * function for Solicitor
         */
        function insert_solicitor() {
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $password   = mysql_escape_string($this->input->post('password'));
            //$restaurant = mysql_escape_string($this->input->post('restaurant'));
            //$first_name = mysql_escape_string($this->input->post('first_name'));
            //$last_name  = mysql_escape_string($this->input->post('last_name'));
            //$phone      = mysql_escape_string($this->input->post('phone'));

            $data       = array();
            $data['email']      = $email;
            $data['username']   = $username;
            $data['password']   = md5($password);
            $data['type']       = 'solicitor';
            $data['active']     = 1;
            $this->db->insert_batch('pct_users', array($data));
        }
		
		function update_solicitor($id) {
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $password   = mysql_escape_string($this->input->post('password'));
            //$restaurant = mysql_escape_string($this->input->post('restaurant'));
            //$first_name = mysql_escape_string($this->input->post('first_name'));
            //$last_name  = mysql_escape_string($this->input->post('last_name'));
            //$phone      = mysql_escape_string($this->input->post('phone'));
            $active     = mysql_escape_string($this->input->post('active'));
		
            $data       = array();
            $data['email']      = $email;
            $data['username']   = $username;
            $data['type']       = 'solicitor';
            $data['active']     = $active;
			$data['id']         = $id;
            if ($password) {
                $data['password']   = md5($password);   
            }
			
            $this->db->update_batch('pct_users', array($data), 'id');
        }
        
		
		function load_solicitor($id) {
            $query = "SELECT * FROM pct_users WHERE id=$id";
            $row = $this->db->query($query);
            $data = $row->row_array();
            if ($data) return $data;
            else show_error('This record does not exists', 500 );
        }

        /**
         * ============================================================================================================================================
         * function for editor
         */
        function insert_editor() {
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $password   = mysql_escape_string($this->input->post('password'));

            $data       = array();
            $data['email']      = $email;
            $data['username']   = $username;
            $data['password']   = md5($password);
            $data['type']       = 'editor';
            $data['active']     = 1;
            $this->db->insert_batch('pct_users', array($data));
        }
		/**
         * ============================================================================================================================================
         * function for initiative editor
         */
        function insert_initiative_editor() {
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $password   = mysql_escape_string($this->input->post('password'));

            $data       = array();
            $data['email']      = $email;
            $data['username']   = $username;
            $data['password']   = md5($password);
            $data['type']       = 'initiative_editor';
            $data['active']     = 1;
            $this->db->insert_batch('pct_users', array($data));
        }
		 function update_initiative_editor($id) {
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $password   = mysql_escape_string($this->input->post('password'));
            $active     = mysql_escape_string($this->input->post('active'));

            $data       = array();
            $data['email']      = $email;
            $data['username']   = $username;
            $data['type']       = 'initiative_editor';
            $data['active']     = $active;
            $data['id']         = $id;
            if ($password) {
                $data['password']   = md5($password);
            }

            $this->db->update_batch('pct_users', array($data), 'id');
        }
        function update_editor($id) {
            $email      = mysql_escape_string($this->input->post('email'));
            $username   = mysql_escape_string($this->input->post('username'));
            $password   = mysql_escape_string($this->input->post('password'));
            $active     = mysql_escape_string($this->input->post('active'));

            $data       = array();
            $data['email']      = $email;
            $data['username']   = $username;
            $data['type']       = 'editor';
            $data['active']     = $active;
            $data['id']         = $id;
            if ($password) {
                $data['password']   = md5($password);
            }

            $this->db->update_batch('pct_users', array($data), 'id');
        }
	
		
/**
         * ============================================================================================================================================
        * function for Solicitor
        */
        function insert_affiliate($ucode, $upass) {
            $first_name = mysql_escape_string($this->input->post('first_name'));
			$last_name  = mysql_escape_string($this->input->post('last_name'));
			$email      = mysql_escape_string($this->input->post('email'));
			$username 	= mysql_escape_string($this->input->post('username'));
            $type   	= mysql_escape_string($this->input->post('type'));
            $rate   	= mysql_escape_string($this->input->post('rate'));
            
            $data       		= array();
            $data['email']      = $email;
            $data['type']  		= $type;
            $data['rate']   	= $rate;
            $data['first_name']	= $first_name;
			$data['last_name']  = $last_name;
			$data['username'] 	= $username;
			$data['code']  		= $ucode;
			$data['password']  	= md5($upass);
			
           // $data['active']     = 1;
            $this->db->insert_batch('pct_affiliates', array($data));
        }	
        
		
		function update_affiliate($id) {
            $first_name = mysql_escape_string($this->input->post('first_name'));
			$last_name  = mysql_escape_string($this->input->post('last_name'));
			$email      = mysql_escape_string($this->input->post('email'));
            $type   	= mysql_escape_string($this->input->post('type'));
            $rate   	= mysql_escape_string($this->input->post('rate'));
            //$active     = mysql_escape_string($this->input->post('active'));
		
            $data       		= array();
            $data['email']      = $email;
            $data['type']  		= $type;
            $data['rate']   	= $rate;
            $data['first_name']	= $first_name;
			$data['last_name']  = $last_name;
			$data['id']         = $id;
			
            $this->db->update_batch('pct_affiliates', array($data), 'id');
        }
        
		
		function load_affiliate($id) {
            $query = "SELECT * FROM pct_affiliates WHERE id=$id";
            $row = $this->db->query($query);
            $data = $row->row_array();
            if ($data) return $data;
            else show_error('This record does not exists', 500 );
        }
		

		
        /**
        insert referer id
        */  
        function insert_referer($data) {
            //$data['code'] = str_replace(' ', '-', $data['code']);
            $query = 'INSERT INTO pct_referer
                        SET user_id="'.$data['user_id'].'",
                            code="'.$data['code'].'"';
            $this->db->query($query);
        }
        
        function load_email_template($id=0) {
            //$data['code'] = str_replace(' ', '-', $data['code']);
            $query = "SELECT * FROM pct_email_templates";
            if ($id) {
                $query .= " WHERE id=$id";
                return $this->db->query($query)->row();
            } else {
                $query .= " ORDER BY id";
            }
            
            return $this->db->query($query)->result();
        }
		
		function load_grant_email_template($id=0) {
            $query = "SELECT * FROM pct_grant_email_templates";
            if ($id) {
                $query .= " WHERE id=$id";
                return $this->db->query($query)->row();
            } else {
                $query .= " ORDER BY id";
            }
            return $this->db->query($query)->result();
        }
		
		function update_grant_email_template($data) {
            $this->db->update_batch('pct_grant_email_templates', $data, 'id');
        }
		
		 function load_initiative_email_template($initiative_id="",$id=0) {
            //$data['code'] = str_replace(' ', '-', $data['code']);
            $query = "SELECT * FROM pct_initiative_email_templates";
            if ($initiative_id) {
                $query .= " WHERE user_id=$initiative_id AND template_id=$id";
                return $this->db->query($query)->row();
            } else {
                $query .= " WHERE user_id=$initiative_id ORDER BY template_id";
            }
            
            return $this->db->query($query)->result();
        }
		 function get_enabled_initiative_email_template($initiative_id="",$id=0) {
            //$data['code'] = str_replace(' ', '-', $data['code']);
            $query = "SELECT * FROM pct_initiative_email_templates";
            if ($initiative_id) {
                $query .= " WHERE user_id=$initiative_id AND template_id=$id AND enable_email='1'";
                return $this->db->query($query)->row();
            } else {
                $query .= " WHERE user_id=$initiative_id AND enable_email='1' ORDER BY template_id";
            }
            
            return $this->db->query($query)->result();
        }
        
        function update_email_template($data) {
            $this->db->update_batch('pct_email_templates', $data, 'id');
        }
        
        function get_refid($user_id) {
            $sql = "SELECT code FROM pct_referer WHERE user_id=$user_id";
            return $this->db->query($sql)->row();
        }
		
		function get_figures($user_id) {
            // load invoices
            $query = "SELECT * FROM pct_figures WHERE user_id=$user_id ORDER BY id DESC";
            return $this->db->query($query)->result();
        }
        
        function get_bulk_partner_figures($user_id) {
            // load invoices
            $query = "SELECT *, date_created AS date_added FROM pct_invoices WHERE user_id=$user_id ORDER BY id DESC";
            return $this->db->query($query)->result();
        }
		
		function get_intitiative_partner_figures($user_id) {
    
            $query = "SELECT (SUM(pct_initiative_partner.free_trees) + SUM(pct_initiative_partner.tree_nums)) as total_trees,pct_users.date_created,pctusers.username as bulker_username,pctusers.email as bulker_email FROM `pct_initiative_partner` join `pct_users` on pct_users.id = pct_initiative_partner.user_id join `pct_users` pctusers on pctusers.id = pct_initiative_partner.bulker_id WHERE pct_initiative_partner.bulker_id =$user_id group by pct_initiative_partner.figure_history_key";
			//$res = $this->db->query($query)->result();
			//echo "<pre>"; print_r($res);die;
            return $this->db->query($query)->result();
        }

        function insert_referrer_redirect() {
            $data = array();
            $data['code'] = $this->input->post('code');
            $data['redirect_url'] = $this->input->post('redirect_url');
            $data['type'] = $this->input->post('type');
            $data['date_created'] = date('Y-m-d H:m:i');

            $this->db->insert_batch('pct_referrer_redirect', array($data));
        }

        function update_referrer_redirect($id) {
            $data = array();
            $data['id'] = $id;
            $data['code'] = $this->input->post('code');
            $data['redirect_url'] = $this->input->post('redirect_url');
            $data['type'] = $this->input->post('type');
            $data['date_created'] = date('Y-m-d H:m:i');

            $this->db->update_batch('pct_referrer_redirect', array($data), 'id');
        }
    }
?>