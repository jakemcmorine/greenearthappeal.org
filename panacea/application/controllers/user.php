<?php
if (!defined('BASEPATH'))
        exit('No direct script access allowed');
// stop
//error_reporting(1);
//ini_set('display_errors', 1);
// Report simple running errors
//error_reporting(E_ERROR | E_WARNING | E_PARSE);

class User extends CI_Controller {

        public function __construct() {
                parent::__construct();
                session_start();
			if (!isset($_SESSION['login']))
                        $_SESSION['login'] = false;
                $this->data = array();
                $this->load->helper('url');
                $this->load->helper('form');
				$this->load->database();
				$this->load->model('user_model');
                $segment_login = $this->uri->segment(2);
                // check if user logged in
                if ((!isset($_SESSION['login']) || !$_SESSION['login']) && !in_array($segment_login, array('login','auto_login_client_link', 'client_login','confirm_stages','confirm_grant_stage','grant_application_users','set_grant_user_password','unsubscribe_grant_reminder_email','forget_password','stages_confirmation','stage_confirmed', 'update_password','add_tpccapi_intitiative_clients', 'solicitor_autologin','restaurant_autologin','autoupload_clients_from_imacro','add_initiative_partner_from_api','update_initiative_partner_from_api','change_initiative_partner_email_from_api','update_cfdpartner_info_api','add_partner_monthly_figures','add_partner_monthly_figures_cfdapi','add_initiative_client_from_api','inbound_upload_client_cronjob','check_quitable_law_pdf','get_cfd_gea_counters','autoupload_sonne_energeticus_clients_cron','gea_counter','cfd_counter','upload_partner_csv_records_cron','add_sustainable_redeem_credits','test_invoice_dynamic')) ) {
                        redirect('user/login');
                        die();
                }

                if (isset($_SESSION['login']['type']) && $_SESSION['login']['type'] == 'bulk_partner') {
                        $this->data['latest_certificate_url'] = '';
                         // get the latest bulk_partner certificate
                        $this->load->database();
                        $qr = "SELECT latest_certificate FROM pct_invoices WHERE user_id= {$_SESSION['login']['id']} ORDER BY id DESC LIMIT 1";
                        $query = $this->db->query($qr);

                        if ($query->num_rows()) {
                                $rowq = $query->row_array();
                                $this->data['latest_certificate_url'] = $rowq['latest_certificate'];
                        }

                }
				if (isset($_SESSION['login']['type']) && $_SESSION['login']['type'] == 'intitiative') {
                        $this->data['latest_certificate_url'] = '';
                        // get the latest bulk_partner certificate
                        $this->load->database();
                        $qr = "SELECT latest_certificate FROM pct_invoices WHERE user_id= {$_SESSION['login']['id']} ORDER BY id DESC LIMIT 1";
                        $query = $this->db->query($qr);

                        if ($query->num_rows()) {
                                $rowq = $query->row_array();
                                $this->data['latest_certificate_url'] = $rowq['latest_certificate'];
                        }
                }

                require_once('phpmailer/class.phpmailer.php');
                
        }
		
		
		/* public function test_invoice_dynamic() {
			
			$bulk_partner = $this->user_model->load_initiative('54186');
			//echo "<pre>"; print_r($bulk_partner); die;
			
			if($bulk_partner['enable_invoice_data'] == 1){	
			
				require('libraries/invoice-dynamic.php');           
			}else{						 	
			
				require('libraries/invoice.php');				
			}
		  
		} */
		/* public function test_restaurants() {
			
			$rows = $this->user_model->get_list_users();
			
			$count = 1;
			foreach ($rows as $row){
				
				if($row->type=="intitiative"){
					if($count > 400 && $count < 451){
						$total_trees = @$row->tree_nums+ @$row->free_trees + @$row->tree_nums2;
						echo $row->email."<br>";
						$this->user_model->update_hubspot_restaurants_total_trees($row->email, $total_trees);
					
					}
					$count++;
				}
				
			}
			
		} */

        public function index() {
				//echo "<pre>"; print_r($_SESSION['login']); die;
                // process member level here
                //$this->check_user(array('admin', 'restaurant',));
                $this->load->model('user_model');
			
                if ($_SESSION['login']['type'] == 'admin') {
					
                        $rows = $this->user_model->get_list_users();
						//echo "<pre>"; print_r($rows); die;
                        $this->load->view('template', array('title' => 'GEA Control Panel', 'body' => 'user_list', 'rows' => $rows));
                } elseif ($_SESSION['login']['type'] == 'manager') {
                        $rows = $this->user_model->get_list_users_manager();
                        $this->load->view('template', array('title' => 'GEA Control Panel', 'body' => 'user_list', 'rows' => $rows));
                } elseif ($_SESSION['login']['type'] == 'restaurant') {

                        if ($_SESSION['login']['first']) {
                                $restaurant = $this->user_model->load_restaurant($_SESSION['login']['id']);

                                $name = $restaurant['restaurant'];
                                $first_name = $restaurant['first_name'];
                                // $template_id = ($restaurant['new']==1) ? 2 : 3;
                                $template_id = 2;
                                $email_template = $this->user_model->load_email_template($template_id);

                                // send welcome email
                                $config = array();
                                $config['to'] = $_SESSION['login']['email'];
                                $config['subject'] = $email_template->subject;
                                $message = $email_template->message;
                                $message = str_ireplace(array('[name]', '[first_name]'), array($name, $first_name), $message);
                                $config['message'] = $message;
                                $pct_sendmail = $this->send_mail($config);
                                $this->user_model->update_user_state($_SESSION['login']['id']);
                                $_SESSION['login']['first'] = 0;
                        }
                        redirect(site_url('user/add_a_figure'));
                } 
				elseif (($_SESSION['login']['type'] == 'bulk_partner')) {
					
                        if ($_SESSION['login']['first']) {
                                $bulk_partner = $this->user_model->load_bulk_partner($_SESSION['login']['id']);

                                $name = $bulk_partner['company'];
                                $first_name = $bulk_partner['first_name'];
                                // $template_id = ($restaurant['new']==1) ? 2 : 3;
                                $email_template = $this->user_model->load_email_template(8);

                                // send welcome email
                                $config = array();
                                $config['to'] = $_SESSION['login']['email'];
                                $config['subject'] = $email_template->subject;
                                $message = $email_template->message;
                                $message = str_ireplace(array('[name]', '[first_name]'), array($name, $first_name), $message);
                                $config['message'] = $message;

                                $pct_sendmail = $this->send_mail($config);
                                $this->user_model->update_user_state($_SESSION['login']['id']);
                                $_SESSION['login']['first'] = 0;
                        }

                        $this->load->model('user_model');
                        // set new session
                        $bulk_partner = $this->user_model->load_bulk_partner($_SESSION['login']['id']);
                        $_SESSION['login']['data']['name'] = $bulk_partner['company'];
                        $invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);

//                        $_SESSION['login']['data']['total_trees'] = $bulk_partner['tree_nums'] + $invoice_trees->total;
                        $_SESSION['login']['data']['total_trees'] = $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2'];

                        $this->load->model('user_model');
						
                        $rows = $this->user_model->get_clients_list($_SESSION['login']['id']);
						//echo "<pre>"; print_r( $this); die;
                        $this->load->view('template', array('title' => 'Bulk Partner Home', 'body' => 'bulk_partner_home', 'rows' => $rows));

                        //$this->edit_bulk_partner($_SESSION['login']['id']);
                } 
				elseif (($_SESSION['login']['type'] == 'intitiative')) { 
		           
						$this->load->database();
						$this->load->model('user_model');
						$query1 = "SELECT pi.initiative_name FROM pct_initiative_partner AS pip INNER JOIN pct_initiative AS pi ON pi.id=pip.initiative where pip.user_id = '".$_SESSION['login']['id']."'";
						$get_initiative = $this->db->query($query1)->row();
						$initiative_name = $get_initiative->initiative_name; 
                        if ($_SESSION['login']['first']) {
							
                                $bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
								
                                $name = $bulk_partner['company'];
                                $first_name = $bulk_partner['first_name'];
                                $initiative = $bulk_partner['initiative'];
                                //$template_id = ($restaurant['new']==1) ? 2 : 3;
                                $email_template = $this->user_model->load_initiative_email_template($initiative,2);

                                // send welcome email
                                $config = array();
                                $config['to'] = $_SESSION['login']['email'];
                                $subject = $email_template->subject;
                                $config['subject'] = str_ireplace(array('[name]', '[first_name]','[initiative_name]'), array($name, $first_name,$initiative_name), $subject);
                                $message = $email_template->message;
                                $message = str_ireplace(array('[name]', '[first_name]','[initiative_name]'), array($name, $first_name,$initiative_name), $message);
                                $config['message'] = $message;

                               // $pct_sendmail = $this->send_mail($config);
                                $this->user_model->update_user_state($_SESSION['login']['id']);
                                $_SESSION['login']['first'] = 0;
                        }

                        $this->load->model('user_model');
                        // set new session
                        $bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
						// echo "<pre>"; print_r($bulk_partner); die;
                        $_SESSION['login']['data']['name'] = $bulk_partner['company'];
                        $invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
			//           $_SESSION['login']['data']['total_trees'] = $bulk_partner['tree_nums'] + $invoice_trees->total;
                        $_SESSION['login']['data']['total_trees'] = $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']+$bulk_partner['free_trees'];
                        $this->load->model('user_model');
						
                        $rows = $this->user_model->get_initiative_clients_list($_SESSION['login']['id']);
						//echo "<pre>"; print_r( $this); die;
						
                        $this->load->view('template', array('title' => 'Initiative Partner Home', 'body' => 'initiative_home', 'rows' => $rows));

                        //$this->edit_bulk_partner($_SESSION['login']['id']);
                } 
				elseif (($_SESSION['login']['type'] == 'solicitor')) {
                        redirect(site_url('user/invoices'));
                } elseif (($_SESSION['login']['type'] == 'editor')) {
                        redirect(site_url('user/email_templates'));
						
                } elseif (($_SESSION['login']['type'] == 'initiative_editor')) {
					$user_id = $_SESSION['login']['id'];
					 $rows = $this->user_model->get_initiative_users_list($user_id);
					// echo "<pre>"; print_r($rows); die;
					$this->load->view('template', array('title' => 'GEA Control Panel', 'body' => 'user_list', 'rows' => $rows));
						
                }
				elseif(($_SESSION['login']['type'] == 'intitiative_client')) {
					//echo "<pre>"; print_r($_SESSION['login']); die;
                     $this->edit_intitiative_partner_clients($_SESSION['login']['id']);
                }elseif(($_SESSION['login']['type'] == 'grant_user')) {
					//	echo "<pre>"; print_r($_SESSION['login']);
                     $this->edit_grant_user($_SESSION['login']['id']);
                }elseif(($_SESSION['login']['type'] == 'grant_admin')) {
					//	echo "<pre>"; print_r($_SESSION['login']);
                     $this->list_grant_users($_SESSION['login']['id']);
                }
				else{
					$this->edit_bulk_partner($_SESSION['login']['id']);
				}
        }
		
        function login() {
                // check if user logged in
                if (isset($_SESSION['login']) && $_SESSION['login']) {
                        redirect(site_url());
                }
                // load recaptcha library
                $this->load->library('Recaptcha');
                // Store the captcha HTML for correct MVC pattern use.
               // $this->data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
			     $this->data['recaptcha_html'] = $this->recaptcha->render();;
                // Call to recaptcha to get the data validation set within the class.
             /*    $captcha_error = false;
                if ($_POST) {
                        $this->recaptcha->recaptcha_check_answer();
                        if (!$this->recaptcha->getIsValid()) {
                                $this->data['error'] = '<div class="text-error">Wrong captcha</div>';
                                $captcha_error = true;
                        }
                } */

                $this->load->library('form_validation');
                $this->form_validation->set_rules('username', 'Username', 'trim|required');
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->form_validation->set_rules('g-recaptcha-response', '<b>Captcha</b>', 'callback_getResponse');


                $this->load->database();
                $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                // validate form
                if ($this->form_validation->run() === false) {
                        $data = array('title' => 'Green Earth Appeal Partner Login', 'body' => 'login_form');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                } else {
                        $username = $this->input->post('username');
                        $password = $this->input->post('password');
                        $username = mysql_escape_string($username);
                        $password = md5($password);

                        // check user login
                        $query = $this->db->query("SELECT * FROM pct_users WHERE username='$username' AND password='$password'");

                        if ($query->num_rows()) {
                                // call apprirate View
                                $row = $query->row_array();

                                if ($row['active'] == 0) {
                                        show_error('Your account is not enable yet.');
                                } else {
                                        $_SESSION['login'] = $row;
											
                                        redirect(site_url());
                                }
                        } else {
                                $aff_login = $this->login_as_affiliate($username, $password);
                                if ($aff_login == "yes") {
                                        redirect(site_url("user/affiliate_index"));
                                } else {
                                        $error['error'] = '<div class="text-error">Login Fail</div>';
                                        $data = array('title' => 'Green Earth Appeal Partner Login', 'body' => 'login_form');
                                        $this->data = array_merge($this->data, $data, $error);
                                        $this->load->view('template', $this->data);
                                }
                        }
                }
        }
			
		public function getResponse($str){

			$response = $this->recaptcha->verifyResponse($str);
			if ($response['success'])
			{     
				return true;
			}     
			else
			{
				//$this->form_validation->set_message('getResponse', '%s '. var_dump($response) );
				$this->form_validation->set_message('getResponse', 'The captcha field is required.' );
				return false;
			}
		}
			
		function auto_login_client_link($activation_key){
			$this->load->database();
			// check if user logged in
			if (isset($_SESSION['login']) && $_SESSION['login']) {
					redirect(site_url());
			}
			 // check user login
				$query = $this->db->query("SELECT * FROM pct_users WHERE activation_key='$activation_key'");

				if ($query->num_rows()) {
						// call apprirate View
						$row = $query->row_array();

						if ($row['active'] == 0) {
								show_error('Your account is not enable yet.');
						} else {
								$_SESSION['login'] = $row;
									
								redirect(site_url());
						}
				}else{
					$error['error'] = '<div class="text-error">Your account is not enable yet.</div>';
					$data = array('title' => 'Green Earth Appeal Partner Login', 'body' => 'login_form');
					$this->data = array_merge($this->data, $data, $error);
					$this->load->view('template', $this->data);
				}
		}
	   function grant_application_users($data='') {

			$this->load->database();
			$this->load->model('user_model');
			if(!empty($_GET['user_email'])){
				$this->user_model->insert_grant_app_user($_GET);
			}
		 
		echo json_encode($_GET); 
	   }
		function unsubscribe_grant_reminder_email($user_id="") {
                $this->load->model('user_model');
				   if($user_id!=""){
					   $this->db->query("UPDATE pct_grant_users SET reminder_email='0' WHERE user_id='$user_id'");
				   }
				  $data = array('title' => 'Unsubscribed successfuly!', 'body' => 'unsubscribe_grant_reminder_email');
				  $this->load->view('template', $data);
        }
	   
	 function set_grant_user_password($pass="",$key="") {
			 
			$this->load->database();
			$this->load->model('user_model');
			  if (isset($_SESSION['login']) && $_SESSION['login']) {
                        redirect(site_url());
                }
				if(!empty($pass) && !empty($key)){		
					 
					 $activation_key = $key; 
					 $password = base64_decode(urldecode($pass));
					 
					$query = $this->db->query("SELECT * FROM pct_users WHERE activation_key='$activation_key'");
					$row = $query->row_array();
					  if ($query->num_rows()) {
						  
								$client_id = $row['id'];
								$config['subject'] = 'Green Earth Appeal Account Details';
								
								$message.='Dear ' .ucfirst($row['firstname']).","."<br><br>";
								//$message.='Thank you for confirming your details. Please continue your application by visiting https://panacea.greenearthappeal.org.' . "<br><br>";
								$message.='Thank you for confirming your details. Please <a href="https://panacea.greenearthappeal.org">click here</a> (https://panacea.greenearthappeal.org) to login and continue to your application.' . "<br><br>";
								$message.= 'User: '.$row['email'].'<br><br>';
								$message.= 'Password: chosen when you registered'.'<br><br>';
								$message.='We wish you every success with your application and look forward to working with you in the future.' . "<br><br>";
								$message.= "Kind Regards,".'<br><br>';
								$message.="Partnerships Department".'<br><br>';
								$message.="Green Earth Appeal".'<br><br>';
								
								$config['message'] = $message;
								
								$config['to'] = $row['email'];
								$config['email_title'] = 'Partnerships Department – Green Earth Appeal';
								
								 $this->send_mail($config);
								 
								 $email = $row['email'];
								 $password = md5($password);
								 $this->db->query("UPDATE pct_users SET password='$password' WHERE id='$client_id'");
								  unset($_SESSION['login']);
								  $_SESSION['login'] = $row;
								
								 redirect(site_url());

					  }else{
						  
						$error['error'] = 'Your Password is already created.';
						$data = array('title' => 'Green Earth Appeal Partner', 'body' => 'create_grant_user_pass');
						$this->data = array_merge($this->data, $data, $error);
						$this->load->view('template', $this->data);
						  
					  }
			}else{
			
						$error['error'] = 'Wrong activation key';
						$data = array('title' => 'Green Earth Appeal Partner', 'body' => 'create_grant_user_pass');
						$this->data = array_merge($this->data, $data, $error);
						$this->load->view('template', $this->data);
			}
		 
	 }
	   
	   
	 /*   function set_grant_user_password($activation_key) {
			$this->load->database();
			$this->load->model('user_model');
			 if (isset($_SESSION['login']) && $_SESSION['login']) {
                        redirect(site_url());
                }
		    $query = $this->db->query("SELECT * FROM pct_users WHERE activation_key='$activation_key'");
			$this->load->library('form_validation');
			
			 if ($query->num_rows()) {
			  if (isset($_POST['submit'])) {
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
				$activation_key = mysql_escape_string($activation_key);
				// check user login
                 $query = $this->db->query("SELECT * FROM pct_users WHERE activation_key='$activation_key'");
			
									$row = $query->row_array();
									if ($row['active'] == 0) {
											$error['error'] = 'Your account is not enable yet';
											$data = array('title' => 'Green Earth Appeal Partner', 'body' => 'update_grant_user_pass');
											$this->data = array_merge($this->data, $data, $error);
											$this->load->view('template', $this->data);
									} else {
										    $client_id = $row['id'];
											$config['subject'] = 'Thank you email';
										
											$message.='Dear ' .$row['username']. "<br><br><br>";
											$message.='------Log In Information-----' . "<br><br>";
											$message.='You can log in to your Initiative Control Panel by visiting http://www.greenearthappeal.org/panacea' . "<br><br><br>";
											$message.='Username: ' .$row['email']. "<br><br>";
											$message.='Thank you for your participation in our tree planting initiative'. "<br><br>";
											
											$config['message'] = $message;
											
											$config['to'] = $row['email'];
											$config['email_title'] = 'Partnerships Department – Green Earth Appeal';
											
											 $this->send_mail($config);
											 
											 $email = $row['email'];
											 $password = md5($this->input->post('password'));
											 $this->db->query("UPDATE pct_users SET password='$password', activation_key='' WHERE id='$client_id'");
										      unset($_SESSION['login']);
											  $_SESSION['login'] = $row;
											
											 redirect(site_url());
									}
						
				}else{
					    $data = array('title' => 'Green Earth Appeal Partner Forget Password', 'body' => 'update_grant_user_pass');
                        $this->load->view('template', $data);
				}
			
			 }else{
						$error['error'] = 'Wrong activation key';
						$data = array('title' => 'Green Earth Appeal Partner', 'body' => 'update_grant_user_pass');
						$this->data = array_merge($this->data, $data, $error);
						$this->load->view('template', $this->data);
			 }
        } */
	   
	   	 /**
         * =================================================================================================================================
         * Grant application users List
         */
		
		 function list_grant_users() {
			    //echo "<pre>"; print_r($_SESSION); die;
                $this->check_user(array("admin","grant_admin"));
                $this->load->model('user_model');
               
				 if($this->input->post()){
					 $stage = $this->input->post('stage');
					 $rows = $this->user_model->list_grant_users($stage);
				 }else{
					 $rows = $this->user_model->list_grant_users();
				 }
                $this->load->view('template', array('title' => 'Grant Applications User List', 'body' => 'grant_users_list', 'rows' => $rows));
        }
		function enable_grant_reminder_email($user_id="") {
                $this->check_user(array("admin","grant_admin"));
                $this->load->model('user_model');
				   if($user_id!=""){
					   $this->db->query("UPDATE pct_grant_users SET reminder_email='1' WHERE user_id='$user_id'");
				   }
				 redirect('user/list_grant_users');
        }
		function disable_grant_reminder_email($user_id="") {
                 $this->check_user(array("admin","grant_admin"));
                $this->load->model('user_model');
				   if($user_id!=""){
					   $this->db->query("UPDATE pct_grant_users SET reminder_email='0' WHERE user_id='$user_id'");
				   }
				 redirect('user/list_grant_users');
        }
		
		
	   function grant_email_templates() {
                $this->check_user(array("admin"));
                $this->load->model('user_model');
               
				 // process form here
                if (isset($_POST['email_template'])) {
                        $data = $_POST['data'];
                        $rows = $this->user_model->update_grant_email_template($data);

                        redirect('user/grant_email_templates');
                }
                $rows = $this->user_model->load_grant_email_template();
                $data = array('title' => 'Grant Application Schedule Email Templates', 'body' => 'grant_email_template', 'rows' => $rows);
                $this->data = array_merge($this->data, $data);
                $this->load->view('template', $this->data);
        }
		function delete_grant_user($user_id) {
                $this->check_user(array("admin","grant_admin"));
				$this->load->database();
                $this->load->model('user_model');
                $this->db->query("DELETE FROM pct_users WHERE id=$user_id");
                $this->db->query("DELETE FROM pct_grant_users WHERE user_id=$user_id");
                $this->db->query("DELETE FROM pct_grant_user_stage1 WHERE user_id=$user_id");
                $this->db->query("DELETE FROM pct_grant_user_stage2 WHERE user_id=$user_id");
                $this->db->query("DELETE FROM pct_grant_user_stage3 WHERE user_id=$user_id");
                $this->db->query("DELETE FROM pct_breakdowns_stage1 WHERE user_id=$user_id");
                $this->db->query("DELETE FROM pct_organisation_stage1 WHERE user_id=$user_id");
                $this->db->query("DELETE FROM pct_person_stage1 WHERE user_id=$user_id");
                $this->db->query("DELETE FROM pct_stage2_locations WHERE user_id=$user_id");
                $this->db->query("DELETE FROM pct_stage2_species WHERE user_id=$user_id");
                $this->db->query("DELETE FROM pct_stage3_community_photos WHERE user_id=$user_id");
                $this->db->query("DELETE FROM pct_stage3_location_photos WHERE user_id=$user_id");
                $this->db->query("DELETE FROM pct_stage3_person_photos WHERE user_id=$user_id");
                $this->db->query("DELETE FROM pct_stage3_videos_links WHERE user_id=$user_id");
                redirect(site_url('user/list_grant_users'));
        }
		
		
		
		function client_login($activation_key) {
			$this->load->database();
			$this->load->model('user_model');
			 if (isset($_SESSION['login']) && $_SESSION['login']) {
                        redirect(site_url());
                }
			$this->load->library('form_validation');
			if(!empty($activation_key)){
				$this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
				$activation_key = mysql_escape_string($activation_key);
				// check user login
                 $query = $this->db->query("SELECT * FROM pct_users WHERE activation_key='$activation_key'");
			
				  if ($query->num_rows()) {
									$row = $query->row_array();
									if ($row['active'] == 0) {
											$error['error'] = 'Your account is not enable yet';
											$data = array('title' => 'Green Earth Appeal Partner', 'body' => 'client_activation');
											$this->data = array_merge($this->data, $data, $error);
											$this->load->view('template', $this->data);
									} else {
											$client_id = $row['id'];
											$query = $this->db->query("SELECT * FROM pct_client_activation_emaildata WHERE client_id='$client_id'");
											$result = $query->row_array();
											$reslt = json_decode($result['all_email_data']);
											$res = (array)($reslt);
											$email_template = $this->user_model->load_initiative_email_template($res['initiative_id'],3);
											$message = $email_template->message;
											 
											$message = str_ireplace(array('[contact_person]','[company_name]', '[username]', '[password]', '[unique_url]', '[ticker_url]','[initiative_name]'), array($res['first_name'], $res['company'], $res['username'], $res['password'], $res['unique_url'], $res['ticker_url'],$res['initiative_name']), $message);

											$config['subject'] = $email_template->subject;
											
											$config['message'] = $message;
											
											$config['to'] = $res['email'];
											
											$config['subject'] = str_ireplace(array('[contact_person]', '[company_name]','[initiative_name]'), array($res['first_name'], $res['company'], $res['initiative_name']), $config['subject']);
											
											$config['attach'] = array(dirname(__FILE__) . '/../../uploads/certs/clients/' . $res['cert_name']);
											
											$config['email_title'] = $res['email_title'];
											//echo "<pre>"; print_r($config); die;
											 $this->send_mail($config);
											 $email = $row['email'];
											 $password = md5($res['password']);
											 $this->db->query("UPDATE pct_users SET password='$password', activation_key='' WHERE id='$client_id'");
										      unset($_SESSION['login']);
											  $_SESSION['login'] = $row;
											
											 redirect(site_url());
									}
							}else{
									$error['error'] = 'Wrong activation key';
									$data = array('title' => 'Green Earth Appeal Partner', 'body' => 'client_activation');
									$this->data = array_merge($this->data, $data, $error);
									$this->load->view('template', $this->data);
							}
				
				}else{
					$error['error'] = 'Incorrect account';
					$data = array('title' => 'Green Earth Appeal Partner', 'body' => 'client_activation');
					$this->data = array_merge($this->data, $data, $error);
					$this->load->view('template', $this->data);
				}
			
            
        }
        function logout() {
                unset($_SESSION['login']);
                redirect('user/login');
        }

        function forget_password() {
                // check if user logged in
                if (isset($_SESSION['login']) && $_SESSION['login']) {
                        redirect(site_url());
                }

                if (isset($_POST['submit'])) {
                        $this->load->library('form_validation');
                       // $this->form_validation->set_rules('username', 'Username', 'trim|required');
                        $this->form_validation->set_rules('email', 'Email', 'trim|email|required');


                        $this->load->database();
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false || $captcha_error) {
                                $data = array('title' => 'Green Earth Appeal Partner Forget Password', 'body' => 'forget_password_form');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                                //$this->load->view('forget_password', $this->data);
                        } else {
                                //$username = $this->input->post('username');
                                $email = $this->input->post('email');
                               // $username = mysql_escape_string($username);
                                $email = mysql_escape_string($email);

                                // check user login
                                $query = $this->db->query("SELECT * FROM pct_users WHERE email='$email'");

                                if ($query->num_rows()) {
                                        // call apprirate View
                                        $row = $query->row_array();

                                        if ($row['active'] == 0) {
                                                show_error('Your account is not enable yet.');
                                        } else {
                                                $reset_code = $this->generateCode(60);
                                                $this->db->query("UPDATE pct_users SET reset_code='$reset_code' WHERE email='$email'");
                                                $reset_link = site_url('user/update_password') . "?reset_code=$reset_code";

                                                // send forget password to email
                                                $config = array();
                                                $config['to'] = $row['email'];
                                                $config['from'] = 'partners@greenearthappeal.org';
                                                $config['from_name'] = 'Partners – Green Earth Appeal';
                                                $config['subject'] = "Forgot Password on Green Earth Appeal";
                                                $config['message'] = "<p>Please <a href='$reset_link'>click here</a> to reset your password</p>";
                                                $pct_sendmail = $this->send_mail($config);

                                                // display confirmation message
                                                $data = array('title' => 'Green Earth Appeal Partner Forget Password', 'body' => 'forget_password_confirmation');
                                                $this->load->view('template', $data);
                                        }
                                } else {
                                        $error['error'] = '<div class="text-error">Username and Email do not match with our records</div>';
                                        $data = array('title' => 'Green Earth Appeal Partner Forget Password', 'body' => 'forget_password_form');
                                        $this->data = array_merge($this->data, $data, $error);
                                        $this->load->view('template', $this->data);
                                }
                        }
                } else {
                        $data = array('title' => 'Green Earth Appeal Partner Forget Password', 'body' => 'forget_password_form');

                        $this->load->view('template', $data);
                }
        }

        function update_password() {

                if (isset($_POST['submit'])) {
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('password', 'Password', 'trim|required');
						$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false || $captcha_error) {
                                $data = array('title' => 'Green Earth Appeal Partner Update Password', 'body' => 'update_password_form');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                                //$this->load->view('forget_password', $this->data);
                        } else {
                                //$username = $this->input->get('username');
                                $reset_code = $this->input->get('reset_code');

                                $row = $this->is_valid_reset_code($username, $reset_code);
                                if ($row) {
                                        if ($row['active'] == 0) {
                                                show_error('Your account is not enable yet.');
                                        } else {
                                                // udpate password
                                                $this->load->database();
                                                $password = md5($this->input->post('password'));
                                                $this->db->query("UPDATE pct_users SET password='$password', reset_code='' WHERE reset_code='$reset_code'");

                                                $data = array('title' => 'Green Earth Appeal Partner Update Password', 'body' => 'update_password_confirmation');

                                                $this->load->view('template', $data);
                                        }
                                } else {
                                        show_error('Invalid reset code');
                                }
                        }
                } else {
                        //$username = $this->input->get('username');
                        $reset_code = $this->input->get('reset_code');

                        if ($this->is_valid_reset_code($username, $reset_code)) {
                                $data = array('title' => 'Green Earth Appeal Partner Update Password', 'body' => 'update_password_form');
                                $this->load->view('template', $data);
                        } else {
                                show_error('Invalid reset code');
                        }
                }
        }

        private function is_valid_reset_code($username, $reset_code) {
               // $username = mysql_escape_string($username);
                $reset_code = mysql_escape_string($reset_code);

                $this->load->database();
                // check user login
                $query = $this->db->query("SELECT * FROM pct_users WHERE reset_code='$reset_code'");
                if ($query->num_rows()) {
                        return $query->row_array();
                } else {
                        return false;
                }
        }

        function login_as_affiliate($uname, $upass) {
                $this->load->database();

                $query = $this->db->query("SELECT * FROM pct_affiliates WHERE username='" . $uname . "' AND password='" . $upass . "' ");

                if ($query->num_rows()) {

                        $row = $query->row_array();
                        $log['id'] = $row['id'];
                        $log['first_name'] = $row['first_name'];
                        $log['last_name'] = $row['last_name'];
                        $log['email'] = $row['email'];
                        $log['username'] = $row['username'];
                        $log['type'] = 'affiliate';
                        $log['affcode'] = $row['code'];
                        unset($_SESSION['login']);
                        $_SESSION['login'] = $log;
                        return "yes";
                } else {

                        return "no";
                }
        }

        function delete_user($id) {
                $this->check_user(array("admin", "manager"));
                $this->load->database();
                $query = "DELETE FROM pct_users WHERE id=$id";
                $this->db->query($query);
                redirect(site_url());
        }
		/**
         * =================================================================================================================================
         * Main Initiative part
         */
        function add_initiative() {
                $this->check_user(array("admin","initiative_editor"));
				$this->load->database();
				$this->load->model('user_model');
                if (isset($_POST['add_initiative'])) {
					
                        // process Initiative data
                        $this->load->library('form_validation');
						
						$this->form_validation->set_rules('initiative_name', 'Initiative Name', 'trim|required'); 
						$this->form_validation->set_rules('ini_certificate_name', 'Certificate Name', 'trim|required'); 
						$this->form_validation->set_rules('local_name', 'Local Name', 'trim|required'); 
						$this->form_validation->set_rules('website', 'Website', 'trim|required');
						//$this->form_validation->set_rules('request_for_funds');
						$this->form_validation->set_rules('sender_email_title');
						$this->form_validation->set_rules('ini_counter_type');
						$this->form_validation->set_rules('cert_type');
						$this->form_validation->set_rules('activation_email');
						$this->form_validation->set_rules('certificate_layout');
						$this->form_validation->set_rules('logo');
						
						$this->form_validation->set_rules('invoice_company_logo');
						$this->form_validation->set_rules('invoice_company_name');
						$this->form_validation->set_rules('invoice_company_address');
						$this->form_validation->set_rules('invoice_contact_details');
						$this->form_validation->set_rules('invoice_bank_details');
						$this->form_validation->set_rules('invoice_name');
						
						$this->form_validation->set_rules('social_text');
						$this->form_validation->set_rules('company_text');
						$this->form_validation->set_rules('user_text');
						$this->form_validation->set_rules('certificate1');
						$this->form_validation->set_rules('certificate2');
						$this->form_validation->set_rules('certificate3');
						$this->form_validation->set_rules('certificate4');
						$this->form_validation->set_rules('data[1][subject]');
						$this->form_validation->set_rules('data[1][name]');
						$this->form_validation->set_rules('data[1][message]');
						$this->form_validation->set_rules('data[2][subject]');
						$this->form_validation->set_rules('data[2][name]');
						$this->form_validation->set_rules('data[2][message]');
						$this->form_validation->set_rules('data[3][subject]');
						$this->form_validation->set_rules('data[3][name]');
						$this->form_validation->set_rules('data[3][message]');
						$this->form_validation->set_rules('data[4][subject]');
						$this->form_validation->set_rules('data[4][name]');
						$this->form_validation->set_rules('data[4][message]');
						$this->form_validation->set_rules('data[5][subject]');
						$this->form_validation->set_rules('data[5][name]');
						$this->form_validation->set_rules('data[5][message]');
						$this->form_validation->set_rules('data[6][subject]');
						$this->form_validation->set_rules('data[6][name]');
						$this->form_validation->set_rules('data[6][message]');						
						
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Add Initiative', 'body' => 'add_initiative');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
								$logo = $invoice_company_logo = '';
                                // check upload
						
                                if ($_FILES['logo']['size']) {
										$config['file_name'] = date('Ymdhis').'-'.$_FILES['logo']['name'];
                                        $config['upload_path'] = './libraries/imgs/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
										
									if (!$this->upload->do_upload('logo')) {
											$data = array('title' => 'Add Initiative', 'body' => 'add_initiative', 'error' => $this->upload->display_errors());
											$this->data = array_merge($this->data, $data);
											$this->load->view('template', $this->data);
											return;
									} else {
											$logo = $this->upload->data();
											$logo = $logo['file_name'];
									}
								}
								
								if ($_FILES['invoice_company_logo']['size']) {
										$config['file_name'] = date('Ydmhis').'-'.$_FILES['invoice_company_logo']['name'];
                                        $config['upload_path'] = './libraries/imgs/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
										
									if (!$this->upload->do_upload('invoice_company_logo')) {
											$data = array('title' => 'Add Initiative', 'body' => 'add_initiative', 'error' => $this->upload->display_errors());
											$this->data = array_merge($this->data, $data);
											$this->load->view('template', $this->data);
											return;
									} else {
											$invoice_company_logo = $this->upload->data();
											$invoice_company_logo = $invoice_company_logo['file_name'];
									}
								}
							
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->insert_initiative($logo, $invoice_company_logo);
                                redirect(site_url('user/initiative_list'));
                        }
                } else {
					$data = array('title' => 'Add Initiative', 'body' => 'add_initiative');
					$this->data = array_merge($this->data, $data);
					$this->load->view('template', $this->data);
                }
        }
		
		function edit_initiative($id = 0) {
                $this->check_user(array("admin","initiative_editor"));
				$this->load->database();
                $this->load->model('user_model');
                if (isset($_POST['edit_initiative'])) {
                          // process Initiative data
						 // echo "<pre>"; print_r($_POST); die;
                        $this->load->library('form_validation');
						
						$this->form_validation->set_rules('initiative_name', 'Initiative Name', 'trim|required'); 
						$this->form_validation->set_rules('ini_certificate_name', 'Certificate Name', 'trim|required'); 
						$this->form_validation->set_rules('local_name', 'Local Name', 'trim|required'); 
						$this->form_validation->set_rules('website', 'Website', 'trim|required');
						$this->form_validation->set_rules('request_for_funds');
						//$this->form_validation->set_rules('sender_email_title');
						$this->form_validation->set_rules('activation_email');
						$this->form_validation->set_rules('ini_counter_type');
						$this->form_validation->set_rules('cert_type');
						$this->form_validation->set_rules('certificate_layout');
						$this->form_validation->set_rules('social_text');
						$this->form_validation->set_rules('company_text');
						$this->form_validation->set_rules('user_text');
						$this->form_validation->set_rules('logo');
						$this->form_validation->set_rules('invoice_company_logo');
						$this->form_validation->set_rules('invoice_company_name');
						$this->form_validation->set_rules('invoice_company_address');
						$this->form_validation->set_rules('invoice_contact_details');
						$this->form_validation->set_rules('invoice_bank_details');
						$this->form_validation->set_rules('invoice_name');
						$this->form_validation->set_rules('certificate1');
						$this->form_validation->set_rules('certificate2');
						$this->form_validation->set_rules('certificate3');
						$this->form_validation->set_rules('certificate4');
						$this->form_validation->set_rules('data[1][subject]');
						$this->form_validation->set_rules('data[1][name]');
						$this->form_validation->set_rules('data[1][message]');
						$this->form_validation->set_rules('data[2][subject]');
						$this->form_validation->set_rules('data[2][name]');
						$this->form_validation->set_rules('data[2][message]');
						$this->form_validation->set_rules('data[3][subject]');
						$this->form_validation->set_rules('data[3][name]');
						$this->form_validation->set_rules('data[3][message]');
						$this->form_validation->set_rules('data[4][subject]');
						$this->form_validation->set_rules('data[4][name]');
						$this->form_validation->set_rules('data[4][message]');
						$this->form_validation->set_rules('data[5][subject]');
						$this->form_validation->set_rules('data[5][name]');
						$this->form_validation->set_rules('data[5][message]');
						$this->form_validation->set_rules('data[6][subject]');
						$this->form_validation->set_rules('data[6][name]');
						$this->form_validation->set_rules('data[6][message]');
				
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // validate form
                        if ($this->form_validation->run() === false) {
								$query = "SELECT * FROM pct_initiative_email_templates where user_id=$id ORDER BY id";
								$rows = $this->db->query($query);
								$data3['rows'] = $rows->result_array();
								$data2 = $this->user_model->load_main_initiative($id);
								$data = array('title' => 'Edit Initiative', 'body' => 'edit_initiative');
                                $this->data = array_merge($this->data, $data, $data2,$data3);
                                $this->load->view('template', $this->data);
                        } else {

								$logo = $invoice_company_logo = '';
									// check upload
							
                                if ($_FILES['logo']['size']) {
										$config['file_name'] = date('Ymdhis').'-'.$_FILES['logo']['name'];
                                        $config['upload_path'] = './libraries/imgs/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '50000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
										
									if (!$this->upload->do_upload('logo')) {
											$data = array('title' => 'Edit Initiative', 'body' => 'edit_initiative', 'error' => $this->upload->display_errors());
											$this->data = array_merge($this->data, $data);
											$this->load->view('template', $this->data);
											return;
									} else {
											$logo = $this->upload->data();
											$logo = $logo['file_name'];
									}
								}
								
								if ($_FILES['invoice_company_logo']['size']) {
										$config['file_name'] = date('Ydmhis').'-'.$_FILES['invoice_company_logo']['name'];
                                        $config['upload_path'] = './libraries/imgs/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '50000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
										
									if (!$this->upload->do_upload('invoice_company_logo')) {
											$data = array('title' => 'Edit Initiative', 'body' => 'edit_initiative', 'error' => $this->upload->display_errors());
											$this->data = array_merge($this->data, $data);
											$this->load->view('template', $this->data);
											return;
									} else {
											$invoice_company_logo = $this->upload->data();
											$invoice_company_logo = $invoice_company_logo['file_name'];
									}
								}
								
								
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->update_initiative($id, $logo, $invoice_company_logo); 
                                redirect(site_url('user/initiative_list'));
						}
					   
                } else {
					$query = "SELECT * FROM pct_initiative_email_templates where user_id=$id ORDER BY id";
					$rows = $this->db->query($query);
					$data3['rows'] = $rows->result_array();
					$data2 = $this->user_model->load_main_initiative($id);
					//$this->check_user();
					$data = array('title' => 'Edit Initiative', 'body' => 'edit_initiative');
					$this->data = array_merge($this->data, $data, $data2,$data3);
					//echo "<pre>"; print_r($this->data); die('here');
					$this->load->view('template', $this->data);
                }
		  }
		  function cv_thankyou_email($id=41) {
                $this->check_user(array("admin","initiative_editor"));
				$this->load->database();
                $this->load->model('user_model');
                if (isset($_POST['cv_thankyou_email'])) {
						 //echo "<pre>"; print_r($_POST); die;
                        $this->load->library('form_validation');
						$this->form_validation->set_rules('cv_thankyou_email');
						$this->form_validation->set_rules('cv_subject');
						$this->form_validation->set_rules('cv_message');
						 
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false) {
								$query = "SELECT * FROM pct_global_email_templates where id='$id'";
								$rows = $this->db->query($query);
								$data3['rows'] = $rows->row_array();
								$data = array('title' => 'CV Thank You Email', 'body' => 'edit_cv_thankyou_email');
                                $this->data = array_merge($this->data, $data,$data3);
                                $this->load->view('template', $this->data);
                        } else {
							
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->update_cv_thankyou_template($id);
                                redirect(site_url('user/cv_thankyou_email'));
						}
					   
                } else {
					$query = "SELECT * FROM pct_global_email_templates where id='$id'";
					$rows = $this->db->query($query);
					$data3['rows'] = $rows->row_array();
					$data = array('title' => 'CV Thank You Email', 'body' => 'edit_cv_thankyou_email');
					$this->data = array_merge($this->data, $data,$data3);
					//echo "<pre>"; print_r($this->data); die('here');
					$this->load->view('template', $this->data);
                }
		  }
		  function global_email_template($id = 0) {
                $this->check_user(array("admin","initiative_editor"));
				$this->load->database();
                $this->load->model('user_model');
                if (isset($_POST['edit_global_emails'])) {
                          // process Initiative data
						 //echo "<pre>"; print_r($_POST); die;
                        $this->load->library('form_validation');
						 for($i=1; $i<=10; $i++){
								$this->form_validation->set_rules('data['.$i.'][subject]');
								$this->form_validation->set_rules('data['.$i.'][name]');
								$this->form_validation->set_rules('data['.$i.'][message]');
								$this->form_validation->set_rules('data['.$i.'][time_period]');
								$this->form_validation->set_rules('data['.$i.'][enable_email]');
						 }  
				
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false) {
								$query = "SELECT * FROM pct_global_email_templates where set_id='$id' ORDER BY id";
								$rows = $this->db->query($query);
								$data3['rows'] = $rows->result_array();
								$data = array('title' => 'Edit Global Emails Set '.$id, 'body' => 'edit_global_template_emails');
                                $this->data = array_merge($this->data, $data,$data3);
                                $this->load->view('template', $this->data);
                        } else {
							
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->update_global_email_templates($id);
                                redirect(site_url('user/global_email_template/'.$id));
						}
					   
                } else {
					$query = "SELECT * FROM pct_global_email_templates where set_id='$id' ORDER BY id";
					$rows = $this->db->query($query);
					$data3['rows'] = $rows->result_array();
					$data = array('title' => 'Edit Global Emails Set '.$id, 'body' => 'edit_global_template_emails');
					$this->data = array_merge($this->data, $data,$data3);
					//echo "<pre>"; print_r($this->data); die('here');
					$this->load->view('template', $this->data);
                }
		  }
		 /**
         * =================================================================================================================================
         * Main initiative list part
         */
		
		 function initiative_list() {
			  //echo "<pre>"; print_r($_SESSION['login']); die;
                //$this->check_user(array("admin"));
                $this->check_user(array("admin","initiative_editor"));
                $this->load->model('user_model');
				if($_SESSION['login']['type']=='initiative_editor'){
					 $rows = $this->user_model->list_editor_initiatives($_SESSION['login']['id']);
				}else{
					 $rows = $this->user_model->list_initiative();
				}
               
                $this->load->view('template', array('title' => 'Initiative List', 'body' => 'initiative_list', 'rows' => $rows));
        }
		function copy_initiative(){
			$this->load->database();
			$initiative_id = $this->input->post('initiative_id');
			$this->check_user(array("admin","initiative_editor"));
            $query = "SELECT * FROM pct_initiative where id=$initiative_id";
            $result = $this->db->query($query)->row();
			if(!empty($result))	{
				$data['initiative_name']         = $result->initiative_name;
				$data['website']       		     = $result->website;
				$data['sender_email_title']      = $result->sender_email_title;
				$data['company_text']            = $result->company_text;
				$data['user_text']               = $result->user_text;
				$data['certificate1']            = $result->certificate1;
				$data['certificate2']            = $result->certificate2;
				$data['certificate3']            = $result->certificate3;
				$data['certificate4']            = $result->certificate4;
				$data['token'] 					 = strtolower($this->randomKey(20));
				$data['user_id']                 = $result->user_id;
				$this->db->insert_batch('pct_initiative', array($data));
				$new_initiative_id = $this->db->insert_id();
				//To get intiative email templates
				$query = "SELECT * FROM pct_initiative_email_templates where user_id=$initiative_id ORDER BY id";
				$records = $this->db->query($query)->result_array();
				foreach($records as $key => $val){
					$emails['name']         = $records[$key]['name'];
					$emails['subject']      = $records[$key]['subject'];
					$emails['message']      = $records[$key]['message'];
					$emails['note']         = $records[$key]['note'];
					$emails['template_id']  = $key+1;
					$emails['user_id'] 		= $new_initiative_id;
				   $this->db->insert_batch('pct_initiative_email_templates', array($emails));
						
				}
				$data = array('status'=>"yes");
				echo json_encode($data);
				exit(0);
			}else{
				$data = array('status'=>"no");
				echo json_encode($data);
				exit(0);
			}
		}
		function delete_initiative($id) {
                //$this->check_user(array("admin"));
				$this->check_user(array("admin","initiative_editor"));
                $this->load->database();
                $query = "DELETE FROM pct_initiative WHERE id=$id";
                $this->db->query($query);
               redirect(site_url('user/initiative_list'));
        }
		/**
         * =================================================================================================================================
         * Sub Level Initiative part
         */
        function add_sublevel_initiative() {
                $this->check_user(array("admin"));
                if (isset($_POST['main_title'])) {
                        // process restaurant data
                        $this->load->library('form_validation');
						
						$this->form_validation->set_rules('initiative', 'Initiative', 'trim|required');
                        $this->form_validation->set_rules('main_title', 'Title', 'trim|required');
                        $this->form_validation->set_rules('sub_title', 'Sub Title', 'trim|required');
                        $this->form_validation->set_rules('tli_id', 'Top Level Initiative', 'trim|required');
						$this->form_validation->set_rules('address', 'Address', 'trim|required');
						$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
						$this->form_validation->set_rules('partner_name');
						$this->form_validation->set_rules('website');
						$this->form_validation->set_rules('first_name');
						$this->form_validation->set_rules('last_name');
						$this->form_validation->set_rules('phone');
						$this->form_validation->set_rules('brand');
						$this->form_validation->set_rules('twitter');
						$this->form_validation->set_rules('twitter_hashtag');
                        $this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                        $this->form_validation->set_rules('logo');
						$this->form_validation->set_rules('opportunity_str', 'Opportunity String', 'trim|required');
                        $this->form_validation->set_rules('code', 'Referer ID', 'trim|required|callback_referer_check');
						
						
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // validate form
                        if ($this->form_validation->run() === false) {
                               $data = array('title' => 'add_sublevel_initiative', 'body' => 'sub_level_initiative');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
								// logo upload
								$logo = "";
                                if ($_FILES['logo']['size']) {
										$config['file_name'] = date('Ymdhis').'-'.$_FILES['logo']['name'];
                                        $config['upload_path'] = './uploads/sublevel_initiative/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
                                        if ($this->upload->do_upload('logo')) {
                                                $logo = $this->upload->data();
                                                 $logo = $logo['file_name'];
                                                 $logo_name = $_FILES['logo']['name'];
                                        }
                                }
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->insert_sublevel_initiative($logo);
                                redirect(site_url());
                        }
                } else {
					$this->load->database();
					$query = "SELECT * FROM pct_toplevel_initiative";
					$row = $this->db->query($query);
					$data2['results'] = $row->result_array();
					$data = array('title' => 'Sub Level Initiative', 'body' => 'sub_level_initiative');
					$this->data = array_merge($this->data, $data, $data2);
					$this->load->view('template', $this->data);
                }
        }
		
		function edit_sub_level_initiative($id = 0) {
                $this->check_user(array('admin'));

                $this->load->model('user_model');
                if (isset($_POST['main_title'])) {
                        $this->load->library('form_validation');
                       $this->form_validation->set_rules('initiative', 'Initiative', 'trim|required');
                        $this->form_validation->set_rules('main_title', 'Title', 'trim|required');
						$this->form_validation->set_rules('tli_id', 'Top Level Initiative', 'trim|required');
                        $this->form_validation->set_rules('sub_title', 'Sub Title', 'trim|required');
						$this->form_validation->set_rules('address', 'Address', 'trim|required');
						$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
						$this->form_validation->set_rules('partner_name');
						$this->form_validation->set_rules('website');
						$this->form_validation->set_rules('first_name');
						$this->form_validation->set_rules('last_name');
						$this->form_validation->set_rules('phone');
						$this->form_validation->set_rules('brand');
                        $this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                        $this->form_validation->set_rules('logo');
						$this->form_validation->set_rules('opportunity_str', 'Opportunity String', 'trim|required');
                        $this->form_validation->set_rules('code', 'Referer ID', 'trim|required|callback_referer_check');
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // validate form
                        if ($this->form_validation->run() === false) {
                               $data = array('title' => 'add_sublevel_initiative', 'body' => 'sub_level_initiative');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
								// logo upload
								$logo = "";
                                if ($_FILES['logo']['size']) {
										$config['file_name'] = date('Ymdhis').'-'.$_FILES['logo']['name'];
                                        $config['upload_path'] = './uploads/sublevel_initiative/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
                                        if ($this->upload->do_upload('logo')) {
                                                $logo = $this->upload->data();
                                                 $logo = $logo['file_name'];
                                                 $logo_name = $_FILES['logo']['name'];
                                        }
                                }
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->update_sublevel_initiative($id,$logo);
                                redirect(site_url('user/sub_level_initiative_list'));
						}
					   
                } else {
					
					$this->load->database();
					$query = "SELECT * FROM pct_toplevel_initiative";
					$row = $this->db->query($query);
					$data3['results'] = $row->result_array();
					
					$this->load->model('user_model');
					$data2 = $this->user_model->load_sub_level_initiative($id);
					//$this->check_user();
					$data = array('title' => 'Edit Sub Level Initiative', 'body' => 'sub_level_initiative_edit');
					$this->data = array_merge($this->data, $data, $data2,$data3);
					$this->load->view('template', $this->data);
                }
		  }
		
		/**
         * =================================================================================================================================
         * Sub initiative list part
         */
		
		 function sub_level_initiative_list() {
                $this->check_user(array("admin"));
                $this->load->model('user_model');
                $rows = $this->user_model->list_sub_level_initiative();
                $this->load->view('template', array('title' => 'Sub Level Initiative Partner List', 'body' => 'sub_level_initiative_list', 'rows' => $rows));
        }
		 function delete_sub_level_initiative($id) {
                $this->check_user(array("admin"));
                $this->load->database();
                $query = "DELETE FROM pct_sublevel_initiative WHERE id=$id";
                $this->db->query($query);
               redirect(site_url('user/sub_level_initiative_list'));
        }
		/**
         * =================================================================================================================================
         * Top Level Initiative part
         */
        function add_toplevel_initiative() {
                $this->check_user(array("admin"));
                if (isset($_POST['main_title'])) {
                        // process restaurant data
                        $this->load->library('form_validation');
						
						$this->form_validation->set_rules('initiative', 'Initiative', 'trim|required');
                        $this->form_validation->set_rules('main_title', 'Title', 'trim|required');
                        $this->form_validation->set_rules('sub_title', 'Sub Title', 'trim|required');
						$this->form_validation->set_rules('address', 'Address', 'trim|required');
						$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
						$this->form_validation->set_rules('partner_name');
						$this->form_validation->set_rules('website');
						$this->form_validation->set_rules('first_name');
						$this->form_validation->set_rules('last_name');
						$this->form_validation->set_rules('phone');
						$this->form_validation->set_rules('twitter');
						$this->form_validation->set_rules('twitter_hashtag');
                        $this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                        $this->form_validation->set_rules('logo');
						$this->form_validation->set_rules('opportunity_str', 'Opportunity String', 'trim|required');
                        $this->form_validation->set_rules('code', 'Referer ID', 'trim|required|callback_referer_check');
						
						
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // validate form
                        if ($this->form_validation->run() === false) {
                               $data = array('title' => 'add_toplevel_initiative', 'body' => 'top_level_initiative');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
								// logo upload
								$logo = "";
                                if ($_FILES['logo']['size']) {
										$config['file_name'] = date('Ymdhis').'-'.$_FILES['logo']['name'];
                                        $config['upload_path'] = './uploads/toplevel_initiative/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
                                        if ($this->upload->do_upload('logo')) {
                                                $logo = $this->upload->data();
                                                 $logo = $logo['file_name'];
                                                 $logo_name = $_FILES['logo']['name'];
                                        }
                                }
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->insert_toplevel_initiative($logo);
                                redirect(site_url());
                        }
                } else {
                        $data = array('title' => 'Top Level Initiative', 'body' => 'top_level_initiative');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }
		 /**
         * =================================================================================================================================
         * top initiative list part
         */
		
		 function top_level_initiative_list() {
                $this->check_user(array("admin"));
                $this->load->model('user_model');
                $rows = $this->user_model->list_top_level_initiative();
                $this->load->view('template', array('title' => 'Top Level Initiative Partner List', 'body' => 'top_level_initiative_list', 'rows' => $rows));
        }
		
		function edit_top_level_initiative($id = 0) {
                $this->check_user(array('admin'));
                $this->load->model('user_model');
                if (isset($_POST['main_title'])) {
                        $this->load->library('form_validation');
                       $this->form_validation->set_rules('initiative', 'Initiative', 'trim|required');
                        $this->form_validation->set_rules('main_title', 'Title', 'trim|required');
                        $this->form_validation->set_rules('sub_title', 'Sub Title', 'trim|required');
						$this->form_validation->set_rules('address', 'Address', 'trim|required');
						$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
						$this->form_validation->set_rules('partner_name');
						$this->form_validation->set_rules('website');
						$this->form_validation->set_rules('first_name');
						$this->form_validation->set_rules('last_name');
						$this->form_validation->set_rules('phone');
						$this->form_validation->set_rules('twitter');
                        $this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                        $this->form_validation->set_rules('logo');
						$this->form_validation->set_rules('opportunity_str', 'Opportunity String', 'trim|required');
                        $this->form_validation->set_rules('code', 'Referer ID', 'trim|required|callback_referer_check');
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // validate form
                        if ($this->form_validation->run() === false) {
                               $data = array('title' => 'add_toplevel_initiative', 'body' => 'top_level_initiative');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
								// logo upload
								$logo = "";
                                if ($_FILES['logo']['size']) {
										$config['file_name'] = date('Ymdhis').'-'.$_FILES['logo']['name'];
                                        $config['upload_path'] = './uploads/toplevel_initiative/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
                                        if ($this->upload->do_upload('logo')) {
                                                $logo = $this->upload->data();
                                                 $logo = $logo['file_name'];
                                                 $logo_name = $_FILES['logo']['name'];
												 
                                        }
                                }
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->update_toplevel_initiative($id,$logo);
                                redirect(site_url('user/top_level_initiative_list'));
						}
					   
                } else {
                        $this->load->model('user_model');
                        $data2 = $this->user_model->load_top_level_initiative($id);
                        //$this->check_user();
                        $data = array('title' => 'Edit Top Level Initiative', 'body' => 'top_level_initiative_edit');
                        $this->data = array_merge($this->data, $data, $data2);
                        $this->load->view('template', $this->data);
                }
		  }
		  
		  function delete_top_level_initiative($id) {
                $this->check_user(array("admin"));
                $this->load->database();
                $query = "DELETE FROM pct_toplevel_initiative WHERE id=$id";
                $this->db->query($query);
               redirect(site_url('user/top_level_initiative_list'));
        }
		
		 /**
         * =================================================================================================================================
         * Corporate Partner part
         */
        function add_corporate_partner() {
                $this->check_user(array("admin"));
                if (isset($_POST['partner_name'])) {
                        // process restaurant data
                        $this->load->library('form_validation');

                        // new field
                        $this->form_validation->set_rules('initiative', 'Initiative', 'trim|required');
                        $this->form_validation->set_rules('opportunity_str', 'Opportunity String', 'trim|required');
                        $this->form_validation->set_rules('main_title', 'Title', 'trim|required');
                        $this->form_validation->set_rules('sub_title', 'Sub Title', 'trim|required');
                        $this->form_validation->set_rules('address', 'Address', 'trim|required');

                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
                        //$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check');
                        //$this->form_validation->set_rules('new', 'Is new restaurant', 'trim|required');

                        //$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password2]');
                        $this->form_validation->set_rules('code', 'Referer ID', 'trim|required|callback_referer_check');

                        //$this->form_validation->set_message('matches', 'Password field and Password conf field do not match');
                        //
                        //$this->form_validation->set_rules('price', 'Price', 'trim|required|is_numeric');
                       // $this->form_validation->set_rules('currency', 'Currency', 'trim|required|is_natural_no_zero');
                       // $this->form_validation->set_rules('tax', 'Tax', 'trim|required|is_numeric');

                        $this->form_validation->set_rules('partner_name');
                        $this->form_validation->set_rules('website');
                        $this->form_validation->set_rules('first_name');
                        $this->form_validation->set_rules('last_name');
                        $this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                        $this->form_validation->set_rules('phone');
                        $this->form_validation->set_rules('logo');
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // validate form
                        if ($this->form_validation->run() === false) {
                               $data = array('title' => 'Add Corporate partner', 'body' => 'corporate_partner');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
								// logo upload
								$logo = "";
                                if ($_FILES['logo']['size']) {
										$config['file_name'] = date('Ymdhis').'-'.$_FILES['logo']['name'];
                                        $config['upload_path'] = './uploads/corporate_partner/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
                                        if ($this->upload->do_upload('logo')) {
                                                $logo = $this->upload->data();
                                                 $logo = $logo['file_name'];
                                                 $logo_name = $_FILES['logo']['name'];
                                        }
                                }
								
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->insert_corporate_partner($logo);

                                // send email confirmation
                                // expend -- add referer code
                                $initiative = $this->input->post('initiative');
                                $opportunity_str = $this->input->post('opportunity_str');

                                $code = trim($this->input->post('code'));
                                $unique_url = 'http://www.greenearthappeal.org/?refid=' . $code;
                                $email = $this->input->post('email');
                               // $username = $this->input->post('username');
                                //$password = $this->input->post('password');
                                $name = $this->input->post('partner_name');
                                $first_name = $this->input->post('first_name');
                               // $new = $this->input->post('new');

                                // create ticker
                               // $this->create_ticker($this->input->post('tree_nums'), $code);

                                // load template email
                              /*   $template_id = ($new == 1) ? 1 : 3;
                                $email_template = $this->user_model->load_email_template($template_id);

                                $subject = $email_template->subject;

                                $message = $email_template->message;
                                $message = str_ireplace(array('[name]', '[username]', '[password]', '[unique_url]', '[first_name]', '[initiative]', '[opportunity_str]'), array($name, $username, $password, $unique_url, $first_name, $initiative, $opportunity_str), $message );
                                /* $new = $this->input->post('new');
                                        $new_str = $new ? 'You are a new restaurant' : 'You are an exists restaurant'; */
                               /*  $config = array();
                                $config['to'] = $email;
                                if ($new) {
                                        $config['from'] = 'marvin@greenearthappeal.org';
                                        $config['from_name'] = 'Marvin Baker – Green Earth Appeal';
                                        $config['signature'] = false;
                                } else {
                                        $config['from'] = 'partners@greenearthappeal.org';
                                        $config['from_name'] = 'Partnerships Department – Green Earth Appeal';
                                        $config['signature'] = false;
                                }
                                $config['subject'] = $subject;
                                $config['message'] = $message;

                                $pct_sendmail = $this->send_mail($config); */ 

                                // return to homepage
                                redirect(site_url());
                        }
                } else {
                        $data = array('title' => 'Add Corporate partner', 'body' => 'corporate_partner');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }
		
		
		
		  function edit_corporate_partner($id = 0) {
                $this->check_user(array('admin'));

                $this->load->model('user_model');
             

                if (isset($_POST['partner_name'])) {
					
                         $this->load->library('form_validation');

                        // new field
                        $this->form_validation->set_rules('initiative', 'Initiative', 'trim|required');
                        $this->form_validation->set_rules('opportunity_str', 'Opportunity String', 'trim|required');
                        $this->form_validation->set_rules('main_title', 'Title', 'trim|required');
                        $this->form_validation->set_rules('sub_title', 'Sub Title', 'trim|required');
                        $this->form_validation->set_rules('address', 'Address', 'trim|required');

                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
                        $this->form_validation->set_rules('code', 'Referer ID', 'trim|required|callback_referer_check');
                        $this->form_validation->set_rules('partner_name');
                        $this->form_validation->set_rules('website');
                        $this->form_validation->set_rules('first_name');
                        $this->form_validation->set_rules('last_name');
                        $this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                        $this->form_validation->set_rules('phone');
                        $this->form_validation->set_rules('logo');
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // validate form
                        if ($this->form_validation->run() === false) {
                               $data = array('title' => 'Edit Corporate partner', 'body' => 'corporate_partner_edit');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
								// logo upload
								$logo = "";
                                if ($_FILES['logo']['size']) {
										$config['file_name'] = date('Ymdhis').'-'.$_FILES['logo']['name'];
                                        $config['upload_path'] = './uploads/corporate_partner/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
                                        if ($this->upload->do_upload('logo')) {
                                                $logo = $this->upload->data();
                                                 $logo = $logo['file_name'];
                                                 $logo_name = $_FILES['logo']['name'];
                                        }
                                }
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->update_corporate_partner($id,$logo);
								redirect(site_url('user/corporate_partner_list'));
						}
					   
                } else {
                        $this->load->model('user_model');
                        $data2 = $this->user_model->load_corporate_partner($id);
                        //$this->check_user();
                        $data = array('title' => 'Edit Corporate partner', 'body' => 'corporate_partner_edit');
                        $this->data = array_merge($this->data, $data, $data2);
                        $this->load->view('template', $this->data);
                }
		  }
				
		 function delete_corporate_partner($id) {
                $this->check_user(array("admin"));
                $this->load->database();
                $query = "DELETE FROM pct_corporate_partner WHERE id=$id";
                $this->db->query($query);
               redirect(site_url('user/corporate_partner_list'));
        }
		
		  /**
         * =================================================================================================================================
         * corporate list part
         */
		
		 function corporate_partner_list() {
                $this->check_user(array("admin"));
                $this->load->model('user_model');
                $rows = $this->user_model->list_corporate_partner();
                $this->load->view('template', array('title' => 'Corporate Partner List', 'body' => 'corporate_partner_list', 'rows' => $rows));
        }
        /**
         * =================================================================================================================================
         * restaurant part
         */
        function add_restaurant() {
                $this->check_user(array("admin", "manager","initiative_editor"));
                if (isset($_POST['add_restaurant'])) {
                        // process restaurant data
                        $this->load->library('form_validation');

                        // new field
                        $this->form_validation->set_rules('initiative', 'Initiative', 'trim|required');
                        $this->form_validation->set_rules('opportunity_str', 'Opportunity String', 'trim|required');

                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
                        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check');
                        $this->form_validation->set_rules('new', 'Is new restaurant', 'trim|required');

                        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password2]');
                        $this->form_validation->set_rules('code', 'Referer ID', 'trim|required|callback_referer_check');

                        $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');
                        //
                        $this->form_validation->set_rules('price', 'Price', 'trim|required|is_numeric');
                        $this->form_validation->set_rules('currency', 'Currency', 'trim|required|is_natural_no_zero');
                        $this->form_validation->set_rules('tax', 'Tax', 'trim|required|is_numeric');

                        $this->form_validation->set_rules('restaurant');
                        $this->form_validation->set_rules('website');
                        $this->form_validation->set_rules('first_name');
                        $this->form_validation->set_rules('last_name');
                        $this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                        $this->form_validation->set_rules('phone');
                        //$this->form_validation->set_rules('password2', 'Password Confirm', 'trim|required|match[password]');
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Add restaurant', 'body' => 'restaurant');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {

                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->insert_restaurant();

                                // send email confirmation
                                // expend -- add referer code
                                $initiative = $this->input->post('initiative');
                                $opportunity_str = $this->input->post('opportunity_str');

                                $code = trim($this->input->post('code'));
                                $unique_url = 'http://www.greenearthappeal.org/?refid=' . $code;
                                $email = $this->input->post('email');
                                $username = $this->input->post('username');
                                $password = $this->input->post('password');
                                $name = $this->input->post('restaurant');
                                $first_name = $this->input->post('first_name');
                                $new = $this->input->post('new');

                                // create ticker
                                $this->create_ticker($this->input->post('tree_nums'), $code);

                                // load template email
                                $template_id = ($new == 1) ? 1 : 3;
                                $email_template = $this->user_model->load_email_template($template_id);

                                $subject = $email_template->subject;

                                $message = $email_template->message;
                                $message = str_ireplace(array('[name]', '[username]', '[password]', '[unique_url]', '[first_name]', '[initiative]', '[opportunity_str]'), array($name, $username, $password, $unique_url, $first_name, $initiative, $opportunity_str), $message );
                                /* $new = $this->input->post('new');
                                        $new_str = $new ? 'You are a new restaurant' : 'You are an exists restaurant'; */
                                $config = array();
                                $config['to'] = $email;
                                if ($new) {
                                        $config['from'] = 'partners@greenearthappeal.org';
                                        $config['from_name'] = 'Partnerships Department – Green Earth Appeal';
                                        $config['signature'] = false;
                                } else {
                                        $config['from'] = 'partners@greenearthappeal.org';
                                        $config['from_name'] = 'Partnerships Department – Green Earth Appeal';
                                        $config['signature'] = false;
                                }
                                $config['subject'] = $subject;
                                $config['message'] = $message;

                                $pct_sendmail = $this->send_mail($config);

                                // return to homepage
                                redirect(site_url());
                        }
                } else {
                        $data = array('title' => 'Add restaurant', 'body' => 'restaurant');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }

        function edit_restaurant($id = 0) {
                $this->check_user(array('admin', 'restaurant', 'manager','initiative_editor'));

                $this->load->model('user_model');
                $this->data['latest_certificate_url'] = $this->user_model->get_latest_certificate_data('latest_certificate', $_SESSION['login']['id']);
                $this->data['latest_certificate_snippet'] = $this->user_model->get_latest_certificate_data('code_snippet', $_SESSION['login']['id']);

                // process for restaurant
                if ($_SESSION['login']['type'] == 'restaurant') {
                        if ($id == 0) {
                                $id = $_SESSION['login']['id'];
                        } elseif ($id != $_SESSION['login']['id']) {
                                show_error('you do not have permisson on this restaurant', 500);
                        }
                }

                if (isset($_POST['edit_restaurant'])) {
                        // proccess save restaurant
                        $this->load->library('form_validation');
                        // validate form
                        $this->form_validation->set_rules('initiative', 'Initiative', 'trim|required');
                        $this->form_validation->set_rules('opportunity_str', 'Opportunity String', 'trim|required');

                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check2[' . $id . ']');
                        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check[' . $id . ']');

                        // expend
                        $this->form_validation->set_rules('email2', 'Email (Solicitor)', 'trim|valid_email');

                        $this->form_validation->set_rules('address', 'Address', 'trim|required');
                        $this->form_validation->set_rules('city', 'City', 'trim|required');
                        $this->form_validation->set_rules('state', 'Area', 'trim|required');
                        $this->form_validation->set_rules('post_code', 'Post Code', 'trim|required');

                        $this->form_validation->set_rules('password', 'Password', 'matches[password2]');
                        $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');
                        if ($_SESSION['login']['type'] == 'admin') {
                                $this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                                //
                                $this->form_validation->set_rules('price', 'Price', 'trim|required|is_numeric');
                                $this->form_validation->set_rules('currency', 'Currency', 'trim|required|is_natural_no_zero');
                                $this->form_validation->set_rules('tax', 'Tax', 'trim|required|is_numeric');
                                $this->form_validation->set_rules('cc_email', 'CC Email', 'trims');
                        }
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // run validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Edit partner', 'body' => 'restaurant_edit');
                                $data2 = $this->user_model->load_restaurant($id);
                                $this->data = array_merge($this->data, $data, $data2);
                                $this->load->view('template', $this->data);
                        } else {
                                $logo = '';
                                // check upload
                                if ($_FILES['logo']['size']) {
                                        $config['upload_path'] = './uploads/company/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
                                        if (!$this->upload->do_upload('logo')) {
                                                $data = array('title' => 'Edit partner', 'body' => 'restaurant_edit', 'error' => $this->upload->display_errors());
                                                $data2 = $this->user_model->load_restaurant($id);
                                                $this->data = array_merge($this->data, $data, $data2);
                                                $this->load->view('template', $this->data);
                                                return;
                                        } else {
                                                $logo = $this->upload->data();
                                                $logo = $logo['file_name'];
                                        }
                                }
                                $this->user_model->update_restaurant($id, $logo);
                                redirect(site_url());
                        }
                } else {
                        $this->load->model('user_model');
                        $data2 = $this->user_model->load_restaurant($id);
                        //$this->check_user();
                        $data = array('title' => 'Edit partner', 'body' => 'restaurant_edit');
                        $this->data = array_merge($this->data, $data, $data2);
                        $this->load->view('template', $this->data);
                }
        }

        function delete_restaurant($id) {
                $this->check_user(array("admin", "manager","initiative_editor"));
                $this->load->database();
                $this->db->query("DELETE FROM pct_users WHERE id=$id");
                $this->db->query("DELETE FROM pct_restaurants WHERE user_id=$id");
                $this->db->query("DELETE FROM pct_figures WHERE user_id=$id");
                $this->db->query("DELETE FROM pct_referer WHERE user_id=$id");
                $this->db->query("DELETE FROM pct_invoices WHERE user_id=$id");
                redirect(site_url());
        }

        function login_as_user($id) {
                $this->load->database();
                $query = $this->db->query("SELECT * FROM pct_users WHERE id='" . $id . "'");
                if ($query->num_rows()) {
                        $row = $query->row_array();
                        unset($_SESSION['login']);
                        $_SESSION['login'] = $row;
                        $_SESSION['login']['from_admin'] = true;
                        redirect(site_url());
                }
        }
		function restaurant_autologin($activation_key) {
                $this->load->database();
                $query = $this->db->query("SELECT * FROM pct_users WHERE activation_key='" . $activation_key . "' AND type='restaurant'");
				//echo "<pre>"; print_r($query); die;
                if ($query->num_rows()) {
                        $row = $query->row_array();
                        unset($_SESSION['login']);
                        $_SESSION['login'] = $row;
                        $_SESSION['login']['from_admin'] = true;
                        redirect(site_url());
                }
        }
		
		function confirm_stages($id) {
                $this->load->database();
                $query = $this->db->query("SELECT * FROM pct_users WHERE id='1'");
                if ($query->num_rows()) {
                        $row = $query->row_array();
                        unset($_SESSION['login']);
                        $_SESSION['login'] = $row;
						//echo "<pre>"; print_r($row); die;
                        $_SESSION['login']['from_admin'] = true;
                        redirect(site_url('user/edit_grant_user/'.$id));
                }
        }
		
		function confirm_grant_stage($id) {
                $this->load->database();
                $query = $this->db->query("SELECT * FROM pct_users WHERE id='1551'");       //grant admin userid
                if ($query->num_rows()) {
                        $row = $query->row_array();
                        unset($_SESSION['login']);
                        $_SESSION['login'] = $row;
						//echo "<pre>"; print_r($row); die;
                        $_SESSION['login']['from_admin'] = true;
                        redirect(site_url('user/edit_grant_user/'.$id));
                }
        }

		
		function add_partner_monthly_figures($random_token = Null) {

                $this->load->model('user_model');
                
                if (isset($_POST['add_monthly_figure'])) {
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('trees', 'Your Tree Number', 'trim|required|is_numeric');
                        // run validation
                        if ($this->form_validation->run() === false) {
								$row = $this->user_model->check_random_number_exist($random_token);
                                $data = array('title' => 'Submit figure', 'body' => 'add_partner_monthly_figures');
                                $this->data = array_merge($this->data, $data, $row);
                                $this->load->view('template', $this->data);
                        } else {
								//echo "<pre>"; print_r($_POST); die;
								if(!empty($_POST)){
										$total_trees = 0;
										$free_trees = 0;
										$old_user = 0;
										$old_rows = 0;
										$new_rows = 0;
										$client_trees = 0;
										$panid =  $this->input->post('user_id');
									 $sql = "SELECT u.*,bp.first_name,bp.last_name,bp.company FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.id='$panid'";    
									 $restaurant_data = $this->db->query($sql)->row();
									 $res_data['first_name'] = $restaurant_data->first_name;
									 $res_data['last_name'] = $restaurant_data->last_name;
									 $res_data['email'] = $restaurant_data->email;
									 $res_data['company'] = $restaurant_data->company;
									 $res_data['free_trees'] = '0';
									 $res_data['tree_nums'] = $this->input->post('trees');
									 
									 $_SESSION['restaurant'] = (array)$restaurant_data;  							 
								 
									 if (is_array($res_data) && count($res_data)) {
										$data_row = array();
										$data_row =  $res_data;
										$data_row['password'] = $this->randomKey(6);
										if (!is_numeric($data_row['tree_nums']) || !trim($data_row['password']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)
										) {
												continue;
										}
										
										$rff_temp = str_replace(" ","_",$data_row['company']);           //referid as company name
										$data_row['referer_id'] = str_replace("’","",strtolower($rff_temp));
										
										$data_row['username'] = $data_row['email'];
										
										// create referer id
										$referer_id = trim($data_row['referer_id']);
										$i = 0;
										while (!$this->referer_check($referer_id)) {
												$i++;
												$referer_id = $data_row['referer_id'] . $i;
										}
										$data_row['referer_id'] = $referer_id;
										
										//$data_row['short_code'] =  trim($data_row['short_code']);    //to show short_code as refer_id in tpcc api
										$data_row['short_code'] = $referer_id;    
										
										if($data_row['free_trees']==""){
											$data_row['free_trees']=0;
										}
										$client_trees = $data_row['tree_nums'];
									
										if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['restaurant']['id'])) {       
											  
												$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
											
												$query = $this->db->query($qr);
												if ($query->num_rows()) {
													   
														$rowq = $query->row_array();
														$tr = $rowq['tree_nums'];
														$f_tr = $tr + $data_row['tree_nums']+$rowq['free_trees'];
														
														$qru = "UPDATE pct_initiative_partner SET tree_nums = '" . $f_tr . "' WHERE user_id = '" . $rowq['user_id'] . "' ";
														$this->db->query($qru);
												}
												continue;
										}
										if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) {
											
												$old_user = 1;
												$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
												$query = $this->db->query($qr);
												if ($query->num_rows()) {
														$rowq = $query->row_array();
														$tr = $rowq['tree_nums'];
														$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
												}
												$client_trees = $f_tr;
												/* $check_em = "SELECT * FROM pct_users AS u INNER JOIN pct_referer AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";    //to get old referr_id */
												$check_em = "SELECT * FROM pct_referer WHERE user_id='" . $rowq['user_id'] . "' ";    //to get old referr_id
												$getuser_id = $this->db->query($check_em);
												 if ($getuser_id->num_rows()) {
													  $ress = $getuser_id->row_array();
													  $data_row['referer_id'] = $ress['code'];
												 }
												
												//continue;
										}

										if(isset($_SESSION['restaurant']['id'])){
										
											$check_ini_id = "SELECT * FROM pct_initiative_partner WHERE user_id='" . $_SESSION['restaurant']['id'] . "' ";    //to get  initiative id
												$getini_id = $this->db->query($check_ini_id);
												 if ($getini_id->num_rows()) {
													  $ress1 = $getini_id->row_array();
													  $data_row['initiative_id'] = $ress1['initiative'];
												 }
										}
										
										if ($old_user == 0) {
												$data_row['old_user'] = $old_user;
												$data_row['client_trees'] = $client_trees;
												$new_rows++;
										} else if ($old_user == 1) {
												$data_row['old_user'] = $old_user;
												$data_row['client_trees'] = $client_trees;
												$old_rows++;
										}

										$data_rows[] = $data_row;
										$total_trees += $data_row['tree_nums'];
										$free_trees += $data_row['free_trees'];
										
									}
									
									$price = $_SESSION['restaurant']['price'];
									$bulk_partner = $this->user_model->load_initiative($_SESSION['restaurant']['id']);
									$invoice_trees = $this->user_model->load_invoice_trees($_SESSION['restaurant']['id']);
									$current_trees = $total_trees;
									$all_free_trees = $free_trees;
									$all_invoice_trees = $total_trees+$free_trees;
									//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
									$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums']+ $bulk_partner['free_trees'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']; 
									$tax = $_SESSION['restaurant']['tax'];
									$sub_total = $current_trees * $price / 100;
									$total_price = $sub_total * ( 100 + $tax ) / 100;
									$total_price = number_format($total_price, 2);
									$data = array('title' => 'Confirmation', 'body' => 'confirm_restaurant_trees', 'total_trees' => $total_trees, 'current_trees' => $all_invoice_trees, 'total_price' => $total_price, 'tax' => $tax, 'sub_total' => $sub_total, 'total_rows' => $new_rows, 'total_rows_updated' => $old_rows);
									$this->data = array_merge($this->data, $data);
									
									//var_dump($data_rows);
									if (count($data_rows)) {
										//echo "<pre>"; print_r($data_rows); die;
											$_SESSION['login']['data'] = array();
											$_SESSION['login']['data']['rows'] = json_encode($data_rows);
											$_SESSION['login']['data']['total'] = $total_price;
											$_SESSION['login']['data']['total_trees'] = $total_trees;
											$_SESSION['login']['data']['current_tree'] = $current_trees;
											$_SESSION['login']['data']['all_invoice_trees'] = $all_invoice_trees;
											$_SESSION['login']['data']['all_free_trees'] = $all_free_trees;
						
											// expend
											// $_SESSION['login']['data']['code']   = $referer_id;
											$_SESSION['login']['data']['address'] = $bulk_partner['address'];
											$_SESSION['login']['data']['city'] = $bulk_partner['city'];
											$_SESSION['login']['data']['state'] = $bulk_partner['state'];
											$_SESSION['login']['data']['post_code'] = $bulk_partner['post_code'];
										
									}
									 $this->load->view('template', $this->data);
							}
							
                        }
                } else {
						$row = $this->user_model->check_random_number_exist($random_token);
						
                        $data = array('title' => 'Submit figure', 'body' => 'add_partner_monthly_figures','random_token' => $random_token);
						$this->data = array_merge($data, $row);
						//echo "<pre>"; print_r($this->data); die;
                        $this->load->view('template', $this->data);
                }
        }
		
		function add_partner_monthly_figures_cfdapi() {
				$this->load->database();
                $this->load->model('user_model'); 
				
				$sql = "SELECT * from pct_partner_hubspot_monthly_figures WHERE submitted='1' AND processed='0'";    
				$figures = $this->db->query($sql)->row_array();
				//echo "<pre>"; print_r($figures); die;
				if(empty($figures)){
					exit("No record found to process...");
				}			
				$total_trees = 0;
				$free_trees = 0;
				$old_user = 0;
				$old_rows = 0;
				$new_rows = 0;
				$client_trees = 0;
				$panid =   $figures['partner_id'];
				
				 $sql = "SELECT u.*,bp.first_name,bp.last_name,bp.company FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.id='$panid'";    
				 $restaurant_data = $this->db->query($sql)->row();
				 $res_data['first_name'] = $restaurant_data->first_name;
				 $res_data['last_name'] = $restaurant_data->last_name;
				 $res_data['email'] = $restaurant_data->email;
				 $res_data['company'] = $restaurant_data->company;
				 $res_data['free_trees'] = '0';
				 $res_data['tree_nums'] = $figures['trees'];
				 $partner_token = $figures['random_token'];
				 $hs_company_id = $figures['hs_company_id'];
				 
				 $_SESSION['restaurant'] = (array)$restaurant_data;  							 
			 
				 if (is_array($res_data) && count($res_data)) {
					$data_row = array();
					$data_row =  $res_data;
					$data_row['password'] = $this->randomKey(6);
					if (!is_numeric($data_row['tree_nums']) || !trim($data_row['password']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)
					) {
							continue;
					}
					
					$rff_temp = str_replace(" ","_",$data_row['company']);           //referid as company name
					$data_row['referer_id'] = str_replace("’","",strtolower($rff_temp));
					
					$data_row['username'] = $data_row['email'];
					
					// create referer id
					$referer_id = trim($data_row['referer_id']);
					$i = 0;
					while (!$this->referer_check($referer_id)) {
							$i++;
							$referer_id = $data_row['referer_id'] . $i;
					}
					$data_row['referer_id'] = $referer_id;
					
					//$data_row['short_code'] =  trim($data_row['short_code']);    //to show short_code as refer_id in tpcc api
					$data_row['short_code'] = $referer_id;    
					
					if($data_row['free_trees']==""){
						$data_row['free_trees']=0;
					}
					$client_trees = $data_row['tree_nums'];
				
					if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['restaurant']['id'])) {       
						  
							$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
						
							$query = $this->db->query($qr);
							if ($query->num_rows()) {
								   
									$rowq = $query->row_array();
									$tr = $rowq['tree_nums'];
									$f_tr = $tr + $data_row['tree_nums']+$rowq['free_trees'];
									
									$qru = "UPDATE pct_initiative_partner SET tree_nums = '" . $f_tr . "' WHERE user_id = '" . $rowq['user_id'] . "' ";
									$this->db->query($qru);
							}
							continue;
					}
					if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) {
						
							$old_user = 1;
							$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
							$query = $this->db->query($qr);
							if ($query->num_rows()) {
									$rowq = $query->row_array();
									$tr = $rowq['tree_nums'];
									$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
							}
							$client_trees = $f_tr;
							/* $check_em = "SELECT * FROM pct_users AS u INNER JOIN pct_referer AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";    //to get old referr_id */
							$check_em = "SELECT * FROM pct_referer WHERE user_id='" . $rowq['user_id'] . "' ";    //to get old referr_id
							$getuser_id = $this->db->query($check_em);
							 if ($getuser_id->num_rows()) {
								  $ress = $getuser_id->row_array();
								  $data_row['referer_id'] = $ress['code'];
							 }
							
							//continue;
					}

					if(isset($_SESSION['restaurant']['id'])){
					
						$check_ini_id = "SELECT * FROM pct_initiative_partner WHERE user_id='" . $_SESSION['restaurant']['id'] . "' ";    //to get  initiative id
							$getini_id = $this->db->query($check_ini_id);
							 if ($getini_id->num_rows()) {
								  $ress1 = $getini_id->row_array();
								  $data_row['initiative_id'] = $ress1['initiative'];
							 }
					}
					
					if ($old_user == 0) {
							$data_row['old_user'] = $old_user;
							$data_row['client_trees'] = $client_trees;
							$new_rows++;
					} else if ($old_user == 1) {
							$data_row['old_user'] = $old_user;
							$data_row['client_trees'] = $client_trees;
							$old_rows++;
					}

					$data_rows[] = $data_row;
					$total_trees += $data_row['tree_nums'];
					$free_trees += $data_row['free_trees'];
					
				}
				
				$price = $_SESSION['restaurant']['price'];
				$bulk_partner = $this->user_model->load_initiative($_SESSION['restaurant']['id']);
				$invoice_trees = $this->user_model->load_invoice_trees($_SESSION['restaurant']['id']);
				$current_trees = $total_trees;
				$all_free_trees = $free_trees;
				$all_invoice_trees = $total_trees+$free_trees;
				//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
				$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums']+ $bulk_partner['free_trees'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']; 
				$tax = $_SESSION['restaurant']['tax'];
				$sub_total = $current_trees * $price / 100;
				$total_price = $sub_total * ( 100 + $tax ) / 100;
				$total_price = number_format($total_price, 2);
									
				//var_dump($data_rows);
				if (count($data_rows)) {
					//echo "<pre>"; print_r($data_rows); die;
						$_SESSION['login']['data'] = array();
						$_SESSION['login']['data']['rows'] = json_encode($data_rows);
						$_SESSION['login']['data']['total'] = $total_price;
						$_SESSION['login']['data']['total_trees'] = $total_trees;
						$_SESSION['login']['data']['partner_token'] = $partner_token;
						$_SESSION['login']['data']['hs_company_id'] = $hs_company_id;
						$_SESSION['login']['data']['current_tree'] = $current_trees;
						$_SESSION['login']['data']['all_invoice_trees'] = $all_invoice_trees;
						$_SESSION['login']['data']['all_free_trees'] = $all_free_trees;
	
						// expend
						// $_SESSION['login']['data']['code']   = $referer_id;
						$_SESSION['login']['data']['address'] = $bulk_partner['address'];
						$_SESSION['login']['data']['city'] = $bulk_partner['city'];
						$_SESSION['login']['data']['state'] = $bulk_partner['state'];
						$_SESSION['login']['data']['post_code'] = $bulk_partner['post_code'];
						
					//echo "<pre>"; print_r($_SESSION['login']['data']); die;
					$update_record = [];
					$update_record['random_token'] = $partner_token;
					$update_record['processed'] = '1';
					$this->db->update_batch('pct_partner_hubspot_monthly_figures', array($update_record), 'random_token');
		 
					$this->confirm_partner_monthly_figures();
				}
        }
		
		
		 /**
         *   Section for confirm partner monthlyfigures
         */
        function confirm_partner_monthly_figures() {
			 $this->load->database();
             $this->load->model('user_model');
			if (!isset($_SESSION['login']['data']) || empty($_SESSION['login']['data']['rows'])) {
                        unset($_SESSION['login']['data']);
                        redirect(site_url());
                        die();
                }

                $data = json_decode($_SESSION['login']['data']['rows']);
				//echo "<pre>"; print_r($data); die;
				
				$redirect = 0;        //$redirect = 1;  for tpcc api call
                $bulk_partner = $this->user_model->load_initiative($_SESSION['restaurant']['id']);
				
                $initiative = $bulk_partner['initiative_name'];
                $initiative_id = $bulk_partner['initiative'];
                $activation_email =  $bulk_partner['activation_email'];
                $send_invoices =  $bulk_partner['send_invoices'];
                $invoice_free_trial =  $bulk_partner['free_trial'];
				
                $request_for_funds =  $bulk_partner['request_for_funds'];

                $attach_cert_inemails =  $bulk_partner['attach_cert_inemails'];
				
				$counter_type =  $bulk_partner['counter_type'];         
			
				$bk_image 	=  $bulk_partner['bk_image'];
				
				$partner_bk_image =  $bulk_partner['partner_bk_image'];
				
				if($partner_bk_image != ""){                                   			//check partner background cert image first
					$bk_image 	=  $bulk_partner['partner_bk_image'];
				}else{
					$bk_image 	=  $bulk_partner['bk_image'];
				}
			
                $opportunity_str = '';

               
				$_SESSION['figure_history_key'] = $this->randomKey(10);
                $config = array();
				$trial_trees = 0; 
                $current_month_text = date("F_Y");
                foreach ($data AS &$row) {
                        // store client into database
                        $row = (array) $row;
						if($redirect==1){
						    $unique_url =  $bulk_partner['website_url'].'/'. $row['short_code'];
						  }else{
							$unique_url =  $bulk_partner['website_url'].'/?refid=' . $row['referer_id'];
						  }
                        // create the ticker
                       // $this->create_ticker($row['tree_nums'], $row['referer_id']);
					     $total_client_trees = $row['client_trees']+$row['free_trees'];
	
                        //$this->create_ticker($total_client_trees, $row['referer_id']);        //update this to update trees in logo
						if($counter_type != 0){															// if counter is set on partner
							if($counter_type==1){
								 $this->create_ticker($total_client_trees, $row['referer_id']);            //GEA tickers
							}elseif($counter_type==2){
								$this->create_cfd_ticker($total_client_trees, $row['referer_id']);        //CFD tickers
							}elseif($counter_type==3){
								$this->create_non_partner_ticker($total_client_trees, $row['referer_id']);        //Non partner tickers
							}
							
						}else{                                                                              // if counter is set on initiative
							if($ini_counter_type==1){
								 $this->create_ticker($total_client_trees, $row['referer_id']);            //GEA tickers
							}elseif($ini_counter_type==2){
								$this->create_cfd_ticker($total_client_trees, $row['referer_id']);        //CFD tickers
							}elseif($ini_counter_type==3){
								$this->create_non_partner_ticker($total_client_trees, $row['referer_id']);       //Non partner tickers
							}
						}
						
						$this->create_cfd_small_ticker($total_client_trees, $row['referer_id'], 'black');
						
						$this->create_cfd_small_ticker($total_client_trees, $row['referer_id'], 'green');
						
						$this->create_cfd_small_ticker($total_client_trees, $row['referer_id'], 'white');
						
						
                        $ticker_url = base_url() . 'tickers/ticker_' . $row['referer_id'] . '.png';
                        $cert_thumb_url = base_url() . 'cert/thumb_' . $row['referer_id'] . '.jpg';
                        $cert_image_url = base_url() . 'cert/full_' . $row['referer_id'] . '.jpg';
						$row['activation_key'] = $this->randomKey(6);
                        $activation_url = base_url() . 'index.php/user/client_login/' . $row['activation_key'];
                        $auto_login_link = base_url() . 'index.php/user/auto_login_client_link/' . $row['activation_key'];
						 if ($row['old_user'] == 0) {
								if($activation_email==1){
									$email_template = $this->user_model->load_initiative_email_template($initiative_id,2);  // send activation email for new clients
								}else{
									$email_template = $this->user_model->load_initiative_email_template($initiative_id,3); 
								}
								
						  }else{
								$email_template = $this->user_model->load_initiative_email_template($initiative_id,5);     //send email for update clients
						  } 
						  
                        // create the certificate
                        $restaurant = $row['company'];
                        $total_trees = $row['tree_nums']+$row['free_trees'];
                        $client_trees = $row['client_trees']+$row['free_trees'];
						if($redirect==1){                  //update trees to pct_sublevel_initiative
								$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $row['email'] . "' ";
								$query = $this->db->query($qr);
								if ($query->num_rows()) {
										$rowq = $query->row_array();
										$tr = $rowq['tree_nums'];
										$total_client_trees = $tr + $row['tree_nums']+$row['free_trees'];
										
										$qru = "UPDATE pct_sublevel_initiative SET tree_nums = '" . $total_client_trees . "' WHERE email = '" . $row['email'] . "' ";
										$this->db->query($qru);
								}
						}	
                        $calculated_cups = $total_client_trees*1000;
		
							if($client_trees=='1'){
								 $client_trees = number_format($total_trees).' '.'tree';
							}else{
								$client_trees = number_format($total_trees).' '.'trees';
							}
							$bk_image 	=  $bulk_partner['bk_image'];
				
							$partner_bk_image =  $bulk_partner['partner_bk_image'];
							
							if($partner_bk_image != ""){                                   			//check partner background cert image first
								$bk_image 	=  $bulk_partner['partner_bk_image'];
							}else{
								$bk_image 	=  $bulk_partner['bk_image'];
							}
						
                        if ($row['old_user'] == 0) {
							//echo "here"; die;
								 $row['activation_email'] = $activation_email;
								if($redirect==1){
								 $row['auto_login_link'] = 1;
								}
								$row['figure_history_key'] = $_SESSION['figure_history_key'];
                                $client_id = $this->user_model->insert_monthly_figure_initiative_client($row);
                        } else {
								//echo "there"; die;
								$row['figure_history_key'] = $_SESSION['figure_history_key'];
                                $client_id = $this->user_model->update_initiative_client($row);
                        }
						//to add client certificate url
						$client_certificate_url = 'https://www.greenearthappeal.org/panacea/uploads/certs/clients/'.'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $client_id . '.pdf'; 
														
						//$all_counters 	= $this->get_cfd_gea_counters();						
                        $message = $email_template->message;
						$total_client_trees = number_format($total_client_trees);
						
						$all_counters 	= $this->get_cfd_gea_counters();	
						
                        $message = str_ireplace(array('[contact_person]','[activation_url]','[auto_login_link]','[company_name]','[total_trees]','[current_trees]', '[username]', '[password]', '[unique_url]', '[ticker_url]','[initiative_name]','[calculated_cups]','[cert_thumb_url]','[cert_image_url]','[certificate_url]','[cfd_counter]','[gea_counter]'), array($row['first_name'], $activation_url,$auto_login_link, $row['company'],$total_client_trees, $total_trees, $row['username'], $row['password'], $unique_url, $ticker_url,$initiative,$calculated_cups,$cert_thumb_url,$cert_image_url,$client_certificate_url,$all_counters['cfd_counter'],$all_counters['gea_counter']), $message);

					
						$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups,$total_client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate1']));
						$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups,$total_client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate2']));
						$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups,$total_client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate3']));
						$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups,$total_client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate4']));

                        $ds = DIRECTORY_SEPARATOR;
                 
                        $certificate_sub = $certificate_msg = $certificate_text = '';

                        $certificate_sub = preg_replace("@[\t]@", '', $bulk_partner['subject']);
                        $certificate_cc_email = preg_replace("@[\t]@", '', $bulk_partner['cc_email']);
                        $certificate_msg = preg_replace("@[\t]@", '', $bulk_partner['message']);
                        $certificate_text = preg_replace("@[\t]@", '', $bulk_partner['certificate_text']);
                        $certificate_text = stripslashes($certificate_text);
                        $certificate_text = str_ireplace(array('[name]', '[total_trees]', '[initiative]', '[opportunity_str]'), array($restaurant, $total_trees, $initiative, $opportunity_str), $certificate_text);

                        $cert_name = 'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $client_id . '.pdf';
						
						 if ($row['old_user'] == 0) {
							 
							 $row['initiative_name']  = $bulk_partner['initiative_name'];
							 $row['unique_url']       =  $unique_url;
							 $row['ticker_url'] 	  = $ticker_url;
							 $row['cert_name'] 	  	  = $cert_name;
							 $row['email_title']      = $bulk_partner['sender_email_title'];
								//insert all client data in "pct_client_activation_emaildata" table for email
							$this->user_model->insert_initiative_client_email_records($row,$client_id);
							
						}
						
                        $cert_path = dirname(__FILE__) . '/../../uploads/certs/clients/';
                        require('libraries/cert-ini.php');

						$image_file_name = date('Ymdhis');

						$image_path = dirname(__FILE__) . '/../../uploads/certs/clients/images/';
						$image_url = dirname(__FILE__) . '/../../cert/';
						$pdf_path = $cert_path.$cert_name;
						if (extension_loaded('imagick')){   //extension imagick is installed
							$this->create_pdf_to_image($pdf_path, $image_file_name, $image_path); 
						}	
						if (extension_loaded('imagick')){   		//extension imagick is installed
							$this->create_cert_pdf_to_image($pdf_path, $row['referer_id'], $image_url); //to create pef images for clients
						}							
						//echo "<pre>"; print_r($row); die;
						$data3 = array();                       //store clients latest updated certificate
						$data3['client_id'] = $client_id;
						$data3['latest_certificate'] = $cert_name;
						$data3['certificate_image'] = 'certificate_'.$image_file_name.'.jpg';
						
						$this->user_model->insert_client_latest_certificate($data3);
						
                        // send email to client
                        $config['to'] =  $row['email'];
						
                       // $config['to'] =  'testing.whizkraft1@gmail.com';
                       
                        if (isset($certificate_sub) && !empty($certificate_sub)) {
                                $config['subject'] = $certificate_sub;
                        } else {
                                $config['subject'] = $email_template->subject;
                        }

                        $config['subject'] = str_ireplace(array('[contact_person]', '[company_name]','[initiative_name]','[total_trees]'), array($cname, $restaurant,$initiative,$total_trees), $config['subject']);
										
                        if (isset($certificate_msg) && !empty($certificate_msg)) {
                                $config['message'] = $certificate_msg;
                        } else {
                                $config['message'] = $message;
                        }
						if($attach_cert_inemails==1){              //to stop attach client certificate if option in disabled
							$attach_cert = array(dirname(__FILE__) . '/../../uploads/certs/clients/' . $cert_name);
						}else{
							$attach_cert = "";
						}
						
                        if ($row['old_user'] == 0) {
								if($activation_email==1){
									$config['attach'] = "";
								}else{
									$config['attach'] = $attach_cert;
								}
						}else{
							$config['attach'] = $attach_cert;
						}
                        // stop send_mail
					    $config['email_title'] = $bulk_partner['sender_email_title'];
						
						$enable_email = $email_template->enable_email;
						
						if($enable_email == 1){
							$this->send_mail($config);   //only send email if it is enabled in initiative email templates
						}

                        if (isset($certificate_cc_email) && !empty($certificate_cc_email)) {
                                $config2 = array();
                                $config2['to'] = $certificate_cc_email;

                                if (isset($certificate_sub) && !empty($certificate_sub)) {
                                        $config2['subject'] = $certificate_sub;
                                } else {
                                        $config2['subject'] = $email_template->subject;
                                }

                                if (isset($certificate_msg) && !empty($certificate_msg)) {
                                        $config2['message'] = $certificate_msg;
                                } else {
                                        $config2['message'] = $message;
                                }

                                $config2['attach'] = array(dirname(__FILE__) . '/../../uploads/certs/clients/' . $cert_name);

                                //$this->send_mail($config2);
                                // stop send_mail
                        }
						
						//insert record for send monthly invoice
						if($send_invoices=="monthly"){
							$record['tree_nums']      = $row['tree_nums'];
							$record['company']   = $row['company'];
							$record['free_trees']     = $row['free_trees'];
							$record['initiative_id']  = $row['initiative_id'];
							if($request_for_funds == 1)
							{
							  $record['request_for_funds']  = '1';
							}
							$record['partner_id']      = $_SESSION['restaurant']['id'];
							$insert_monthly = $this->user_model->insert_initiative_monthly_invoice_records($record);  
						}
						if ($row['old_user'] == 0) {
							   $trial_trees+= $row['tree_nums'];         //get sum on all new user trees
						 }
                }
                // prepare to create the invoice
                $cert_path = '';
                $pct_rows = &$data;
				
                $item2 = array();
				
                $bulk_partner = $this->user_model->load_initiative($_SESSION['restaurant']['id']);
				$email_template = $this->user_model->load_initiative_email_template($bulk_partner['initiative'],4);      

                $item2['name'] = "Total number of trees";
				//echo "<pre>"; print_r($_SESSION['login']['data']); die;
                $item2['free_trees'] = $_SESSION['login']['data']['all_free_trees'];
				$item2['unit'] = $_SESSION['restaurant']['price'] / 100;
				if($invoice_free_trial=='1'){        //if free trial send 0 invoice for new clients
					$item2['tree_nums'] = $_SESSION['login']['data']['current_tree'] - $trial_trees;
				}else{
				   $item2['tree_nums'] = $_SESSION['login']['data']['current_tree'];
				}
				
                switch ($_SESSION['restaurant']['currency']) {
                        case 1:
                                $item2['currency'] = '$';
                                break;
                        case 2:
                                $item2['currency'] = '&pound;';
                                break;
						case 3:
								$item2['currency'] = '€';
								break;
                }

                $item2['price'] = $item2['tree_nums'] * $item2['unit'];
                $tax = $_SESSION['restaurant']['tax'];
                $sub_total = $currency . $item2['price'];
                //$total_no = number_format($item2['price'] * (100 + $tax)/100, 2);
                $total_no = $item2['price'] * (100 + $tax) / 100;
                $total = $item2['currency'] . number_format($total_no, 2);
                $total_amt = $item2['currency'] . number_format($total_no, 2);

                // store invoice in database
                $data1 = array();
                $data1['user_id'] = $_SESSION['restaurant']['id'];
                $data1['date_created'] = 'NOW()';
                $data1['total'] = $total_no;
                $data1['state'] = 0;
               // $data1['trees'] = $_SESSION['login']['data']['current_tree'];
			    $data1['figure_history_key'] = $_SESSION['figure_history_key'];
			   
                $data1['trees'] = $_SESSION['login']['data']['all_invoice_trees'];
				$total_planted_trees = $_SESSION['login']['data']['all_invoice_trees'];
				if($send_invoices=="onupload"){
					$invoice_id = $this->user_model->insert_initiative_invoice($data1);
				}else{
					$invoice_id = 'partner_'.$bulk_partner['id'];
				}
				

                // expend
                $invoice_company = $cert_company = $bulk_partner['company'];
                $address = $_SESSION['login']['data']['address'];
                $city = $_SESSION['login']['data']['city'];
                $state = $_SESSION['login']['data']['state'];
                $post_code = $_SESSION['login']['data']['post_code'];
                $ini_first_name = $bulk_partner['first_name'];

                $current_month_text = date("F_Y");
                $invoice_name = 'The_Green_Earth_Appeal_Invoice_' . $current_month_text . '_' . $invoice_id . '.pdf';

                $cert_name = 'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $invoice_id . '.pdf';
                // update the invoice
                $invoice_number = $invoice_id;
                $invoice_data = array();
                $invoice_data['id'] = $invoice_id;
                $invoice_data['number'] = $invoice_number;
              
				if($send_invoices=="onupload" && $request_for_funds==1){
					$invoice_name = 'The_Green_Earth_Appeal_Request_For_Funds_' . $current_month_text . '_' . $invoice_id .'.pdf';
					$invoice_data['name'] = $invoice_name;
				}else{
				   $invoice_data['name'] = $invoice_name;
				}

                // this is only for bulkpartner, so we can load previous figure month
                $invoice_data['latest_certificate'] = $cert_name;
                $invoice_data['month'] = date('mY');
				
				$monthly['invoice_id']    	= $invoice_id;
				$monthly['first_name']    	= $bulk_partner['first_name'];
				$monthly['company_name']  	= $bulk_partner['company'];
				$monthly['total_trees'] 	= $_SESSION['login']['data']['total_trees'];
				$monthly['current_trees'] 	= $_SESSION['login']['data']['all_invoice_trees'];
				$monthly['total']			= $total_amt;
				$monthly['ticker_url'] 		= "";
				$monthly['unique_url'] 		= "";
				$monthly['initiative_name'] = $initiative;
			
				if($send_invoices=="onupload"){
					$this->db->update_batch('pct_invoices', array($invoice_data), 'id');
				}
				if($send_invoices=="onupload" && $request_for_funds==1){
					
					require_once('libraries/gocardless/vendor/autoload.php');	
						 //$token = "sandbox_ZH42k6x0Ee7-owAISf_-HLlRsbeWQBc07NwUNN2Z";  //sandbox token
						 
						$new_gocardless_enable_date  = strtotime("2020-01-20 11:00:00");
						$user_register_date  		 = strtotime($bulk_partner['user_register_date']);

						if($user_register_date > $new_gocardless_enable_date){
							$token = "live_icu2SM1QdA7w2GTGJF1mKV58dU00O0VM58y-XVAb";      //New GC user
							$_SESSION['gocardless_user'] = "new";
						}else{
							$token = "live_roMMEqbGeKFuN_sHAyfqYGLOFsMvmEDpVeP5Ltec";      //Old user
							$_SESSION['gocardless_user'] = "old";
						}
						 
						 $client = new \GoCardlessPro\Client([
									   'access_token' => $token,
										'environment' => \GoCardlessPro\Environment::LIVE  //LIVE/SANDBOX
									]);
									
							/* // create connection with CFD */
							$db_name = "carbonfd_cfddb";
							$db_host = "localhost";
							$db_user = "carbonfd_cfddbus";
							$db_pass = "vVZKUCXr@wAU";
							
							$con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
							// Check connection
							if (mysqli_connect_errno())
							  {
							  echo "Failed to connect to MySQL: " . mysqli_connect_error();
							  }
							  
							 
							 $dashboard_user_email = $bulk_partner['dashboard_user_email'];
							 
							if($dashboard_user_email != ""){
								$user_email = $dashboard_user_email;
							}else{
								$user_email = $bulk_partner['email'];
							}
							  
						 	$cfwp_sql = "SELECT * from `cfwp_users` AS u INNER JOIN cfwp_ls_partners_details AS pd ON u.ID=pd.user_id where u.user_email='".$user_email."' AND pd.cancelled='0'";
						
							$cfwp_result = mysqli_query($con,$cfwp_sql);
							
							$mendate_id = "";
							if(mysqli_num_rows($cfwp_result) > 0) {   
							
								$cfd_row = mysqli_fetch_assoc($cfwp_result);
								$cfwp_user_id 	= $cfd_row['ID'];
								$pan_user_id 	= ""; 
								$mendate_id 	= $cfd_row['mendate_id'];
								$customer_id 	= $cfd_row['customer_id'];
							
							}else{
								
								$pan_user_id 	= $bulk_partner['pan_user_id']; 
								$partner_sql = "SELECT * from `cfwp_ls_partners_details` where panacea_id='".$pan_user_id."' AND cancelled='0'";
								
								$partner_result = mysqli_query($con,$partner_sql);
								
								if(mysqli_num_rows($partner_result) > 0) { 
								
									$cfdp_row = mysqli_fetch_assoc($partner_result);
									
									$cfwp_user_id 	= ""; 
									$mendate_id 	= $cfdp_row['mendate_id'];
									$customer_id 	= $cfdp_row['customer_id'];
								}
								
							}
							
							$gocardless_link = false;
							//if(mysqli_num_rows($cfwp_result) > 0) {                  //change this sign after testing to >
							
							if($mendate_id != "") {  
							
									/*Call goCardless API to create payment using mendate ID*/	
									switch ($bulk_partner['currency']) {
										    case 1:
													$payment_currency = 'GBP';                  //usd But it does not exist
													break;
											case 2:
													$payment_currency = 'GBP';
													break;
											case 3:
													$payment_currency = 'EUR';
													break;
									}
									
									try {
											if(isset($bulk_partner['delay_payment_by']) && $bulk_partner['delay_payment_by'] > 0){
												$delay_payment_by =	$bulk_partner['delay_payment_by'];
												$today_date = date('Y-m-d');
												$charge_date = date('Y-m-d', strtotime($today_date. '+ '.$delay_payment_by.' days'));	
												
												 $payment= $client->payments()->create([
													  "params" => ["amount" => $total_no*100,
																   "currency" => $payment_currency,
																    "charge_date" => $charge_date,
																   "links" => [
																	 "mandate" =>$mendate_id
																   ]]
													]);

											}else{
												
												$payment= $client->payments()->create([
												  "params" => ["amount" => $total_no*100,
															   "currency" => $payment_currency,
															   "links" => [
																 "mandate" =>$mendate_id
															   ]]
												]);
											}
											$gocardless_link = false;
											
											if(!empty($payment)){
												
												$insert_cfwp_sql = "INSERT INTO cfwp_ls_partners_payments SET user_id = '{$cfwp_user_id}',panacea_id = '{$pan_user_id}',customer_id = '{$customer_id}',amount = '{$total_no}',trees = '{$total_planted_trees}',payment_id = '{$payment->id}',payment_status = 'Pending',invoice_id = {$invoice_id}";

												 mysqli_query($con,$insert_cfwp_sql); 
												 
												 $payment_status['id'] = $invoice_id;
												 $payment_status['state'] = '-3';
												 $payment_status['payment_type'] = '1';
												 $this->db->update_batch('pct_invoices', array($payment_status), 'id');
											}
											
									}catch (\GoCardlessPro\Core\Exception\ApiException $e) {
											   $error = $e->getMessage();
											} catch (\GoCardlessPro\Core\Exception\MalformedResponseException $e) {
											    $error = $e->getMessage();
											} catch (\GoCardlessPro\Core\Exception\ApiConnectionException $e) {
											   $error = $e->getMessage();
									}
									
							}else{
									$activation_key = $this->randomKey(25);
									$pcinvoice['gc_activation_key'] = $activation_key;
								    $pcinvoice['id'] = $invoice_id;
								    $this->db->update_batch('pct_invoices', array($pcinvoice), 'id');
										 
									$gocardless_link = "https://carbonfreedining.org/pay-by-gocardless/?activation_key=$activation_key";
								
							}
					$_SESSION['login']['currency']    = $_SESSION['restaurant']['currency'];
					$_SESSION['login']['price']		  = $_SESSION['restaurant']['price'];

				    $invoice_name = 'The_Green_Earth_Appeal_Request_For_Funds_' . $current_month_text . '_' . $invoice_id .'.pdf';
					require('libraries/monthly_funds_request.php');
					
				}else{
						if($bulk_partner['enable_invoice_data'] == 1){		
						
							require('libraries/invoice-dynamic.php');           
						}else{					
						
							require('libraries/invoice.php');				
						}
				}

                $cname = $bulk_partner['first_name'];
                $restaurant = $bulk_partner['company'];


                $total_trees = $_SESSION['login']['data']['total_trees'];
				
				if($total_trees=='1'){
					 $client_trees = number_format($total_trees).' '.'tree';
				}else{
					$client_trees = number_format($total_trees).' '.'trees';
				}
                $calculated_cups = $total_trees*1000;
				
				$all_counters 	= $this->get_cfd_gea_counters();
				$this->create_ticker($all_counters['gea_counter'], 'gea_counter');           //create GEA ticker for all partners
				$this->create_cfd_ticker($all_counters['cfd_counter'], 'cfd_counter');		 //create CFD ticker for all partners
				
				$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate1']));
				$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate2']));
				$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate3']));
				$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate4']));

                $certificate_sub = $certificate_msg = $certificate_text = '';
                $certificate_cc_email = preg_replace("@[\t]@", '', $bulk_partner['cc_email']);
                $certificate_sub = preg_replace("@[\t]@", '', $bulk_partner['subject']);
                $certificate_msg = preg_replace("@[\t]@", '', $bulk_partner['message']);
                $certificate_text = preg_replace("@[\t]@", '', $bulk_partner['certificate_text']);
                $certificate_text = stripslashes($certificate_text);
                $certificate_text = str_ireplace(array('[name]', '[total_trees]', '[initiative]', '[opportunity_str]'), array($restaurant, $total_trees, $initiative, $opportunity_str), $certificate_text);

                require('libraries/cert-ini.php');
							
                // create the ticker
                $refid = $this->user_model->get_refid($_SESSION['restaurant']['id']);
                $ticker_url = base_url() . 'tickers/ticker_' . $refid->code . '.png';
				
				if($initiative_id =='34' || $initiative_id =='35' || $initiative_id =='25' || $initiative_id =='31' ){
					$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
				}else{
					 $this->create_ticker($total_trees, $refid->code);            //GEA tickers
				}
				
				$this->create_cfd_small_ticker($total_trees, $refid->code, 'black');
				
				$this->create_cfd_small_ticker($total_trees, $refid->code, 'green');
				
				$this->create_cfd_small_ticker($total_trees, $refid->code, 'white');
				
				//to create pdf images for initiatives
				$cert_thumb_url = base_url() . 'cert/thumb_' . $refid->code . '.jpg';
                $cert_image_url = base_url() . 'cert/full_' . $refid->code . '.jpg';
				if (extension_loaded('imagick')){   		//extension imagick is installed
					$cert_path = dirname(__FILE__) . '/../../uploads/certs/';
					$pdf_path = $cert_path.$cert_name;
					$image_url = dirname(__FILE__) . '/../../cert/';
					
					if($send_invoices=="onupload"){
						$this->create_cert_pdf_to_image($pdf_path, $refid->code, $image_url); 
					}
				}	
				
                //$unique_url = 'http://www.greenearthappeal.org/?refid=' . $refid->code;
                $unique_url = $bulk_partner['website_url'].'/?refid=' . $refid->code;

				if($send_invoices=="monthly"){
					$monthly['id']    		= $insert_monthly_invoice_id;
					$monthly['ticker_url'] 		= $ticker_url;
					$monthly['unique_url'] 		= $unique_url;
					$monthly['cert_thumb_url'] 		= $cert_thumb_url;
					$monthly['cert_image_url'] 		= $cert_image_url;
				 //$this->db->update_batch('pct_initiative_monthly_invoice_data', array($monthly), 'id');
				}
                // send certificate to the restaurant
                $config = array();
                
			     if($send_invoices=="onupload"){
					 $config['to'] = $_SESSION['restaurant']['email'];
				 }else{
					 $config['to'] = 'panacea@greenearthappeal.org';
					
				 }   
				//$config['to'] =  'testing.whizkraft1@gmail.com';
				
				 
                /* $config['subject'] = $bulk_partner['subject'];
                        $message = str_ireplace(array('[name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]'),
                        array( $restaurant, $total_trees, $data1['trees'], $total_amt, $bulk_partner['first_name'], $ticker_url),
                        $bulk_partner['message']);
                        $config['message'] = $message; */

						
                $subject = $email_template->subject;
                $config['subject'] = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]', '[unique_url]','[initiative_name]'), array($cname, $restaurant, $total_trees, $data1['trees'], $total_amt, $bulk_partner['first_name'], $ticker_url, $unique_url,$initiative), $subject);
				
   
			    if($send_invoices=="onupload"){
					 $config['cc'] = $certificate_cc_email;    //main email
					 $config['bcc'] = 'panacea@greenearthappeal.org';
				 }else{
					 $config['cc'] = $certificate_cc_email;    //main email
					 $config['bcc'] = 'panacea@greenearthappeal.org';
				 } 
				   
				$partner_certificate_url = 'https://www.greenearthappeal.org/panacea/uploads/certs/'.$cert_name; //to add partner certificate url   
				
                $message = $email_template->message;
				
                $message = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]', '[unique_url]','[initiative_name]','[calculated_cups]','[cert_thumb_url]','[cert_image_url]','[certificate_url]','[cfd_counter]','[gea_counter]'), array($cname, $restaurant, $total_trees, $data1['trees'], $total_amt, $bulk_partner['first_name'], $ticker_url, $unique_url,$initiative,$calculated_cups,$cert_thumb_url,$cert_image_url,$partner_certificate_url,$all_counters['cfd_counter'],$all_counters['gea_counter']), $message);
				
                $config['message'] = $message;
				
				if($send_invoices=="onupload" && $request_for_funds==1){
					$config['attach'] = array(dirname(__FILE__) . '/../../uploads/request_for_funds/' . $invoice_name, dirname(__FILE__) . '/../../uploads/certs/' . $cert_name);
				}else{
					$config['attach'] = array(dirname(__FILE__) . '/../../uploads/invoices/' . $invoice_name, dirname(__FILE__) . '/../../uploads/certs/' . $cert_name);
				}
				$config['email_title'] = $bulk_partner['sender_email_title'];
				
				//if($redirect!=1){	
				 if($send_invoices=="onupload"){
						$enable_email = $email_template->enable_email;
						if($enable_email == 1){
								$this->send_mail($config);
						}
				 }
				 $update_record['random_token'] = $_SESSION['login']['data']['partner_token'];
				 $update_record['processed'] = '2';
				 $this->db->update_batch('pct_partner_hubspot_monthly_figures', array($update_record), 'random_token');
				 
				 $hs_company_id = $_SESSION['login']['data']['hs_company_id'];
				 if($hs_company_id !=""){
					// $hapikey = "7aed04ae-daa3-4a6f-a17f-452346b7eb0c";        //developer
					 $hapikey = "71c60211-d5ee-435f-9a0b-c4ee06e1e716";        //live
								
								$update_comp = array(                  //to update the company
											'properties' => array(
											array(
												'name' => 'panacea_upload_submitted',
												'value' => 'Yes'            
											)));	
								$contact_url = 'https://api.hubapi.com/companies/v2/companies/'.$hs_company_id.'/'.'?hapikey='.$hapikey;
								$compch = @curl_init($contact_url);
								$update_comp_post_json = json_encode($update_comp);
								curl_setopt($compch, CURLOPT_URL, $contact_url);
								curl_setopt($compch, CURLOPT_CUSTOMREQUEST, "PUT");
								@curl_setopt($compch, CURLOPT_POSTFIELDS, $update_comp_post_json);
								curl_setopt($compch, CURLOPT_RETURNTRANSFER, 1);
								@curl_setopt($compch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
								$compresponse = @curl_exec($compch);
								$status_code3 = @curl_getinfo($compch, CURLINFO_HTTP_CODE);
								$curl_errors3 = curl_error($compch);
								@curl_close($compch);
					 
				 }
				//}
				echo "Record uploaded successfully!";
                unset($_SESSION['figure_history_key']);
                unset($_SESSION['login']['data']);
                unset($_SESSION['login']);
				return;
		}
        /**
         *   Section for figure
         */
        function add_a_figure() {
                $this->check_user('restaurant');
                $this->load->model('user_model');
                // calculate previous month
                $prev_month = (date('m') == 1) ? '12' . (date('Y') - 1) : (date('m') - 1) . '' . date('Y');
                $has_figure = 0;
                // stop
                $has_figure = $this->user_model->has_figure($prev_month, $_SESSION['login']['id']);
                $this->data['has_figure'] = $has_figure;
                $this->data['latest_certificate_url'] = $this->user_model->get_latest_certificate_data('latest_certificate', $_SESSION['login']['id']);

                if (isset($_POST['add_a_figure']) && ( $_SESSION['login']['from_admin'] || !$this->data['has_figure']) ) {
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('trees', 'Your Tree Number', 'trim|required|is_numeric');
                        // run validation
                        if ($this->form_validation->run() === false) {
                                $this->load->model('user_model');
                                $row = $this->user_model->load_restaurant($_SESSION['login']['id']);
                                // calculate previous month
                                // $prev_month = (date('m') == 1) ? '12-'.(date('Y')-1) : (date('m')-1).'-'.date('Y');
                                $prev_month_text = date("F Y", strtotime("first day of previous month"));

                                $data = array('title' => 'Submit figure', 'body' => 'add_a_figure', 'prev_month' => $prev_month_text);
                                $this->data = array_merge($this->data, $data, $row);
                                $this->load->view('template', $this->data);
                        } else {
                                // confirmation display screen
                                $total_trees = $this->user_model->load_figures($_SESSION['login']['id']);
                                $restaurant = $this->user_model->load_restaurant($_SESSION['login']['id']);
                                $total_trees = $total_trees + $restaurant['tree_nums'] + (int) $this->input->post('trees');
                                $current_trees = (int) $this->input->post('trees');

                                //
                                $tax = $_SESSION['login']['tax'];
                                $sub_total = $current_trees * $_SESSION['login']['price'] / 100;
                                $total_price = $sub_total * (100 + $tax) / 100;
                                $total_price = number_format($total_price, 2);
                                $data = array('title' => 'Confirmation', 'body' => 'confirm_figure', 'total_trees' => $total_trees, 'total' => $total_price, 'tax' => $tax, 'sub_total' => $sub_total, 'current_trees' => $current_trees);
                                $this->data = array_merge($this->data, $data);
                                //var_dump($data_rows);
                                $_SESSION['login']['data'] = array();
                                $_SESSION['login']['data']['trees'] = $current_trees;
                                //$_SESSION['login']['data']['total'] = $total_price;
                                $_SESSION['login']['data']['total_trees'] = $total_trees;
                                $_SESSION['login']['data']['logo'] = $restaurant['logo'] = false;
                                $_SESSION['login']['data']['subject'] = $restaurant['subject'];
                                $_SESSION['login']['data']['cc_email'] = $restaurant['cc_email'];
                                $_SESSION['login']['data']['message'] = $restaurant['message'];
                                $_SESSION['login']['data']['certificate_text'] = $restaurant['certificate_text'];

                                // store figure in session for confirmation step
                                $figure = array();
                                $prev_month = (date('m') == 1) ? '12' . (date('Y') - 1) : (date('m') - 1) . date('Y');
                                $figure['month'] = $prev_month;
                                $figure['user_id'] = $_SESSION['login']['id'];
                                $figure['trees'] = (int) $this->input->post('trees');
                                $figure['initiative'] = $restaurant['initiative'];
                                $figure['opportunity_str'] = $restaurant['opportunity_str'];

                                $figure['restaurant'] = $restaurant['restaurant'];
                                $figure['first_name'] = $restaurant['first_name'];
                                // expend
                                $figure['address'] = $restaurant['address'];
                                $figure['city'] = $restaurant['city'];
                                $figure['state'] = $restaurant['state'];
                                $figure['post_code'] = $restaurant['post_code'];
                                $figure['subject'] = $restaurant['subject'];
                                $figure['cc_email'] = $restaurant['cc_email'];
                                $figure['message'] = $restaurant['message'];
                                $figure['certificate_text'] = $restaurant['certificate_text'];

                                $_SESSION['login']['data']['figure'] = $figure;

                                $this->load->view('template', $this->data);
                                //$this->user_model->insert_a_figure($data);
                        }
                } else {
                        $this->load->model('user_model');
                        $row = $this->user_model->load_restaurant($_SESSION['login']['id']);
                        // $prev_month = (date('m') == 1) ? '12-'.(date('Y')-1) : (date('m')-1).'-'.date('Y');
						//echo "<pre>"; print_r($row); die;
                        $prev_month_text = date("F Y", strtotime("first day of previous month"));
                        $data = array('title' => 'Submit figure', 'body' => 'add_a_figure', 'prev_month' => $prev_month_text);
                        $this->data = array_merge($this->data, $data, $row);
                        $this->load->view('template', $this->data);
                }
        }

        /**
         *   Section for arcive figure
         */
        function arcive_figures() {
                $this->check_user('restaurant');
                $this->load->model('user_model');
                $this->data['latest_certificate_url'] = $this->user_model->get_latest_certificate_data('latest_certificate', $_SESSION['login']['id']);
                $month_array = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");

                $rows = $this->user_model->get_figures($_SESSION['login']['id']);

                $this->load->view('template', array('title' => 'Figures History', 'body' => 'arcive_figures', 'rows' => $rows, 'months' => $month_array, 'username' => $_SESSION['login']['username'], 'email' => $_SESSION['login']['email']));
        }

        /**
         *   Section for previous month figure on intitiative
         */
        function intitiative_partner_figures() {
                $this->check_user('intitiative');
                $this->load->model('user_model');

                $month_array = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");

                $rows = $this->user_model->get_bulk_partner_figures($_SESSION['login']['id']);

                $this->load->view('template', array('title' => 'Figures History', 'body' => 'initiative_arcive_figures', 'rows' => $rows, 'months' => $month_array, 'username' => $_SESSION['login']['username'], 'email' => $_SESSION['login']['email']));
        }
		
		 /**
         *   Section for previous month figure on bulk_partner
         */
        function bulk_partner_figures() {
                $this->check_user('bulk_partner');
                $this->load->model('user_model');

                $month_array = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");

                $rows = $this->user_model->get_bulk_partner_figures($_SESSION['login']['id']);

                $this->load->view('template', array('title' => 'Figures History', 'body' => 'arcive_figures', 'rows' => $rows, 'months' => $month_array, 'username' => $_SESSION['login']['username'], 'email' => $_SESSION['login']['email']));
        }

        function cancel_figure() {
                unset($_SESSION['login']['data']);
                redirect(site_url('user/add_a_figure'));
        }

        function confirm_figure() {
                $this->check_user('restaurant');
				$this->load->model('user_model');
                if (!isset($_SESSION['login']['data']) || empty($_SESSION['login']['data']['figure'])) {
                        unset($_SESSION['login']['data']);
                        redirect(site_url());
                        die();
                }
				  $query = $this->db->query("SELECT * FROM pct_restaurants WHERE user_id=" .$_SESSION['login']['id']. "");
                  $res = $query->row_array(); 
				  $send_email_immediately = $res['send_email_immediately'];
                // store figure in database
               
                $figure = $_SESSION['login']['data']['figure'];

                //$total = $_SESSION['login']['data']['total'];
                // calculate total price
                $tax = $_SESSION['login']['tax'];
                //$sub_total = $figure['trees'] * $_SESSION['login']['price']/100;
                $sub_total_no = $figure['trees'] * $_SESSION['login']['price'] / 100;
                $sub_total = number_format($sub_total_no, 2);
                //$sub_total = number_format($sub_total, 2);
                $total_no = $sub_total_no * (100 + $tax) / 100;
                $total = number_format($total_no, 2);

                // store invoice in database
                $data1 = array();
                $data1['user_id'] = $_SESSION['login']['id'];
                $data1['date_created'] = 'NOW()';
                $data1['total'] = $total_no;
                $data1['state'] = 0;
                $data1['trees'] = $figure['trees'];
                $invoice_id = $this->user_model->insert_invoice($data1);
				
				$record['user_id'] = $_SESSION['login']['id']; 
				$record['month'] =  date('FY'); 
				//update auto email record if figure submit by restaurant
				$this->db->query("UPDATE pct_invite_restaurants_figures_records SET submit_figure='1' WHERE user_id=".$record['user_id']." AND month='".$record['month']."'");
				
                $item2 = array();
                $invoice_company = $cert_company = $figure['restaurant'];
                $item2['name'] = $figure['restaurant'];
                $month = date('F', strtotime('last month'));
                $item2['name'] = $item2['name'] . ' - ' . $month . ' Trees';

                $item2['unit'] = $_SESSION['login']['price'] / 100;
                $item2['tree_nums'] = $figure['trees'];

                // expend
                $city = $figure['city'];
                $state = $figure['state'];
                $post_code = $figure['post_code'];
                $address = $figure['address'];

                // create the invoice
                switch ($_SESSION['login']['currency']) {
                        case 1:
                                $item2['currency'] = '$';
                                break;
                        case 2:
                                $item2['currency'] = '&pound;';
                                break;
						case 3:
								$item2['currency'] = '€';
								break;
                }

                $item2['price'] = $sub_total_no;
                $sub_total = $item2['currency'] . $sub_total;
                $total = $item2['currency'] . $total;

                $prev_month_text = date("F_Y", strtotime("first day of previous month"));
                $invoice_name = 'The_Green_Earth_Appeal_Invoice_' . $prev_month_text . '_' . $invoice_id . '.pdf';
				$cert_name = 'The_Green_Earth_Appeal_Certificate_' . $prev_month_text . '_' . $invoice_id . '.pdf';

                // update the invoice
                // $invoice_number = date('dm').$invoice_id;
                $invoice_number = $invoice_id;
                $invoice_data = array();
                $invoice_data['id'] = $invoice_id;
                $invoice_data['number'] = $invoice_number;
                $invoice_data['name'] = $invoice_name;
				$invoice_data['latest_certificate'] = $cert_name;
                $this->db->update_batch('pct_invoices', array($invoice_data), 'id');
				
                // $invoice_name = 'invoice_'.$invoice_id.'.pdf';
                require('libraries/invoice.php');

                // create the certificate
                $initiative = $figure['initiative'];
                $opportunity_str = $figure['opportunity_str'];

                $restaurant = $figure['restaurant'];
                $first_name = $figure['first_name'];
                $total_trees = $_SESSION['login']['data']['total_trees'];
                $client_trees = $total_trees;

                $rest_logo = $_SESSION['login']['data']['logo'];
                $ds = DIRECTORY_SEPARATOR;

                // create the ticker
                $refid = $this->user_model->get_refid($_SESSION['login']['id']);
                $ticker_url = base_url() . 'tickers/ticker_' . $refid->code . '.png';
                $this->create_ticker($total_trees, $refid->code);
				
				if($send_email_immediately==0){
					$nextday['user_id'] =  $_SESSION['login']['id'];
					$nextday['restaurants_name'] = $restaurant;
					$nextday['total_trees'] =  $total_trees;
					$nextday['current_trees'] =  $figure['trees'];
					$nextday['total'] =  $total;
					$nextday['first_name'] =  $first_name;
					$nextday['ticker_url'] =  $ticker_url;
					$nextday['cert_name'] =  $cert_name;
					$nextday['invoice_name'] =  $invoice_name;
					$this->db->insert_batch('pct_restaurants_nextday_invoices_data', array($nextday));
				}

                $certificate_sub = $certificate_msg = $certificate_text = '';
                if ($figure['certificate_text'] != "") {
                        $certificate_sub = preg_replace("@[\t]@", '', $figure['subject']);
                        $certificate_msg = preg_replace("@[\t]@", '', $figure['message']);
                        $certificate_text = preg_replace("@[\t]@", '', $figure['certificate_text']);
                        $certificate_text = stripslashes($certificate_text);
                        $certificate_text = str_ireplace(array('[name]', '[total_trees]'), array($restaurant, $total_trees), $certificate_text);
                }
                $certificate_cc_email = preg_replace("@[\t]@", '', $figure['cc_email']);
                /*
                        $pct_logo_url = '';
                        if (file_exists( dirname(__FILE__).'/../../uploads/restaurant/'.$rest_logo ) ) {
                        $pct_logo = 'uploads/restaurant/'.$rest_logo;
                        $pct_logo_url = '<img src="'.$pct_logo.'" title="logo" />';
                        }
                 */
                //$prev_month_text = date("F_Y", strtotime("first day of previous month") );
                $cert_name = 'The_Green_Earth_Appeal_Certificate_' . $prev_month_text . '_' . $invoice_id . '.pdf';
                require('libraries/cert.php');

                //$figure[] = array_merge($this->data, $figure);

                $download_link = base_url("uploads/certs/" . $cert_name);
                $download_img_src = base_url("img/latest-certificate.jpg");
                $figure['latest_certificate'] = $cert_name;
                $figure['code_snippet'] = '<a href="' . $download_link . '" class="btn btn-info" target="_blank"><img src="' . $download_img_src . '" Title="Download Latest Certificate" alt="Download Latest Certificate"></a>';
                $this->user_model->insert_a_figure($figure);

                // send certificate to the restaurant
                $email_template = $this->user_model->load_email_template(5);
                $message = $email_template->message;
                $message = str_ireplace(array('[name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]'), array($restaurant, $total_trees, $figure['trees'], $total, $first_name, $ticker_url), $message);
                $config = array();
                $config['to'] = $_SESSION['login']['email'];
               // $config['to'] ="testing.whizkraft1@gmail.com";
                $config['cc'] = $certificate_cc_email;
                $config['bcc'] = "panacea@greenearthappeal.org";

                //$config['subject'] = $email_template->subject;
                if (isset($certificate_sub) && !empty($certificate_sub)) {
                        $config['subject'] = $certificate_sub;
                } else {
                        $config['subject'] = $email_template->subject;
                }
                //$config['message'] = $message;
                if (isset($certificate_msg) && !empty($certificate_msg)) {
                        $config['message'] = $certificate_msg;
                } else {
                        $config['message'] = $message;
                }
                $config['attach'] = array(dirname(__FILE__) . '/../../uploads/certs/' . $cert_name, dirname(__FILE__) . '/../../uploads/invoices/' . $invoice_name);
				if($send_email_immediately==1){
					$this->send_mail($config);
				}
                


                /* if(isset($certificate_cc_email) && !empty($certificate_cc_email)){
                        $config2 = array();
                        $config2['to'] = $certificate_cc_email;

                        if ( isset($certificate_sub) && !empty($certificate_sub) ) {
                        $config2['subject'] = $certificate_sub;
                        } else {
                        $config2['subject'] = $email_template->subject;
                        }

                        if ( isset($certificate_msg) && !empty($certificate_msg) ) {
                        $config2['message'] = $certificate_msg;
                        } else {
                        $config2['message'] = $message;
                        }

                        $config2['attach'] = array( dirname(__FILE__).'/../../uploads/certs/clients/'.$cert_name );

                        //$this->send_mail($config2);
                        // stop send_mail
                        } */

                unset($_SESSION['login']['data']);
                redirect(site_url());
        }

        /**
         *  =========================================================================================================
         * bulk partner part
         */
        function add_bulk_partner() {
                $this->check_user(array("admin", "manager"));
                if (isset($_POST['add_bulk_partner'])) {
                        // process restaurant data
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('initiative', 'Initiative', 'trim|required');
                        $this->form_validation->set_rules('opportunity_str', 'Opportunity String', 'trim|required');

                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
                        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check');
                        $this->form_validation->set_rules('company');
                        $this->form_validation->set_rules('code', 'Referer ID', 'trim|required|callback_referer_check');

                        //
                        $this->form_validation->set_rules('price', 'Price', 'trim|required|is_numeric');
                        $this->form_validation->set_rules('currency', 'Currency', 'trim|required|is_natural_no_zero');
                        $this->form_validation->set_rules('tax', 'Tax', 'trim|required|is_numeric');

                        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password2]');
                        $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');
                        $this->form_validation->set_rules('first_name');
                        $this->form_validation->set_rules('last_name');
                        $this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                        $this->form_validation->set_rules('phone');
                        $this->form_validation->set_rules('website');
                        //$this->form_validation->set_rules('password2', 'Password Confirm', 'trim|required|match[password]');
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');


                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Add bulk partner', 'body' => 'bulk_partner');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->insert_bulk_partner();

                                // send email confirmation
                                $email = $this->input->post('email');
                                $username = $this->input->post('username');
                                $password = $this->input->post('password');
                                $first_name = $this->input->post('first_name');

                                $initiative = $this->input->post('initiative');
                                $opportunity_str = $this->input->post('opportunity_str');

                                // logo upload
								$logo = "";
                                if ($_FILES['logo']['size']) {
                                        $config['upload_path'] = './uploads/company/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
                                        if ($this->upload->do_upload('logo')) {
                                                $logo = $this->upload->data();
                                                $logo = $logo['file_name'];
                                        }
                                }

                                // load template email
                                $code = trim($this->input->post('code'));
                                $ticker_url = base_url() . 'tickers/ticker_' . $code . '.png';
                                $unique_url = 'http://www.greenearthappeal.org/?refid=' . $code;
                                $email_template = $this->user_model->load_email_template(11);
                                $subject = $email_template->subject;
                                $name = $this->input->post('company');
                                $message = $email_template->message;

                                $message = str_ireplace(array('[name]', '[username]', '[password]', '[unique_url]', '[first_name]', '[ticker_url]', '[initiative]', '[opportunity_str]'), array($name, $username, $password, $unique_url, $first_name, $ticker_url, $initiative, $opportunity_str), $message);


                                // create ticker
                                $this->create_ticker($this->input->post('tree_nums'), $code);

                                // send email
                                $config = array();
                                $config['to'] = $email;
                                $config['subject'] = $subject;
                                $config['message'] = $message;
                                $pct_sendmail = $this->send_mail($config);

                                /*
                                        if ($pct_sendmail!==1) {
                                        echo $pct_sendmail;
                                        }
                                 */
                                // return to homepage
                                redirect(site_url());
                        }
                } else {
                        $data = array('title' => 'Add bulk partner', 'body' => 'bulk_partner');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }
		
		 /*
		*  =========================================================================================================
         * Company Initiative  partner from API
         */
        function change_initiative_partner_email_from_api() {
			   $this->load->database();
			   $this->load->model('user_model');	
			   
               if(!empty($_GET)){
				   	$data_row =  $this->utf8_converter($_GET);
					$old_email      = $data_row['old_email'];
					$email      	= $data_row['email'];
					
					$this->db->query("UPDATE pct_users SET email='".$email."',dashboard_user_email='".$email."' WHERE email='".$old_email."' AND type='intitiative'");
					//$this->db->query("UPDATE pct_users SET dashboard_user_email='".$email."' WHERE dashboard_user_email='".$old_email."' AND type='intitiative'");
			   }
		}
		
		 /*
		*  =========================================================================================================
         * Change Initiative partner date using API
         */
        function update_cfdpartner_info_api() {

               if(!empty($_GET)){
				   	$data_row =  $this->utf8_converter($_GET);
					$old_email      =  $data_row['old_email'];
					$email      	=  $data_row['email'];
					$first_name     =  $data_row['first_name'];
					$last_name      =  $data_row['last_name'];
					$company      	=  $data_row['company'];
					$city      		=  $data_row['city'];
					$phone      	=  $data_row['phone'];
					$post_code      =  $data_row['post_code'];
					$address        =  $data_row['address'];
					
					$user = $this->db->query('SELECT * FROM pct_users WHERE `email`="'.$old_email.'"')->row();
					if($user){
						$this->db->query("UPDATE pct_users SET email='".$email."',dashboard_user_email='".$email."' WHERE email='".$old_email."' AND type='intitiative'");
					
						$this->db->query('UPDATE pct_initiative_partner SET company="'.$company.'",first_name="'.$first_name.'",last_name="'.$last_name.'",phone="'.$phone.'",city="'.$city.'",post_code="'.$post_code.'",address="'.$address.'" WHERE user_id="'.$user->id.'"');
					}
			   }
		}
			
		 /*
		*  =========================================================================================================
         * Company Initiative  partner from API
         */
        function update_initiative_partner_from_api() {
			
				$this->load->database();
			    $this->load->model('user_model');	
				if(!empty($_GET)){
					   if (is_array($_GET) && count($_GET)) {
						   
						   $this->db->query('UPDATE pct_initiative_partner SET hs_company_id="'.$_GET['hs_company_id'].'",hs_deal_id="'.$_GET['hs_deal_id'].'" WHERE  user_id ="'.$_GET['partner_id'].'"');
					   }
				}
				return;

		}	
		
		/*
		  *  ================================================
          * Add sustainable redeem credit from API
        */
        function add_sustainable_redeem_credits() {
			
			if (is_array($_GET) && count($_GET)) {
				
				$data = array();
				$data_row =  $this->utf8_converter($_GET);
				
				$partner_id     =  $data_row['pan_id'];
				
				$data['partner_id']     =  $partner_id;
				$data['suscredits']     =  -$data_row['reduce_credits'];
				$data['processed']      =  "1";
				$data['redeemed']       =  "1";
				
				$this->db->insert_batch('pct_partner_sustainable_credits', array($data));
				
				$query = "SELECT SUM(suscredits) as credits FROM pct_partner_sustainable_credits where partner_id = $partner_id and processed='1'";
				$response = $this->db->query($query)->row();
				//echo "<pre>"; print_r($response); die;
				if($response->credits > 0){
					$credits = $response->credits;
				}else{
					$credits =  0;
				}
				echo $credits;
			}
			
		}
		 /*
		*  =========================================================================================================
         * Company Initiative  partner from API
         */
        function add_initiative_partner_from_api() {

                $this->load->database();
			    $this->load->model('user_model');	
			    $this->load->model('ambassador_model');	
										   
               if(!empty($_GET)){
				   
				    if (is_array($_GET) && count($_GET)) {
						$data_row = array();
						$data_row =  $this->utf8_converter($_GET);
						//echo "<pre>"; print_r($data_row);  
						$code = $this->randomKey(5);            //generate code for referid
						
						// send email confirmation
						$email = $data_row['email'];
						$username =  strtolower($data_row['first_name'].$code);
						if($data_row['password']==""){
							$password =  $this->randomKey(8);
						}else{
							$password =  $data_row['password'];
						}
						$first_name = str_replace(" ","",$data_row['first_name']);
						$last_name = $data_row['last_name'];
						$company  =  stripslashes($data_row['company']);
						
						
						$data       = array();
						$data['email']      = $email;
						$data['dashboard_user_email']  = $email;
						$data['username']   = $username;
						$data['password']   = md5($password);
						$data['type']       = 'intitiative';
						$data['active']     = 1;
						
						$data['price']      = '99';
						$data['tax']        = '0';
						
						if($data_row['is_suscredit'] == '1'){ 
							$data['is_suscredit']    = '1';
						}
						
						if($data_row['form_type'] == 'eu-non-ls-signp-form'){
							$data['currency']  = $data_row['currency'];
							if($data_row['currency'] == 1 ){                                  //if currency USD
								$data['price']    = '125';
							}
						}else{
							$data['currency']  = '2';
						}
						
						$this->db->insert_batch('pct_users', array($data));
						
						$user_id    = $this->db->insert_id();
						$data       = array();
						
						if($data_row['form_type']=='non-ls-signp-form'){
							$initiative = '35';
							$data['send_invoices']  = 'onupload';
						}elseif($data_row['form_type'] == 'eu-non-ls-signp-form'){
							
							$initiative = '25';
							$data['send_invoices']  = 'monthly';
							
						}elseif($data_row['form_type']=='ls-renewal-form' || $data_row['form_type']=='cfd-14-day-trial'){
							
							$initiative = '25';
							$data['send_invoices']  = 'monthly';
						} 
						
						$data['initiative']     = $initiative;
						
						$data['user_id']        = $user_id;
						$data['company']        = $company;
						$data['first_name']     = $first_name;
						$data['free_trial']  	= '0';
						$data['last_name']      = $last_name;
						$data['phone']          = $data_row['phone'];
						$data['tree_nums']      = $data_row['tree_nums'];
						$data['free_trees']      = '100';
						
						$this->user_model->update_hubspot_restaurants_total_trees($email, '100'); // update in hubspot company
						
						if(isset($data_row['ambassador_pan_id']) && $data_row['ambassador_pan_id']!=""){
							$data['ambassador_id']     =  $data_row['ambassador_pan_id'];
							
							/* send email to EU ambassador */
							if($data_row['form_type'] == "eu-non-ls-signp-form"){
								
									$email_template = $this->ambassador_model->load_eu_ambassdor_template(1);            //1 template ID
									$amb_first_name = $data_row['amb_firstname'];
									$message = str_ireplace(array('[restaurant_name]','[contact_person]'), array(stripslashes($company), stripslashes($amb_first_name)), $email_template->message);
									$subject = str_ireplace(array('[restaurant_name]','[contact_person]'), array(stripslashes($company), stripslashes($amb_first_name)), $email_template->subject);
									$config = array();
									//$config['to'] = "testing.whizkraft1@gmail.com";
									$config['to'] =  $data_row['amb_email_address'];              //send to associate EU Ambassador
									$config['message'] = $message;
									$config['subject'] = $subject;
									$this->eu_send_mail($config);	
									
							}
						}
						
						$this->db->insert_batch('pct_initiative_partner', array($data));
						
								
						// insert referer code
						$data2 = array();
						$data2['user_id']        = $user_id;
						$data2['code']           = $code;
						$pct_refererquery = 'INSERT INTO pct_referer
                        SET user_id="'.$data2['user_id'].'",code="'.$data2['code'].'"';
						$this->db->query($pct_refererquery);
						
						$query1 = "SELECT * FROM pct_initiative where id = $initiative";
						$get_initiative = $this->db->query($query1)->row();
						$initiative_name = $get_initiative->initiative_name;
						$website_url = $get_initiative->website;
						// load template email
						$ticker_url = base_url() . 'tickers/ticker_' . $code . '.png';
						$unique_url = $website_url.'/?refid=' . $code;
						$email_template = $this->user_model->load_initiative_email_template($initiative,0);
						$subject = $email_template->subject;
						$name =  $data_row['company'];
						$message = $email_template->message;

						$message = str_ireplace(array('[name]',['contact_person'], '[username]', '[password]', '[unique_url]', '[first_name]', '[ticker_url]', '[initiative]','[initiative_name]'), array($company,$first_name, $username, $password, $unique_url, $first_name, $ticker_url, $initiative_name,$initiative_name), $message);


						// create ticker
						$this->create_cfd_ticker($data['free_trees'], $code);
						
						$this->create_cfd_small_ticker($data['free_trees'], $code, 'black');
						
						$this->create_cfd_small_ticker($data['free_trees'], $code, 'green');
						
						$this->create_cfd_small_ticker($data['free_trees'], $code, 'white');
							
						$this->create_ticker_certificates($initiative); // to create tickers and certificates for initiatives
						
						$client_trees = '100';
						
						if($client_trees=='1'){
							 $client_trees = number_format($client_trees).' '.'tree';
						}else{
							$client_trees = number_format($client_trees).' '.'trees';
						}
						
						
						$calculated_cups = $client_trees*1000;  
						
						$current_month_text = date("F_Y");
						
						$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($company, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($get_initiative->certificate1));
						$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($company, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($get_initiative->certificate2));
						$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($company, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($get_initiative->certificate3));
						$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($company, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($get_initiative->certificate4));

						$certificate_sub = $certificate_msg = $certificate_text = '';
						
						$cert_name = 'The_Green_Earth_Appeal_Certificate_ls_'. $user_id . '.pdf';
						
						$bk_image =  $get_initiative->bk_image;
						require('libraries/cert-initiatives.php');
						
						sleep(2);
						
						if (extension_loaded('imagick')){   		//extension imagick is installed
							$cert_path = dirname(__FILE__) . '/../../uploads/certs/initiatives/';
							$pdf_path = $cert_path.$cert_name;
							$image_url = dirname(__FILE__) . '/../../cert/';
							$this->create_cert_pdf_to_image($pdf_path, $code, $image_url); 
						}
						
						if(isset($data_row['ambassador_pan_id']) && $data_row['ambassador_pan_id']!="0"){
							 $this->create_ambassador_certificate($data_row['ambassador_pan_id']);
						}
						
						echo $user_id;
						exit;
						
                   }
                }
        }
		
		 /**
         * =========================================================================================================
		 *  section to add create ambassador certificate
         */
		function create_ambassador_certificate($ambassador_id = NULL) { 
				  if($ambassador_id==0 || $ambassador_id==NULL){
					  return;
				  }
				 $user_sql = "SELECT * FROM pct_users WHERE `id`='$ambassador_id'";    
				 $user_res = $this->db->query($user_sql)->row();	
				 
				 
				 $ambassador_sql = "SELECT * FROM pct_initiative_partner WHERE `ambassador_id`='$ambassador_id'";    
				 $amb_res = $this->db->query($ambassador_sql)->result_array();
				 foreach($amb_res as $ambres){
					 $bulker_id  = $ambres['user_id'];
					 $client_treee = "SELECT (SUM(free_trees)+ SUM(tree_nums)) As total_trees FROM pct_initiative_partner WHERE `bulker_id`='$bulker_id'";    
					 $get_client_trees = $this->db->query($client_treee)->row();
					 $client_trees += $get_client_trees->total_trees; 
					 $rest_trees +=  $ambres['free_trees']+$ambres['tree_nums'];
					 
				 }
				 $client_trees = $client_trees + $rest_trees;
				 
				 $initiatives_id = 20;
				 $ini_sql = "SELECT * from pct_initiative WHERE `id`='$initiatives_id'";    
				 $bulk_partner = $this->db->query($ini_sql)->row();
				 $initiative_name = $bulk_partner->initiative_name; 
				 $ini_certificate_name = $user_res->email;	
				 $calculated_cups = $client_trees*1000; 
				
				 $client_trees = number_format($client_trees);
				 //echo "<pre>"; print_r($user_res); die;
				$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate1));
				$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate2));
				$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate3));
				$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate4));
				
				$certificate_sub = $certificate_msg = $certificate_text = '';
				$cert_name = 'The_Green_Earth_Appeal_Certificate_Ambassador_'. $ambassador_id . '.pdf';
				
				$refere_sql = "SELECT * from pct_referer WHERE `user_id`='$ambassador_id'";    
				$pct_referer = $this->db->query($refere_sql)->row();
				  
				if($pct_referer){
					
					$this->create_ambassador_ticker($client_trees, $pct_referer->code);
				
				}
				$bk_image =  $bulk_partner->bk_image;
				require('libraries/cert-initiatives.php');
				
				if (extension_loaded('imagick')){   		//extension imagick is installed
				
					$cert_path = dirname(__FILE__) . '/../../uploads/certs/initiatives/';
					$pdf_path = $cert_path.$cert_name;
					$image_url = dirname(__FILE__) . '/../../cert/ambassador_cert/';
					$this->create_amb_cert_pdf_to_image($pdf_path, $pct_referer->code, $image_url); 
								
				}
				return;
		
		}
		
			 /**
         * =========================================================================================================
		 *  section to add create_ticker_certificates
         */
		function create_ticker_certificates($initiativeid = NULL) {   
						$this->load->database();
					 //$initiatives_ids = 	array_unique($initiatives_ids);
					if($initiativeid!=""){
						$query = "SELECT * from pct_initiative WHERE id='$initiativeid'";   
					}else{
						$query = "SELECT * from pct_initiative"; 
					}

                     $initiatives_ids = $this->db->query($query)->result_array();
					foreach($initiatives_ids as $initiatives_id){
						// echo "<pre>"; print_r($initiatives_id); die;
						 $initiatives_id = $initiatives_id['id'];
						 $initiative_sql = "SELECT (SUM(free_trees)+ SUM(tree_nums)) As total_trees FROM pct_initiative_partner WHERE `initiative`='$initiatives_id'";    
						 $ini_res = $this->db->query($initiative_sql)->row();
						 $client_trees = $ini_res->total_trees;
						 
						 $ini_sql = "SELECT * from pct_initiative WHERE `id`='$initiatives_id'";    
						 $bulk_partner = $this->db->query($ini_sql)->row();
						 $initiative_name = $bulk_partner->initiative_name; 
						 $ini_certificate_name = $bulk_partner->ini_certificate_name; 
						 $restaurant = $bulk_partner->initiative_name; 
						 
						 $calculated_cups = $client_trees*1000; 
						 
						 $client_trees = number_format($client_trees);
						 //echo "<pre>"; print_r($bulk_partner); 
						$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate1));
						$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate2));
						$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate3));
						$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate4));
						
						$certificate_sub = $certificate_msg = $certificate_text = '';
						$cert_name = 'The_Green_Earth_Appeal_Certificate_Initiative_'. $initiatives_id . '.pdf';
						
						$this->create_initiative_ticker($client_trees, $initiatives_id);
						$bk_image =  $bulk_partner->bk_image;
						require('libraries/cert-initiatives.php');
					}
					//	echo "<pre>"; print_r($initiatives_ids); 
		
		}
            /**
         *  =========================================================================================================
         * Company Initiative  part
         */
        function add_initiative_partner() {
                $this->check_user(array("admin", "manager","initiative_editor"));
                if (isset($_POST['add_initiative_partner'])) {
					//echo "<pre>"; print_r($_POST); die;
					
                        // process restaurant data
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('initiative', 'Initiative', 'trim|required');
                       // $this->form_validation->set_rules('opportunity_str', 'Opportunity String', 'trim|required');

                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
                        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check');
                        $this->form_validation->set_rules('company');
                        $this->form_validation->set_rules('login_detail_email');
						$this->form_validation->set_rules('dashboard_user_email');
                        $this->form_validation->set_rules('logo');
                        $this->form_validation->set_rules('code', 'Referer ID', 'trim|required|callback_referer_check');

                        //
                        $this->form_validation->set_rules('price', 'Price', 'trim|required|is_numeric');
                        $this->form_validation->set_rules('currency', 'Currency', 'trim|required|is_natural_no_zero');
                        $this->form_validation->set_rules('tax', 'Tax', 'trim|required|is_numeric');

                        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password2]');
                        $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');
                        $this->form_validation->set_rules('first_name');
                        $this->form_validation->set_rules('send_invoices');
                        $this->form_validation->set_rules('free_trial');
                        $this->form_validation->set_rules('last_name');
                        $this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                        $this->form_validation->set_rules('phone');
                        $this->form_validation->set_rules('tech_support_email');
                        $this->form_validation->set_rules('delay_payment_by');
                        //$this->form_validation->set_rules('password2', 'Password Confirm', 'trim|required|match[password]');
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');


                        // validate form
                        if ($this->form_validation->run() === false) {
								$this->load->database();
								$query = "SELECT * FROM pct_initiative";
								$row = $this->db->query($query);
								$data3['results'] = $row->result_array();
                                $data = array('title' => 'Add Initiative Partner', 'body' => 'add_initiative_partner');
                                $this->data = array_merge($this->data, $data,$data3);
                                $this->load->view('template', $this->data);
                        } else {
                                $this->load->database();
                                $this->load->model('user_model');
								
								/* $logo = '';
                                // check upload
                                if ($_FILES['logo']['size']) {
										$config['file_name'] = date('Ymdhis').'-'.$_FILES['logo']['name'];
										$config['upload_path'] = './uploads/company/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
										
									if (!$this->upload->do_upload('logo')) {
											$data = array('title' => 'Add Initiative Partner', 'body' => 'add_initiative_partner', 'error' => $this->upload->display_errors());
											$this->data = array_merge($this->data, $data);
											$this->load->view('template', $this->data);
											return;
									} else {
											$logo = $this->upload->data();
											$logo = $logo['file_name'];
									}
								} */
									$file_name='';
									$image_data = $this->input->post('logo-image');
									if(!empty($image_data)){
										$file_name = date('Ymdhis').'_logo.png';
										$upload_path = './uploads/company/'.$file_name;
										$image_data = $this->input->post('logo-image');
										list($type, $image_data) = explode(';', $image_data);
										list(, $image_data)      = explode(',', $image_data);
										$image_data = base64_decode($image_data);
										file_put_contents($upload_path, $image_data);
									}
                                $this->user_model->insert_initiative_partner($file_name);

                                // send email confirmation
                                $email = $this->input->post('email');
                                $username = $this->input->post('username');
                                $login_detail_email = $this->input->post('login_detail_email');
                                $password = $this->input->post('password');
                                $first_name = $this->input->post('first_name');

                                $initiative = $this->input->post('initiative');
								$query1 = "SELECT * FROM pct_initiative where id = $initiative";
								$get_initiative = $this->db->query($query1)->row();
								$initiative_name = $get_initiative->initiative_name;
								$website_url = $get_initiative->website;
                               // $opportunity_str = $this->input->post('opportunity_str');

                              
                                // load template email
                                $code = trim($this->input->post('code'));
                                $ticker_url = base_url() . 'tickers/ticker_' . $code . '.png';
                                $unique_url = $website_url.'/?refid=' . $code;
                                $email_template = $this->user_model->load_initiative_email_template($initiative,0);
                                $subject = $email_template->subject;
                                $name = $this->input->post('company');
                                $message = $email_template->message;

                                $message = str_ireplace(array('[name]',['contact_person'], '[username]', '[password]', '[unique_url]', '[first_name]', '[ticker_url]', '[initiative]','[initiative_name]'), array($name,$name, $username, $password, $unique_url, $first_name, $ticker_url, $initiative_name,$initiative_name), $message);

                                // create ticker
                                //$this->create_ticker($this->input->post('tree_nums'), $code);
								
								if($initiative =='34' || $initiative =='35' || $initiative =='25' || $initiative =='31' || $initiative =='83'){
									$this->create_cfd_ticker($this->input->post('tree_nums'), $code);        //CFD tickers
								}else{
									 $this->create_ticker($this->input->post('tree_nums'), $code);            //GEA tickers
								}
								$this->create_cfd_small_ticker($this->input->post('tree_nums'), $code, 'black');
								
								$this->create_cfd_small_ticker($this->input->post('tree_nums'), $code, 'green');
								
								$this->create_cfd_small_ticker($this->input->post('tree_nums'), $code, 'white');

                                // send email
                                $config = array();
                                $config['to'] = $email;
                                $config['subject'] = str_ireplace(array('[name]', '[contact_person]','[username]', '[password]', '[unique_url]', '[first_name]', '[ticker_url]', '[initiative]','[initiative_name]'), array($name,$name, $username, $password, $unique_url, $first_name, $ticker_url, $initiative_name,$initiative_name), $subject);

                                $config['message'] = $message;
								$config['email_title'] = $get_initiative->sender_email_title;
								if($login_detail_email=='1'){
									 $pct_sendmail = $this->send_mail($config);       //send email only if it is enable
								}
                               
                                /*
                                        if ($pct_sendmail!==1) {
                                        echo $pct_sendmail;
                                        }
                                 */
                                // return to homepage
                                redirect(site_url());
                        }
                } else {
						$this->load->database();
						$query = "SELECT * FROM pct_initiative";
						$row = $this->db->query($query);
						$data3['results'] = $row->result_array();
                        $data = array('title' => 'Add Initiative Partner', 'body' => 'add_initiative_partner');
                        $this->data = array_merge($this->data, $data ,$data3);
                        $this->load->view('template', $this->data);
                }
        }
		  function update_ls_api_credentials($id = 0) {
			  $this->check_user(array('admin', 'intitiative', 'intitiative_client', 'manager','initiative_editor'));
			   //echo "<pre>"; print_r($_POST); die;
			   if(isset($_POST['update_api_save'])){
					$db_name = "greenapp_lightspeed_api_data";
					$db_host = "localhost";
					$db_user = "greenapp_ls_api";
					$db_pass = "2IIOQB{SH=lI";
					mysql_connect($db_host, $db_user, $db_pass) ;
					mysql_select_db($db_name);
					$companyid = mysql_escape_string($this->input->post('ls_api_company_id'));
					$masterid = mysql_escape_string($this->input->post('ls_api_master_id'));
					$username = mysql_escape_string($this->input->post('ls_api_username'));
					$password = mysql_escape_string($this->input->post('ls_api_password'));
					$serveraddress = mysql_escape_string($this->input->post('ls_api_server'));
					$active = mysql_escape_string($this->input->post('api_active')); 
					$productid = mysql_escape_string($this->input->post('ls_api_product_id'));
					$is_suscredit = mysql_escape_string($this->input->post('api_is_suscredit'));
				
					$update_sql = "UPDATE `ls_api_credentials` SET companyid='".$companyid."',masterid='".$masterid."',username='".$username."',password='".$password."',serveraddress='".$serveraddress."',productid='".$productid."',active='".$active."',is_suscredit='".$is_suscredit."' WHERE panid='".$id."'";
					
					 mysql_query($update_sql);
					 redirect(site_url());
			   }
			   
		  }
                
		   function edit_intitiative_partner($id = 0) {
			   //echo "<pre>"; print_r($_SESSION['login']['id']); die;
				$this->load->model('user_model');
                $this->check_user(array('admin', 'intitiative', 'intitiative_client', 'manager','initiative_editor'));
                $user = $_SESSION['login'];
                if ($user['type'] == 'intitiative_client' && $user['id'] != $id && $id) {
                        redirect(site_url('user/edit_intitiative_partner'));
                        return false;
                }
				
				if (isset($_POST['update_partner_password'])) {
						$this->load->library('form_validation');
					  	
						$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check[' . $id . ']');
						$this->form_validation->set_rules('password', 'Password', 'matches[password2]');
                        $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');
						
						$this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
						  if ($this->form_validation->run() === false) {
							 
								$this->load->database();
								$query = "SELECT * FROM pct_initiative";
								$row = $this->db->query($query);
								$data3['results'] = $row->result_array();
								// to connect with the api database
								$db_name = "greenapp_lightspeed_api_data";
								$db_host = "localhost";
								$db_user = "greenapp_ls_api";
								$db_pass = "2IIOQB{SH=lI";
								
								mysql_connect($db_host, $db_user, $db_pass) ;
								mysql_select_db($db_name);
								$data3['results'] = $row->result_array();
								$sql = mysql_query("SELECT * from `ls_api_credentials` WHERE panid='".$id."'");
								$data3['api_result'] = mysql_fetch_row($sql);
								$data = array('title' => 'Edit Initiative Partner', 'body' => 'edit_intitiative_partner');
								$data2 = $this->user_model->load_initiative($id);
								$this->data = array_merge($this->data, $data, $data2,$data3);
								$this->load->view('template', $this->data);
								
						}else{
						
								$file_name='';
								$this->user_model->update_intitiative_partner_password($id);
                                redirect(site_url());
							
						} 

				}elseif (isset($_POST['edit_intitiative_partner'])) {
					
                        // proccess save restaurant
                        $this->load->library('form_validation');
						if ($_SESSION['login']['type'] == 'admin') {
							$this->form_validation->set_rules('initiative', 'Initiative', 'trim|required');
							$this->form_validation->set_rules('send_invoices');
							$this->form_validation->set_rules('free_trial');
						}
						//$this->form_validation->set_rules('logo');
                       // $this->form_validation->set_rules('opportunity_str', 'Opportunity String', 'trim|required');

                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check2[' . $id . ']');
                        //$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check[' . $id . ']');

                        // expend
                        //$this->form_validation->set_rules('email2', 'Email (Solicitor)', 'trim|valid_email');

                        if ($_SESSION['login']['type'] != 'admin') {  // disabled validations for admin
                                $this->form_validation->set_rules('address', 'Address', 'trim|required');
                                $this->form_validation->set_rules('city', 'City', 'trim|required');
                                $this->form_validation->set_rules('state', 'Area', 'trim|required');
                                $this->form_validation->set_rules('post_code', 'Post Code', 'trim|required');
                        } //
                        //$this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                       // $this->form_validation->set_rules('password', 'Password', 'matches[password2]');
                       // $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');

					   
                        if ($_SESSION['login']['type'] == 'admin') {
                                $this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                                //
                                $this->form_validation->set_rules('price', 'Price', 'trim|required|is_numeric');
                                $this->form_validation->set_rules('suscredit_price', 'Sustainable Credit Price', 'trim|required|is_numeric');
                                $this->form_validation->set_rules('currency', 'Currency', 'trim|required|is_natural_no_zero');
                                $this->form_validation->set_rules('tax', 'Tax', 'trim|required|is_numeric');

								$this->form_validation->set_rules('tech_support_email');
								$this->form_validation->set_rules('delay_payment_by');
								$this->form_validation->set_rules('dashboard_user_email');
								$this->form_validation->set_rules('no_of_restaurants');
								$this->form_validation->set_rules('counter_type');
                                //$this->form_validation->set_rules('subject', 'Subject', 'trim|required');
                                $this->form_validation->set_rules('cc_email', 'CC Email', 'trims');
                                // $this->form_validation->set_rules('message', 'Message', 'trim|required');
                                //$this->form_validation->set_rules('certificate_text', 'Text on Certificate', 'trim|required');
                        }
						  if ($_SESSION['login']['type'] == 'intitiative_client') {
							  
							   $this->form_validation->set_rules('twitter');
							   $this->form_validation->set_rules('facebook');
							   $this->form_validation->set_rules('instagram');
							   $this->form_validation->set_rules('pinterest');
							   $this->form_validation->set_rules('linkedin');
					      
						  }

                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        
                        if ($this->form_validation->run() === false) {
								$this->load->database();
								$query = "SELECT * FROM pct_initiative";
								$row = $this->db->query($query);
								$data3['results'] = $row->result_array();
								// to connect with the api database
								$db_name = "greenapp_lightspeed_api_data";
								$db_host = "localhost";
								$db_user = "greenapp_ls_api";
								$db_pass = "2IIOQB{SH=lI";
								
								mysql_connect($db_host, $db_user, $db_pass) ;
								mysql_select_db($db_name);
								$data3['results'] = $row->result_array();
								$sql = mysql_query("SELECT * from `ls_api_credentials` WHERE panid='".$id."'");
								$data3['api_result'] = mysql_fetch_row($sql);
                                $data = array('title' => 'Edit Initiative Partner', 'body' => 'edit_intitiative_partner');
                                $data2 = $this->user_model->load_initiative($id);
                                $this->data = array_merge($this->data, $data, $data2,$data3);
                                $this->load->view('template', $this->data);
                        } else {
							
								$file_name='';
								$image_data = $this->input->post('logo-image');
								if(!empty($image_data)){
									$file_name = date('Ymdhis').'_logo.png';
									$upload_path = './uploads/company/'.$file_name;
									$image_data = $this->input->post('logo-image');
									list($type, $image_data) = explode(';', $image_data);
									list(, $image_data)      = explode(',', $image_data);
									$image_data = base64_decode($image_data);
									file_put_contents($upload_path, $image_data);
								}
								$cert = '';
								// check upload
								if ($_FILES['logo']['size']) {
										$config['file_name'] = date('Ymdhis').'-'.$_FILES['logo']['name'];
										$config['upload_path'] = './libraries/imgs/';
										$config['allowed_types'] = 'jpg|jpeg|png|gif';
										$config['max_size'] = '50000';
										$this->load->library('upload');
										$this->upload->initialize($config);
										
									if (!$this->upload->do_upload('logo')) {
											$data = array('title' => 'Edit Initiative Partner', 'body' => 'edit_intitiative_partner', 'error' => $this->upload->display_errors());
											$this->data = array_merge($this->data, $data);
											$this->load->view('template', $this->data);
											return;
									}else {
											$cert = $this->upload->data();
											$cert = $cert['file_name'];
									}
								}
                                $this->user_model->update_intitiative_partner($id, $file_name, $cert);

                                redirect(site_url());
                        }
                } else {
					    $this->load->database();
                        $id = empty($id) ? $_SESSION['login']['id'] : $id;
                        $this->load->model('user_model');
                        $data2 = $this->user_model->load_initiative($id);
						//echo "<pre>"; print_r($data2); die;
						$query = "SELECT * FROM pct_initiative";
						$row = $this->db->query($query);
						
						// to connect with the api database
						$db_name = "greenapp_lightspeed_api_data";
						$db_host = "localhost";
						$db_user = "greenapp_ls_api";
						$db_pass = "2IIOQB{SH=lI";
						
						mysql_connect($db_host, $db_user, $db_pass) ;
						mysql_select_db($db_name);
						$data3['results'] = $row->result_array();
						$sql = mysql_query("SELECT * from `ls_api_credentials` WHERE panid='".$id."'");
						$data3['api_result'] = mysql_fetch_row($sql);
                        $data = array('title' => 'Edit Initiative Partner', 'body' => 'edit_intitiative_partner');
                        $this->data = array_merge($this->data, $data, $data2 ,$data3 );
                        $this->load->view('template', $this->data);
                }
        } 
		
		 function change_client_password($id ="") {
			 if($id==""){
				  $user_id  = $_SESSION['login']['id'];
			 }else{
				  $user_id  = $id;
			 }
			   $this->load->model('user_model');
			   $this->load->library('form_validation');
			    if (isset($_POST['password'])) {
					
					$this->form_validation->set_rules('password', 'Password', 'matches[password2]');
					$this->form_validation->set_message('matches', 'Password field and Password conf field do not match');
					$this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
					 if ($this->form_validation->run() === false) {
								$this->load->database();
                                 $data = array('title' => 'Change Password', 'body' => 'change_initiative_client_pass');
								$this->data = array_merge($this->data, $data);
								$this->load->view('template', $this->data);
                        } else {
							  $this->user_model->update_clients_password($user_id);
							 $data = array('title' => 'Green Earth Appeal Partner Update Password', 'body' => 'update_password_confirmation');
							 $this->load->view('template', $data);
						}
				}else{
						$this->load->database();
                        $data = array('title' => 'Change Password', 'body' => 'change_initiative_client_pass');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
				}
			 
		 }
		 	 function edit_intitiative_partners($id = 0) {
			  // echo "<pre>"; print_r($_SESSION['login']); die;
                $this->check_user(array('admin', 'intitiative', 'intitiative_client', 'manager'));
                $user = $_SESSION['login'];
                if ($user['type'] == 'intitiative_client' && $user['id'] != $id && $id) {
                        redirect(site_url('user/edit_intitiative_partner_clients'));
                        return false;
                }
                if (isset($_POST['edit_intitiative_partner_clients'])) {
				
                        // proccess save restaurant
                        $this->load->library('form_validation');
						//echo "<pre>"; print_r($_POST); die;
						if ($_POST['company_info']!="") {
							
							$this->form_validation->set_rules('address', 'Address', 'trim|required');
							$this->form_validation->set_rules('city', 'City', 'trim|required');
							$this->form_validation->set_rules('state', 'Area', 'trim|required');
							$this->form_validation->set_rules('post_code', 'Post Code', 'trim|required');
						}
							if ($_POST['user_info']!="") {
								$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check2[' . $id . ']');
								 $this->form_validation->set_rules('first_name');
								 $this->form_validation->set_rules('last_name');
							}

							if ($_POST['social_info']!="") {
								
								   $this->form_validation->set_rules('twitter');
								   $this->form_validation->set_rules('facebook');
								   $this->form_validation->set_rules('instagram');
								   $this->form_validation->set_rules('pinterest');
								   $this->form_validation->set_rules('linkedin');
						  }

                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        $this->load->model('user_model');
                        if ($this->form_validation->run() === false) {
								$this->load->database();
								$query = "SELECT * FROM pct_initiative";
								$row = $this->db->query($query);
								$data3['results'] = $row->result_array();
                                $data = array('title' => 'Edit Initiative Partner', 'body' => 'edit_intitiative_partners');
                                $data2 = $this->user_model->load_initiative($id);
                                $this->data = array_merge($this->data, $data, $data2,$data3);
                                $this->load->view('template', $this->data);
                        } else {
							
                                $logo = '';
                                // check upload
                                if ($_FILES['logo']['size']) {
										$config['file_name'] = date('Ymdhis').'-'.$_FILES['logo']['name'];
                                        $config['upload_path'] = './uploads/company/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
										
                                        if (!$this->upload->do_upload('logo')) {
                                                $data = array('title' => 'Edit Initiative Partner', 'body' => 'edit_intitiative_partners', 'error' => $this->upload->display_errors());
                                                $data2 = $this->user_model->load_initiative($id);
                                                $this->data = array_merge($this->data, $data, $data2);
                                                $this->load->view('template', $this->data);
                                                return;
                                        } else {
                                                $logo = $this->upload->data();
                                                $logo = $logo['file_name'];
                                        }
                                }
								

                                $this->user_model->update_intitiative_partner_clients($id, $logo);

                                redirect(site_url());
                        }
                } else {
					
					    $this->load->database();
                        $id = empty($id) ? $_SESSION['login']['id'] : $id;
                        $this->load->model('user_model');
                        $data2 = $this->user_model->load_initiative($id);
						$query = "SELECT * FROM pct_initiative";
						$row = $this->db->query($query);
						$data3['results'] = $row->result_array();
                        $data = array('title' => 'Edit Initiative Partner', 'body' => 'edit_intitiative_partners');
                        $this->data = array_merge($this->data, $data, $data2 ,$data3);
                        $this->load->view('template', $this->data);
                }
        }
		/* list CFD and GEA counters */
		function all_counters() {
			
				$data = array('title' => 'Counters', 'body' => 'list_counters');
				$this->data = $data;
				$this->load->view('template', $this->data);

		}
		/**
		 *  =========================================================================================================
		 * Company Initiative  part
		 */
		function create_invoice_report() {
			    $this->load->model('user_model');
				$this->check_user(array("admin", "manager","initiative_editor"));
				$data3['invoices'] = array();
				
				if (isset($_POST['create_invoice_report'])) {
						// echo "<pre>"; print_r($_POST); die;
						 $this->load->library('form_validation');
						 $this->form_validation->set_rules('to_date');
						 $this->form_validation->set_rules('date_from');
				
						$status = $this->input->post('status');
						//$verify = $this->input->post('verify');
						$date_from = $this->input->post('date_from');
						$to_date = $this->input->post('to_date');
						$initiative = $this->input->post('initiative');
						$data3['invoices']= $this->user_model->get_invoices_reports($status, $date_from, $to_date, $initiative);
						
						foreach($data3['invoices'] as $key=>$invoice){
							
							 $data3['invoices'][$key]->company = $this->user_model->get_company_name($invoice->user_id);
							
						}
						//echo "<pre>"; print_r($data3); die;
						$query = "SELECT * FROM pct_initiative";
						$row = $this->db->query($query);
						$data3['results'] = $row->result_array();
						
						
						$data = array('title' => 'Create Report', 'body' => 'create_invoice_report');
						$this->data = array_merge($this->data, $data,$data3);
						$this->load->view('template', $this->data);
                } else {
					
				    $this->load->database();
					$query = "SELECT * FROM pct_initiative";
					$row = $this->db->query($query);
					$data3['results'] = $row->result_array();
					$data = array('title' => 'Create Report', 'body' => 'create_invoice_report');
					$this->data = array_merge($this->data, $data,$data3);
					$this->load->view('template', $this->data);
					
                }
				
		}
		function stage_confirmed(){
			header("Access-Control-Allow-Origin: *");
			$this->load->database();
			$this->load->model('user_model');
		    $stage= $this->input->post('stage');
			$user_id= $this->input->post('user_id');
				$sql = "SELECT * FROM pct_users WHERE id='$user_id'";
				$query = $this->db->query($sql);
				$query1 = $query->row();
				
			if($stage==1){
				 $query = "UPDATE pct_grant_user_stage1 SET status='1' WHERE user_id=$user_id";
				 $this->db->query($query);
				 $user_id = $_SESSION['login']['id'];
				 //$config['subject'] = 'Green Earth Appeal – Project Overview Confirmed';
				 $config['subject'] = 'Tree Planting Grant Application Success';
				 
				$message.='Dear ' .ucfirst($query1->firstname).","."<br><br>";
				$message.='Thank you for submitting the Overview of your Project. I am pleased to inform you that we have reviewed your project and have selected you as a potential partnership.' . "<br><br>";
				$message.='We now require some further Project Details to allow us to progress your application.' . "<br><br>";
				$message.= "Please <a href='https://panacea.greenearthappeal.org'>click here to login and continue</a> (  https://panacea.greenearthappeal.org )".'<br><br>';
				$message.= 'User: '.$query1->email.'<br><br>';
				$message.= 'Password: chosen when you initially registered'.'<br><br>';
				$message.= "Once logged in, please provide the information requested on the Project Details tab.".'<br><br>';
				$message.= "Kind Regards,".'<br><br>';
				$message.="Partnerships Department".'<br><br>';
				$message.="Green Earth Appeal".'<br><br>';
				 
				 $config['message'] = $message;
				 $config['to'] = $query1->email;
				 $config['email_title'] = 'Partnerships Department – Green Earth Appeal';
				 $this->send_mail($config);
				
			}
				if($stage==2){
				 $query = "UPDATE pct_grant_user_stage2 SET stage2_status='1' WHERE user_id=$user_id";
				 $this->db->query($query);
				 $user_id = $_SESSION['login']['id'];
				 $config['subject'] = 'Green Earth Appeal – Project Details Confirmed';
				 
				 $message.='Dear ' .ucfirst($query1->firstname).","."<br><br>";
				 $message.='Thank you for submitting the Details of your Project.' . "<br><br>";
				 $message.='I am pleased to inform you that we have reviewed the details of your project and have selected it for final review.' . "<br><br>";
				 $message.='To complete your application and submit it for final review,please <a href="https://panacea.greenearthappeal.org">click here</a> ( https://panacea.greenearthappeal.org ) to login using '.$query1->email.' and your chosen password login and provide as much supporting information as possible.' . "<br><br>";
				 $message.= "Kind Regards,".'<br><br>';
				 $message.="Partnerships Department".'<br><br>';
				 $message.="Green Earth Appeal".'<br><br>';
				 
				$message.= "Please <a href='https://panacea.greenearthappeal.org'>click here</a> (  https://panacea.greenearthappeal.org ) to login and continue".'<br><br>';
				
				 $config['message'] = $message;
				 $config['to'] = $query1->email;
				 $config['email_title'] = 'Partnerships Department – Green Earth Appeal';
				 $this->send_mail($config);
				
			}
			if($stage==3){
				 $query = "UPDATE pct_grant_user_stage3 SET stage3_status='1' WHERE user_id=$user_id";
				 $this->db->query($query);
				 $user_id = $_SESSION['login']['id'];
				 $config['subject'] = 'Green Earth Appeal – Supporting Information Confirmed';
				 
				 $message.='Dear ' .ucfirst($query1->firstname).","."<br><br>";
				 $message.='Thank you for submitting the supporting information for your project.' . "<br><br>";
				 $message.='I am pleased to inform you that we have reviewed all the information provided and it has been selected as a potential candidate for funding.' . "<br><br>";
				 //$message.='You may review the information submitted at any time by logging into your account at https://panacea.greenearthappeal.org using your login details.' . "<br><br>";
				 $message.='You may review the information submitted at any time by <a href="https://panacea.greenearthappeal.org">click here</a> ( https://panacea.greenearthappeal.org ) to login using your login details.' . "<br><br>";
				 $message.='We will be in touch shortly once our funding board has reviewed and verified your project.' . "<br><br>";
				 $message.= "Kind Regards,".'<br><br>';
				 $message.="Partnerships Department".'<br><br>';
				 $message.="Green Earth Appeal".'<br><br>';
				 
				 $config['message'] = $message;
				 $config['to'] = $query1->email;
				 $config['email_title'] = 'Partnerships Department – Green Earth Appeal';
				 $this->send_mail($config);
				
			}
			return $user_id;
		}
		 function firsttime_grant_user_login(){
			$this->load->database();
			$this->load->model('user_model');
			$first_time = $_POST['first_time'];
			$query = "UPDATE pct_users SET first_time_grant_user='1' WHERE id='".$_SESSION['login']['id']."'";
			$this->db->query($query);
		} 
		function stage_confirmed_email(){
				 $config['subject'] = $_POST['subject'];
				// $config['message'] = nl2br($_POST['message']);
				 $message.= nl2br($_POST['message']).'<br><br>';
				 $message.= "Kind Regards,".'<br><br>';
				 $message.="Partnerships Department".'<br><br>';
				 $config['message'] = $message;
				 $config['to'] =  $_POST['email'];
				 $config['email_title'] = 'Partnerships Department – Green Earth Appeal';
				 $this->send_mail($config);
		}
		function stages_confirmation($stage="",$user_id=""){
			redirect(site_url('user/confirm_stages/'.$user_id));
			$this->load->database();
			$this->load->model('user_model');
			if($stage==1){
				$data1 = $this->user_model->get_all_stage1_user_data($user_id);
				$data = array('title' => 'Stage 1 Confirmation', 'body' => 'list_stage1_confirmation');
				$this->data = array_merge($this->data, $data,$data1);
				$this->load->view('template', $this->data);
			}
			if($stage==2){
				$data1 = $this->user_model->get_all_stage2_user_data($user_id);
				$data = array('title' => 'Stage 2 Confirmation', 'body' => 'list_stage2_confirmation');
				$this->data = array_merge($this->data, $data,$data1);
				$this->load->view('template', $this->data);
			}
			if($stage==3){
				
				$data1 = $this->user_model->get_all_stage1_user_data($user_id);
				$data2 = $this->user_model->get_all_stage2_user_data($user_id);
				$data3 = $this->user_model->get_all_stage3_user_data($user_id,$data1,$data2);
				$data = array('title' => 'Stage 3 Confirmation', 'body' => 'list_stage3_confirmation');
				$this->data = array_merge($this->data, $data,$data3);
				$this->load->view('template', $this->data);
				
					
			}
			
		}
		function submit_for_approval(){
			header("Access-Control-Allow-Origin: *");
			$this->load->database();
			$this->load->model('user_model');
		     $stage = $this->input->post('stage');
		     $user_id = $this->input->post('user_id');
			if($stage=='1'){
					$config['subject'] = 'Project Overview Confirmation Email';
					$stage_1_link = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_stages/'.$user_id;
					$message.="<p>Please <a href='$stage_1_link'>click here</a> for stage 1 confirmation.</p>"."<br><br>";
					$message.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
					$config['message'] = $message;
					//$config['to'] ='marvin@greenearthappeal.org';
					$config['cc'] ='laksmi.bruce@greenearthappeal.org';
					$config['to'] = "marvin@greenearthappeal.org"; 
					$config['email_title'] = 'Partnerships Department – Green Earth Appeal';
					$this->send_grant_email($config);
				
			}
		}
	  function unlock_stages_for_resubmission(){
			header("Access-Control-Allow-Origin: *");
			$this->load->database();
			$this->load->model('user_model');
		      $stage = $this->input->post('stage');
		      $user_id = $this->input->post('user_id');

			if($stage=='1'){
					$query = "UPDATE pct_grant_user_stage1 SET status='2' WHERE user_id=$user_id";
					return $this->db->query($query);
			}elseif($stage=='2'){
				$query = "UPDATE pct_grant_user_stage2 SET stage2_status='2' WHERE user_id=$user_id";
				return $this->db->query($query);
			}elseif($stage=='3'){
				$query = "UPDATE pct_grant_user_stage3 SET stage3_status='2' WHERE user_id=$user_id";
				return $this->db->query($query);
			}
		}
		 function lock_stages_for_resubmission(){
			header("Access-Control-Allow-Origin: *");
			$this->load->database();
			$this->load->model('user_model');
		      $stage = $this->input->post('stage');
		      $user_id = $this->input->post('user_id');

			if($stage=='1'){
					$query = "UPDATE pct_grant_user_stage1 SET status='0' WHERE user_id=$user_id";
					return $this->db->query($query);
			}elseif($stage=='2'){
				$query = "UPDATE pct_grant_user_stage2 SET stage2_status='0' WHERE user_id=$user_id";
				return $this->db->query($query);
			}elseif($stage=='3'){
				$query = "UPDATE pct_grant_user_stage3 SET stage3_status='0' WHERE user_id=$user_id";
				return $this->db->query($query);
			}
		}
		
		
		 function edit_grant_user($id = 0) {
		
				$this->load->database();
				$this->load->model('user_model');
				//echo "<pre>"; print_r($_SESSION['login']);
                $this->check_user(array('admin', 'intitiative', 'intitiative_client','grant_user','manager','grant_admin'));
                $user = $_SESSION['login'];
                $user_id = $id;
				//echo "<pre>"; print_r($_SESSION['login']);
				 if($id==0){
							$user_id = $_SESSION['login']['id'];
					   }else{
						   $user_id = $id;
					   }  
                if ($user['type'] == 'grant_user' && $user['id'] != $id && $id) {
					  redirect(site_url('user/edit_intitiative_partner_clients'));
                        return false;
                }
				 if (isset($_POST['edit_stage_1'])) {
					 
				     $sql = "SELECT * FROM  pct_grant_user_stage1 WHERE user_id='$user_id'";
					$query = $this->db->query($sql);
					$query1 = $query->result_array();
					$message ="";
					$message1 ="";
					 if(empty($query1)){
						$data = $this->user_model->add_grant_user_stage1();
						if($data!=''){
							$user_id = $_SESSION['login']['id'];
							$config['subject'] = 'Project Overview Confirmation Email';
							$stage_1_link = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_stages/'.$user_id;
							$message.="<p>Please <a href='$stage_1_link'>click here</a> for stage 1 confirmation.</p>"."<br><br>";
							$message.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
							$config['message'] = $message;
							//$config['to'] ='marvin@greenearthappeal.org';
							$config['cc'] ='laksmi.bruce@greenearthappeal.org';
							$config['to'] = "marvin@greenearthappeal.org"; 
							$config['email_title'] = 'Partnerships Department – Green Earth Appeal';
							$this->send_grant_email($config);
							
							//Send email to grantadmin@greenearthappeal.org
							
							$config1['subject'] = 'Project Overview Confirmation Email';
							$stage_1_link1 = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_grant_stage/'.$user_id;
							$message1.="<p>Please <a href='$stage_1_link1'>click here</a> for stage 1 confirmation.</p>"."<br><br>";
							$message1.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
							$config1['message'] = $message1;
							$config1['to'] = "grantadmin@greenearthappeal.org"; 
							$config1['email_title'] = 'Partnerships Department – Green Earth Appeal';
							$this->send_grant_email($config1);
							
							$this->data['success'] = true;
						} 
					 }else{
							 if ($_SESSION['login']['type'] == 'grant_user') {
									$config['subject'] = 'Project Overview Confirmation Email';
									$stage_1_link = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_stages/'.$user_id;
									$message.="<p>Please <a href='$stage_1_link'>click here</a> for stage 1 confirmation.</p>"."<br><br>";
									$message.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
									$config['message'] = $message;
									//$config['to'] ='marvin@greenearthappeal.org';
									$config['cc'] ='laksmi.bruce@greenearthappeal.org';
									$config['to'] = "marvin@greenearthappeal.org"; 
									$config['email_title'] = 'Partnerships Department – Green Earth Appeal';
									$this->send_grant_email($config);

									//Send email to grantadmin@greenearthappeal.org
									$config1['subject'] = 'Project Overview Confirmation Email';
									$stage_1_link1 = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_grant_stage/'.$user_id;
									$message1.="<p>Please <a href='$stage_1_link1'>click here</a> for stage 1 confirmation.</p>"."<br><br>";
									$message1.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
									$config1['message'] = $message1;
									$config1['to'] = "grantadmin@greenearthappeal.org"; 
									$config1['email_title'] = 'Partnerships Department – Green Earth Appeal';
									$this->send_grant_email($config1);									

									$this->data['success'] = true;
							 }
							$data = $this->user_model->update_grant_user_stage1($user_id);
					 }
						$data1 = $this->user_model->get_all_stage1_user_data($user_id);
						$data2 = $this->user_model->get_all_stage2_user_data($user_id);
						$data3 = $this->user_model->get_all_stage3_user_data($user_id,$data1,$data2);
						
					    $data = array('title' => 'Project Overview', 'body' => 'edit_organisation_user','user_id'=>$user_id);
                        $this->data = array_merge($this->data, $data,$data1,$data2,$data3);
                        $this->load->view('template', $this->data);
						
				 }elseif(isset($_POST['edit_stage_2'])) {
				 
					 $sql = "SELECT * FROM  pct_grant_user_stage2 WHERE user_id='$user_id'";
					$query = $this->db->query($sql);
					$query1 = $query->result_array();
					// echo "<pre>"; print_r($query1); die;
					 if(empty($query1)){
						$data = $this->user_model->add_grant_user_stage2();
						 if($data!=''){
							 
							$user_id = $_SESSION['login']['id'];
							$config['subject'] = 'Project Details Confirmation Email';
							$stage_2_link = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_stages/'.$user_id;
							$message.="<p>Please <a href='$stage_2_link'>click here</a> for stage 2 confirmation.</p>"."<br><br>";
							$message.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
							$config['message'] = $message;
							//$config['to'] ='marvin@greenearthappeal.org';
							$config['cc'] ='laksmi.bruce@greenearthappeal.org';
							$config['to'] = "marvin@greenearthappeal.org"; 
							$config['email_title'] = 'Partnerships Department – Green Earth Appeal';
							$this->send_grant_email($config);
							
							//Send email to grantadmin@greenearthappeal.org
							
							$config1['subject'] = 'Project Details Confirmation Email';
							$stage_2_link1 = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_grant_stage/'.$user_id;
							$message1.="<p>Please <a href='$stage_2_link1'>click here</a> for stage 2 confirmation.</p>"."<br><br>";
							$message1.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
							$config1['message'] = $message1;
							$config1['to'] = "grantadmin@greenearthappeal.org"; 
							$config1['email_title'] = 'Partnerships Department – Green Earth Appeal';
							$this->send_grant_email($config1);
							$this->data['success2'] = true;
						} 
						 
					 }else{
						  if ($_SESSION['login']['type'] == 'grant_user') {
								$config['subject'] = 'Project Details Confirmation Email';
								$stage_2_link = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_stages/'.$user_id;
								$message.="<p>Please <a href='$stage_2_link'>click here</a> for stage 2 confirmation.</p>"."<br><br>";
								$message.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
								$config['message'] = $message;
								//$config['to'] ='marvin@greenearthappeal.org';
								$config['cc'] ='laksmi.bruce@greenearthappeal.org';
								$config['to'] = "marvin@greenearthappeal.org"; 
								$config['email_title'] = 'Partnerships Department – Green Earth Appeal';
								$this->send_grant_email($config);
								
								//Send email to grantadmin@greenearthappeal.org
								$config1['subject'] = 'Project Details Confirmation Email';
								$stage_2_link1 = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_grant_stage/'.$user_id;
								$message1.="<p>Please <a href='$stage_2_link1'>click here</a> for stage 2 confirmation.</p>"."<br><br>";
								$message1.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
								$config1['message'] = $message1;
								$config1['to'] = "grantadmin@greenearthappeal.org"; 
								$config1['email_title'] = 'Partnerships Department – Green Earth Appeal';
								$this->send_grant_email($config1);
								
								$this->data['success2'] = true;
						  }
						 $data = $this->user_model->update_grant_user_stage2($user_id);
					 }
						$data1 = $this->user_model->get_all_stage1_user_data($user_id);
						$data2 = $this->user_model->get_all_stage2_user_data($user_id);
						$data3 = $this->user_model->get_all_stage3_user_data($user_id,$data1,$data2);
						
					    $data = array('title' => 'Project Overview', 'body' => 'edit_organisation_user');
                        $this->data = array_merge($this->data, $data,$data1,$data2,$data3);
                        $this->load->view('template', $this->data);
				 
				 }elseif(isset($_POST['edit_stage_3'])) {
					// echo "<pre>"; print_r($_POST);die;
				 	$sql = "SELECT * FROM  pct_grant_user_stage3 WHERE user_id='$user_id'";
					$query = $this->db->query($sql);
					$query1 = $query->result_array();
					 if(empty($query1)){
						$data = $this->user_model->add_grant_user_stage3();
					 	 if($data!=''){
							 
							$user_id = $_SESSION['login']['id'];
							$config['subject'] = 'Supporting Information Confirmation Email';
							$stage_3_link = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_stages/'.$user_id;
							$message.="<p>Please <a href='$stage_3_link'>click here</a> for stage 3 confirmation.</p>"."<br><br>";
							$message.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
							$config['message'] = $message;
							//$config['to'] ='marvin@greenearthappeal.org';
							 $config['cc'] ='laksmi.bruce@greenearthappeal.org';
							$config['to'] = "marvin@greenearthappeal.org"; 
							$config['email_title'] = 'Partnerships Department – Green Earth Appeal';
							$this->send_grant_email($config);
							
							//Send email to grantadmin@greenearthappeal.org
							$config1['subject'] = 'Supporting Information Confirmation Email';
							$stage_3_link1 = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_grant_stage/'.$user_id;
							$message1.="<p>Please <a href='$stage_3_link1'>click here</a> for stage 3 confirmation.</p>"."<br><br>";
							$message1.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
							$config1['message'] = $message1;
							$config1['to'] = "grantadmin@greenearthappeal.org"; 
							$config1['email_title'] = 'Partnerships Department – Green Earth Appeal';
							$this->send_grant_email($config1);
							
							//$this->data['success'] = true;
						} 
						 
					 }else{
						 if ($_SESSION['login']['type'] == 'grant_user') {
							$config['subject'] = 'Supporting Information Confirmation Email';
							$stage_3_link = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_stages/'.$user_id;
							$message.="<p>Please <a href='$stage_3_link'>click here</a> for stage 3 confirmation.</p>"."<br><br>";
							$message.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
							$config['message'] = $message;
							//$config['to'] ='marvin@greenearthappeal.org';
							 $config['cc'] ='laksmi.bruce@greenearthappeal.org';
							$config['to'] = "marvin@greenearthappeal.org"; 
							$config['email_title'] = 'Partnerships Department – Green Earth Appeal';
							$this->send_grant_email($config);
							
							//Send email to grantadmin@greenearthappeal.org
							$config1['subject'] = 'Supporting Information Confirmation Email';
							$stage_3_link1 = 'https://www.greenearthappeal.org/panacea/index.php/user/confirm_grant_stage/'.$user_id;
							$message1.="<p>Please <a href='$stage_3_link1'>click here</a> for stage 3 confirmation.</p>"."<br><br>";
							$message1.='User Email: ' .$_SESSION['login']['email']. "<br><br>";
							$config1['message'] = $message1;
							$config1['to'] = "grantadmin@greenearthappeal.org"; 
							$config1['email_title'] = 'Partnerships Department – Green Earth Appeal';
							$this->send_grant_email($config1);
							//$this->data['success'] = true;
						 }
						 $data = $this->user_model->update_grant_user_stage3($user_id);
					 }
						$data1 = $this->user_model->get_all_stage1_user_data($user_id);
						$data2 = $this->user_model->get_all_stage2_user_data($user_id);
						$data3 = $this->user_model->get_all_stage3_user_data($user_id,$data1,$data2);
						
					    $data = array('title' => 'Project Overview', 'body' => 'edit_organisation_user');
                        $this->data = array_merge($this->data, $data,$data1,$data2,$data3);
                        $this->load->view('template', $this->data);
				 
				 }else{
						
						$data1 = $this->user_model->get_all_stage1_user_data($user_id);
						$data2 = $this->user_model->get_all_stage2_user_data($user_id);
						$data3 = $this->user_model->get_all_stage3_user_data($user_id,$data1,$data2);
						
					    $data = array('title' => 'Project Overview', 'body' => 'edit_organisation_user','user_id'=>$user_id);
                        $this->data = array_merge($this->data, $data,$data1,$data2,$data3);
						//echo "<pre>"; print_r( $this->data ); die; 
                        $this->load->view('template', $this->data);
				 }
				
		 }
		 
		 
		function remove_grant_community_photo(){
			    $this->load->database();
                $this->load->model('user_model');

			 if(!empty($_POST)){
			   $image_name  = $_POST['image_name'];
			   $id  	= $_POST['id'];
			   $delete_type  	= $_POST['delete_type'];
				if($delete_type=='community'){
					$this->db->query("DELETE FROM pct_stage3_community_photos WHERE id ='".$id."'");
				}
				if($delete_type=='location'){
					$this->db->query("DELETE FROM pct_stage3_location_photos WHERE id ='".$id."'");
				}
			 }
			 
		 }
		function remove_grant_headquarters_photo(){
			    $this->load->database();
                $this->load->model('user_model');

			 if(!empty($_POST)){
			   $image_name  = $_POST['image_name'];
			   $id  	= $_POST['id'];
			   $delete_type  	= $_POST['delete_type'];
			   
				if($delete_type=='headquarters'){
					$this->db->query("UPDATE pct_grant_user_stage3 SET org_headquarter_photo='' WHERE org_headquarter_photo ='".$image_name."'");
				}
				if($delete_type=='additional'){
					$this->db->query("UPDATE pct_grant_user_stage3 SET additional_documentation='' WHERE additional_documentation ='".$image_name."'");
				}
               
			 }
			 
		 }
		 function remove_grant_upload_photo(){
			    $this->load->database();
                $this->load->model('user_model');

			 if(!empty($_POST)){
			   $image_name  = $_POST['image_name'];
			   $person_id  	= $_POST['person_id'];
			  
               $this->db->query("DELETE FROM pct_stage3_person_photos WHERE person_id ='".$person_id."' AND person_photo='".$image_name."'");
			 }
			 
		 }
			 
		 
		 function add_lightspeed_api_partner_credentials(){
			 
				// echo "<pre>"; print_r($_POST); die("teee");
				 //database connection for the lightspeed_api
				$db_name = "greenapp_lightspeed_api_data";
				$db_host = "localhost";
				$db_user = "greenapp_ls_api";
				$db_pass = "2IIOQB{SH=lI";
				
				mysql_connect($db_host, $db_user, $db_pass) or die('Can not connect to db');
				mysql_select_db($db_name) or die(mysql_error()); 
				
				$compname = mysql_real_escape_string($_POST['api_company_name']);
				$companyid = mysql_real_escape_string($_POST['api_company_id']);
				$masterid = mysql_real_escape_string($_POST['master_id']);
				$username = mysql_real_escape_string($_POST['api_username']);
				$password = mysql_real_escape_string($_POST['api_password']);
				$serveraddress = mysql_real_escape_string($_POST['api_server']);
				$panid = mysql_real_escape_string($_POST['pan_id']);
				$productid = mysql_real_escape_string($_POST['api_product_id']);
				$active = '0';
				  $sql = "Insert into `ls_api_credentials` (compname, companyid, masterid, username, password, serveraddress, panid, productid, active) VALUES ('".$compname."','".$companyid."','".$masterid."','".$username."','".$password."','".$serveraddress."','".$panid."','".$productid."','".$active."')";
				  $result = mysql_query($sql);
		 }
		 function edit_intitiative_partner_clients($id = 0) {
			  // echo "<pre>"; print_r($_SESSION['login']); die;
                $this->check_user(array('admin', 'intitiative', 'intitiative_client', 'manager'));
                $user = $_SESSION['login'];
                if ($user['type'] == 'intitiative_client' && $user['id'] != $id && $id) {
                        redirect(site_url('user/edit_intitiative_partner_clients'));
                        return false;
                }
                if (isset($_POST['edit_intitiative_partner_clients'])) {
				
                        // proccess save restaurant
                        $this->load->library('form_validation');
						//echo "<pre>"; print_r($_POST); die;
						if ($_POST['company_info']!="") {
						
							$this->form_validation->set_rules('address', 'Address', 'trim|required');
							$this->form_validation->set_rules('city', 'City', 'trim|required');
							$this->form_validation->set_rules('state', 'Area', 'trim|required');
							$this->form_validation->set_rules('post_code', 'Post Code', 'trim|required');
						}
							if ($_POST['user_info']!="") {
								$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check2[' . $id . ']');
								 $this->form_validation->set_rules('first_name');
								 $this->form_validation->set_rules('last_name');
							}

							if ($_POST['social_info']!="") {
								
								   $this->form_validation->set_rules('twitter');
								   $this->form_validation->set_rules('facebook');
								   $this->form_validation->set_rules('instagram');
								   $this->form_validation->set_rules('pinterest');
								   $this->form_validation->set_rules('linkedin');
								   $this->form_validation->set_rules('instagram_token');
						  }
						  if ($_POST['logo_info']!="") {
								   $this->form_validation->set_rules('logo_info');
						  }

                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        $this->load->model('user_model');
                        if ($this->form_validation->run() === false) {
								$this->load->database();
								$query = "SELECT * FROM pct_initiative";
								$row = $this->db->query($query);
								$data3['results'] = $row->result_array();
                                $data = array('title' => 'Edit Initiative Partner', 'body' => 'edit_intitiative_partner_clients');
                                $data2 = $this->user_model->load_initiative($id);
                                $this->data = array_merge($this->data, $data, $data2,$data3);
                                $this->load->view('template', $this->data);
                        } else {
							   
                                $logo = '';
							/* 	  // check upload
                                if ($_FILES['client_logo']['size']) {
										$config['file_name'] = date('Ymdhis').'-'.$_FILES['client_logo']['name'];
                                        $config['upload_path'] = './uploads/sublevel_initiative/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
										
                                        if (!$this->upload->do_upload('client_logo')) {
                                                $data = array('title' => 'Edit Initiative Partner', 'body' => 'edit_intitiative_partner_clients', 'error' => $this->upload->display_errors());
                                                $data2 = $this->user_model->load_initiative($id);
                                                $this->data = array_merge($this->data, $data, $data2);
                                                $this->load->view('template', $this->data);
                                                return;
                                        } else {
                                                $logo = $this->upload->data();
                                                $logo = $logo['file_name'];
                                        }
                                }  */ 
								 $file_name = '';
								$image_data = $this->input->post('logo-image');
								if(!empty($image_data)){
									$file_name = date('Ymdhis').'_logo.png';
									$upload_path = './uploads/company/'.$file_name;
									$image_data = $this->input->post('logo-image');
									list($type, $image_data) = explode(';', $image_data);
									list(, $image_data)      = explode(',', $image_data);
									$image_data = base64_decode($image_data);
									file_put_contents($upload_path, $image_data);
								}

                                $this->user_model->update_intitiative_partner_clients($id, $file_name);

                                redirect(site_url());
                        }
                } else {
					
					    $this->load->database();
                        $id = empty($id) ? $_SESSION['login']['id'] : $id;
                        $this->load->model('user_model');
                        $data2 = $this->user_model->load_initiative($id);
						$query = "SELECT * FROM pct_initiative";
						$row = $this->db->query($query);
						$data3['results'] = $row->result_array();
                        $data = array('title' => 'Edit Initiative Partner', 'body' => 'edit_intitiative_partner_clients');
                        $this->data = array_merge($this->data, $data, $data2 ,$data3);
                        $this->load->view('template', $this->data);
                }
        }
		function delete_intitiative_partner($id) {
                $this->check_user(array("admin", "manager","initiative_editor"));
                $this->load->database();
                $this->load->model('user_model');
				
                // load clients
                $id_str = '';
                $partner_id = $id;
                $clients = $this->user_model->load_initiative_clients($id);
				//echo "<pre>"; print_r( $clients ); die;
                foreach ($clients AS $v) {
                        $id_str .= $v->user_id . ',';
                }
                $id_str .= $id;
				
                $this->db->query("DELETE FROM pct_users WHERE id IN ($id_str)");
                $this->db->query("DELETE FROM pct_initiative_partner WHERE user_id IN ($id_str)");
                $this->db->query("DELETE FROM pct_referer WHERE user_id IN ($id_str)");
                $this->db->query("DELETE FROM pct_invoices WHERE user_id IN ($id_str)");
				$this->db->query("DELETE FROM pct_initiative_monthly_invoice_records WHERE partner_id=$partner_id");    //to delete monthly invoices
				$this->create_ticker_certificates($initiative = NULL);       //to update initiative tickers
                redirect(site_url());
        }
		 
		function delete_intitiative_partner_clients($id) {
                $this->check_user(array("admin", "intitiative"));
                $this->load->database();
                $this->load->model('user_model');
				$res = $this->db->query("SELECT *,(SUM(tree_nums) + SUM(free_trees)) as 'total_trees' FROM pct_initiative_partner where user_id=$id")->row();
				$invoice_trees = $this->db->query("SELECT trees FROM pct_invoices where figure_history_key='$res->figure_history_key'")->row();
				$trees_left = $invoice_trees->trees-$res->total_trees;
				if($trees_left>0){
					$this->db->query("UPDATE pct_invoices SET trees=$trees_left WHERE figure_history_key='$res->figure_history_key'");
				}
				//$this->db->query("DELETE FROM pct_invoices WHERE user_id=$id");
                $this->db->query("DELETE FROM pct_users WHERE id=$id");
                $this->db->query("DELETE FROM pct_initiative_partner WHERE user_id=$id");
                $this->db->query("DELETE FROM pct_referer WHERE user_id=$id");
                
			   //echo $this->db->last_query();die;
                //redirect(site_url('user/intitiative_clients_list'));
                redirect(site_url());
        }
        function preview_template() {
                $this->load->database();
                $this->load->model('user_model');
                $id = $this->input->post('tempid');
                $name = $this->input->post('company');
                $email = $this->input->post('email');
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $first_name = $this->input->post('first_name');
                $code = $this->input->post('code');
                $ticker_url = base_url() . 'tickers/ticker_' . $code . '.png';

                $email_template = $this->user_model->load_email_template($id);
                $message = $email_template->message;
                $message = str_ireplace(array('[name]', '[username]', '[password]', '[unique_url]', '[first_name]', '[ticker_url]'), array($name, $username, $password, '---not generated--', $first_name, $ticker_url), $message);
                echo $message;
                //$this->load->view('template', $this->email_template);
        }
	
        function edit_bulk_partner($id = 0) {
                $this->check_user(array('admin', 'bulk_partner', 'client', 'manager'));
                $user = $_SESSION['login'];
                if ($user['type'] == 'client' && $user['id'] != $id && $id) {
                        redirect(site_url('user/edit_bulk_partner'));
                        return false;
                }
                if (isset($_POST['edit_bulk_partner'])) {
                        // proccess save restaurant
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('initiative', 'Initiative', 'trim|required');
                        $this->form_validation->set_rules('opportunity_str', 'Opportunity String', 'trim|required');

                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check2[' . $id . ']');
                        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check[' . $id . ']');

                        // expend
                        //$this->form_validation->set_rules('email2', 'Email (Solicitor)', 'trim|valid_email');

                        if ($_SESSION['login']['type'] != 'admin') {  // disabled validations for admin
                                $this->form_validation->set_rules('address', 'Address', 'trim|required');
                                $this->form_validation->set_rules('city', 'City', 'trim|required');
                                $this->form_validation->set_rules('state', 'Area', 'trim|required');
                                $this->form_validation->set_rules('post_code', 'Post Code', 'trim|required');
                        } //
                        //$this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                        $this->form_validation->set_rules('password', 'Password', 'matches[password2]');
                        $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');

                        if ($_SESSION['login']['type'] == 'admin') {
                                $this->form_validation->set_rules('tree_nums', 'Tree number', 'trim|required|is_natural');
                                //
                                $this->form_validation->set_rules('price', 'Price', 'trim|required|is_numeric');
                                $this->form_validation->set_rules('currency', 'Currency', 'trim|required|is_natural_no_zero');
                                $this->form_validation->set_rules('tax', 'Tax', 'trim|required|is_numeric');

                                //$this->form_validation->set_rules('subject', 'Subject', 'trim|required');
                                $this->form_validation->set_rules('cc_email', 'CC Email', 'trims');
                                $this->form_validation->set_rules('cc_people_email', 'CCd', 'trims');
                                // $this->form_validation->set_rules('message', 'Message', 'trim|required');
                                //$this->form_validation->set_rules('certificate_text', 'Text on Certificate', 'trim|required');
                        }

                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        $this->load->model('user_model');
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Edit Bulk Partner', 'body' => 'bulk_partner_edit');
                                $data2 = $this->user_model->load_bulk_partner($id);
                                $this->data = array_merge($this->data, $data, $data2);
                                $this->load->view('template', $this->data);
                        } else {
                                $logo = '';
                                // check upload
                                if ($_FILES['logo']['size']) {
                                        $config['upload_path'] = './uploads/company/';
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '10000';
                                        $this->load->library('upload');
                                        $this->upload->initialize($config);
                                        if (!$this->upload->do_upload('logo')) {
                                                $data = array('title' => 'Edit Bulk Partner', 'body' => 'bulk_partner_edit', 'error' => $this->upload->display_errors());
                                                $data2 = $this->user_model->load_bulk_partner($id);
                                                $this->data = array_merge($this->data, $data, $data2);
                                                $this->load->view('template', $this->data);
                                                return;
                                        } else {
                                                $logo = $this->upload->data();
                                                $logo = $logo['file_name'];
                                        }
                                }

                                $this->user_model->update_bulk_partner($id, $logo);

                                redirect(site_url());
                        }
                } else {
                        $id = empty($id) ? $_SESSION['login']['id'] : $id;
                        $this->load->model('user_model');
                        $data2 = $this->user_model->load_bulk_partner($id);
                        //$this->check_user();
                        $data = array('title' => 'Edit Bulk Partner', 'body' => 'bulk_partner_edit');
                        $this->data = array_merge($this->data, $data, $data2);
                        $this->load->view('template', $this->data);
                }
        }

        function delete_bulk_partner($id) {
                $this->check_user(array("admin", "manager"));
                $this->load->database();
                $this->load->model('user_model');

                //
                // load clients
                $id_str = '';
                $clients = $this->user_model->load_clients($id);
                foreach ($clients AS $v) {
                        $id_str .= $v->user_id . ',';
                }
                $id_str .= $id;
                $this->db->query("DELETE FROM pct_users WHERE id IN ($id_str)");
                $this->db->query("DELETE FROM pct_bulk_partners WHERE user_id IN ($id_str)");
                $this->db->query("DELETE FROM pct_referer WHERE user_id IN ($id_str)");
                $this->db->query("DELETE FROM pct_invoices WHERE user_id IN ($id_str)");
                redirect(site_url());
        }

        /**
         * =========================================================================================================
        section for intitiative clients
         */
        function intitiative_clients_list() {
                $this->check_user('intitiative');
                $this->load->model('user_model');
                $rows = $this->user_model->get_intitiative_clients_list($_SESSION['login']['id']);
				//echo "<pre>"; print_r($rows); die;
                $this->load->view('template', array('title' => 'Inititiative Clients List', 'body' => 'intitiative_clients_list', 'rows' => $rows));
        }
		 /**
         * =========================================================================================================
        section for clients
         */
        function clients_list() {
                $this->check_user('bulk_partner');
                $this->load->model('user_model');
                $rows = $this->user_model->get_clients_list($_SESSION['login']['id']);
                $this->load->view('template', array('title' => 'Clients List', 'body' => 'clients_list', 'rows' => $rows));
        }
		 /**
         * =========================================================================================================
			function to encode utf8
         */
		function utf8_converter($array)       
		{
			array_walk_recursive($array, function(&$item, $key){
				if(!mb_detect_encoding($item, 'utf-8', true)){
						$item = utf8_encode($item);
				}
			});
		 
			return $array;
		}
		 /**
         * =========================================================================================================
			section for add clients automatically from Tpcc API
         */
		function add_tpccapi_intitiative_clients() {               
			 $this->load->model('user_model');
			if(!empty($_GET)){
					$total_trees = 0;
					$free_trees = 0;
					$old_user = 0;
					$old_rows = 0;
					$new_rows = 0;
					$client_trees = 0;
				 $sql = "SELECT * FROM pct_users  WHERE id='1300'";    //1300 tpcc initiative partner ID
                 $tpcc_data = $this->db->query($sql)->row();
				 $_SESSION['login'] = (array)$tpcc_data;      			
				 if (is_array($_GET) && count($_GET)) {
					$data_row = array();
					$data_row =  $this->utf8_converter($_GET);
					$data_row['password'] = $this->randomKey(6);
					if (!is_numeric($data_row['tree_nums']) || !trim($data_row['password']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)
					) {
							continue;
					}
					
					$rff_temp = str_replace(" ","_",$data_row['company']);           //referid as company name
					$data_row['referer_id'] = str_replace("’","",strtolower($rff_temp));
					
					$data_row['username'] = $data_row['email'];
					
					// create referer id
					$referer_id = trim($data_row['referer_id']);
					$i = 0;
					while (!$this->referer_check($referer_id)) {
							$i++;
							$referer_id = $data_row['referer_id'] . $i;
					}
					
					$data_row['referer_id'] = $referer_id;
					
					$data_row['short_code'] =  trim($data_row['short_code']);    //to show short_code as refer_id in tpcc api
					
					if($data_row['free_trees']==""){
						$data_row['free_trees']=0;
					}
					$client_trees = $data_row['tree_nums'];
				
					if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['login']['id'])) {       
						  
						 	$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
						
							$query = $this->db->query($qr);
							if ($query->num_rows()) {
								   
									$rowq = $query->row_array();
									$tr = $rowq['tree_nums'];
									$f_tr = $tr + $data_row['tree_nums']+$rowq['free_trees'];
									
									$qru = "UPDATE pct_initiative_partner SET tree_nums = '" . $f_tr . "' WHERE user_id = '" . $rowq['user_id'] . "' ";
									$this->db->query($qru);
							}
							continue;
					}
					if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) {
						
							$old_user = 1;
							$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
							$query = $this->db->query($qr);
							if ($query->num_rows()) {
									$rowq = $query->row_array();
									$tr = $rowq['tree_nums'];
									$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
							}
							$client_trees = $f_tr;
							/* $check_em = "SELECT * FROM pct_users AS u INNER JOIN pct_referer AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";    //to get old referr_id */
							$check_em = "SELECT * FROM pct_referer WHERE user_id='" . $rowq['user_id'] . "' ";    //to get old referr_id
							$getuser_id = $this->db->query($check_em);
							 if ($getuser_id->num_rows()) {
								  $ress = $getuser_id->row_array();
								  $data_row['referer_id'] = $ress['code'];
							 }
							
							//continue;
					}

					if(isset($_SESSION['login']['id'])){
					
						$check_ini_id = "SELECT * FROM pct_initiative_partner WHERE user_id='" . $_SESSION['login']['id'] . "' ";    //to get  initiative id
							$getini_id = $this->db->query($check_ini_id);
							 if ($getini_id->num_rows()) {
								  $ress1 = $getini_id->row_array();
								  $data_row['initiative_id'] = $ress1['initiative'];
							 }
					}
					
					if ($old_user == 0) {
							$data_row['old_user'] = $old_user;
							$data_row['client_trees'] = $client_trees;
							$new_rows++;
					} else if ($old_user == 1) {
							$data_row['old_user'] = $old_user;
							$data_row['client_trees'] = $client_trees;
							$old_rows++;
					}

					$data_rows[] = $data_row;
					$total_trees += $data_row['tree_nums'];
					$free_trees += $data_row['free_trees'];
					
				}
				
				$price = $_SESSION['login']['price'];
				$bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
				$invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
				$current_trees = $total_trees;
				$all_free_trees = $free_trees;
				$all_invoice_trees = $total_trees+$free_trees;
				//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
				$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']; 
				$tax = $_SESSION['login']['tax'];
				$sub_total = $current_trees * $price / 100;
				$total_price = $sub_total * ( 100 + $tax ) / 100;
				$total_price = number_format($total_price, 2);
				$data = array('title' => 'Confirmation', 'body' => 'initiative_confirmation', 'total_trees' => $total_trees, 'current_trees' => $all_invoice_trees, 'total_price' => $total_price, 'tax' => $tax, 'sub_total' => $sub_total, 'total_rows' => $new_rows, 'total_rows_updated' => $old_rows);
				$this->data = array_merge($this->data, $data);
				
				//var_dump($data_rows);
				if (count($data_rows)) {
					//echo "<pre>"; print_r($data_rows); die;
						$_SESSION['login']['data'] = array();
						$_SESSION['login']['data']['rows'] = json_encode($data_rows);
						$_SESSION['login']['data']['total'] = $total_price;
						$_SESSION['login']['data']['total_trees'] = $total_trees;
						$_SESSION['login']['data']['current_tree'] = $current_trees;
						$_SESSION['login']['data']['all_invoice_trees'] = $all_invoice_trees;
						$_SESSION['login']['data']['all_free_trees'] = $all_free_trees;
						$_SESSION['login']['data']['from_tpcc_api'] = "1";

						// expend
						// $_SESSION['login']['data']['code']   = $referer_id;
						$_SESSION['login']['data']['address'] = $bulk_partner['address'];
						$_SESSION['login']['data']['city'] = $bulk_partner['city'];
						$_SESSION['login']['data']['state'] = $bulk_partner['state'];
						$_SESSION['login']['data']['post_code'] = $bulk_partner['post_code'];
				}
					// echo "<pre>"; print_r($data_rows); 
					// print_r($_SESSION); die('here');
					$this->confirm_intitiative();
			}
		}
		/* function to autoupload csv from ondrive*/
		
		function autoupload_clients_from_imacro() {
				$this->load->database();
				$this->load->model('user_model');
                if (isset($_POST['token'])) {
					   
					     // process Initiative data
                        $this->load->library('form_validation');
						
						$this->form_validation->set_rules('token', 'Token', 'trim|required'); 
						$this->form_validation->set_rules('first_name', 'First Name', 'trim|required'); 
						$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required'); 
						$this->form_validation->set_rules('email_address', 'Email', 'trim|required'); 
						$this->form_validation->set_rules('company', 'Company', 'trim|required'); 
						$this->form_validation->set_rules('tree_nums', 'Trees', 'trim|required'); 
						$this->form_validation->set_rules('partner_id', 'Partner ID', 'trim|required'); 
					
						
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Add Initiative', 'body' => 'autoupload_clients');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
													
                                $this->load->database();
                                $this->load->model('user_model');
								 if($_POST['token']=='7ace69e33f3b38dd1dd0920868700b5c'){
									  $this->user_model->insert_autoupload_clients();
									   echo "record Inserted";
								 }else{
									 echo "Token is not matched"; 
									 exit;
								 }
                        }
                } else {
					$data = array('title' => 'Add Client Trees', 'body' => 'autoupload_clients');
					$this->data = array_merge($this->data, $data);
					$this->load->view('template', $this->data);
                }
			
		}
		/* upload client from inbound API */
		function inbound_upload_client_cronjob() {
			
			 $records = $this->user_model->get_inbound_api_data();
			 //echo "<pre>"; print_r($records); die;
			 foreach($records as $record){
				 
					$client_data = (array)json_decode($record['data']);
					//echo "<pre>"; print_r($record); die;
					$first = true;
					$data_rows = array();
					$data_row['free_trees'] = 0;
					$data_rows_updated = array();
					$total_trees = 0;
					$free_trees = 0;
					$old_user = 0;
					$old_rows = 0;
					$new_rows = 0;
					$client_trees = 0;		
					$partner_id = utf8_encode($record['panacea_id']);
					
					 $sql = "SELECT * FROM pct_users  WHERE id='" . $partner_id . "'";               //Pass initiative partner ID
					 $initiative_partner_login_data = $this->db->query($sql)->row();
					 $_SESSION['login'] = (array)$initiative_partner_login_data;   
					//echo "<pre>"; print_r($_SESSION); die;
					
					foreach(array($client_data) as $data){
						
							//echo "<pre>"; print_r($data); die;
				 
							$old_user = 0;
							$data_row = array();
							//$data_row['company']    = utf8_encode($data['first_name'].' '.$data['last_name']);
							$data_row['company']    = ($data['first_name'].' '.$data['last_name']);
							$data_row['email']      = ($data['email']);
							$data_row['first_name'] = ($data['first_name']);
							$data_row['last_name']  = ($data['last_name']);
							$data_row['tree_nums']  = ($data['no_of_trees']);
							$data_row['free_trees'] = '0';
							
							
							$data_row['password'] = $this->randomKey(6);
							if (!is_numeric($data_row['tree_nums']) || !trim($data_row['password']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)
							) {
									continue;
							}
							//$data_row['referer_id'] = $this->randomKey(5);
							
							$rff_temp = str_replace(" ","_",$data_row['company']);           //referid as company name
							$data_row['referer_id'] = str_replace("’","",strtolower($rff_temp));
							
							$data_row['username'] = $data_row['email'];
							
							// create referer id
							$referer_id = trim($data_row['referer_id']);
							$i = 0;
							while (!$this->referer_check($referer_id)) {
									$i++;
									$referer_id = $data_row['referer_id'] . $i;
							}
							$data_row['referer_id'] = $referer_id;
							if($data_row['free_trees']==""){
								$data_row['free_trees']=0;
							}
							$client_trees = $data_row['tree_nums'];
							
							if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['login']['id'])) {
								  
									$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
									$query = $this->db->query($qr);
									if ($query->num_rows()) {
										   
											$rowq = $query->row_array();
											$tr = $rowq['tree_nums'];
											$f_tr = $tr + $data_row['tree_nums']+$rowq['free_trees'];
											
											$qru = "UPDATE pct_initiative_partner SET tree_nums = '" . $f_tr . "' WHERE user_id = '" . $rowq['user_id'] . "' ";
											$this->db->query($qru);
									}
									continue;
							}
							if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) {
									$old_user = 1;
									 $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
								  
									$query = $this->db->query($qr);
									if ($query->num_rows()) {
											$rowq = $query->row_array();
											$tr = $rowq['tree_nums'];
											$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
									}
									$client_trees = $f_tr;
								 
									//$check_em = "SELECT * FROM pct_users AS u INNER JOIN pct_referer AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";    //to get old referr_id
									$check_em = "SELECT * FROM pct_referer WHERE user_id='" . $rowq['user_id'] . "' ";    //to get old referr_id
									$getuser_id = $this->db->query($check_em);
									 if ($getuser_id->num_rows()) {
										  $ress = $getuser_id->row_array();
										  $data_row['referer_id'] = $ress['code'];
									 }
									
									//continue;
							}
							if(isset($_SESSION['login']['id'])){
							
								$check_ini_id = "SELECT * FROM pct_initiative_partner WHERE user_id='" . $_SESSION['login']['id'] . "' ";    //to get  initiative id
									$getini_id = $this->db->query($check_ini_id);
									 if ($getini_id->num_rows()) {
										  $ress1 = $getini_id->row_array();
										  $data_row['initiative_id'] = $ress1['initiative'];
									 }
							}
							if ($old_user == 0) {
									$data_row['old_user'] = $old_user;
									$data_row['client_trees'] = $client_trees;
									$new_rows++;
							} else if ($old_user == 1) {
									$data_row['old_user'] = $old_user;
									$data_row['client_trees'] = $client_trees;
									$old_rows++;
							}

							$data_rows[] = $data_row;
							$total_trees += $data_row['tree_nums'];
							$free_trees += $data_row['free_trees'];
					}
					
					/* echo "<pre>"; print_r($data_rows); die('here');  */
					$price = $_SESSION['login']['price'];
					$bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
					$invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
					$current_trees = $total_trees;
					$all_free_trees = $free_trees;
					$all_invoice_trees = $total_trees+$free_trees;
					//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
					$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']+$bulk_partner['free_trees']; 
					$tax = $_SESSION['login']['tax'];
					$sub_total = $current_trees * $price / 100;
					$total_price = $sub_total * ( 100 + $tax ) / 100;
					$total_price = number_format($total_price, 2);
					
					//var_dump($data_rows);
					if (count($data_rows)) {
						//echo "<pre>"; print_r($data_rows); die;
							$_SESSION['login']['data'] = array();
							$_SESSION['login']['data']['rows'] = json_encode($data_rows);
							$_SESSION['login']['data']['total'] = $total_price;
							$_SESSION['login']['data']['total_trees'] = $total_trees;
							$_SESSION['login']['data']['current_tree'] = $current_trees;
							$_SESSION['login']['data']['all_invoice_trees'] = $all_invoice_trees;
							$_SESSION['login']['data']['all_free_trees'] = $all_free_trees;
							

							// expend
							// $_SESSION['login']['data']['code']   = $referer_id;
							$_SESSION['login']['data']['address'] = $bulk_partner['address'];
							$_SESSION['login']['data']['city'] = $bulk_partner['city'];
							$_SESSION['login']['data']['state'] = $bulk_partner['state'];
							$_SESSION['login']['data']['post_code'] = $bulk_partner['post_code'];
							
							$_SESSION['login']['data']['from_initiaive_api'] = "1";

					}
					//echo "<pre>"; print_r($_SESSION); die("here");
					$current_dt = date("Y-m-d H:i:s");
				    $update_query = "UPDATE panacea_api_data SET processed = '1',proc_date ='" . $current_dt. "' WHERE id = '" . $record['id'] . "' "; 
                    $this->db->query($update_query);
					
					$this->confirm_intitiative();        // to add the trees against the partner
					
					$current_dt = date("Y-m-d H:i:s");
				    $update_query = "UPDATE panacea_api_data SET processed = '2',proc_date ='" . $current_dt. "' WHERE id = '" . $record['id'] . "' "; 
                    $this->db->query($update_query);
					
					echo "Client uploaded successfully : ".$client_data['email'].'<br>';

                }
			 }
		
		/* to add initiative client from API */
		function add_initiative_client_from_api() {
			 
			if(isset($_GET['token']) && $_GET['token']=="hiwqr0d0uqxx9di") {
				
				$first = true;
				$data_rows = array();
				$data_row['free_trees'] = 0;
				$data_rows_updated = array();
				$total_trees = 0;
				$free_trees = 0;
				$old_user = 0;
				$old_rows = 0;
				$new_rows = 0;
				$client_trees = 0;		
				$partner_id = utf8_encode($_POST['partner_id']);
				
				 $sql = "SELECT * FROM pct_users  WHERE id='" . $partner_id . "'";               //Pass initiative partner ID
				 $initiative_partner_login_data = $this->db->query($sql)->row();
				 $_SESSION['login'] = (array)$initiative_partner_login_data;   
				// echo "<pre>"; print_r($_SESSION); die;
				
				foreach(array($_POST) as $data){
					
					echo "<pre>"; print_r($data); die;
		 
					$old_user = 0;
					$data_row = array();
					$data_row['company']    = utf8_encode($data['company']);
					$data_row['email']      = utf8_encode($data['email']);
					$data_row['first_name'] = utf8_encode($data['first_name']);
					$data_row['last_name']  = utf8_encode($data['last_name']);
					$data_row['tree_nums']  = utf8_encode($data['tree_nums']);
					$data_row['free_trees'] = utf8_encode($data['free_trees']);
					
					
					$data_row['password'] = $this->randomKey(6);
					if (!is_numeric($data_row['tree_nums']) || !trim($data_row['password']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)
					) {
							continue;
					}
					//$data_row['referer_id'] = $this->randomKey(5);
					
					$rff_temp = str_replace(" ","_",$data_row['company']);           //referid as company name
					$data_row['referer_id'] = str_replace("’","",strtolower($rff_temp));
					
					$data_row['username'] = $data_row['email'];
					
					// create referer id
					$referer_id = trim($data_row['referer_id']);
					$i = 0;
					while (!$this->referer_check($referer_id)) {
							$i++;
							$referer_id = $data_row['referer_id'] . $i;
					}
					$data_row['referer_id'] = $referer_id;
					if($data_row['free_trees']==""){
						$data_row['free_trees']=0;
					}
					$client_trees = $data_row['tree_nums'];
					
					if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['login']['id'])) {
						  
							$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
							$query = $this->db->query($qr);
							if ($query->num_rows()) {
								   
									$rowq = $query->row_array();
									$tr = $rowq['tree_nums'];
									$f_tr = $tr + $data_row['tree_nums']+$rowq['free_trees'];
									
									$qru = "UPDATE pct_initiative_partner SET tree_nums = '" . $f_tr . "' WHERE user_id = '" . $rowq['user_id'] . "' ";
									$this->db->query($qru);
							}
							continue;
					}
					if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) {
							$old_user = 1;
							 $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
						  
							$query = $this->db->query($qr);
							if ($query->num_rows()) {
									$rowq = $query->row_array();
									$tr = $rowq['tree_nums'];
									$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
							}
							$client_trees = $f_tr;
						 
							/* $check_em = "SELECT * FROM pct_users AS u INNER JOIN pct_referer AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";    //to get old referr_id */
							$check_em = "SELECT * FROM pct_referer WHERE user_id='" . $rowq['user_id'] . "' ";    //to get old referr_id
							$getuser_id = $this->db->query($check_em);
							 if ($getuser_id->num_rows()) {
								  $ress = $getuser_id->row_array();
								  $data_row['referer_id'] = $ress['code'];
							 }
							
							//continue;
					}
					if(isset($_SESSION['login']['id'])){
					
						$check_ini_id = "SELECT * FROM pct_initiative_partner WHERE user_id='" . $_SESSION['login']['id'] . "' ";    //to get  initiative id
							$getini_id = $this->db->query($check_ini_id);
							 if ($getini_id->num_rows()) {
								  $ress1 = $getini_id->row_array();
								  $data_row['initiative_id'] = $ress1['initiative'];
							 }
					}
					if ($old_user == 0) {
							$data_row['old_user'] = $old_user;
							$data_row['client_trees'] = $client_trees;
							$new_rows++;
					} else if ($old_user == 1) {
							$data_row['old_user'] = $old_user;
							$data_row['client_trees'] = $client_trees;
							$old_rows++;
					}

					$data_rows[] = $data_row;
					$total_trees += $data_row['tree_nums'];
					$free_trees += $data_row['free_trees'];
				}
				
				/* echo "<pre>"; print_r($data_rows); die('here');  */
				$price = $_SESSION['login']['price'];
				$bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
				$invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
				$current_trees = $total_trees;
				$all_free_trees = $free_trees;
				$all_invoice_trees = $total_trees+$free_trees;
				//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
				$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']+$bulk_partner['free_trees']; 
				$tax = $_SESSION['login']['tax'];
				$sub_total = $current_trees * $price / 100;
				$total_price = $sub_total * ( 100 + $tax ) / 100;
				$total_price = number_format($total_price, 2);
				
				//var_dump($data_rows);
				if (count($data_rows)) {
					//echo "<pre>"; print_r($data_rows); die;
						$_SESSION['login']['data'] = array();
						$_SESSION['login']['data']['rows'] = json_encode($data_rows);
						$_SESSION['login']['data']['total'] = $total_price;
						$_SESSION['login']['data']['total_trees'] = $total_trees;
						$_SESSION['login']['data']['current_tree'] = $current_trees;
						$_SESSION['login']['data']['all_invoice_trees'] = $all_invoice_trees;
						$_SESSION['login']['data']['all_free_trees'] = $all_free_trees;
						

						// expend
						// $_SESSION['login']['data']['code']   = $referer_id;
						$_SESSION['login']['data']['address'] = $bulk_partner['address'];
						$_SESSION['login']['data']['city'] = $bulk_partner['city'];
						$_SESSION['login']['data']['state'] = $bulk_partner['state'];
						$_SESSION['login']['data']['post_code'] = $bulk_partner['post_code'];
						
						$_SESSION['login']['data']['from_initiaive_api'] = "1";

				}
				//echo "<pre>"; print_r($_SESSION); die("here");
				$this->confirm_intitiative();        // to add the trees against the partner

                }
		}
		function add_intitiative_clients_dev() {
			
                $this->check_user('intitiative');
				
				$user_id = $_SESSION['login']['id'];
				$parter  = $this->user_model->get_parter_data($user_id);
				//echo "<pre>"; print_r($parter); die;
				
                if (isset($_POST['add_initiative_clients'])) {
					 
                        // initialize upload library
                        $config['upload_path'] = './uploads/';
                        $config['allowed_types'] = 'csv';
                        $config['max_size'] = '10000';

                        $this->load->library('upload', $config);

                        if (!$this->upload->do_upload('csv_file1')) {
							
                                $error = array('error' => $this->upload->display_errors());
                                $data = array('title' => 'Upload clients', 'body' => 'add_initiative_clients');
                                $this->data = array_merge($this->data, $data, $error);
                                $this->load->view('template', $this->data);
								
                        } else {

                                // process after uploading file successfully
                                $this->load->model('user_model');
                                $data = $this->upload->data();
                                // read csv file\
                                $first = true;
                                $file = fopen($data['full_path'], "r");
								$data_rows = array();
                                $data_rows_updated = array();
                                $total_trees = $free_trees =  $old_user = $old_rows = $new_rows = $client_trees = 0;
								
								$invalid_csv = array();
							
                                while (!feof($file)) { 
								
                                        $old_user = 0; 
										
                                        // check if csv file is valid
                                        if ($first) {
                                                $head_title = array('company', 'first_name', 'last_name', 'email', 'tree_nums', 'free_trees');
                                                $data = fgetcsv($file);

                                                foreach ($data AS &$v_title) {
                                                        $v_title = strtolower($v_title);
                                                }
                                                
                                                foreach ($head_title AS $v) {
													if (!in_array($v, $data)) {
														$invalid_csv['header'] = 1;
													}
                                                } 
                                                if (count($data) < 6) {
                                                      //show_error('Invalid CSV file');
													$invalid_csv['items'] = 1;
                                                }

                                                $header_mapping_array = $data;

                                                $first = false;
                                                continue; 
                                        } 
										
                                        $data = fgetcsv($file);
										
                                        if (is_array($data) && count($data)) {
											
                                                $data_row = array();
                                                foreach ($data as $k => &$v) {
													$v = trim($v);
													if($v){
														$data_row[$header_mapping_array[$k]] = utf8_encode($v);
														
													}
                                                }
																								 									
											//echo "<pre>"; print_r($data_row); 
											$client_trees = $data_row['tree_nums'];
											
											if ($data_row && (!is_numeric($data_row['tree_nums']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL))) {
													$invalid_csv['items'] = 1;
											}  
											
											if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['login']['id'])) {
				  
													$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
													$query = $this->db->query($qr);
													if ($query->num_rows()) {
														   
															$rowq = $query->row_array();
															$tr = $rowq['tree_nums'];
															$f_tr = $tr + $data_row['tree_nums'] + $rowq['free_trees'];
													}
													continue;
											}
											if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) { 
											
													$old_user = 1;
													 $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
												  
													$query = $this->db->query($qr);
													if ($query->num_rows()) {
															$rowq = $query->row_array();
															$tr = $rowq['tree_nums'];
															$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
													}
													$client_trees = $f_tr;
													
													//continue;
											}
											if ($old_user == 0) { 
													$data_row['old_user'] = $old_user;
													$data_row['client_trees'] = $client_trees;
													$new_rows++;
											} else if ($old_user == 1) {
													$data_row['old_user'] = $old_user;
													$data_row['client_trees'] = $client_trees;
													$old_rows++;
											}  
											$data_rows[] = $data_row;
											$total_trees += $data_row['tree_nums'];
											$free_trees += $data_row['free_trees'];
										}
								}
								
								if ($invalid_csv) {
									
									if($invalid_csv['header'] == 1){
										$error = array('error' => "Invalid CSV headings.Please check the format on sample CSV.");
									
									}
									if($invalid_csv['items'] == 1){
										$error = array('error' => "Invalid CSV items.Please check the format on sample CSV.");
									
									}
									
									$data 	=  array('title' => 'Upload clients', 'body' => 'add_initiative_clients');
									$this->data = array_merge($this->data, $data, $error);
									$this->load->view('template', $this->data);
									
								}else{
									
									$data_rows = array_map("unserialize", array_unique(array_map("serialize", $data_rows)));
									
									$records = [];
									foreach($data_rows as $key => $_data){
										
										if($_data['company']!=""){
											
											$records[$key]['company']    = $_data['company'];
											$records[$key]['email']  	 = $_data['email'];
											$records[$key]['first_name'] = $_data['first_name'];
											$records[$key]['last_name']  = $_data['last_name'];
											$records[$key]['tree_nums']  = ($_data['tree_nums']=="")? 0 : $_data['tree_nums'];
											$records[$key]['free_trees'] = ($_data['free_trees']=="")? 0 : $_data['free_trees'];
										}
									} 
									
									$price = $_SESSION['login']['price'];
									$bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
									$invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
									$current_trees = $total_trees;
									$all_free_trees = $free_trees;
									$all_invoice_trees = $total_trees+$free_trees;
									//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
									$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']+$bulk_partner['free_trees']; 
									$tax = $_SESSION['login']['tax'];
									$sub_total = $current_trees * $price / 100;
									$total_price = $sub_total * ( 100 + $tax ) / 100;
									$total_price = number_format($total_price, 2);
									
									$data = array('title' => 'Confirmation', 'body' => 'initiative_confirmation', 'total_trees' => $total_trees, 'current_trees' => $all_invoice_trees, 'total_price' => $total_price, 'tax' => $tax, 'sub_total' => $sub_total, 'total_rows' => $new_rows, 'total_rows_updated' => $old_rows);
									$this->data = array_merge($this->data, $data);
									
								   // echo "<pre>"; print_r($data_rows); die;
									if (count($data_rows)) {
										//echo "<pre>"; print_r($data_rows); die;
											$_SESSION['login']['data'] = array();
											$_SESSION['login']['data']['rows'] =  json_encode($records);
											$_SESSION['login']['data']['total_trees'] 	= $total_trees;
									}
									$this->load->view('template', $this->data);
									
								}
                                
                        }
                } elseif(isset($_POST['add_grid_clients'])) {
					  	
								$records = [];
								$data_rows = array();
                                $data_rows_updated = array();
                                $total_trees = $free_trees =  $old_user = $old_rows = $new_rows = $client_trees = 0;
								
                                foreach($_POST['breakdown'] as $key => $data){
									
									$old_user = 0;
		
									$data_row = array();
									$data_row['company'] = ($data['company']);
									$data_row['email'] = ($data['email']);
									$data_row['first_name'] = ($data['first_name']);
									$data_row['last_name'] = ($data['last_name']);
									$data_row['tree_nums'] = ($data['tree_nums']);	
									
									$records[$key]['company'] 		= $data['company'];
									$records[$key]['email'] 		= $data['email'];
									$records[$key]['first_name'] 	= $data['first_name'];
									$records[$key]['last_name'] 	= $data['last_name'];
									$records[$key]['tree_nums'] 	= $data['tree_nums'];
									$records[$key]['free_trees'] 	= 0;
									
									$data_row['free_trees'] = 0;
									
									$client_trees = $data_row['tree_nums'];
									
									if (!is_numeric($data_row['tree_nums']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)) {
												continue;
									} 
									
									if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) {
											$old_user = 1;
											 $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
										  
											$query = $this->db->query($qr);
											if ($query->num_rows()) {
													$rowq = $query->row_array();
													$tr = $rowq['tree_nums'];
													$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
											}
											$client_trees = $f_tr;
											
											//continue;
									}
									
									if ($old_user == 0) {
											$data_row['old_user'] = $old_user;
											$data_row['client_trees'] = $client_trees;
											$new_rows++;
									} else if ($old_user == 1) {
											$data_row['old_user'] = $old_user;
											$data_row['client_trees'] = $client_trees;
											$old_rows++;
									}

									$data_rows[] = $data_row;
									$total_trees += $data_row['tree_nums'];
									$free_trees  += $data_row['free_trees'];
									
                                }
								
								$price = $_SESSION['login']['price'];
								$bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
								$invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
								$current_trees = $total_trees;
								$all_free_trees = $free_trees;
								$all_invoice_trees = $total_trees+$free_trees;
								//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
								$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']+$bulk_partner['free_trees']; 
								$tax = $_SESSION['login']['tax'];
								$sub_total = $current_trees * $price / 100;
								$total_price = $sub_total * ( 100 + $tax ) / 100;
								$total_price = number_format($total_price, 2);
								
								$data = array('title' => 'Confirmation', 'body' => 'initiative_confirmation', 'total_trees' => $total_trees, 'current_trees' => $all_invoice_trees, 'total_price' => $total_price, 'tax' => $tax, 'sub_total' => $sub_total, 'total_rows' => $new_rows, 'total_rows_updated' => $old_rows);
								$this->data = array_merge($this->data, $data);
								
								if (count($data_rows)) {
										//echo "<pre>"; print_r($data_rows); die;
										$_SESSION['login']['data'] = array();
										$_SESSION['login']['data']['rows'] =  json_encode($records);
										$_SESSION['login']['data']['total_trees'] 	= $total_trees;
								}
								$this->load->view('template', $this->data);
								
								
                } elseif($parter['grid_upload']=='1') {   

						$clients['client_data'] = $this->user_model->get_last_uploaded_clients($_SESSION['login']['id']);
						//echo "<pre>"; print_r($clients); die;
						$data = array('title' => 'Upload clients', 'body' => 'add_grid_initiative_clients');
                        $this->data = array_merge($this->data, $data,$clients);
                        $this->load->view('template', $this->data);
					
				}else{
					
                        $data = array('title' => 'Upload clients', 'body' => 'add_initiative_clients');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }
		
		function submit_initiative_records(){ 
			
			$this->check_user('intitiative');
			$user_id = $_SESSION['login']['id'];
			
			$upload['partner_id']   =  $user_id; 
			$upload['upload_data']  =  $_SESSION['login']['data']['rows']; 
			 $this->db->insert_batch('pct_partner_csv_upload_data', array($upload));
	
			$success = array('success' => "Records uploaded successfully!");
			$data 	=  array('title' => 'Upload clients', 'body' => 'add_initiative_clients');
			$this->data = array_merge($this->data, $data, $success);
			$this->load->view('template', $this->data); 
		}
		
		function add_intitiative_clients() { 
			
                $this->check_user('intitiative');
				
				$user_id = $_SESSION['login']['id'];
				$parter  = $this->user_model->get_parter_data($user_id);
				//echo "<pre>"; print_r($parter); die;
				
                if (isset($_POST['add_initiative_clients'])) {
					 
                        // initialize upload library
                        $config['upload_path'] = './uploads/';
                        $config['allowed_types'] = 'csv';
                        $config['max_size'] = '10000';

                        $this->load->library('upload', $config);

                        if (!$this->upload->do_upload('csv_file1')) {
							
                                $error = array('error' => $this->upload->display_errors());
                                $data = array('title' => 'Upload clients', 'body' => 'add_initiative_clients');
                                $this->data = array_merge($this->data, $data, $error);
                                $this->load->view('template', $this->data);
								
                        } else {

                                // process after uploading file successfully
                                $this->load->model('user_model');
                                $data = $this->upload->data();
                                // read csv file\
                                $first = true;
                                $file = fopen($data['full_path'], "r");
								$data_rows = array();
                                $data_rows_updated = array();
                                $total_trees = $free_trees =  $old_user = $old_rows = $new_rows = $client_trees = 0;
								
								$invalid_csv = array();
							
                                while (!feof($file)) { 
								
                                        $old_user = 0; 
										
                                        // check if csv file is valid
                                        if ($first) {
                                                $head_title = array('company', 'first_name', 'last_name', 'email', 'tree_nums', 'free_trees');
                                                $data = fgetcsv($file);

                                                foreach ($data AS &$v_title) {
                                                        $v_title = strtolower($v_title);
                                                }
                                                
                                                foreach ($head_title AS $v) {
													if (!in_array($v, $data)) {
														$invalid_csv['header'] = 1;
													}
                                                } 
                                                if (count($data) < 6) {
                                                      //show_error('Invalid CSV file');
													$invalid_csv['items'] = 1;
                                                }

                                                $header_mapping_array = $data;

                                                $first = false;
                                                continue; 
                                        } 
										
                                        $data = fgetcsv($file);
										
                                        if (is_array($data) && count($data)) {
											
                                                $data_row = array();
                                                foreach ($data as $k => &$v) {
													$v = trim($v);
													if($v){
														$data_row[$header_mapping_array[$k]] = utf8_encode($v);
														
													}
                                                }
																								 									
											//echo "<pre>"; print_r($data_row); 
											$client_trees = $data_row['tree_nums'];
											
											if ($data_row && (!is_numeric($data_row['tree_nums']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL))) {
													$invalid_csv['items'] = 1;
											}  
											
											if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['login']['id'])) {
				  
													$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
													$query = $this->db->query($qr);
													if ($query->num_rows()) {
														   
															$rowq = $query->row_array();
															$tr = $rowq['tree_nums'];
															$f_tr = $tr + $data_row['tree_nums'] + $rowq['free_trees'];
													}
													continue;
											}
											if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) { 
											
													$old_user = 1;
													 $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
												  
													$query = $this->db->query($qr);
													if ($query->num_rows()) {
															$rowq = $query->row_array();
															$tr = $rowq['tree_nums'];
															$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
													}
													$client_trees = $f_tr;
													
													//continue;
											}
											if ($old_user == 0) { 
													$data_row['old_user'] = $old_user;
													$data_row['client_trees'] = $client_trees;
													$new_rows++;
											} else if ($old_user == 1) {
													$data_row['old_user'] = $old_user;
													$data_row['client_trees'] = $client_trees;
													$old_rows++;
											}  
											$data_rows[] = $data_row;
											$total_trees += $data_row['tree_nums'];
											$free_trees += $data_row['free_trees'];
										}
								}
								
								if ($invalid_csv) {
									
									if($invalid_csv['header'] == 1){
										$error = array('error' => "Invalid CSV headings.Please check the format on sample CSV.");
									
									}
									if($invalid_csv['items'] == 1){
										$error = array('error' => "Invalid CSV items.Please check the format on sample CSV.");
									
									}
									
									$data 	=  array('title' => 'Upload clients', 'body' => 'add_initiative_clients');
									$this->data = array_merge($this->data, $data, $error);
									$this->load->view('template', $this->data);
									
								}else{
									
									$data_rows = array_map("unserialize", array_unique(array_map("serialize", $data_rows)));
									
									$records = [];
									foreach($data_rows as $key => $_data){
										
										if($_data['company']!=""){
											
											$records[$key]['company']    = $_data['company'];
											$records[$key]['email']  	 = $_data['email'];
											$records[$key]['first_name'] = $_data['first_name'];
											$records[$key]['last_name']  = $_data['last_name'];
											$records[$key]['tree_nums']  = ($_data['tree_nums']=="")? 0 : $_data['tree_nums'];
											$records[$key]['free_trees'] = ($_data['free_trees']=="")? 0 : $_data['free_trees'];
										}
									} 
									
									$price = $_SESSION['login']['price'];
									$bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
									$invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
									$current_trees = $total_trees;
									$all_free_trees = $free_trees;
									$all_invoice_trees = $total_trees+$free_trees;
									//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
									$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']+$bulk_partner['free_trees']; 
									$tax = $_SESSION['login']['tax'];
									$sub_total = $current_trees * $price / 100;
									$total_price = $sub_total * ( 100 + $tax ) / 100;
									$total_price = number_format($total_price, 2);
									
									$data = array('title' => 'Confirmation', 'body' => 'initiative_confirmation', 'total_trees' => $total_trees, 'current_trees' => $all_invoice_trees, 'total_price' => $total_price, 'tax' => $tax, 'sub_total' => $sub_total, 'total_rows' => $new_rows, 'total_rows_updated' => $old_rows);
									$this->data = array_merge($this->data, $data);
									
								   // echo "<pre>"; print_r($data_rows); die;
									if (count($data_rows)) {
										//echo "<pre>"; print_r($data_rows); die;
											$_SESSION['login']['data'] = array();
											$_SESSION['login']['data']['rows'] =  json_encode($records);
											$_SESSION['login']['data']['total_trees'] 	= $total_trees;
									}
									$this->load->view('template', $this->data);
									
								}
                                
                        }
                } elseif(isset($_POST['add_grid_clients'])) {
					  	
								$records = [];
								$data_rows = array();
                                $data_rows_updated = array();
                                $total_trees = $free_trees =  $old_user = $old_rows = $new_rows = $client_trees = 0;
								
                                foreach($_POST['breakdown'] as $key => $data){
									
									$old_user = 0;
		
									$data_row = array();
									$data_row['company'] = ($data['company']);
									$data_row['email'] = ($data['email']);
									$data_row['first_name'] = ($data['first_name']);
									$data_row['last_name'] = ($data['last_name']);
									$data_row['tree_nums'] = ($data['tree_nums']);	
									
									$records[$key]['company'] 		= $data['company'];
									$records[$key]['email'] 		= $data['email'];
									$records[$key]['first_name'] 	= $data['first_name'];
									$records[$key]['last_name'] 	= $data['last_name'];
									$records[$key]['tree_nums'] 	= $data['tree_nums'];
									$records[$key]['free_trees'] 	= 0;
									
									$data_row['free_trees'] = 0;
									
									$client_trees = $data_row['tree_nums'];
									
									if (!is_numeric($data_row['tree_nums']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)) {
												continue;
									} 
									
									if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) {
											$old_user = 1;
											 $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
										  
											$query = $this->db->query($qr);
											if ($query->num_rows()) {
													$rowq = $query->row_array();
													$tr = $rowq['tree_nums'];
													$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
											}
											$client_trees = $f_tr;
											
											//continue;
									}
									
									if ($old_user == 0) {
											$data_row['old_user'] = $old_user;
											$data_row['client_trees'] = $client_trees;
											$new_rows++;
									} else if ($old_user == 1) {
											$data_row['old_user'] = $old_user;
											$data_row['client_trees'] = $client_trees;
											$old_rows++;
									}

									$data_rows[] = $data_row;
									$total_trees += $data_row['tree_nums'];
									$free_trees  += $data_row['free_trees'];
									
                                }
								
								$price = $_SESSION['login']['price'];
								$bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
								$invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
								$current_trees = $total_trees;
								$all_free_trees = $free_trees;
								$all_invoice_trees = $total_trees+$free_trees;
								//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
								$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']+$bulk_partner['free_trees']; 
								$tax = $_SESSION['login']['tax'];
								$sub_total = $current_trees * $price / 100;
								$total_price = $sub_total * ( 100 + $tax ) / 100;
								$total_price = number_format($total_price, 2);
								
								$data = array('title' => 'Confirmation', 'body' => 'initiative_confirmation', 'total_trees' => $total_trees, 'current_trees' => $all_invoice_trees, 'total_price' => $total_price, 'tax' => $tax, 'sub_total' => $sub_total, 'total_rows' => $new_rows, 'total_rows_updated' => $old_rows);
								$this->data = array_merge($this->data, $data);
								
								if (count($data_rows)) {
										//echo "<pre>"; print_r($data_rows); die;
										$_SESSION['login']['data'] = array();
										$_SESSION['login']['data']['rows'] =  json_encode($records);
										$_SESSION['login']['data']['total_trees'] 	= $total_trees;
								}
								$this->load->view('template', $this->data);
								
								
                } elseif($parter['grid_upload']=='1') {   

						$clients['client_data'] = $this->user_model->get_last_uploaded_clients($_SESSION['login']['id']);
						//echo "<pre>"; print_r($clients); die;
						$data = array('title' => 'Upload clients', 'body' => 'add_grid_initiative_clients');
                        $this->data = array_merge($this->data, $data,$clients);
                        $this->load->view('template', $this->data);
					
				}else{
					
                        $data = array('title' => 'Upload clients', 'body' => 'add_initiative_clients');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }
		/********************************************************
		 Function : To upload CSV upload records using Cronjob
		********************************************************/
		
		function upload_partner_csv_records_cron() {
								
				$ini_sql = "SELECT *,( SELECT COUNT(id) FROM `pct_partner_csv_upload_data` WHERE `processed` = '1') as processing from `pct_partner_csv_upload_data` WHERE `processed` = '0' ORDER BY id ASC LIMIT 1";        
				$uploads_data = (array)$this->db->query($ini_sql)->row(); 
				 
				if(empty($uploads_data) || $uploads_data['processing'] > 0){    // if no record in the db or if record is already processing
					
					exit("No record to upload!");
				}
				
				$panid 	   = $uploads_data['partner_id'];
				$table_id  = $uploads_data['id']; 
				
				$alldata  = json_decode($uploads_data['upload_data'], true);
								
				$sql = "SELECT u.*,bp.first_name,bp.last_name,bp.company FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.id='$panid'";    
                $restaurant_data = $this->db->query($sql)->row();
				
				$_SESSION['login'] = (array)$restaurant_data;
				
				// echo "<pre>"; print_r($alldata);  die; 
				$data_rows = array();
				$data_row['free_trees'] = 0;
				$total_trees = $free_trees = $old_user = $old_rows = $new_rows = $client_trees = 0;
				
				foreach($alldata as $data){
					
					$old_user = 0;
					$data_row = array();
					$data_row['company'] 	= ($data['company']);
					$data_row['email'] 		= ($data['email']);
					$data_row['first_name'] = ($data['first_name']);
					$data_row['last_name'] 	= ($data['last_name']);
					$data_row['tree_nums'] 	= ($data['tree_nums']);
					
					//echo "<pre>"; print_r($data_row); die('here');
					$data_row['password'] = $this->randomKey(6);
					if (!is_numeric($data_row['tree_nums']) || !trim($data_row['password']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)) {
							continue;
					}
					//$data_row['referer_id'] = $this->randomKey(5);
					
					$rff_temp = str_replace(" ","_",$data_row['company']);           //referid as company name
					$data_row['referer_id'] = str_replace("’","",strtolower($rff_temp));
					
					$data_row['username'] = $data_row['email'];
					
					// create referer id
					$referer_id = trim($data_row['referer_id']); 
					
					$i = 0;
					while (!$this->referer_check($referer_id)) {
							$i++;
							$referer_id = $data_row['referer_id'] . $i;
					}
					$data_row['referer_id'] = $referer_id;
					if($data_row['free_trees']==""){
						$data_row['free_trees'] = 0;
					}
					$client_trees = $data_row['tree_nums'];
					
					if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['login']['id'])) {
						  
							$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
							$query = $this->db->query($qr);
							if ($query->num_rows()) {
								   
									$rowq = $query->row_array();
									$tr = $rowq['tree_nums'];
									$f_tr = $tr + $data_row['tree_nums']+$rowq['free_trees'];
									
									$qru = "UPDATE pct_initiative_partner SET tree_nums = '" . $f_tr . "' WHERE user_id = '" . $rowq['user_id'] . "' ";
									$this->db->query($qru);
							}
							continue;
					}
					if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) {
							$old_user = 1;
							 $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
						  
							$query = $this->db->query($qr);
							if ($query->num_rows()) {
									$rowq = $query->row_array();
									$tr = $rowq['tree_nums'];
									$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
							}
							$client_trees = $f_tr;
							//$check_em = "SELECT * FROM pct_users AS u INNER JOIN pct_referer AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";    //to get old referr_id
							
							$check_em = "SELECT * FROM pct_referer WHERE user_id='" . $rowq['user_id'] . "' ";    //to get old referr_id
							$getuser_id = $this->db->query($check_em);
							 if ($getuser_id->num_rows()) {
								  $ress = $getuser_id->row_array();
								  $data_row['referer_id'] = $ress['code'];
							 }
							
							//continue;
					}
					if(isset($_SESSION['login']['id'])){
					
						$check_ini_id = "SELECT * FROM pct_initiative_partner WHERE user_id='" . $_SESSION['login']['id'] . "' ";    //to get  initiative id
							$getini_id = $this->db->query($check_ini_id);
							 if ($getini_id->num_rows()) {
								  $ress1 = $getini_id->row_array();
								  $data_row['initiative_id'] = $ress1['initiative'];
							 }
					}
					if ($old_user == 0) {
							$data_row['old_user'] = $old_user;
							$data_row['client_trees'] = $client_trees;
							$new_rows++;
					} else if ($old_user == 1) {
							$data_row['old_user'] = $old_user;
							$data_row['client_trees'] = $client_trees;
							$old_rows++;
					}

					$data_rows[] = $data_row;
					$total_trees += $data_row['tree_nums'];
					$free_trees += $data_row['free_trees'];
				}
				
				/* echo "<pre>"; print_r($data_rows); die('here');  */
				$price = $_SESSION['login']['price'];
				$bulk_partner  = $this->user_model->load_initiative($_SESSION['login']['id']);
				$invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
				$current_trees = $total_trees;
				$all_free_trees = $free_trees;
				$all_invoice_trees = $total_trees+$free_trees;
				//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
				$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']+$bulk_partner['free_trees']; 
				$tax = $_SESSION['login']['tax'];
				$sub_total = $current_trees * $price / 100;
				$total_price = $sub_total * ( 100 + $tax ) / 100;
				$total_price = number_format($total_price, 2);
				
				//var_dump($data_rows);
				if (count($data_rows)) {
					//echo "<pre>"; print_r($data_rows); die;
						$_SESSION['login']['data'] = array();
						$_SESSION['login']['data']['rows'] 	= json_encode($data_rows);
						$_SESSION['login']['data']['total'] = $total_price;
						$_SESSION['login']['data']['total_trees'] 	= $total_trees;
						$_SESSION['login']['data']['current_tree'] 	= $current_trees;
						$_SESSION['login']['data']['all_invoice_trees'] = $all_invoice_trees;
						$_SESSION['login']['data']['all_free_trees'] 	= $all_free_trees;
						$_SESSION['login']['data']['from_initiaive_api'] = "1";       //to return through cron

						$_SESSION['login']['data']['address'] 	= $bulk_partner['address'];
						$_SESSION['login']['data']['city'] 		= $bulk_partner['city'];
						$_SESSION['login']['data']['state'] 	= $bulk_partner['state'];
						$_SESSION['login']['data']['post_code'] = $bulk_partner['post_code'];
				}
				//echo "<pre>"; print_r($_SESSION); die("here");
				$before_upload = "UPDATE `pct_partner_csv_upload_data` SET `processed` = '1' WHERE id = '" . $table_id. "' ";
				$this->db->query($before_upload);
				
				$this->confirm_intitiative();        // to add the trees against the partner
				
				$after_upload = "UPDATE `pct_partner_csv_upload_data` SET `processed` = '2' WHERE id = '" . $table_id . "' ";
				$this->db->query($after_upload);
				
				echo "Records uploaded successfully!"; 
			
		}
		
		function add_intitiative_clients_old() {
			
                $this->check_user('intitiative');
				$user_id = $_SESSION['login']['id'];
				$parter = $this->user_model->get_parter_data($user_id);
				//echo "<pre>"; print_r($parter); die;
				
                if (isset($_POST['add_initiative_clients'])) {
                        // initalize upload library
                        $config['upload_path'] = './uploads/';
                        $config['allowed_types'] = 'csv';
                        $config['max_size'] = '10000';

                        $this->load->library('upload', $config);
                        // upload file unsuccess
                        if (!$this->upload->do_upload('csv_file1')) {
                                $error = array('error' => $this->upload->display_errors());
                                $data = array('title' => 'Upload clients', 'body' => 'add_initiative_clients');
                                $this->data = array_merge($this->data, $data, $error);
                                $this->load->view('template', $this->data);
                        } else {

                                // process after uploading file successfully
                                $this->load->model('user_model');
                                $data = $this->upload->data();
                                // read csv file
                                // start processing
                                $first = true;
                                $file = fopen($data['full_path'], "r");
                                $data_rows = array();
                                $data_rows_updated = array();
                                $total_trees = 0;
                                $free_trees = 0;
                                $old_user = 0;
                                $old_rows = 0;
                                $new_rows = 0;
                                $client_trees = 0;	
								
                                while (!feof($file)) {
                                        $old_user = 0;
                                        //echo 0; die();
                                        // check if csv file is valid
                                        if ($first) {
                                                $head_title = array('company', 'first_name', 'last_name', 'email', 'tree_nums','free_trees');
                                                $data = fgetcsv($file);

                                                foreach ($data AS &$v_title) {
                                                        $v_title = strtolower($v_title);
                                                }
                                                $invalid_csv = false;

                                                foreach ($head_title AS $v) {
                                                        if (!in_array($v, $data)) {
                                                                $invalid_csv = true;
                                                        }
                                                }
                                                if (count($data) < 5 || $invalid_csv) {
                                                        show_error('Invalid csv file');
                                                }

                                                $header_mapping_array = $data;

                                                $first = false;
                                                continue;
                                        }

                                        $data = fgetcsv($file);
										
				
                                        if (is_array($data) && count($data)) {
                                                $data_row = array();
                                                foreach ($data AS $k => &$v) {
														$v = trim($v);
                                                        $data_row[$header_mapping_array[$k]] = utf8_encode($v);
                                                }
												
                                                //$data_row['bulker_id'] = $_SESSION['login']['id'];
                                                // check required fields
												$data_row['password'] = $this->randomKey(6);
                                                if (!is_numeric($data_row['tree_nums']) || !trim($data_row['password']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)
                                                ) {
                                                        continue;
                                                }
												//$data_row['referer_id'] = $this->randomKey(5);
												
												$rff_temp = str_replace(" ","_",$data_row['company']);           //referid as company name
												$data_row['referer_id'] = str_replace("’","",strtolower($rff_temp));
												
												$data_row['username'] = $data_row['email'];
												
                                                // create referer id
                                                $referer_id = trim($data_row['referer_id']);
                                                $i = 0;
                                                while (!$this->referer_check($referer_id)) {
                                                        $i++;
                                                        $referer_id = $data_row['referer_id'] . $i;
                                                }
                                                $data_row['referer_id'] = $referer_id;
												if($data_row['free_trees']==""){
													$data_row['free_trees']=0;
												}
                                                $client_trees = $data_row['tree_nums'];
												//echo "<pre>"; print_r($data_row); die;
                                                // save csv fields into database
                                                // stop
                                                //if ( !$this->email_password_check( $data_row['email'], $data_row['password'], $_SESSION['login']['data']['id'] ) ){continue;}
                                                if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['login']['id'])) {
                                                      
                                                        $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
                                                        $query = $this->db->query($qr);
                                                        if ($query->num_rows()) {
															   
                                                                $rowq = $query->row_array();
                                                                $tr = $rowq['tree_nums'];
                                                                $f_tr = $tr + $data_row['tree_nums']+$rowq['free_trees'];
                                                                
                                                                $qru = "UPDATE pct_initiative_partner SET tree_nums = '" . $f_tr . "' WHERE user_id = '" . $rowq['user_id'] . "' ";
                                                                $this->db->query($qru);
                                                        }
                                                        continue;
                                                }
                                                if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) {
                                                        $old_user = 1;
                                                         $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
													  
                                                        $query = $this->db->query($qr);
                                                        if ($query->num_rows()) {
                                                                $rowq = $query->row_array();
                                                                $tr = $rowq['tree_nums'];
                                                                $f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
                                                        }
                                                        $client_trees = $f_tr;
													 
														/* $check_em = "SELECT * FROM pct_users AS u INNER JOIN pct_referer AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";    //to get old referr_id */
														$check_em = "SELECT * FROM pct_referer WHERE user_id='" . $rowq['user_id'] . "' ";    //to get old referr_id
                                                        $getuser_id = $this->db->query($check_em);
														 if ($getuser_id->num_rows()) {
															  $ress = $getuser_id->row_array();
															  $data_row['referer_id'] = $ress['code'];
														 }
														
                                                        //continue;
                                                }
												if(isset($_SESSION['login']['id'])){
												
													$check_ini_id = "SELECT * FROM pct_initiative_partner WHERE user_id='" . $_SESSION['login']['id'] . "' ";    //to get  initiative id
                                                        $getini_id = $this->db->query($check_ini_id);
														 if ($getini_id->num_rows()) {
															  $ress1 = $getini_id->row_array();
															  $data_row['initiative_id'] = $ress1['initiative'];
														 }
												}

                                                if ($old_user == 0) {
                                                        $data_row['old_user'] = $old_user;
                                                        $data_row['client_trees'] = $client_trees;
                                                        $new_rows++;
                                                } else if ($old_user == 1) {
                                                        $data_row['old_user'] = $old_user;
                                                        $data_row['client_trees'] = $client_trees;
                                                        $old_rows++;
                                                }

                                                $data_rows[] = $data_row;
                                                $total_trees += $data_row['tree_nums'];
                                                $free_trees += $data_row['free_trees'];
                                        }
                                }
								
								/* echo "<pre>"; print_r($data_rows); die('here');
								echo "<pre>"; print_r($_SESSION);
								die('here');  */
                                $price = $_SESSION['login']['price'];
                                $bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
                                $invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
                                $current_trees = $total_trees;
                                $all_free_trees = $free_trees;
                                $all_invoice_trees = $total_trees+$free_trees;
                                //$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
                                $total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']+$bulk_partner['free_trees']; 
                                $tax = $_SESSION['login']['tax'];
                                $sub_total = $current_trees * $price / 100;
                                $total_price = $sub_total * ( 100 + $tax ) / 100;
                                $total_price = number_format($total_price, 2);
                                $data = array('title' => 'Confirmation', 'body' => 'initiative_confirmation', 'total_trees' => $total_trees, 'current_trees' => $all_invoice_trees, 'total_price' => $total_price, 'tax' => $tax, 'sub_total' => $sub_total, 'total_rows' => $new_rows, 'total_rows_updated' => $old_rows);
                                $this->data = array_merge($this->data, $data);
								
								//echo "<pre>"; print_r($data_rows); 
								if(count($data_rows) > 1){
									foreach($data_rows as $_data_row){
										if(isset($datanew[str_replace(' ','',$_data_row['company'].$_data_row['email'])])){
											
										   $datanew[str_replace(' ','',$_data_row['company'].$_data_row['email'])]['client_trees'] +=  $_data_row['client_trees'];
										   $datanew[str_replace(' ','',$_data_row['company'].$_data_row['email'])]['tree_nums'] +=  $_data_row['tree_nums'];
											
										}else{
											$datanew[str_replace(' ','',$_data_row['company'].$_data_row['email'])]= $_data_row;
										}
									}
									$data_rows    =  array_values($datanew);
								}
							   // echo "<pre>"; print_r($data_rows); die;
                                //var_dump($data_rows);
                                if (count($data_rows)) {
									//echo "<pre>"; print_r($data_rows); die;
                                        $_SESSION['login']['data'] = array();
                                        $_SESSION['login']['data']['rows'] = json_encode($data_rows);
                                        $_SESSION['login']['data']['total'] = $total_price;
                                        $_SESSION['login']['data']['total_trees'] = $total_trees;
                                        $_SESSION['login']['data']['current_tree'] = $current_trees;
                                        $_SESSION['login']['data']['all_invoice_trees'] = $all_invoice_trees;
                                        $_SESSION['login']['data']['all_free_trees'] = $all_free_trees;

                                        // expend
                                        // $_SESSION['login']['data']['code']   = $referer_id;
                                        $_SESSION['login']['data']['address'] = $bulk_partner['address'];
                                        $_SESSION['login']['data']['city'] = $bulk_partner['city'];
                                        $_SESSION['login']['data']['state'] = $bulk_partner['state'];
                                        $_SESSION['login']['data']['post_code'] = $bulk_partner['post_code'];
                                }
								//echo "<pre>"; print_r($_SESSION); die;
                                $this->load->view('template', $this->data);
                                //echo '</pre>';
                                fclose($file);
                        }
                } elseif(isset($_POST['add_grid_clients'])) {
					     //echo "<pre>"; print_r($_POST); die;
                                $first = true;
                                $data_rows = array();
								$data_row['free_trees'] = 0;
                                $data_rows_updated = array();
                                $total_trees = 0;
                                $free_trees = 0;
                                $old_user = 0;
                                $old_rows = 0;
                                $new_rows = 0;
                                $client_trees = 0;	
								
                                foreach($_POST['breakdown'] as $data){
									
									$old_user = 0;
								
									$data_row = array();
									$data_row['company'] = ($data['company']);
									$data_row['email'] = ($data['email']);
									$data_row['first_name'] = ($data['first_name']);
									$data_row['last_name'] = ($data['last_name']);
									$data_row['tree_nums'] = ($data['tree_nums']);
									
									//echo "<pre>"; print_r($data_row); die('here');
									
									$data_row['password'] = $this->randomKey(6);
									if (!is_numeric($data_row['tree_nums']) || !trim($data_row['password']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)
									) {
											continue;
									}
									//$data_row['referer_id'] = $this->randomKey(5);
									
									$rff_temp = str_replace(" ","_",$data_row['company']);           //referid as company name
									$data_row['referer_id'] = str_replace("’","",strtolower($rff_temp));
									
									$data_row['username'] = $data_row['email'];
									
									// create referer id
									$referer_id = trim($data_row['referer_id']);
									$i = 0;
									while (!$this->referer_check($referer_id)) {
											$i++;
											$referer_id = $data_row['referer_id'] . $i;
									}
									$data_row['referer_id'] = $referer_id;
									if($data_row['free_trees']==""){
										$data_row['free_trees']=0;
									}
									$client_trees = $data_row['tree_nums'];
									
									if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['login']['id'])) {
										  
											$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
											$query = $this->db->query($qr);
											if ($query->num_rows()) {
												   
													$rowq = $query->row_array();
													$tr = $rowq['tree_nums'];
													$f_tr = $tr + $data_row['tree_nums']+$rowq['free_trees'];
													
													$qru = "UPDATE pct_initiative_partner SET tree_nums = '" . $f_tr . "' WHERE user_id = '" . $rowq['user_id'] . "' ";
													$this->db->query($qru);
											}
											continue;
									}
									if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) {
											$old_user = 1;
											 $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
										  
											$query = $this->db->query($qr);
											if ($query->num_rows()) {
													$rowq = $query->row_array();
													$tr = $rowq['tree_nums'];
													$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
											}
											$client_trees = $f_tr;
											//$check_em = "SELECT * FROM pct_users AS u INNER JOIN pct_referer AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";    //to get old referr_id
											
											$check_em = "SELECT * FROM pct_referer WHERE user_id='" . $rowq['user_id'] . "' ";    //to get old referr_id
											$getuser_id = $this->db->query($check_em);
											 if ($getuser_id->num_rows()) {
												  $ress = $getuser_id->row_array();
												  $data_row['referer_id'] = $ress['code'];
											 }
											
											//continue;
									}
									if(isset($_SESSION['login']['id'])){
									
										$check_ini_id = "SELECT * FROM pct_initiative_partner WHERE user_id='" . $_SESSION['login']['id'] . "' ";    //to get  initiative id
											$getini_id = $this->db->query($check_ini_id);
											 if ($getini_id->num_rows()) {
												  $ress1 = $getini_id->row_array();
												  $data_row['initiative_id'] = $ress1['initiative'];
											 }
									}
									if ($old_user == 0) {
											$data_row['old_user'] = $old_user;
											$data_row['client_trees'] = $client_trees;
											$new_rows++;
									} else if ($old_user == 1) {
											$data_row['old_user'] = $old_user;
											$data_row['client_trees'] = $client_trees;
											$old_rows++;
									}

									$data_rows[] = $data_row;
									$total_trees += $data_row['tree_nums'];
									$free_trees += $data_row['free_trees'];
                                }
								
								/* echo "<pre>"; print_r($data_rows); die('here');  */
                                $price = $_SESSION['login']['price'];
                                $bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
                                $invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
                                $current_trees = $total_trees;
                                $all_free_trees = $free_trees;
                                $all_invoice_trees = $total_trees+$free_trees;
                                //$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
                                $total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']+$bulk_partner['free_trees']; 
                                $tax = $_SESSION['login']['tax'];
                                $sub_total = $current_trees * $price / 100;
                                $total_price = $sub_total * ( 100 + $tax ) / 100;
                                $total_price = number_format($total_price, 2);
								
                                //var_dump($data_rows);
                                if (count($data_rows)) {
									//echo "<pre>"; print_r($data_rows); die;
                                        $_SESSION['login']['data'] = array();
                                        $_SESSION['login']['data']['rows'] = json_encode($data_rows);
                                        $_SESSION['login']['data']['total'] = $total_price;
                                        $_SESSION['login']['data']['total_trees'] = $total_trees;
                                        $_SESSION['login']['data']['current_tree'] = $current_trees;
                                        $_SESSION['login']['data']['all_invoice_trees'] = $all_invoice_trees;
                                        $_SESSION['login']['data']['all_free_trees'] = $all_free_trees;

                                        // expend
                                        // $_SESSION['login']['data']['code']   = $referer_id;
                                        $_SESSION['login']['data']['address'] = $bulk_partner['address'];
                                        $_SESSION['login']['data']['city'] = $bulk_partner['city'];
                                        $_SESSION['login']['data']['state'] = $bulk_partner['state'];
                                        $_SESSION['login']['data']['post_code'] = $bulk_partner['post_code'];
                                }
								//echo "<pre>"; print_r($_SESSION); die("here");
								$this->confirm_intitiative();        // to add the trees against the partner

                } elseif($parter['grid_upload']=='1') {

						$clients['client_data'] = $this->user_model->get_last_uploaded_clients($_SESSION['login']['id']);
						//echo "<pre>"; print_r($clients); die;
						$data = array('title' => 'Upload clients', 'body' => 'add_grid_initiative_clients');
                        $this->data = array_merge($this->data, $data,$clients);
                        $this->load->view('template', $this->data);
					
				}else{
					
                        $data = array('title' => 'Upload clients', 'body' => 'add_initiative_clients');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }
				
		/***********************************************************
		Function: To upload autoupload clients using cron  
		***********************************************************/
		
		function autoupload_sonne_energeticus_clients_cron($partner_id = Null){
			
				session_start();
				if($partner_id==Null){  show_error('Pass partner ID', 500 ); }
				
				$clients = $this->user_model->get_last_uploaded_clients($partner_id);
				
				if(empty($clients)){  show_error('No records found!', 500 ); }
				
				$sql = "SELECT * FROM pct_users  WHERE id='" . $partner_id . "'";               //Pass initiative partner ID
				$partner_login = $this->db->query($sql)->row();
				$_SESSION['login'] = (array)$partner_login;  
				
				$first = true;
				$data_rows = array();
				$data_row['free_trees'] = 0;
				$data_rows_updated = array();
				$total_trees = $free_trees = $old_user = $new_rows = $old_rows = $client_trees = 0;
				//echo "<pre>"; print_r($clients); die("here");
				foreach($clients as $data){
					
						$data = (array)$data;
						$old_user = 0;
						$data_row = array();
						$data_row['company'] 	= ($data['company']);
						$data_row['email'] 		= ($data['email']);
						$data_row['first_name'] = ($data['first_name']);
						$data_row['last_name'] 	= ($data['last_name']);
						$data_row['tree_nums'] 	= ($data['tree_nums']);
						
						//echo "<pre>"; print_r($data_row); die('here');
						
						$data_row['password'] = $this->randomKey(6);
						if (!is_numeric($data_row['tree_nums']) || !trim($data_row['password']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)
						) {
								continue;
						}
						//$data_row['referer_id'] = $this->randomKey(5);
						
						$rff_temp = str_replace(" ","_",$data_row['company']);           //referid as company name
						$data_row['referer_id'] = str_replace("’","",strtolower($rff_temp));
						
						$data_row['username'] = $data_row['email'];
						
						// create referer id
						$referer_id = trim($data_row['referer_id']);
						$i = 0;
						while (!$this->referer_check($referer_id)) {
								$i++;
								$referer_id = $data_row['referer_id'] . $i;
						}
						$data_row['referer_id'] = $referer_id;
						if($data_row['free_trees']==""){
							$data_row['free_trees']=0;
						}
						$client_trees = $data_row['tree_nums'];
						
						if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['login']['id'])) {
							  
								$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
								$query = $this->db->query($qr);
								if ($query->num_rows()) {
									   
										$rowq = $query->row_array();
										$tr = $rowq['tree_nums'];
										$f_tr = $tr + $data_row['tree_nums']+$rowq['free_trees'];
										
										$qru = "UPDATE pct_initiative_partner SET tree_nums = '" . $f_tr . "' WHERE user_id = '" . $rowq['user_id'] . "' ";
										$this->db->query($qru);
								}
								continue;
						}
						if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) {
								$old_user = 1;
								 $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client' AND bp.company='" . $data_row['company'] . "'";
							  
								$query = $this->db->query($qr);
								if ($query->num_rows()) {
										$rowq = $query->row_array();
										$tr = $rowq['tree_nums'];
										$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
								}
								$client_trees = $f_tr;
								//$check_em = "SELECT * FROM pct_users AS u INNER JOIN pct_referer AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";    //to get old referr_id
								
								$check_em = "SELECT * FROM pct_referer WHERE user_id='" . $rowq['user_id'] . "' ";    //to get old referr_id
								$getuser_id = $this->db->query($check_em);
								 if ($getuser_id->num_rows()) {
									  $ress = $getuser_id->row_array();
									  $data_row['referer_id'] = $ress['code'];
								 }
								
								//continue;
						}
						if(isset($_SESSION['login']['id'])){
						
							$check_ini_id = "SELECT * FROM pct_initiative_partner WHERE user_id='" . $_SESSION['login']['id'] . "' ";    //to get  initiative id
								$getini_id = $this->db->query($check_ini_id);
								 if ($getini_id->num_rows()) {
									  $ress1 = $getini_id->row_array();
									  $data_row['initiative_id'] = $ress1['initiative'];
								 }
						}
						if ($old_user == 0) {
								$data_row['old_user'] = $old_user;
								$data_row['client_trees'] = $client_trees;
								$new_rows++;
						} else if ($old_user == 1) {
								$data_row['old_user'] = $old_user;
								$data_row['client_trees'] = $client_trees;
								$old_rows++;
						}

						$data_rows[] = $data_row;
						$total_trees += $data_row['tree_nums'];
						$free_trees += $data_row['free_trees'];
				}
				
				/* echo "<pre>"; print_r($data_rows); die('here');  */
				$price = $_SESSION['login']['price'];
				$bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
				$invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
				$current_trees = $total_trees;
				$all_free_trees = $free_trees;
				$all_invoice_trees = $total_trees+$free_trees;
				//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
				$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']+$bulk_partner['free_trees']; 
				$tax = $_SESSION['login']['tax'];
				$sub_total = $current_trees * $price / 100;
				$total_price = $sub_total * ( 100 + $tax ) / 100;
				$total_price = number_format($total_price, 2);
				
				//var_dump($data_rows);
				if (count($data_rows)) {
					//echo "<pre>"; print_r($data_rows); die;
						$_SESSION['login']['data'] = array();
						$_SESSION['login']['data']['rows'] 			= json_encode($data_rows);
						$_SESSION['login']['data']['total'] 		= $total_price;
						$_SESSION['login']['data']['total_trees'] 	= $total_trees;
						$_SESSION['login']['data']['current_tree'] 	= $current_trees;
						$_SESSION['login']['data']['all_invoice_trees'] = $all_invoice_trees;
						$_SESSION['login']['data']['all_free_trees'] = $all_free_trees;

						// expend
						// $_SESSION['login']['data']['code']   = $referer_id;
						$_SESSION['login']['data']['address'] 	= $bulk_partner['address'];
						$_SESSION['login']['data']['city'] 		= $bulk_partner['city'];
						$_SESSION['login']['data']['state'] 	= $bulk_partner['state'];
						$_SESSION['login']['data']['post_code'] = $bulk_partner['post_code'];
						$_SESSION['login']['data']['from_initiaive_api'] = "1";
				}
				//echo "<pre>"; print_r($_SESSION); die("here");
				$this->confirm_intitiative();        // to add the trees against the partner
				echo "Records Uploaded Successfully"; 
			
		}
		
        function add_clients() {
                $this->check_user('bulk_partner');
                if (isset($_POST['add_clients'])) {
                        // initalize upload library
                        $config['upload_path'] = './uploads/';
                        $config['allowed_types'] = 'csv';
                        $config['max_size'] = '10000';

                        $this->load->library('upload', $config);
                        // upload file unsuccess
                        if (!$this->upload->do_upload('csv_file1')) {
                                $error = array('error' => $this->upload->display_errors());
                                $data = array('title' => 'Upload clients', 'body' => 'add_clients');
                                $this->data = array_merge($this->data, $data, $error);
                                $this->load->view('template', $this->data);
                        } else {

                                // process after uploading file successfully
                                $this->load->model('user_model');
                                $data = $this->upload->data();
                                // read csv file
                                // start processing
                                $first = true;
                                $file = fopen($data['full_path'], "r");
                                $data_rows = array();
                                $data_rows_updated = array();
                                $total_trees = 0;
                                $old_user = 0;
                                $old_rows = 0;
                                $new_rows = 0;
                                $client_trees = 0;
                                while (!feof($file)) {
                                        $old_user = 0;
                                        //echo 0; die();
                                        // check if csv file is valid
                                        if ($first) {
                                                $head_title = array('company', 'first_name', 'last_name', 'email', 'username', 'password', 'website', 'tree_nums', 'referer_id');
                                                $data = fgetcsv($file);

                                                foreach ($data AS &$v_title) {
                                                        $v_title = strtolower($v_title);
                                                }
                                                $invalid_csv = false;

                                                foreach ($head_title AS $v) {
                                                        if (!in_array($v, $data)) {
                                                                $invalid_csv = true;
                                                        }
                                                }
                                                if (count($data) < 9 || $invalid_csv) {
                                                        show_error('Invalid csv file');
                                                }

                                                $header_mapping_array = $data;

                                                $first = false;
                                                continue;
                                        }

                                        $data = fgetcsv($file);
                                        if (is_array($data) && count($data)) {
                                                $data_row = array();
                                                foreach ($data AS $k => &$v) {
														$v = trim($v);
                                                        $data_row[$header_mapping_array[$k]] = utf8_encode($v);
                                                }
                                                //$data_row['bulker_id'] = $_SESSION['login']['id'];
                                                // check required fields
                                                if (!is_numeric($data_row['tree_nums']) || !trim($data_row['password']) || !trim($data_row['username']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)
                                                ) {
                                                        continue;
                                                }

                                                // create referer id
                                                $referer_id = trim($data_row['referer_id']);
                                                $i = 0;
                                                while (!$this->referer_check($referer_id)) {
                                                        $i++;
                                                        $referer_id = $data_row['referer_id'] . $i;
                                                        $referer_ids = $data_row['referer_id'];
                                                }
                                                $data_row['referer_id'] = $referer_id;
                                               $data_row['referer_ids'] = $referer_ids ;
                                                $client_trees = $data_row['tree_nums'];
                                                // save csv fields into database
                                                // stop
                                                //if ( !$this->email_password_check( $data_row['email'], $data_row['password'], $_SESSION['login']['data']['id'] ) ){continue;}
                                                if (!$this->email_check($data_row['email'], 'client', $_SESSION['login']['id'])) {

                                                        $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_bulk_partners AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
                                                        $query = $this->db->query($qr);
                                                        if ($query->num_rows()) {
                                                                $rowq = $query->row_array();
                                                                $tr = $rowq['tree_nums'];
                                                                $f_tr = $tr + $data_row['tree_nums'];

                                                                $qru = "UPDATE pct_bulk_partners SET tree_nums = '" . $f_tr . "' WHERE user_id = '" . $rowq['user_id'] . "' ";
                                                                $this->db->query($qru);
                                                        }
                                                        continue;
                                                }
                                                if (!$this->username_check($data_row['username'])) {
                                                        $old_user = 1;
                                                        $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_bulk_partners AS bp ON u.id=bp.user_id WHERE u.username='" . $data_row['username'] . "' ";
                                                        $query = $this->db->query($qr);
                                                        if ($query->num_rows()) {
                                                                $rowq = $query->row_array();
                                                                $tr = $rowq['tree_nums'];
                                                                $f_tr = $tr + $data_row['tree_nums'];
                                                        }
                                                        $client_trees = $f_tr;
                                                        //continue;
                                                }
                                                if ($old_user == 0) {
                                                        $data_row['old_user'] = $old_user;
                                                        $data_row['client_trees'] = $client_trees;
                                                        $new_rows++;
                                                } else if ($old_user == 1) {
                                                        $data_row['old_user'] = $old_user;
                                                        $data_row['client_trees'] = $client_trees;
                                                        $old_rows++;
                                                }

                                                $data_rows[] = $data_row;
                                                $total_trees += $data_row['tree_nums']+$data_row['free_trees'];
                                        }
                                }

                                $price = $_SESSION['login']['price'];
                                $bulk_partner = $this->user_model->load_bulk_partner($_SESSION['login']['id']);
                                $invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
                                $current_trees = $total_trees;
                                //$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
                                $total_trees = $total_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2'];
                                $tax = $_SESSION['login']['tax'];
                                $sub_total = $current_trees * $price / 100;
                                $total_price = $sub_total * ( 100 + $tax ) / 100;
                                $total_price = number_format($total_price, 2);
                                $data = array('title' => 'Confirmation', 'body' => 'confirmation', 'total_trees' => $total_trees, 'current_trees' => $current_trees, 'total_price' => $total_price, 'tax' => $tax, 'sub_total' => $sub_total, 'total_rows' => $new_rows, 'total_rows_updated' => $old_rows);
                                $this->data = array_merge($this->data, $data);
                                //var_dump($data_rows);
								
                                if (count($data_rows)) {
                                        $_SESSION['login']['data'] = array();
                                        $_SESSION['login']['data']['rows'] = json_encode($data_rows);
                                        $_SESSION['login']['data']['total'] = $total_price;
                                        $_SESSION['login']['data']['total_trees'] = $total_trees;
                                        $_SESSION['login']['data']['current_tree'] = $current_trees;

                                        // expend
                                        // $_SESSION['login']['data']['code']   = $referer_id;
                                        $_SESSION['login']['data']['address'] = $bulk_partner['address'];
                                        $_SESSION['login']['data']['city'] = $bulk_partner['city'];
                                        $_SESSION['login']['data']['state'] = $bulk_partner['state'];
                                        $_SESSION['login']['data']['post_code'] = $bulk_partner['post_code'];
                                }
                                $this->load->view('template', $this->data);
                                //echo '</pre>';
                                fclose($file);
                        }
                } else {
                        $data = array('title' => 'Upload clients', 'body' => 'add_clients');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }

        function cancel() {
                $this->check_user('bulk_partner');
                unset($_SESSION['login']['data']);
                redirect(site_url());
        }
	 	   
		   
		// confirm tree number for bulk partners
        function confirm_intitiative() {
				
                $this->check_user('intitiative');
                if (!isset($_SESSION['login']['data']) || empty($_SESSION['login']['data']['rows'])) {
                        unset($_SESSION['login']['data']);
                        redirect(site_url());
                        die();
                }

                $data = json_decode($_SESSION['login']['data']['rows']);
				//echo "<pre>"; print_r($data); die('here');
				if($_SESSION['login']['data']['from_tpcc_api']==1){
					$redirect = 1;
				}elseif($_SESSION['login']['data']['from_initiaive_api']==1){
					$redirect = 2;                //from_initiaive_api
				}else{
					$redirect = 0;
				}
                // $total = $_SESSION['login']['data']['total'];
				
                $this->load->database();
                $this->load->model('user_model');
                $bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
				
				//echo "<pre>"; print_r($bulk_partner); die;
               // $initiative = $bulk_partner['initiative'];
				//$opportunity_str = $bulk_partner['opportunity_str'];
				
                $initiative 		=  $bulk_partner['initiative_name'];
                $initiative_id 		=  $bulk_partner['initiative'];
                $certificate_layout =  $bulk_partner['certificate_layout'];
                $activation_email 	=  $bulk_partner['activation_email'];
                $send_invoices 		=  $bulk_partner['send_invoices'];
                $invoice_free_trial =  $bulk_partner['free_trial'];
                $request_for_funds 	=  $bulk_partner['request_for_funds'];
                $attach_cert_inemails =  $bulk_partner['attach_cert_inemails'];
                $counter_type 		=  $bulk_partner['counter_type'];                           // check if partner counter is selected
                $ini_counter_type 	=  $bulk_partner['ini_counter_type'];					 // check if initiative counter is selected
				$bk_image 			=  $bulk_partner['bk_image'];
				$partner_bk_image 	=  $bulk_partner['partner_bk_image'];
				
				if($partner_bk_image != ""){                                   				//check partner background cert image first
					$bk_image 	=  $bulk_partner['partner_bk_image'];
				}else{
					$bk_image 	=  $bulk_partner['bk_image'];
				}
			
				//echo "<pre>"; print_r($bulk_partner); die('here');
                $opportunity_str = '';
               
				$_SESSION['figure_history_key'] = $this->randomKey(10);
                $config = array();
				$trial_trees = 0; 
                $current_month_text = date("F_Y");
				$initiative_clients = array();
				$count_rows = 0;
                foreach ($data AS &$row) {
                        // store client into database
                        $row = (array) $row;
						if($redirect==1){
						    $unique_url =  $bulk_partner['website_url'].'/'. $row['short_code'];
						  }else{
							$unique_url =  $bulk_partner['website_url'].'/?refid=' . $row['referer_id'];
						  }
                        // create the ticker
                       // $this->create_ticker($row['tree_nums'], $row['referer_id']);
					     $total_client_trees = $row['client_trees']+$row['free_trees'];
	
                        //$this->create_ticker($total_client_trees, $row['referer_id']);        		//update this to update trees in logo
						
						if($counter_type != 0){															// if counter is set on partner
							if($counter_type==1){
								 $this->create_ticker($total_client_trees, $row['referer_id']);            //GEA tickers
							}elseif($counter_type==2){
								$this->create_cfd_ticker($total_client_trees, $row['referer_id']);        //CFD tickers
							}elseif($counter_type==3){
								$this->create_non_partner_ticker($total_client_trees, $row['referer_id']);        //Non partner tickers
							}
							
						}else{                                                                              // if counter is set on initiative
							if($ini_counter_type==1){
								 $this->create_ticker($total_client_trees, $row['referer_id']);            //GEA tickers
							}elseif($ini_counter_type==2){
								$this->create_cfd_ticker($total_client_trees, $row['referer_id']);        //CFD tickers
							}elseif($ini_counter_type==3){
								$this->create_non_partner_ticker($total_client_trees, $row['referer_id']);        //Non partner tickers
							}
						}
						/* disable these fucntion to decrease the uploading time */
						//$this->create_cfd_small_ticker($total_client_trees, $row['referer_id'], 'black');
						
						//$this->create_cfd_small_ticker($total_client_trees, $row['referer_id'], 'green');
						
						//$this->create_cfd_small_ticker($total_client_trees, $row['referer_id'], 'white');
						
						
                        $ticker_url = base_url() . 'tickers/ticker_' . $row['referer_id'] . '.png';
                        $cert_thumb_url = base_url() . 'cert/thumb_' . $row['referer_id'] . '.jpg';
                        $cert_image_url = base_url() . 'cert/full_' . $row['referer_id'] . '.jpg';
						$row['activation_key'] = $this->randomKey(6);
                        $activation_url = base_url() . 'index.php/user/client_login/' . $row['activation_key'];
                        $auto_login_link = base_url() . 'index.php/user/auto_login_client_link/' . $row['activation_key'];
						 if ($row['old_user'] == 0) {
								//$email_template = $this->user_model->load_initiative_email_template($initiative_id,3);  //send email for new clients
								
								if($activation_email==1){
									$email_template = $this->user_model->load_initiative_email_template($initiative_id,2);  // send activation email for new clients
								}else{
									$email_template = $this->user_model->load_initiative_email_template($initiative_id,3); 
									//$email_template = $this->user_model->load_initiative_email_template($initiative_id,2);  //changes for None CFD
								}
								
						  }else{
								$email_template = $this->user_model->load_initiative_email_template($initiative_id,5);     //send email for update clients
						  } 
						  
                        // create the certificate
                        $restaurant = $row['company'];
                        $total_trees = $row['tree_nums']+$row['free_trees'];
                        $client_trees = $row['client_trees']+$row['free_trees'];
						
						/* if($redirect==1){                  //update trees to pct_sublevel_initiative
								$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $row['email'] . "' ";
								$query = $this->db->query($qr);
								if ($query->num_rows()) {
										$rowq = $query->row_array();
										$tr = $rowq['tree_nums'];
										$total_client_trees = $tr + $row['tree_nums']+$row['free_trees'];
										
										$qru = "UPDATE pct_sublevel_initiative SET tree_nums = '" . $total_client_trees . "' WHERE email = '" . $row['email'] . "' ";
										$this->db->query($qru);
								}
						}	 */

                        $calculated_cups = $total_client_trees*1000;
		
							if($client_trees=='1'){
								 $client_trees = number_format($total_trees).' '.'tree';
							}else{
								$client_trees = number_format($total_trees).' '.'trees';
							}
						//$bk_image =  $bulk_partner['bk_image'];
						
                        if ($row['old_user'] == 0) {
							//echo "here"; die;
								 $row['activation_email'] = $activation_email;
								if($redirect==1){
								 $row['auto_login_link'] = 1;
								}
								$row['figure_history_key'] = $_SESSION['figure_history_key'];
                                $client_id = $this->user_model->insert_initiative_client($row);
                        } else {
								//echo "there"; die;
								$row['figure_history_key'] = $_SESSION['figure_history_key'];
                                $client_id = $this->user_model->update_initiative_client($row);
                        }
						$initiative_clients[$count_rows]['partner_id'] = $_SESSION['login']['id'];
						$initiative_clients[$count_rows]['tree_nums'] =  $row['tree_nums'];
						$initiative_clients[$count_rows]['user_id'] = $client_id;
						
						//to add client certificate url
						$client_certificate_url = 'https://www.greenearthappeal.org/panacea/uploads/certs/clients/'.'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $client_id . '.pdf'; 
																								
                        $message = $email_template->message;
						//$total_client_trees = number_format($total_client_trees);
						
						if($total_client_trees=='1'){
							 $total_client_trees = number_format($total_client_trees).' '.'tree';
						}else{
							$total_client_trees = number_format($total_client_trees).' '.'trees';
						}
						$all_counters = array();
						$all_counters['gea_counter'] = "";
						$all_counters['cfd_counter'] = "";
						//$all_counters 	= $this->get_cfd_gea_counters();   // disable this function to decrease the loading time
						
                        $message = str_ireplace(array('[contact_person]','[activation_url]','[auto_login_link]','[company_name]','[total_trees]','[current_trees]', '[username]', '[password]', '[unique_url]', '[ticker_url]','[initiative_name]','[calculated_cups]','[cert_thumb_url]','[cert_image_url]','[certificate_url]','[cfd_counter]','[gea_counter]'), array($row['first_name'], $activation_url,$auto_login_link, $row['company'],$total_client_trees, $total_trees, $row['username'], $row['password'], $unique_url, $ticker_url,$initiative,$calculated_cups,$cert_thumb_url,$cert_image_url,$client_certificate_url,$all_counters['cfd_counter'],$all_counters['gea_counter']), $message);

					
						$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $total_client_trees, $initiative,$calculated_cups,$total_client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate1']));
						$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $total_client_trees, $initiative,$calculated_cups,$total_client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate2']));
						$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $total_client_trees, $initiative,$calculated_cups,$total_client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate3']));
						$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $total_client_trees, $initiative,$calculated_cups,$total_client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate4']));

						
                        //$rest_logo      = $_SESSION['login']['data']['logo'];
                        $ds = DIRECTORY_SEPARATOR;
                        /* $pct_logo_url = '';
                                if (file_exists( dirname(__FILE__).'/../../uploads/restaurant/'.$rest_logo ) ) {
                                $pct_logo = 'uploads/restaurant/'.$rest_logo;
                                $pct_logo_url = '<img src="'.$pct_logo.'" title="logo" />';
                                } */

                        $certificate_sub = $certificate_msg = $certificate_text = '';

                        $certificate_sub = preg_replace("@[\t]@", '', $bulk_partner['subject']);
                        $certificate_cc_email = preg_replace("@[\t]@", '', $bulk_partner['cc_email']);
                        $certificate_msg = preg_replace("@[\t]@", '', $bulk_partner['message']);
                        $certificate_text = preg_replace("@[\t]@", '', $bulk_partner['certificate_text']);
                        $certificate_text = stripslashes($certificate_text);
                        $certificate_text = str_ireplace(array('[name]', '[total_trees]', '[initiative]', '[opportunity_str]'), array($restaurant, $total_trees, $initiative, $opportunity_str), $certificate_text);

                        //$cert_name = 'certificate_'.$invoice_id.'.pdf';
                        // $prev_month_text = date("F_Y", strtotime("first day of previous month") );
                        $cert_name = 'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $client_id . '.pdf';
						
					
						 if ($row['old_user'] == 0) {
							 
							 $row['initiative_name']  = $bulk_partner['initiative_name'];
							 $row['unique_url']       =  $unique_url;
							 $row['ticker_url'] 	  = $ticker_url;
							 $row['cert_name'] 	  	  = $cert_name;
							 $row['email_title']      = $bulk_partner['sender_email_title'];
								//insert all client data in "pct_client_activation_emaildata" table for email
							$this->user_model->insert_initiative_client_email_records($row,$client_id);
							
						}
						
                        $cert_path = dirname(__FILE__) . '/../../uploads/certs/clients/';
						
						
						if($certificate_layout == '1'){
							require('libraries/cert-ini.php');
						}else{
							$cert_client_name = $row['first_name'].' '.$row['last_name'];
							require('libraries/cert-ini-layout2.php');
						}

						$image_file_name = date('Ymdhis');

						$image_path = dirname(__FILE__) . '/../../uploads/certs/clients/images/';
						$image_url = dirname(__FILE__) . '/../../cert/';
						$pdf_path = $cert_path.$cert_name;
						if (extension_loaded('imagick')){   //extension imagick is installed
							$this->create_pdf_to_image($pdf_path, $image_file_name, $image_path); 
						}	
						if (extension_loaded('imagick')){   		//extension imagick is installed
							$this->create_cert_pdf_to_image($pdf_path, $row['referer_id'], $image_url); //to create pef images for clients
						}							
						//echo "<pre>"; print_r($row); die;
						$data3 = array();                       //store clients latest updated certificate
						$data3['client_id'] = $client_id;
						$data3['latest_certificate'] = $cert_name;
						$data3['certificate_image'] = 'certificate_'.$image_file_name.'.jpg';
						
						$this->user_model->insert_client_latest_certificate($data3);
						
                        // send email to client
                        $config['to'] =  $row['email'];
						
                       // $config['to'] =  'testing.whizkraft1@gmail.com';
                       
                        if (isset($certificate_sub) && !empty($certificate_sub)) { 
                                $config['subject'] = $certificate_sub;
                        } else {
                                $config['subject'] = $email_template->subject;
                        }

                        $config['subject'] = str_ireplace(array('[contact_person]', '[company_name]','[initiative_name]','[total_trees]'), array($cname, $restaurant,$initiative,$total_trees), $config['subject']);
										
                        if (isset($certificate_msg) && !empty($certificate_msg)) {
                                $config['message'] = $certificate_msg;
                        } else {
                                $config['message'] = $message;
                        }
						if($attach_cert_inemails==1){              //to stop attach client certificate if option in disabled
							$attach_cert = array(dirname(__FILE__) . '/../../uploads/certs/clients/' . $cert_name);
						}else{
							$attach_cert = "";
						}
						
                        if ($row['old_user'] == 0) {
								if($activation_email==1){
									$config['attach'] = "";
								}else{
									$config['attach'] = $attach_cert;
								}
						}else{
							$config['attach'] = $attach_cert;
						}
                        // stop send_mail
					    $config['email_title'] = $bulk_partner['sender_email_title'];
						
						$enable_email = $email_template->enable_email;
						
						if($enable_email == 1){
							$this->send_mail($config);   //only send email if it is enabled in initiative email templates
						}
	
                        // a copy of email to the cc email id
                        if (isset($certificate_cc_email) && !empty($certificate_cc_email)) {
                                $config2 = array();
                                $config2['to'] = $certificate_cc_email;

                                if (isset($certificate_sub) && !empty($certificate_sub)) {
                                        $config2['subject'] = $certificate_sub;
                                } else {
                                        $config2['subject'] = $email_template->subject;
                                }

                                if (isset($certificate_msg) && !empty($certificate_msg)) {
                                        $config2['message'] = $certificate_msg;
                                } else {
                                        $config2['message'] = $message;
                                }

                                $config2['attach'] = array(dirname(__FILE__) . '/../../uploads/certs/clients/' . $cert_name);

                                //$this->send_mail($config2);
                                // stop send_mail
                        }
						
						//insert record for send monthly invoice
						if($send_invoices=="monthly"){
							$record['tree_nums']      = $row['tree_nums'];
							$record['company']   = $row['company'];
							$record['free_trees']     = $row['free_trees'];
							$record['initiative_id']  = $row['initiative_id'];
							if($request_for_funds == 1)
							{
							  $record['request_for_funds']  = '1';
							}
							$record['partner_id']      = $_SESSION['login']['id'];
							$insert_monthly = $this->user_model->insert_initiative_monthly_invoice_records($record);  
						}
						if ($row['old_user'] == 0) {
							   $trial_trees+= $row['tree_nums'];         //get sum on all new user trees
						 }
						 $count_rows++;
                }
                // prepare to create the invoice
                $cert_path = '';
                $pct_rows = &$data;
				
				/* Insert initiatives clients */
				if(!empty($initiative_clients)){
					$part_id = $_SESSION['login']['id'];
					$this->db->query("UPDATE pct_initiative_clients SET current='0' WHERE partner_id='$part_id'");

					foreach($initiative_clients as $in_client){
						$in_clients['partner_id']      		 = $in_client['partner_id'];
						$in_clients['user_id']      		 = $in_client['user_id'];
						$in_clients['tree_nums']      		 = $in_client['tree_nums'];
						$in_clients['current']      		 = '1';
						$this->db->insert_batch('pct_initiative_clients', array($in_clients));
					}
				}
				
                $item2 = array();
				
                $bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
				$email_template = $this->user_model->load_initiative_email_template($bulk_partner['initiative'],4);      

                $item2['name'] = "Total number of trees";
				//echo "<pre>"; print_r($_SESSION['login']['data']); die;
                $item2['free_trees'] = $_SESSION['login']['data']['all_free_trees'];
				$item2['unit'] = $_SESSION['login']['price'] / 100;
				if($invoice_free_trial=='1'){        //if free trial send 0 invoice for new clients
					$item2['tree_nums'] = $_SESSION['login']['data']['current_tree'] - $trial_trees;
				}else{
				   $item2['tree_nums'] = $_SESSION['login']['data']['current_tree'];
				}
				
                switch ($_SESSION['login']['currency']) {
                        case 1:
                                $item2['currency'] = '$';
                                break;
                        case 2:
                                $item2['currency'] = '&pound;';
                                break;
						case 3:
								$item2['currency'] = '€';
								break;
                }

                $item2['price'] = $item2['tree_nums'] * $item2['unit'];
                $tax = $_SESSION['login']['tax'];
               // $sub_total = $currency . $item2['price'];
                $sub_total = $item2['currency'] . $item2['price'];
                //$total_no = number_format($item2['price'] * (100 + $tax)/100, 2);
                $total_no = $item2['price'] * (100 + $tax) / 100;
                $total = $item2['currency'] . number_format($total_no, 2);
                $total_amt = $item2['currency'] . number_format($total_no, 2);

                // store invoice in database
                $data1 = array();
                $data1['user_id'] = $_SESSION['login']['id'];
                $data1['date_created'] = 'NOW()';
                $data1['total'] = $total_no;
                $data1['state'] = 0;
               // $data1['trees'] = $_SESSION['login']['data']['current_tree'];
			    $data1['figure_history_key'] = $_SESSION['figure_history_key'];
			   
                $data1['trees'] = $_SESSION['login']['data']['all_invoice_trees'];
				$total_planted_trees = $_SESSION['login']['data']['all_invoice_trees'];
				if($send_invoices=="onupload"){
					$invoice_id = $this->user_model->insert_initiative_invoice($data1);
				}else{
					$invoice_id = 'partner_'.$bulk_partner['id'];
				}
				
				

                // expend
                $invoice_company = $cert_company = $bulk_partner['company'];
                $address = $_SESSION['login']['data']['address'];
                $city = $_SESSION['login']['data']['city'];
                $state = $_SESSION['login']['data']['state'];
                $post_code = $_SESSION['login']['data']['post_code'];
                $ini_first_name = $bulk_partner['first_name'];

                // create the invoice
                // $invoice_name = 'invoice_'.$invoice_id.'.pdf';
                // $prev_month_text = date("F_Y", strtotime("first day of previous month") );
                $current_month_text = date("F_Y");
                $invoice_name = 'The_Green_Earth_Appeal_Invoice_' . $current_month_text . '_' . $invoice_id . '.pdf';

                $cert_name = 'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $invoice_id . '.pdf';
                // update the invoice
                // $invoice_number = date('dm').$invoice_id;
                $invoice_number = $invoice_id;
                $invoice_data = array();
                $invoice_data['id'] = $invoice_id;
                $invoice_data['number'] = $invoice_number;
                $invoice_data['name'] = $invoice_name;

                // this is only for bulkpartner, so we can load previous figure month
                $invoice_data['latest_certificate'] = $cert_name;
                $invoice_data['month'] = date('mY');
				
				$monthly['invoice_id']    	= $invoice_id;
				$monthly['first_name']    	= $bulk_partner['first_name'];
				$monthly['company_name']  	= $bulk_partner['company'];
				$monthly['total_trees'] 	= $_SESSION['login']['data']['total_trees'];
				$monthly['current_trees'] 	= $_SESSION['login']['data']['all_invoice_trees'];
				$monthly['total']			= $total_amt;
				$monthly['ticker_url'] 		= "";
				$monthly['unique_url'] 		= "";
				$monthly['initiative_name'] = $initiative;
				/* if($send_invoices=="monthly"){
					$insert_monthly_invoice_id = $this->user_model->insert_initiative_monthly_invoice_data($monthly);  //insert record for send monthly invoice
				} */
				if($send_invoices=="onupload"){
					$this->db->update_batch('pct_invoices', array($invoice_data), 'id');
				}
				if($send_invoices=="onupload" && $request_for_funds==1){
					
					require_once('libraries/gocardless/vendor/autoload.php');	
						 //$token = "sandbox_ZH42k6x0Ee7-owAISf_-HLlRsbeWQBc07NwUNN2Z";  //sandbox token
						 
						$new_gocardless_enable_date  = strtotime("2020-01-20 11:00:00");
						$user_register_date  		 = strtotime($bulk_partner['user_register_date']);

						if($user_register_date > $new_gocardless_enable_date){
							$token = "live_icu2SM1QdA7w2GTGJF1mKV58dU00O0VM58y-XVAb";      //New user
							$_SESSION['gocardless_user'] = "new";
						}else{
							$token = "live_roMMEqbGeKFuN_sHAyfqYGLOFsMvmEDpVeP5Ltec";      //Old user
							$_SESSION['gocardless_user'] = "old";
						}
						 
						 $client = new \GoCardlessPro\Client([
									   'access_token' => $token,
										'environment' => \GoCardlessPro\Environment::LIVE  //LIVE/SANDBOX
									]);
							/* // create connection with CFD */
							$db_name = "carbonfd_cfddb";
							$db_host = "localhost";
							$db_user = "carbonfd_cfddbus";
							$db_pass = "vVZKUCXr@wAU";
							
							$con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
							// Check connection
							if (mysqli_connect_errno())
							  {
							  echo "Failed to connect to MySQL: " . mysqli_connect_error();
							  }
							  
							 
							 $dashboard_user_email = $bulk_partner['dashboard_user_email'];
							 
							if($dashboard_user_email != ""){
								$user_email = $dashboard_user_email;
							}else{
								$user_email = $bulk_partner['email'];
							}
							  
						 	$cfwp_sql = "SELECT * from `cfwp_users` AS u INNER JOIN cfwp_ls_partners_details AS pd ON u.ID=pd.user_id where u.user_email='".$user_email."' AND pd.cancelled='0'";
						
							$cfwp_result = mysqli_query($con,$cfwp_sql);
							
							$mendate_id = "";
							if(mysqli_num_rows($cfwp_result) > 0) {   

								$cfd_row = mysqli_fetch_assoc($cfwp_result);
								$cfwp_user_id 	= $cfd_row['ID'];
								$pan_user_id 	= ""; 
								$mendate_id 	= $cfd_row['mendate_id'];
								$customer_id 	= $cfd_row['customer_id'];

							}else{
								
								$pan_user_id 	= $bulk_partner['pan_user_id']; 
								$partner_sql = "SELECT * from `cfwp_ls_partners_details` where panacea_id='".$pan_user_id."' AND cancelled='0'";
								
								$partner_result = mysqli_query($con,$partner_sql);
								
								if(mysqli_num_rows($partner_result) > 0) { 
								
									$cfdp_row = mysqli_fetch_assoc($partner_result);
									
									$cfwp_user_id 	= ""; 
									$mendate_id 	= $cfdp_row['mendate_id'];
									$customer_id 	= $cfdp_row['customer_id'];
								}
								
							}
														
							$gocardless_link = false;
							//if(mysqli_num_rows($cfwp_result) > 0) {                  //change this sign after testing to >
							
							if($mendate_id != "") {
									 
									/*Call goCardless API to create payment using mendate ID*/	
									switch ($bulk_partner['currency']) {
										    case 1:
													$payment_currency = 'GBP';             //usd But it does not exist
													break;
											case 2:
													$payment_currency = 'GBP';
													break;
											case 3:
													$payment_currency = 'EUR';
													break;
									}
									
									try {
											if(isset($bulk_partner['delay_payment_by']) && $bulk_partner['delay_payment_by'] > 0){
												$delay_payment_by =	$bulk_partner['delay_payment_by'];
												$today_date = date('Y-m-d');
												$charge_date = date('Y-m-d', strtotime($today_date. '+ '.$delay_payment_by.' days'));	
												
												 $payment= $client->payments()->create([
													  "params" => ["amount" => $total_no*100,
																   "currency" => $payment_currency,
																    "charge_date" => $charge_date,
																   "links" => [
																	 "mandate" =>$mendate_id
																   ]]
													]);

											}else{
												
												$payment= $client->payments()->create([
												  "params" => ["amount" => $total_no*100,
															   "currency" => $payment_currency,
															   "links" => [
																 "mandate" =>$mendate_id
															   ]]
												]);
											}
											$gocardless_link = false;
											
											if(!empty($payment)){
												
												$insert_cfwp_sql = "INSERT INTO cfwp_ls_partners_payments SET user_id = '{$cfwp_user_id}',panacea_id = '{$pan_user_id}',customer_id = '{$customer_id}',amount = '{$total_no}',trees = '{$total_planted_trees}',payment_id = '{$payment->id}',payment_status = 'Pending',invoice_id = {$invoice_id}";

												 mysqli_query($con,$insert_cfwp_sql); 
												 
												 $payment_status['id'] = $invoice_id;
												 $payment_status['state'] = '-3';
												 $payment_status['payment_type'] = '1';
												 $this->db->update_batch('pct_invoices', array($payment_status), 'id');
											}
											
									}catch (\GoCardlessPro\Core\Exception\ApiException $e) {
											   $error = $e->getMessage();
											} catch (\GoCardlessPro\Core\Exception\MalformedResponseException $e) {
											    $error = $e->getMessage();
											} catch (\GoCardlessPro\Core\Exception\ApiConnectionException $e) {
											   $error = $e->getMessage();
									}
									
							}else{
									$activation_key = $this->randomKey(25);
									$pcinvoice['gc_activation_key'] = $activation_key;
								    $pcinvoice['id'] = $invoice_id;
								    $this->db->update_batch('pct_invoices', array($pcinvoice), 'id');
										 
									$gocardless_link = "https://carbonfreedining.org/pay-by-gocardless/?activation_key=$activation_key";
								
							}
							 
				    $invoice_name = 'The_Green_Earth_Appeal_Request_For_Funds_' . $current_month_text . '_' . $invoice_id .'.pdf';
					require('libraries/monthly_funds_request.php');
					
				}else{	
					    if($bulk_partner['enable_invoice_data'] == 1){		
						
							require('libraries/invoice-dynamic.php');           
						}else{					
						
							require('libraries/invoice.php');				
						}
				}
			
                /* // send invoice to the bulk partner
                        $config = array();
                        $config['to'] = $_SESSION['login']['email'];
                        $config['subject'] = 'Invoice for '.$bulk_partner['company'];
                        $config['message'] = 'Total: '.$total;

                        $this->send_mail($config); */

                // create and send the certificate
                // create the certificate
                $cname = $bulk_partner['first_name'];
                $restaurant = $bulk_partner['company'];


                $total_trees = $_SESSION['login']['data']['total_trees'];
				
				if($total_trees=='1'){
					 $client_trees = number_format($total_trees).' '.'tree';
				}else{
					$client_trees = number_format($total_trees).' '.'trees';
				}
                $calculated_cups = $total_trees*1000;
				
				$all_counters['cfd_counter'] = "";
				$all_counters['gea_counter'] = "";
				//$all_counters 	= $this->get_cfd_gea_counters();
				
				//$this->create_ticker($all_counters['gea_counter'], 'gea_counter');           //create GEA ticker for all partners
				//$this->create_cfd_ticker($all_counters['cfd_counter'], 'cfd_counter');		 //create CFD ticker for all partners
				
				$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate1']));
				$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate2']));
				$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate3']));
				$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate4']));

                //$rest_logo      = $_SESSION['login']['data']['logo'];
                /**
                $ds = DIRECTORY_SEPARATOR;
                $pct_logo_url = '';
                if (file_exists( dirname(__FILE__).'/../../uploads/restaurant/'.$rest_logo ) ) {
                $pct_logo = 'uploads/restaurant/'.$rest_logo;
                $pct_logo_url = '<img src="'.$pct_logo.'" title="logo" />';
                }
                 */
                $certificate_sub = $certificate_msg = $certificate_text = '';
                $certificate_cc_email = preg_replace("@[\t]@", '', $bulk_partner['cc_email']);
                $certificate_sub = preg_replace("@[\t]@", '', $bulk_partner['subject']);
                $certificate_msg = preg_replace("@[\t]@", '', $bulk_partner['message']);
                $certificate_text = preg_replace("@[\t]@", '', $bulk_partner['certificate_text']);
                $certificate_text = stripslashes($certificate_text);
                $certificate_text = str_ireplace(array('[name]', '[total_trees]', '[initiative]', '[opportunity_str]'), array($restaurant, $total_trees, $initiative, $opportunity_str), $certificate_text);
                //$cert_name = 'certificate_'.$invoice_id.'.pdf';
                // $prev_month_text = date("F_Y", strtotime("first day of previous month") );

					if($certificate_layout == '1'){
						require('libraries/cert-ini.php');
					}else{
						$cert_client_name = $bulk_partner['first_name'].' '.$bulk_partner['last_name'];
						require('libraries/cert-ini-layout2.php');
					}
							
                // create the ticker
                $refid = $this->user_model->get_refid($_SESSION['login']['id']);
                $ticker_url = base_url() . 'tickers/ticker_' . $refid->code . '.png';
				
				$counter_type  = $bulk_partner['counter_type'];
				/* if($initiative_id =='34' || $initiative_id =='35' || $initiative_id =='25' || $initiative_id =='31' ){
					$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
				}elseif($counter_type == 2){
					$this->create_cfd_ticker($total_trees, $refid->code);       //CFD tickers if enable in admin
				}else{
					 $this->create_ticker($total_trees, $refid->code);            //GEA tickers
				} */
				
				if($counter_type != 0){											      // if counter is set on partner
					if($counter_type==1){
						 $this->create_ticker($total_trees, $refid->code);            //GEA tickers
					}elseif($counter_type==2){
						$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
					}elseif($counter_type==3){
						$this->create_non_partner_ticker($total_trees, $refid->code);        //Non partner tickers
					}
					
				}else{                                                               // if counter is set on initiative
					if($ini_counter_type==1){
						$this->create_ticker($total_trees, $refid->code);            //GEA tickers
					}elseif($ini_counter_type==2){
						$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
					}elseif($ini_counter_type==3){
						$this->create_non_partner_ticker($total_trees, $refid->code);        //Non partner tickers
					}
				}
				
				$this->create_cfd_small_ticker($total_trees, $refid->code, 'black');
				
				$this->create_cfd_small_ticker($total_trees, $refid->code, 'green');
				
				$this->create_cfd_small_ticker($total_trees, $refid->code, 'white');
				
				if($bulk_partner['dashboard_user_email'] == ""){
					$hs_contact_email = $bulk_partner['email'];
				}else{
					$hs_contact_email = $bulk_partner['dashboard_user_email'];
				}
				
				$this->user_model->update_hubspot_restaurants_total_trees($hs_contact_email,$total_trees); // update in hubspot company
				
				$this->create_ticker_certificates($initiative_id); 		// to create tickers and certificates for initiatives added on 21-feb-2019

				if(isset($bulk_partner['ambassador_id']) && $bulk_partner['ambassador_id']!=0){
						 $this->create_ambassador_certificate($bulk_partner['ambassador_id']);
				}
				
				//to create pdf images for initiatives
				$cert_thumb_url = base_url() . 'cert/thumb_' . $refid->code . '.jpg';
                $cert_image_url = base_url() . 'cert/full_' . $refid->code . '.jpg';
				if (extension_loaded('imagick')){   		//extension imagick is installed
					$cert_path = dirname(__FILE__) . '/../../uploads/certs/';
					$pdf_path = $cert_path.$cert_name;
					$image_url = dirname(__FILE__) . '/../../cert/';
					
					if($send_invoices=="onupload"){
						$this->create_cert_pdf_to_image($pdf_path, $refid->code, $image_url); 
					}else{
						$this->create_cert_pdf_to_image($pdf_path, $refid->code, $image_url); 
					}
				}	
				
                //$unique_url = 'http://www.greenearthappeal.org/?refid=' . $refid->code;
                $unique_url = $bulk_partner['website_url'].'/?refid=' . $refid->code;

				if($send_invoices=="monthly"){
					$monthly['id']    		= $insert_monthly_invoice_id;
					$monthly['ticker_url'] 		= $ticker_url;
					$monthly['unique_url'] 		= $unique_url;
					$monthly['cert_thumb_url'] 		= $cert_thumb_url;
					$monthly['cert_image_url'] 		= $cert_image_url;
				 //$this->db->update_batch('pct_initiative_monthly_invoice_data', array($monthly), 'id');
				}
                // send certificate to the restaurant
                $config = array();
                
			      if($send_invoices == "onupload"){
					 $config['to'] = $_SESSION['login']['email'];
				 }else{
					 $config['to'] = 'panacea@greenearthappeal.org';
					
				 }     
				//$config['to'] =  'testing.whizkraft1@gmail.com';
				
				 
                /* $config['subject'] = $bulk_partner['subject'];
                        $message = str_ireplace(array('[name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]'),
                        array( $restaurant, $total_trees, $data1['trees'], $total_amt, $bulk_partner['first_name'], $ticker_url),
                        $bulk_partner['message']);
                        $config['message'] = $message; */

						
                $subject = $email_template->subject;
                $config['subject'] = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]', '[unique_url]','[initiative_name]'), array($cname, $restaurant, $total_trees, $data1['trees'], $total_amt, $bulk_partner['first_name'], $ticker_url, $unique_url,$initiative), $subject);
				
   
			     if($send_invoices=="onupload"){
					 $config['cc'] = $certificate_cc_email;    //main email
					 $config['bcc'] = 'panacea@greenearthappeal.org, 4641198@bcc.hubspot.com';
				 }else{
					 $config['cc'] = $certificate_cc_email;    //main email
					 $config['bcc'] = 'panacea@greenearthappeal.org, 4641198@bcc.hubspot.com';
				 }   
				   
				$partner_certificate_url = 'https://www.greenearthappeal.org/panacea/uploads/certs/'.$cert_name; //to add partner certificate url   
				
                $message = $email_template->message;
				
                $message = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]', '[unique_url]','[initiative_name]','[calculated_cups]','[cert_thumb_url]','[cert_image_url]','[certificate_url]','[cfd_counter]','[gea_counter]'), array($cname, $restaurant, $total_trees, $data1['trees'], $total_amt, $bulk_partner['first_name'], $ticker_url, $unique_url,$initiative,$calculated_cups,$cert_thumb_url,$cert_image_url,$partner_certificate_url,$all_counters['cfd_counter'],$all_counters['gea_counter']), $message);
				
                $config['message'] = $message;
				
				if($send_invoices=="onupload" && $request_for_funds==1){
					$config['attach'] = array(dirname(__FILE__) . '/../../uploads/request_for_funds/' . $invoice_name, dirname(__FILE__) . '/../../uploads/certs/' . $cert_name);
				}else{
					$config['attach'] = array(dirname(__FILE__) . '/../../uploads/invoices/' . $invoice_name, dirname(__FILE__) . '/../../uploads/certs/' . $cert_name);
				}
				$config['email_title'] = $bulk_partner['sender_email_title'];
				
				//if($redirect!=1){	
				 if($send_invoices=="onupload"){
						$enable_email = $email_template->enable_email;
						if($enable_email == 1){
								$this->send_mail($config);
						}
				 }
				//}

				
                unset($_SESSION['figure_history_key']);
                unset($_SESSION['login']['data']);
				if($redirect==1 || $redirect ==2){	
                    return;
				}else{
					 redirect(site_url());
				}
        }
		
        // confirm tree number for bulk partners
        function confirm() {
                $this->check_user('bulk_partner');
                if (!isset($_SESSION['login']['data']) || empty($_SESSION['login']['data']['rows'])) {
                        unset($_SESSION['login']['data']);
                        redirect(site_url());
                        die();
                }

                $data = json_decode($_SESSION['login']['data']['rows']);

                // $total = $_SESSION['login']['data']['total'];
                // update active status
                $this->load->database();
                $this->load->model('user_model');
                $bulk_partner = $this->user_model->load_bulk_partner($_SESSION['login']['id']);

                $initiative = $bulk_partner['initiative'];
                $opportunity_str = $bulk_partner['opportunity_str'];

                //$this->user_model->update_clients_state($_SESSION['login']['id']);
                // send email to all clients
                $email_template = $this->user_model->load_email_template(12);
				$invoice_free_trial = 0;
                $config = array();
                $current_month_text = date("F_Y");
                foreach ($data AS &$row) {
                        // store client into database
                        $row = (array) $row;
                        $unique_url = 'http://www.greenearthappeal.org/?refid=' . $row['referer_id'];
                        // create the ticker
                       // $this->create_ticker($row['tree_nums'], $row['referer_id']);
			
                        $this->create_ticker($row['client_trees'], $row['referer_ids']);

                        $ticker_url = base_url() . 'tickers/ticker_' . $row['referer_ids'] . '.png';
						$cert_thumb_url = base_url() . 'cert/thumb_' . $row['referer_ids'] . '.jpg';
						$cert_image_url = base_url() . 'cert/full_' . $row['referer_ids'] . '.jpg';

                        if ($row['old_user'] == 0) {
                                $client_id = $this->user_model->insert_client($row);
                        } else {
                                $client_id = $this->user_model->update_client($row);
                        }
                        $message = $email_template->message;

                        $message = str_ireplace(array('[contact_person]', '[company_name]', '[username]', '[password]', '[unique_url]', '[ticker_url]', '[cert_thumb_url]', '[cert_image_url]'), array($row['first_name'], $row['company'], $row['username'], $row['password'], $unique_url, $ticker_url,$cert_thumb_url,$cert_image_url), $message);

                        // create the certificate
                        $restaurant = $row['company'];
                        $total_trees = $row['tree_nums'];
                        $client_trees = $row['client_trees'];

                        //$rest_logo      = $_SESSION['login']['data']['logo'];
                        $ds = DIRECTORY_SEPARATOR;
                        /* $pct_logo_url = '';
                                if (file_exists( dirname(__FILE__).'/../../uploads/restaurant/'.$rest_logo ) ) {
                                $pct_logo = 'uploads/restaurant/'.$rest_logo;
                                $pct_logo_url = '<img src="'.$pct_logo.'" title="logo" />';
                                } */

                        $certificate_sub = $certificate_msg = $certificate_text = '';

                        $certificate_sub = preg_replace("@[\t]@", '', $bulk_partner['subject']);
                        $certificate_cc_email = preg_replace("@[\t]@", '', $bulk_partner['cc_email']);
                        $certificate_msg = preg_replace("@[\t]@", '', $bulk_partner['message']);
                        $certificate_text = preg_replace("@[\t]@", '', $bulk_partner['certificate_text']);
                        $certificate_text = stripslashes($certificate_text);
                        $certificate_text = str_ireplace(array('[name]', '[total_trees]', '[initiative]', '[opportunity_str]'), array($restaurant, $total_trees, $initiative, $opportunity_str), $certificate_text);

                        //$cert_name = 'certificate_'.$invoice_id.'.pdf';
                        // $prev_month_text = date("F_Y", strtotime("first day of previous month") );
                        $cert_name = 'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $client_id . '.pdf';
                        $cert_path = dirname(__FILE__) . '/../../uploads/certs/clients/';
                        require('libraries/cert.php');
						$image_url = dirname(__FILE__) . '/../../cert/';
						$pdf_path = $cert_path.$cert_name;
							
						if (extension_loaded('imagick')){   		//extension imagick is installed
							$this->create_cert_pdf_to_image($pdf_path, $row['referer_ids'], $image_url); //to create pef images for clients
						}	
                        // send email to client
                        $config['to'] = $row['email'];

                        if (isset($certificate_sub) && !empty($certificate_sub)) {
                                $config['subject'] = $certificate_sub;
                        } else {
                                $config['subject'] = $email_template->subject;
                        }

                        $config['subject'] = str_ireplace(array('[contact_person]', '[company_name]'), array($cname, $restaurant), $config['subject']);

                        if (isset($certificate_msg) && !empty($certificate_msg)) {
                                $config['message'] = $certificate_msg;
                        } else {
                                $config['message'] = $message;
                        }

                        $config['attach'] = array(dirname(__FILE__) . '/../../uploads/certs/clients/' . $cert_name);
                        // stop send_mail
                        $this->send_mail($config);
                        //send email to cc client
                        //if(isset($certificate_cc_email["cc_email"]) && !empty($certificate_cc_email["cc_email"])){
                        // a copy of email to the cc email id
                        if (isset($certificate_cc_email) && !empty($certificate_cc_email)) {
                                $config2 = array();
                                $config2['to'] = $certificate_cc_email;

                                if (isset($certificate_sub) && !empty($certificate_sub)) {
                                        $config2['subject'] = $certificate_sub;
                                } else {
                                        $config2['subject'] = $email_template->subject;
                                }

                                if (isset($certificate_msg) && !empty($certificate_msg)) {
                                        $config2['message'] = $certificate_msg;
                                } else {
                                        $config2['message'] = $message;
                                }

                                $config2['attach'] = array(dirname(__FILE__) . '/../../uploads/certs/clients/' . $cert_name);

                                //$this->send_mail($config2);
                                // stop send_mail
                        }
                }

                // prepare to create the invoice
                $cert_path = '';
                $pct_rows = &$data;
                $item2 = array();
                $bulk_partner = $this->user_model->load_bulk_partner($_SESSION['login']['id']);
                $email_template = $this->user_model->load_email_template(13);
                $item2['name'] = "Total number of trees";

                $item2['tree_nums'] = $_SESSION['login']['data']['current_tree'];
                $item2['unit'] = $_SESSION['login']['price'] / 100;
                switch ($_SESSION['login']['currency']) {
                        case 1:
                                $item2['currency'] = '$';
                                break;
                        case 2:
                                $item2['currency'] = '&pound;';
                                break;
						case 3:
								$item2['currency'] = '€';
								break;		
                }

                $item2['price'] = $item2['tree_nums'] * $item2['unit'];
                $tax = $_SESSION['login']['tax'];
                $sub_total = $currency . $item2['price'];
                //$total_no = number_format($item2['price'] * (100 + $tax)/100, 2);
                $total_no = $item2['price'] * (100 + $tax) / 100;
                $total = $item2['currency'] . number_format($total_no, 2);
                $total_amt = $item2['currency'] . number_format($total_no, 2);

                // store invoice in database
                $data1 = array();
                $data1['user_id'] = $_SESSION['login']['id'];
                $data1['date_created'] = 'NOW()';
                $data1['total'] = $total_no;
                $data1['state'] = 0;
                $data1['trees'] = $_SESSION['login']['data']['current_tree'];

                $invoice_id = $this->user_model->insert_invoice($data1);

                // expend
                $invoice_company = $cert_company = $bulk_partner['company'];
                $address = $_SESSION['login']['data']['address'];
                $city = $_SESSION['login']['data']['city'];
                $state = $_SESSION['login']['data']['state'];
                $post_code = $_SESSION['login']['data']['post_code'];


                // create the invoice
                // $invoice_name = 'invoice_'.$invoice_id.'.pdf';
                // $prev_month_text = date("F_Y", strtotime("first day of previous month") );
                $current_month_text = date("F_Y");
                $invoice_name = 'The_Green_Earth_Appeal_Invoice_' . $current_month_text . '_' . $invoice_id . '.pdf';

                $cert_name = 'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $invoice_id . '.pdf';
                // update the invoice
                // $invoice_number = date('dm').$invoice_id;
                $invoice_number = $invoice_id;
                $invoice_data = array();
                $invoice_data['id'] = $invoice_id;
                $invoice_data['number'] = $invoice_number;
                $invoice_data['name'] = $invoice_name;

                // this is only for bulkpartner, so we can load previous figure month
                $invoice_data['latest_certificate'] = $cert_name;
                $invoice_data['month'] = date('mY');

                $this->db->update_batch('pct_invoices', array($invoice_data), 'id');
                require('libraries/invoice.php');

                /* // send invoice to the bulk partner
                        $config = array();
                        $config['to'] = $_SESSION['login']['email'];
                        $config['subject'] = 'Invoice for '.$bulk_partner['company'];
                        $config['message'] = 'Total: '.$total;

                        $this->send_mail($config); */

                // create and send the certificate
                // create the certificate
                $cname = $bulk_partner['first_name'];
                $restaurant = $bulk_partner['company'];


                $total_trees = $_SESSION['login']['data']['total_trees'];
                $client_trees = $total_trees;

                //$rest_logo      = $_SESSION['login']['data']['logo'];
                /**
                $ds = DIRECTORY_SEPARATOR;
                $pct_logo_url = '';
                if (file_exists( dirname(__FILE__).'/../../uploads/restaurant/'.$rest_logo ) ) {
                $pct_logo = 'uploads/restaurant/'.$rest_logo;
                $pct_logo_url = '<img src="'.$pct_logo.'" title="logo" />';
                }
                 */
                $certificate_sub = $certificate_msg = $certificate_text = '';
                $certificate_cc_email = preg_replace("@[\t]@", '', $bulk_partner['cc_email']);
                $certificate_cc_people_email= preg_replace("@[\t]@", '', $bulk_partner['cc_people_email']);
                $certificate_sub = preg_replace("@[\t]@", '', $bulk_partner['subject']);
                $certificate_msg = preg_replace("@[\t]@", '', $bulk_partner['message']);
                $certificate_text = preg_replace("@[\t]@", '', $bulk_partner['certificate_text']);
                $certificate_text = stripslashes($certificate_text);
                $certificate_text = str_ireplace(array('[name]', '[total_trees]', '[initiative]', '[opportunity_str]'), array($restaurant, $total_trees, $initiative, $opportunity_str), $certificate_text);
                //$cert_name = 'certificate_'.$invoice_id.'.pdf';
                // $prev_month_text = date("F_Y", strtotime("first day of previous month") );


                require('libraries/cert.php');

                // create the ticker
                $refid = $this->user_model->get_refid($_SESSION['login']['id']);
                $ticker_url = base_url() . 'tickers/ticker_' . $refid->code . '.png';
                $this->create_ticker($total_trees, $refid->code);
                $unique_url = 'http://www.greenearthappeal.org/?refid=' . $refid->code;

				
				//to create pdf images for bulk partners
				$cert_thumb_url = base_url() . 'cert/thumb_' . $refid->code . '.jpg';
				$cert_image_url = base_url() . 'cert/full_' . $refid->code . '.jpg';
				if (extension_loaded('imagick')){   		//extension imagick is installed
					$cert_path = dirname(__FILE__) . '/../../uploads/certs/';
					$pdf_path = $cert_path.$cert_name;
					$image_url = dirname(__FILE__) . '/../../cert/';
					$this->create_cert_pdf_to_image($pdf_path, $refid->code, $image_url); 
				}	
                // send certificate to the restaurant
                $config = array();
                $config['to'] = $_SESSION['login']['email'];
                /* $config['subject'] = $bulk_partner['subject'];
                        $message = str_ireplace(array('[name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]'),
                        array( $restaurant, $total_trees, $data1['trees'], $total_amt, $bulk_partner['first_name'], $ticker_url),
                        $bulk_partner['message']);
                        $config['message'] = $message; */

                $config['subject'] = $email_template->subject;
                $config['cc'] = $certificate_cc_email .','.$certificate_cc_people_email;
                $config['bcc'] = 'panacea@greenearthappeal.org';
                $message = $email_template->message;
                $message = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]', '[unique_url]', '[cert_thumb_url]', '[cert_image_url]'), array($cname, $restaurant, $total_trees, $data1['trees'], $total_amt, $bulk_partner['first_name'], $ticker_url, $unique_url, $cert_thumb_url, $cert_image_url), $message);
                $config['message'] = $message;

                $config['attach'] = array(dirname(__FILE__) . '/../../uploads/invoices/' . $invoice_name, dirname(__FILE__) . '/../../uploads/certs/' . $cert_name);
                $this->send_mail($config);

                unset($_SESSION['login']['data']);
                redirect(site_url());
        }

        /**
         * =========================================================================================================
         * Invoice functions
         */
        public function invoices() {
                $this->check_user(array('admin', 'solicitor'));
                $this->load->model('user_model');
				 if($this->input->post()){
					 $paid = $this->input->post('paid');
					 $verify = $this->input->post('verify');
					$rows = $this->user_model->get_invoices($paid,$verify);
				 }else{
					$rows = $this->user_model->get_invoices();
				 }
				 foreach($rows as $key => $row){
					 
					 $getcomp = $this->user_model->get_invoice_company($row->type, $row->user_id);
					 if(!empty($getcomp)){
						$rows[$key]->company  = $getcomp->company;
					 }else{
						 $rows[$key]->company  = "";
					 }
					 
				 }
				// echo "<pre>"; print_r($rows); die;
                $this->load->view('template', array('title' => 'Invoices History', 'body' => 'invoices_list', 'rows' => $rows, 'utype' => $_SESSION['login']['type']));
        }
		
        /**
         * =========================================================================================================
         *  initiative_partner Invoice functions
         */
        public function initiative_partner_invoices() {
                $this->check_user(array('admin', 'intitiative'));
                $this->load->model('user_model');
				 if($this->input->post()){
					 $paid = $this->input->post('paid');
					 $verify = $this->input->post('verify');
					$rows = $this->user_model->get_initiative_partner_invoices($paid,$verify);
				 }else{
					$rows = $this->user_model->get_initiative_partner_invoices();
				 }
                $this->load->view('template', array('title' => 'Invoices History', 'body' => 'initiative_partner_invoices', 'rows' => $rows, 'utype' => $_SESSION['login']['type']));
        }
		 /**
         * =========================================================================================================
         *  initiative editor Invoice functions
         */
        public function initiative_editor_invoices() {
                $this->check_user(array('admin', 'initiative_editor'));
                $this->load->model('user_model');
				//echo "<pre>"; print_r($_SESSION['login']); die("here");
				 if($this->input->post()){
					 $paid = $this->input->post('paid');
					 $verify = $this->input->post('verify');
					$rows = $this->user_model->get_initiative_editor_invoices($paid,$verify);
				 }else{
					$rows = $this->user_model->get_initiative_editor_invoices();
				 }
				 
				 $invoices_arrr = array();
				  foreach($rows as $key => $row){
					 $invoices_arrr[]  = $row->id;
					 $getcomp = $this->user_model->get_invoice_company($row->type, $row->user_id);
					 if(!empty($getcomp)){
						$rows[$key]->company  = $getcomp->company;
					 }else{
						 $rows[$key]->company  = "";
					 }
					 
				 }
				$_SESSION['invoices_arrr'] = $invoices_arrr;
              //  $this->load->view('template', array('title' => 'Invoices History', 'body' => 'initiative_partner_invoices', 'rows' => $rows, 'utype' => $_SESSION['login']['type']));
                $this->load->view('template', array('title' => 'Invoices History', 'body' => 'invoices_list', 'rows' => $rows, 'utype' => $_SESSION['login']['type']));
        }
		
		  /**
         * =========================================================================================================
         * Invoice functions
         */
        public function invoices_list() {
                $this->check_user(array('admin', 'solicitor'));
                $this->load->model('user_model');
                $rows = $this->user_model->get_invoices();
                $this->load->view('template', array('title' => 'Invoices History', 'body' => 'invoices_list_test', 'rows' => $rows, 'utype' => $_SESSION['login']['type']));
        }
		  /**
         * =========================================================================================================
         * View All Invoice functions
         */
        public function view_deleted_invoices() {
                $this->check_user(array('admin', 'solicitor','initiative_editor'));
                $this->load->model('user_model');
				if($this->input->post()){
					 $paid = $this->input->post('paid');
					 $verify = $this->input->post('verify');
					$rows = $this->user_model->get_deleted_invoices($paid,$verify);
				 }else{
					$rows = $this->user_model->get_deleted_invoices();
				 }
                $this->load->view('template', array('title' => 'Deleted Invoices History', 'body' => 'deleted_invoices_list', 'rows' => $rows, 'utype' => $_SESSION['login']['type']));
        }

		public function deleted_invoices($user_id = NULL) {
			   $this->load->model('user_model');
			     $this->check_user(array('admin'));
				 $user_ids = $this->input->post('id');
				 foreach($user_ids as $key=>$val){
				   $this->user_model->invoices_deleted($user_ids[$key]);
				 }
		}
		public function undeleted_invoices($user_id = NULL) {
			   $this->load->model('user_model');
			     $this->check_user(array('admin'));
				 $user_ids = $this->input->post('id');
				 foreach($user_ids as $key=>$val){
				   $this->user_model->invoices_undeleted($user_ids[$key]);
				 }
		}
		
        public function invoice_action() {
                $this->check_user(array('admin', 'solicitor','initiative_editor'));
                if (isset($_POST['task'])) {
                        $id = $_POST['id'];
                        $this->load->model('user_model');
                        switch ($_POST['task']) {
                                case 'pay':
                                        if ($_SESSION['login']['type'] == 'admin' || $_SESSION['login']['type'] == 'initiative_editor') {
                                                $this->user_model->invoice_pay($id);
                                                /* send email to the solicitor */
                                                $sol_user = $this->user_model->get_solicitor_user();
                                                $email_template = $this->user_model->load_email_template(22);

                                                $login_url = site_url('user/login');
                                                $forget_password = site_url('user/forget_password');

                                                $username = $sol_user['username'];
                                                $password = $sol_user['password'];
                                                $solicitor_autologin = site_url('user/solicitor_autologin') . "/{$username}/{$password}";

                                                $message = $email_template->message;
                                                $message = str_ireplace(
                                                        array('[username]', '[forget_password]', '[login_url]', '[solicitor_autologin]'),
                                                        array($sol_user['username'], $forget_password, $login_url, $solicitor_autologin),
                                                        $message);

                                                $config = array();
                                                //$config['to'] = "dan.johnson@equitablelaw.com";
                                                $config['to'] = $sol_user['email'];
                                                $config['subject'] = $email_template->subject;
                                                $config['message'] = $message;
                                                $this->send_mail($config);
                                                /* -- */
                                        }
                                        break;
								case 'refund':
                                        if ($_SESSION['login']['type'] == 'admin' || $_SESSION['login']['type'] == 'initiative_editor') {
                                                $this->user_model->invoice_refund($id);
                                        }
                                        break;
                                case 'delete':
                                        if ($_SESSION['login']['type'] == 'admin' || $_SESSION['login']['type'] == 'initiative_editor')
                                                $this->user_model->invoice_delete($id);
                                        break;
                                case 'solicitor':
                                        if ($_SESSION['login']['type'] == 'solicitor') {
                                                echo 'solicitor';
                                                die();
                                        }
                                        break;
                        }
                }
                redirect(site_url('user/invoices'));
        }
		
		    /**
         * =========================================================================================================
         * common functions
         */

        function eu_send_mail($data) {

                $mail             = new PHPMailer();
                $mail->IsSMTP(); // telling the class to use SMTP
                $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                // 1 = errors and messages
                // 2 = messages only
                $mail->SMTPAuth   = true;                  // enable SMTP authentication
                $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
                $mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
                $mail->Port       = 587;                   // set the SMTP port for the GMAIL server
                $mail->Username   = "partners@greenearthappeal.org";  // GMAIL username
                $mail->Password   = "P8%k<g_39x,#1";            // GMAIL password
                $mail->CharSet = 'UTF-8';

                if (isset($data['from'])) {
                        $mail->SetFrom($data['from'], '');
                } elseif(isset($data['email_title'])){
                        $mail->SetFrom('partners@greenearthappeal.org', $data['email_title'] );
                }else{
					//$mail->SetFrom('partners@greenearthappeal.org', 'Partnerships Department – Green Earth Appeal');
					 $mail->SetFrom('richard.dickson@greenearthappeal.org', 'Richard Dickson - Carbon Free Dining');
				}

                if (isset($data['attach']) && !empty($data['attach'])) {
                        if (!is_array($data['attach'])) {
                                $data['attach'] = (array) $data['attach'];
                        }
                        foreach ($data['attach'] AS $attach) {
                                $mail->AddAttachment($attach); // attachment
                        }
                }

                ob_start();
                if ((isset($data['signature']) && $data['signature'] == false) || (!$data['signature'])) {
                        echo '<style type="text/css">*, p {font-family:Calibri!important; font-size: 11pt!important;}</style>';
                        require('libraries/signature_richard.htm');
                } else {
                        require('libraries/signature_solicitor.htm');
                }

                $message = ob_get_contents();
                ob_end_clean();

                $message = $data['message'] . $message;

                $mail->ClearReplyTos();
                $mail->AddReplyTo("richard.dickson@greenearthappeal.org", "Richard Dickson - Carbon Free Dining");
                $mail->Subject    = $data['subject'];
                $mail->MsgHTML($message);
                $mail->AddAddress($data['to']);

                if (isset($data['cc'])) {
                        $cc_emails = explode(',', $data['cc']);
                        foreach ($cc_emails AS $cc) {
                                $mail->AddCC($cc);
                        }
                }

                if (isset($data['bcc'])) {
                        $bcc_emails = explode(',', $data['bcc']);
                        foreach ($bcc_emails AS $bcc) {
                                $mail->AddBCC($bcc);
                        }
                }

                /*if (@$attachment) {
                                $mail->AddAttachment($attachment); // attachment
                }*/

                if (!$mail->Send()) {
                        echo "Mailer Error: " . $mail->ErrorInfo;
                }

        }

        /**
         * =========================================================================================================
         * common functions
         */

        function send_mail($data) {

                $mail             = new PHPMailer();

                $mail->IsSMTP(); // telling the class to use SMTP
                $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                // 1 = errors and messages
                // 2 = messages only
                $mail->SMTPAuth   = true;                  // enable SMTP authentication
                $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
//$mail->Host       = "greenearthappeal.org";      // sets GMAIL as the SMTP server
                $mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
                $mail->Port       = 587;                   // set the SMTP port for the GMAIL server
//$mail->Username   = "vinhnd@greenearthappeal.org";  // GMAIL username
                $mail->Username   = "partners@greenearthappeal.org";  // GMAIL username

//$mail->Password   = "hT7t3dKf-";            // GMAIL password
                $mail->Password   = "P8%k<g_39x,#1";            // GMAIL password
                $mail->CharSet = 'UTF-8';



                if (isset($data['from'])) {
                        $mail->SetFrom($data['from'], '');
                } elseif(isset($data['email_title'])){
                        $mail->SetFrom('partners@greenearthappeal.org', $data['email_title'] );
                }else{
					$mail->SetFrom('partners@greenearthappeal.org', 'Partnerships Department – Green Earth Appeal');
					// $mail->SetFrom('richard.dickson@greenearthappeal.org', 'Richard Dickson - Carbon Free Dining');
				}

                if (isset($data['attach']) && !empty($data['attach'])) {
                        if (!is_array($data['attach'])) {
                                $data['attach'] = (array) $data['attach'];
                        }
                        foreach ($data['attach'] AS $attach) {
                                $mail->AddAttachment($attach); // attachment
                        }
                }

                ob_start();
                if ((isset($data['signature']) && $data['signature'] == false) || (!$data['signature'])) {
                        echo '<style type="text/css">*, p {font-family:Calibri!important; font-size: 11pt!important;}</style>';
                        require('libraries/signature.htm');
                } else {
                        require('libraries/signature_solicitor.htm');
                }

                $message = ob_get_contents();
                ob_end_clean();

                $message = $data['message'] . $message;

                $mail->ClearReplyTos();
                $mail->AddReplyTo("partners@greenearthappeal.org", "Partnerships Department - Green Earth Appeal");
                $mail->Subject    = $data['subject'];
                $mail->MsgHTML($message);
                $mail->AddAddress($data['to']);

                if (isset($data['cc'])) {
                        $cc_emails = explode(',', $data['cc']);
                        foreach ($cc_emails AS $cc) {
                                $mail->AddCC($cc);
                        }
                }

                if (isset($data['bcc'])) {
                        $bcc_emails = explode(',', $data['bcc']);
                        foreach ($bcc_emails AS $bcc) {
                                $mail->AddBCC($bcc);
                        }
                }

                /*if (@$attachment) {
                                $mail->AddAttachment($attachment); // attachment
                }*/

                if (!$mail->Send()) {
                        echo "Mailer Error: " . $mail->ErrorInfo;
                }

        }
		
		 /**
         * =========================================================================================================
         * common functions
         */

        function send_grant_email($data) {

                $mail             = new PHPMailer();

                $mail->IsSMTP(); // telling the class to use SMTP
                $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                $mail->SMTPAuth   = true;                  // enable SMTP authentication
                $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
                $mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
                $mail->Port       = 587;                   // set the SMTP port for the GMAIL server
                $mail->Username   = "partners@greenearthappeal.org";  // GMAIL username
                $mail->Password   = "P8%k<g_39x,#1";            // GMAIL password
                $mail->CharSet = 'UTF-8';

                if (isset($data['from'])) {
                        $mail->SetFrom($data['from'], '');
                } elseif(isset($data['email_title'])){
                        $mail->SetFrom('partners@greenearthappeal.org', $data['email_title'] );
                }else{
					$mail->SetFrom('partners@greenearthappeal.org', 'Partnerships Department – Green Earth Appeal');
				}

                if (isset($data['attach']) && !empty($data['attach'])) {
                        if (!is_array($data['attach'])) {
                                $data['attach'] = (array) $data['attach'];
                        }
                        foreach ($data['attach'] AS $attach) {
                                $mail->AddAttachment($attach); // attachment
                        }
                }

                ob_start();
                if ((isset($data['signature']) && $data['signature'] == false) || (!$data['signature'])) {
                        echo '<style type="text/css">*, p {font-family:Calibri!important; font-size: 11pt!important;}</style>';
                        require('libraries/signature.htm');
                } else {
                        require('libraries/signature_solicitor.htm');
                }

                $message = ob_get_contents();
                ob_end_clean();

                $message = $data['message'] . $message;

                $mail->ClearReplyTos();
                //$mail->AddReplyTo("marvin@greenearthappeal.org", "Marvin Baker");
                $mail->Subject    = $data['subject'];
                $mail->MsgHTML($message);
                $mail->AddAddress($data['to']);

                if (isset($data['cc'])) {
                        $cc_emails = explode(',', $data['cc']);
                        foreach ($cc_emails AS $cc) {
                                $mail->AddCC($cc);
                        }
                }

                if (isset($data['bcc'])) {
                        $bcc_emails = explode(',', $data['bcc']);
                        foreach ($bcc_emails AS $bcc) {
                                $mail->AddBCC($bcc);
                        }
                }
                if (!$mail->Send()) {
                        echo "Mailer Error: " . $mail->ErrorInfo;
                }

        }


        // check if email exists
        function email_check($email, $user_type = '', $id = 0) {
                return true;
                $this->load->library('form_validation');
                $this->load->database();
                $sql = "SELECT * FROM pct_users WHERE email='$email'";
                if ($user_type) {
                        $sql .= " AND type<>'$user_type'";
                }
                $query = $this->db->query($sql);
                if ($query->num_rows()) {
                        $this->form_validation->set_message('email_check', 'The %s field exists');
                        return FALSE;
                } else {
                        // check if email exists in the same client group
                        if ($id) {
                                $sql = "SELECT * FROM pct_users AS u
                                                                                                INNER JOIN pct_bulk_partners AS bp
                                                                                                ON u.id=bp.user_id
                                                                                                WHERE u.id=$id
                                                                                                AND email='$email'";
                                $query = $this->db->query($sql);
                                if ($query->num_rows()) {
                                        $this->form_validation->set_message('email_check', 'The %s field exists');
                                        return FALSE;
                                }
                        }
                        return TRUE;
                }
        }

        function email_check2($email, $id) {
                return true;
                $this->load->database();
                $query = $this->db->query("SELECT * FROM pct_users WHERE email='$email' AND id<>$id");
                if ($query->num_rows()) {
                        $this->form_validation->set_message('email_check2', 'The %s field exists');
                        return FALSE;
                } else {
                        return TRUE;
                }
        }

        // check if email exists
        function username_check($username, $id = 0) {
                $this->load->library('form_validation');
                $this->load->database();
                $sql = "SELECT * FROM pct_users WHERE username='$username'";
                if ($id) {
                        $sql .= " AND id<>$id";
                }
                $query = $this->db->query($sql);
                if ($query->num_rows()) {
                        $this->form_validation->set_message('username_check', 'The %s field exists');
                        return FALSE;
                } else {
                        return TRUE;
                }
        }
		 // check if user already exists with same email and same company name
         function initiative_username_check($username, $company) {
                $this->load->library('form_validation');
                $this->load->database();
				$sql = 'SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email="'. $username . '" AND u.type="intitiative_client" AND bp.company="' . $company . '"';
                $query = $this->db->query($sql);
                if ($query->num_rows()) {
                        $this->form_validation->set_message('username_check', 'The %s field exists');
                        return FALSE;
                } else {
                        return TRUE;
                }
        }

        function referer_check($code) {
                $code = stripcslashes(trim($code));
                $this->load->library('form_validation');
                $this->load->database();
                $sql = 'SELECT * FROM pct_referer WHERE code="'.$code.'"';
                $query = $this->db->query($sql);
                if ($query->num_rows()) {
                        $this->form_validation->set_message('referer_check', 'The %s field exists');
                        return FALSE;
                } else {
                        return TRUE;
                }
        }

        function check_user($user_type = 'admin') {
                if (!is_array($user_type)) {
                        $user_type = (array) $user_type;
                }

                $check = in_array($_SESSION['login']['type'], $user_type);
                if (!$check) {
                        echo 'You dont have permission.';
                        die();
                }
                return true;
        }

        function check_active($id) {
                $query = "SELECT active FROM pct_users WHERE id=$id";
                $this->load->database();
                $row = $this->db->query($query);
                $row = $row->row_array();
                if ($row['active']) {
                        return true;
                } else {
                        return false;
                }
        }

        function email_templates() {
                $this->check_user(array("admin", "manager", "editor"));
                $this->load->database();
                $this->load->model('user_model');

                // process form here
                if (isset($_POST['email_template'])) {
                        $data = $_POST['data'];
                        $rows = $this->user_model->update_email_template($data);

                        redirect('user/email_templates');
                }
                $rows = $this->user_model->load_email_template();
                $data = array('title' => 'Email templates', 'body' => 'email_template', 'rows' => $rows);
                $this->data = array_merge($this->data, $data);
                $this->load->view('template', $this->data);
        }

        /**
         * ============================================================================================================================================
         * function for Solicitor
         */
        function add_solicitor() {
                $this->check_user();
                if (isset($_POST)) {
                        // process restaurant data
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
                        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check');
                        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password2]');
                        $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');

                        //$this->form_validation->set_rules('restaurant');
                        //$this->form_validation->set_rules('first_name');
                        //$this->form_validation->set_rules('last_name');
                        //$this->form_validation->set_rules('phone');
                        //$this->form_validation->set_rules('password2', 'Password Confirm', 'trim|required|match[password]');
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Add Solicitor', 'body' => 'solicitor');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->insert_solicitor();

                                // send email confirmation
                                $email = $this->input->post('email');
                                $username = $this->input->post('username');
                                $password = $this->input->post('password');
                                $config = array();
                                $config['to'] = $email;

                                $config['from'] = $email; //Added 3rd April
                                $config['subject'] = '[Solicitor] - Your info account on GEA';
                                $config['message'] = "
                                                                                                                                                                <p>Username: $username</p>
                                                                                                                                                                <p>Password: $password</p>";
                                $pct_sendmail = $this->send_mail($config);
                                // return to homepage
                                redirect(site_url());
                        }
                } else {
                        $data = array('title' => 'Add Solicitor', 'body' => 'solicitor');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }

        function edit_solicitor($id = 0) {
                $this->check_user('admin');
                if ($_POST) {
                        // process restaurant data
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check2['.$id.']');
                        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check['.$id.']');

                        $this->form_validation->set_rules('password', 'Password', 'matches[password2]');
                        $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');

                        //$this->form_validation->set_rules('restaurant');
                        //$this->form_validation->set_rules('first_name');
                        //$this->form_validation->set_rules('last_name');
                        //$this->form_validation->set_rules('phone');
                        //$this->form_validation->set_rules('password2', 'Password Confirm', 'trim|required|match[password]');
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Edit Solicitor', 'body' => 'solicitor_edit');
                                $this->load->model('user_model');
                                $data2 = $this->user_model->load_solicitor($id);
                                $this->data = array_merge($this->data, $data, $data2);
                                $this->load->view('template', $this->data);
                        } else {
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->update_solicitor($id);

                                /* // send email confirmation
                                        $email = $this->input->post('email');
                                        $username = $this->input->post('username');
                                        $password = $this->input->post('password');
                                        $config = array();
                                        $config['to'] = $email;

                                        $config['from'] = $email; //Added 3rd April
                                        $config['subject'] = '[Solicitor] - Your info account on GEA';
                                        $config['message'] = "
                                        <p>Username: $username</p>
                                        <p>Password: $password</p>";
                                        $pct_sendmail = $this->send_mail($config); */
                                // return to homepage
                                redirect(site_url());
                        }
                } else {
                        $data = array('title' => 'Edit Solicitor', 'body' => 'solicitor_edit');
                        $this->load->model('user_model');
                        $data2 = $this->user_model->load_solicitor($id);
                        $this->data = array_merge($this->data, $data, $data2);
                        $this->load->view('template', $this->data);
                }
        }

        /**
         * ============================================================================================================================================
         * function for Editor
         */
        function add_editor() {
                $this->check_user();
                if (isset($_POST)) {
                        // process editor data
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
                        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check');
                        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password2]');
                        $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');

                        //$this->form_validation->set_rules('restaurant');
                        //$this->form_validation->set_rules('first_name');
                        //$this->form_validation->set_rules('last_name');
                        //$this->form_validation->set_rules('phone');
                        //$this->form_validation->set_rules('password2', 'Password Confirm', 'trim|required|match[password]');
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Add Editor', 'body' => 'editor');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->insert_editor();

                                // send email confirmation
                                $email = $this->input->post('email');
                                $username = $this->input->post('username');
                                $password = $this->input->post('password');
                                $config = array();
                                $config['to'] = $email;

                                $config['from'] = $email; //Added 3rd April
                                $config['subject'] = '[Editor] - Your info account on GEA';
                                $config['message'] = "
                                                                                                                                                                <p>Username: $username</p>
                                                                                                                                                                <p>Password: $password</p>";
                                $pct_sendmail = $this->send_mail($config);
                                // return to homepage
                                redirect(site_url());
                        }
                } else {
                        $data = array('title' => 'Add Editor', 'body' => 'editor');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }
		 /**
         * ============================================================================================================================================
         * function for initiative Editor
         */
        function add_initiative_editor() {
                $this->check_user();
                if (isset($_POST)) {
                        // process editor data
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
                        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check');
                        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password2]');
                        $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');

                        //$this->form_validation->set_rules('restaurant');
                        //$this->form_validation->set_rules('first_name');
                        //$this->form_validation->set_rules('last_name');
                        //$this->form_validation->set_rules('phone');
                        //$this->form_validation->set_rules('password2', 'Password Confirm', 'trim|required|match[password]');
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Add Initiative Editor', 'body' => 'initiative_editor');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->insert_initiative_editor();

                                // send email confirmation
                                $email = $this->input->post('email');
                                $username = $this->input->post('username');
                                $password = $this->input->post('password');
                                $config = array();
                                $config['to'] = $email;

                                $config['from'] = $email; //Added 3rd April
                                $config['subject'] = '[Editor] - Your info account on GEA';
                                $config['message'] = "
                                                                                                                                                                <p>Username: $username</p>
                                                                                                                                                                <p>Password: $password</p>";
                                $pct_sendmail = $this->send_mail($config);
                                // return to homepage
                                redirect(site_url());
                        }
                } else {
                        $data = array('title' => 'Add Initiative Editor', 'body' => 'initiative_editor');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }
		
		function edit_initiative_editor($id = 0) {
                $this->check_user('admin');
                if ($_POST) {
                        // process restaurant data
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check2['.$id.']');
                        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check['.$id.']');

                        $this->form_validation->set_rules('password', 'Password', 'matches[password2]');
                        $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');

                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Edit Initiative Editor', 'body' => 'initiative_editor_edit');
                                $this->load->model('user_model');
                                $data2 = $this->user_model->load_solicitor($id);
                                $this->data = array_merge($this->data, $data, $data2);
                                $this->load->view('template', $this->data);
                        } else {
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->update_initiative_editor($id);

                                // return to homepage
                                redirect(site_url());
                        }
                } else {
                        $data = array('title' => 'Edit Initiative Editor', 'body' => 'initiative_editor_edit');
                        $this->load->model('user_model');
                        $data2 = $this->user_model->load_solicitor($id);
                        $this->data = array_merge($this->data, $data, $data2);
                        $this->load->view('template', $this->data);
                }
        }
        function edit_editor($id = 0) {
                $this->check_user('admin');
                if ($_POST) {
                        // process restaurant data
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check2['.$id.']');
                        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash|callback_username_check['.$id.']');

                        $this->form_validation->set_rules('password', 'Password', 'matches[password2]');
                        $this->form_validation->set_message('matches', 'Password field and Password conf field do not match');

                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Edit Editor', 'body' => 'editor_edit');
                                $this->load->model('user_model');
                                $data2 = $this->user_model->load_solicitor($id);
                                $this->data = array_merge($this->data, $data, $data2);
                                $this->load->view('template', $this->data);
                        } else {
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->update_editor($id);

                                // return to homepage
                                redirect(site_url());
                        }
                } else {
                        $data = array('title' => 'Edit Editor', 'body' => 'editor_edit');
                        $this->load->model('user_model');
                        $data2 = $this->user_model->load_solicitor($id);
                        $this->data = array_merge($this->data, $data, $data2);
                        $this->load->view('template', $this->data);
                }
        }

        function view_invoice($invoice_id) {
			
                $this->check_user(array('admin', 'solicitor','initiative_editor'));
				
				if($_SESSION['login']['type']=='initiative_editor'){
					if(!in_array($invoice_id,$_SESSION['invoices_arrr'])){
						show_error('Can not access this invoice!');
					}
				}
                $this->load->model('user_model');
                $row = $this->user_model->get_invoice($invoice_id);
				//echo "<pre>"; print_r($row); die;
                $invoice_id = $row->id;
                $invoice_name = $row->name;
                $date_created = $row->date_created;
				$date_created = date("F_Y", strtotime($date_created));
				//$string = strpos($invoice_name,"The_Green_Earth_Appeal_Request_For_Funds");
				$string = strcmp($invoice_name,"The_Green_Earth_Appeal_Request_For_Funds");
				
				$fund_invoice_name = "The_Green_Earth_Appeal_Request_For_Funds_".$date_created.'_'.$invoice_id.'.pdf';
				 $fund_invoice_name  = dirname(__FILE__) . '/../../uploads/request_for_funds/' . $fund_invoice_name;
				
                if ($row->user_id == 802 || $row->user_id == 803) {
                        $invoice_path = dirname(__FILE__) . '/../../../the_sonne/invoices/' . $invoice_name;
                }elseif($string=='20'){
					  $invoice_path = dirname(__FILE__) . '/../../uploads/request_for_funds/' . $invoice_name;
				}elseif(file_exists($fund_invoice_name) && is_file($fund_invoice_name)){
					  $invoice_path = $fund_invoice_name;
				}else {
                      $invoice_path = dirname(__FILE__) . '/../../uploads/invoices/' . $invoice_name;
                }
				//echo $invoice_path; die;
                // $file = fopen($invoice_path);
                if (file_exists($invoice_path) && is_file($invoice_path)) {
                        header('Content-type: application/pdf');
                        echo file_get_contents($invoice_path);
                } else {
                        echo 'this file does not exists anymore';
                }
        }
			/* for creating pdf tests */
		function check_quitable_law_pdf($id) {
			
                        $this->load->model('user_model');
                        $row = $this->user_model->get_invoice($id);
                        $total = number_format($row->total, 2);
                        $current_trees = (int) $row->trees;
                        $amt_words = $this->convertNumber( str_replace(',', '', $total));
                        $num_trees = ucwords($this->convertNumber($current_trees)) . " ($current_trees)";
                        $num_trees = str_ireplace(array(' ## Zero', ' ##'), '', $num_trees);


                        $user_id = $row->user_id;
                        $user = $this->user_model->get_user($user_id);
						
                        $currency = '';
                        $curr = '';
                        $subcurr = '';
                        switch ($user['currency']) {
                                case 1:
                                        $currency = '$';
                                        $curr = 'Dollars';
                                        $subcurr = 'Cent';
                                        break;
                                case 2:
                                        $currency = '&pound;';
                                        $curr = 'Pounds';
                                        $subcurr = 'Pence';
                                        break;
                        }

                        $amt_words = str_replace("##", strtolower($curr), $amt_words);
                        $amt_words = ucwords($amt_words) . " " . $subcurr;
                        $amt_words .= " ({$currency}{$total})";

                        $total = $currency . $total;
                        $first_name = $user['first_name2'] ? $user['first_name2'] : $user['first_name'];
                        $email = $user['email2'] ? $user['email2'] : $user['email'];
                        $current_month = @date('F');

                        /* generate pdf file from pdf template */
                        if (isset($user['company'])) {
                                $companyname = $user['company'];
                        } else {
                                $companyname = $user['restaurant'];
                        }

                        require('libraries/pdf_mail_page1.php');
                        require('libraries/pdf_mail_page2.php');

                        $pdftext_foa = $first_name . " " . $user['last_name'];
                        $pdftext_comp = $companyname;
                        $pdftext_add = $user['address'];
                        $pdftext_city = $user['city'];
                        $pdftext_state = $user['state'];
                        $pdftext_postcode = $user['post_code'];
                        $pdftext_sentdate = date('l, d F Y', time());

                        $your_ref = $row->number;

                        $pdftext_all_page1 = str_replace("[pdftext_foa]", $pdftext_foa, $pdftext_all_page1);
                        $pdftext_all_page1 = str_replace("[pdftext_comp]", $pdftext_comp, $pdftext_all_page1);
                        $pdftext_all_page1 = str_replace("[pdftext_add]", $pdftext_add, $pdftext_all_page1);
                        $pdftext_all_page1 = str_replace("[pdftext_city]", $pdftext_city, $pdftext_all_page1);
                        $pdftext_all_page1 = str_replace("[pdftext_state]", $pdftext_state, $pdftext_all_page1);
                        $pdftext_all_page1 = str_replace("[pdftext_postcode]", $pdftext_postcode, $pdftext_all_page1);
                        $pdftext_all_page1 = str_replace("[pdftext_sentdate]", $pdftext_sentdate, $pdftext_all_page1);

                        $pdftext_all_page1 = str_replace("[your_ref]", $your_ref, $pdftext_all_page1);

                        $pdftext_all_page2 = str_replace("[amt_words]", $amt_words, $pdftext_all_page2);
                        $pdftext_all_page2 = str_replace("[pdftext_comp]", $pdftext_comp, $pdftext_all_page2);
                        $pdftext_all_page2 = str_replace("[num_trees]", $num_trees, $pdftext_all_page2);
                        require('libraries/cert_pdf.php');

		}		
        function document($id) {
                $this->check_user(array('admin', 'solicitor','initiative_editor'));
				if($_SESSION['login']['type']=='initiative_editor'){
					if(!in_array($id,$_SESSION['invoices_arrr'])){
						show_error('Can not access this invoice!');
					}
				}
                $this->load->model('user_model');
                $row = $this->user_model->get_invoice($id);
                $upload_data = array('file1' => '', 'file2' => '', 'id' => $id);

                if ($_FILES['file1']['size'] || $_FILES['file2']['size']) {
                        // process upload files
                        // check upload
                        if (isset($_FILES['file1'])) {
                                $config['upload_path'] = './uploads/document/';
                                $config['allowed_types'] = 'pdf|doc|docx|xlsx|eml|msg';
                                $config['max_size'] = '5000';
                                $this->load->library('upload');
                                $this->upload->initialize($config);
                                if (!$this->upload->do_upload('file1')) {
                                        //$rows = $this->user_model->load_email_template();
                                        $data = array('title' => 'Invoice document', 'body' => 'document', 'error' => $this->upload->display_errors(), 'row' => $row);
                                        $this->data = array_merge($this->data, $data);
                                        $this->load->view('template', $this->data);
                                        return;
                                } else {
                                        $file = $this->upload->data();
                                        $upload_data['file1'] = $file['file_name'];
                                }
                        }
                        // file2
                        if (isset($_FILES['file2'])) {
                                $config['upload_path'] = './uploads/document/';
                                $config['allowed_types'] = 'pdf|doc|docx|xlsx|eml|msg';
                                $config['max_size'] = '5000';
                                $this->load->library('upload');
                                $this->upload->initialize($config);

                                if (!$this->upload->do_upload('file2')) {
                                        //$rows = $this->user_model->load_email_template();
                                        $data = array('title' => 'Invoice document', 'body' => 'document', 'error' => $this->upload->display_errors(), 'row' => $row);
                                        $this->data = array_merge($this->data, $data);
                                        $this->load->view('template', $this->data);
                                        return;
                                } else {
                                        $file = $this->upload->data();
                                        $upload_data['file2'] = $file['file_name'];
                                }
                        }
                        //
                        $this->db->update_batch('pct_invoices', array($upload_data), 'id');
                        redirect(site_url('user/invoices'));
                } elseif (isset($_POST['forward'])) {
                        // process restaurant data
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                        $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
                        $this->form_validation->set_rules('message', 'Message', 'trim|required');

                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Invoice document', 'body' => 'document', 'row' => $row);
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
                                // forward to solicitor here
                                // send welcome email
                                $doc_path = dirname(__FILE__) . '/../../uploads/document/';
                                $config = array();
                                $config['to'] = $_POST['email'];
                                $config['subject'] = $_POST['subject'];
                                $config['message'] = $_POST['message'];
                                $config['attach'] = array();
                                if ($row->file1) {
                                        $config['attach'][] = $doc_path . $row->file1;
                                }
                                if ($row->file2) {
                                        $config['attach'][] = $doc_path . $row->file2;
                                }
                                $pct_sendmail = $this->send_mail($config);
                                redirect(site_url('user/invoices'));
                        }
                } else {
                        //$rows = $this->user_model->load_email_template();
                        $data = array('title' => 'Invoice document', 'body' => 'document', 'row' => $row);
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }

        function confirm_payment($send_mail = true) {
                $this->check_user(array('admin', 'solicitor'));

                if (isset($_POST['id'])) {
                        // process sending email
                        $id = (int) $this->input->post('id');
                        $this->load->model('user_model');
                        $row = $this->user_model->get_invoice($id);
                        $total = number_format($row->total, 2);
                        $current_trees = (int) $row->trees;
                        $amt_words = $this->convertNumber( str_replace(',', '', $total));
                        $num_trees = ucwords($this->convertNumber($current_trees)) . " ($current_trees)";
                        $num_trees = str_ireplace(array(' ## Zero', ' ##'), '', $num_trees);


                        $user_id = $row->user_id;
                        $user = $this->user_model->get_user($user_id);
                        $currency = '';
                        $curr = '';
                        $subcurr = '';
                        switch ($user['currency']) {
                                case 1:
                                        $currency = '$';
                                        $curr = 'Dollars';
                                        $subcurr = 'Cent';
                                        break;
                                case 2:
                                        $currency = '&pound;';
                                        $curr = 'Pounds';
                                        $subcurr = 'Pence';
                                        break;
                        }

                        $amt_words = str_replace("##", strtolower($curr), $amt_words);
                        $amt_words = ucwords($amt_words) . " " . $subcurr;
                        $amt_words .= " ({$currency}{$total})";

                        $total = $currency . $total;
                        $first_name = $user['first_name2'] ? $user['first_name2'] : $user['first_name'];
                        $email = $user['email2'] ? $user['email2'] : $user['email'];
                        $current_month = @date('F');

                        /* generate pdf file from pdf template */
                        if (isset($user['company'])) {
                                $companyname = $user['company'];
                        } else {
                                $companyname = $user['restaurant'];
                        }

                        require('libraries/pdf_mail_page1.php');
                        require('libraries/pdf_mail_page2.php');

                        $pdftext_foa = $first_name . " " . $user['last_name'];
                        $pdftext_comp = $companyname;
                        $pdftext_add = $user['address'];
                        $pdftext_city = $user['city'];
                        $pdftext_state = $user['state'];
                        $pdftext_postcode = $user['post_code'];
                        $pdftext_sentdate = date('l, d F Y', time());

                        $your_ref = $row->number;

                        $pdftext_all_page1 = str_replace("[pdftext_foa]", $pdftext_foa, $pdftext_all_page1);
                        $pdftext_all_page1 = str_replace("[pdftext_comp]", $pdftext_comp, $pdftext_all_page1);
                        $pdftext_all_page1 = str_replace("[pdftext_add]", $pdftext_add, $pdftext_all_page1);
                        $pdftext_all_page1 = str_replace("[pdftext_city]", $pdftext_city, $pdftext_all_page1);
                        $pdftext_all_page1 = str_replace("[pdftext_state]", $pdftext_state, $pdftext_all_page1);
                        $pdftext_all_page1 = str_replace("[pdftext_postcode]", $pdftext_postcode, $pdftext_all_page1);
                        $pdftext_all_page1 = str_replace("[pdftext_sentdate]", $pdftext_sentdate, $pdftext_all_page1);

                        $pdftext_all_page1 = str_replace("[your_ref]", $your_ref, $pdftext_all_page1);

                        $pdftext_all_page2 = str_replace("[amt_words]", $amt_words, $pdftext_all_page2);
                        $pdftext_all_page2 = str_replace("[pdftext_comp]", $pdftext_comp, $pdftext_all_page2);
                        $pdftext_all_page2 = str_replace("[num_trees]", $num_trees, $pdftext_all_page2);

                        require('libraries/cert_pdf.php');

                        if ( ! $send_mail ) {
                                header('Location: http://www.greenearthappeal.org/panacea/uploads/solicitor/Equitable_Law_' . $user_id . '.pdf');
                                return false;
                        }

                        /* -- -- -- */
                        /* echo "done";
                                die; */

                        $email_template = $this->user_model->load_email_template(21);
                        // send email
                        // send welcome email
                        //$signimg = "<img src='http://localhost/partner_login/img/image0011.png' />";
                        $signimg = "<img src='http://www.greenearthappeal.org/partner_login/img/image0011.png' />";
                        $attach_path = dirname(__FILE__) . '/../../uploads/solicitor/';
                        $message = str_ireplace(
                                array('[first_name]', '[current_trees]', '[total]', '[imagepath]', '[month]'),
                                array($first_name, $current_trees, $total, $signimg, $current_month),
                                $email_template->message);
                        $config = array();
                        $config['to'] = $email;
                        $sol_user = $this->user_model->get_solicitor_user();
                        $config['cc'] = $sol_user['email'];
                        $config['bcc'] = "panacea@greenearthappeal.org";

                        $config['subject'] = $email_template->subject;
                        $config['message'] = $message;
                        $config['attach'] = array();
                        $config['attach'][] = $attach_path . 'Equitable_Law_' . $user_id . '.pdf';
                        //$config['from'] = $_SESSION['login']['email'];
                        $config['from'] = "dan.johnson@equitablelaw.com";
                        $config['from_name'] = '';
                        // $config['signature'] = 'solicitor';
                        $pct_sendmail = $this->send_mail($config);

                        // confirm invoice
                        $confirm_data = array();
                        $confirm_data['id'] = $id;
                        $confirm_data['confirmed'] = 1;
                        $this->db->update_batch('pct_invoices', array($confirm_data), 'id');
						
						$query1 = "SELECT * FROM pct_invoices WHERE id=$id";
						$res = $this->db->query($query1)->row();
						
						/* to insert data in the  pct_invoices_logs table*/
						
						$query2 = "INSERT INTO pct_invoices_logs
							 SET user_id = '{$res->user_id}',
								invoice_id = '{$res->id}',
								event = 'verified',
								status = '1',
								verified_date = NOW()";
						$insert_log = $this->db->query($query2);

						
						/* // create connection with CFD */
						$db_name = "carbonfd_cfddb";
						$db_host = "localhost";
						$db_user = "carbonfd_cfddbus";
						$db_pass = "vVZKUCXr@wAU";

						$con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
						// Check connection
						if (mysqli_connect_errno())
						  {
						  echo "Failed to connect to MySQL: " . mysqli_connect_error();
						  }
						  $update_cfwp_sql = "Update cfwp_ls_partners_payments SET verified = '1' where invoice_id = '{$row->id}'";
						  mysqli_query($con,$update_cfwp_sql); 
						  
                        redirect(site_url('user/invoices'));
                } else {
                        redirect('user/invoices');
                }
        }

        function prepare_letter($invoice_id) {
                $_POST['id'] = $invoice_id;
                $this->confirm_payment(false);
        }

        function solicitor_autologin($username, $password) {
                $username = mysql_escape_string($username);
                $password = mysql_escape_string($password);
                $this->load->database();

                // check user login
                $query = $this->db->query("SELECT * FROM pct_users WHERE username='$username' AND password='$password'");

                if ($query->num_rows()) {
                        // call apprirate View
                        $row = $query->row_array();

                        if ($row['active'] == 0) {
                                show_error('Your account is not enable yet.');
                        } else {
                                $_SESSION['login'] = $row;
                                redirect(site_url());
                        }
                } else {
                        show_error('Oops. Somethings goes wrong');
                }

        }

        /**
         * ============================================================================================================================================
         * function for Affiliates
         */
        function track_list() {
                $this->check_user(array("admin", "manager"));
                $this->load->model('user_model');
                $rows = $this->user_model->list_track();
                $this->load->view('template', array('title' => 'Affiliates Tracking', 'body' => 'track_list', 'rows' => $rows));
        }

        function affiliate_index() {
                $this->check_user('affiliate');
                $this->load->model('user_model');
                $rows = $this->user_model->get_submission_list();
                $this->load->view('template', array('title' => 'Submission List', 'body' => 'affiliate_index', 'rows' => $rows));
        }

        function affiliates_list() {
                $this->check_user(array("admin", "manager"));
                $this->load->model('user_model');
                $rows = $this->user_model->list_affiliates();
                $this->load->view('template', array('title' => 'Affiliates List', 'body' => 'affiliates_list', 'rows' => $rows));
        }

        function delete_affiliate($id) {
                $this->check_user(array("admin", "manager"));
                $this->load->database();
                $this->db->query("DELETE FROM pct_affiliates WHERE id=$id");
                redirect(site_url("user/affiliates_list"));
        }

        function add_affiliate() {
                $this->check_user(array("admin", "manager"));
                if (isset($_POST)) {
                        // process restaurant data
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
                        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha');
                        $this->form_validation->set_rules('type', 'Type', 'trim|required|alpha');
                        $this->form_validation->set_rules('rate', 'Rate', 'trim|required|is_numeric');

                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        $rand_code = $this->generateCode(8);
                        $rand_pass = $this->generateCode(8);

                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Add Affiliate', 'body' => 'add_affiliate');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->insert_affiliate($rand_code, $rand_pass);

                                // send email confirmation
                                $email = $this->input->post('email');
                                $uname = $this->input->post('username');

                                $config = array();
                                $config['to'] = $email;

                                $config['from'] = $email; //Added 3rd April
                                $config['subject'] = 'Your info account on GEA';
                                $config['message'] = "<p>Username: $uname</p>
                                                                                                                                                                <p>Password: $rand_pass</p>";
                                $pct_sendmail = $this->send_mail($config);
                                // return to homepage
                                redirect(site_url("user/affiliates_list"));
                        }
                } else {
                        $data = array('title' => 'Add Affiliate', 'body' => 'add_affiliate');
                        $this->data = array_merge($this->data, $data);
                        $this->load->view('template', $this->data);
                }
        }

        function edit_affiliate($id = 0) {
                $this->check_user(array("admin", "manager"));
                if ($_POST) {
                        // process restaurant data
                        $this->load->library('form_validation');

                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
                        $this->form_validation->set_rules('type', 'Type', 'trim|required|alpha');
                        $this->form_validation->set_rules('rate', 'Rate', 'trim|required|is_numeric');

                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Edit Affiliate', 'body' => 'affiliate_edit');
                                $this->load->model('user_model');
                                $data2 = $this->user_model->load_affiliate($id);
                                $this->data = array_merge($this->data, $data, $data2);
                                $this->load->view('template', $this->data);
                        } else {
                                $this->load->database();
                                $this->load->model('user_model');
                                $this->user_model->update_affiliate($id);

                                /* // send email confirmation
                                        $email = $this->input->post('email');
                                        $username = $this->input->post('username');
                                        $password = $this->input->post('password');
                                        $config = array();
                                        $config['to'] = $email;

                                        $config['from'] = $email; //Added 3rd April
                                        $config['subject'] = '[Solicitor] - Your info account on GEA';
                                        $config['message'] = "
                                        <p>Username: $username</p>
                                        <p>Password: $password</p>";
                                        $pct_sendmail = $this->send_mail($config); */
                                // return to homepage
                                redirect(site_url("user/affiliates_list"));
                        }
                } else {
                        $data = array('title' => 'Edit Affiliate', 'body' => 'affiliate_edit');
                        $this->load->model('user_model');
                        $data2 = $this->user_model->load_affiliate($id);
                        $this->data = array_merge($this->data, $data, $data2);
                        $this->load->view('template', $this->data);
                }
        }

        function send_test_email() {
                $this->check_user(array("admin", "manager", "editor"));
                if ($_POST) {
                        // process restaurant data
                        $this->load->library('form_validation');

                        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                        $this->form_validation->set_rules('template_id', 'Template ID', 'trim|required|integer');

                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false) {
                                echo 'Invalid data.';
                                die();
                        } else {
                                $this->load->database();
                                $this->load->model('user_model');
                                $id = $_POST['template_id'];
                                $template = $this->user_model->load_email_template($id);

                                if ($template) {
                                        // send email
                                        $config = array();
                                        $config['to'] = $_POST['email'];
                                        $config['from'] = 'partners@greenearthappeal.org'; //Added 3rd April
                                        $config['subject'] = $template->subject;
                                        $config['message'] = $template->message;
                                        $this->send_mail($config);
                                } else {
                                        echo 'Invalid template id.';
                                        die();
                                }

                                // return to homepage
                                redirect(site_url("user/email_templates"));
                        }
                } else {
                        redirect(site_url());
                }
        }


        function manage_ref() {
                $limit = 30;
                $this->check_user(array("admin"));
                $this->load->model('user_model');
                //$rows = $this->user_model->list_refid();
                $CI = &get_instance();
                $db2 = $CI->load->database('db2', TRUE);

                $result = $db2->query('SELECT COUNT(*) AS total FROM t_sponsors');
                $total = (int) $result->result()[0]->total;
                $limit_start = $this->uri->segment(3) ? $this->uri->segment(3) : 0;

                $result2 = $db2->query("SELECT * FROM t_sponsors ORDER BY id DESC LIMIT $limit_start, $limit");
                $rows = $result2->result();

                foreach ($rows AS &$row) {
                        $query = $this->db->query("SELECT * FROM pct_referrer_redirect WHERE type='ref' AND code='{$row->scode}'");
                        if ($query->num_rows()) {
                                $item = $query->row();
                                $row->redirect_url = $item->redirect_url;
                                $row->redirect_id = $item->id;
                        }
                }

                $this->load->library('pagination');

                $config['base_url'] = site_url('user/manage_ref');
                $config['total_rows'] = $total;
                $config['per_page'] = $limit;
                $config['num_links'] = 6;

                $this->pagination->initialize($config);
                $pagination = $this->pagination->create_links();

                $this->load->view('template', array('title' => 'List of ref', 'body' => 'list_ref', 'rows' => $rows, 'pagination' => $pagination));
        }

        function save_referrer_redirect() {
                $id = (int) $this->input->post('id');
                $this->load->model('user_model');

                if ($id) {
                        $this->user_model->update_referrer_redirect($id);
                } else {
                        $this->user_model->insert_referrer_redirect();
                }

                //$_SESSION['message'] = 'Redirect_url is saved successfully';
                redirect($_SERVER['HTTP_REFERER']);
        }

        function manage_refid() {
                $limit = 30;
                $this->check_user(array("admin"));
                $this->load->model('user_model');
                //$rows = $this->user_model->list_refid();

                $sql = "SELECT COUNT(*) AS total FROM pct_referer AS pr
                                                                INNER JOIN pct_users AS pu
                                                                ON pu.id = pr.user_id
                                                                WHERE pu.type IN ('bulk_partner', 'restaurant', 'client')";
                $result = $this->db->query($sql);
                $total = (int) $result->row()->total;

                $limit_start = $this->uri->segment(3) ? $this->uri->segment(3) : 0;

                $sql = "SELECT pr.*, prr.redirect_url AS redirect_url, prr.id AS redirect_id
                                                                FROM pct_referer AS pr
                                                                INNER JOIN pct_users AS pu
                                                                ON pu.id = pr.user_id
                                                                LEFT JOIN pct_referrer_redirect AS prr
                                                                ON prr.code = pr.code AND prr.type='refid'
                                                                WHERE pu.type IN ('bulk_partner', 'restaurant', 'client')
                                                                ORDER BY pr.id DESC
                                                                LIMIT $limit_start, $limit";
                $result2 = $this->db->query($sql);
                $rows = $result2->result();

                $this->load->library('pagination');

                $config['base_url'] = site_url('user/manage_refid');
                $config['total_rows'] = $total;
                $config['per_page'] = $limit;
                $config['num_links'] = 6;

                $this->pagination->initialize($config);
                $pagination = $this->pagination->create_links();

                $this->load->view('template', array('title' => 'List of refid', 'body' => 'list_refid', 'rows' => $rows, 'pagination' => $pagination));
        }

        /* ================================ ================================ ================================ ================================ */


        /* common uses */

        function generateCode($num) {
                $alphas = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z'));
                $found = true;
                $max = count($alphas);
                while ($found) {
                        $code = "";
                        for ($i = 0; $i < $num; $i++) {
                                $code .= $alphas[rand(0, $max - 1)];
                        }
                        //Check code not used
                        $sql = "SELECT COUNT(*) FROM pct_affiliates WHERE code='{$code}' ";
                        $r = mysql_query($sql);
                        if ($r[0] == 0) {
                                return $code;
                        } else {
                                $this->generateCode($num);
                        }
                }
        }

// func end
		
		function create_initiative_ticker($trees, $initiative_id) {

                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
                //$text = number_format($trees);
                $text = $trees;
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                 $fname = dirname(__FILE__) . '/../../tickers/initiative_tickers/ticker_' . $initiative_id . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }
		
		function create_ambassador_ticker($trees, $ambassador_id) {

                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/ambassador_ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
                //$text = number_format($trees);
                $text = $trees;
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                 $fname = dirname(__FILE__) . '/../../tickers/ambassador_tickers/ticker_' . $ambassador_id . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }


        function create_ticker($trees, $refid) {

                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
				$text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                 $fname = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }
		function create_cfd_small_ticker($trees, $refid, $color) {
			
                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/cfd-tickers/cfd-small-ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);
				
				// Allocate A Color For The Text - white,Green,Black
				if($color=="black"){
					$font_color = imagecolorallocate($png_image, 0, 0, 0);
				}elseif($color=="white"){
					$font_color = imagecolorallocate($png_image, 255, 255, 255);
				}else{
					$font_color = imagecolorallocate($png_image, 22, 105, 54);      //green
				}

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
                $text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                $fname = dirname(__FILE__) . '/../../tickers/cfd-tickers/cfd_'.$color.'_ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }
		function create_cfd_ticker($trees, $refid) {
			
                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/cfd-tickers/cfd-ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
                 $text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                 $fname = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }
		function create_non_partner_ticker($trees, $refid) {
			
                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                 $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/non-partner.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/MYRIADPRO-BOLDCOND.OTF';

                // Set Number Text to Be Printed On Image
                 $text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '140';
                } else if ($chklen == 2) {
                        $dist = 125;
                } else if ($chklen == 3) {
                        $dist = 112;
                } else if ($chklen == 4) {
                        $dist = 92;
                } else if ($chklen == 5) {
                        $dist = 88;
                }else if ($chklen == 6) {
                        $dist = 69;
                }else if ($chklen == 7) {
                        $dist = 58;
                }else if ($chklen == 9) {
                        $dist = 40;
                }
				
                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 28, 0, $dist, 35, $font_color, $font_path, $text);

               $fname = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
        }

        function create_ticker_old($trees, $refid) {
                $this->load->library('image_lib');
                $config['source_image'] = dirname(__FILE__) . '/../../tickers/ticker.jpg';
                $config['new_image'] = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.jpg';
                // $config['dynamic_output'] = true;
                $text = $trees > 1 ? 'trees' : 'tree';
                $config['wm_text'] = "Counter: $trees $text";
                $config['wm_type'] = 'text';
                $config['wm_font_color'] = '000';
                $config['wm_font_size'] = '26';
                $config['wm_vrt_offset'] = '30';
                $config['wm_hor_alignment'] = 'center';
                //$config['wm_padding'] = '40';
                $this->image_lib->initialize($config);
                $this->image_lib->watermark();
                /*         * echo $this->image_lib->display_errors();
                        echo 'success';
                        return; */
        }

        /*  -----------    Number convertsion functions     -----------   */

        function convertNumber($num) {
                /* if(strpos($num,".")){
                        list($num, $dec) = explode(".", $num);
                        } */
                list($num, $dec) = explode(".", $num);

                $output = "";

                if ($num{0} == "-") {
                        $output = "negative ";
                        $num = ltrim($num, "-");
                } else if ($num{0} == "+") {
                        $output = "positive ";
                        $num = ltrim($num, "+");
                }

                if ($num{0} == "0") {
                        $output .= "zero";
                } else {
                        $num = str_pad($num, 36, "0", STR_PAD_LEFT);
                        $group = rtrim(chunk_split($num, 3, " "), " ");
                        $groups = explode(" ", $group);

                        $groups2 = array();
                        foreach ($groups as $g)
                                $groups2[] = $this->convertThreeDigit($g{0}, $g{1}, $g{2});

                        for ($z = 0; $z < count($groups2); $z++) {
                                if ($groups2[$z] != "") {
                                        $output .= $groups2[$z] . $this->convertGroup(11 - $z) . ($z < 11 && !array_search('', array_slice($groups2, $z + 1, -1)) && $groups2[11] != '' && $groups[11]{0} == '0' ? ", " : " and ");
                                }
                        }
                        $output = substr($output, 0, $output . strlen() - 4);
                        //$output = rtrim($output, ", ");
                }

                // comment by Vinh, I am not sure why they need this block

                if ($dec > 0 && $dec != "") {
                        $output .= " ## ";
                        $strnum1 = 0;
                        $strnum2 = 0;
                        //
                        for ($i = 0; $i < strlen($dec); $i++) {
                                if ($i == 0) {
                                        $strnum1 = $dec{$i};
                                } else {
                                        $strnum2 = $dec{$i};
                                }
                        }
                        $output .= " " . $this->convertTwoDigit($strnum1, $strnum2);
                        //for($i = 0; $i < strlen($dec); $i++) $output .= " ".$this->convertDigit($dec{$i});
                } else {
                        $output .= " ## zero";
                }

                return $output;
        }

        function convertGroup($index) {
                switch ($index) {
                        case 11: return " decillion";
                        case 10: return " nonillion";
                        case 9: return " octillion";
                        case 8: return " septillion";
                        case 7: return " sextillion";
                        case 6: return " quintrillion";
                        case 5: return " quadrillion";
                        case 4: return " trillion";
                        case 3: return " billion";
                        case 2: return " million";
                        case 1: return " thousand";
                        case 0: return "";
                }
        }

        function convertThreeDigit($dig1, $dig2, $dig3) {
                $output = "";

                if ($dig1 == "0" && $dig2 == "0" && $dig3 == "0")
                        return "";

                if ($dig1 != "0") {
                        $output .= $this->convertDigit($dig1) . " hundred";
                        if ($dig2 != "0" || $dig3 != "0")
                                $output .= " and ";
                }

                if ($dig2 != "0")
                        $output .= $this->convertTwoDigit($dig2, $dig3);
                else if ($dig3 != "0")
                        $output .= $this->convertDigit($dig3);

                return $output;
        }

        function convertTwoDigit($dig1, $dig2) {
                if ($dig2 == "0") {
                        switch ($dig1) {
                                case "1": return "ten";
                                case "2": return "twenty";
                                case "3": return "thirty";
                                case "4": return "forty";
                                case "5": return "fifty";
                                case "6": return "sixty";
                                case "7": return "seventy";
                                case "8": return "eighty";
                                case "9": return "ninety";
                        }
                } else if ($dig1 == "1") {
                        switch ($dig2) {
                                case "1": return "eleven";
                                case "2": return "twelve";
                                case "3": return "thirteen";
                                case "4": return "fourteen";
                                case "5": return "fifteen";
                                case "6": return "sixteen";
                                case "7": return "seventeen";
                                case "8": return "eighteen";
                                case "9": return "nineteen";
                        }
                } else {
                        $temp = $this->convertDigit($dig2);
                        switch ($dig1) {
                                case "0": return "$temp";
                                case "2": return "twenty-$temp";
                                case "3": return "thirty-$temp";
                                case "4": return "forty-$temp";
                                case "5": return "fifty-$temp";
                                case "6": return "sixty-$temp";
                                case "7": return "seventy-$temp";
                                case "8": return "eighty-$temp";
                                case "9": return "ninety-$temp";
                        }
                }
        }

        function convertDigit($digit) {
                switch ($digit) {
                        case "0": return "zero";
                        case "1": return "one";
                        case "2": return "two";
                        case "3": return "three";
                        case "4": return "four";
                        case "5": return "five";
                        case "6": return "six";
                        case "7": return "seven";
                        case "8": return "eight";
                        case "9": return "nine";
                }
        }
		function randomKey($length) {  //create no of random digit alphanumeric code
			$key='';
			$pool = array_merge(range(0,9),range('A', 'Z'));

			for($i=0; $i < $length; $i++) {
				$key .= $pool[mt_rand(0, count($pool) - 1)];
			}
			return $key;
		}
		function create_pdf_to_image($pdf,$filename,$image_path) {  //create image from pdf

			$im = new imagick($pdf); 
			$im->setImageColorspace(13); 
			$im->setCompression(Imagick::COMPRESSION_JPEG); 
			$im->setCompressionQuality(80); 
			$im->setImageFormat('jpeg'); 
			$im->resizeImage(290, 375, imagick::FILTER_LANCZOS, 1);  
			$im->writeImage($image_path.'certificate_'.$filename.'.jpg'); 
			$im->clear(); 
			$im->destroy(); 
			return ;
		}
		function create_cert_pdf_to_image($pdf,$ref_id,$image_url) {  //create image from certificate pdf
				$im = new imagick();
				$im->setResolution(300, 300);
				$im->readImage($pdf);
				$im->setImageColorspace(13); 
				$im->setImageCompression(Imagick::COMPRESSION_JPEG); 
				$im->setImageCompressionQuality(80);
				$im->setImageFormat('jpeg'); 
				$im->resizeImage(1199, 1771, imagick::FILTER_LANCZOS, 1);  
				$im->writeImage($image_url.'full_'.$ref_id.'.jpg');  
				$im->resizeImage(300, 443, imagick::FILTER_LANCZOS, 1);  
				$im->writeImage($image_url.'thumb_'.$ref_id.'.jpg'); 
				$im->clear(); 
				$im->destroy(); 
				return ;
		}
	function create_amb_cert_pdf_to_image($pdf,$ref_id,$image_url) {  //create image from certificate pdf
				$im = new imagick();
				$im->setResolution(300, 300);
				$im->readImage($pdf);
				$im->setImageColorspace(13); 
				$im->setImageCompression(Imagick::COMPRESSION_JPEG); 
				$im->setImageCompressionQuality(80);
				$im->setImageFormat('jpeg'); 
				$im->resizeImage(1199, 1771, imagick::FILTER_LANCZOS, 1);  
				$im->writeImage($image_url.'full_'.$ref_id.'.jpg');  
				$im->resizeImage(300, 443, imagick::FILTER_LANCZOS, 1);  
				$im->writeImage($image_url.'thumb_'.$ref_id.'.jpg'); 
				$im->clear(); 
				$im->destroy(); 
				return ;
		}
		
		public function gea_counter(){
			$counter = $this->get_cfd_gea_counters();
			echo $counter['gea_counter'];
		}
		
		public function cfd_counter(){
			$counter = $this->get_cfd_gea_counters();
			echo $counter['cfd_counter'];
		}
		
		/* TO get CFD and GEA counter */
		public function get_cfd_gea_counters()
		{
			$query = "SELECT u.id,bp.counter_type,pi.ini_counter_type,(bp.free_trees + bp.tree_nums) as partner_trees, (SELECT (SUM(free_trees)+ SUM(tree_nums)) FROM pct_initiative_partner WHERE bulker_id=u.id) As client_trees
                        FROM pct_initiative_partner AS bp
                        INNER JOIN pct_users AS u
                        ON u.id=bp.user_id
						INNER JOIN pct_initiative AS pi
                        ON pi.id=bp.initiative
                        WHERE u.type IN ('admin', 'intitiative') and active = '1'";
				
           $results = $this->db->query($query)->result();
		   $all_trees = array();
		   // echo "<pre>"; print_r($results); die;
		   foreach($results as $result){
				   $total_trees = 0;
				   $total_trees = $result->client_trees + $result->partner_trees;
				   
				   if($result->counter_type != 0){	
						if($result->counter_type == 1){	
							$all_trees['gea_counter']  += $total_trees;
						}elseif($result->counter_type==2){
							$all_trees['cfd_counter']  += $total_trees;
						}else{
							$all_trees['non_partner_counter']  += $total_trees;
						}
						 
				   }else{
					    if($result->ini_counter_type == 1){	
							$all_trees['gea_counter']  += $total_trees;
						}elseif($result->ini_counter_type == 2){
							$all_trees['cfd_counter']  += $total_trees;
						}else{
							$all_trees['non_partner_counter']  += $total_trees;
						}
				   }
				   $all_trees['all_partner_trees']  += $total_trees;
		   } 
		   //echo "<pre>"; print_r($all_trees); die;
		   return  $all_trees;
		}
	
}

// class ends	