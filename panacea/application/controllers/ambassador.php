<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ambassador extends CI_Controller {

		 public function __construct() {
                parent::__construct();
                session_start();
                $this->data = array();
                $this->load->helper('url');
                $this->load->helper('form');
				$this->load->model('user_model');
				$this->load->model('ambassador_model');
                $segment_login = $this->uri->segment(2);
                require_once('phpmailer/class.phpmailer.php');
                
        }
			
		public function create_ambassador_from_cfd()
		{ 
		

			// $ambassador_sql = "SELECT (SUM(free_trees)+ SUM(tree_nums)) as total_trees FROM pct_initiative_partner WHERE bulker_id=9056";    
			// $amb_res = $this->db->query($ambassador_sql)->result_array();
			// echo "<pre>"; print_r($amb_res ); die;
			
	/* 		$ambassador_sql = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='Joe.Hague@landuse.co.uk' AND u.type='intitiative_client' AND bp.company='LUC";    
			$amb_res = $this->db->query($ambassador_sql)->result_array();
			echo "<pre>"; print_r($amb_res ); die; */
		
			 if(!empty($_GET)){
				   
				    if (is_array($_GET) && count($_GET)) {
						//echo "<pre>"; print_r($_GET); die;
						$data_row = array();
						$data_row =  $this->utf8_converter($_GET);
						$data_row['pct_referer'] = $this->randomKey(8);
						$this->create_ambassador_ticker( '0', $data_row['pct_referer']);
						echo $insert_id = $this->ambassador_model->create_ambassador($data_row);
						exit;
					}
			 }
			
		}
		 /**
		 * =========================================================================================================
		 *  section to update ambassador email address through API
		 */
		public function update_ambassador_email()
		{ 
				if(!empty($_GET)){
					
				   	$data_row 	=  $this->utf8_converter($_GET);
					$old_email  =  $data_row['old_email'];
					$email      =  $data_row['email'];
					$type     	=  $data_row['type'];
					
					$user = $this->db->query('SELECT * FROM pct_users WHERE `email`="'.$old_email.'" And `type`="'.$type.'"')->row();
					if($user){
						$this->db->query("UPDATE pct_users SET email='".$email."',dashboard_user_email='".$email."' WHERE email='".$old_email."' AND type='".$type."'");
					}
					
			   }
		
		}
		
		 /**
		 * =========================================================================================================
		 *  section to check rest pan ID using email API
		 */
		public function check_rest_panid()
		{ 
	
			if(isset($_GET['email'])){
					
					$email = $_GET['email'];
					
				   	$user = $this->db->query('SELECT * FROM pct_users WHERE `email`="'.$email.'"')->row();
				   	$user = json_encode($user);
					echo $user;
			   }
		
		}
		
		 /**
         * =========================================================================================================
		 *  section to add create ambassador certificate
         */
		function create_ambassador_certificate($ambassador_id = NULL) { 
				 $user_sql = "SELECT * FROM pct_users WHERE `id`='$ambassador_id'";    
				 $user_res = $this->db->query($user_sql)->row();	
				 
				 $ambassador_sql = "SELECT * FROM pct_initiative_partner WHERE `ambassador_id`='$ambassador_id'";    
				 $amb_res = $this->db->query($ambassador_sql)->result_array();
				 foreach($amb_res as $ambres){
					 $bulker_id  = $ambres['user_id'];
					 $client_treee = "SELECT (SUM(free_trees)+ SUM(tree_nums)) As total_trees FROM pct_initiative_partner WHERE `bulker_id`='$bulker_id'";    
					 $get_client_trees = $this->db->query($client_treee)->row();
					 $client_trees += $get_client_trees->total_trees; 
					 $rest_trees +=  $ambres['free_trees']+$ambres['tree_nums'];
					 
				 }
				 $client_trees = $client_trees + $rest_trees;

				 //$client_trees = $amb_res->total_trees;
				 
				 $initiatives_id = 20;
				 $ini_sql = "SELECT * from pct_initiative WHERE `id`='$initiatives_id'";    
				 $bulk_partner = $this->db->query($ini_sql)->row();
				 $initiative_name = ""; 
				 $ini_certificate_name = $user_res->email;	
				 $calculated_cups = $client_trees*1000; 
				 
				 $client_trees = number_format($client_trees);
				 //echo "<pre>"; print_r($user_res); die;
				$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate1));
				$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate2));
				$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate3));
				$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate4));
				
				$certificate_sub = $certificate_msg = $certificate_text = '';
				$cert_name = 'The_Green_Earth_Appeal_Certificate_Ambassador_'. $ambassador_id . '.pdf';
				
				//$this->create_ambassador_tickers($client_trees, $ambassador_id);
				$bk_image =  $bulk_partner->bk_image;
				require('libraries/cert-initiatives.php');
		
		}
		
		function get_ambassador_data_for_cfd(){
		     $amb_pan_id =  trim($_GET['panacea_id']); 
			 $this->load->database();
			 $ambassador_sql = "SELECT * FROM pct_initiative_partner WHERE `ambassador_id`='$amb_pan_id'";    
			 $amb_res = $this->db->query($ambassador_sql)->result_array();
			 $total_restaurants = 0;
			 foreach($amb_res as $ambres){
				 $bulker_id  = $ambres['user_id'];
				 $client_treee = "SELECT (SUM(free_trees)+ SUM(tree_nums)) As total_trees FROM pct_initiative_partner WHERE `bulker_id`='$bulker_id'";    
				 $get_client_trees = $this->db->query($client_treee)->row();
				 $client_trees += $get_client_trees->total_trees; 
				 $rest_trees +=  $ambres['free_trees']+$ambres['tree_nums'];
				 $total_restaurants +=  1;
				 
			 }
			 $client_trees = $client_trees + $rest_trees;
			 $amb_query = "SELECT u.*,r.code FROM pct_users u INNER JOIN pct_referer r ON u.id=r.user_id WHERE u.id='$amb_pan_id'";   		
			 $row = $this->db->query($amb_query);
			 $data = $row->row();
			 $data->total_trees = $client_trees;
			 $data->total_restaurants = $total_restaurants;
			 //echo "<pre>"; print_r($data); die;	
			 echo  json_encode($data);
		}
		
		function get_monthwise_ambassador_trees_old($ambassador_id = Null){
			
			$this->load->database();
			$ambassador_sql = "SELECT user_id FROM pct_initiative_partner WHERE `ambassador_id`='$ambassador_id'";    
			$amb_res = $this->db->query($ambassador_sql)->result_array();
			$rest_ids = implode("','", array_column($amb_res, "user_id"));
			 
			$query = "SELECT LEFT((MONTHNAME(date_created)),3) as label,YEAR(date_created)as year, (SUM(`trees`)) as y FROM `pct_invoices` WHERE `user_id` IN ('".$rest_ids."') group by MONTH(`date_created`) order by year(date_created), month(date_created) ASC";	
			$data = $this->db->query($query)->result_array();
			  //echo "<pre>"; print_r($data); die;	
			 
			$query1 = "SELECT LEFT((MONTHNAME(created_at)),3) as label,YEAR(created_at)as year, (SUM(`free_trees`)) as y FROM `pct_initiative_partner` WHERE `user_id` IN ('".$rest_ids."') group by MONTH(`created_at`)";	
			$data1 = $this->db->query($query1)->result_array();
			$data_rows = array_merge($data1, $data);
			 
			$datanew =  array();
			$total_trees =  0;
			foreach($data_rows as $_data_row){
				
				if(isset($datanew[$_data_row['label'].$_data_row['year']])){
				   $datanew[$_data_row['label'].$_data_row['year']]['y'] +=  $_data_row['y'];
					
				}else{
					$datanew[$_data_row['label'].$_data_row['year']]= $_data_row;
				}
				$total_trees+= $_data_row['y'];
			}
			$data_rows    =  array_values($datanew);
			//echo "<pre>"; print_r($data_rows); 
			$data_rows['total_trees'] = $total_trees;
			echo $array_final =  json_encode($data_rows, JSON_NUMERIC_CHECK);
			 
		}
		
		function get_monthwise_ambassador_trees($ambassador_id = Null){
			
			$this->load->database();
			$ambassador_sql = "SELECT user_id FROM pct_initiative_partner WHERE `ambassador_id`='$ambassador_id'";    
			$amb_res = $this->db->query($ambassador_sql)->result_array();
			$rest_ids = implode("','", array_column($amb_res, "user_id"));
			 
			$query = "SELECT LEFT((MONTHNAME(date_created)),3) as label,YEAR(date_created)as year, (SUM(`trees`)) as y FROM `pct_invoices` WHERE `user_id` IN ('".$rest_ids."') group by MONTH(`date_created`) order by year(date_created), month(date_created) ASC";	
			$data = $this->db->query($query)->result_array();
			  //echo "<pre>"; print_r($data); die;	
			 
			$query1 = "SELECT LEFT((MONTHNAME(created_at)),3) as label,YEAR(created_at)as year, (SUM(`free_trees`)) as y FROM `pct_initiative_partner` WHERE `user_id` IN ('".$rest_ids."') group by MONTH(`created_at`)";	
			$data1 = $this->db->query($query1)->result_array();
			$data_rows = array_merge($data1, $data);
			 
			$datanew =  array();
			$total_trees =  0;
			foreach($data_rows as $_data_row){
				
				if(isset($datanew[$_data_row['label'].$_data_row['year']])){
				   
				   $datanew[$_data_row['label'].$_data_row['year']]['y'] +=  $_data_row['y'];
					
				}else{
					$datanew[$_data_row['label'].$_data_row['year']]= $_data_row;
				}
				$total_trees+= $_data_row['y'];
			}
			$monthly_data =  array();
			foreach($datanew as $_datanew){
				 $mnth_no = date_parse($_datanew['label']);
				 $mnth_no = $mnth_no['month'];
				 if($mnth_no < 10){
					 $mnth_no = '0'.$mnth_no;
				 }
				 $monthly_data[$_datanew['year'].$mnth_no]    =   $_datanew;
			}
			ksort($monthly_data);
			$data_rows    =  array_values($monthly_data);
			 
			$data_rows['total_trees'] = $total_trees;
			echo $array_final =  json_encode($data_rows, JSON_NUMERIC_CHECK); 
			 
		}
	
		function get_monthwise_all_ambassador_trees($company = NULL){
			
			$this->load->database(); 
			/* // create connection with CFD */
			$db_name = "carbonfd_cfddb";
			$db_host = "localhost";
			$db_user = "carbonfd_cfddbus";
			$db_pass = "vVZKUCXr@wAU";
			
			$con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
			// Check connection
			if (mysqli_connect_errno()){
			  echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
			
			if($company!=""){
				$amb_sql = 'SELECT panacea_id from cfwp_hubspot_ambassador_list WHERE eu_ambassador="1" AND eu_manager_company = "'.$company.'"';
			}else{
				$amb_sql = "SELECT panacea_id from cfwp_hubspot_ambassador_list WHERE eu_ambassador='1'";
			}
			$amb_ids = array();
			$amb_result = mysqli_query($con,$amb_sql);
			 if(mysqli_num_rows($amb_result) > 0) {
					while($row = mysqli_fetch_assoc($amb_result)) {		
						$amb_ids[] = $row['panacea_id'];
					}
			}
			if(empty($amb_ids)){
				echo $amb_ids;
				exit;
			}
			
			 $ambassador_ids = implode("','", $amb_ids);
			 $ambassador_sql = "SELECT user_id FROM pct_initiative_partner WHERE `ambassador_id` IN ('$ambassador_ids')";   
			 $amb_res = $this->db->query($ambassador_sql)->result_array();
			 $rest_ids = implode("','", array_column($amb_res, "user_id"));
			 
			 $query = "SELECT LEFT((MONTHNAME(date_created)),3) as label,YEAR(date_created)as year, (SUM(`trees`)) as y FROM `pct_invoices` WHERE `user_id` IN ('".$rest_ids."') group by MONTH(`date_created`) order by year(date_created), month(date_created) ASC";	
			 $data = $this->db->query($query)->result_array();
			  //echo "<pre>"; print_r($data); die;	
			 
			 $query1 = "SELECT LEFT((MONTHNAME(created_at)),3) as label,YEAR(created_at)as year, (SUM(`free_trees`)) as y FROM `pct_initiative_partner` WHERE `user_id` IN ('".$rest_ids."') group by MONTH(`created_at`)";	
			 $data1 = $this->db->query($query1)->result_array();
			 $data_rows = array_merge($data1, $data);
			 
			 $datanew =  array();
			 $total_trees =  0;
			 foreach($data_rows as $_data_row){
				
				if(isset($datanew[$_data_row['label'].$_data_row['year']])){
				   $datanew[$_data_row['label'].$_data_row['year']]['y'] +=  $_data_row['y'];
					
				}else{
					$datanew[$_data_row['label'].$_data_row['year']]= $_data_row;
				}
				$total_trees+= $_data_row['y'];
			}
			$data_rows    =  array_values($datanew);
			//echo "<pre>"; print_r($data_rows); 
			$data_rows['total_trees'] = $total_trees;
			echo $array_final =  json_encode($data_rows, JSON_NUMERIC_CHECK); 
			 
		}
		
		/************************************************************************
		**** Function ALL the Top Performing Restaurants planted trees  ********
		*************************************************************************/
		function get_all_top_performing_restaurants_trees($dashboard_page = NUll, $company = NULL){
			 
			 $this->load->database();
			 
			/* // create connection with CFD */
			$db_name = "carbonfd_cfddb";
			$db_host = "localhost";
			$db_user = "carbonfd_cfddbus";
			$db_pass = "vVZKUCXr@wAU";
			
			$con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
			// Check connection
			if (mysqli_connect_errno()){
			  echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
			if($company!=""){
				$amb_sql = 'SELECT panacea_id from cfwp_hubspot_ambassador_list WHERE eu_ambassador="1" AND eu_manager_company = "'.$company.'"';
			}else{
				$amb_sql = "SELECT panacea_id from cfwp_hubspot_ambassador_list WHERE eu_ambassador='1'";
			}
			
			$amb_result = mysqli_query($con,$amb_sql);
			 if(mysqli_num_rows($amb_result) > 0) {
					while($row = mysqli_fetch_assoc($amb_result)) {		
						$amb_ids[] = $row['panacea_id'];
					}
			}
			 $ambassador_ids = implode("','", $amb_ids);
			 $ambassador_sql = "SELECT user_id as rest_id FROM pct_initiative_partner WHERE `ambassador_id` IN ('$ambassador_ids')";   
			 $rest_ids = $this->db->query($ambassador_sql)->result_array();
			 $data_rows = array();
             foreach($rest_ids as $rest_id){
				 $query = "SELECT SUM(trees) as trees,user_id, (SELECT Sum(tree_nums+free_trees) FROM pct_initiative_partner WHERE user_id='".$rest_id['rest_id']."') AS tree_nums2,(SELECT company FROM pct_initiative_partner WHERE user_id='".$rest_id['rest_id']."') AS company,(SELECT date_created FROM pct_invoices WHERE user_id='".$rest_id['rest_id']."' order by date_created DESC Limit 1) AS since_date from pct_invoices where user_id='".$rest_id['rest_id']."' group by user_id order by date_created DESC";
				 $result = $this->db->query($query)->row_array();
				 if(empty($result)){
					$query = "SELECT Sum(pi.tree_nums+pi.free_trees) as total_trees,pi.company,pi.user_id,pi.created_at as since_date,pu.email FROM pct_initiative_partner as pi INNER JOIN pct_users as pu on pu.id=pi.user_id WHERE pi.user_id='".$rest_id['rest_id']."'";
					$result = $this->db->query($query)->row_array();
				 }else{
					 $result['total_trees'] = $result['trees'] + $result['tree_nums2'];
				 }
				 $data_rows[] =  $result;	
			 
			 }
			 if($dashboard_page == 1){
				 $data_rows = $this->subval_sort($data_rows,'total_trees'); 
				 $data_rows 	= array_slice($data_rows, 0, 4);                     //to get only top four restuarants
			 }
			 //echo "<pre>"; print_r($data_rows); 
			 echo $array_final =  json_encode($data_rows); 
			 
		}
		/************************************************************************
		**** Function get the Top Performing Restaurants planted trees  ********
		*************************************************************************/
		function get_top_performing_restaurants_trees($ambassador_id = Null,$dashboard_page = NUll){
			 $this->load->database();
			 
			 $ambassador_sql = "SELECT user_id as rest_id FROM pct_initiative_partner WHERE `ambassador_id`='$ambassador_id'";    
			 $rest_ids = $this->db->query($ambassador_sql)->result_array();
			 $data_rows = array();
             foreach($rest_ids as $rest_id){
				 $query = "SELECT SUM(trees) as trees,user_id, (SELECT Sum(tree_nums+free_trees) FROM pct_initiative_partner WHERE user_id='".$rest_id['rest_id']."') AS tree_nums2,(SELECT company FROM pct_initiative_partner WHERE user_id='".$rest_id['rest_id']."') AS company,(SELECT date_created FROM pct_invoices WHERE user_id='".$rest_id['rest_id']."' order by date_created DESC Limit 1) AS since_date from pct_invoices where user_id='".$rest_id['rest_id']."' group by user_id order by date_created DESC";
				 $result = $this->db->query($query)->row_array();
				 if(empty($result)){
					$query = "SELECT Sum(pi.tree_nums+pi.free_trees) as total_trees,pi.company,pi.user_id,pi.created_at as since_date,pu.email FROM pct_initiative_partner as pi INNER JOIN pct_users as pu on pu.id=pi.user_id WHERE pi.user_id='".$rest_id['rest_id']."'";
					$result = $this->db->query($query)->row_array();
				 }else{
					 $result['total_trees'] = $result['trees'] + $result['tree_nums2'];
				 }
				 $data_rows[] =  $result;	
			 
			 }
			 if($dashboard_page == 1){
				 $data_rows = $this->subval_sort($data_rows,'total_trees'); 
				 $data_rows 	= array_slice($data_rows, 0, 4);                     //to get only top four restuarants
			 }
			 //echo "<pre>"; print_r($data_rows); 
			 echo $array_final =  json_encode($data_rows); 
			 
		}
		function get_ambassadors_restaurants(){
		     $amb_pan_id =  trim($_GET['amb_pan_id']); 
			 $this->load->database();
			 //$query = "SELECT * from pct_initiative_partner where ambassador_id=$amb_pan_id";
			 
			 $query = "SELECT bp.*, u.*,r.code AS code, (SELECT latest_certificate FROM pct_invoices WHERE user_id=u.id ORDER BY id DESC LIMIT 1) AS latest_certificate,(SELECT SUM(trees) FROM pct_invoices WHERE user_id=u.id) AS tree_nums2
							FROM pct_initiative_partner AS bp
							INNER JOIN pct_users AS u
							ON u.id=bp.user_id
							INNER JOIN pct_referer AS r
							ON r.user_id=bp.user_id 
							WHERE bp.ambassador_id=$amb_pan_id";
					
			 $row = $this->db->query($query);
			 $data = $row->result_array();
			 //echo "<pre>"; print_r($data); die;	
			 echo  json_encode($data);
		}
		
		function create_ambassador_ticker($trees, $ambassador_id) {

                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/ambassador_ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
                //$text = number_format($trees);
                $text = $trees;
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                 $fname = dirname(__FILE__) . '/../../tickers/ambassador_tickers/ticker_' . $ambassador_id . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }

		 /**
         * =========================================================================================================
			function to encode utf8
         */
		function utf8_converter($array)       
		{
			array_walk_recursive($array, function(&$item, $key){
				if(!mb_detect_encoding($item, 'utf-8', true)){
						$item = utf8_encode($item);
				}
			});
		 
			return $array;
		}
		
		/**
         * =========================================================================================================
			function to soring mulitdimentional array 
         */
		function subval_sort($a,$subkey) {
				foreach($a as $k=>$v) {
					$b[$k] = strtolower($v[$subkey]);
				}
				arsort($b);
				foreach($b as $key=>$val) {
					$c[] = $a[$key];
				}
				return $c;
		}
		
		function randomKey($length) {  //create no of random digit alphanumeric code
			$key='';
			$pool = array_merge(range(0,9),range('A', 'Z'));

			for($i=0; $i < $length; $i++) {
				$key .= $pool[mt_rand(0, count($pool) - 1)];
			}
			return $key;
		}
}
