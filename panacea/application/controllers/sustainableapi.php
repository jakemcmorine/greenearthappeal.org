<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sustainableapi extends CI_Controller {
	
	function get_partner_for_sustaiable_meeting(){
		 $pan_id =  trim($_GET['pan_id']);
		 $this->load->database();
		 $query = $this->db->query("SELECT id FROM pct_users where id='$pan_id' AND type='intitiative'");  
		 $row = $query->row();
		 $data = array();
		  if($row->id!=""){
			$query = "SELECT bp.*, u.*,r.code AS code, (SELECT latest_certificate FROM pct_invoices WHERE user_id=u.id ORDER BY id DESC LIMIT 1) AS latest_certificate,(SELECT SUM(trees) FROM pct_invoices WHERE user_id=u.id) AS tree_nums2
						FROM pct_initiative_partner AS bp
						INNER JOIN pct_users AS u
						ON u.id=bp.user_id
						INNER JOIN pct_referer AS r
						ON r.user_id=bp.user_id 
						WHERE u.id=$row->id";
				
				$row = $this->db->query($query);
				$data = $row->row_array();
				//echo "<pre>"; print_r($data); die;	
				 echo  json_encode($data);
		  }
		}
		
		function get_onupload_monthwise_partner_trees($partner_id = Null){          // to get data from invoice table if onupload is selected
		
			  $this->load->database();
			
			  if($partner_id!=""){
				  $query = "SELECT LEFT((MONTHNAME(date_created)),3) as label,YEAR(date_created)as year, (SUM(`trees`)) as y FROM `pct_invoices` WHERE `user_id` = '$partner_id'  group by MONTH(`date_created`) order by year(date_created), month(date_created) ASC";	
					$row = $this->db->query($query);
			 	  $query1 = "SELECT LEFT((MONTHNAME(created_at)),3) as label,YEAR(created_at)as year, (SUM(`free_trees`)) as y FROM `pct_initiative_partner` WHERE `user_id` = '$partner_id' group by MONTH(`created_at`)";	
	
					$row1 = $this->db->query($query1);
					$data1 = $row1->row_array();
					
					$data = $row->result_array();
					
					foreach($data as &$dat){
						if($dat['label'] == $data1['label'] && $dat['year'] == $data1['year']){
							
							$dat['y'] += $data1['y'];
							
							$added = true;
						}
					}
					//$added = false;
					if(!isset($added)){
						
					$data = array_merge(array($data1),$data);	
						
					}
					 $total_trees = 0;
					 foreach($data as $tree){
						 $total_trees += $tree['y']; 
					 }
					if(count($data)>12){                              //to show latest 12 records
						$old_records = count($data)-12;
						$data = array_slice($data,$old_records);
					}
					 $data['total_trees'] = $total_trees;
					//echo "<pre>"; print_r($data); 
					echo $array_final =  json_encode($data, JSON_NUMERIC_CHECK); 
			  }
		}
		
		function get_monthly_monthwise_partner_trees($partner_id = Null){
			 $this->load->database();
			
			  if($partner_id!=""){
				$query = "SELECT LEFT((MONTHNAME(created_at)),3) as label,YEAR(created_at)as year, (SUM(`tree_nums`)+SUM(`free_trees`)) as y FROM `pct_initiative_partner` WHERE `bulker_id` = '$partner_id' OR user_id = '$partner_id' group by MONTH(`created_at`)";	
					$row = $this->db->query($query);
					$data = $row->result_array();
					//echo "<pre>"; print_r($data); 
					 $total_trees = 0;
					 foreach($data as $tree){
						 $total_trees += $tree['y']; 
						
					 }
					if(count($data)>12){                              //to show latest 12 records
						$old_records = count($data)-12;
						$data = array_slice($data,$old_records);
					}
					 $data['total_trees'] = $total_trees;
					  echo $array_final =  json_encode($data, JSON_NUMERIC_CHECK); 
			  }
			
			
		}
}
