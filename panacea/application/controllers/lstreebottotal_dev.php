<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lstreebottotal_dev extends CI_Controller {

		/**
		 * Index Page for this controller.
		 *
		 * Maps to the following URL
		 * 		http://example.com/index.php/welcome
		 *	- or -  
		 * 		http://example.com/index.php/welcome/index
		 *	- or -
		 * Since this controller is set as the default controller in 
		 * config/routes.php, it's displayed at http://example.com/
		 *
		 * So any other public methods not prefixed with an underscore will
		 * map to /index.php/welcome/<method_name>
		 * @see http://codeigniter.com/user_guide/general/urls.html
		 */
		        public function __construct() {
                parent::__construct();
                session_start();
                $this->data = array();
                $this->load->helper('url');
                $this->load->helper('form');
				$this->load->model('user_model');
                $segment_login = $this->uri->segment(2);
                require_once('phpmailer/class.phpmailer.php');
                
        }
		public function index()
		{
			$this->load->view('welcome_message');
		}
		public function ls_tree_daily_total($getday = NULL)
		{
			$db_name = "greenapp_lightspeed_api_data";
			$db_host = "localhost";
			$db_user = "greenapp_botuser";
			$db_pass = "4{b1hb[tlW2Q";

			$con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
			// Check connection
			if (mysqli_connect_errno())
			  {
			  echo "Failed to connect to MySQL: " . mysqli_connect_error();
			  }

			if(isset($getmonth)){
		    	$sql = "SELECT ad.date, SUM(ad.nocfd) as tree_nums, SUM(ad.noreceipts) as noreceipts FROM ls_api_data as ad WHERE ad.date = $getday";
			}else{
			   $sql = "select t.date, SUM(t.tree_nums) as total_tree_nums, SUM(t.noreceipts) as total_noreceipts FROM
(SELECT date, SUM(nocfd) as tree_nums, SUM(noreceipts) as noreceipts FROM greenapp_lightspeed_api_data.ls_api_data WHERE date = subdate(curdate(), 1)
UNION ALL
SELECT date, SUM(nocfd) as tree_nums, SUM(noreceipts) as noreceipts FROM greenapp_partner.tevalis_api_data WHERE date = subdate(curdate(), 1)) t";
			}
			$result = mysqli_query($con,$sql);

			$initiatives_ids = array();
			 if(mysqli_num_rows($result) > 0) {
				  while($row = mysqli_fetch_assoc($result)) {
					   //echo "<pre>"; print_r($row); 
					   //$this->add_ls_restaurant_client($row);    
					   //$company_id = $row['companyid'];
					   //$rest_date = $row['date'];
					   //$panid = $row['panid'];
					   
					   //$sqlss = "SELECT u.*,bp.initiative FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.id='$panid'";    
					   //$res_record = $this->db->query($sqlss)->row();
					  
					   //$initiatives_ids[] = $res_record->initiative;
					   
					   $this->send_data_to_slack($row);    //send data to slack
					   
					   //$update_sql = "UPDATE `ls_api_data` SET processed='1' WHERE companyid='".$company_id."' AND date='".$rest_date."'";
					   //mysqli_query($con,$update_sql); 
					   
					   
				  }
				  //$this->create_ticker_certificates($initiatives_ids);
			 }
			 
			 
		    
		}
		 /**
         * =========================================================================================================
		 *  section to add send_data_to_slack
         */
			function send_data_to_slack($data) {   
					$thedate = date('d M Y',strtotime($data[date]));
					 $array = array( 'text' => "Tree Planting Total for $thedate",
								'attachments' => array(
										array('title' => " ",
											   'color' => "#ff2b2e",
											   'thumb_url' => "https://greenearthappeal.org/images/tree_icon.png",
											   'fields' => array(
													array('title' => "Total Bills",
														  'value' => $data['total_noreceipts'],
														  'short' => true
														 ),
													array('title' => "Carbon Free Diners",
														 'value' => $data['total_tree_nums'],
														 'short' => true
														 )
													)
											))); 
					// Create a constant to store your Slack URL
					 define('SLACK_WEBHOOK', 'https://hooks.slack.com/services/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');
					  // Make your message
					  $message = array('payload' => json_encode($array));
					  // Use curl to send your message
					  $c = curl_init(SLACK_WEBHOOK);
					  curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
					  curl_setopt($c, CURLOPT_POST, true);
					  curl_setopt($c, CURLOPT_POSTFIELDS, $message);
					  curl_exec($c);
							if (curl_errno($c)) {
									echo 'Error:' . curl_error($c);
								} 
					  curl_close($c); 
					 return;
			}
			
		  /**
         * =========================================================================================================
         * check_user
         */
        function check_user($user_type = 'admin') {
                if (!is_array($user_type)) {
                        $user_type = (array) $user_type;
                }

                $check = in_array($_SESSION['login']['type'], $user_type);
                if (!$check) {
                        echo 'You dont have permission.';
                        die();
                }
                return true;
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */