<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eposnow extends CI_Controller {

		public function __construct() {
			
			parent::__construct();
			session_start();
			
			$this->data = array();
			$this->load->helper('url');
			$this->load->database();
			$this->load->model('user_model');
			$segment_login = $this->uri->segment(2);
			
			require_once('phpmailer/class.phpmailer.php');
			
			/* PosCrafts database connection */
			$db_name = "carbonfd_cfddb";
			$db_host = "localhost";
			$db_user = "carbonfd_cfddbus";
			$db_pass = "vVZKUCXr@wAU";

			$this->con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
			// Check  database connection
			if (mysqli_connect_errno())
			{
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
				exit;
			}
                
        }
		
		/********************************************* 
		  Function to get data from CFD eposnow
		**********************************************/
		
		public function upload_eposnow_transactions() 
		{	
			$sql = "SELECT id as transaction_id,panacea_id as panid,Trees as tree_nums from `cfwp_eposnow_transactions` WHERE processed = '0' ORDER BY id ASC LIMIT 1";
			$result = mysqli_query($this->con,$sql);
			
			$post_data = [];   
			if(mysqli_num_rows($result) > 0) {
				while($row = mysqli_fetch_assoc($result)) {
					$post_data[] =  $row;
					   
				}
			}
			
			//$post_data = file_get_contents('php://input');
			//$post_data = json_decode( $post_data, true);
			//echo "<pre>"; print_r($post_data); exit;
			
			if($post_data){
				  
				foreach($post_data as $row){
						
					//echo "<pre>"; print_r($row); die("here");
					$this->add_eposnow_restaurant_client($row);  

					$transaction_id = $row['transaction_id'];
					$panid 			= $row['panid'];

					$update_sql = "UPDATE `cfwp_eposnow_transactions` SET processed='1' WHERE id='".$transaction_id."'";
					mysqli_query($this->con,$update_sql); 			

					//$this->send_data_to_slack($row);    //send data to slack

					//$sqlss = "SELECT u.*,bp.initiative FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.id='$panid'";    
					//$res_record = $this->db->query($sqlss)->row();

					//to create intiatives ticker and certificates
					//$this->create_ticker_certificates($res_record->initiative);         

				}
				echo "Record uploaded successfully!";
				exit;
		     
			}
			
		}
		 
		/********************************************* 
		    Section to add send_data_to_slack
		**********************************************/
		
		function send_data_to_slack($data) 
		{ 
			$thedate = date('d M Y',strtotime($data['date']));
			$array = array( 'text' => "Tree Planting for $thedate",
					'attachments' => array(
						array('title' => $data['compname'],
						   'color' => "#10672f",
						   'thumb_url' => "https://greenearthappeal.org/images/tree_icon.png",
						   'fields' => array(
							array('title' => "Total Bills",
							  'value' => $data['noreceipts'],
							  'short' => true
							),
							array('title' => "Carbon Free Diners",
							 'value' => $data['tree_nums'],
							 'short' => true
							)
							)
						))
					); 
					
			// Create a constant to store your Slack URL
			define('SLACK_WEBHOOK', 'https://hooks.slack.com/services/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');
			$message = array('payload' => json_encode($array));
			$c = curl_init(SLACK_WEBHOOK);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, $message);
			curl_exec($c);
				if (curl_errno($c)) {
					echo 'Error:' . curl_error($c);
				} 
			curl_close($c); 
			return;
		}
		
		
		/********************************************* 
		   Section to create ticker certificates 
		**********************************************/
		
		function create_ticker_certificates($initiatives_id = NULL) {   
					
			if($initiatives_id){
				
				$initiative_sql  = 	"SELECT (SUM(free_trees)+ SUM(tree_nums)) As total_trees FROM pct_initiative_partner WHERE `initiative`='$initiatives_id'";    
				$ini_res 	  	 = 	$this->db->query($initiative_sql)->row();
				$client_trees 	 = 	$ini_res->total_trees;

				$ini_sql = "SELECT * from pct_initiative WHERE `id`='$initiatives_id'";    
				$bulk_partner 			=  $this->db->query($ini_sql)->row();
				$initiative_name 		=  $bulk_partner->initiative_name; 
				$ini_certificate_name 	=  $bulk_partner->ini_certificate_name; 
				$restaurant 			=  $bulk_partner->initiative_name; 
				$calculated_cups 		=  $client_trees*1000; 

				$client_trees = number_format($client_trees);
				//echo "<pre>"; print_r($bulk_partner); 
				$certificate_mess1 	= str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate1));
				
				$certificate_mess2 	= str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate2));
				
				$certificate_mess3 	= str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate3));
				
				$certificate_mess4 	= str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate4));

				$certificate_sub = $certificate_msg = $certificate_text = '';
				$cert_name 	= 'The_Green_Earth_Appeal_Certificate_Initiative_'. $initiatives_id . '.pdf';

				$this->create_initiative_ticker($client_trees, $initiatives_id);
				$bk_image =  $bulk_partner->bk_image;
				require('libraries/cert-initiatives.php');
				
			}
		
		}
		
		/***************************************************************
		 Section to add clients automatically to restuarants
		***************************************************************/
		
		function add_eposnow_restaurant_client($res_data) {    

			$transaction_id  = $res_data['transaction_id'];
			
			if ($res_data)
			{
				$total_trees = $free_trees = $old_user = $old_rows = $new_rows = $client_trees = 0;
				
				$panid 	= $res_data['panid'];
				$transaction_id = $res_data['transaction_id'];
				
				$sql = "SELECT u.*,bp.first_name,bp.last_name,bp.company FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.id='$panid'";    
				$restaurant_data = $this->db->query($sql)->row();
				
				$res_data['first_name'] = $restaurant_data->first_name;
				$res_data['last_name'] 	= $restaurant_data->last_name;
				$res_data['email'] 		= $restaurant_data->email;
				$res_data['company'] 	= $restaurant_data->company;
				$res_data['free_trees'] = '0';
				 
				 $_SESSION['login'] = (array)$restaurant_data;   
			 
				if (is_array($res_data) && count($res_data)) 
				{
					$data_row = array();
					$data_row =  $res_data;
					$data_row['password'] = $this->randomKey(6);
					
					$rff_temp = str_replace(" ","_",$data_row['company']);           //referid as company name
					$data_row['referer_id'] = str_replace("’","",strtolower($rff_temp));
					$data_row['username'] 	= $data_row['email'];
					
					// create referer id
					$referer_id = trim($data_row['referer_id']);
					$i = 0;
					while (!$this->referer_check($referer_id)) {
							$i++;
							$referer_id = $data_row['referer_id'] . $i;
					}
					$data_row['referer_id'] = $referer_id;
					$data_row['short_code'] = $referer_id;    
					
					if($data_row['free_trees'] == ""){
						
						$data_row['free_trees']=0;
						
					}
					$client_trees = $data_row['tree_nums'];
				
					if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['login']['id'])) {       
						  
						 	$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
							$query = $this->db->query($qr);
							
							if ($query->num_rows()) {
								   
								$rowq = $query->row_array();
								$tr = $rowq['tree_nums'];
								$f_tr = $tr + $data_row['tree_nums']+$rowq['free_trees'];
								
								$qru = "UPDATE pct_initiative_partner SET tree_nums = '" . $f_tr . "' WHERE user_id = '" . $rowq['user_id'] . "' ";
								$this->db->query($qru);
							}
							continue;
					}
					if (!$this->initiative_username_check($data_row['email'],$data_row['company'])) {
						
							$old_user = 1;
							$qr = 'SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email="' . $data_row['email'] . '" AND u.type="intitiative_client" AND bp.company="' . $data_row['company'] . '"';
							$query = $this->db->query($qr);
							if ($query->num_rows()) {
								
								$rowq = $query->row_array();
								$tr = $rowq['tree_nums'];
								$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
								
							}
							$client_trees = $f_tr;
							$check_em   = "SELECT * FROM pct_referer WHERE user_id='" . $rowq['user_id'] . "' ";    //to get old referr_id
							$getuser_id = $this->db->query($check_em);
							
							if($getuser_id->num_rows()) {
								  $ress = $getuser_id->row_array();
								  $data_row['referer_id'] = $ress['code'];
							 }
							//continue;
					}

					if(isset($_SESSION['login']['id'])){
					
						$check_ini_id = "SELECT * FROM pct_initiative_partner WHERE user_id='" . $_SESSION['login']['id'] . "' ";    //to get  initiative id
						$getini_id = $this->db->query($check_ini_id);
						
						if ($getini_id->num_rows()) {
							$ress1 = $getini_id->row_array();
							$data_row['initiative_id'] = $ress1['initiative'];
						}
					}
					
					if ($old_user == 0) {
						
						$data_row['old_user'] = $old_user;
						$data_row['client_trees'] = $client_trees;
						$new_rows++;
							
					} else if ($old_user == 1) {
						
						$data_row['old_user'] = $old_user;
						$data_row['client_trees'] = $client_trees;
						$old_rows++;
							
					}

					$data_rows[] = $data_row;
					$total_trees += $data_row['tree_nums'];
					$free_trees += $data_row['free_trees'];
					
				}
				
				$price = $_SESSION['login']['price'];
				
				$bulk_partner 	= $this->user_model->load_initiative($_SESSION['login']['id']);
				$invoice_trees  = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
				$current_trees  = $total_trees;
				$all_free_trees = $free_trees;
				
				$all_invoice_trees = $total_trees+$free_trees;
				//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
				$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums']+ $bulk_partner['free_trees'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']; 
				$tax = $_SESSION['login']['tax'];
				$sub_total 	 = $current_trees * $price / 100;
				$total_price = $sub_total * ( 100 + $tax ) / 100;
				$total_price = number_format($total_price, 2);
				
				$data = array('title' => 'Confirmation', 'body' => 'initiative_confirmation', 'total_trees' => $total_trees, 'current_trees' => $all_invoice_trees, 'total_price' => $total_price, 'tax' => $tax, 'sub_total' => $sub_total, 'total_rows' => $new_rows, 'total_rows_updated' => $old_rows);
				$this->data = array_merge($this->data, $data);
				
				if (count($data_rows)) {
					
					$_SESSION['login']['data'] = array();
					
					$_SESSION['login']['data']['rows'] 				= 	json_encode($data_rows);
					$_SESSION['login']['data']['total'] 			= 	$total_price;
					$_SESSION['login']['data']['total_trees'] 		= 	$total_trees;
					$_SESSION['login']['data']['current_tree'] 		= 	$current_trees;
					$_SESSION['login']['data']['all_invoice_trees'] = 	$all_invoice_trees;
					$_SESSION['login']['data']['all_free_trees']	= 	$all_free_trees;
					$_SESSION['login']['data']['transaction_id'] 	= 	$transaction_id;

					// $_SESSION['login']['data']['code']   = $referer_id;
					$_SESSION['login']['data']['address'] 	= $bulk_partner['address'];
					$_SESSION['login']['data']['city'] 		= $bulk_partner['city'];
					$_SESSION['login']['data']['state'] 	= $bulk_partner['state'];
					$_SESSION['login']['data']['post_code'] = $bulk_partner['post_code'];

					$process_sql = "UPDATE `cfwp_eposnow_transactions` SET processed='2' WHERE id='".$transaction_id."'";
					mysqli_query($this->con, $process_sql); 
					
					$this->confirm_epsonow_restaurant_intitiative();
						
				}
			}
		}
		
		/***************************************************************
		 Section to create certificates and tickers
		***************************************************************/

        function confirm_epsonow_restaurant_intitiative() 
		{
			$this->check_user('intitiative');
			
			if (!isset($_SESSION['login']['data']) || empty($_SESSION['login']['data']['rows'])) {
				unset($_SESSION['login']['data']);
				redirect(site_url());
				die();
			}
			$data = json_decode($_SESSION['login']['data']['rows']);
			
			//echo "<pre>"; print_r($data ); die;
			$bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
			
			$initiative 		  =  $bulk_partner['initiative_name'];
			$initiative_id 		  =  $bulk_partner['initiative'];
			$activation_email 	  =  $bulk_partner['activation_email'];
			$send_invoices 		  =  $bulk_partner['send_invoices'];
			$invoice_free_trial   =  $bulk_partner['free_trial'];
			$request_for_funds    =  $bulk_partner['request_for_funds'];
			$attach_cert_inemails =  $bulk_partner['attach_cert_inemails'];
			$counter_type 		  =  $bulk_partner['counter_type'];                      // check if partner counter is selected
			$ini_counter_type     =  $bulk_partner['ini_counter_type'];					 // check if initiative counter is selected
			$bk_image 			  =  $bulk_partner['bk_image'];
			$partner_bk_image 	  =  $bulk_partner['partner_bk_image'];

			if($partner_bk_image != ""){                                   				//check partner background cert image first
				$bk_image 	=  $bulk_partner['partner_bk_image'];
			}else{
				$bk_image 	=  $bulk_partner['bk_image'];
			}
		   
			$_SESSION['figure_history_key'] = $this->randomKey(10);
			$config = array();
			$trial_trees = $redirect = 0; 
			$opportunity_str = '';
			$current_month_text = date("F_Y");
			
			foreach ($data AS &$row) {
				
				$row = (array) $row;
				$unique_url =  $bulk_partner['website_url'].'/?refid=' . $row['referer_id'];
				// create the ticker
				$total_client_trees = $row['client_trees'] + $row['free_trees'];
					
				//$this->create_cfd_ticker($total_client_trees, $row['referer_id']);        //CFD tickers
					
					if($counter_type != 0)
					{																				 // if counter is set on partner
						if($counter_type == 1)
						{
							$this->create_ticker($total_client_trees, $row['referer_id']);            //GEA tickers
						}else
						{
							$this->create_cfd_ticker($total_client_trees, $row['referer_id']);         //CFD tickers
						}
						
					}else
					{                                                                                 // if counter is set on initiative
						if($ini_counter_type == 1)
						{
							$this->create_ticker($total_client_trees, $row['referer_id']);            //GEA tickers
						}else{
							$this->create_cfd_ticker($total_client_trees, $row['referer_id']);        //CFD tickers
						}
					}
					
					$this->create_cfd_small_ticker($total_client_trees, $row['referer_id'], 'black');
					
					$this->create_cfd_small_ticker($total_client_trees, $row['referer_id'], 'green');
					
					$this->create_cfd_small_ticker($total_client_trees, $row['referer_id'], 'white');
					
					
					$ticker_url	    = base_url() . 'tickers/ticker_' . $row['referer_id'] . '.png';
					$cert_thumb_url = base_url() . 'cert/thumb_' . $row['referer_id'] . '.jpg';
					$cert_image_url = base_url() . 'cert/full_' . $row['referer_id'] . '.jpg';
					$row['activation_key'] = $this->randomKey(6);
					
					$activation_url  = base_url() . 'index.php/user/client_login/' . $row['activation_key'];
					$auto_login_link = base_url() . 'index.php/user/auto_login_client_link/' . $row['activation_key'];
					
					if ($row['old_user'] == 0) 
					{
						if($activation_email==1)
						{
							$email_template = $this->user_model->load_initiative_email_template($initiative_id,2);  // send activation email for new clients
						}else
						{
							$email_template = $this->user_model->load_initiative_email_template($initiative_id,3);  
						}
							
					}else
					{
						$email_template = $this->user_model->load_initiative_email_template($initiative_id,5);     //send email for update clients
					} 
					  
					// create the certificate
					$restaurant   = $row['company'];
					$total_trees  = $row['tree_nums'] + $row['free_trees'];
					$client_trees = $row['client_trees'] + $row['free_trees'];
						
					$calculated_cups = $total_client_trees*1000;
	
					if($client_trees == '1')
					{
						$client_trees = number_format($total_trees).' '.'tree';
					}else
					{
						$client_trees = number_format($total_trees).' '.'trees';
					}
											
					if ($row['old_user'] == 0) {
						
						$row['activation_email']   = $activation_email;
						$row['figure_history_key'] = $_SESSION['figure_history_key'];
						$client_id = $this->user_model->insert_initiative_client($row);
							
					} else {
						
						$row['figure_history_key'] = $_SESSION['figure_history_key'];
						$client_id = $this->user_model->update_initiative_client($row);
							
					}
					
					//to add client certificate url
					$client_certificate_url = 'https://www.greenearthappeal.org/panacea/uploads/certs/clients/'.'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $client_id . '.pdf'; 
					
					$message 	=  $email_template->message;
					
					$all_counters 	= $this->get_cfd_gea_counters();
					
					$message = str_ireplace(array('[contact_person]','[activation_url]','[auto_login_link]','[company_name]','[total_trees]','[current_trees]', '[username]', '[password]', '[unique_url]', '[ticker_url]','[initiative_name]','[calculated_cups]','[cert_thumb_url]','[cert_image_url]','[certificate_url]','[cfd_counter]','[gea_counter]'), array($row['first_name'], $activation_url,$auto_login_link, $row['company'],$total_client_trees, $total_trees, $row['username'], $row['password'], $unique_url, $ticker_url,$initiative,$calculated_cups,$cert_thumb_url,$cert_image_url,$client_certificate_url,$all_counters['cfd_counter'],$all_counters['gea_counter']), $message);
					
					$total_client_trees = number_format($total_client_trees);
	
					$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups,$total_client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate1']));
					
					$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups,$total_client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate2']));
					
					$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups,$total_client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate3']));
					
					$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups,$total_client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate4']));

					$ds = DIRECTORY_SEPARATOR;
					
					$certificate_sub = $certificate_msg = $certificate_text = '';

					$cert_name = 'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $client_id . '.pdf';
					
					if ($row['old_user'] == 0) {
						 
						$row['initiative_name']  = $bulk_partner['initiative_name'];
						$row['unique_url']       =  $unique_url;
						$row['ticker_url'] 	  	 = $ticker_url;
						$row['cert_name'] 	  	 = $cert_name;
						$row['email_title']      = $bulk_partner['sender_email_title'];
						
						$this->user_model->insert_initiative_client_email_records($row,$client_id);
						
					}
					
					$cert_path = dirname(__FILE__) . '/../../uploads/certs/clients/';
					require('libraries/cert-ini.php');

					$image_file_name = date('Ymdhis');
					$image_path 	 = dirname(__FILE__) . '/../../uploads/certs/clients/images/';
					$image_url 		 = dirname(__FILE__) . '/../../cert/';
					$pdf_path 		 = $cert_path.$cert_name;
					
					if (extension_loaded('imagick')){   //extension imagick is installed
					
						$this->create_pdf_to_image($pdf_path, $image_file_name, $image_path); 
						$this->create_cert_pdf_to_image($pdf_path, $row['referer_id'], $image_url); //to create pef images for clients
						
					}	
											
					//echo "<pre>"; print_r($row); die;
					$data3 = array();                       //store clients latest updated certificate
					$data3['client_id'] = $client_id;
					$data3['latest_certificate'] = $cert_name;
					$data3['certificate_image'] = 'certificate_'.$image_file_name.'.jpg';
					
					$this->user_model->insert_client_latest_certificate($data3);
					
					// send email to client
					$config['to']   =  $row['email'];
					//$config['to'] =  'testing.whizkraft1@gmail.com';
				   
					if (isset($certificate_sub) && !empty($certificate_sub)) 
					{
						$config['subject'] = $certificate_sub;
					}else{
						$config['subject'] = $email_template->subject;
					}

					$config['subject'] = str_ireplace(array('[contact_person]', '[company_name]','[initiative_name]','[total_trees]'), array($cname, $restaurant,$initiative,$total_trees), $config['subject']);
									
					if (isset($certificate_msg) && !empty($certificate_msg)) {
						$config['message'] = $certificate_msg;
					} else {
						$config['message'] = $message;
					}
					if($attach_cert_inemails == 1){              //to stop attach client certificate if option in disabled
						$attach_cert = array(dirname(__FILE__) . '/../../uploads/certs/clients/' . $cert_name);
					}else{
						$attach_cert = "";
					}
					if ($row['old_user'] == 0) {
						
						if($activation_email == 1){
							$config['attach'] = "";
						}else{
							$config['attach'] = $attach_cert;
						}
					}else{
						$config['attach'] = $attach_cert;
					}

					$config['email_title'] = $bulk_partner['sender_email_title'];
					$enable_email  = $email_template->enable_email;
					
					if($enable_email == 1){
						$this->send_mail($config);    //disabeled this email
					}
					
					//insert record for send monthly invoice
					
					if($send_invoices == "monthly"){
						
						$record['tree_nums']      = $row['tree_nums'];
						$record['company']   	  = $row['company'];
						$record['free_trees']     = $row['free_trees'];
						$record['initiative_id']  = $row['initiative_id'];
						
						if($request_for_funds == 1)
						{
							$record['request_for_funds']  = '1';
							//$record['restaurant_date']    = $_SESSION['login']['data']['restaurant_date'];
						}
						$record['partner_id']   = $_SESSION['login']['id'];
						$insert_monthly = $this->user_model->insert_initiative_monthly_invoice_records($record);  
						
					}
					
					if ($row['old_user'] == 0) {
						 $trial_trees+= $row['tree_nums'];         //get sum on all new user trees
					}
			}
			
			// prepare to create the invoice
			$cert_path = '';
			$pct_rows = &$data;
			
			$item2 = array();
			$bulk_partner   = $this->user_model->load_initiative($_SESSION['login']['id']);
			$email_template = $this->user_model->load_initiative_email_template($bulk_partner['initiative'],4); 
			
			$item2['name']  	 = "Total number of trees";
			$item2['free_trees'] = $_SESSION['login']['data']['all_free_trees'];
			$item2['unit'] 		 = $_SESSION['login']['price'] / 100;
			
			if($invoice_free_trial == '1'){        //if free trial send 0 invoice for new clients
			
				$item2['tree_nums'] = $_SESSION['login']['data']['current_tree'] - $trial_trees;
			}else{
				
			   $item2['tree_nums'] = $_SESSION['login']['data']['current_tree'];
			}
			
			switch ($_SESSION['login']['currency']) {
				
				case 1:
						$item2['currency'] = '$';
						break;
				case 2:
						$item2['currency'] = '&pound;';
						break;
				case 3:
						$item2['currency'] = '€';
						break;				
			}

			$item2['price'] = $item2['tree_nums'] * $item2['unit'];
			$tax 	  	 	= $_SESSION['login']['tax'];
			$sub_total 		= $currency . $item2['price'];
			$total_no 		= $item2['price'] * (100 + $tax) / 100;
			$total 			= $item2['currency'] . number_format($total_no, 2);
			$total_amt 		= $item2['currency'] . number_format($total_no, 2);

			// store invoice in database
			$data1 = array();
			$data1['user_id'] 	   	= $_SESSION['login']['id'];
			$data1['date_created'] 	= 'NOW()';
			$data1['total'] 		= $total_no;
			$data1['state'] 		= 0;
		   // $data1['trees'] = $_SESSION['login']['data']['current_tree'];
		   
			$data1['figure_history_key'] = $_SESSION['figure_history_key'];
			$data1['trees'] 			 = $_SESSION['login']['data']['all_invoice_trees'];
			
			if($send_invoices == "onupload"){
				$invoice_id = $this->user_model->insert_initiative_invoice($data1);
			}else{
				$invoice_id = 'partner_'.$bulk_partner['id'];
			}
			
			$invoice_company = $cert_company = $bulk_partner['company'];
			$address 	= $_SESSION['login']['data']['address'];
			$city 		= $_SESSION['login']['data']['city'];
			$state 		= $_SESSION['login']['data']['state'];
			$post_code 	= $_SESSION['login']['data']['post_code'];
			$ini_first_name = $bulk_partner['first_name'];

			// create the invoice
			$current_month_text = date("F_Y");
			$invoice_name 		= 'The_Green_Earth_Appeal_Invoice_' . $current_month_text . '_' . $invoice_id . '.pdf';
			$cert_name 			= 'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $invoice_id . '.pdf';
			$invoice_number 	= $invoice_id;
			$invoice_data 		= array();
			$invoice_data['id'] = $invoice_id;
			
			$invoice_data['number'] = $invoice_number;
			$invoice_data['name'] 	= $invoice_name;

			$invoice_data['latest_certificate'] = $cert_name;
			$invoice_data['month'] = date('mY');
			
			$monthly['invoice_id']    	= $invoice_id;
			$monthly['first_name']    	= $bulk_partner['first_name'];
			$monthly['company_name']  	= $bulk_partner['company'];
			$monthly['total_trees'] 	= $_SESSION['login']['data']['total_trees'];
			$monthly['current_trees'] 	= $_SESSION['login']['data']['all_invoice_trees'];
			$monthly['total']			= $total_amt;
			$monthly['ticker_url'] 		= "";
			$monthly['unique_url'] 		= "";
			$monthly['initiative_name'] = $initiative;
			$insert_monthly_invoice_id = "";

			if($send_invoices == "onupload"){
				$this->db->update_batch('pct_invoices', array($invoice_data), 'id');
			}
			
			if($send_invoices == "onupload" && $request_for_funds == 1){
				$invoice_name = 'The_Green_Earth_Appeal_Request_For_Funds_' . $current_month_text . '_' . $invoice_id .'.pdf';
				require('libraries/monthly_funds_request.php');
			}else{
				        if($bulk_partner['enable_invoice_data'] == 1){		
						
							require('libraries/invoice-dynamic.php');           
						}else{					
						
							require('libraries/invoice.php');				
						}
			}
			
			$cname 		= $bulk_partner['first_name'];
			$restaurant = $bulk_partner['company'];


			$total_trees = $_SESSION['login']['data']['total_trees'];
			
			if($total_trees == '1'){
				 $client_trees = number_format($total_trees).' '.'tree';
			}else{
				$client_trees = number_format($total_trees).' '.'trees';
			}
			
			$calculated_cups 	= $total_trees*1000;
			$all_counters 		= $this->get_cfd_gea_counters();	
			
			$this->create_ticker($all_counters['gea_counter'], 'gea_counter');           //create GEA ticker for all partners
			$this->create_cfd_ticker($all_counters['cfd_counter'], 'cfd_counter');		 //create CFD ticker for all partners
			
			$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate1']));
			
			$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate2']));
			
			$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate3']));
			
			$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($restaurant, $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate4']));

			$certificate_sub = $certificate_msg = $certificate_text = '';

			require('libraries/cert-ini.php');
						
			// create the ticker
			$refid 		= $this->user_model->get_refid($_SESSION['login']['id']);
			$ticker_url = base_url() . 'tickers/ticker_' . $refid->code . '.png';
						
			if($counter_type != 0){											      // if counter is set on partner
			
				if($counter_type == 1){
					$this->create_ticker($total_trees, $refid->code);            //GEA tickers
				}else{
					$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
				}
					
			}else{                                                               // if counter is set on initiative
			
				if($ini_counter_type == 1){
					$this->create_ticker($total_trees, $refid->code);            //GEA tickers
				}else{
					$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
				}
			}
				
			$this->create_cfd_small_ticker($total_trees, $refid->code, 'black');

			$this->create_cfd_small_ticker($total_trees, $refid->code, 'green');

			$this->create_cfd_small_ticker($total_trees, $refid->code, 'white');
			
			//to create pdf images for initiatives
			$cert_thumb_url = base_url() . 'cert/thumb_' . $refid->code . '.jpg';
			$cert_image_url = base_url() . 'cert/full_' . $refid->code . '.jpg';
			
			if (extension_loaded('imagick')){   		//extension imagick is installed
				$cert_path = dirname(__FILE__) . '/../../uploads/certs/';
				$pdf_path = $cert_path.$cert_name;
				$image_url = dirname(__FILE__) . '/../../cert/';
				//if($send_invoices=="onupload"){
					$this->create_cert_pdf_to_image($pdf_path, $refid->code, $image_url); 
				//}
			}	

			$unique_url = $bulk_partner['website_url'].'/?refid=' . $refid->code;

			if($send_invoices == "monthly"){
				
				$monthly['id']    			= $insert_monthly_invoice_id;
				$monthly['ticker_url'] 		= $ticker_url;
				$monthly['unique_url'] 		= $unique_url;
				$monthly['cert_thumb_url'] 	= $cert_thumb_url;
				$monthly['cert_image_url'] 	= $cert_image_url;
				//$this->db->update_batch('pct_initiative_monthly_invoice_data', array($monthly), 'id');
			}
			
			// send certificate to the restaurant
			$config = array();
			
		 /*   if($send_invoices=="onupload"){
				 $config['to'] = $_SESSION['login']['email'];
			 }else{
				 $config['to'] = 'marvin@greenearthappeal.org';
				
			 }  */   
			//$config['to'] =  'testing.whizkraft2@gmail.com';
			$config['to']   = $_SESSION['login']['email'];
							
			$subject = $email_template->subject;
			$message = $email_template->message;
			
			$partner_certificate_url = 'https://www.greenearthappeal.org/panacea/uploads/certs/'.$cert_name; //to add partner certificate url  
			
			$config['subject'] = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]', '[unique_url]','[initiative_name]'), array($cname, $restaurant, $total_trees, $data1['trees'], $total_amt, $bulk_partner['first_name'], $ticker_url, $unique_url,$initiative), $subject);
						 
			$message = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]', '[unique_url]','[initiative_name]','[calculated_cups]','[cert_thumb_url]','[cert_image_url]','[certificate_url]','[cfd_counter]','[gea_counter]'), array($cname, $restaurant, $total_trees, $data1['trees'], $total_amt, $bulk_partner['first_name'], $ticker_url, $unique_url,$initiative,$calculated_cups,$cert_thumb_url,$cert_image_url,$partner_certificate_url,$all_counters['cfd_counter'],$all_counters['gea_counter']), $message);
			
			$config['message'] = $message;
			
			if($send_invoices == "onupload" && $request_for_funds == 1){
				$config['attach']  = array(dirname(__FILE__) . '/../../uploads/request_for_funds/' . $invoice_name, dirname(__FILE__) . '/../../uploads/certs/' . $cert_name);
			}else{
				$config['attach']  = array(dirname(__FILE__) . '/../../uploads/invoices/' . $invoice_name, dirname(__FILE__) . '/../../uploads/certs/' . $cert_name);
			}
			$config['email_title'] = $bulk_partner['sender_email_title'];
			
			if($send_invoices == "onupload"){
				 
				$enable_email = $email_template->enable_email;
				
				if($enable_email == 1){
					$this->send_mail($config);
				}
					
			}
			unset($_SESSION['figure_history_key']);
			unset($_SESSION['login']['data']);
			
			return;
			
        }
	
	
	    /**
         * ===============================================
         *  Common function to send emails using SMTP
		 * ===============================================
        */
        function send_mail($data) {
			
			$mail             = new PHPMailer();
			$mail->IsSMTP(); // telling the class to use SMTP
			
			$mail->SMTPDebug  = 0;                     		// enables SMTP debug 1 = errors and messages, 2 = messages only
			$mail->SMTPAuth   = true;                  		// enable SMTP authentication
			$mail->SMTPSecure = "tls";                 		// sets the prefix to the servier
			$mail->Host       = "smtp.gmail.com";      		// sets GMAIL as the SMTP server
			$mail->Port       = 587;                   		// set the SMTP port for the GMAIL server
			$mail->Username   = "partners@greenearthappeal.org";  // GMAIL username
			$mail->Password   = "P8%k<g_39x,#1";            // GMAIL password
			$mail->CharSet 	  = 'UTF-8';

			if (isset($data['from'])) {
				$mail->SetFrom($data['from'], '');
			} elseif(isset($data['email_title'])){
				$mail->SetFrom('partners@greenearthappeal.org', $data['email_title'] );
			}else{
				$mail->SetFrom('partners@greenearthappeal.org', 'Partnerships Department – Green Earth Appeal');
			}

			if (isset($data['attach']) && !empty($data['attach'])) {
				
				if (!is_array($data['attach'])) {
					$data['attach'] = (array) $data['attach'];
				}
				foreach ($data['attach'] AS $attach) {
					$mail->AddAttachment($attach); // attachment
				}
			}

			ob_start();
			
			if ((isset($data['signature']) && $data['signature'] == false) || (!$data['signature'])) {
				
				echo '<style type="text/css">*, p {font-family:Calibri!important; font-size: 11pt!important;}</style>';
				require('libraries/signature.htm');
				
			} else {
				
				require('libraries/signature_solicitor.htm');
				
			}

			$message = ob_get_contents();
			ob_end_clean();

			$message = $data['message'] . $message;

			$mail->ClearReplyTos();
			$mail->AddReplyTo("partners@greenearthappeal.org", "Partnership Department - Green Earth Appeal");
			$mail->Subject    = $data['subject'];
			$mail->MsgHTML($message);
			$mail->AddAddress($data['to']);

			if (isset($data['cc'])) {
				
				$cc_emails = explode(',', $data['cc']);
				foreach ($cc_emails AS $cc) {
					$mail->AddCC($cc);
				}
			}

			if (isset($data['bcc'])) {
				
				$bcc_emails = explode(',', $data['bcc']);
				
				foreach ($bcc_emails AS $bcc) {
					$mail->AddBCC($bcc);
				}
			}
			/*if (@$attachment) {
					$mail->AddAttachment($attachment); // attachment
			}*/

			if (!$mail->Send()) {
				echo "Mailer Error: " . $mail->ErrorInfo;
			}

        }
		 /**
         * ====================================
         * Common function for random key
		 * ====================================
         */
		function randomKey($length) {  //create no of random digit alphanumeric code
			$key='';
			$pool = array_merge(range(0,9),range('A', 'Z'));

			for($i=0; $i < $length; $i++) {
				$key .= $pool[mt_rand(0, count($pool) - 1)];
			}
			return $key;
		}
		
		/**
         * ====================================
         * 	  Function to create tickers
		 * ====================================
        */
		function create_initiative_ticker($trees, $initiative_id) {

			// @header('Content-type: image/png');
			// Create Image From Existing File
			$png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/ticker.png');

			// Allocate A Bachground Color For The Text - TRANSPARENT
			$bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

			// Allocate A Color For The Text - GREEN
			$font_color = imagecolorallocate($png_image, 22, 105, 54);

			// Set Path to Font File
			$font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

			// Set Number Text to Be Printed On Image
			//  $text = number_format($trees);
			$text 	= $trees;
			$chklen = strlen($text);
			
			if ($chklen == 1) {
					$dist = '134';
			} else if ($chklen == 2) {
					$dist = 106;
			} else if ($chklen == 3) {
					$dist = 80;
			} else if ($chklen == 4) {
					$dist = 56;
			} else if ($chklen == 5) {
					$dist = 28;
			}

			imagesavealpha($png_image, true);
			imagefill($png_image, 0, 0, $bg_color);

			// Print Text On Image
			imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

			$fname = dirname(__FILE__) . '/../../tickers/initiative_tickers/ticker_' . $initiative_id . '.png';

			// Send Image to Browser
			imagepng($png_image, $fname);

			// Clear Memory
			imagedestroy($png_image);
				
        }

		/**
         * ====================================
         * 	  Function to create tickers
		 * ====================================
        */
		function create_ticker($trees, $refid) {

			// @header('Content-type: image/png');
			// Create Image From Existing File
			$png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/ticker.png');

			// Allocate A Bachground Color For The Text - TRANSPARENT
			$bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

			// Allocate A Color For The Text - GREEN
			$font_color = imagecolorallocate($png_image, 22, 105, 54);

			// Set Path to Font File
			$font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

			// Set Number Text to Be Printed On Image
			$text  = number_format($trees);
			$chklen = strlen($text);
			if ($chklen == 1) {
					$dist = '134';
			} else if ($chklen == 2) {
					$dist = 106;
			} else if ($chklen == 3) {
					$dist = 80;
			} else if ($chklen == 4) {
					$dist = 56;
			} else if ($chklen == 5) {
					$dist = 28;
			}

			imagesavealpha($png_image, true);
			imagefill($png_image, 0, 0, $bg_color);

			// Print Text On Image
			imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

			 $fname = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.png';

			// Send Image to Browser
			imagepng($png_image, $fname);

			// Clear Memory
			imagedestroy($png_image);
			
        }
		
		/**
         * ====================================
         * 	  Function to create tickers
		 * ====================================
        */
		function create_cfd_small_ticker($trees, $refid, $color) {
			
			// @header('Content-type: image/png');
			// Create Image From Existing File
			$png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/cfd-tickers/cfd-small-ticker.png');

			// Allocate A Bachground Color For The Text - TRANSPARENT
			$bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);
			
			// Allocate A Color For The Text - white,Green,Black
			if($color == "black"){
				$font_color = imagecolorallocate($png_image, 0, 0, 0);
			}elseif($color == "white"){
				$font_color = imagecolorallocate($png_image, 255, 255, 255);
			}else{
				$font_color = imagecolorallocate($png_image, 22, 105, 54);      //green
			}

			// Set Path to Font File
			$font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

			// Set Number Text to Be Printed On Image
			$text = number_format($trees);
			$chklen = strlen($text);
			if ($chklen == 1) {
					$dist = '134';
			} else if ($chklen == 2) {
					$dist = 106;
			} else if ($chklen == 3) {
					$dist = 80;
			} else if ($chklen == 4) {
					$dist = 56;
			} else if ($chklen == 5) {
					$dist = 28;
			}

			imagesavealpha($png_image, true);
			imagefill($png_image, 0, 0, $bg_color);

			// Print Text On Image
			imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

			$fname = dirname(__FILE__) . '/../../tickers/cfd-tickers/cfd_'.$color.'_ticker_' . $refid . '.png';

			// Send Image to Browser
			imagepng($png_image, $fname);

			// Clear Memory
			imagedestroy($png_image);
				
        }
		/**
         * ====================================
         * 	  Function to create CFD tickers
		 * ====================================
        */
		function create_cfd_ticker($trees, $refid) {
			
			// @header('Content-type: image/png');
			// Create Image From Existing File
			$png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/cfd-tickers/cfd-ticker.png');

			// Allocate A Bachground Color For The Text - TRANSPARENT
			$bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

			// Allocate A Color For The Text - GREEN
			$font_color = imagecolorallocate($png_image, 22, 105, 54);

			// Set Path to Font File
			$font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

			// Set Number Text to Be Printed On Image
			 $text = number_format($trees);
			$chklen = strlen($text);
			if ($chklen == 1) {
					$dist = '134';
			} else if ($chklen == 2) {
					$dist = 106;
			} else if ($chklen == 3) {
					$dist = 80;
			} else if ($chklen == 4) {
					$dist = 56;
			} else if ($chklen == 5) {
					$dist = 28;
			}

			imagesavealpha($png_image, true);
			imagefill($png_image, 0, 0, $bg_color);

			// Print Text On Image
			imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

			 $fname = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.png';

			// Send Image to Browser
			imagepng($png_image, $fname);

			// Clear Memory
			imagedestroy($png_image);
				
        }
		
		/**
         * ====================================
         * 	 Function to create PDF to image
		 * ====================================
        */
		function create_cert_pdf_to_image($pdf,$ref_id,$image_url) {  
		
			$im = new imagick();
			$im->setResolution(300, 300);
			$im->readImage($pdf);
			$im->setImageColorspace(13); 
			$im->setImageCompression(Imagick::COMPRESSION_JPEG); 
			$im->setImageCompressionQuality(80);
			$im->setImageFormat('jpeg'); 
			$im->resizeImage(1199, 1771, imagick::FILTER_LANCZOS, 1);  
			$im->writeImage($image_url.'full_'.$ref_id.'.jpg');  
			$im->resizeImage(300, 443, imagick::FILTER_LANCZOS, 1);  
			$im->writeImage($image_url.'thumb_'.$ref_id.'.jpg'); 
			$im->clear(); 
			$im->destroy(); 
			return ;
		}
		
		/**
         * ====================================
         * 	 Function to check referes
		 * ====================================
        */
		 function referer_check($code) {
			 
			$code = stripcslashes(trim($code));
			$this->load->library('form_validation');
			$this->load->database();
			$sql   = 'SELECT * FROM pct_referer WHERE code="'.$code.'"';
			$query = $this->db->query($sql);
			
			if ($query->num_rows()) {
					$this->form_validation->set_message('referer_check', 'The %s field exists');
					return FALSE;
			} else {
					return TRUE;
			}
        }
		/**
         * ====================================
         * 	 Function to check if email exists
		 * ====================================
        */
        function email_check($email, $user_type = '', $id = 0) {
			
			return true;
			$this->load->library('form_validation');
			$this->load->database();
			$sql = "SELECT * FROM pct_users WHERE email='$email'";
			if ($user_type) {
					$sql .= " AND type<>'$user_type'";
			}
			$query = $this->db->query($sql);
			if ($query->num_rows()) {
					$this->form_validation->set_message('email_check', 'The %s field exists');
					return FALSE;
			} else {
					// check if email exists in the same client group
					if ($id) {
						$sql = "SELECT * FROM pct_users AS u
									INNER JOIN pct_bulk_partners AS bp
									ON u.id=bp.user_id
									WHERE u.id=$id
									AND email='$email'";
						$query = $this->db->query($sql);
						if ($query->num_rows()) {
								$this->form_validation->set_message('email_check', 'The %s field exists');
								return FALSE;
						}
					}
					return TRUE;
			}
        }
		/**
         * ==================================================================================
         * 	 Function to check if user already exists with same email and same company name
		 * ==================================================================================
        */
         function initiative_username_check($username, $company) {
			 
			$this->load->library('form_validation');
			$this->load->database();
			$sql = 'SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email="'. $username . '" AND u.type="intitiative_client" AND bp.company="' . $company . '"';
			$query = $this->db->query($sql);
			if ($query->num_rows()) {
					$this->form_validation->set_message('username_check', 'The %s field exists');
					return FALSE;
			} else {
					return TRUE;
			}
			
        }
		/**
         * ====================================================
         * 	 Function to create certificate image from pdf
		 * ====================================================
        */
		function create_pdf_to_image($pdf,$filename,$image_path) {  

			$im = new imagick($pdf); 
			$im->setImageColorspace(13); 
			$im->setCompression(Imagick::COMPRESSION_JPEG); 
			$im->setCompressionQuality(80); 
			$im->setImageFormat('jpeg'); 
			$im->resizeImage(290, 375, imagick::FILTER_LANCZOS, 1);  
			$im->writeImage($image_path.'certificate_'.$filename.'.jpg'); 
			$im->clear(); 
			$im->destroy(); 
			return ;
			
		}
		/**
         * ============================
         * 	 Function to Check user
		 * ============================
        */
        function check_user($user_type = 'admin') {
			
			if (!is_array($user_type)) {
				$user_type = (array) $user_type;
			}

			$check = in_array($_SESSION['login']['type'], $user_type);
			if (!$check) {
					echo 'You dont have permission.';
					die();
			}
			return true;
        }
	
		/**
         * ====================================================
         * 	 Function to get CFD and GEA counter
		 * ====================================================
        */
		
		public function get_cfd_gea_counters()
		{
			$query = "SELECT u.id,bp.counter_type,pi.ini_counter_type,(bp.free_trees + bp.tree_nums) as partner_trees, (SELECT (SUM(free_trees)+ SUM(tree_nums)) FROM pct_initiative_partner WHERE bulker_id=u.id) As client_trees
						FROM pct_initiative_partner AS bp
						INNER JOIN pct_users AS u
						ON u.id=bp.user_id
						INNER JOIN pct_initiative AS pi
						ON pi.id=bp.initiative
						WHERE u.type IN ('admin', 'intitiative') and active = '1'";
				
		   $results = $this->db->query($query)->result();
		   $all_trees = array();
		   // echo "<pre>"; print_r($results); die;
		   
		   foreach($results as $result){
			   
			   $total_trees = 0;
			   $total_trees = $result->client_trees + $result->partner_trees;
			   
			    if($result->counter_type != 0){	
					if($result->counter_type == 1){	
						$all_trees['gea_counter']  += $total_trees;
					}elseif($result->counter_type==2){
						$all_trees['cfd_counter']  += $total_trees;
					}else{
						$all_trees['non_partner_counter']  += $total_trees;
					}
					
			    }else{
					if($result->ini_counter_type == 1){	
						$all_trees['gea_counter']  += $total_trees;
					}elseif($result->ini_counter_type == 2){
						$all_trees['cfd_counter']  += $total_trees;
					}else{
						$all_trees['non_partner_counter']  += $total_trees;
					}
			    }
			   $all_trees['all_partner_trees']  += $total_trees;
		   } 
		   //echo "<pre>"; print_r($all_trees); die;
		   return  $all_trees;
		}
}
