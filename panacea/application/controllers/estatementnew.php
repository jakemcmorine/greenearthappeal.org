<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estatementnew extends CI_Controller {

		/**
		 * Index Page for this controller.
		 * So any other public methods not prefixed with an underscore will
		 * map to /index.php/welcome/<method_name>
		 * @see http://codeigniter.com/user_guide/general/urls.html
		 */
			public function __construct() {
			parent::__construct();
			session_start();
			$this->data = array();
			$this->load->helper('url');
			$this->load->helper('form');
			$this->load->model('user_model');
			$segment_login = $this->uri->segment(2);
			require_once('phpmailer/class.phpmailer.php');
			//error_reporting(E_ALL);
			
			/* GEA database connection */
			$db_name = "greenapp_wp956";
			$db_host = "localhost";
			$db_user = "greenapp_wp956";
			$db_pass = "!LS0-9P71F";

			$this->con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
                
        }
		
		public function autoupload_estatement_tree_plant_cronjobnew() 
		{
		
			// Check connection 
			if (mysqli_connect_errno())
			{
					echo "Failed to connect to MySQL: " . mysqli_connect_error();
					exit();
			}
			$sql = "SELECT * from `wpiq_newday_estatement_tree_plant_data_dev` where `processed` = '0' ORDER BY id ASC LIMIT 1";
			$result = mysqli_query($this->con,$sql);
			  			  
			$record =  array();
			  
			$record = $this->format_array($result);
			  
			if(!empty($record)){
					
				$check_resubmission = "SELECT * from `wpiq_newday_estatement_tree_plant_data_dev` where processed = '1'"; 
				$result_val = mysqli_query($this->con,$check_resubmission);	
				$count_record = mysqli_num_rows($result_val);	
				$filed_record = $this->format_array($result_val);
				
				if($count_record > 0){
					
					$update_filedsql 	= "UPDATE `wpiq_newday_estatement_tree_plant_data_dev` SET processed='3' WHERE id='".$filed_record['id']."'";
					$update_failed_status  = mysqli_query($this->con,$update_filedsql); 
                    exit();
				}					
			      
				if($record['id']!=""){
					
					$update_sql 	= "UPDATE `wpiq_newday_estatement_tree_plant_data_dev` SET processed='1',process_start_date= NOW() WHERE id='".$record['id']."'";
					$update_status  = mysqli_query($this->con,$update_sql); 
				
				}
			
				if($update_status){            //run function only if query executed 
					
					/* $record['trigger_time']   =  date("Y-m-d H:i:s"); 
					$log = fopen('estatement-webhook-response.txt', 'a');
					fwrite($log, print_r($record , TRUE) . "\n\n");
					fclose($log); */
					  
					$this->add_estatement_client($record); 
					echo "Record Uploaded";
					$record =  array();
				}  				  
			}
			  
		}

		/**
         * ===========================================
		 *  section to Add eStatement client
        */
		
		function add_estatement_client($data){ 
				
				$data_rows = array();
				$data_row['free_trees'] = 0;
				$data_rows_updated = array();
				$total_trees = $free_trees = $old_user = $old_rows = $new_rows = $client_trees = 0;	
				$partner_id = $data['panacea_id'];
				
				 $sql = "SELECT * FROM pct_users  WHERE id='" . $partner_id . "'";               //Pass initiative partner ID
				 $initiative_partner_login_data = $this->db->query($sql)->row();
				 $_SESSION['login'] = (array)$initiative_partner_login_data;   
				 
					$data_row = array();
					$data_row['company']    = "";
					$data_row['email']      = utf8_encode($data['email_address']);
					$data_row['first_name'] = utf8_encode($data['first_name']);
					$data_row['last_name']  = utf8_encode($data['last_name']);
					$data_row['tree_nums']  = 1;
					$data_row['free_trees']  = 0;
					
					
					$data_row['password'] = $this->randomKey(6);
					if (!is_numeric($data_row['tree_nums']) || !trim($data_row['password']) || !filter_var($data_row['email'], FILTER_VALIDATE_EMAIL)
					) {
							continue;
					}
					$data_row['referer_id'] = $this->randomKey(5);
					
					//$rff_temp = str_replace(" ","_",$data_row['company']);           //referid as company name
					//$data_row['referer_id'] = str_replace("’","",strtolower($rff_temp));
					
					$data_row['username'] = $data_row['email'];
					
					// create referer id
					$referer_id = trim($data_row['referer_id']);
					$i = 0;
					while (!$this->referer_check($referer_id)) {
							$i++;
							$referer_id = $data_row['referer_id'] . $i;
					}
					$data_row['referer_id'] = $referer_id;
					if($data_row['free_trees']==""){
						$data_row['free_trees']=0;
					}
					$client_trees = $data_row['tree_nums'];
					
					if (!$this->email_check($data_row['email'], 'intitiative_client', $_SESSION['login']['id'])) {
						  
							$qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";
							$query = $this->db->query($qr);
							if ($query->num_rows()) {
								   
									$rowq = $query->row_array();
									$tr = $rowq['tree_nums'];
									$f_tr = $tr + $data_row['tree_nums']+$rowq['free_trees'];
									
									$qru = "UPDATE pct_initiative_partner SET tree_nums = '" . $f_tr . "' WHERE user_id = '" . $rowq['user_id'] . "' ";
									$this->db->query($qru);
							}
							continue;
					}
					if (!$this->initiative_username_check($data_row['email'])) {
							 $old_user = 1;
							 $qr = "SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' AND u.type='intitiative_client'";
						  
							$query = $this->db->query($qr);
							if ($query->num_rows()) {
									$rowq = $query->row_array();
									$tr = $rowq['tree_nums'];
									$f_tr = $tr + $data_row['tree_nums']+ $rowq['free_trees'];
							}
							$client_trees = $f_tr;
						 
							/* $check_em = "SELECT * FROM pct_users AS u INNER JOIN pct_referer AS bp ON u.id=bp.user_id WHERE u.email='" . $data_row['email'] . "' ";    //to get old referr_id */
							$check_em = "SELECT * FROM pct_referer WHERE user_id='" . $rowq['user_id'] . "' ";    //to get old referr_id
							$getuser_id = $this->db->query($check_em);
							 if ($getuser_id->num_rows()) {
								  $ress = $getuser_id->row_array();
								  $data_row['referer_id'] = $ress['code'];
							 }
							
							//continue;
					}
					if(isset($_SESSION['login']['id'])){
					
						$check_ini_id = "SELECT * FROM pct_initiative_partner WHERE user_id='" . $_SESSION['login']['id'] . "' ";    //to get  initiative id
							$getini_id = $this->db->query($check_ini_id);
							 if ($getini_id->num_rows()) {
								  $ress1 = $getini_id->row_array();
								  $data_row['initiative_id'] = $ress1['initiative'];
							 }
					}
					if ($old_user == 0) {
							$data_row['old_user'] = $old_user;
							$data_row['client_trees'] = $client_trees;
							$new_rows++;
					} else if ($old_user == 1) {
							$data_row['old_user'] = $old_user;
							$data_row['client_trees'] = $client_trees;
							$old_rows++;
					}

					$data_rows[] = $data_row;
					$total_trees += $data_row['tree_nums'];
					$free_trees += $data_row['free_trees'];
				
				
				/* echo "<pre>"; print_r($data_rows); die('here');  */
				$price = $_SESSION['login']['price'];
				$bulk_partner  = $this->user_model->load_initiative($_SESSION['login']['id']);
				//$invoice_trees = $this->user_model->load_invoice_trees($_SESSION['login']['id']);
				$current_trees = $total_trees;
				$all_free_trees = $free_trees;
				$all_invoice_trees = $total_trees+$free_trees;
				//$total_trees = $total_trees + $bulk_partner['tree_nums'] + $invoice_trees->total;
				$total_trees = $total_trees + $free_trees + $bulk_partner['tree_nums'] + $bulk_partner['tree_nums2']+$bulk_partner['free_trees1']+$bulk_partner['free_trees']; 
				$tax = $_SESSION['login']['tax'];
				$sub_total = $current_trees * $price / 100;
				$total_price = $sub_total * ( 100 + $tax ) / 100;
				$total_price = number_format($total_price, 2);
				
				//var_dump($data_rows);
				if (count($data_rows)) {
					//echo "<pre>"; print_r($data_rows); die;
						$_SESSION['login']['data'] = array();
						$_SESSION['login']['data']['rows'] = json_encode($data_rows);
						$_SESSION['login']['data']['total'] = $total_price;
						$_SESSION['login']['data']['total_trees'] = $total_trees;
						$_SESSION['login']['data']['current_tree'] = $current_trees;
						$_SESSION['login']['data']['all_invoice_trees'] = $all_invoice_trees;
						$_SESSION['login']['data']['all_free_trees'] = $all_free_trees;
						
						$_SESSION['login']['data']['address']   = $bulk_partner['address'];
						$_SESSION['login']['data']['city']      = $bulk_partner['city'];
						$_SESSION['login']['data']['state']     = $bulk_partner['state'];
						$_SESSION['login']['data']['post_code'] = $bulk_partner['post_code'];

				}
				//echo "<pre>"; print_r($_SESSION); die("here");
				$panacea_client_id = $this->confirm_gea_estatment_intitiative();        // to add the trees against the partner
				
				 $update_sql = "UPDATE `wpiq_newday_estatement_tree_plant_data_dev` SET processed='2',process_end_date= NOW(),panacea_client_id ='".$panacea_client_id."'  WHERE id='".$data['id']."'";
				 mysqli_query($this->con,$update_sql);
				 
				 return true;

		}
		
		/**
         * =========================================================================================================
		 *  section to confirm tree number for bulk partners
         */
        function confirm_gea_estatment_intitiative() {
			
                $this->check_user('intitiative');
                if (!isset($_SESSION['login']['data']) || empty($_SESSION['login']['data']['rows'])) {
                        unset($_SESSION['login']['data']);
                        redirect(site_url());
                        die();
                }

                $data = json_decode($_SESSION['login']['data']['rows']);
				//echo "<pre>"; print_r($data); die('here');
                $this->load->database();
                $this->load->model('user_model');
                $bulk_partner = $this->user_model->load_initiative($_SESSION['login']['id']);
				
				//echo "<pre>"; print_r($bulk_partner); die;
				
                $initiative 			=  $bulk_partner['initiative_name'];
                $initiative_id 			=  $bulk_partner['initiative'];
                $certificate_layout 	=  $bulk_partner['certificate_layout'];
                $activation_email 		=  $bulk_partner['activation_email'];
                $send_invoices 			=  $bulk_partner['send_invoices'];
                $invoice_free_trial 	=  $bulk_partner['free_trial'];
                $request_for_funds 		=  $bulk_partner['request_for_funds'];
                $attach_cert_inemails 	=  $bulk_partner['attach_cert_inemails'];
                $counter_type 			=  $bulk_partner['counter_type'];                           // check if partner counter is selected
                $ini_counter_type		=  $bulk_partner['ini_counter_type'];					 // check if initiative counter is selected
				$bk_image 				=  $bulk_partner['bk_image'];
				$partner_bk_image 		=  $bulk_partner['partner_bk_image'];
				
				if($partner_bk_image != ""){                                   			//check partner background cert image first
					$bk_image 	=  $bulk_partner['partner_bk_image'];
				}else{
					$bk_image 	=  $bulk_partner['bk_image'];
				}
			
                $opportunity_str = '';
               
				$_SESSION['figure_history_key'] = $this->randomKey(10);
                $config = array();
                $current_month_text = date("F_Y");
				$initiative_clients = array();
				$count_rows = $trial_trees = $redirect = 0;
				
                foreach ($data AS &$row) {
                        // store client into database
                        $row = (array) $row;
						
						$unique_url 	=  $bulk_partner['website_url'].'/?refid=' . $row['referer_id']; 
					    $total_client_trees = $row['client_trees']+$row['free_trees'];
						
						 // create the ticker
						if($counter_type != 0){															// if counter is set on partner
							if($counter_type==1){
								 $this->create_ticker($total_client_trees, $row['referer_id']);            //GEA tickers
							}elseif($counter_type==2){
								$this->create_cfd_ticker($total_client_trees, $row['referer_id']);        //CFD tickers
							}elseif($counter_type==3){
								$this->create_non_partner_ticker($total_client_trees, $row['referer_id']);        //Non partner tickers
							}
							
						}else{                                                                              // if counter is set on initiative
							if($ini_counter_type==1){
								 $this->create_ticker($total_client_trees, $row['referer_id']);            //GEA tickers
							}elseif($ini_counter_type==2){
								$this->create_cfd_ticker($total_client_trees, $row['referer_id']);        //CFD tickers
							}elseif($ini_counter_type==3){
								$this->create_non_partner_ticker($total_client_trees, $row['referer_id']);        //Non partner tickers
							}
						}
						
						// $this->create_cfd_small_ticker($total_client_trees, $row['referer_id'], 'black');
						
						// $this->create_cfd_small_ticker($total_client_trees, $row['referer_id'], 'green');
						
						// $this->create_cfd_small_ticker($total_client_trees, $row['referer_id'], 'white');
						
						
                        $ticker_url 		= base_url() . 'tickers/ticker_' . $row['referer_id'] . '.png';
                        $cert_thumb_url 	= base_url() . 'cert/thumb_' . $row['referer_id'] . '.jpg';
                        $cert_image_url 	= base_url() . 'cert/full_' . $row['referer_id'] . '.jpg';
						$row['activation_key'] = $this->randomKey(6);
						
                        $activation_url = base_url() . 'index.php/user/client_login/' . $row['activation_key'];
                        $auto_login_link = base_url() . 'index.php/user/auto_login_client_link/' . $row['activation_key'];
						
						if ($row['old_user'] == 0) {
							if($activation_email==1){
								$email_template = $this->user_model->load_initiative_email_template($initiative_id,2);  // send activation email for new clients
							}else{
								$email_template = $this->user_model->load_initiative_email_template($initiative_id,3); 
							}
								
						}else{
							$email_template = $this->user_model->load_initiative_email_template($initiative_id,5);     //send email for update clients
						} 
						  
                        // create the certificate
                        $restaurant 	  = $row['company'];
                        $restaurant_name  = $row['first_name'].' '.$row['last_name'];
                        $total_trees 	  = $row['tree_nums']+$row['free_trees'];
                        $client_trees 	  = $row['client_trees']+$row['free_trees'];
						
                        $calculated_cups = $total_client_trees*1000;
		
						if($client_trees=='1'){
							 $client_trees = number_format($total_trees).' '.'tree';
						}else{
							 $client_trees = number_format($total_trees).' '.'trees';
						}
						
                        if ($row['old_user'] == 0) {
							$row['activation_email'] = $activation_email;
							$row['auto_login_link'] = 0;
							$row['figure_history_key'] = $_SESSION['figure_history_key'];
							$client_id = $this->user_model->insert_initiative_client($row);
                        } else {
							$row['figure_history_key'] = $_SESSION['figure_history_key'];
							$client_id = $this->user_model->update_initiative_client($row);
                        }
						
						$initiative_clients[$count_rows]['partner_id'] 	=  $_SESSION['login']['id'];
						$initiative_clients[$count_rows]['tree_nums'] 	=  $row['tree_nums'];
						$initiative_clients[$count_rows]['user_id'] 	=  $client_id;
						
						//to add client certificate url
						$client_certificate_url = 'https://www.greenearthappeal.org/panacea/uploads/certs/clients/'.'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $client_id . '.pdf'; 
																								
                        $message = $email_template->message;
						
						if($total_client_trees=='1'){
							$total_client_trees  = number_format($total_client_trees).' '.'tree';
						}else{
							$total_client_trees  = number_format($total_client_trees).' '.'trees';
						}
						
						//$all_counters 	= $this->get_cfd_gea_counters();
						
                        $message = str_ireplace(array('[contact_person]','[activation_url]','[auto_login_link]','[company_name]','[total_trees]','[current_trees]', '[username]', '[password]', '[unique_url]', '[ticker_url]','[initiative_name]','[calculated_cups]','[cert_thumb_url]','[cert_image_url]','[certificate_url]'), array(ucwords($row['first_name']), $activation_url,$auto_login_link, $row['company'],$total_client_trees, $total_trees, $row['username'], $row['password'], $unique_url, $ticker_url,$initiative,$calculated_cups,$cert_thumb_url,$cert_image_url,$client_certificate_url), $message);

					
						$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]'), array(ucwords($restaurant_name), $total_client_trees, $initiative,$calculated_cups,$total_client_trees), stripslashes($bulk_partner['certificate1']));
						$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]'), array(ucwords($restaurant_name), $total_client_trees, $initiative,$calculated_cups,$total_client_trees), stripslashes($bulk_partner['certificate2']));
						$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]'), array(ucwords($restaurant_name), $total_client_trees, $initiative,$calculated_cups,$total_client_trees), stripslashes($bulk_partner['certificate3']));
						$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]','[total_trees]'), array(ucwords($restaurant_name), $total_client_trees, $initiative,$calculated_cups,$total_client_trees), stripslashes($bulk_partner['certificate4']));

						
                        //$rest_logo      = $_SESSION['login']['data']['logo'];
                        $ds = DIRECTORY_SEPARATOR;
                        $certificate_sub = $certificate_msg = $certificate_text = '';
	
                        $cert_name = 'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $client_id . '.pdf';
						
						 if ($row['old_user'] == 0) {
							 
							 $row['initiative_name']  = $bulk_partner['initiative_name'];
							 $row['unique_url']       =  $unique_url;
							 $row['ticker_url'] 	  = $ticker_url;
							 $row['cert_name'] 	  	  = $cert_name;
							 $row['email_title']      = $bulk_partner['sender_email_title'];
							 
							//insert all client data in "pct_client_activation_emaildata" table for email
							$this->user_model->insert_initiative_client_email_records($row,$client_id);
							
						}
						
                        $cert_path = dirname(__FILE__) . '/../../uploads/certs/clients/';
												
						if($certificate_layout == '1'){
							require('libraries/cert-ini.php');
						}else{
							$cert_client_name = $row['first_name'].' '.$row['last_name'];
							require('libraries/cert-ini-layout2.php');
						}

						$image_file_name = date('Ymdhis');

						$image_path = dirname(__FILE__) . '/../../uploads/certs/clients/images/';
						$image_url 	= dirname(__FILE__) . '/../../cert/';
						$pdf_path 	= $cert_path.$cert_name;
						
						$estatement_client_name    =  ucwords($row['first_name'].' '.$row['last_name']);
						$estatement_client_name    =  str_replace(' ', '-', $estatement_client_name);
						$estatement_client_name    =  $estatement_client_name.'-Tree-Planting-Certificate-'.$row['referer_id'].'.jpg';
						
						//$image_path_url = dirname(__FILE__) . '/../../cert/full_'.$row['referer_id'].'.jpg';
						$image_path_url = dirname(__FILE__) . '/../../cert/'.$estatement_client_name;
						/* if (extension_loaded('imagick')){   //extension imagick is installed
							$this->create_pdf_to_image($pdf_path, $image_file_name, $image_path); 
						} */	
						if (extension_loaded('imagick')){   		//extension imagick is installed
							//$this->create_cert_pdf_to_image($pdf_path, $row['referer_id'], $image_url); //to create pef images for clients
							$this->estatement_cert_pdf_to_image($pdf_path, $estatement_client_name, $image_url); 
						}							
												
                        // send email to client
                        $config['to'] =  $row['email'];
						
                        //$config['to'] =  'testing.whizkraft1@gmail.com';
                       
                        $config['subject'] = $email_template->subject;

                        $config['subject'] = str_ireplace(array('[contact_person]', '[company_name]','[initiative_name]','[total_trees]'), array($row['first_name'], $restaurant,$initiative,$total_trees), $config['subject']);
										
                        $config['message'] = $message;
						
						if($attach_cert_inemails==1){              //to stop attach client certificate if option in disabled
							$attach_cert = array($image_path_url);
						}else{
							$attach_cert = "";
						}
						
                        if ($row['old_user'] == 0) {
								if($activation_email==1){
									$config['attach'] = "";
								}else{
									$config['attach'] = $attach_cert;
								}
						}else{
							$config['attach'] = $attach_cert;
						}
						
					    $config['email_title'] = $bulk_partner['sender_email_title'];
						
						//if($email_template->enable_email == 1){
							$this->send_mail($config);   //only send email if it is enabled in initiative email templates
						//}
						
						//insert record for send monthly invoice
						if($send_invoices=="monthly"){
							
							$record['tree_nums']      = $row['tree_nums'];
							$record['company']   	  = $row['company'];
							$record['free_trees']     = $row['free_trees'];
							$record['initiative_id']  = $row['initiative_id'];
							if($request_for_funds == 1)
							{
								$record['request_for_funds']  = '1';
							}
							$record['partner_id']     = $_SESSION['login']['id'];
							$insert_monthly = $this->user_model->insert_initiative_monthly_invoice_records($record);  
						}
						if ($row['old_user'] == 0) {
							   $trial_trees+= $row['tree_nums'];         //get sum on all new user trees
						}
						$count_rows++;
                }
                // prepare to create the invoice
                $cert_path = '';
                $pct_rows = &$data;
				
				/* Insert initiatives clients */
				if(!empty($initiative_clients)){
					
					$part_id = $_SESSION['login']['id'];
					$this->db->query("UPDATE pct_initiative_clients SET current='0' WHERE partner_id='$part_id'");

					foreach($initiative_clients as $in_client){
						$in_clients['partner_id']      		 = $in_client['partner_id'];
						$in_clients['user_id']      		 = $in_client['user_id'];
						$in_clients['tree_nums']      		 = $in_client['tree_nums'];
						$in_clients['current']      		 = '1';
						$this->db->insert_batch('pct_initiative_clients', array($in_clients));
					}
				}
				
                $bulk_partner 	= $this->user_model->load_initiative($_SESSION['login']['id']);  

              	$total_trees 	= $_SESSION['login']['data']['total_trees'];
				
				//$all_counters 	= $this->get_cfd_gea_counters();
				
				//$this->create_ticker($all_counters['gea_counter'], 'gea_counter');          	 //create GEA ticker for all partners
				//$this->create_cfd_ticker($all_counters['cfd_counter'], 'cfd_counter');		 //create CFD ticker for all partners
					
                // create the ticker
                $refid  = $this->user_model->get_refid($_SESSION['login']['id']);
				$counter_type  = $bulk_partner['counter_type'];
								
				if($counter_type != 0){											      // if counter is set on partner
					if($counter_type==1){
						 $this->create_ticker($total_trees, $refid->code);            //GEA tickers
					}elseif($counter_type==2){
						$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
					}elseif($counter_type==3){
						$this->create_non_partner_ticker($total_trees, $refid->code);        //Non partner tickers
					}
					
				}else{                                                               // if counter is set on initiative
					if($ini_counter_type==1){
						$this->create_ticker($total_trees, $refid->code);            //GEA tickers
					}elseif($ini_counter_type==2){
						$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
					}elseif($ini_counter_type==3){
						$this->create_non_partner_ticker($total_trees, $refid->code);        //Non partner tickers
					}
				}
				
				//to delete Certificate image and pdf
				if (file_exists($image_path_url)){
					unlink($image_path_url);       
				}
				if (file_exists($pdf_path)){
					unlink($pdf_path);
				}					
				
                unset($_SESSION['figure_history_key']);
                unset($_SESSION['login']['data']);
				
				return $client_id;
				
        }
		
		function format_array($result){
			$records = [];
			if(mysqli_num_rows($result) > 0) {
				  while($row = mysqli_fetch_assoc($result)) {
					    $records =  $row;
					   
				  }
			 }
		   return $records;
		}
		  /**
         * =========================================================================================================
         * common functions
         */

        function send_mail($data) {
                $mail             = new PHPMailer();
                $mail->IsSMTP(); // telling the class to use SMTP
                $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                // 1 = errors and messages
                // 2 = messages only
                $mail->SMTPAuth   = true;                  // enable SMTP authentication
                $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
                $mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
                $mail->Port       = 587;                   // set the SMTP port for the GMAIL server
                $mail->Username   = "partners@greenearthappeal.org";  // GMAIL username
                $mail->Password   = "P8%k<g_39x,#1";            // GMAIL password
                $mail->CharSet = 'UTF-8';

                if (isset($data['from'])) {
                        $mail->SetFrom($data['from'], '');
                } elseif(isset($data['email_title'])){
                        $mail->SetFrom('partners@greenearthappeal.org', $data['email_title'] );
                }else{
					$mail->SetFrom('partners@greenearthappeal.org', 'Partnerships Department – Green Earth Appeal');
				}

                if (isset($data['attach']) && !empty($data['attach'])) {
                        if (!is_array($data['attach'])) {
                                $data['attach'] = (array) $data['attach'];
                        }
                        foreach ($data['attach'] AS $attach) {
                                $mail->AddAttachment($attach); // attachment
                        }
                }

                ob_start();
                if ((isset($data['signature']) && $data['signature'] == false) || (!$data['signature'])) {
                        echo '<style type="text/css">*, p {font-family:Calibri!important; font-size: 11pt!important;}</style>';
                        require('libraries/signature.htm');
                } else {
                        require('libraries/signature_solicitor.htm');
                }

                $message = ob_get_contents();
                ob_end_clean();

                $message = $data['message'] . $message;

                $mail->ClearReplyTos();
                $mail->AddReplyTo("partners@greenearthappeal.org", "Partnership Department - Green Earth Appeal");
                $mail->Subject    = $data['subject'];
                $mail->MsgHTML($message);
                $mail->AddAddress($data['to']);

                if (isset($data['cc'])) {
                        $cc_emails = explode(',', $data['cc']);
                        foreach ($cc_emails AS $cc) {
                                $mail->AddCC($cc);
                        }
                }

                if (isset($data['bcc'])) {
                        $bcc_emails = explode(',', $data['bcc']);
                        foreach ($bcc_emails AS $bcc) {
                                $mail->AddBCC($bcc);
                        }
                }

                /*if (@$attachment) {
                                $mail->AddAttachment($attachment); // attachment
                }*/

                if (!$mail->Send()) {
                        echo "Mailer Error: " . $mail->ErrorInfo;
                }

        }
		  /**
         * =========================================================================================================
         * common functions
         */
		function randomKey($length) {  //create no of random digit alphanumeric code
			$key='';
			$pool = array_merge(range(0,9),range('A', 'Z'));

			for($i=0; $i < $length; $i++) {
				$key .= $pool[mt_rand(0, count($pool) - 1)];
			}
			return $key;
		}
		
		function create_initiative_ticker($trees, $initiative_id) {

                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
              //  $text = number_format($trees);
                $text = $trees;
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                 $fname = dirname(__FILE__) . '/../../tickers/initiative_tickers/ticker_' . $initiative_id . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }

		  /**
         * =========================================================================================================
         * common functions
         */
		 function create_ticker($trees, $refid) {

                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
               	$text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                 $fname = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }
		
		
		function create_cfd_small_ticker($trees, $refid, $color) {
			
                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/cfd-tickers/cfd-small-ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);
				
				// Allocate A Color For The Text - white,Green,Black
				if($color=="black"){
					$font_color = imagecolorallocate($png_image, 0, 0, 0);
				}elseif($color=="white"){
					$font_color = imagecolorallocate($png_image, 255, 255, 255);
				}else{
					$font_color = imagecolorallocate($png_image, 22, 105, 54);      //green
				}

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
                $text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                $fname = dirname(__FILE__) . '/../../tickers/cfd-tickers/cfd_'.$color.'_ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }
		function create_cfd_ticker($trees, $refid) {
			
                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/cfd-tickers/cfd-ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
                 $text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                 $fname = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }
		  /**
         * =========================================================================================================
         * common functions
         */
		function create_cert_pdf_to_image($pdf,$ref_id,$image_url) {  //create image from certificate pdf
				$im = new imagick();
				$im->setResolution(300, 300);
				$im->readImage($pdf);
				$im->setImageColorspace(13); 
				$im->setImageCompression(Imagick::COMPRESSION_JPEG); 
				$im->setImageCompressionQuality(80);
				$im->setImageFormat('jpeg'); 
				$im->resizeImage(1199, 1771, imagick::FILTER_LANCZOS, 1);  
				$im->writeImage($image_url.'full_'.$ref_id.'.jpg');  
				$im->resizeImage(300, 443, imagick::FILTER_LANCZOS, 1);  
				$im->writeImage($image_url.'thumb_'.$ref_id.'.jpg'); 
				$im->clear(); 
				$im->destroy(); 
				return ;
		}
		 /**
         * =========================================================================================================
         * common functions
         */
		function estatement_cert_pdf_to_image($pdf,$image_name,$image_url) {  //create image from certificate pdf
				$im = new imagick();
				$im->setResolution(300, 300);
				$im->readImage($pdf);
				$im->setImageColorspace(13); 
				$im->setImageCompression(Imagick::COMPRESSION_JPEG); 
				$im->setImageCompressionQuality(80);
				$im->setImageFormat('jpeg'); 
				$im->resizeImage(1199, 1771, imagick::FILTER_LANCZOS, 1);  
				$im->writeImage($image_url.$image_name);  
				$im->clear(); 
				$im->destroy(); 
				return ;
		}
		  /**
         * =========================================================================================================
         * common functions
         */
		 function referer_check($code) {
                $code = stripcslashes(trim($code));
                $this->load->library('form_validation');
                $this->load->database();
                $sql = 'SELECT * FROM pct_referer WHERE code="'.$code.'"';
                $query = $this->db->query($sql);
                if ($query->num_rows()) {
                        $this->form_validation->set_message('referer_check', 'The %s field exists');
                        return FALSE;
                } else {
                        return TRUE;
                }
        }
		  /**
         * =========================================================================================================
         * common functions
         */
        // check if email exists
        function email_check($email, $user_type = '', $id = 0) {
                return true;
                $this->load->library('form_validation');
                $this->load->database();
                $sql = "SELECT * FROM pct_users WHERE email='$email'";
                if ($user_type) {
                        $sql .= " AND type<>'$user_type'";
                }
                $query = $this->db->query($sql);
                if ($query->num_rows()) {
                        $this->form_validation->set_message('email_check', 'The %s field exists');
                        return FALSE;
                } else {
                        // check if email exists in the same client group
                        if ($id) {
                                $sql = "SELECT * FROM pct_users AS u
										INNER JOIN pct_bulk_partners AS bp
										ON u.id=bp.user_id
										WHERE u.id=$id
										AND email='$email'";
                                $query = $this->db->query($sql);
                                if ($query->num_rows()) {
                                        $this->form_validation->set_message('email_check', 'The %s field exists');
                                        return FALSE;
                                }
                        }
                        return TRUE;
                }
        }
		  /**
         * =========================================================================================================
         * common functions
         */
		// check if user already exists with same email and same company name
         function initiative_username_check($username) {
                $this->load->library('form_validation');
                $this->load->database();
				$sql = 'SELECT * FROM pct_users AS u INNER JOIN pct_initiative_partner AS bp ON u.id=bp.user_id WHERE u.email="'. $username . '" AND u.type="intitiative_client"';
                $query = $this->db->query($sql);
                if ($query->num_rows()) {
                        $this->form_validation->set_message('username_check', 'The %s field exists');
                        return FALSE;
                } else {
                        return TRUE;
                }
        }
		  /**
         * =========================================================================================================
         * create image from pdf
         */
		function create_pdf_to_image($pdf,$filename,$image_path) {  

			$im = new imagick($pdf); 
			$im->setImageColorspace(13); 
			$im->setCompression(Imagick::COMPRESSION_JPEG); 
			$im->setCompressionQuality(80); 
			$im->setImageFormat('jpeg'); 
			$im->resizeImage(290, 375, imagick::FILTER_LANCZOS, 1);  
			$im->writeImage($image_path.'certificate_'.$filename.'.jpg'); 
			$im->clear(); 
			$im->destroy(); 
			return ;
		}
		  /**
         * =========================================================================================================
         * check_user
         */
        function check_user($user_type = 'admin') {
                if (!is_array($user_type)) {
                        $user_type = (array) $user_type;
                }

                $check = in_array($_SESSION['login']['type'], $user_type);
                if (!$check) {
                        echo 'You dont have permission.';
                        die();
                }
                return true;
        }
		
		/* TO get CFD and GEA counter */
		public function get_cfd_gea_counters()
		{
			$query = "SELECT u.id,bp.counter_type,pi.ini_counter_type,(bp.free_trees + bp.tree_nums) as partner_trees, (SELECT (SUM(free_trees)+ SUM(tree_nums)) FROM pct_initiative_partner WHERE bulker_id=u.id) As client_trees
						FROM pct_initiative_partner AS bp
						INNER JOIN pct_users AS u
						ON u.id=bp.user_id
						INNER JOIN pct_initiative AS pi
						ON pi.id=bp.initiative
						WHERE u.type IN ('admin', 'intitiative') and active = '1'";
				
		   $results = $this->db->query($query)->result();
		   $all_trees = array();
		   // echo "<pre>"; print_r($results); die;
		   foreach($results as $result){
				   $total_trees = 0;
				   $total_trees = $result->client_trees + $result->partner_trees;
				   
				   if($result->counter_type != 0){	
						if($result->counter_type == 1){	
							$all_trees['gea_counter']  += $total_trees;
						}elseif($result->counter_type==2){
							$all_trees['cfd_counter']  += $total_trees;
						}else{
							$all_trees['non_partner_counter']  += $total_trees;
						}
						
				   }else{
						if($result->ini_counter_type == 1){	
							$all_trees['gea_counter']  += $total_trees;
						}elseif($result->ini_counter_type == 2){
							$all_trees['cfd_counter']  += $total_trees;
						}else{
							$all_trees['non_partner_counter']  += $total_trees;
						}
				   }
				   $all_trees['all_partner_trees']  += $total_trees;
		   } 
		   //echo "<pre>"; print_r($all_trees); die;
		   return  $all_trees;
		}
		
			 /**
         * =========================================================================================================
		 *  section to add create_ticker_certificates
         */
		function create_ticker_certificates($initiativeid = NULL) {   
						$this->load->database();
					 //$initiatives_ids = 	array_unique($initiatives_ids);
					if($initiativeid!=""){
						$query = "SELECT * from pct_initiative WHERE id='$initiativeid'";   
					}else{
						$query = "SELECT * from pct_initiative"; 
					}

                     $initiatives_ids = $this->db->query($query)->result_array();
					foreach($initiatives_ids as $initiatives_id){
						// echo "<pre>"; print_r($initiatives_id); die;
						 $initiatives_id = $initiatives_id['id'];
						 $initiative_sql = "SELECT (SUM(free_trees)+ SUM(tree_nums)) As total_trees FROM pct_initiative_partner WHERE `initiative`='$initiatives_id'";    
						 $ini_res = $this->db->query($initiative_sql)->row();
						 $client_trees = $ini_res->total_trees;
						 
						 $ini_sql = "SELECT * from pct_initiative WHERE `id`='$initiatives_id'";    
						 $bulk_partner = $this->db->query($ini_sql)->row();
						 $initiative_name = $bulk_partner->initiative_name; 
						 $ini_certificate_name = $bulk_partner->ini_certificate_name; 
						 $restaurant = $bulk_partner->initiative_name; 
						 
						 $calculated_cups = $client_trees*1000; 
						 
						 $client_trees = number_format($client_trees);
						 //echo "<pre>"; print_r($bulk_partner); 
						$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate1));
						$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate2));
						$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate3));
						$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]'), array($ini_certificate_name, $client_trees, $initiative_name,$calculated_cups, $client_trees), stripslashes($bulk_partner->certificate4));
						
						$certificate_sub = $certificate_msg = $certificate_text = '';
						$cert_name = 'The_Green_Earth_Appeal_Certificate_Initiative_'. $initiatives_id . '.pdf';
						
						$this->create_initiative_ticker($client_trees, $initiatives_id);
						$bk_image =  $bulk_partner->bk_image;
						require('libraries/cert-initiatives.php');
					}
					//	echo "<pre>"; print_r($initiatives_ids); 
		
		}
		
		function create_non_partner_ticker($trees, $refid) {
			
                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                 $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/non-partner.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/MYRIADPRO-BOLDCOND.OTF';

                // Set Number Text to Be Printed On Image
                 $text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '140';
                } else if ($chklen == 2) {
                        $dist = 125;
                } else if ($chklen == 3) {
                        $dist = 112;
                } else if ($chklen == 4) {
                        $dist = 92;
                } else if ($chklen == 5) {
                        $dist = 88;
                }else if ($chklen == 6) {
                        $dist = 69;
                }else if ($chklen == 7) {
                        $dist = 58;
                }else if ($chklen == 9) {
                        $dist = 40;
                }
				
                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 28, 0, $dist, 35, $font_color, $font_path, $text);

               $fname = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
        }
					
}