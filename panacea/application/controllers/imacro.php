<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imacro extends CI_Controller {

		/**
		 * Index Page for this controller.
		 *
		 * Since this controller is set as the default controller in 
		 * So any other public methods not prefixed with an underscore will
		 * map to /index.php/welcome/<method_name>
		 * @see http://codeigniter.com/user_guide/general/urls.html
		*/
		 
	    public function __construct() {
			
                parent::__construct();
                session_start();
                $this->data = array();
                $this->load->helper('url');
                $this->load->helper('form');
				$this->load->database();
				$this->load->model('imacro_model');
				$this->load->model('user_model');
                $segment_login = $this->uri->segment(2);
                require_once('phpmailer/class.phpmailer.php');
                
        }
		
		/* function to autoupload csv from ondrive*/
		
		function autoupload_clients() {
			
			$this->load->database();
			$this->load->model('imacro_model');
			
			if (isset($_POST['token'])) {
				   
					// process Initiative data
					$this->load->library('form_validation');
					
					$this->form_validation->set_rules('token', 'Token', 'trim|required'); 
					$this->form_validation->set_rules('first_name', 'First Name', 'trim|required'); 
					$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required'); 
					$this->form_validation->set_rules('email_address', 'Email', 'trim|required'); 
					$this->form_validation->set_rules('company', 'Company', 'trim|required'); 
					$this->form_validation->set_rules('tree_nums', 'Trees', 'trim|required'); 
					$this->form_validation->set_rules('partner_id', 'Partner ID', 'trim|required'); 
				
					
					$this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

					// validate form
					if ($this->form_validation->run() === false) {
							$data = array('title' => 'Add Initiative', 'body' => 'autoupload_imacro_clients');
							$this->data = array_merge($this->data, $data);
							$this->load->view('template', $this->data);
					} else {
												
							$this->load->database();
							$this->load->model('imacro_model');
							 if($_POST['token']=='7ace69e33f3b38dd1dd0920868700b5c'){
								  $this->imacro_model->insert_autoupload_clients();
								   echo "Record Inserted successfully";
							 }else{
								 echo "Token is not matched"; 
								 exit;
							 }
					}
			} else {
				$unique_key = $this->random_alphabet('2').$this->random_number('2').$this->random_alphabet('2').$this->random_number('2');
				$data = array('title' => 'Add Client Trees', 'body' => 'autoupload_imacro_clients','unique_key'=>$unique_key);
				$this->data = array_merge($this->data, $data);
				$this->load->view('template', $this->data);
			}
			
		}
			
		function autoupload_client_lead_process_new() {
			
				$this->load->database();
				$this->load->model('imacro_model');
                if (isset($_POST['token'])) {
					   
					    // process Initiative data
                        $this->load->library('form_validation');
						
						$this->form_validation->set_rules('first_name', 'First Name', 'trim|required'); 
						$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required'); 
						$this->form_validation->set_rules('job_title', 'Job Title', 'trim|required'); 
						$this->form_validation->set_rules('company_number', 'Company Number', 'trim|required'); 
						$this->form_validation->set_rules('company', 'Company', 'trim|required'); 
						$this->form_validation->set_rules('city', 'City/Town', 'trim|required'); 
						$this->form_validation->set_rules('country', 'Country', 'trim|required'); 
						$this->form_validation->set_rules('linkedin_profile_url', 'Linkedin profile URL', 'trim|required'); 
						$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required'); 
						$this->form_validation->set_rules('tree_nums', 'Trees', 'trim|required'); 
						$this->form_validation->set_rules('token', 'Token', 'trim|required'); 
						$this->form_validation->set_rules('partner_id', 'Partner ID', 'trim|required'); 
					
						
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Add Initiative', 'body' => 'autoupload_imacro_client_process_new');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
													
                                $this->load->database();
                                $this->load->model('imacro_model');
								 if($_POST['token']=='abcde12345' || $_POST['token']=='hgudk85496'){
									 $client_id =  $this->imacro_model->insert_autoupload_clients();
									   // echo "Record Inserted successfully";
									    $redirect_url = site_url().'/newuploadimacroclient/new_autoupload_imacro_client/'.$client_id;
										redirect($redirect_url);
										exit;
								}else{
									 
									 echo "Token is not matched"; 
									 exit;
								}
                        }
                } else {
					$delete_key = $this->generateRandomString(8);
					$unique_key = $this->random_alphabet('2').$this->random_number('2').$this->random_alphabet('2').$this->random_number('2');
					$data = array('title' => 'Add Client Trees', 'body' => 'autoupload_imacro_client_process_new','unique_key'=>$unique_key,'delete_key'=>$delete_key);
					$this->data = array_merge($this->data, $data);
					$this->load->view('template', $this->data);
                }
			
		}
		
		function create_non_partner_ticker($trees, $refid) {
			
                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/non-partner.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/MYRIADPRO-BOLDCOND.OTF';

                // Set Number Text to Be Printed On Image
                 $text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '140';
                } else if ($chklen == 2) {
                        $dist = 125;
                } else if ($chklen == 3) {
                        $dist = 112;
                } else if ($chklen == 4) {
                        $dist = 92;
                } else if ($chklen == 5) {
                        $dist = 88;
                }else if ($chklen == 6) {
                        $dist = 69;
                }else if ($chklen == 7) {
                        $dist = 58;
                }else if ($chklen == 9) {
                        $dist = 40;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 28, 0, $dist, 35, $font_color, $font_path, $text);

                $fname = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
        }
		
		function autoupload_client_lead_process_update() {
			
			$this->load->database();
			$this->load->model('imacro_model');
			
			if (isset($_POST['token'])) {
				
				 // process Initiative data
				$this->load->library('form_validation');
				
				$this->form_validation->set_rules('first_name', 'First Name', 'trim|required'); 
				$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required'); 
				
				// $this->form_validation->set_rules('search_url', 'Search URL', 'trim|required'); 
				// $this->form_validation->set_rules('search_id', 'Search ID', 'trim|required'); 
				
				$this->form_validation->set_rules('job_title', 'Job Title', 'trim|required'); 
				$this->form_validation->set_rules('company_number', 'Company Number', 'trim|required'); 
				$this->form_validation->set_rules('company', 'Company', 'trim|required'); 
				$this->form_validation->set_rules('city', 'City/Town', 'trim|required'); 
				$this->form_validation->set_rules('country', 'Country', 'trim|required'); 
				$this->form_validation->set_rules('linkedin_profile_url', 'Linkedin profile URL', 'trim|required'); 
				$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required'); 
				$this->form_validation->set_rules('tree_nums', 'Trees', 'trim|required'); 
				$this->form_validation->set_rules('token', 'Token', 'trim|required'); 
				$this->form_validation->set_rules('partner_id', 'Partner ID', 'trim|required'); 
			
				$this->form_validation->set_rules(
								'email_address', 'Email', 'required|is_unique[pct_imacroData.email_address]',
								array('is_unique' => 'This %s already exists.')
							);
				$this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

				// validate form
				if ($this->form_validation->run() === false) {
						$data = array('title' => 'Add Initiative', 'body' => 'new_autoupload_imacro_client_process_update');
						$this->data = array_merge($this->data, $data);
						$this->load->view('template', $this->data);
				}else {
											
						$this->load->database();
						$this->load->model('imacro_model');
						$token = trim($_POST['token']);      
						//echo "<pre>"; print_r($_POST); die;		
						
						if($token=='7ace69e33f3b38dd1dd0920868700b5c' || $token=='555x69w33f3v38pp1dd03489200b6gsz' || $token == '2aty30e55f3b38cv1dd45908700c4s' || $token == '45690l55fsb89cd1dd437087vdc4g'){
							
							$client_id =  $this->imacro_model->new_insert_autoupload_clients();
							 //echo "Record Inserted successfully"; die;
							$redirect_url = site_url().'/newuploadimacroclient/new_autoupload_imacro_client_update/'.$client_id.'/'.$token;
							redirect($redirect_url);									
							exit;
							
						}else{
							echo "Token is not matched"; 
							exit;
						}
				}
				
			}else{
				
				$delete_key = $this->generateRandomString(8);
				$unique_key = $this->random_alphabet('2').$this->random_number('2').$this->random_alphabet('2').$this->random_number('2');
				$data = array('title' => 'Add Client Trees', 'body' => 'new_autoupload_imacro_client_process_update','unique_key'=>$unique_key,'delete_key'=>$delete_key);
				$this->data = array_merge($this->data, $data);
				$this->load->view('template', $this->data);
			}

		}		
		function autoupload_client_lead_process() {
			 
				$this->load->database();
				$this->load->model('imacro_model');
				
                if (isset($_POST['token'])) {
					   
					     // process Initiative data
                        $this->load->library('form_validation');
						
						$this->form_validation->set_rules('first_name', 'First Name', 'trim|required'); 
						$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required'); 
						
						// $this->form_validation->set_rules('search_url', 'Search URL', 'trim|required'); 
						// $this->form_validation->set_rules('search_id', 'Search ID', 'trim|required'); 
						
						$this->form_validation->set_rules('job_title', 'Job Title', 'trim|required'); 
						$this->form_validation->set_rules('company_number', 'Company Number', 'trim|required'); 
						$this->form_validation->set_rules('company', 'Company', 'trim|required'); 
						$this->form_validation->set_rules('city', 'City/Town', 'trim|required'); 
						$this->form_validation->set_rules('country', 'Country', 'trim|required'); 
						$this->form_validation->set_rules('linkedin_profile_url', 'Linkedin profile URL', 'trim|required'); 
						$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required'); 
						$this->form_validation->set_rules('tree_nums', 'Trees', 'trim|required'); 
						$this->form_validation->set_rules('token', 'Token', 'trim|required'); 
						$this->form_validation->set_rules('partner_id', 'Partner ID', 'trim|required'); 
						
						$this->form_validation->set_rules(
										'email_address', 'Email', 'required|is_unique[pct_imacroData.email_address]',
										array('is_unique' => 'This %s already exists.')
									);
					
						
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');

                        // validate form
                        if ($this->form_validation->run() === false) {
                                $data = array('title' => 'Add Initiative', 'body' => 'new_autoupload_imacro_client_process');
                                $this->data = array_merge($this->data, $data);
                                $this->load->view('template', $this->data);
                        } else {
													
                                $this->load->database();
                                $this->load->model('imacro_model');
								 if($_POST['token']=='7ace69e33f3b38dd1dd0920868700b5c'){
									  $client_id =  $this->imacro_model->new_insert_autoupload_clients();
									   // echo "Record Inserted successfully";
									    $redirect_url = site_url().'/newuploadimacroclient/new_autoupload_imacro_client/'.$client_id;
										redirect($redirect_url);									
										exit;
								 }elseif($_POST['token']=='8086866e2f8dd5f1cda291c9c2a44a72'){
										$post_data = $_POST;
									 	if($_POST['email_address']=="" || $_POST['email_address']=="-"){
											$random_no = strtolower($this->randomEmailKey(6));
											$post_data['email_address'] ='noemail'.$random_no.'@noemail.com';     //if email address is empty
										}
										
									 $hubspot_response =  $this->create_js3global_hubspot_contact( $post_data );
									 if($hubspot_response['success']==1){
										 
											$hubspot_contact_id = $hubspot_response['response'];
											$client_id =  $this->imacro_model->newlinkedin_insert_autoupload_clients($hubspot_contact_id);
											$this->update_js3global_hubspot_abtesting( $client_id, $hubspot_contact_id);
											
											$get_user = $this->db->query("SELECT clients.*,abtest.ab FROM pct_js3global_lead_clients as clients INNER JOIN pct_hubspot_ab_testing as abtest ON clients.id = abtest.user_id where clients.id='".$client_id."'");
											$row = $get_user->row_array();
											$set_id   =  $row['search_id'].$row['ab'];
											
											$config =  array();
											$email_template = $this->imacro_model->load_global_email_template('1', $set_id);           //send first email from seq of emails
											
											if($email_template){
												
												  $message = str_ireplace(array('[first_name]','[last_name]','[email_address]','[phone_number]','[city]','[country]','[company_name]','[company_number]','[job_title]','[linkedin_profile_url]'), array($row['first_name'],$row['last_name'],$row['email_address'],$row['phone_number'],$row['city'],$row['country'],$row['company'],$row['company_number'],$row['job_title'],$row['linkedin_profile_url']), $email_template['message']); 
												 
												 $config['message'] 	= $message;
												 $config['to'] 			= $row['email_address'];
												 $config['subject'] 	= $email_template['subject'];
												 
												 $query = "UPDATE pct_js3global_lead_clients SET email_number='1',last_email_time=NOW() WHERE id='".$client_id."'";
												 $update_data = $this->db->query($query);
												 sleep(3);
												 if($email_template['enable_email'] == "1"){
													$this->js3global_send_mail($config);     //send email
												 }
											}
										
												echo "Contact Created Successfully";
												exit;
									 }else{
										 echo $hubspot_response['response'];
										 exit;
									 }

								 }else{
									 echo "Token is not matched"; 
									 exit;
								 }
                        }
                } else {
					$delete_key = $this->generateRandomString(8);
					$unique_key = $this->random_alphabet('2').$this->random_number('2').$this->random_alphabet('2').$this->random_number('2');
					$data = array('title' => 'Add Client Trees', 'body' => 'new_autoupload_imacro_client_process','unique_key'=>$unique_key,'delete_key'=>$delete_key);
					$this->data = array_merge($this->data, $data);
					$this->load->view('template', $this->data);
                }
			
		}
		/**************************************************
		  function to Send automated emails to users
		*************************************************/
		function js3_global_automated_email_cronjob_old()  
		{	 			 
			 $this->js3_global_read_inbox_emails();   //to stop sending emails
			 $query = $this->db->query("SELECT clients.*,abtest.ab FROM pct_js3global_lead_clients as clients INNER JOIN pct_hubspot_ab_testing as abtest ON clients.id = abtest.user_id where clients.stop_emails='0'");
			 $rows = $query->result_array();
			
			 $norecord = 0;
			 foreach($rows as $row){
			
				 $config = array();
				 //echo "<pre>"; print_r($row); die;
				 // echo $row['created_at']."<br>";
				 // echo date("Y-m-d h:i:s")."<br>";
				$created_at			 =  strtotime($row['created_at']);
				$current_datetime    =  strtotime(date("Y-m-d h:i:s"));
				$interval  = abs($current_datetime - $created_at);
				$minutes   = round($interval / 60);

				 if ($minutes > 2 && $minutes <=30 && $minutes % 3 == 0) {         //send email for every three minutes
					 	$template_id =  $minutes/3;
						$set_id  	 =  $row['search_id'].$row['ab'];
						$email_template = $this->imacro_model->load_global_email_template($template_id, $set_id);
						if($email_template){
							 $message = str_ireplace(array('[first_name]','[last_name]','[email_address]','[phone_number]','[city]','[country]','[company_name]','[company_number]','[job_title]','[linkedin_profile_url]'), array($row['first_name'],$row['last_name'],$row['email_address'],$row['phone_number'],$row['city'],$row['country'],$row['company'],$row['company_number'],$row['job_title'],$row['linkedin_profile_url']), $email_template['message']); 
							 $config['message'] = $message;
							 $config['to'] 		= $row['email_address'];
							 $config['subject'] = $email_template['subject'];
							 $this->js3global_send_mail($config);     //send email
						}
						 //echo "<pre>"; print_r($email_template); die;
						 $norecord = 1;
						 echo "<h3>Email Sent to </h3>".$row['email_address'];
				 }
				/*   $days = $this->get_date_differnce($row['created_at']);         //calculdate days of created user
				  if ($days > 13 && $days <=70 && $days % 7 == 0) {
						$template_id =  $days/7;
						$set_id   =  $row['search_id'].$row['ab'];
						$email_template = $this->imacro_model->load_global_email_template($template_id, $set_id);
						if($email_template){
							 $message = str_ireplace(array('[first_name]'), array($row['first_name']), $email_template['message']); 
							 $config['message'] = $message;
							 $config['to'] 		= $row['email_address'];
							 $config['subject'] = $email_template['subject'];
							 $this->js3global_send_mail($config);     //send email
						}
						 //echo "<pre>"; print_r($email_template); die;
						 $norecord = 1;
						 echo "<h3>Email Sent to </h3>".$row['email_address'];
				   } */
				   
			 }
			 if($norecord == 0){
				 echo "<h3>No user to send email!</h3>";
			 }
			 
		}
		
		/**************************************************
		  function to Send automated emails to users
		*************************************************/
		function js3_global_automated_email_cronjob()  
		{
			
			 /* $this->js3_global_read_inbox_emails();   //to stop sending emails
			 $config = array();
			 $config['message'] = "test msg";
			 $config['to'] 		= "testing.whizkraft1@gmail.com";
			 $config['subject'] = "Test cronjob122";
			 $this->js3global_send_mail($config);
			 
			 die("here"); */
		
			 $this->js3_global_read_inbox_emails();   //to stop sending emails
			 $query = $this->db->query("SELECT clients.*,abtest.ab FROM pct_js3global_lead_clients as clients INNER JOIN pct_hubspot_ab_testing as abtest ON clients.id = abtest.user_id where clients.stop_emails='0' AND clients.email_number < '10' AND clients.created_at > '2019-05-07'");
			 $rows = $query->result_array();
			 
			  
			 $norecord = 0;
			 foreach($rows as $row){
			
				$config = array();
				$status = 0;
				$template_id =  $row['email_number']+1;
				$set_id  	 =  $row['search_id'].$row['ab'];
				$email_template = $this->imacro_model->load_global_email_template($template_id, $set_id);
				
				$email_time  = strtotime($row['last_email_time'].' + '.$email_template['time_period'].' minute');
				$current_datetime    =  strtotime(date("Y-m-d H:i:s"));

				if($email_template){
					
					  /* echo "<pre>"; print_r($row); print_r($email_template);
					  echo $row['last_email_time'];
					  echo date("Y-m-d H:i:s",$current_datetime);
					  echo "<br>";
					  echo date("Y-m-d H:i:s",$email_time); */
					if($current_datetime > $email_time){
					     //echo "<pre>"; print_r($row); print_r($email_template); die;
											 
						 $message = str_ireplace(array('[first_name]','[last_name]','[email_address]','[phone_number]','[city]','[country]','[company_name]','[company_number]','[job_title]','[linkedin_profile_url]'), array($row['first_name'],$row['last_name'],$row['email_address'],$row['phone_number'],$row['city'],$row['country'],$row['company'],$row['company_number'],$row['job_title'],$row['linkedin_profile_url']), $email_template['message']); 
						 
						 
						 $config['message'] = $message;
						 $config['to'] 		= $row['email_address'];
						 $config['subject'] = $email_template['subject'];
						 
						 if($email_template['enable_email'] == "1"){
							$status = $this->js3global_send_mail($config);
						 }
						 sleep(3); 
						 if($email_template['enable_email'] == "0"){
							 $query = "UPDATE pct_js3global_lead_clients SET email_number='".$template_id."',last_email_time=NOW() WHERE id='".$row['id']."'";
							 $this->db->query($query);
						 }
						 if($status == 1){
							 $query = "UPDATE pct_js3global_lead_clients SET email_number='".$template_id."',last_email_time=NOW() WHERE id='".$row['id']."'";
							 $this->db->query($query);
							 
							 $norecord = 1;
							 echo "<h3>Email Sent to </h3>".$row['email_address'];
						 }else{
							 echo $status;
						 }
					
						// echo "<pre>"; print_r($row); print_r($email_template); die;
					}
					
				}
				
			 }
			 if($norecord == 0){
				 echo "<h3>No user to send email!</h3>";
			 } 
			 
		}
		
		function random_alphabet($length, $keyspace = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')  //to create random alphabet
		{
		   $pieces = [];
		   $max = mb_strlen($keyspace, '8bit') - 1;
		   for ($i = 0; $i < $length; ++$i) {
			   $pieces []= $keyspace[mt_rand(0, $max)];
		   }
		   return implode('', $pieces);
		}

		function random_number($length, $keyspace = '0123456789')            //to create random number
		{
		   $pieces = [];
		   $max = mb_strlen($keyspace, '8bit') - 1;
		   for ($i = 0; $i < $length; ++$i) {
			   $pieces []= $keyspace[mt_rand(0, $max)];
		   }
		   return implode('', $pieces);
		} 
		function generateRandomString($length = 10) {
				$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$charactersLength = strlen($characters);
				$randomString = '';
				for ($i = 0; $i < $length; $i++) {
					$randomString .= $characters[rand(0, $charactersLength - 1)];
				}
				return $randomString;
		}
		
		/* To create hubspot contact from New linkedin autoupload process */
		function create_js3global_hubspot_contact($post_data){
				$arr = array(
						'properties' => array(
							array(
								'property' => 'email',
								'value' => $post_data['email_address']
							),
							array(
								'property' => 'firstname',
								'value' =>  ucfirst($post_data['first_name'])
							),
							array(
								'property' => 'lastname',
								'value' =>  ucfirst($post_data['last_name'])
							),
							array(
								'property' => 'company',
								'value' =>  $post_data['company']
							),
							array(
								'property' => 'jobtitle',
								'value' => $post_data['job_title']         
							),
							array(
								'property' => 'linkedin_company_number',
								'value' => $post_data['company_number']         
							),
							array(
								'property' => 'city',
								'value' => $post_data['city']         
							),
							array(
								'property' => 'country',
								'value' => $post_data['country']         
							),
							array(
								'property' => 'linkedin_profile_url',
								'value' => $post_data['linkedin_profile_url']         
							),
							array(
								'property' => 'phone',
								'value' => $post_data['phone_number']         
							),
							array(
								'property' => 'twitterhandle',
								'value' => $post_data['twitter1']         
							),
							array(
								'property' => 'website',
								'value' => $post_data['website']         
							),
							array(
								'property' => 'date_of_birth',
								'value' => $post_data['date_birth']         
							),
							array(
								'property' => 'search_id',
								'value' => $post_data['search_id']         
							)
						)
					);
			    //echo "<pre>"; print_r($arr); die;
				$hapikey = "24b6933b-b330-4830-9648-c5fd67d48f54";        //LIVE
				
				$post_json = json_encode($arr);
				$endpoint = 'https://api.hubapi.com/contacts/v1/contact?hapikey=' . $hapikey;
				$ch = @curl_init();
				@curl_setopt($ch, CURLOPT_POST, true);
				@curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
				@curl_setopt($ch, CURLOPT_URL, $endpoint);
				@curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$response = @curl_exec($ch);
				$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
				$curl_errors = curl_error($ch);
				@curl_close($ch);
				//echo "\nResponse: " . $response;
				if($status_code == 200 || $status_code == 204){
					$contactid = json_decode($response);
					return array("success"=>'1','response'=>$contactid->vid);
				}else{
					$error = json_decode($response);
					return array("success"=>'0','response'=>$error->message);
				}
		}
		
		/* To update hubspot contact from New linkedin autoupload process */
		function update_js3global_hubspot_abtesting($client_id, $hubspot_contact_id){
			
			  $query = "SELECT * from pct_hubspot_ab_testing order by id DESC Limit 1";
			  $row = $this->db->query($query);
              $result = $row->row_array();
						  
				$first_arr 		= 	array("A","B");
				$second_arr 	= 	array("A","B","C");
				$third_arr 		= 	array("A","B","C","D");
				$fourth_arr 	= 	array("A","B","C","D","E");
			
			  if($result){
				  
					$ab 	= $this->get_next_value($first_arr,  $result['ab']);
					$abc 	= $this->get_next_value($second_arr, $result['abc']);
					$abcd 	= $this->get_next_value($third_arr,  $result['abcd']);
					$abcde 	= $this->get_next_value($fourth_arr, $result['abcde']);
					
			  }else{
					$ab 	= "A";
					$abc 	= "A";
					$abcd 	= "A";
					$abcde 	= "A";
				  
			  }
			  $arr = array(
				'properties' => array(
					array(
						'property' => 'ab',
						'value' => $ab                
					),
					array(
						'property' => 'abc',
						'value' => $abc                
					),
					array(
						'property' => 'abcd',
						'value' => $abcd                
					),
					array(
						'property' => 'abcde',
						'value' => $abcde                
					)
				));
				$update_contact = $this->update_js3global_hubspot_contact_prop($arr, $hubspot_contact_id);
				if($update_contact == 200 || $update_contact == 204){
					$data = array();			
					$data['hb_contact_id']  = $hubspot_contact_id;
					$data['ab'] 			= $ab;
					$data['abc'] 			= $abc;
					$data['abcd'] 			= $abcd;
					$data['abcde'] 			= $abcde;
					$data['user_id'] 		= $client_id;
					$this->db->insert_batch('pct_hubspot_ab_testing', array($data));    				
				}
						
		}
		
	/**************************************************
	function to check the previous contact properties
	*************************************************/
	
	function get_next_value($data, $key){
		
		$total_record = count($data)-1;
		$index  =  array_search($key, $data); 
		if($total_record == $index){
			$new_key = "A";
		}else{
			$next_index  = $index + 1;
			$new_key = $data[$next_index];
		}
		return $new_key;
		
	}
	function update_js3global_hubspot_contact_prop($post_data, $contact_id){
		
			$hapikey = "24b6933b-b330-4830-9648-c5fd67d48f54";        //LIVE
			
			$post_json = json_encode($post_data);
			$endpoint = 'https://api.hubapi.com/contacts/v1/contact/vid/'.$contact_id.'/profile?hapikey=' . $hapikey;
			$ch = @curl_init();
			@curl_setopt($ch, CURLOPT_POST, true);
			@curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
			@curl_setopt($ch, CURLOPT_URL, $endpoint);
			@curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = @curl_exec($ch);
			$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$curl_errors = curl_error($ch);
			@curl_close($ch);
			$curl_errors = "curl Errors to update contact: " . $curl_errors.' Status Code: '.$status_code;
			return $status_code;
		
	}
		/**
		 * =========================================================================================================
		 * common functions
		 */

        function js3global_send_mail($data) {

                $mail             = new PHPMailer();
                $mail->IsSMTP(); 										// telling the class to use SMTP
                $mail->SMTPDebug  = 0;                     				// enables SMTP debug information (for testing)
                $mail->SMTPAuth   = true;                 			 	// enable SMTP authentication
                $mail->SMTPSecure = "tls";                 				// sets the prefix to the servier
                $mail->Host       = "smtp.office365.com";      				// sets GMAIL as the SMTP server
                $mail->Port       = 587;                   				// set the SMTP port for the GMAIL server
                $mail->Username   = "eucommission@js3global.com"; 	// GMAIL username
                $mail->Password   = "Hub75181";            		// GMAIL password
                $mail->CharSet 	  = 'UTF-8';
				
                if (isset($data['from'])) {
                        $mail->SetFrom($data['from'], 'JS3 Global');
                }else{
					$mail->SetFrom('eucommission@js3global.com', 'JS3 Global');
				}

                if (isset($data['attach']) && !empty($data['attach'])) {
                        if (!is_array($data['attach'])) {
                                $data['attach'] = (array) $data['attach'];
                        }
                        foreach ($data['attach'] AS $attach) {
                                $mail->AddAttachment($attach); // attachment
                        }
                }

                ob_start();
               /*  if ((isset($data['signature']) && $data['signature'] == false) || (!$data['signature'])) {
                        echo '<style type="text/css">*, p {font-family:Calibri!important; font-size: 11pt!important;}</style>';
                        require('libraries/signature.htm');
                } else {
                        require('libraries/signature_solicitor.htm');
                } */

                $message = ob_get_contents();
                ob_end_clean();

                $message = $data['message'] . $message;

                $mail->ClearReplyTos();
				if($data['replyto']){
					 $mail->AddReplyTo($data['replyto'], "");
				}else{
					 $mail->AddReplyTo("eucommission@js3global.com", "JS3 Global");
				}
               
                $mail->Subject    = $data['subject'];
                $mail->MsgHTML($message);
                $mail->AddAddress($data['to']);

                if (isset($data['cc'])) {
                        $cc_emails = explode(',', $data['cc']);
                        foreach ($cc_emails AS $cc) {
                                $mail->AddCC($cc);
                        }
                }

                if (isset($data['bcc'])) {
                        $bcc_emails = explode(',', $data['bcc']);
                        foreach ($bcc_emails AS $bcc) {
                                $mail->AddBCC($bcc);
                        }
                }
                if (!$mail->Send()) {
                        $status = "Mailer Error: " . $mail->ErrorInfo;
                }else{
					    $status = 1;
				}
				return $status;

        }
		
	     /**
         * =========================================================================================================
         * Date Difference
         */
        function get_date_differnce($date="") {
				$now = strtotime(date('Y-m-d'));  				// current date
				$dt = new DateTime($date);
				$date_created = strtotime($dt->format('Y-m-d'));
				$datediff = $now - $date_created;
				$days = floor($datediff / (60 * 60 * 24)); 
				return $days;
		}
		
		function randomEmailKey($length) {  //create no of random digit alphanumeric code
			$key='';
			$pool = array_merge(range(0,9),range('A', 'Z'));

			for($i=0; $i < $length; $i++) {
				$key .= $pool[mt_rand(0, count($pool) - 1)];
			}
			return $key;
		}
			
		
		/**************************************************
		  function to read inbox emails for a day and stop emails if replied (IMAP)
		*************************************************/
		function js3_global_read_inbox_emails()  
		{
			$hostname = '{outlook.office365.com:993/imap/ssl}INBOX';
			$username = 'eucommission@js3global.com';
			$password = 'Hub75181'; 
			
			$inbox = imap_open($hostname,$username,$password) or die('Cannot connect to: ' . imap_last_error());
			$date  = date('Y-m-d');        						//Current Day 			
			//$date = date('Y-m-d',strtotime("-1 days"));        //Previous Day       
			//$emails = imap_search($inbox,'ALL');
			$emails = imap_search($inbox, "ON \"$date\"" ); 
			if($emails) {
				rsort($emails);
				foreach($emails as $email_number) {
					//if(isset($overview[0]->in_reply_to)){
					//echo "<pre>"; print_r($overview); 
					$header 	= imap_headerinfo($inbox, $email_number);
					$structure  = imap_fetchstructure($inbox,$email_number);
					$from_email = $header->from[0]->mailbox . "@" . $header->from[0]->host;
					$overview = imap_fetch_overview($inbox,$email_number,0);
					 $attachments = array();
					  if(isset($structure->parts) && count($structure->parts)) {
						 for($i = 0; $i < count($structure->parts); $i++) {
						   $attachments[$i] = array(
							  'is_attachment' => false,
							  'filename' => '',
							  'name' => '',
							  'attachment' => '');

						   if($structure->parts[$i]->ifdparameters) {
							 foreach($structure->parts[$i]->dparameters as $object) {
							   if(strtolower($object->attribute) == 'filename') {
								 $attachments[$i]['is_attachment'] = true;
								if( strpos( $object->value, "UTF-8" ) !== false) {
									 $attachments[$i]['filename'] = iconv_mime_decode($object->value,0,"UTF-8"); 
								}elseif(strpos( $object->value, "utf-8" ) !== false){
									$attachments[$i]['filename'] = iconv_mime_decode($object->value,0,"utf-8"); 
								}else{
									 $attachments[$i]['filename'] = $object->value; 
								}
								
							   }
							 }
						   }

						   if($structure->parts[$i]->ifparameters) {
							 foreach($structure->parts[$i]->parameters as $object) {
							   if(strtolower($object->attribute) == 'name') {
								 $attachments[$i]['is_attachment'] = true;
								 $attachments[$i]['name'] = $object->value;
							   }
							 }
						   }

						   if($attachments[$i]['is_attachment']) {
							 $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i+1);
							 if($structure->parts[$i]->encoding == 3) { // 3 = BASE64
							   $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
							 }
							 elseif($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
							   $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
							 }
						   }             
						 } 
					} 
					
						$getuser 	= $this->db->query("SELECT * FROM pct_js3global_lead_clients WHERE email_address='".$from_email."'");
						$userdata 	= $getuser->row();
						if($userdata){
							
							$existing_msg_no = explode(',', $userdata->attachment_email_no);
							$config = array();  
							if(count($attachments)!=0){
								foreach($attachments as $at){
									if($at['is_attachment']==1){
										$path_parts = pathinfo($at['filename']);
										$rename_file = $path_parts['filename'].'-'.rand(10000,99999).'.'.$path_parts['extension'];
										$filename = dirname(__FILE__) . '/../../js3global-email-cv/'.$rename_file;
										if($path_parts['extension']=="pdf" || $path_parts['extension']=="PDF" || $path_parts['extension']=="doc" || $path_parts['extension']=="DOC" || $path_parts['extension']=="docx" || $path_parts['extension']=="DOCX"){
											
											$config['attach'][] = $filename;
											
											if(!in_array($overview[0]->msgno, $existing_msg_no)){
												file_put_contents($filename, $at['attachment']);
											}
										}
										
									}
								}
							}
								
								if($config['attach']){
										if(!in_array($overview[0]->msgno, $existing_msg_no)){
											$attachment_msg_number = "";
											if($userdata->attachment_email_no != 0){
												 $attachment_msg_number = $userdata->attachment_email_no.','.$overview[0]->msgno;
											}else{
												$attachment_msg_number = $overview[0]->msgno;
											}
											 $config['message'] = " ";
											 $config['to'] 		= "cvs@chameleoni.com";
											 $config['subject'] = $overview[0]->subject;
											 $config['replyto'] = $from_email;
											 // echo $overview[0]->msgno.' '.$from_email;
											 // echo "<br>";
											 $status = $this->js3global_send_mail($config);
											  if($status == 1){
												 
												  $attachquery = "UPDATE pct_js3global_lead_clients SET attachment_email_no='".$attachment_msg_number."' WHERE email_address='".$from_email."'";
												  $this->db->query($attachquery);
												  												  
												  $get_thankyou_email 	= $this->db->query("SELECT * FROM pct_global_email_templates WHERE id='41'");
												  $thanku_template 		= $get_thankyou_email->row_array();
												  
												  $configarr = array();
												  												   
												   $message = str_ireplace(array('[first_name]','[last_name]','[email_address]','[phone_number]','[city]','[country]','[company_name]','[company_number]','[job_title]','[linkedin_profile_url]'), array($userdata->first_name, $userdata->last_name, $userdata->email_address, $userdata->phone_number, $userdata->city, $userdata->country, $userdata->company, $userdata->company_number, $userdata->job_title, $userdata->linkedin_profile_url), $thanku_template['message']); 
												   
												  $configarr['message'] = $message;
												  $configarr['to'] 		= $from_email;
												  $configarr['subject'] = $thanku_template['subject'];
												  $this->js3global_send_mail($configarr);     //send email 
												  
												  sleep(4);
												  /* if(isset($config['attach'][0])){
														unlink($config['attach'][0]);       //to remove cv from directory
												  } */
												   $folder = 'js3global-email-cv';
													//Get a list of all of the file names in the folder.
												   $files = glob($folder . '/*');
													foreach($files as $file){
														if(is_file($file)){
															unlink($file);
														}
													}
											  } 
									}
								}
						}
						
					
					
					if($userdata && $userdata->stop_emails ==0){
						 $query = "UPDATE pct_js3global_lead_clients SET stop_emails='1' WHERE email_address='".$from_email."'";
						 $this->db->query($query);
					}
					//echo "<pre>"; print_r($userdata); 
				}
			} 
			imap_close($inbox);
		}
		
		
}