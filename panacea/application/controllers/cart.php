<?php

defined('_JEXEC') or die('Restricted access');
?><?php
global $Itemid;
$url_itemid='';
if(!empty($Itemid)){
	$url_itemid='&Itemid='.$Itemid;
}
$this->setLayout('listing_price');
$this->params->set('show_quantity_field', 0);
$desc = $this->params->get('msg');
$cart_type = $this->params->get('cart_type','cart');
if($cart_type == 'wishlist'){
	$convertText = JText::_('WISHLIST_TO_CART');
	$displayText = JText::_('DISPLAY_THE_WISHLIST');
	$displayAllText = JText::_('DISPLAY_THE_WISHLISTS');
	$emptyText = JText::_('WISHLIST_EMPTY');
}else{
	$convertText = JText::_('CART_TO_WISHLIST');
	$displayText = JText::_('DISPLAY_THE_CART');
	$displayAllText = JText::_('DISPLAY_THE_CARTS');
	$emptyText = JText::_('CART_EMPTY');
}
if(empty($desc) && $desc != '0'){
	$this->params->set('msg',$emptyText);
}
if(!headers_sent()){
	header( 'Cache-Control: no-store, no-cache, must-revalidate' );
	header( 'Cache-Control: post-check=0, pre-check=0', false );
	header( 'Pragma: no-cache' );
}
$cart_type=$this->params->get('cart_type','cart');
if($this->params->get('from','no') == 'no'){
	$this->params->set('from',JRequest::getString('from','display'));
}
if(empty($this->rows)){
    echo ('Cart (0)');
}else{
    $pct_num = 0;
    foreach ($this->rows AS &$v) {
        $pct_num += (int)$v->cart_product_quantity;
    }    
?>
<span id="pct_cart_open">Cart (<?php echo $pct_num;?>)</span>
<style>
    #pct_hikashop_cart {position: absolute; left:-30px; top: 40px; z-index: 9999; background: black; color: white; font-size: 14px; width:270px; padding: 10px}
    #pct_hikashop_cart a {color: #FDE7C9}
    #pct_hikashop_cart a:hover {color: white;}
    #pct_hikashop_cart span.hikashop_product_price {color: white;}
    #pct_hikashop_cart form {margin:0}
</style>
<div id="pct_hikashop_cart" class="pct_hikashop_cart" style="display: none;">
    <form action="<?php echo hikashop_completeLink('product&task=updatecart'.$url_itemid,false,true); ?>" method="post" name="<?php echo $form;?>">
		<div id="pct_hikashop_cart_inner">
			<div>
				<?php
					$k = 0;
					$this->cart_product_price = true;
					$group = $this->config->get('group_options',0);
					$cart_id = 0;
					$app = JFactory::getApplication();
					$productClass = hikashop_get('class.product');

					$defaultParams = $this->config->get('default_params');

					$this->image = hikashop_get('helper.image');
					$height = $this->config->get('thumbnail_y');
					$width = $this->config->get('thumbnail_x');
					foreach($this->rows as $i => $row){
						$cart_id = $row->cart_id;
						if(empty($row->cart_product_quantity)&& $cart_type  != 'wishlist' || @$row->hide == 1) continue;
						if($group && $row->cart_product_option_parent_id) continue;
						$productClass->addAlias($row);
						?>
						<div class="<?php echo "row$k"; ?>" style="clear: both; overflow:hidden">
                            <div class="pct_cart_img" style="float:left;width:90px;margin-right:15px;">
    							<?php
    								echo $this->image->display(@$row->images[0]->file_path,true,@$row->images[0]->file_name,'id="hikashop_main_image_'.$row->product_id.'" style="margin-top:10px;margin-bottom:10px;display:inline-block;vertical-align:middle"','', $width,  $height);
    							?>
                            </div>
							<div class="pct_cart_content">
								<?php
                                    echo $row->cart_product_quantity.' x &nbsp;';
                                    if(@$defaultParams['link_to_product_page']){ ?> <a href="<?php echo hikashop_completeLink('product&task=show&cid='.$row->product_id.'&name='.$row->alias.$url_itemid);?>" ><?php } ?>
									<?php echo $row->product_name; ?>
									<?php if ($this->config->get('show_code')) { ?>
										<span class="hikashop_product_code_cart"><?php echo $row->product_code; ?></span>
									<?php } ?>
								<?php if(@$defaultParams['link_to_product_page']){ ?></a><?php } ?>
								<span class="hikashop_cart_product_custom_item_fields">
									<?php
									if(hikashop_level(2) && !empty($this->itemFields)){
										foreach($this->itemFields as $field){
											$namekey = $field->field_namekey;
											if(!empty($row->$namekey) && !strlen($row->$namekey)){
												echo '<p class="hikashop_cart_item_'.$namekey.'">'.$this->fieldsClass->getFieldName($field).': '.$this->fieldsClass->show($field,$row->$namekey).'</p>';
											}
										}
									}
								$input='';
								if($group){
									foreach($this->rows as $j => $optionElement){
										if($optionElement->cart_product_option_parent_id != $row->cart_product_id) continue;
										if(!empty($optionElement->prices[0])){
											if(!isset($row->prices[0])){
												$row->prices[0]->price_value=0;
												$row->prices[0]->price_value_with_tax=0;
												$row->prices[0]->price_currency_id = hikashop_getCurrency();
											}
											foreach(get_object_vars($row->prices[0]) as $key => $value){
												if(is_object($value)){
													foreach(get_object_vars($value) as $key2 => $var2){
														if(strpos($key2,'price_value')!==false) $row->prices[0]->$key->$key2 +=@$optionElement->prices[0]->$key->$key2;
													}
												}else{
													if(strpos($key,'price_value')!==false) $row->prices[0]->$key+=@$optionElement->prices[0]->$key;
												}
											}
										}
										 ?>
									<?php
									$input .='document.getElementById(\'cart_product_option_'.$optionElement->cart_product_id.'\').value=qty_field.value;';
									echo '<input type="hidden" id="cart_product_option_'.$optionElement->cart_product_id.'" name="item['.$optionElement->cart_product_id.'][cart_product_quantity]" value="'.$row->cart_product_quantity.'"/>';
									}
								}
									?>
								</span>
                            </div>
                            <div class="pct_price">
                                <?php
    								$this->row=&$row;
    								echo $this->loadTemplate();
								?>
                            </div>
						</div>                           
						<?php
						$k = 1-$k;
					}
                    
					$this->cart_product_price=false;
				?>
                </div>
                <div style="float: left;"><?php echo $pct_num;?> Products</div>
                <?php if($this->params->get('show_price',1) && $this->params->get('cart_type','cart') != 'wishlist'){ ?>
                <?php
					$this->row=$this->total;
					echo '<div style="float: right;">'.$this->loadTemplate().'</div>';
				?>
			     <?php } ?>
                 <br clear="both" />
                 <div style="float: left;"><?php echo $this->cart->displayButton(JText::_('View Cart'),'checkout',$this->params,hikashop_completeLink('checkout'.$url_itemid),'');?>
                </div>
                <div style="float: right;"><input type="button" id="pct_cart_close" value="Close" class="button btn" /></div>
            <?php
            /*
            if($this->params->get('cart_type','cart') != 'wishlist'  && $this->params->get('from','display') == 'module'){
    			if($this->params->get('show_cart_proceed',1)) echo '<div align="center">'.$this->cart->displayButton(JText::_('PROCEED_TO_CHECKOUT'),'checkout',$this->params,hikashop_completeLink('checkout'.$url_itemid),'').'</div>';
    		} else {
    			?><div class="hikashop_display_cart_show_convert_button"><?php
    			$cart_type = '&cart_type='.$this->params->get('cart_type','cart');
    			if($this->params->get('from','display') != 'module'){
    				echo $this->cart->displayButton($convertText,'wishlist',$this->params,hikashop_completeLink('cart&task=convert'.$url_itemid.$cart_type),'window.location.href = \''.hikashop_completeLink('cart&task=convert'.$url_itemid.$cart_type).'\';return false;');
    			}
    			else{
    				echo '<div align="center">'.$this->cart->displayButton($displayText,'wishlist',$this->params,hikashop_completeLink('cart&task=showcart&cart_id='.$cart_id.$url_itemid.$cart_type),'window.location.href = \''.hikashop_completeLink('cart&task=showcart&cart_id='.$cart_id.$url_itemid.$cart_type).'\';return false;').'</div>';
    			}
    			?></div><?php
    		}
            */
            ?>                 
			</div>
			<?php
			if($this->params->get('show_cart_quantity',1)){ ?>
				<noscript>
					<input type="submit" class="btn button" name="refresh" value="<?php echo JText::_('REFRESH_CART');?>"/>
				</noscript>
			<?php }
		
		?>
		<input type="hidden" name="url" value="<?php echo $this->params->get('url');?>"/>
		<input type="hidden" name="ctrl" value="product"/>
		<input type="hidden" name="task" value="updatecart"/>
	</form>
	
</div>
<div class="clear_both"></div>
<?php } ?>
<?php
if(JRequest::getWord('tmpl','')=='component'){
	if(!headers_sent()){
		header('Content-Type: text/css; charset=utf-8');
	}
	exit;
}
