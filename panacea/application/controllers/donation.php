<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Donation extends CI_Controller {

		/**
		 * Index Page for this controller.
		 */
			public function __construct() {
			parent::__construct();
			session_start();
			$this->data = array();
			$this->load->helper('url');
			$this->load->helper('form');
			$this->load->model('user_model');
			$segment_login = $this->uri->segment(2);
			require_once('phpmailer/class.phpmailer.php');
                
        }
		/* To receive post data and insert in the donation table database */
		public function post_donation_data($post = NULL)
		{
			 $posts = json_decode(file_get_contents('php://input'), true);
			// echo "<pre>"; print_r($post); die;
			 if($posts){
					  $papi_data = array();
					  foreach($posts as $key=>$value){
						
						  $rmslsh = preg_replace('/\\\\/', '', $value);
						  $papi_data[$key] = str_replace('/','',$rmslsh);
					 } 
					 // echo "<pre>"; print_r($papi_data); die;
					 //$papi_data =  array_map('htmlentities',$papi_data);
					 $api_data['panacea_id'] 		    = "26178";
					 $api_data['donation_json_data'] 	= html_entity_decode(json_encode($papi_data));
					 
					 $insert_id = $this->db->insert('pct_donation_sales_data ', $api_data);
					 
					 if($insert_id){
						 
						  exit("Data Inserted Successfully");
						  
					 }else{
						 
						  exit("Something went wrong.");
						  
					 }
			}
			exit();
			
		}

}
