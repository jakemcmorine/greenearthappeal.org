<?php
if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Euambassador extends CI_Controller {

        public function __construct() {
                parent::__construct();
                session_start();
			if (!isset($_SESSION['login']))
                        $_SESSION['login'] = false;
                $this->data = array();
                $this->load->helper('url');
                $this->load->helper('form');
				$this->load->database();
				$this->load->model('user_model');
				$this->load->model('ambassador_model');
                $segment_login = $this->uri->segment(2);
                // check if user logged in
                if ((!isset($_SESSION['login']) || !$_SESSION['login']) && !in_array($segment_login, array('login','gc_reminder_email_cronjob','load_eu_templates','load_eu_single_template','load_eu_fronthouse_templates','load_eu_single_fronthous_template')) ) {
                        redirect('user/login');
                        die();
                }

                if (isset($_SESSION['login']['type']) && $_SESSION['login']['type'] == 'bulk_partner') {
                        $this->data['latest_certificate_url'] = '';
                         // get the latest bulk_partner certificate
                        $this->load->database();
                        $qr = "SELECT latest_certificate FROM pct_invoices WHERE user_id= {$_SESSION['login']['id']} ORDER BY id DESC LIMIT 1";
                        $query = $this->db->query($qr);

                        if ($query->num_rows()) {
                                $rowq = $query->row_array();
                                $this->data['latest_certificate_url'] = $rowq['latest_certificate'];
                        }

                }
				if (isset($_SESSION['login']['type']) && $_SESSION['login']['type'] == 'intitiative') {
                        $this->data['latest_certificate_url'] = '';
                        // get the latest bulk_partner certificate
                        $qr = "SELECT latest_certificate FROM pct_invoices WHERE user_id= {$_SESSION['login']['id']} ORDER BY id DESC LIMIT 1";
                        $query = $this->db->query($qr);

                        if ($query->num_rows()) {
                                $rowq = $query->row_array();
                                $this->data['latest_certificate_url'] = $rowq['latest_certificate'];
                        }
                }

                require_once('phpmailer/class.phpmailer.php');
                
        }
		
		function email_templates() {
			 	
                $this->check_user(array("admin","initiative_editor"));
				
				if (isset($_POST['edit_euamb_emails'])) {
						// echo "<pre>"; print_r($_POST); die;
                        $this->load->library('form_validation');
						for($i=1; $i<=12; $i++){
							
							$this->form_validation->set_rules('data['.$i.'][subject]');
							$this->form_validation->set_rules('data['.$i.'][name]');
							$this->form_validation->set_rules('data['.$i.'][message]');
							$this->form_validation->set_rules('data['.$i.'][time_period]');
							$this->form_validation->set_rules('data['.$i.'][enable_email]');
								
						}  
				
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false) {
							
								$query = "SELECT * FROM pct_euamb_email_templates ORDER BY id";
								$rows = $this->db->query($query);
								$data3['rows'] = $rows->result_array();
								$data = array('title' => 'Edit EU Ambassador Emails', 'body' => '/eu-ambassador/edit_euamb_template_emails');
                                $this->data = array_merge($this->data, $data,$data3);
                                $this->load->view('template', $this->data);
								
                        } else {
								//echo "<pre>"; print_r($_POST); die;
                                $this->ambassador_model->update_euamb_email_templates();
                                redirect(site_url('/euambassador/email_templates'));
						}
					   
                } else {
					$query = "SELECT * FROM pct_euamb_email_templates ORDER BY id";
					$rows = $this->db->query($query);
					$data3['rows'] = $rows->result_array();
					$data = array('title' => 'Edit EU Ambassador Emails', 'body' => '/eu-ambassador/edit_euamb_template_emails.php');
					$this->data = array_merge($this->data, $data,$data3);
					//echo "<pre>"; print_r($this->data); die('here');
					$this->load->view('template', $this->data);
                }
				
				
		} 
		 
		 /* Front of house email templates */
		function front_of_house_emails() {
			 	
                $this->check_user(array("admin","initiative_editor"));
				
				 if (isset($_POST['edit_front_emails'])) {
						// echo "<pre>"; print_r($_POST); die;
                        $this->load->library('form_validation');
						 for($i=1; $i<=7; $i++){
								$this->form_validation->set_rules('data['.$i.'][subject]');
								$this->form_validation->set_rules('data['.$i.'][name]');
								$this->form_validation->set_rules('data['.$i.'][message]');
								$this->form_validation->set_rules('data['.$i.'][time_period]');
								$this->form_validation->set_rules('data['.$i.'][enable_email]');
						 }  
				
                        $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
                        // validate form
                        if ($this->form_validation->run() === false) {
							
								$query = "SELECT * FROM pct_euamb_frontofhouse_emails_templates ORDER BY id";
								$rows = $this->db->query($query);
								$data3['rows'] = $rows->result_array();
								$data = array('title' => 'Edit EU Ambassador Emails', 'body' => '/eu-ambassador/front_of_house_emails');
                                $this->data = array_merge($this->data, $data,$data3);
                                $this->load->view('template', $this->data);
                        } else {
								//echo "<pre>"; print_r($_POST); die;
                                $this->ambassador_model->update_eu_fronthouse_email_templates();
                                redirect(site_url('/euambassador/front_of_house_emails'));
						}
					   
                } else {
					$query = "SELECT * FROM pct_euamb_frontofhouse_emails_templates ORDER BY id";
					$rows = $this->db->query($query);
					$data3['rows'] = $rows->result_array();
					$data = array('title' => 'Edit EU Ambassador Emails', 'body' => '/eu-ambassador/edit_fronthouse_templates.php');
					$this->data = array_merge($this->data, $data,$data3);
					//echo "<pre>"; print_r($this->data); die('here');
					$this->load->view('template', $this->data);
                }
			
		 } 
		 
		function load_eu_templates() {
                $query = "SELECT * FROM pct_euamb_email_templates ORDER BY id";
				$rows = $this->db->query($query);
				$data = $rows->result_array();
				echo json_encode($data);
        }
		function load_eu_single_template($template_id) {
                $query = "SELECT * FROM pct_euamb_email_templates where id=$template_id";
				$rows = $this->db->query($query);
				$data = $rows->row_array();
				echo json_encode($data);
        }
		
		function load_eu_fronthouse_templates() {
                $query = "SELECT * FROM pct_euamb_frontofhouse_emails_templates ORDER BY id";
				$rows = $this->db->query($query);
				$data = $rows->result_array();
				echo json_encode($data);
        }
		function load_eu_single_fronthous_template($template_id) {
                $query = "SELECT * FROM pct_euamb_frontofhouse_emails_templates where id=$template_id";
				$rows = $this->db->query($query);
				$data = $rows->row_array();
				echo json_encode($data);
        }
		
		
		 /* Function: For GoCardless Reminder emails */
		/*  
		function gc_reminder_email_cronjob() {
				
				$api_url = "https://carbonfreedining.org/eu-amb-gc-email-cronjob.php";
				$data = file_get_contents($api_url);
				$rows = json_decode($data);
				echo "<pre>"; print_r($rows); die;
				
				foreach($rows as $row){
					$config = array();
					$email_time  = strtotime($row['last_email_time'].' + '.$email_template['time_period'].' minute');
				}
			
		} */
		 
		 function check_user($user_type = 'admin') {
                if (!is_array($user_type)) {
                        $user_type = (array) $user_type;
                }

                $check = in_array($_SESSION['login']['type'], $user_type);
                if (!$check) {
                        echo 'You dont have permission.';
                        die();
                }
                return true;
        }
		
}