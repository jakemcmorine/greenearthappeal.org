<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// stop
//error_reporting(0);

class Ticker extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->data = array();
        $this->load->helper('url');
    }
    
	public function index()
	{
        $this->load->library('image_lib');
        $config['source_image']	= dirname(__FILE__).'/../../uploads/ticker.jpg';
        //$config['new_image'] = dirname(__FILE__).'/../../uploads/ticker2.jpg';
        $config['dynamic_output'] = true;
        $config['wm_text'] = 'Counter: 155 trees';
        $config['wm_type'] = 'text';
        $config['wm_font_size']	= '16';
        $config['wm_font_color'] = 'ffffff';
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $config['wm_padding'] = '40';
        $this->image_lib->initialize($config); 
        $this->image_lib->watermark();
        // $this->image_lib->initialize($config);
        
        // $this->image_lib->watermark();
        echo $this->image_lib->display_errors();
        echo 'success';
    }
}