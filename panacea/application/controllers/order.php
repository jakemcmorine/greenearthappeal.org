<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// stop
error_reporting(0);

class Order extends CI_Controller {
    public function __construct() {
        parent::__construct();
        session_start();
        if (!isset($_SESSION['login'])) $_SESSION['login'] = false;
        $this->data = array();
        $this->load->helper('url');
        $this->load->helper('form');
        $segment_login = $this->uri->segment(2);

		require_once('phpmailer/class.phpmailer.php');
    }
    
	public function index()
	{

	   // process member level here
        //$this->check_user(array('admin', 'restaurant',));
        $this->load->model('order_model');
		
		$this->load->database();
        $this->load->model('user_model');
redirect(site_url('order/order_trees'));
       
	}
public function order_trees($code)
{
 if (isset($_POST['order_trees_no'])) {

            // process restaurant data
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
	    	$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('restaurant', 'Restaurant Name', 'trim|required');
            $this->form_validation->set_rules('website', 'Website', 'trim|required');
	    	$this->form_validation->set_rules('twitter', 'Twitter Url', 'trim|required');
	    //$this->form_validation->set_rules('facebook', 'Facebook Url', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required');
            $this->form_validation->set_error_delimiters('<div class="text-error">', '</div>');
            // validate form

            if ($this->form_validation->run()=== false) {
    		$this->load->model('order_model');
		$codeA = explode('-',$code);
		$result = $this->order_model->get_affiliate_list($codeA[1]);
                $data = array('title'=>'Add restaurant', 'body'=>'order_trees','result'=>$result);
                $this->data = array_merge($this->data, $data);
                $this->load->view('template_order', $this->data);
            } else {           
                $this->load->database();
  		$this->load->model('user_model');
                $this->load->model('order_model');
		$ranCode = $this->generateCode(4);
                $user_id = $this->order_model->insert_restaurant($ranCode);
                // create the certificate
		$restaurant     = $_POST['restaurant'];
		$first_name     = $_POST['first_name'];
		$last_name     	= $_POST['last_name'];
		$total_trees    = $_POST['tree_nums'];
		$uemailid = 	$_POST['email'];
		$client_trees = $total_trees;
		
		//$rest_logo      = $_SESSION['login']['data']['logo'];
		$ds = DIRECTORY_SEPARATOR;
		
		// create the ticker
		$refid    = $this->user_model->get_refid($user_id);
        	$ticker_url = base_url().'tickers/ticker_'. $refid->code .'.png';
        	$this->create_ticker($total_trees, $refid->code);
		
		/*
		$pct_logo_url = '';
		if (file_exists( dirname(__FILE__).'/../../uploads/restaurant/'.$rest_logo ) ) {
		    $pct_logo = 'uploads/restaurant/'.$rest_logo;
		    $pct_logo_url = '<img src="'.$pct_logo.'" title="logo" />';
		}
		*/
 		$prev_month_text = date("F_Y", strtotime("first day of previous month") );
		//$invoice_name = 'The_Green_Earth_Appeal_Invoice_'. $prev_month_text .'_'.$user_id.'.pdf';
		$cert_name = 'The_Green_Earth_Appeal_Certificate_'. $prev_month_text .'_'.$user_id.'.pdf';
		require('libraries/cert.php');
		
		// send certificate to the restaurant
		$email_template = $this->user_model->load_email_template(5);
		$message = '';
		//$message = $email_template->message;
		//$message = str_ireplace(array('[name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]'),
		                       // array($restaurant, $total_trees, $figure['trees'], $total, $first_name, $ticker_url),
		                       // $message);
		$config = array();
		$config['to'] = $uemailid;
		$config['subject'] = $email_template->subject;
		$config['message'] = $message;
		$config['attach'] = array(dirname(__FILE__).'/../../uploads/certs/'.$cert_name);
		$this->send_mail($config);
		
		
		$config2 = array();
		$config2['to'] = 'marvin@greenearthappeal.org';
		//$config2['to'] = 'thakuryogita@gmail.com';
		$config2['subject'] = 'Submission Of Auto Tree Order';
		$message2 = 'Please see following Order Submission : <br /><br />';
		$message2.= 'Name : '.$first_name. ' ' . $last_name.'<br />';
		$message2.= 'Email : '.$uemailid.'<br />';
		$message2.= 'Phone Number : '.$_POST['phone'].'<br />';
		$message2.= 'Restaurant Name : '.$_POST['restaurant'].'<br />';
		$message2.= 'Number Of Trees : '.$total_trees.'<br />';
		$message2.= 'Website : '.$_POST['website'].'<br />';
		$message2.= 'Twitter : '.$_POST['twitter'].'<br />';
		//$message2.= 'Facebook : '.$_POST['facebook'].'<br />';
		$config2['message'] = $message2;
		$this->send_mail($config2);
                
				
		$this->postTweet('greenearthapp','Thank you @'.$_POST['twitter'].' for letting us plant 10 trees on your behalf.');
		$this->postTweet('food4thoughtuk','Thank you @'.$_POST['twitter'].' for letting us plant 10 trees on your behalf.');		
            }
        } else {
	    $this->load->model('order_model');
	    $codeA = explode('-',$code);
	    $result = $this->order_model->get_affiliate_list($codeA[1]);

            $data = array('title'=>'Add restaurant', 'body'=>'order_trees','result'=>$result);
            $this->data = array_merge($this->data, $data);
            $this->load->view('template_order', $this->data);
        }
}


	function postTweet($account, $tweetText) {	
		require_once(dirname(__FILE__).'/../../libraries/twitter_api/oauth.php');	
	
		$credentials = Array( // PUT ACCESS CREDENTIALS FOR EACH ACCOUNT HERE //
			'greenearthapp' => Array (
				'consumer_key' => 'HYWhVfPTBRceHsvW0hhZImTIJ', //API Key
				'consumer_secret' => 'sh8jC3m1X3D0hK7v4SvZRWaO6H7wyqLhbcMbMvebakwfIhEUWG', //API Secret
				'user_token' => '133217356-zgpisfvTZJfBZw0uB15DraX8YKvFLIqEpRtBLszZ', //Access Token
				'user_secret' => 'LNNClzSO1OYyDKxwZl0RHgngEJ53NYPyWnFyEJTcrQC2N' //Access Token Secret
			),
			'food4thoughtuk' => Array (
				'consumer_key' => 'zzwwxiDghEROyd8GpTQKaq9Bc', //API Key
				'consumer_secret' => 'F7wlhXhkajeWWP3sceYlpZ7VMel4pzB4Y9zyOZjdz7dYEqTGsl', //API Secret
				'user_token' => '133266119-zq2Gj47fo8zbY2GUQCqAgRxvJaILXgG15nXZOCVl', //Access Token
				'user_secret' => 't4WmnpeFG85kXfs6ilhovs7TdO333QqnJikGHFzgjS0o5' //Access Token Secret
			)
		);
		
		$twitter = new tmhOAuth(array(
			'consumer_key' => $credentials[$account]['consumer_key'],
			'consumer_secret' => $credentials[$account]['consumer_secret'],
			'user_token' => $credentials[$account]['user_token'],
			'user_secret' => $credentials[$account]['user_secret'],
		));
	
		$tweetText = (strlen($tweetText) > 140) ? substr($tweetText,0,137).'...' : $tweetText;
		$twitter->request('post',$twitter->url('1.1/statuses/update'), array('status' => $tweetText));
	}

	function send_mail($data) {

        $mail             = new PHPMailer();

        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
//$mail->Host       = "greenearthappeal.org";      // sets GMAIL as the SMTP server
		$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
		$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
//$mail->Username   = "vinhnd@greenearthappeal.org";  // GMAIL username
		$mail->Username   = "partners@greenearthappeal.org";  // GMAIL username

//$mail->Password   = "hT7t3dKf-";            // GMAIL password
		$mail->Password   = "P8%k<g_39x,#1";            // GMAIL password
		$mail->CharSet = 'UTF-8';


        
        if (isset($data['from'])) {
			$mail->SetFrom($data['from'], '');
        } else {
        	$mail->SetFrom('partners@greenearthappeal.org', 'Partnerships Department – Green Earth Appeal');
        }
        
        if (isset($data['attach']) && !empty($data['attach'])) {
            if (!is_array($data['attach'])) {
                $data['attach'] = (array) $data['attach'];
            }
            foreach ($data['attach'] AS $attach) {
                $mail->AddAttachment($attach); // attachment    
            }
        }
        
        ob_start();
        if ((isset($data['signature']) && $data['signature'] == false) || (!$data['signature'])) {
            echo '<style type="text/css">*, p {font-family:Calibri!important; font-size: 11pt!important;}</style>';
            require('libraries/signature.htm');
        } else {
            require('libraries/signature_solicitor.htm');
        }

        $message = ob_get_contents();
        ob_end_clean();

        $message = $data['message'] . $message;
        
        $mail->ClearReplyTos();
        $mail->AddReplyTo("marvin@greenearthappeal.org", "Marvin Baker");
        $mail->Subject    = $data['subject'];
        $mail->MsgHTML($message);
        $mail->AddAddress($data['to']);
		
		if (isset($data['cc'])) {
        	$cc_emails = explode(',', $data['cc']);
        	foreach ($cc_emails AS $cc) {
        		$mail->AddCC($cc);
        	}
        }

        if (isset($data['bcc'])) {
        	$bcc_emails = explode(',', $data['bcc']);
        	foreach ($bcc_emails AS $bcc) {
        		$mail->AddBCC($bcc);
        	}
        }

        if ($attachment) {
            $mail->AddAttachment($attachment); // attachment    
        }
        
        if (!$mail->Send()) {
        	echo "Mailer Error: " . $mail->ErrorInfo;
        }

    }
    
      function send_mail_bk($data) {
        $this->load->library('email');
        $config = array();
        // smtp info
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://mail.greenearthappeal.org';
        $config['smtp_user'] = 'vinhnd@greenearthappeal.org';
        $config['smtp_pass'] = 'hT7t3dKf-';
        $config['smtp_port'] = '465';
        // more info
        $config['charset']  = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->set_newline("\n");
        
       /* if(isset($data['from'])){
			$this->email->from($data['from'], '');
		}
		else*/ 
		//if (isset($data['from']) && isset($data['from_name']) ) {
		if (isset($data['from']) ) {
            $this->email->from($data['from'], "");
        } else {
            $this->email->from('partners@greenearthappeal.org', 'Partnerships Department – Green Earth Appeal');
        }
        $this->email->to($data['to']);
        if (isset($data['cc'])) $this->email->cc($data['cc']);
        if (isset($data['bcc'])) $this->email->bcc($data['bcc']);
        if (isset($data['attach']) && !empty($data['attach'])) {
            if ( !is_array($data['attach'])) {
                $data['attach'] = (array) $data['attach'];
            }
            foreach ($data['attach'] AS $attach) {
                $this->email->attach($attach);
            }
        }
        ob_start();
        
        //if ( !isset($data['signature']) ) {
		if ( (isset($data['signature']) && $data['signature'] == false) || (!$data['signature']) ) {
			 echo '<style type="text/css">*, p {font-family:Calibri!important; font-size: 11pt!important;}</style>'; 
            require('libraries/signature.htm');
        } else {
			require('libraries/signature_solicitor.htm');	
		}
		
        $message = ob_get_contents();
        ob_end_clean();
        
        $message = $data['message']. $message;
        
        $this->email->subject($data['subject']);
        $this->email->message($message);
        //
        if ($this->email->send()) {
            $this->email->clear(true);
            return 1;
        }
        echo $this->email->print_debugger();
    }
    
    
    function generateCode($num)  {
		$alphas = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z'));
		$found = true;
		$max = count($alphas);
		while ($found) {
			 $code = "";
			 for ($i=0; $i<$num; $i++) {
				 $code .= $alphas[rand(0, $max-1)];
			 }
			 //Check code not used
			 $sql = "SELECT COUNT(*) FROM pct_affiliates WHERE code='{$code}' ";
			 $r = mysql_query($sql);
			 if ($r[0]==0) {
				 return $code;
			 } else {
				$this->generateCode($num); 
			 }
		}
		
	}
	function create_ticker($trees, $refid) {
		
		//Set the Content Type
		  // @header('Content-type: image/png');
	
		  // Create Image From Existing File
		  $png_image = imagecreatefrompng(dirname(__FILE__).'/../../tickers/ticker.png');
	
		  // Allocate A Bachground Color For The Text - TRANSPARENT
		  $bg_color = imagecolorallocatealpha($png_image ,22, 105, 54, 127);
	
		  // Allocate A Color For The Text - GREEN
		  $font_color = imagecolorallocate($png_image, 22, 105, 54);
	
		  // Set Path to Font File
		  $font_path = dirname(__FILE__).'/../../fonts/HelveticaLTStd-Bold.ttf';
		  
		  // Set Number Text to Be Printed On Image
		  $text = $trees;
		  $chklen =  strlen($text);
		  if($chklen == 1) {
			 $dist = '134';	  
		  } else if($chklen == 2){
			  $dist = 106;
		  } else if($chklen == 3){
			  $dist = 80;
		  } else if($chklen == 4){
			  $dist = 56;
		  } else if($chklen == 5){
			  $dist = 28;
		  }
	
		  imagesavealpha($png_image, true);
		  imagefill($png_image, 0, 0, $bg_color); 
	
		  // Print Text On Image
		  imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);
	
		  $fname = dirname(__FILE__).'/../../tickers/ticker_'.$refid.'.png';
		  
		  // Send Image to Browser
		  imagepng($png_image,$fname);
			 
		  // Clear Memory
		  imagedestroy($png_image);
	
	} 
	// func end

} // class ends 
