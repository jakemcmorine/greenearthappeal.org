<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inbound extends CI_Controller {

		/**
		 * Index Page for this controller.
		 */
			public function __construct() {
			parent::__construct();
			session_start();
			$this->data = array();
			$this->load->helper('url');
			$this->load->helper('form');
			$this->load->model('user_model');
			$segment_login = $this->uri->segment(2);
			require_once('phpmailer/class.phpmailer.php');
                
        }
		
		public function index()
		{
			$this->load->view('welcome_message');
		}
		
		/* To receive post data and insert in the database */
		public function post_partner_data_dev($post = NULL)
		{
			 $post = json_decode(file_get_contents('php://input'), true);
			 //echo "<pre>"; print_r($post);
			 if(!empty($post))
			 {  
		 
				 $record = $this->user_model->check_inbound_api_userid($post);
				 if(!empty($record))
				 {   
					 $orderid = $this->user_model->check_inbound_order_id($post);

							if(empty($orderid)){
								 $papi_data =  array_map('htmlentities',$post['api_data']);
								 //$papi_data =  array_map("utf8_encode", $post['api_data'] );
								 $current_dt = date("Y-m-d H:i:s");
								 $api_data['panacea_id'] 		= $post['api_keys']['api_user_id'];
								 $api_data['order_id'] 			= $post['api_keys']['order_id'];
								 $api_data['data_date'] 		= $current_dt;
								 $api_data['data'] 				= html_entity_decode(json_encode($papi_data));
								 
								// echo "<pre>"; print_r($api_data); die("here");
								 $insert_id2 = $this->db->insert('panacea_api_data ', $api_data);
								 
								 if($insert_id2){
									  exit("Data Inserted Successfully");
								 }else{
									  exit("Something went wrong.");
								 }
							 }else{
								 exit("Record already received.");
							 }
					 
				 }else
				 {
					 exit("User authentication failed!");
				 }
				
			 }else
			 {
				 exit("No Record Found");
			 }
			 exit();
			
		}
		public function post_partner_data($post = NULL)
		{
			 $post = json_decode(file_get_contents('php://input'), true);
			// echo "<pre>"; print_r($post); die;
			 if(!empty($post))
			 {  
		 
				 $record = $this->user_model->check_inbound_api_userid($post);
				 if(!empty($record))
				 {   
					 $orderid = $this->user_model->check_inbound_order_id($post);
							$papi_data = array();
							if(empty($orderid)){
								  foreach($post['api_data'] as $key=>$value){
									
									  $rmslsh = preg_replace('/\\\\/', '', $value);
									  $papi_data[$key] = str_replace('/','',$rmslsh);
								 } 
								 //echo "<pre>"; print_r($papi_data); die;
								 $papi_data =  array_map('htmlentities',$papi_data);
								 //$papi_data =  array_map("utf8_encode", $post['api_data'] );
								 $current_dt = date("Y-m-d H:i:s");
								 $api_data['panacea_id'] 		= $post['api_keys']['api_user_id'];
								 $api_data['order_id'] 			= $post['api_keys']['order_id'];
								 $api_data['data_date'] 		= $current_dt;
								 $api_data['data'] 				= html_entity_decode(json_encode($papi_data));
								 
								 //echo "<pre>"; print_r($api_data); die("here");
								 $insert_id2 = $this->db->insert('panacea_api_data ', $api_data);
								 
								 if($insert_id2){
									  exit("Data Inserted Successfully");
								 }else{
									  exit("Something went wrong.");
								 }
							 }else{
								 exit("Record already received.");
							 }
					 
				 }else
				 {
					 exit("User authentication failed!");
				 }
				
			 }else
			 {
				 exit("No Record Found");
			 }
			 exit();
			
		}
		 /**
         * =========================================================================================================
			function to encode utf8
         */
		function utf8_converter($array)       
		{
			array_walk_recursive($array, function(&$item, $key){
				if(!mb_detect_encoding($item, 'utf-8', true)){
						$item = utf8_encode($item);
				}
			});
		 
			return $array;
		}

}
