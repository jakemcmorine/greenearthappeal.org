<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lstreebotrunning_dev extends CI_Controller {

		/**
		 * Index Page for this controller.
		 *
		 * Maps to the following URL
		 * 		http://example.com/index.php/welcome
		 *	- or -  
		 * 		http://example.com/index.php/welcome/index
		 *	- or -
		 * Since this controller is set as the default controller in 
		 * config/routes.php, it's displayed at http://example.com/
		 *
		 * So any other public methods not prefixed with an underscore will
		 * map to /index.php/welcome/<method_name>
		 * @see http://codeigniter.com/user_guide/general/urls.html
		 */
		        public function __construct() {
                parent::__construct();
                session_start();
                $this->data = array();
                $this->load->helper('url');
                $this->load->helper('form');
				$this->load->model('user_model');
                $segment_login = $this->uri->segment(2);
                require_once('phpmailer/class.phpmailer.php');
                
        }
		public function index()
		{
			$this->load->view('welcome_message');
		}
		public function ls_tree_running_total($getyear = NULL, $getmonth = NULL)
		{
			
			$db_name = "greenapp_lightspeed_api_data";
			$db_host = "localhost";
			$db_user = "greenapp_botuser";
			$db_pass = "4{b1hb[tlW2Q";
            
			$con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
			// Check connection
			if (mysqli_connect_errno())
			  {
			  echo "Failed to connect to MySQL: " . mysqli_connect_error();
			  }

			   $sql = "select t.date, SUM(t.tree_nums) as running_tree_nums, SUM(t.noreceipts) as running_noreceipts FROM
(SELECT date, SUM(nocfd) as tree_nums, SUM(noreceipts) as noreceipts FROM greenapp_lightspeed_api_data.ls_api_data WHERE month(date) = month(CURRENT_DATE - INTERVAL 1 day) and year(date) = YEAR(CURRENT_DATE - INTERVAL 1 day)
UNION ALL
SELECT date, SUM(nocfd) as tree_nums, SUM(noreceipts) as noreceipts FROM greenapp_partner.tevalis_api_data WHERE month(date) = month(CURRENT_DATE - INTERVAL 1 day) and year(date) = YEAR(CURRENT_DATE - INTERVAL 1 day)) t";
        if ($result = mysqli_query($con, $sql)) {
            $result1 = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $result1 = $row;
            }
            mysqli_free_result($result);
        }
        
        	$sql2 = "select l.date, SUM(l.tree_nums) as last_running_tree_nums, SUM(l.noreceipts) as last_running_noreceipts FROM
(SELECT date, SUM(nocfd) as tree_nums, SUM(noreceipts) as noreceipts FROM greenapp_lightspeed_api_data.ls_api_data WHERE day(date) < day(current_date) and month(date) = month(CURRENT_DATE - INTERVAL 1 month) and year(date) = YEAR(CURRENT_DATE - INTERVAL 1 month)
UNION ALL
SELECT date, SUM(nocfd) as tree_nums, SUM(noreceipts) as noreceipts FROM greenapp_partner.tevalis_api_data WHERE day(date) < day(current_date) and month(date) = month(CURRENT_DATE - INTERVAL 1 month) and year(date) = YEAR(CURRENT_DATE - INTERVAL 1 month)) l";
        if ($result = mysqli_query($con, $sql2)) {
            $result2 = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $result2 = $row;
            }
            mysqli_free_result($result);
        }
        $result3['date'] = $result1['date'];
        $result3['running_noreceipts'] = $result1['running_noreceipts'];
        $result3['running_tree_nums'] = $result1['running_tree_nums'];
        $result3['last_running_noreceipts'] = $result2['last_running_noreceipts'];
        $result3['last_running_tree_nums'] = $result2['last_running_tree_nums'];
        // print_r($result3);
        $this->send_data_to_slack($result3);    //send data to slack
	}
		 /**
         * =========================================================================================================
		 *  section to add send_data_to_slack
         */
			function send_data_to_slack($data) {   
					$thedate = date('M Y',strtotime($data[date]));
					$lastthedate = date('M Y', strtotime('-1 months', strtotime($thedate))); 
					 $array = array( 'text' => "Tree Planting Running Total for *$thedate* vs _$lastthedate._",
								'attachments' => array(
										array('title' => $data['compname'],
											   'color' => "#f4b342",
											   'thumb_url' => "https://greenearthappeal.org/images/tree_icon.png",
											   'fields' => array(
													array('title' => "Total Bills",
														  'value' => "*" . number_format($data['running_noreceipts']) . "* vs _" . number_format($data['last_running_noreceipts']) . "_",
														  'short' => true
														 ),
													array('title' => "Carbon Free Diners",
														 'value' => "*" . number_format($data['running_tree_nums']) . "* vs _" . number_format($data['last_running_tree_nums']) . "_",
														 'short' => true
														 )
													)
											))); 
					// Create a constant to store your Slack URL
					 define('SLACK_WEBHOOK', 'https://hooks.slack.com/services/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');
					  // Make your message
					  $message = array('payload' => json_encode($array));
					  // Use curl to send your message
					  $c = curl_init(SLACK_WEBHOOK);
					  curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
					  curl_setopt($c, CURLOPT_POST, true);
					  curl_setopt($c, CURLOPT_POSTFIELDS, $message);
					  curl_exec($c);
							if (curl_errno($c)) {
									echo 'Error:' . curl_error($c);
								} 
					  curl_close($c); 
					 return;
			}
			
		  /**
         * =========================================================================================================
         * check_user
         */
        function check_user($user_type = 'admin') {
                if (!is_array($user_type)) {
                        $user_type = (array) $user_type;
                }

                $check = in_array($_SESSION['login']['type'], $user_type);
                if (!$check) {
                        echo 'You dont have permission.';
                        die();
                }
                return true;
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */