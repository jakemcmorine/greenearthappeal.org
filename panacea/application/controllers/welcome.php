<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

		/**
		 * Index Page for this controller.
		 *
		 * Maps to the following URL
		 * 		http://example.com/index.php/welcome
		 *	- or -  
		 * 		http://example.com/index.php/welcome/index
		 *	- or -
		 * Since this controller is set as the default controller in 
		 * config/routes.php, it's displayed at http://example.com/
		 *
		 * So any other public methods not prefixed with an underscore will
		 * map to /index.php/welcome/<method_name>
		 * @see http://codeigniter.com/user_guide/general/urls.html
		 */
		 
		public function index()
		{  
			$this->load->view('welcome_message');
		}
		function update_hubspot_contactid($contactid=Null,$unique_key = Null) {
			
			$this->load->database();
			$pct_imacroData['unique_key'] = $unique_key;
		    $pct_imacroData['hubspot_contact_id'] = $contactid;
			$pct_imacroData['hscontact_created_at'] = date("Y-m-d H:i:s");
		    $this->db->update_batch('pct_imacroData', array($pct_imacroData), 'unique_key');
			
		}
		function update_certificate_hubspot_contactid($contactid=Null,$certificate_key = Null) {
			
			$this->load->database();
			$pct_imacroData['certificate_key'] = $certificate_key;
		    $pct_imacroData['hubspot_contact_id'] = $contactid;
		    $pct_imacroData['hscontact_created_at'] = date("Y-m-d H:i:s");
		    $this->db->update_batch('pct_imacroData', array($pct_imacroData), 'certificate_key');
			
		}
	 
		function get_gocardless_data($activation_key='') { 
			
			$this->load->database();
			$query = "SELECT pu.id,pu.email,pu.currency,pu.dashboard_user_email,pu.date_created as user_register_date,pin.first_name,pin.last_name,pin.delay_payment_by,pin.company, pi.total,pi.id,pi.trees
			FROM pct_users AS pu
			INNER JOIN pct_initiative_partner AS pin
			ON pin.user_id=pu.id
			INNER JOIN pct_invoices AS pi
			ON pi.user_id=pu.id
			WHERE pi.gc_activation_key='$activation_key'";
					
			$row = $this->db->query($query);
			$data = $row->row();
			// echo "<pre>"; print_r($data); die;	
			echo json_encode($data);
			 
		}
		function check_partner_token_exist($random_token) {
			
			$this->load->database();
			$query = "SELECT p_hs.*,pin.company from pct_partner_hubspot_monthly_figures as p_hs INNER JOIN pct_initiative_partner AS pin ON pin.user_id=p_hs.partner_id WHERE p_hs.random_token='".$random_token."'";
			$row = $this->db->query($query);
			$data = $row->row_array();
			echo json_encode($data);
			
		}
		 /* function to update CFD hubspot figures */
		function update_cfdhubspot_monthly_figures_data() {
			
				$this->load->database();
				if(!empty($_GET)){
					 $figures =  $this->utf8_converter($_GET);
					 $query = "SELECT * from pct_partner_hubspot_monthly_figures WHERE random_token='".$random_token."' AND submitted ='1'";
					 $row = $this->db->query($query);
					 $res = $row->row_array();
					 if(!empty($res)){
						  $data = array('status'=>"updated");
						  echo json_encode($data);
						  exit;
					 }
					 
					$update_record['random_token'] 		= $figures['token'];
					$update_record['trees'] 			= $figures['trees'];
					$update_record['submitted'] 		= '1';
					$update_record['submitted_time'] 	= date('Y-m-d H:i:s');
					
					$this->db->update_batch('pct_partner_hubspot_monthly_figures', array($update_record), 'random_token');
					$data = array('status'=>"success");
					echo json_encode($data);
				} 
		}
		 /**
		 * =========================================================================================================
			function to encode utf8
		 */
		function utf8_converter($array)       
		{
			array_walk_recursive($array, function(&$item, $key){
				if(!mb_detect_encoding($item, 'utf-8', true)){
						$item = utf8_encode($item);
				}
			});
		 
			return $array;
		}

	   function get_invoices($paid="",$verify="",$id="") {
		   
            // load invoices
			$this->load->database();
			if($paid!='2' && $verify=='3'){
				
				  $query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        INNER JOIN pct_users AS u
                        ON u.id=i.user_id
                        WHERE i.state='$paid'
						AND i.deleted='0'
						AND u.id = '$id'
                        ORDER BY i.date_created DESC";
			}elseif($verify!='2' && $paid=='3'){
				
			 	 $query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        INNER JOIN pct_users AS u
                        ON u.id=i.user_id
                        WHERE i.state<>-2
						AND i.confirmed='$verify'
						AND i.deleted='0'
						AND u.id = '$id'
                        ORDER BY i.date_created DESC";
			}else{
				$query = "SELECT i.*, u.price AS price, u.email AS email, u.type AS type, u.currency AS currency
                        FROM pct_invoices AS i
                        INNER JOIN pct_users AS u
                        ON u.id=i.user_id
						WHERE i.state<>-2
						AND i.deleted='0'
						AND u.id = '$id'
                        ORDER BY i.date_created DESC";
			}
			$row = $this->db->query($query)->result();
			echo  json_encode($row);
        }
		
		function get_corporate_partner($title='') {
		 
		      $main_title=(str_replace('-',' ',$title));
			  $this->load->database();
			  $query = $this->db->query("SELECT * FROM pct_corporate_partner where opportunity_str='$main_title'");
			  $row = $query->row();
			  $row->type = "cp";
			  
			   if(empty($row)){
				$query = $this->db->query("SELECT * FROM pct_sublevel_initiative where opportunity_str='$main_title'");
				$row = $query->row();
				$row->type = "sli";
			  }
			  
			  if(empty($row)){
				  
				$query = $this->db->query("SELECT id FROM pct_toplevel_initiative where opportunity_str='$main_title'");
				$row = $query->row();
				
				 if($row->id!=""){
					 
					 $query = $this->db->query("SELECT id FROM pct_sublevel_initiative where tli_id='$row->id'");
					 $row = $query->row();
					 
					 if($row->id!=""){
						 
						$this->db->select('tli.*,sli.tree_nums as sli_trees,sli.code as referral_id');
						$this->db->from('pct_toplevel_initiative tli,pct_sublevel_initiative sli'); 
						$this->db->where('tli.id = sli.tli_id');
						$this->db->where('tli.opportunity_str',$main_title);
						$query = $this->db->get();
						//echo $this->db->last_query();
						$rows = $query->result();
						$tree_numbers_sli= array();
						$sli_trees   = array();
						 
						foreach($rows as $row){
							 
							$api_url1 ='http://giftmeatree.org/sli_tree_nums_api.php?gift_key='.$row->referral_id;    //total tree sign ups via page
							$res1 = file_get_contents($api_url1);
							if(!empty($res1)){
								$tree_numbers_sli[] = json_decode($res1);
							}
							$sli_trees[] = $row->sli_trees;
						}
						$query = $this->db->query("SELECT * FROM pct_toplevel_initiative where opportunity_str='$main_title'");
						$row = $query->row();
						$row->type = "tli";
						$row->sli_trees = array_sum($sli_trees);
						$row->referral_trees = array_sum($tree_numbers_sli);
						
					}else{
						
						 $query = $this->db->query("SELECT * FROM pct_toplevel_initiative where opportunity_str='$main_title'");
						 $row = $query->row();
						 $row->type = "tli";
						 $row->sli_trees = 0;
						 $row->referral_trees = 0;
					}
				}
				
			}
			 
			echo  json_encode($row);
			  //echo "<pre>"; print_r($row); die;
			
        }
		function get_twitter_text($referralID) {
			
			  $this->load->database();
              $query = $this->db->query("SELECT twitter,twitter_hashtag FROM pct_toplevel_initiative where code='$referralID'");
			  $row = $query->row();
			  if(empty($row)){
				$query = $this->db->query("SELECT twitter,twitter_hashtag FROM pct_sublevel_initiative where code='$referralID'");
				$row = $query->row();
			  }
			  echo  json_encode($row);
			
        }
		
		function get_all_invoices(){
			
			$this->load->database();
			$query = "SELECT * FROM pct_invoices WHERE user_id='2041'";
            $result = $this->db->query($query)->result();
			
			foreach( $result as $res){
				echo "<pre>"; print_r($res); die;
				$record['tree_nums']         = $res->trees;
				$record['company']  		 = 'Carbon Free Dining - LinkedIn';
				$record['initiative_id']     = '31';
				$record['partner_id']        = '2041';
				//$this->db->insert_batch('pct_initiative_monthly_invoice_records', array($record));
			}
			
		}
		
		function get_toplevel_initinative() {
			
			  $this->load->database();
              $query = $this->db->query("SELECT * FROM pct_toplevel_initiative");
              $row = $query->row();
			  echo  json_encode($row);
			
        }
		
		function get_imarcro_client($key, $formtype ="") {            // value 1 = imacroclient ,2 = ichanged form,3 = CFD husbpost forms
		
			$this->load->database();
			$query = $this->db->query("SELECT * FROM pct_imacroData where unique_key='$key' AND form_type='$formtype'");
			$row = $query->row();
		
			if(!empty($row)){
				
				if($row->company_number!=""){
					
				    $company_number = str_replace("\'","%",$row->company_number);
			
				    $query11 = $this->db->query("SELECT SUM(tree_nums) as total_trees FROM pct_imacroData where company_number='".$company_number."' AND partner_id='$row->partner_id' AND processed='1'");
				 
				}else{
					
				    $company = str_replace("\'","%",$row->company);
			
				    $query11 = $this->db->query("SELECT SUM(tree_nums) as total_trees FROM pct_imacroData where company LIKE '".$company."' AND partner_id='$row->partner_id' AND processed='1'");
				 
				}
				  
				$trees = $query11->row();
				$row->total_trees = $trees->total_trees;
				  
			}
			  	  //echo "<pre>"; print_r($row); die;
			echo  json_encode($row);
			
        }
		
		function get_imarcro_client_certificate_page( $key, $formtype = "" ) {            // value 1 = imacroclient ,2 = ichanged form,3 = CFD husbpost forms,4 = Sus husbpost usrs
		
			$this->load->database(); 
			
			if($formtype == 4){
				$query 	= $this->db->query("SELECT * FROM pct_imacroData where certificate_key='$key' AND (form_type='1' OR form_type='4')");
			}else{
				$query 	= $this->db->query("SELECT * FROM pct_imacroData where certificate_key='$key' AND form_type='$formtype'");
			}
            
            $row 	= $query->row();
			//echo "<pre>"; print_r($row); die;
			if(!empty($row)){
				
				if($row->company_number != ""){
					
				    $company_number = str_replace("\'","%", $row->company_number);
				    $query11 = $this->db->query("SELECT SUM(tree_nums) as total_trees FROM pct_imacroData where company_number='".$company_number."' AND partner_id='$row->partner_id' AND processed='1'");
				 
				}else{
					
				    $company = str_replace("\'","%", $row->company);
				    $query11 = $this->db->query("SELECT SUM(tree_nums) as total_trees FROM pct_imacroData where company LIKE '".$company."' AND partner_id='$row->partner_id' AND processed='1'");
				 
				}
				  
				$trees = $query11->row();
				$row->total_trees = $trees->total_trees;
				  
			}
			//echo "<pre>"; print_r($row); die;
			echo json_encode($row);
			
        }
		
		function get_certificate_imacro_clients_for_cfdsitemap($formtype) {             // formtype 1 = certificate ,2 = li pages
		
			$this->load->database();
			
			if($formtype==1){
				
				$query = $this->db->query("SELECT certificate_key,created_at FROM pct_imacroData where certificate_key!='' AND form_type='1' ORDER BY id DESC");
				
			}else{
				   $query = $this->db->query("SELECT unique_key as certificate_key,created_at FROM pct_imacroData where unique_key!='' AND certificate_key='' AND delete_key!='' AND form_type='1' ORDER BY id DESC");
			}
            $row = $query->result();
			//echo "<pre>"; print_r($row); die;	  
			echo  json_encode($row);
			
        }
		
		function delete_imarcro_client_from_database($key) {
			
			$res = array();
			$this->load->database();
			$query = $this->db->query("SELECT * FROM pct_imacroData where delete_key='$key' AND processed='1'");
			$row = $query->row();
			$query = "SELECT u.*,bp.id as bp_id
					FROM pct_initiative_partner AS bp
					INNER JOIN pct_users AS u
					ON u.id=bp.user_id
					WHERE u.email='".$row->email_address."' AND u.type='intitiative_client' AND bp.bulker_id=$row->partner_id";
			$row = $this->db->query($query);
			$data = $row->row();
			
			if(!empty($data)){
				$this->db->query("DELETE FROM pct_imacroData WHERE delete_key='$key'");
				$this->db->query("DELETE FROM pct_users WHERE id='".$data->id."'");
				$res = array('status'=>'deleted');
			}
			//echo "<pre>"; print_r($data); echo "</pre>";
			echo  json_encode($res);
			
        }
		function get_delete_imarcro_client($key) {
			
			$this->load->database();
			$query = $this->db->query("SELECT * FROM pct_imacroData where delete_key='$key' AND processed='1'");
			$row = $query->row();
			//echo "<pre>"; print_r($row); die;
			echo  json_encode($row);
			
        }		

		function get_all_sitemap_urls() {
			
			$this->load->database();
			$pct_toplevel_initiative = array();
			$pct_sublevel_initiative = array();
			$pct_corporate_partner 	 = array();

			$query = $this->db->query("SELECT * FROM pct_toplevel_initiative");
			$pct_toplevel_initiative = $query->result_array();
			$query = $this->db->query("SELECT * FROM pct_sublevel_initiative");
			$pct_sublevel_initiative = $query->result_array();
			$query = $this->db->query("SELECT * FROM pct_corporate_partner");
			$pct_corporate_partner 	 = $query->result_array();
			
			$row = array_merge($pct_toplevel_initiative, $pct_sublevel_initiative, $pct_corporate_partner);

			echo json_encode($row);
			
        }
		
		function get_sli_tli_total_trees($referralID ='') {  // get total tree count for thankyou pages based on the refferralID
		
			$this->load->database();

			if(!empty($referralID)){
				
				$query = $this->db->query("SELECT id FROM pct_toplevel_initiative where code='$referralID'");
				$row = $query->row();
				
				if($row->id!=""){
					 $query = $this->db->query("SELECT id FROM pct_sublevel_initiative where tli_id='$row->id'");
					 $row = $query->row();
	
					 if($row->id!=""){
						 $this->db->select('tli.*,sli.tree_nums as sli_trees,sli.code as referral_id');
						 $this->db->from('pct_toplevel_initiative tli,pct_sublevel_initiative sli'); 
						 $this->db->where('tli.id = sli.tli_id');
						 $query = $this->db->get();
						 //echo $this->db->last_query();
						 $rows = $query->result();
						//echo "<pre>"; print_r($rows); die;
						 $tree_numbers_sli= array();
						 $sli_trees   = array();
						 
						foreach($rows as $row){
							
							$api_url1 ='http://giftmeatree.org/sli_tree_nums_api.php?gift_key='.$row->referral_id;    //total tree sign ups via page
							$res1 = file_get_contents($api_url1);
							if(!empty($res1)){
								$tree_numbers_sli[] = json_decode($res1);
							}
							$sli_trees[] = $row->sli_trees;
						}
						$query = $this->db->query("SELECT * FROM pct_toplevel_initiative where code='$referralID'");
						$row = $query->row();
						$row->type = "tli";
						$row->sli_trees = array_sum($sli_trees);
						$row->referral_trees = array_sum($tree_numbers_sli);
						
					}else{
						 
						$query = $this->db->query("SELECT * FROM pct_toplevel_initiative where code='$referralID'");
						$row = $query->row();
						$row->type = "tli";
						$row->sli_trees = 0;
						$row->referral_trees = 0;
					}
				}
				
			}
			 
			echo  json_encode($row);
			//echo "<pre>"; print_r($row); die;
			
        }
		
		function get_initiative_partner_clients($refid){
			
			$this->load->database();
			$query = $this->db->query("SELECT user_id FROM pct_referer where code='$refid'");
			$row = $query->row();
			$data = array();
			 
			if($row->user_id!=""){
				
				$query = "SELECT pi.*,bp.*, u.id AS id, u.email AS email, u.username AS username, u.active AS active, u.tax AS tax, u.price AS price, u.currency AS currency, r.code AS code, (SELECT latest_certificate FROM pct_initiative_client_certificates WHERE client_id=$row->user_id order by id DESC limit 1) AS latest_certificate,(SELECT certificate_image FROM pct_initiative_client_certificates WHERE client_id=$row->user_id order by id DESC limit 1) AS certificate_image  ,(SELECT SUM(trees) FROM pct_invoices WHERE user_id=u.id) AS tree_nums2
						FROM pct_initiative_partner AS bp
						INNER JOIN pct_users AS u
						ON u.id=bp.user_id
						INNER JOIN pct_referer AS r
						ON r.user_id=bp.user_id 
						INNER JOIN pct_initiative AS pi
						ON pi.id=bp.initiative 
						WHERE u.id=$row->user_id";
				$row = $this->db->query($query);
				$data = $row->row_array();
				echo  json_encode($data);
				
			}
			
			
		}
		
		function get_monthwise_cfdpartner_trees($partner_id = Null){
			
			$this->load->database();
			
			if($partner_id!=""){
				
				$query = "SELECT LEFT((MONTHNAME(created_at)),3) as label,YEAR(created_at)as year, (SUM(`tree_nums`)+SUM(`free_trees`)) as y FROM `pct_initiative_partner` WHERE `bulker_id` = '$partner_id' OR user_id = '$partner_id' group by MONTH(`created_at`),YEAR(`created_at`)";	
					$row = $this->db->query($query);
					$data = $row->result_array();
					//echo "<pre>"; print_r($data); 
					$total_trees = 0;
					foreach($data as $tree){
						 $total_trees += $tree['y']; 
						
					}
					if(count($data)>12){                              //to show latest 12 records
						$old_records = count($data)-12;
						$data = array_slice($data,$old_records);
					}
					$data['total_trees'] = $total_trees; 
					
					$sus_credits = 0;

					$query = $this->db->query("SELECT SUM(suscredits) as suscredits FROM pct_partner_sustainable_credits WHERE partner_id='$partner_id'");
					$credits = $query->row_array();
					if($credits['suscredits'] > 0){
						$sus_credits = $credits['suscredits'];
					}
					
					$data['sus_credits'] = $sus_credits;
					echo $array_final =  json_encode($data, JSON_NUMERIC_CHECK); 
			}
	
		}
		
		function get_month_wise_cfdpartner_trees($partner_id = Null){          // to get data from invoice table if onupload is selected
		
			$this->load->database();
			
			if($partner_id!=""){
				
				$query = "SELECT LEFT((MONTHNAME(date_created)),3) as label,YEAR(date_created)as year, (SUM(`trees`)) as y FROM `pct_invoices` WHERE `user_id` = '$partner_id' group by MONTH(`date_created`),YEAR(`date_created`) order by year(date_created), month(date_created) ASC";	
				
				$row = $this->db->query($query);
			 	$query1 = "SELECT LEFT((MONTHNAME(created_at)),3) as label,YEAR(created_at)as year, (SUM(`free_trees`)) as y FROM `pct_initiative_partner` WHERE `user_id` = '$partner_id' group by MONTH(`created_at`),YEAR(`created_at`)";	
	
				$row1 = $this->db->query($query1);
				$data1 = $row1->row_array();
				
				$data = $row->result_array();
				
				foreach($data as &$dat){
					if($dat['label'] == $data1['label'] && $dat['year'] == $data1['year']){
						
						$dat['y'] += $data1['y'];
						
						$added = true;
					}
				}
				//$added = false;
				if(!isset($added)){
					
					$data = array_merge(array($data1),$data);	
					
				}
				$total_trees = 0;
				foreach($data as $tree){
					$total_trees += $tree['y']; 
				}
				if(count($data)>12){                              //to show latest 12 records
					$old_records = count($data)-12;
					$data = array_slice($data,$old_records);
				}
				$data['total_trees'] = $total_trees;
				
				$sus_credits = 0;

				$query = $this->db->query("SELECT SUM(suscredits) as suscredits FROM pct_partner_sustainable_credits WHERE partner_id='$partner_id'");
				$credits = $query->row_array();
				if($credits['suscredits'] > 0){
					$sus_credits = $credits['suscredits'];
				}
				
				$data['sus_credits'] = $sus_credits;
				//echo "<pre>"; print_r($data); 
				echo $array_final =  json_encode($data, JSON_NUMERIC_CHECK); 
			}
			
		}
		function get_monthwise_lightspeedpartner_trees($partner_id = Null){
			
			$this->load->database();
			
			if($partner_id != ""){
				
				$query = "SELECT LEFT((MONTHNAME(restaurant_date)),3) as label,YEAR(restaurant_date)as year, (SUM(`tree_nums`)+SUM(`free_trees`)) as y FROM `pct_initiative_monthly_invoice_records` WHERE `partner_id` = '$partner_id' group by MONTH(`restaurant_date`),YEAR(`restaurant_date`) ORDER By restaurant_date ASC";	
				$row = $this->db->query($query);
				$data = $row->result_array();

				$query1 = "SELECT LEFT((MONTHNAME(created_at)),3) as label,YEAR(created_at)as year, (SUM(`free_trees`)) as y FROM `pct_initiative_partner` WHERE `user_id` = '$partner_id' group by MONTH(`created_at`),YEAR(`created_at`)";	

				$row1 = $this->db->query($query1);              // in some case planted 100 free trees
				$data1 = $row1->row_array();
				
				/* 	echo "<pre>"; print_r($data1); 
					echo "<pre>"; print_r($data);  */
					//$added = false;
					
				foreach($data as &$dat){
					
					if($dat['label'] == $data1['label'] && $dat['year'] == $data1['year']){
							$dat['y'] += $data1['y'];
							
							$added = true;
					}
				}
				
				if(!isset($added)){
					
					$data = array_merge(array($data1),$data);	
					
				}
				$total_trees = 0;
				foreach($data as $tree){
					 $total_trees += $tree['y']; 
				}
				//echo "<pre>"; print_r($data); 
				if(count($data)>12){
					//to show latest 12 records
					$old_records = count($data)-12;
					$data = array_slice($data,$old_records);
				}
				$data['total_trees'] = $total_trees;
				$sus_credits = 0;

				$query = $this->db->query("SELECT SUM(suscredits) as suscredits FROM pct_partner_sustainable_credits WHERE partner_id='$partner_id'");
				$credits = $query->row_array();
				if($credits['suscredits'] > 0){
					$sus_credits = $credits['suscredits'];
				}
				
				$data['sus_credits'] = $sus_credits;
				
				echo $array_final =  json_encode($data, JSON_NUMERIC_CHECK); 
			}
			
			
		}
		
		function insert_partner_api_token(){
			  $this->load->database();
			  $partnertoken =  trim($_GET['token']);
			  $user_id =  trim($_GET['user_id']);
				if($partnertoken !="" && $user_id !="")
				{
				 $user['id'] = $user_id;
				 $user['partner_api_token'] = $partnertoken;
				 $this->db->update_batch('pct_users', array($user), 'id');
				}
			
		}
		function get_partner_trees_counter_data(){
			 $partner_api_token =  trim($_GET['token']);
			 $this->load->database();
			 $query = $this->db->query("SELECT id FROM pct_users where partner_api_token='$partner_api_token' AND type='intitiative'");
			 $row = $query->row();
			 //echo "<pre>"; print_r($row); 
			 $data = array();
			  if($row->id!=""){
							
				/* $query = "SELECT bp.*, u.*,r.code AS code, (SELECT latest_certificate FROM pct_invoices WHERE user_id=u.id ORDER BY id DESC LIMIT 1) AS latest_certificate,(SELECT SUM(trees) FROM pct_invoices WHERE user_id=u.id) AS tree_nums2
							FROM pct_initiative_partner AS bp
							INNER JOIN pct_users AS u
							ON u.id=bp.user_id
							INNER JOIN pct_referer AS r
							ON r.user_id=bp.user_id 
							WHERE u.id=$row->id"; */
					$row = $this->db->query("SELECT SUM(tree_nums) AS tree_nums,SUM(free_trees) AS free_trees,(SELECT Sum(tree_nums+free_trees) FROM pct_initiative_partner WHERE user_id='".$row->id."') AS tree_nums2 from pct_initiative_partner where bulker_id='".$row->id."'");
					
					//$row = $this->db->query($query);
					$data = $row->row_array();
					//echo "<pre>"; print_r($data);
					//$tree_number = $data['free_trees'] +  $data['tree_nums2']; //old
					$tree_number = $data['free_trees'] +  $data['tree_nums2'] +  $data['tree_nums'];
					$tree_number = array("number" => $tree_number);
					echo  json_encode($tree_number);
			  }
			
			
		}
		/********************************************
		Function to get Initiative Trees Counter String
		********************************************/
		
		function get_initiative_trees_counter_string(){
			 $token =  trim($_GET['token']);
			 $this->load->database();
			 $query = $this->db->query("SELECT id FROM pct_initiative where token='$token'");
			 $row = $query->row_array();
			  if($row['id']!=""){
					$initiatives_id  = $row['id'];
					$initiative_sql = "SELECT (SUM(free_trees)+ SUM(tree_nums)) As total_trees FROM pct_initiative_partner WHERE `initiative`='$initiatives_id'";    
					$ini_res = $this->db->query($initiative_sql)->row_array();
					//echo "<pre>"; print_r($ini_res); die;
					$tree_number = (int)$ini_res['total_trees'];
					$tree_number = array( "number" => $tree_number );
					echo  json_encode($tree_number);
			  }
		}
		
		function get_mfc_clients_info(){
		     $pan_id =  trim($_GET['pan_id']);
		     $email =  trim($_GET['email']);
		     $company =  trim($_GET['company']);
			 $row = array();
			 $this->load->database();
			 $query = $this->db->query('SELECT pip.*,u.*,r.code AS code,(SELECT latest_certificate FROM pct_initiative_client_certificates WHERE client_id=u.id ORDER BY id DESC LIMIT 1) AS latest_certificate FROM pct_initiative_partner as pip INNER JOIN pct_users as u ON u.id=pip.user_id INNER JOIN pct_referer AS r
							ON r.user_id=pip.user_id  where u.email="'.$email.'" AND pip.bulker_id="'.$pan_id.'" AND pip.company="'.$company.'" AND u.type="intitiative_client"'); 
			 $row = $query->row();
			 //echo "<pre>"; print_r($row);  die;
			 echo  json_encode($row);
		}
		
		function get_initiative_partner_for_cfd(){
		     $email =  trim($_GET['email']);
			 $this->load->database();
			 //enable because issue after changing the email in CFD
			 $query = $this->db->query("SELECT id FROM pct_users where email='$email' AND type='intitiative'"); 
			 $row = $query->row();
			 
			// if($email == "manchestermanager@zoukteabar.co.uk"){
				
			 if(empty($row)){
				$query = $this->db->query("SELECT id FROM pct_users where dashboard_user_email='$email' AND type='intitiative'");
				$row = $query->row();
			 }
			 
			 //echo "<pre>"; print_r($row); 
			 $data = array();
			  if($row->id!=""){
							
				$query = "SELECT bp.*, u.*,r.code AS code, (SELECT latest_certificate FROM pct_invoices WHERE user_id=u.id ORDER BY id DESC LIMIT 1) AS latest_certificate,(SELECT SUM(trees) FROM pct_invoices WHERE user_id=u.id) AS tree_nums2
							FROM pct_initiative_partner AS bp
							INNER JOIN pct_users AS u
							ON u.id=bp.user_id
							INNER JOIN pct_referer AS r
							ON r.user_id=bp.user_id 
							WHERE u.id=$row->id";
					
					$row = $this->db->query($query);
					$data = $row->row_array();
					//echo "<pre>"; print_r($data); die;	
					 echo  json_encode($data);
			  }
			
			
		}
		function get_initiative_clients_for_cfd(){
		     $email =  trim($_GET['email']);
			 $this->load->database();
			 $query = $this->db->query("SELECT id FROM pct_users where email='$email' AND type='intitiative_client' ORDER BY id DESC LIMIT 1");
			 $row = $query->row();
			 //echo "<pre>"; print_r($row); 
			 $data = array();
			  if($row->id!=""){
							
				$query = "SELECT bp.*, u.*,r.code AS code, (SELECT latest_certificate FROM pct_invoices WHERE user_id=u.id ORDER BY id DESC LIMIT 1) AS latest_certificate,(SELECT SUM(trees) FROM pct_invoices WHERE user_id=u.id) AS tree_nums2
							FROM pct_initiative_partner AS bp
							INNER JOIN pct_users AS u
							ON u.id=bp.user_id
							INNER JOIN pct_referer AS r
							ON r.user_id=bp.user_id 
							WHERE u.id=$row->id";
					
					$row = $this->db->query($query);
					$data = $row->row_array();
					//echo "<pre>"; print_r($data); die;	
					 echo  json_encode($data);
			  }
			
			
		}
		
		function get_initiative_partner_clients_for_cfd(){
		     $email =  trim($_GET['email']);
			 $this->load->database();
			 $query = $this->db->query("SELECT id FROM pct_users where email='$email'");
			 $row = $query->row();
			 $data = array();
			  if($row->id!=""){
					 $query = "SELECT pi.*,bp.*, u.id AS id, u.email AS email, u.username AS username, u.active AS active, u.tax AS tax, u.price AS price, u.currency AS currency, r.code AS code, (SELECT latest_certificate FROM pct_initiative_client_certificates WHERE client_id=$row->id order by id DESC limit 1) AS latest_certificate,(SELECT certificate_image FROM pct_initiative_client_certificates WHERE client_id=$row->id order by id DESC limit 1) AS certificate_image  ,(SELECT SUM(trees) FROM pct_invoices WHERE user_id=u.id) AS tree_nums2
							FROM pct_initiative_partner AS bp
							INNER JOIN pct_users AS u
							ON u.id=bp.user_id
							INNER JOIN pct_referer AS r
							ON r.user_id=bp.user_id 
							INNER JOIN pct_initiative AS pi
							ON pi.id=bp.initiative 
							WHERE u.id=$row->id";
					$row = $this->db->query($query);
					$data = $row->row_array();
					 echo  json_encode($data);
			  }
			
			
		}
		/**
         * =========================================================================================================
         *  functions: to Display restaurant record on GMAT using referid
         */
		function get_restuarant_record($referid){
			 $this->load->database();
			 $this->load->model('user_model');
			 $query = $this->db->query("SELECT * FROM pct_referer WHERE code='".$referid."'");
			 $res = $query->row();
			 $data = array();
			 if(!empty($res)){
				 $data = $this->user_model->get_restaurant_using_referid($res->user_id);
				 $total_trees = $this->user_model->load_restaurant_figures_trees($res->user_id);
				 $data['total_trees'] = $total_trees->total;
			 }
			 //echo "<pre>"; print_r($res); die;
			 echo  json_encode($data);
			 
		}
		
		function reminder_email_cronjob(){
			 require_once('phpmailer/class.phpmailer.php');
			 $this->load->database();
			 $this->load->model('user_model');
			// $query = $this->db->query("SELECT * FROM pct_users WHERE type='grant_user'");
			 $query = $this->db->query("SELECT u.* FROM pct_users AS u INNER JOIN pct_grant_users AS pg
							ON pg.user_id=u.id WHERE u.type='grant_user' AND pg.reminder_email='1'");
			 $rows = $query->result_array();
			 foreach($rows as $_row){
				  $message ='';
				  $message1 ='';
				  $query = $this->db->query("SELECT * FROM pct_grant_user_stage1 WHERE user_id='".$_row['id']."'");
				  $res = $query->row();
				  $days = $this->get_date_differnce($_row['date_created']);
				  $unsubscribe_link = 'https://www.greenearthappeal.org/panacea/user/unsubscribe_grant_reminder_email/'.$_row['id'];
				 if($_row['password']==""){
					  $records = $this->user_model->load_grant_email_template('1');
					  //echo "<pre>"; print_r($records); echo "</pre>";
					   if(!empty($records)){
						  if($days==$records->days){    
						   echo 'Reminder Email (1) |***| User Email: '.$_row['email'].' |***| Days: '.$days.' |***| Status: User has registered but not confirmed password.'."<br>";
						  $message = $records->message;
						  $activation_url = 'https://www.greenearthappeal.org/choose-password/?key='.$_row['activation_key'];
						  $message = str_ireplace(array('[first_name]','[username]', '[email]', '[activation_url]','[unsubscribe_link]'), array(ucfirst($_row['firstname']), $_row['username'], $_row['email'], $activation_url,$unsubscribe_link), $message);
						  
							$config['subject'] = $records->subject;		
							$config['message'] = $message;
							$config['to']      = $_row['email'];
							//$config['to']      = 'mat@greenearthappeal.org';
							$config['subject'] = str_ireplace(array('[first_name]','[username]', '[email]', '[activation_url]'), array($_row['firstname'], $_row['username'], $_row['email'], $activation_url), $config['subject'] );
							$config['email_title'] = "Partnerships Department – Green Earth Appeal";
							$this->send_mail($config);
						  }
					  }  
				 }elseif(empty($res)){
					      $records = $this->user_model->load_grant_email_template('2');
						   if(!empty($records)){
								  if($days==$records->days){    
								   echo 'Reminder Email (2) |***| User Email: '.$_row['email'].' |***| Days: '.$days.' |***| Status: Confirmed password but not submitted Stage 1.'."<br>";
								  $message = $records->message;
								  $message = str_ireplace(array('[first_name]','[username]', '[email]','[unsubscribe_link]'), array(ucfirst($_row['firstname']), $_row['username'], $_row['email'],$unsubscribe_link), $message);
								  
									$config['subject'] = $records->subject;		
									$config['message'] = $message;
									$config['to']      = $_row['email'];
									//$config['to']      = 'mat@greenearthappeal.org';
									$config['subject'] = str_ireplace(array('[first_name]','[username]', '[email]'), array($_row['firstname'], $_row['username'], $_row['email']), $config['subject'] );
									$config['email_title'] = "Partnerships Department – Green Earth Appeal";
									$this->send_mail($config);
								  }
							  } 
					} 
					 $query1 =	$this->db->query("SELECT * FROM pct_grant_user_stage1 where user_id NOT IN (select user_id from pct_grant_user_stage2) AND user_id = '".$_row['id']."' AND status='1'");		
					 $res1 = $query1->row();
					 $records = $this->user_model->load_grant_email_template('3');
					 if(!empty($res1)){
						$days = $this->get_date_differnce($res1->created_date); 
					   if(!empty($records)){
								  if($days==$records->days){    
								   echo 'Reminder Email (3) |***| User Email: '.$_row['email'].' |***| Days: '.$days.' |***| Status: User has not submitted Stage 2.'."<br>";
								  $message = $records->message;
								  $message = str_ireplace(array('[first_name]','[username]', '[email]','[unsubscribe_link]'), array(ucfirst($_row['firstname']), $_row['username'], $_row['email'],$unsubscribe_link), $message);
								  
									$config['subject'] = $records->subject;		
									$config['message'] = $message;
									$config['to']      = $_row['email'];
									//$config['to']      = 'mat@greenearthappeal.org';
									$config['subject'] = str_ireplace(array('[first_name]','[username]', '[email]'), array($_row['firstname'], $_row['username'], $_row['email']), $config['subject'] );
									$config['email_title'] = "Partnerships Department – Green Earth Appeal";
									$this->send_mail($config);
								  }
							  } 
						}
					 $query2 =	$this->db->query("SELECT * FROM pct_grant_user_stage2 where user_id NOT IN (select user_id from pct_grant_user_stage3) AND user_id = '".$_row['id']."' AND stage2_status='1'");		
					 $res2 = $query2->row();
					  $records = $this->user_model->load_grant_email_template('4');
					 if(!empty($res2)){
						 $days = $this->get_date_differnce($res2->created_date); 
					   if(!empty($records)){
								  if($days==$records->days){    
								   echo 'Reminder Email (4) |***| User Email: '.$_row['email'].' |***| Days: '.$days.' |***| Status: User has not submitted Stage 3.'."<br>";
								  $message = $records->message;
								  $message = str_ireplace(array('[first_name]','[username]', '[email]','[unsubscribe_link]'), array(ucfirst($_row['firstname']), $_row['username'], $_row['email'],$unsubscribe_link), $message);
								  
									$config['subject'] = $records->subject;		
									$config['message'] = $message;
									$config['to']      = $_row['email'];
									//$config['to']      = 'mat@greenearthappeal.org';
									$config['subject'] = str_ireplace(array('[first_name]','[username]', '[email]'), array($_row['firstname'], $_row['username'], $_row['email']), $config['subject'] );
									$config['email_title'] = "Partnerships Department – Green Earth Appeal";
									$this->send_mail($config);
								  }
							  } 
						}
			 }
			 
			//echo "Here";
		}
		 /**
         * =========================================================================================================
         *  functions: to send invoice to partner monthly
         */

		function send_monthly_invoice() {
			 require_once('phpmailer/class.phpmailer.php');
			 $this->load->database();
			 $this->load->helper('url');
             $this->load->helper('form');
			 $this->load->model('user_model');
			 $query = $this->db->query("SELECT partner_id from pct_initiative_monthly_invoice_records where invoice_sent='0' GROUP BY partner_id");								
			 $partners = $query->result_array();
			 //echo "<pre>"; print_r($partners); die("here");
			 $first_of_month = date('d-m-Y', strtotime(date('Y-m-1')));
			 $current_date = date('d-m-Y');
			// if($current_date==$first_of_month){                  //On first of month
			  if(!empty($partners)){
					 foreach($partners as $_partner){
						  $query = $this->db->query("SELECT mi.*,u.email,u.tax,u.currency,u.price from pct_users as u LEFT JOIN pct_initiative_monthly_invoice_records AS mi
								ON mi.partner_id=u.id where mi.partner_id='".$_partner['partner_id']."' AND mi.invoice_sent='0'");
						  $pct_rows = $query->result_array();
						 //echo "<pre>"; print_r($pct_rows); die("here");
						  $sql = $this->db->query("SELECT SUM(tree_nums) AS tree_nums,SUM(free_trees) AS free_trees from pct_initiative_monthly_invoice_records where partner_id='".$_partner['partner_id']."' AND invoice_sent='0'");
						  $trees = $sql->row_array();
						  
						 	//echo "<pre>"; print_r( $trees);die("here");
							 $item2 = array();
							$bulk_partner = $this->user_model->load_initiative($_partner['partner_id']);
							$email_template = $this->user_model->load_initiative_email_template($bulk_partner['initiative'],4);
							$item2['name'] = "Total number of trees";
							//echo "<pre>"; print_r($_SESSION['login']['data']); die;
							$item2['free_trees'] = $trees['free_trees'];
							$item2['unit'] = $pct_rows[0]['price'] / 100;
							$item2['tree_nums'] = $trees['tree_nums'];
							
							
							switch ($pct_rows[0]['currency']) {
									case 1:
											$item2['currency'] = '$';
											break;
									case 2:
											$item2['currency'] = '&pound;';
											break;
									case 3:
											$item2['currency'] = '€';
											break;
							}
							$opportunity_str = '';
							$currency = $item2['currency'];
							$item2['price'] = $item2['tree_nums'] * $item2['unit'];
							$tax = $pct_rows[0]['tax'];
							$sub_total = $currency . $item2['price'];
							$total_no = $item2['price'] * (100 + $tax) / 100;
							$total = $item2['currency'] . number_format($total_no, 2);
							$total_amt = $item2['currency'] . number_format($total_no, 2);
							  // store invoice in database
							$data1 = array();
							$data1['user_id'] = $_partner['partner_id'];
							$data1['date_created'] = 'NOW()';
							$data1['total'] = $total_no;
							$data1['state'] = 0;
						   // $data1['trees'] = $_SESSION['login']['data']['current_tree'];
							$data1['figure_history_key'] = $this->randomKey(10);
							$data1['trees'] = $trees['tree_nums']+$trees['free_trees'];
							//echo "<pre>"; print_r($data1); die;
							$invoice_id = $this->user_model->insert_initiative_invoice($data1);
							$invoice_number = $invoice_id;
							$invoice_company = $cert_company = $bulk_partner['company'];
							$initiative_id = $bulk_partner['initiative'];
							
							$counter_type =  $bulk_partner['counter_type'];                           // check if partner counter is selected
		
							$ini_counter_type =  $bulk_partner['ini_counter_type'];					 // check if initiative counter is selected
							
							$sql1 = $this->db->query("SELECT * from pct_initiative_partner where user_id='".$_partner['partner_id']."'");
						    $pct_ini = $sql1->row_array();
							$address = $pct_ini['address'];
							$city = $pct_ini['city'];
							$state = $pct_ini['state'];
							$post_code = $pct_ini['post_code'];
							$current_month_text = date("F_Y");
							$invoice_name = 'The_Green_Earth_Appeal_Invoice_' . $current_month_text . '_' . $invoice_id .'.pdf';
							$cert_name = 'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $invoice_id . '.pdf';
							$invoice_data = array();
							$invoice_data['id'] = $invoice_id;
							$invoice_data['number'] = $invoice_number;
							$invoice_data['name'] = $invoice_name;
							$invoice_data['latest_certificate'] = $cert_name;
							$invoice_data['month'] = date('mY');
							$this->db->update_batch('pct_invoices', array($invoice_data), 'id');
							require('libraries/monthly_cron_invoice.php');
							$sql3 = $this->db->query("SELECT SUM(tree_nums) AS tree_nums,SUM(free_trees) AS free_trees,(SELECT tree_nums FROM pct_initiative_partner WHERE user_id='".$_partner['partner_id']."') AS tree_nums2 from pct_initiative_partner where bulker_id='".$_partner['partner_id']."'");
							$get_total_trees = $sql3->row_array();
							$total_trees = $get_total_trees['tree_nums']+$get_total_trees['free_trees']+$get_total_trees['tree_nums2'];
							if($total_trees=='1'){
								 $client_trees = number_format($total_trees).' '.'tree';
							}else{
								$client_trees = number_format($total_trees).' '.'trees';
							}
							$calculated_cups = $total_trees*1000;
							$restaurant = $bulk_partner['company'];
							
							$all_counters 	= $this->get_cfd_gea_counters();		
							
							$sql2 = $this->db->query("SELECT pi.initiative_name,pi.sender_email_title,pip.initiative from pct_initiative as pi INNER JOIN pct_initiative_partner AS pip
								ON pip.initiative=pi.id where pip.user_id='".$_partner['partner_id']."'");
						    $initiatives = $sql2->row_array();
						    $initiative = $initiatives['initiative_name'];
							$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($bulk_partner['company'], $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate1']));
							$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($bulk_partner['company'], $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate2']));
							$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($bulk_partner['company'], $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate3']));
							$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($bulk_partner['company'], $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate4']));
							$certificate_sub = $certificate_msg = $certificate_text = '';
							$certificate_cc_email = preg_replace("@[\t]@", '', $bulk_partner['cc_email']);
							$certificate_sub = preg_replace("@[\t]@", '', $bulk_partner['subject']);
							$certificate_msg = preg_replace("@[\t]@", '', $bulk_partner['message']);
							$certificate_text = preg_replace("@[\t]@", '', $bulk_partner['certificate_text']);
							$certificate_text = stripslashes($certificate_text);
						    $certificate_text = str_ireplace(array('[name]', '[total_trees]', '[initiative]', '[opportunity_str]'), array($bulk_partner['company'], $total_trees, $initiative, $opportunity_str), $certificate_text);
							//echo "here";
							require('libraries/cert-ini.php');
							 // create the ticker
							$refid = $this->user_model->get_refid($_partner['partner_id']);
							$ticker_url = base_url() . 'tickers/ticker_' . $refid->code . '.png';
							//$this->create_ticker($total_trees, $refid->code);
							
							/* if($initiative_id =='34' || $initiative_id =='35' || $initiative_id =='25' || $initiative_id =='31' ){
								$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
							}else{
								 $this->create_ticker($total_trees, $refid->code);            //GEA tickers
							} */
							
							if($counter_type != 0){															// if counter is set on partner
									if($counter_type==1){
										 $this->create_ticker($total_trees, $refid->code);            //GEA tickers
									}elseif($counter_type==2){
										$this->create_cfd_ticker($total_trees, $refid->code);          //CFD tickers
									}elseif($counter_type==3){
										$this->create_non_partner_ticker($total_trees, $refid->code);       //Non partner tickers
									}
									
								}else{                                                                              // if counter is set on initiative
									if($ini_counter_type==1){
									    $this->create_ticker($total_trees, $refid->code);             //GEA tickers
									}elseif($ini_counter_type==2){
										$this->create_cfd_ticker($total_trees, $refid->code);          //CFD tickers
									}elseif($ini_counter_type==3){
										$this->create_non_partner_ticker($total_trees, $refid->code);     //Non partner tickers
									}
								}
							$this->create_ticker($all_counters['gea_counter'], 'gea_counter');           //create GEA ticker for all partners
							
							$this->create_cfd_ticker($all_counters['cfd_counter'], 'cfd_counter');		 //create CFD ticker for all partners	
							
							$this->create_cfd_small_ticker($total_trees, $refid->code, 'black');
							
							$this->create_cfd_small_ticker($total_trees, $refid->code, 'green');
							
							$this->create_cfd_small_ticker($total_trees, $refid->code, 'white');
										
							//to create pdf images for initiatives
							$cert_thumb_url = base_url() . 'cert/thumb_' . $refid->code . '.jpg';
							$cert_image_url = base_url() . 'cert/full_' . $refid->code . '.jpg';
							if (extension_loaded('imagick')){   		//extension imagick is installed
								$cert_path = dirname(__FILE__) . '/../../uploads/certs/';
								$pdf_path = $cert_path.$cert_name;
								$image_url = dirname(__FILE__) . '/../../cert/';
								$this->create_cert_pdf_to_image($pdf_path, $refid->code, $image_url); 
							}	
							$unique_url = $bulk_partner['website_url'].'/?refid=' . $refid->code;
						
							$config['attach'] = array(dirname(__FILE__) . '/../../uploads/invoices/' . $invoice_name, dirname(__FILE__) . '/../../uploads/certs/' . $cert_name);
							
							$certificate_url = 'https://www.greenearthappeal.org/panacea/uploads/certs/'.$cert_name;
							
							$initiative_id	= $initiatives['initiative'];
							$email_template = $this->user_model->load_initiative_email_template($initiative_id,4);
							$email_title = $initiatives['sender_email_title'];
							$subject = $email_template->subject;
							$config['subject']  = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[first_name]','[initiative_name]','[calculated_cups]'), array($pct_ini['first_name'],$bulk_partner['company'], $total_trees,$total_trees, $pct_ini['first_name'],$initiatives['initiative_name'],$calculated_cups), $subject);
							$message = $email_template->message; 
							$config['message'] = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[first_name]','[initiative_name]','[calculated_cups]','[ticker_url]', '[unique_url]','[cert_thumb_url]','[cert_image_url]',['certificate_url'],'[cfd_counter]','[gea_counter]'), array($pct_ini['first_name'],$bulk_partner['company'], $total_trees,$total_trees, $pct_ini['first_name'],$initiatives['initiative_name'],$calculated_cups,$ticker_url,$unique_url,$cert_thumb_url,$cert_image_url,$certificate_url,$all_counters['cfd_counter'],$all_counters['gea_counter']), $message);
							if($email_title==""){
								  $config['email_title'] = "Cups That Care";
							  }else{
								  $config['email_title'] = $email_title;
							  }
							  $invoice['invoice_sent'] = '1';
							  $invoice['partner_id'] = $_partner['partner_id'];
							  $this->db->update_batch('pct_initiative_monthly_invoice_records', array($invoice), 'partner_id');
							  //$config['to'] = 'testing.whizkraft1@gmail.com';
							  $config['to'] = 'mat@greenearthappeal.org';
						      $config['cc'] = "marvin@greenearthappeal.org";  
						//  $config['to'] = $bulk_partner['email'];
							$this->send_mail($config);  
							echo "emails sent!";
					}
			  }else{
				  echo "No records found yet!";
			  }
			// } //end of first of month 
			
		}
		/**
         * =========================================================================================================
         *  functions: to send partner monthly reminder email
         */
		function partner_monthly_reminder_email() {
			die("Disabled");
			 require_once('phpmailer/class.phpmailer.php');
			 $this->load->database();
			 $this->load->helper('url');
             $this->load->helper('form');
			 $this->load->model('user_model');
			 $query = $this->db->query("SELECT pu.email,pi.*,pit.initiative_name,pit.sender_email_title from pct_initiative_partner as pi INNER JOIN pct_users AS pu ON pu.id = pi.user_id INNER JOIN pct_initiative AS pit
							ON pit.id=pi.initiative where pi.monthly_reminder_email='1'");	
		 
			 $partners = $query->result_array();
			  //echo "<pre>"; print_r($partners);	die;
			 if(!empty($partners)){
					 foreach($partners as $_partner){
						    $config = array();
							$email_template = $this->user_model->get_enabled_initiative_email_template($_partner['initiative'],6);
							// echo "<pre>"; print_r($email_template);
							$email_title = $_partner['sender_email_title'];
							$subject = $email_template->subject;
							$config['subject']  = str_ireplace(array('[contact_person]', '[company_name]','[initiative_name]'), array($_partner['first_name'],$_partner['company'],$_partner['initiative_name']), $subject);
							
							$message = $email_template->message; 
							$config['message']  = str_ireplace(array('[contact_person]', '[company_name]','[initiative_name]'), array($_partner['first_name'],$_partner['company'],$_partner['initiative_name']), $message);

							//$config['to'] = 'testing.whizkraft1@gmail.com';
						    $config['to'] = $_partner['email'];
							//$this->send_mail($config); 
							echo 'Reminder email sent to <b>'.$config['to'].'</b><br>';							
					 }
			 }else{
				 echo "No record found!";
			 }
			 
		}
		  /**
         * =========================================================================================================
         *  functions: to send unique link to partner using hubspot
         */
		
		function send_monthly_partner_link_hubspot_cronjob($panid = Null) {

			 require_once('phpmailer/class.phpmailer.php');
			 $this->load->database();
			 $this->load->model('user_model');
			 if($panid !=""){
				  $query = $this->db->query("SELECT pu.email,pi.*,pit.initiative_name,pit.sender_email_title from pct_initiative_partner as pi INNER JOIN pct_users AS pu ON pu.id = pi.user_id INNER JOIN pct_initiative AS pit
								ON pit.id=pi.initiative where pi.monthly_reminder_email='1' AND pu.id = '$panid'");	
			 }else{
				 $query = $this->db->query("SELECT pu.email,pi.*,pit.initiative_name,pit.sender_email_title from pct_initiative_partner as pi INNER JOIN pct_users AS pu ON pu.id = pi.user_id INNER JOIN pct_initiative AS pit
								ON pit.id=pi.initiative where pi.monthly_reminder_email='1' and pu.active='1'");	
			 }
			 $partners = $query->result_array();
			  //echo "<pre>"; print_r($partners);	die;
			 
			 /*  Hubspot API key  */
			// $hapikey = "7aed04ae-daa3-4a6f-a17f-452346b7eb0c";         //developer
			  $hapikey = "71c60211-d5ee-435f-9a0b-c4ee06e1e716";        //live
			  $i = 0;
			  if(!empty($partners)){
					 foreach($partners as $_partner){
						 
							$partner_token = $this->randomKey(10);         //to create random token for partner
							$partner_unique_url = "https://carbonfreedining.org/treeupload/".$partner_token;
							if(isset($_partner['hs_company_id']) && $_partner['hs_company_id'] !=""){
								$company_id = $_partner['hs_company_id'];
								echo  "Update partner URL: ".$partner_unique_url."<br>";
								//echo 'company_id: '.$company_id.' Partner_id: '.$comp_results->properties->panacea_id->value.'<br>'; 
						
									$update_comp = array(
												'properties' => array(
												array(
													'name' => 'panacea_upload_submitted',
													'value' => 'No'            
												),
												array(
													'name' => 'panacea_upload_link',
													'value' => $partner_unique_url            
												)
												));	
									$contact_url = 'https://api.hubapi.com/companies/v2/companies/'.$company_id.'/'.'?hapikey='.$hapikey;
									$compch = @curl_init($contact_url);
									$update_comp_post_json = json_encode($update_comp);
									curl_setopt($compch, CURLOPT_URL, $contact_url);
									curl_setopt($compch, CURLOPT_CUSTOMREQUEST, "PUT");
									@curl_setopt($compch, CURLOPT_POSTFIELDS, $update_comp_post_json);
									curl_setopt($compch, CURLOPT_RETURNTRANSFER, 1);
									@curl_setopt($compch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
									$compresponse = @curl_exec($compch);
									$status_code3 = @curl_getinfo($compch, CURLINFO_HTTP_CODE);
									$curl_errors3 = curl_error($compch);
									@curl_close($compch);
									
									 $record['partner_id']		 =  $_partner['user_id'];
									 $record['random_token']	 =  $partner_token;
									 $record['hs_company_id'] 	 =  $company_id;
									 $record['submitted'] =  "0"; 
									 $record['month'] =  date('Y-m-d');   
									 $this->db->insert_batch('pct_partner_hubspot_monthly_figures', array($record));           //to insert record on panacea
								

							}
							//$i++;
					 }       //end partner for loop
			  }	
		}
		
		/**
         * =========================================================================================================
         *  functions: to send fund request to partner monthly dev
         */

		function goCardless_monthly_partner_fund_request($panid = Null) {
			
			require_once('phpmailer/class.phpmailer.php');
			require_once('libraries/gocardless/vendor/autoload.php');
			
			$this->load->helper('url');
			$this->load->helper('form');
			$this->load->database();
			$this->load->model('user_model');

		
			/* // create connection with CFD */
			$db_name = "carbonfd_cfddb";
			$db_host = "localhost";
			$db_user = "carbonfd_cfddbus";
			$db_pass = "vVZKUCXr@wAU";
			
			$con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
			// Check connection
			if (mysqli_connect_errno()){
			  echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
			
			 if($panid!=""){
				 $query = $this->db->query("SELECT partner_id from pct_initiative_monthly_invoice_records where invoice_sent='0' AND partner_id ='".$panid."' AND request_for_funds='1' GROUP BY partner_id");
			 }else{
			    //$query = $this->db->query("SELECT partner_id from pct_initiative_monthly_invoice_records where invoice_sent='0' AND request_for_funds='1' GROUP BY partner_id");
				exit("Please pass the partner ID");
			 }	
			 $partners = $query->result_array();
			 
			 $first_of_month = date('d-m-Y', strtotime(date('Y-m-1')));
			 $current_date = date('d-m-Y');
			 
			 if(!empty($partners)){ 
					 foreach($partners as $_partner){
						  
						  $query = $this->db->query("SELECT mi.*,u.email,u.tax,u.currency,u.price,u.dashboard_user_email from pct_users as u LEFT JOIN pct_initiative_monthly_invoice_records AS mi
								ON mi.partner_id=u.id where mi.partner_id='".$_partner['partner_id']."' AND mi.invoice_sent='0' AND mi.request_for_funds='1' order by id ASC");
						  $pct_rows = $query->result_array();
						   //echo "<pre>"; print_r($pct_rows); die("here");
						  $sql = $this->db->query("SELECT SUM(tree_nums) AS tree_nums,SUM(free_trees) AS free_trees from pct_initiative_monthly_invoice_records where partner_id='".$_partner['partner_id']."' AND invoice_sent='0' AND request_for_funds='1'");
						  $trees = $sql->row_array();
							$item2 = array();
							$bulk_partner = $this->user_model->load_initiative($_partner['partner_id']);
							$email_template = $this->user_model->load_initiative_email_template($bulk_partner['initiative'],4);
							$item2['name'] = "Total number of trees";
							//echo "<pre>"; print_r($_SESSION['login']['data']); die;
							$item2['free_trees'] = $trees['free_trees'];
							$item2['unit'] = $pct_rows[0]['price'] / 100;
							$item2['tree_nums'] = $trees['tree_nums'];
							
							
							switch ($pct_rows[0]['currency']) {
									case 1:
											$item2['currency'] = '$';
											break;
									case 2:
											$item2['currency'] = '&pound;';
											break;
									case 3:
											$item2['currency'] = '€';
											break;

							}
							$opportunity_str = '';
							$currency = $item2['currency'];
							$item2['price'] = $item2['tree_nums'] * $item2['unit'];
							$tax = $pct_rows[0]['tax'];
							$sub_total = $currency . $item2['price'];
							//$total_no = $item2['price'] * (100 + $tax) / 100;
							 $total_no = $item2['price'];
							$total = $item2['currency'] . number_format($total_no, 2);
							$total_amt = $item2['currency'] . number_format($total_no, 2);
							  // store invoice in database
							$data1 = array();
							$data1['user_id'] = $_partner['partner_id'];
							$data1['date_created'] = 'NOW()';
							$data1['total'] = $total_no;
							$data1['state'] = 0;
						   // $data1['trees'] = $_SESSION['login']['data']['current_tree'];
							$data1['figure_history_key'] = $this->randomKey(10);
							$data1['trees'] = $trees['tree_nums']+$trees['free_trees'];
							$total_planted_trees = $trees['tree_nums']+$trees['free_trees'];
							//echo "<pre>"; print_r($data1); die;
							
							$dashboard_user_email = $pct_rows[0]['dashboard_user_email'];
							if($dashboard_user_email != ""){
								$user_email = $dashboard_user_email;
							}else{
								$user_email = $pct_rows[0]['email'];
							}
							/* to check the panacea minimum monthly commitment start */
							 if( $bulk_partner['hs_company_id'] != 0 && $bulk_partner['hs_company_id'] !="" ){
							  
								 $hscompany  =  $this->cfd_get_hubspot_company_properties($bulk_partner['hs_company_id']);
								 $hscompany  =  json_decode($hscompany, true);
								 //echo "<pre>"; print_r($hscompany);
								 if(isset($hscompany['portalId']) && isset($hscompany['properties']['panacea_minimum_monthly_commitment']['value'])){
									 
									$monthly_commitment  =  $hscompany['properties']['panacea_minimum_monthly_commitment']['value'];
									
									if($item2['tree_nums'] < $monthly_commitment){
										
											$total_no   =  $monthly_commitment * $item2['unit'];
											$total  	=  $item2['currency'] . number_format($total_no, 2);
											$shortfall_tree  = $monthly_commitment - $item2['tree_nums'];
											$next_index =   count($pct_rows);
											$pct_rows[$next_index]['company']     		=  "Lightspeed Subscription Donation";
											$pct_rows[$next_index]['tree_nums']   		=  $shortfall_tree;
											$pct_rows[$next_index]['restaurant_date']   =  "";
											$pct_rows[$next_index]['price']  	  		=  $pct_rows[0]['price'];
											
											// to update in pct_invoices table
											$data1['total'] = $total_no;
											$data1['trees'] = $trees['tree_nums'] + $trees['free_trees'] + $shortfall_tree;
											
											//to update shortfall trees under the partner
											$partner_sql = $this->db->query("SELECT * from pct_initiative_partner where user_id='".$_partner['partner_id']."'");
											$partner_res = $partner_sql->row_array();
											$total_partner_trees  =  $partner_res['tree_nums'] + $shortfall_tree;
											
											$qru = "UPDATE pct_initiative_partner SET tree_nums = '".$total_partner_trees."' WHERE id = '".$partner_res['id']."' ";
											$this->db->query($qru);
											
									}
								 }
							  }
							  
							/* to check the panacea minimum monthly commitment end  */
							
							$invoice_id = $this->user_model->insert_initiative_invoice($data1);
							
							$cfwp_sql = "SELECT * from `cfwp_users` AS u INNER JOIN cfwp_ls_partners_details AS pd ON u.ID=pd.user_id where u.user_email='".$user_email."' AND pd.cancelled='0'";
							$cfwp_result = mysqli_query($con,$cfwp_sql);
							
							$mendate_id = "";
							if(mysqli_num_rows($cfwp_result) > 0) {   

								$cfd_row = mysqli_fetch_assoc($cfwp_result);
								$cfwp_user_id 	= $cfd_row['ID'];
								$pan_user_id 	= ""; 
								$mendate_id 	= $cfd_row['mendate_id'];
								$customer_id 	= $cfd_row['customer_id'];

							}else{
								
								$pan_user_id 	= $bulk_partner['pan_user_id']; 
								$partner_sql = "SELECT * from `cfwp_ls_partners_details` where panacea_id='".$pan_user_id."' AND cancelled='0'";
								
								$partner_result = mysqli_query($con,$partner_sql);
								if(mysqli_num_rows($partner_result) > 0) { 
								
									$cfdp_row = mysqli_fetch_assoc($partner_result);
									
									$cfwp_user_id 	= ""; 
									$mendate_id 	= $cfdp_row['mendate_id'];
									$customer_id 	= $cfdp_row['customer_id'];
								}
								
							}
							
							$gocardless_link = false;
							//if(mysqli_num_rows($cfwp_result) > 0) {                  //change this sign after testing to >
							
							if($mendate_id != "") { 
									 
									/*Call goCardless API to create payment using mendate ID*/	
									switch ($pct_rows[0]['currency']) {
										    case 1:
													$payment_currency = 'GBP';             //usd But it does not exist
													break;
											case 2:
													$payment_currency = 'GBP';
													break;
											case 3:
													$payment_currency = 'EUR';
													break;
									}
									
									//$token = "sandbox_ZH42k6x0Ee7-owAISf_-HLlRsbeWQBc07NwUNN2Z";  //sandbox token
									 
									$new_gocardless_enable_date  = strtotime("2020-01-20 11:00:00");
									$user_register_date  		 = strtotime($bulk_partner['user_register_date']);
									
									if($user_register_date > $new_gocardless_enable_date){
										$token = "live_icu2SM1QdA7w2GTGJF1mKV58dU00O0VM58y-XVAb";      //New user
										$_SESSION['gocardless_user'] = "new";
									}else{
										$token = "live_roMMEqbGeKFuN_sHAyfqYGLOFsMvmEDpVeP5Ltec";      //Old user
										$_SESSION['gocardless_user'] = "old";
									}
									
									$client = new \GoCardlessPro\Client([
										'access_token' => $token,
										'environment' => \GoCardlessPro\Environment::LIVE  			//LIVE/SANDBOX
									]); 
									
									try {
											if(isset($bulk_partner['delay_payment_by']) && $bulk_partner['delay_payment_by'] > 0){
												$delay_payment_by =	$bulk_partner['delay_payment_by'];
												$today_date = date('Y-m-d');
												$charge_date = date('Y-m-d', strtotime($today_date. '+ '.$delay_payment_by.' days'));	
												
												 $payment= $client->payments()->create([
													  "params" => ["amount" => $total_no*100,
																   "currency" => $payment_currency,
																    "charge_date" => $charge_date,
																   "links" => [
																	 "mandate" =>$mendate_id
																   ]]
													]);

											}else{
												
												$payment= $client->payments()->create([
												  "params" => ["amount" => $total_no*100,
															   "currency" => $payment_currency,
															   "links" => [
																 "mandate" =>$mendate_id
															   ]]
												]);
											}
										
											$gocardless_link = false;
											
											if(!empty($payment)){
												
												$insert_cfwp_sql = "INSERT INTO cfwp_ls_partners_payments SET user_id = '{$cfwp_user_id}',panacea_id = '{$pan_user_id}',customer_id = '{$customer_id}',amount = '{$total_no}',trees = '{$total_planted_trees}',payment_id = '{$payment->id}',payment_status = 'Pending',invoice_id = {$invoice_id}";
												mysqli_query($con,$insert_cfwp_sql); 
												 
												 $payment_status['id'] = $invoice_id;
												 $payment_status['state'] = '-3';
												 $payment_status['payment_type'] = '1';
												 $this->db->update_batch('pct_invoices', array($payment_status), 'id');
											}
									}catch (\GoCardlessPro\Core\Exception\ApiException $e) {
											 echo  $error = $e->getMessage();
											  die("Error");
											} catch (\GoCardlessPro\Core\Exception\MalformedResponseException $e) {
											 echo   $error = $e->getMessage();
											  die("Error");
											} catch (\GoCardlessPro\Core\Exception\ApiConnectionException $e) {
											 echo  $error = $e->getMessage();
											  die("Error");
									}
									
							}else{
									$activation_key = $this->randomKey(25);
									$pcinvoice['gc_activation_key'] = $activation_key;
								    $pcinvoice['id'] = $invoice_id;
								    $this->db->update_batch('pct_invoices', array($pcinvoice), 'id');
										 
									$gocardless_link = "https://carbonfreedining.org/pay-by-gocardless/?activation_key=$activation_key";
								
							}
							
						     //echo "<pre>"; print_r( $pct_rows); die("here"); 
							$invoice_number = $invoice_id;
							$invoice_company = $cert_company = $bulk_partner['company'];
							$initiative_id = $bulk_partner['initiative'];
							
							$counter_type =  $bulk_partner['counter_type'];                           // check if partner counter is selected
		
							$ini_counter_type =  $bulk_partner['ini_counter_type'];					 // check if initiative counter is selected
							
							$sql1 = $this->db->query("SELECT * from pct_initiative_partner where user_id='".$_partner['partner_id']."'");
						    $pct_ini = $sql1->row_array();
							$address = $pct_ini['address'];
							$city = $pct_ini['city'];
							$state = $pct_ini['state'];
							$post_code = $pct_ini['post_code'];
							$ini_first_name = $pct_ini['first_name'];
							$current_month_text = date("F_Y");
							$invoice_name = 'The_Green_Earth_Appeal_Request_For_Funds_' . $current_month_text . '_' . $invoice_id .'.pdf';
							$cert_name = 'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $invoice_id . '.pdf';
							$invoice_data = array();
							$invoice_data['id'] = $invoice_id;
							$invoice_data['number'] = $invoice_number;
							$invoice_data['name'] = $invoice_name;
							$invoice_data['latest_certificate'] = $cert_name;
							$invoice_data['month'] = date('mY');
							$this->db->update_batch('pct_invoices', array($invoice_data), 'id');
							
							$bk_image 	=  $bulk_partner['bk_image'];

							$partner_bk_image =  $bulk_partner['partner_bk_image'];

							if($partner_bk_image != ""){                                   			//check partner background cert image first
								$bk_image 	=  $bulk_partner['partner_bk_image'];
							}else{
								$bk_image 	=  $bulk_partner['bk_image'];
							}
							require('libraries/monthly_funds_request.php');
							/* echo "https://www.greenearthappeal.org/panacea/uploads/request_for_funds/".$invoice_name;
							echo '<br>';
							die("Request fund created"); */
							$sql3 = $this->db->query("SELECT SUM(tree_nums) AS tree_nums,SUM(free_trees) AS free_trees,(SELECT Sum(tree_nums+free_trees) FROM pct_initiative_partner WHERE user_id='".$_partner['partner_id']."') AS tree_nums2 from pct_initiative_partner where bulker_id='".$_partner['partner_id']."'");
							$get_total_trees = $sql3->row_array();
							$total_trees = $get_total_trees['tree_nums'] + $get_total_trees['free_trees'] + $get_total_trees['tree_nums2'];
							if($total_trees=='1'){
								 $client_trees = number_format($total_trees).' '.'tree';
							}else{
								$client_trees = number_format($total_trees).' '.'trees';
							}
							$calculated_cups = $total_trees*1000;
							$restaurant = $bulk_partner['company'];
							$sql2 = $this->db->query("SELECT pi.initiative_name,pi.sender_email_title,pip.initiative from pct_initiative as pi INNER JOIN pct_initiative_partner AS pip
								ON pip.initiative=pi.id where pip.user_id='".$_partner['partner_id']."'");
						    $initiatives = $sql2->row_array();
						    $initiative = $initiatives['initiative_name'];
							
							$all_counters 	= $this->get_cfd_gea_counters();
							
							$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($bulk_partner['company'], $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate1']));
							$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($bulk_partner['company'], $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate2']));
							$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($bulk_partner['company'], $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate3']));
							$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($bulk_partner['company'], $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate4']));
							$certificate_sub = $certificate_msg = $certificate_text = '';
							$certificate_cc_email = preg_replace("@[\t]@", '', $bulk_partner['cc_email']);
							$certificate_sub = preg_replace("@[\t]@", '', $bulk_partner['subject']);
							$certificate_msg = preg_replace("@[\t]@", '', $bulk_partner['message']);
							$certificate_text = preg_replace("@[\t]@", '', $bulk_partner['certificate_text']);
							$certificate_text = stripslashes($certificate_text);
						    $certificate_text = str_ireplace(array('[name]', '[total_trees]', '[initiative]', '[opportunity_str]'), array($bulk_partner['company'], $total_trees, $initiative, $opportunity_str), $certificate_text);
							//echo "here";
							require('libraries/cert-ini.php');
							 // create the ticker
							$refid = $this->user_model->get_refid($_partner['partner_id']);
							$ticker_url = base_url() . 'tickers/ticker_' . $refid->code . '.png';
							//$this->create_ticker($total_trees, $refid->code);
							
							/* if($initiative_id =='34' || $initiative_id =='35' || $initiative_id =='25' || $initiative_id =='31' ){
								$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
							}else{
								 $this->create_ticker($total_trees, $refid->code);            //GEA tickers
							} */
							
							if($counter_type != 0){															// if counter is set on partner
									if($counter_type==1){
										 $this->create_ticker($total_trees, $refid->code);           //GEA tickers
									}elseif($counter_type==2){
										$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
									}elseif($counter_type==3){
										$this->create_non_partner_ticker($total_trees, $refid->code);        //Non partner tickers
									}
									
								}else{                                                                              // if counter is set on initiative
									if($ini_counter_type==1){
										$this->create_ticker($total_trees, $refid->code);                //GEA tickers
									}elseif($ini_counter_type==2){
										$this->create_cfd_ticker($total_trees, $refid->code);      //CFD tickers
									}elseif($ini_counter_type==3){
										$this->create_non_partner_ticker($total_trees, $refid->code);        //Non partner tickers 
									}
								}
								
							$this->create_ticker($all_counters['gea_counter'], 'gea_counter');           //create GEA ticker for all partners
							
							$this->create_cfd_ticker($all_counters['cfd_counter'], 'cfd_counter');		 //create CFD ticker for all partners
							
							$this->create_cfd_small_ticker($total_trees, $refid->code, 'black');
							
							$this->create_cfd_small_ticker($total_trees, $refid->code, 'green');
							
							$this->create_cfd_small_ticker($total_trees, $refid->code, 'white');
							
							//to create pdf images for initiatives
							$cert_thumb_url = base_url() . 'cert/thumb_' . $refid->code . '.jpg';
							$cert_image_url = base_url() . 'cert/full_' . $refid->code . '.jpg';
							if (extension_loaded('imagick')){   		//extension imagick is installed
								$cert_path = dirname(__FILE__) . '/../../uploads/certs/';
								$pdf_path = $cert_path.$cert_name;
								$image_url = dirname(__FILE__) . '/../../cert/';
								$this->create_cert_pdf_to_image($pdf_path, $refid->code, $image_url); 
							}	
							$unique_url = $bulk_partner['website_url'].'/?refid=' . $refid->code;
					    	$certificate_url =  "";
							$config['attach'] = array(dirname(__FILE__) . '/../../uploads/request_for_funds/' . $invoice_name, dirname(__FILE__) . '/../../uploads/certs/' . $cert_name);
							$initiative_id	= $initiatives['initiative'];
							$email_template = $this->user_model->load_initiative_email_template($initiative_id,4);
							$email_title = $initiatives['sender_email_title'];
							$subject = $email_template->subject;
							$config['subject']  = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[first_name]','[initiative_name]','[calculated_cups]'), array($pct_ini['first_name'],$bulk_partner['company'], $total_trees,$total_trees, $pct_ini['first_name'],$initiatives['initiative_name'],$calculated_cups), $subject);
							$message = $email_template->message; 
							$config['message'] = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[first_name]','[initiative_name]','[calculated_cups]','[ticker_url]', '[unique_url]','[cert_thumb_url]','[cert_image_url]','[certificate_url]','[cfd_counter]','[gea_counter]'), array($pct_ini['first_name'],$bulk_partner['company'], $total_trees,$total_trees, $pct_ini['first_name'],$initiatives['initiative_name'],$calculated_cups,$ticker_url,$unique_url,$cert_thumb_url,$cert_image_url,$certificate_url,$all_counters['cfd_counter'],$all_counters['gea_counter']), $message);
							if($email_title==""){
								  $config['email_title'] = "Carbon Free Dining";
							  }else{
								  $config['email_title'] = $email_title;
							  }
							  $invoice['invoice_sent'] = '1';
							  $invoice['request_for_funds'] = '0';
							  $invoice['partner_id'] = $_partner['partner_id'];
							  if($_partner['partner_id']!='3733'){
								$this->db->update_batch('pct_initiative_monthly_invoice_records', array($invoice), 'partner_id');
							  }
							  //$config['to'] = 'testing.whizkraft1@gmail.com';
							   //$config['to'] = 'mat@greenearthappeal.org';
						      // $config['cc'] = "marvin@greenearthappeal.org";  
							  
						      $config['bcc'] = "billing@greenearthappeal.org";  
						      $config['to']  = $bulk_partner['email'];
							  
							 
							$this->send_mail($config);  
							echo "Email sent";
					}
			  }else{
				  echo "No records found yet!";
			  }
						
		}
		
		 /**
         * =========================================================================================================
         *  functions: to fetch the info on the hubspot companies 
         */
		function cfd_get_hubspot_company_properties($hscompany_id){
			
				$hapikey = "71c60211-d5ee-435f-9a0b-c4ee06e1e716";    //live hubspot API key 
				// to get contact ID of the existing user using email
				$check_email = 'https://api.hubapi.com/companies/v2/companies/'.$hscompany_id.'?hapikey=' . $hapikey;
				$ch_email = curl_init($check_email);
				curl_setopt($ch_email, CURLOPT_TIMEOUT, 5);
				curl_setopt($ch_email, CURLOPT_CONNECTTIMEOUT, 5);
				curl_setopt($ch_email, CURLOPT_CUSTOMREQUEST, "GET"); 
				curl_setopt($ch_email, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($ch_email);
				$status_code = @curl_getinfo($ch_email, CURLINFO_HTTP_CODE);
				$curl_errors = curl_error($ch_email);
				curl_close($ch_email);
				 $curl_errors = "curl Errors Get contact ID: " . $curl_errors.' Status Code: '.$status_code;
				return $response;
					 
		}
	     /**
         * =========================================================================================================
         *  functions: to send fund request to partner monthly
         */

		function send_monthly_funds_request($panid = Null) {
			
			 require_once('phpmailer/class.phpmailer.php');
			 require_once('libraries/gocardless/vendor/autoload.php');
						
			 $this->load->database();
			 $this->load->helper('url');
             $this->load->helper('form');
			 $this->load->model('user_model');
			 
			/* // create connection with CFD */
			$db_name = "carbonfd_cfddb";
			$db_host = "localhost";
			$db_user = "carbonfd_cfddbus";
			$db_pass = "vVZKUCXr@wAU";
			
			$con = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
			// Check connection
			if (mysqli_connect_errno())
			  {
			  echo "Failed to connect to MySQL: " . mysqli_connect_error();
			  }
			 /* // create connection with CFD end */
			 if($panid!=""){
				 $query = $this->db->query("SELECT partner_id from pct_initiative_monthly_invoice_records where invoice_sent='0' AND partner_id ='".$panid."'  AND request_for_funds='1' GROUP BY partner_id");
			 }else{
			    $query = $this->db->query("SELECT partner_id from pct_initiative_monthly_invoice_records where invoice_sent='0' AND request_for_funds='1' GROUP BY partner_id");
			 }			 
			 $partners = $query->result_array();
			
			//echo "<pre>"; print_r($partners); die("here");
			 
			 $first_of_month = date('d-m-Y', strtotime(date('Y-m-1')));
			 $current_date = date('d-m-Y');
			 
			// if($current_date == $first_of_month){                  //On first of month
			  if(!empty($partners)){
					 foreach($partners as $_partner){
						 //echo "<pre>"; print_r($_SESSION['login']['data']); die;
						  $query = $this->db->query("SELECT mi.*,u.email,u.tax,u.currency,u.price,u.dashboard_user_email from pct_users as u LEFT JOIN pct_initiative_monthly_invoice_records AS mi
								ON mi.partner_id=u.id where mi.partner_id='".$_partner['partner_id']."' AND mi.invoice_sent='0' AND mi.request_for_funds='1' order by id ASC");
						  $pct_rows = $query->result_array();
						   //echo "<pre>"; print_r($pct_rows); die("here");
						  $sql = $this->db->query("SELECT SUM(tree_nums) AS tree_nums,SUM(free_trees) AS free_trees from pct_initiative_monthly_invoice_records where partner_id='".$_partner['partner_id']."' AND invoice_sent='0' AND request_for_funds='1'");
						  $trees = $sql->row_array();
							$item2 = array();
							$bulk_partner = $this->user_model->load_initiative($_partner['partner_id']);
							$email_template = $this->user_model->load_initiative_email_template($bulk_partner['initiative'],4);
							$item2['name'] = "Total number of trees";
							//echo "<pre>"; print_r($_SESSION['login']['data']); die;
							$item2['free_trees'] = $trees['free_trees'];
							$item2['unit'] = $pct_rows[0]['price'] / 100;
							$item2['tree_nums'] = $trees['tree_nums'];
							
							
							switch ($pct_rows[0]['currency']) {
									case 1:
											$item2['currency'] = '$';
											break;
									case 2:
											$item2['currency'] = '&pound;';
											break;
									case 3:
											$item2['currency'] = '€';
											break;

							}
							$opportunity_str = '';
							$currency = $item2['currency'];
							$item2['price'] = $item2['tree_nums'] * $item2['unit'];
							$tax = $pct_rows[0]['tax'];
							$sub_total = $currency . $item2['price'];
							//$total_no = $item2['price'] * (100 + $tax) / 100;
							 $total_no = $item2['price'];
							$total = $item2['currency'] . number_format($total_no, 2);
							$total_amt = $item2['currency'] . number_format($total_no, 2);
							  // store invoice in database
							$data1 = array();
							$data1['user_id'] = $_partner['partner_id'];
							$data1['date_created'] = 'NOW()';
							$data1['total'] = $total_no;
							$data1['state'] = 0;
						   // $data1['trees'] = $_SESSION['login']['data']['current_tree'];
							$data1['figure_history_key'] = $this->randomKey(10);
							$data1['trees'] = $trees['tree_nums']+$trees['free_trees'];
							$total_planted_trees = $trees['tree_nums']+$trees['free_trees'];
							//echo "<pre>"; print_r($data1); die;
							$invoice_id = $this->user_model->insert_initiative_invoice($data1);
							
							
							$dashboard_user_email = $pct_rows[0]['dashboard_user_email'];
							if($dashboard_user_email != ""){
								$user_email = $dashboard_user_email;
							}else{
								$user_email = $pct_rows[0]['email'];
							}
							
							$cfwp_sql = "SELECT * from `cfwp_users` AS u INNER JOIN cfwp_ls_partners_details AS pd ON u.ID=pd.user_id where u.user_email='".$user_email."' AND pd.cancelled='0'";
							$cfwp_result = mysqli_query($con,$cfwp_sql);
							
							$mendate_id = "";
							if(mysqli_num_rows($cfwp_result) > 0) {   

								$cfd_row = mysqli_fetch_assoc($cfwp_result);
								$cfwp_user_id 	= $cfd_row['ID'];
								$pan_user_id 	= ""; 
								$mendate_id 	= $cfd_row['mendate_id'];
								$customer_id 	= $cfd_row['customer_id'];

							}else{
								
								$pan_user_id 	= $bulk_partner['pan_user_id']; 
								$partner_sql = "SELECT * from `cfwp_ls_partners_details` where panacea_id='".$pan_user_id."' AND cancelled='0'";
								
								$partner_result = mysqli_query($con,$partner_sql);
								if(mysqli_num_rows($partner_result) > 0) { 
								
									$cfdp_row = mysqli_fetch_assoc($partner_result);
									
									$cfwp_user_id 	= ""; 
									$mendate_id 	= $cfdp_row['mendate_id'];
									$customer_id 	= $cfdp_row['customer_id'];
								}
								
							}
							
							
							$gocardless_link = false;
							
							//if(mysqli_num_rows($cfwp_result) > 0) {                  //change this sign after testing to >
							
							if($mendate_id != "") {  	
									 
									/*Call goCardless API to create payment using mendate ID*/	
									switch ($pct_rows[0]['currency']) {
										    case 1:
													$payment_currency = 'GBP';             //usd But it does not exist
													break;
											case 2:
													$payment_currency = 'GBP';
													break;
											case 3:
													$payment_currency = 'EUR';
													break;
									}
									
									//$token = "sandbox_ZH42k6x0Ee7-owAISf_-HLlRsbeWQBc07NwUNN2Z";  //sandbox token
									
									$new_gocardless_enable_date  = strtotime("2020-01-20 11:00:00");
									$user_register_date  		 = strtotime($bulk_partner['user_register_date']);
									
									if($user_register_date > $new_gocardless_enable_date){
										$token = "live_icu2SM1QdA7w2GTGJF1mKV58dU00O0VM58y-XVAb";      //New user
										$_SESSION['gocardless_user'] = "new";
									}else{ 
										$token = "live_roMMEqbGeKFuN_sHAyfqYGLOFsMvmEDpVeP5Ltec";      //Old user
										$_SESSION['gocardless_user'] = "old";
									}
									
									$client = new \GoCardlessPro\Client([
										'access_token' => $token,
										'environment' => \GoCardlessPro\Environment::LIVE  			//LIVE/SANDBOX
									]); 
									
									try {
											if(isset($bulk_partner['delay_payment_by']) && $bulk_partner['delay_payment_by'] > 0){
												$delay_payment_by =	$bulk_partner['delay_payment_by'];
												$today_date = date('Y-m-d');
												$charge_date = date('Y-m-d', strtotime($today_date. '+ '.$delay_payment_by.' days'));	
												
												 $payment= $client->payments()->create([
													  "params" => ["amount" => $total_no*100,
																   "currency" => $payment_currency,
																    "charge_date" => $charge_date,
																   "links" => [
																	 "mandate" =>$mendate_id
																   ]]
													]);

											}else{
												
												$payment= $client->payments()->create([
												  "params" => ["amount" => $total_no*100,
															   "currency" => $payment_currency,
															   "links" => [
																 "mandate" =>$mendate_id
															   ]]
												]);
											}
										
											$gocardless_link = false;
											
											if(!empty($payment)){
												
												$insert_cfwp_sql = "INSERT INTO cfwp_ls_partners_payments SET user_id = '{$cfwp_user_id}',panacea_id = '{$pan_user_id}',customer_id = '{$customer_id}',amount = '{$total_no}',trees = '{$total_planted_trees}',payment_id = '{$payment->id}',payment_status = 'Pending',invoice_id = {$invoice_id}";
												 mysqli_query($con,$insert_cfwp_sql); 
												 
												 $payment_status['id'] = $invoice_id;
												 $payment_status['state'] = '-3';
												 $payment_status['payment_type'] = '1';
												 $this->db->update_batch('pct_invoices', array($payment_status), 'id');
											}
									}catch (\GoCardlessPro\Core\Exception\ApiException $e) {
											 echo  $error = $e->getMessage();
											  die("Error");
											} catch (\GoCardlessPro\Core\Exception\MalformedResponseException $e) {
											 echo   $error = $e->getMessage();
											  die("Error");
											} catch (\GoCardlessPro\Core\Exception\ApiConnectionException $e) {
											 echo  $error = $e->getMessage();
											  die("Error");
									}
									
							}else{
									$activation_key = $this->randomKey(25);
									$pcinvoice['gc_activation_key'] = $activation_key;
								    $pcinvoice['id'] = $invoice_id;
								    $this->db->update_batch('pct_invoices', array($pcinvoice), 'id');
										 
									$gocardless_link = "https://carbonfreedining.org/pay-by-gocardless/?activation_key=$activation_key";
								
							}
							
						     //echo "<pre>"; print_r( $pct_rows); die("here"); 
							$invoice_number = $invoice_id;
							$invoice_company = $cert_company = $bulk_partner['company'];
							$initiative_id = $bulk_partner['initiative'];
							
							$counter_type =  $bulk_partner['counter_type'];                           // check if partner counter is selected
		
							$ini_counter_type =  $bulk_partner['ini_counter_type'];					 // check if initiative counter is selected
							
							$sql1 = $this->db->query("SELECT * from pct_initiative_partner where user_id='".$_partner['partner_id']."'");
						    $pct_ini = $sql1->row_array();
							$address = $pct_ini['address'];
							$city = $pct_ini['city'];
							$state = $pct_ini['state'];
							$post_code = $pct_ini['post_code'];
							$ini_first_name = $pct_ini['first_name'];
							$current_month_text = date("F_Y");
							$invoice_name = 'The_Green_Earth_Appeal_Request_For_Funds_' . $current_month_text . '_' . $invoice_id .'.pdf';
							$cert_name = 'The_Green_Earth_Appeal_Certificate_' . $current_month_text . '_' . $invoice_id . '.pdf';
							$invoice_data = array();
							$invoice_data['id'] = $invoice_id;
							$invoice_data['number'] = $invoice_number;
							$invoice_data['name'] = $invoice_name;
							$invoice_data['latest_certificate'] = $cert_name;
							$invoice_data['month'] = date('mY');
							$this->db->update_batch('pct_invoices', array($invoice_data), 'id');
							
							$bk_image 	=  $bulk_partner['bk_image'];

							$partner_bk_image =  $bulk_partner['partner_bk_image'];

							if($partner_bk_image != ""){                                   			//check partner background cert image first
								$bk_image 	=  $bulk_partner['partner_bk_image'];
							}else{
								$bk_image 	=  $bulk_partner['bk_image'];
							}
							require('libraries/monthly_funds_request.php');
							/* echo "https://www.greenearthappeal.org/panacea/uploads/request_for_funds/".$invoice_name;
							echo '<br>';
							die("Request fund created"); */
							$sql3 = $this->db->query("SELECT SUM(tree_nums) AS tree_nums,SUM(free_trees) AS free_trees,(SELECT Sum(tree_nums+free_trees) FROM pct_initiative_partner WHERE user_id='".$_partner['partner_id']."') AS tree_nums2 from pct_initiative_partner where bulker_id='".$_partner['partner_id']."'");
							$get_total_trees = $sql3->row_array();
							$total_trees = $get_total_trees['tree_nums'] + $get_total_trees['free_trees'] + $get_total_trees['tree_nums2'];
							if($total_trees=='1'){
								 $client_trees = number_format($total_trees).' '.'tree';
							}else{
								$client_trees = number_format($total_trees).' '.'trees';
							}
							$calculated_cups = $total_trees*1000;
							$restaurant = $bulk_partner['company'];
							$sql2 = $this->db->query("SELECT pi.initiative_name,pi.sender_email_title,pip.initiative from pct_initiative as pi INNER JOIN pct_initiative_partner AS pip
								ON pip.initiative=pi.id where pip.user_id='".$_partner['partner_id']."'");
						    $initiatives = $sql2->row_array();
						    $initiative = $initiatives['initiative_name'];
							
							$all_counters 	= $this->get_cfd_gea_counters();
							
							$certificate_mess1 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($bulk_partner['company'], $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate1']));
							$certificate_mess2 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($bulk_partner['company'], $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate2']));
							$certificate_mess3 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($bulk_partner['company'], $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate3']));
							$certificate_mess4 = str_ireplace(array('[company_name]', '[client_trees]', '[initiative_name]','[calculated_cups]', '[total_trees]','[cfd_counter]','[gea_counter]'), array($bulk_partner['company'], $client_trees, $initiative,$calculated_cups, $client_trees,$all_counters['cfd_counter'],$all_counters['gea_counter']), stripslashes($bulk_partner['certificate4']));
							$certificate_sub = $certificate_msg = $certificate_text = '';
							$certificate_cc_email = preg_replace("@[\t]@", '', $bulk_partner['cc_email']);
							$certificate_sub = preg_replace("@[\t]@", '', $bulk_partner['subject']);
							$certificate_msg = preg_replace("@[\t]@", '', $bulk_partner['message']);
							$certificate_text = preg_replace("@[\t]@", '', $bulk_partner['certificate_text']);
							$certificate_text = stripslashes($certificate_text);
						    $certificate_text = str_ireplace(array('[name]', '[total_trees]', '[initiative]', '[opportunity_str]'), array($bulk_partner['company'], $total_trees, $initiative, $opportunity_str), $certificate_text);
							//echo "here";
							require('libraries/cert-ini.php');
							 // create the ticker
							$refid = $this->user_model->get_refid($_partner['partner_id']);
							$ticker_url = base_url() . 'tickers/ticker_' . $refid->code . '.png';
							//$this->create_ticker($total_trees, $refid->code);
							
							/* if($initiative_id =='34' || $initiative_id =='35' || $initiative_id =='25' || $initiative_id =='31' ){
								$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
							}else{
								 $this->create_ticker($total_trees, $refid->code);            //GEA tickers
							} */
							
							if($counter_type != 0){															// if counter is set on partner
									if($counter_type==1){
										 $this->create_ticker($total_trees, $refid->code);           //GEA tickers
									}elseif($counter_type==2){
										$this->create_cfd_ticker($total_trees, $refid->code);        //CFD tickers
									}elseif($counter_type==3){
										$this->create_non_partner_ticker($total_trees, $refid->code);        //Non partner tickers
									}
									
								}else{                                                                              // if counter is set on initiative
									if($ini_counter_type==1){
										$this->create_ticker($total_trees, $refid->code);                //GEA tickers
									}elseif($ini_counter_type==2){
										$this->create_cfd_ticker($total_trees, $refid->code);      //CFD tickers
									}elseif($ini_counter_type==3){
										$this->create_non_partner_ticker($total_trees, $refid->code);        //Non partner tickers 
									}
								}
								
							$this->create_ticker($all_counters['gea_counter'], 'gea_counter');           //create GEA ticker for all partners
							
							$this->create_cfd_ticker($all_counters['cfd_counter'], 'cfd_counter');		 //create CFD ticker for all partners
							
							$this->create_cfd_small_ticker($total_trees, $refid->code, 'black');
							
							$this->create_cfd_small_ticker($total_trees, $refid->code, 'green');
							
							$this->create_cfd_small_ticker($total_trees, $refid->code, 'white');
							
							//to create pdf images for initiatives
							$cert_thumb_url = base_url() . 'cert/thumb_' . $refid->code . '.jpg';
							$cert_image_url = base_url() . 'cert/full_' . $refid->code . '.jpg';
							if (extension_loaded('imagick')){   		//extension imagick is installed
								$cert_path = dirname(__FILE__) . '/../../uploads/certs/';
								$pdf_path = $cert_path.$cert_name;
								$image_url = dirname(__FILE__) . '/../../cert/';
								$this->create_cert_pdf_to_image($pdf_path, $refid->code, $image_url); 
							}	
							$unique_url = $bulk_partner['website_url'].'/?refid=' . $refid->code;
					    	$certificate_url =  "";
							$config['attach'] = array(dirname(__FILE__) . '/../../uploads/request_for_funds/' . $invoice_name, dirname(__FILE__) . '/../../uploads/certs/' . $cert_name);
							$initiative_id	= $initiatives['initiative'];
							$email_template = $this->user_model->load_initiative_email_template($initiative_id,4);
							$email_title = $initiatives['sender_email_title'];
							$subject = $email_template->subject;
							$config['subject']  = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[first_name]','[initiative_name]','[calculated_cups]'), array($pct_ini['first_name'],$bulk_partner['company'], $total_trees,$total_trees, $pct_ini['first_name'],$initiatives['initiative_name'],$calculated_cups), $subject);
							$message = $email_template->message; 
							$config['message'] = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[first_name]','[initiative_name]','[calculated_cups]','[ticker_url]', '[unique_url]','[cert_thumb_url]','[cert_image_url]','[certificate_url]','[cfd_counter]','[gea_counter]'), array($pct_ini['first_name'],$bulk_partner['company'], $total_trees,$total_trees, $pct_ini['first_name'],$initiatives['initiative_name'],$calculated_cups,$ticker_url,$unique_url,$cert_thumb_url,$cert_image_url,$certificate_url,$all_counters['cfd_counter'],$all_counters['gea_counter']), $message);
							if($email_title==""){
								  $config['email_title'] = "Carbon Free Dining";
							  }else{
								  $config['email_title'] = $email_title;
							  }
							  $invoice['invoice_sent'] = '1';
							  $invoice['request_for_funds'] = '0';
							  $invoice['partner_id'] = $_partner['partner_id'];
							  if($_partner['partner_id']!='3733'){
								$this->db->update_batch('pct_initiative_monthly_invoice_records', array($invoice), 'partner_id');
							  }
							  //$config['to'] = 'testing.whizkraft1@gmail.com';
							   //$config['to'] = 'mat@greenearthappeal.org';
						      // $config['cc'] = "marvin@greenearthappeal.org";  
						      $config['bcc'] = "billing@greenearthappeal.org";  
						      $config['to'] = $bulk_partner['email'];
							 
							$this->send_mail($config);  
							echo "Emails sent";
					}
			  }else{
				  echo "No records found yet!";
			  }
			// } //end of first of month 
			
		}
		 /**
         * =========================================================================================================
         *  functions: to send invoice to partner monthly
         */

		function send_monthly_invoices() {
			 require_once('phpmailer/class.phpmailer.php');
			 $this->load->database();
			 $this->load->model('user_model');
			 $query = $this->db->query("SELECT i.name as invoice_name,i.latest_certificate,mi.*,u.email FROM pct_invoices AS i LEFT JOIN pct_users AS u
							ON u.id=i.user_id
						INNER JOIN pct_initiative_monthly_invoice_data AS mi
                        ON mi.invoice_id=i.id							
						WHERE u.type='intitiative' AND i.invoice_sent_monthly='0'");				
							
			 $rows = $query->result_array();
			  // echo "<pre>"; print_r($rows); die;
			 $email_template = $this->user_model->load_initiative_email_template('19',4);
			 $query2 = $this->db->query("SELECT * FROM pct_initiative WHERE id='19'");
			 $res2 = $query2->row_array();
			 $email_title = $res2['sender_email_title'];
			// echo "<pre>"; print_r($rows); die('here');
			 $first_of_month = date('d-m-Y', strtotime(date('Y-m-1')));
			 $current_date = date('d-m-Y');
			 if($current_date==$first_of_month){                  //On first of month
			 
			 echo "<pre>"; print_r($rows);
				  if(!empty($rows)){
					 foreach($rows as $_row){
						  $config = array();
						  $data = array();
						  $subject = $email_template->subject;
						  $calculated_cups = $_row['total_trees']*1000;
						  
						  $config['subject'] = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]', '[unique_url]','[initiative_name]','[calculated_cups]'), array($_row['first_name'],$_row['company_name'], $_row['total_trees'], $_row['current_trees'], $_row['total'], $_row['first_name'],$_row['ticker_url'],$_row['unique_url'],$_row['initiative_name'],$calculated_cups), $subject);
				
						  $message = $email_template->message; 
						  $config['message'] = str_ireplace(array('[contact_person]', '[company_name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]', '[unique_url]','[initiative_name]','[calculated_cups]','[cert_thumb_url]','[cert_image_url]'), array($_row['first_name'],$_row['company_name'], $_row['total_trees'], $_row['current_trees'], $_row['total'], $_row['first_name'],$_row['ticker_url'],$_row['unique_url'],$_row['initiative_name'],$calculated_cups,$_row['cert_thumb_url'],$_row['cert_image_url']), $message);
						  
						  $config['attach'] = array(dirname(__FILE__) . '/../../uploads/invoices/' . $_row['invoice_name'], dirname(__FILE__) . '/../../uploads/certs/' . $_row['latest_certificate']);
						//  $config['to'] = $_row['email'];
						  $config['to'] = 'mat@greenearthappeal.org';
						  $config['cc'] = "marvin@greenearthappeal.org";     
						  if($email_title==""){
							  $config['email_title'] = "Cups That Care";
						  }else{
							  $config['email_title'] = $email_title;
						  }
						 // $this->send_mail($config);  
						  $data['invoice_sent_monthly'] = '1';
						  $data['id'] = $_row['invoice_id'];
						  $this->db->update_batch('pct_invoices', array($data), 'id');
					 }
				  }
			 }
			
		}
		
		 /**
         * =========================================================================================================
         *  functions: to send auto input email for restaurants
         */

		function monthly_auto_emails_to_restaurants() {
			 require_once('phpmailer/class.phpmailer.php');
			 $this->load->database();
			 $this->load->model('user_model');
			 $query = $this->db->query("SELECT pr.*,u.email FROM pct_restaurants as pr INNER JOIN pct_users AS u
                        ON u.id=pr.user_id WHERE pr.auto_input_email='1'");								
			 $rows = $query->result_array();
			  //echo "<pre>"; print_r($rows); die('here');
			 $first_of_month = date('d-m-Y', strtotime(date('Y-m-1')));
			 $current_date = date('d-m-Y');
			 //if($current_date==$first_of_month){                  //On first of month
			   if(!empty($rows)){
					 foreach($rows as $_row){
						  //echo "<pre>"; print_r($_row); 
						 $data['id'] = $_row['user_id'];
						 $data['activation_key'] = $this->randomKey(10);
						 $this->db->update_batch('pct_users', array($data), 'id');
						 $record['user_id'] = $_row['user_id']; 
						 $record['first_name'] = $_row['first_name']; 
						 $record['month'] =  date('FY');   // like September2017
						 $this->db->insert_batch('pct_invite_restaurants_figures_records', array($record));
						 
						 $message = "";
						 $message.='Dear ' .ucfirst($_row['first_name']).","."<br><br>";
						 $message.='Please submit your restaurant figure for the previous month.' . "<br><br>";
						 $message.='To submit restaurant figure <a href="https://www.greenearthappeal.org/panacea/index.php/user/restaurant_autologin/' .$data['activation_key'].'">click here</a>' . "<br><br>";
						 $message.= "Kind Regards,".'<br><br>';
						 $message.="Partnerships Department".'<br><br>';
						 $message.="Green Earth Appeal".'<br><br>';
						 $config['message'] = $message;
						 //$config['to'] = 'testing.whizkraft1@gmail.com';
						 //$config['to'] = $_row['email'];
						 $config['to'] = 'mat@greenearthappeal.org';
						 $config['cc'] = "marvin@greenearthappeal.org";    
						 $config['subject'] = 'Submit your restaurant figure for the previous month';
						 $this->send_mail($config);  
						 echo "Email sent!";
						 echo "<br>";
					 }
			   }       //end of for loop
			// }else{
			//	echo "It will send emails on the Ist of the month";
			//}
			 
			 
		}
		 /**
         * =========================================================================================================
         *  functions: to send reminder email to restaurants
         */

		function reminder_emails_to_restaurants() {
			 require_once('phpmailer/class.phpmailer.php');
			 $this->load->database();
			 $this->load->model('user_model');
			 $month =  date('FY'); 
			 $query = $this->db->query("SELECT pr.*,u.email,u.activation_key FROM pct_users as u LEFT JOIN pct_invite_restaurants_figures_records AS pr
                        ON pr.user_id=u.id WHERE pr.submit_figure='0' AND month='".$month ."'");								
			 $rows = $query->result_array();
			 
			  if(!empty($rows)){
					 foreach($rows as $_row){
						    //echo "<pre>"; print_r($_row); die('here');
							$datetime1 = new DateTime($_row['created_date']);    //email send to restaurant time
							$datetime2 = new DateTime(date('Y-m-d H:i:s'));    //current time
							$interval = $datetime1->diff($datetime2);
							$hours = $interval->format('%H');
							if(($hours%72)==0){                 // after every 72 hours
								 $message = "";
								 $message.='Dear ' .ucfirst($_row['first_name']).","."<br><br>";
								 $message.='Please submit your restaurant figure for the previous month.' . "<br><br>";
								 $message.='To submit restaurant figure <a href="https://www.greenearthappeal.org/panacea/index.php/user/restaurant_autologin/' .$_row['activation_key'].'">click here</a>' . "<br><br>";
								 $message.= "Kind Regards,".'<br><br>';
								 $message.="Partnerships Department".'<br><br>';
								 $message.="Green Earth Appeal".'<br><br>';
								 $config['message'] = $message;
								 //$config['to'] = 'testing.whizkraft1@gmail.com';
								 //$config['to'] = $_row['email'];
								 $config['to'] = 'mat@greenearthappeal.org';
								 $config['cc'] = "marvin@greenearthappeal.org";    
								 $config['subject'] = 'Reminder email to submit restaurant figure for the previous month';
								 $this->send_mail($config);  
								 //echo "Email sent to ".$_row['email']."";
								 echo "Email sent!";
								 echo "<br>";
							}else{
								echo "It will send reminder email after 72 hours for ".$_row['email']."";
								echo "<br>";
							}
							
					 }
			  }
		}
		
		/**
         * =========================================================================================================
         *  functions: to send restaurant invoice email on next day
         */

		function restaurant_invoice_email_on_nextday() {
			 require_once('phpmailer/class.phpmailer.php');
			 $this->load->database();
			 $this->load->model('user_model'); 
			 $query = $this->db->query("SELECT pr.*,u.email FROM pct_users as u LEFT JOIN pct_restaurants_nextday_invoices_data AS pr
                        ON pr.user_id=u.id WHERE pr.sent_invoice='0'");								
			 $rows = $query->result_array();
			// echo "<pre>"; print_r($rows); die('here');
			 if(!empty($rows)){
					 foreach($rows as $_row){
								//echo "<pre>"; print_r($_row); die('here');
								$dt = new DateTime($_row['created_date']);
								$dt->modify('+1 day');             //add one day to the date
								/* $data = "2017-09-16";
								if(date("w",strtotime($data))==6)
									echo "Sat"; */
						        $created_date =	$dt->format('Y-m-d');
								$current_date = date('Y-m-d');
						      if($created_date==$current_date){
									$email_template = $this->user_model->load_email_template(5);
									$message = $email_template->message;
									$message = str_ireplace(array('[name]', '[total_trees]', '[current_trees]', '[total]', '[first_name]', '[ticker_url]'), array($_row['restaurants_name'], $_row['total_trees'], $_row['current_trees'],$_row['total'], $_row['first_name'], $_row['ticker_url']), $message);
									$config = array();
									$config['to'] = $_row['email'];
									//$config['to'] ="testing.whizkraft1@gmail.com";
									//$config['to'] = 'mat@greenearthappeal.org';
									$config['cc'] = "marvin@greenearthappeal.org";
									$config['subject'] = "Partnership Certificate and Invoice";
									$config['message'] = $message;
									$config['attach'] = array(dirname(__FILE__) . '/../../uploads/certs/' .  $_row['cert_name'], dirname(__FILE__) . '/../../uploads/invoices/' .  $_row['invoice_name']);
									$this->send_mail($config);
									$data['sent_invoice'] = '1';
									$data['id'] = $_row['id'];
									$this->db->update_batch('pct_restaurants_nextday_invoices_data', array($data), 'id');
									echo "email sent";
							  }else{
								  echo "No invoice found yet";
							  }
					 }
			 }else{
				 echo "No invoice found yet";
			 }
		}
        /**
         * =========================================================================================================
         * common functions
         */

        function send_mail($data) {
                $mail             = new PHPMailer();
                $mail->IsSMTP(); // telling the class to use SMTP
                $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                // 1 = errors and messages
                // 2 = messages only
                $mail->SMTPAuth   = true;                  // enable SMTP authentication
                $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
                $mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
                $mail->Port       = 587;                   // set the SMTP port for the GMAIL server
                $mail->Username   = "partners@greenearthappeal.org";  // GMAIL username
                $mail->Password   = "P8%k<g_39x,#1";            // GMAIL password
                $mail->CharSet = 'UTF-8';

                if (isset($data['from'])) {
                        $mail->SetFrom($data['from'], '');
                } elseif(isset($data['email_title'])){
                        $mail->SetFrom('partners@greenearthappeal.org', $data['email_title'] );
                }else{
					$mail->SetFrom('partners@greenearthappeal.org', 'Partnerships Department – Green Earth Appeal');
				}

                if (isset($data['attach']) && !empty($data['attach'])) {
                        if (!is_array($data['attach'])) {
                                $data['attach'] = (array) $data['attach'];
                        }
                        foreach ($data['attach'] AS $attach) {
                                $mail->AddAttachment($attach); // attachment
                        }
                }

                ob_start();
                if ((isset($data['signature']) && $data['signature'] == false) || (!$data['signature'])) {
                        echo '<style type="text/css">*, p {font-family:Calibri!important; font-size: 11pt!important;}</style>';
                        require('libraries/signature.htm');
                } else {
                        require('libraries/signature_solicitor.htm');
                }

                $message = ob_get_contents();
                ob_end_clean();

                $message = $data['message'] . $message;

                $mail->ClearReplyTos();
                $mail->AddReplyTo("marvin@greenearthappeal.org", "Marvin Baker");
                $mail->Subject    = $data['subject'];
                $mail->MsgHTML($message);
                $mail->AddAddress($data['to']);

                if (isset($data['cc'])) {
                        $cc_emails = explode(',', $data['cc']);
                        foreach ($cc_emails AS $cc) {
                                $mail->AddCC($cc);
                        }
                }

                if (isset($data['bcc'])) {
                        $bcc_emails = explode(',', $data['bcc']);
                        foreach ($bcc_emails AS $bcc) {
                                $mail->AddBCC($bcc);
                        }
                }

                /*if (@$attachment) {
                                $mail->AddAttachment($attachment); // attachment
                }*/

                if (!$mail->Send()) {
                        echo "Mailer Error: " . $mail->ErrorInfo;
                }

        }
		 /**
         * =========================================================================================================
         * common functions
         */

        function get_date_differnce($date="") {
				$now = strtotime(date('Y-m-d'));  				// current date
				$dt = new DateTime($date);
				$date_created = strtotime($dt->format('Y-m-d'));
				$datediff = $now - $date_created;
				$days = floor($datediff / (60 * 60 * 24)); 
				return $days;
		}
		 /**
         * =========================================================================================================
         * to get total tpcc trees and used this in the tpcc api cron email 
         */

        function get_total_tpcc_trees() {
				$url ='http://www.greenearthappeal.org/panacea/index.php/welcome/get_corporate_partner/tpcc';
				$res = file_get_contents($url);
				if(!empty($res)){
				 $result = json_decode($res);
				}
				$api_url ='http://giftmeatree.org/sli_tree_nums_api.php?gift_key='.$result->code;    //total tree sign ups via page
				$res = file_get_contents($api_url);
				if(!empty($res)){
				  $tree_numbers = json_decode($res);
				}
				
			    $total_trees= $result->tree_nums + $tree_numbers + $result->referral_trees + $result->sli_trees; 
				$arr = array("total_trees"=>$total_trees,"tli_name"=>$result->title);
			    echo  json_encode($arr);
		}
		function randomKey($length) {  //create no of random digit alphanumeric code
			$key='';
			$pool = array_merge(range(0,9),range('A', 'Z'));

			for($i=0; $i < $length; $i++) {
				$key .= $pool[mt_rand(0, count($pool) - 1)];
			}
			return $key;
		}
		 function create_ticker($trees, $refid) {

                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
                $text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                 $fname = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }
		
		function create_cfd_small_ticker($trees, $refid, $color) {
			
                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/cfd-tickers/cfd-small-ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);
				
				// Allocate A Color For The Text - white,Green,Black
				if($color=="black"){
					$font_color = imagecolorallocate($png_image, 0, 0, 0);
				}elseif($color=="white"){
					$font_color = imagecolorallocate($png_image, 255, 255, 255);
				}else{
					$font_color = imagecolorallocate($png_image, 22, 105, 54);      //green
				}

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
                $text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                $fname = dirname(__FILE__) . '/../../tickers/cfd-tickers/cfd_'.$color.'_ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }
		function create_non_partner_ticker($trees, $refid) {
			
                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                 $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/non-partner.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/MYRIADPRO-BOLDCOND.OTF';

                // Set Number Text to Be Printed On Image
                 $text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '140';
                } else if ($chklen == 2) {
                        $dist = 125;
                } else if ($chklen == 3) {
                        $dist = 112;
                } else if ($chklen == 4) {
                        $dist = 92;
                } else if ($chklen == 5) {
                        $dist = 88;
                }else if ($chklen == 6) {
                        $dist = 69;
                }else if ($chklen == 7) {
                        $dist = 58;
                }else if ($chklen == 9) {
                        $dist = 40;
                }
				


                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 28, 0, $dist, 35, $font_color, $font_path, $text);

               $fname = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
        }
		
		function create_cfd_ticker($trees, $refid) {
			
                //Set the Content Type
                // @header('Content-type: image/png');
                // Create Image From Existing File
                $png_image = imagecreatefrompng(dirname(__FILE__) . '/../../tickers/cfd-tickers/cfd-ticker.png');

                // Allocate A Bachground Color For The Text - TRANSPARENT
                $bg_color = imagecolorallocatealpha($png_image, 22, 105, 54, 127);

                // Allocate A Color For The Text - GREEN
                $font_color = imagecolorallocate($png_image, 22, 105, 54);

                // Set Path to Font File
                $font_path = dirname(__FILE__) . '/../../fonts/HelveticaLTStd-Bold.ttf';

                // Set Number Text to Be Printed On Image
                 $text = number_format($trees);
                $chklen = strlen($text);
                if ($chklen == 1) {
                        $dist = '134';
                } else if ($chklen == 2) {
                        $dist = 106;
                } else if ($chklen == 3) {
                        $dist = 80;
                } else if ($chklen == 4) {
                        $dist = 56;
                } else if ($chklen == 5) {
                        $dist = 28;
                }

                imagesavealpha($png_image, true);
                imagefill($png_image, 0, 0, $bg_color);

                // Print Text On Image
                imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);

                 $fname = dirname(__FILE__) . '/../../tickers/ticker_' . $refid . '.png';

                // Send Image to Browser
                imagepng($png_image, $fname);

                // Clear Memory
                imagedestroy($png_image);
				
        }
		function create_cert_pdf_to_image($pdf,$ref_id,$image_url) {  //create image from certificate pdf
				$im = new imagick();
				$im->setResolution(300, 300);
				$im->readImage($pdf);
				$im->setImageColorspace(13); 
				$im->setImageCompression(Imagick::COMPRESSION_JPEG); 
				$im->setImageCompressionQuality(80);
				$im->setImageFormat('jpeg'); 
				$im->resizeImage(1199, 1771, imagick::FILTER_LANCZOS, 1);  
				$im->writeImage($image_url.'full_'.$ref_id.'.jpg');  
				$im->resizeImage(300, 443, imagick::FILTER_LANCZOS, 1);  
				$im->writeImage($image_url.'thumb_'.$ref_id.'.jpg'); 
				$im->clear(); 
				$im->destroy(); 
				return ;
		}
		/* TO get CFD and GEA counter */
		public function get_cfd_gea_counters()
		{
			$query = "SELECT u.id,bp.counter_type,pi.ini_counter_type,(bp.free_trees + bp.tree_nums) as partner_trees, (SELECT (SUM(free_trees)+ SUM(tree_nums)) FROM pct_initiative_partner WHERE bulker_id=u.id) As client_trees
						FROM pct_initiative_partner AS bp
						INNER JOIN pct_users AS u
						ON u.id=bp.user_id
						INNER JOIN pct_initiative AS pi
						ON pi.id=bp.initiative
						WHERE u.type IN ('admin', 'intitiative') and active = '1'";
				
		   $results = $this->db->query($query)->result();
		   $all_trees = array();
		   // echo "<pre>"; print_r($results); die;
		   foreach($results as $result){
				   $total_trees = 0;
				   $total_trees = $result->client_trees + $result->partner_trees;
				   
				   if($result->counter_type != 0){	
						if($result->counter_type == 1){	
							$all_trees['gea_counter']  += $total_trees;
						}elseif($result->counter_type==2){
							$all_trees['cfd_counter']  += $total_trees;
						}else{
							$all_trees['non_partner_counter']  += $total_trees;
						}
						
				   }else{
						if($result->ini_counter_type == 1){	
							$all_trees['gea_counter']  += $total_trees;
						}elseif($result->ini_counter_type == 2){
							$all_trees['cfd_counter']  += $total_trees;
						}else{
							$all_trees['non_partner_counter']  += $total_trees;
						}
				   }
				   $all_trees['all_partner_trees']  += $total_trees;
		   } 
		   //echo "<pre>"; print_r($all_trees); die;
		   return  $all_trees;
		}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */