<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__).'/tcpdf/config/lang/eng.php');
require_once(dirname(__FILE__).'/tcpdf/tcpdf.php');
require_once(dirname(__FILE__).'/fpdi/fpdi.php');

class PDF1 extends FPDI {
 /**
 * "Remembers" the template id of the imported page
 */
 var $_tplIdx;
 /**
 * include a background template for every page
 */
 
 var $pdf_path = '';
 
 function Header() {
	 if (is_null($this->_tplIdx)) {
		 $pdf_path = dirname(__FILE__).'/../uploads/solicitor/';	 
		 $this->setSourceFile($pdf_path.'Equitable_Law_Template_Page1.pdf');
		 $this->_tplIdx = $this->importPage(1);
	 }
	 
	 $this->useTemplate($this->_tplIdx);
	 $this->SetTextColor(255);
	 $this->SetXY(60.5, 24.8);
	 $this->Cell(0, 12, "TCPDF and FPDI");
 }
 
 function Footer() {}
 
}
 
// initiate PDF
$pdf = new PDF1();
$pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
$pdf->SetAutoPageBreak(true, 40);
$pdf->setFontSubsetting(false);
$pdf->SetFont('freesans', '', 10);

// add a page
$pdf->AddPage();
$pdf->WriteHTML($pdftext_all_page1);

$save_path = dirname(__FILE__).'/../uploads/solicitor/';
$newname = 'Equitable_Law_'.$user_id.'.pdf';  
$pdf->Output($save_path.$newname, 'F');