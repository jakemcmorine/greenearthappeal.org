<?php $pdftext_all_page2 = '<table width="98%" style="margin-left:10px;">
    <tr>
    	<td width="2%">&nbsp;</td>
		<td width="48%">&nbsp;</td>
        <td width="50%">&nbsp;</td>
    </tr>
	<tr>
    	<td width="2%">&nbsp;</td>
		<td colspan="2">We understand that Green Earth Appeal has recently received [amt_words] from [pdftext_comp]. This letter confirms that we have satisfactorily satisfied ourselves as to Green Earth Appeal’s bona fides and undertaken the necessary due diligence upon Green Earth Appeal (and its international network of charities and NGO’s) to confirm that Green Earth Appeal have now paid to an appropriate third party, a sum represented by those monies in a manner which will lead to [num_trees] trees being planted upon Green Earth Appeal’s client’s behalf, either prior to or within a short period after the date of this letter.</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
    <tr>
    	<td>&nbsp;</td>
		<td colspan="2">Our research concludes that based upon research from Dr. Lester R. Brown (formerly of the Earth Policy Institute) (<a style="color: #c00000;" href="https://en.wikipedia.org/wiki/Lester_R._Brown">https://en.wikipedia.org/wiki/Lester_R._Brown</a>), each tree planted will effectively offset a minimum of one metric ton (1 tonne / 1000 kg) of carbon from the environment during its lifetime (Paragraph 4 : <a style="color: #c00000;" href="http://www.earthpolicy.org/images/uploads/book_files/pb4ch08.pdf#page=5">http://www.earthpolicy.org/images/uploads/book_files/pb4ch08.pdf#page=5</a>)<br/><br/> Green Earth Appeal provides further background information at: <a style="color: #c00000;" href="https://greenearthappeal.org/co2-verification">https://greenearthappeal.org/co2-verification</a>.</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
    <tr>
    	<td>&nbsp;</td>
		<td colspan="2">If you have any queries with regard to the subject matter of this letter, please do not hesitate to contact the writer to discuss further.</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
    <tr>
    	<td>&nbsp;</td>
		<td>Yours faithfully</td>
        <td>&nbsp;</td>
	</tr>
    <tr>
    	<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td>&nbsp;</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td colspan="3">
		<img src="http://www.greenearthappeal.org/panacea/libraries/signature.png" alt="signature" style="width: 190px;" />
	</td></tr>
</table>';
?>