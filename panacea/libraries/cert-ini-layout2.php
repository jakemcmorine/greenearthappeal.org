<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__).'/tcpdf/config/lang/eng.php');
require_once(dirname(__FILE__).'/tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
if (!class_exists('MYPDF')) {
    class MYPDF extends TCPDF {
		
		
    	//Page header
    	public function Header() {
    		// get the current page break margin
    		$bMargin = $this->getBreakMargin();
    		// get current auto-page-break mode
    		$auto_page_break = $this->AutoPageBreak;
    		// disable auto-page-break
    		$this->SetAutoPageBreak(false, 0);
    		// set bacground image
    		//$img_file = K_PATH_IMAGES.'bg.jpg';
			
			
			if($this->bgimage!=""){
				$img_file = $_SERVER["DOCUMENT_ROOT"].'panacea/libraries/imgs/'.$this->bgimage;
			}else{
				$img_file = $_SERVER["DOCUMENT_ROOT"].'panacea/libraries/imgs/cfc-bg.png';
			}
             
            //$this->Image($img_file, 0, 0, 1200, 1772, '', '', '', false, 300, '', false, false, 0);
            $this->Image($img_file, 0, 0, 757, 1118, '', '', '', false, 300, '', false, false, 0);
            
    		// restore auto-page-break status
    		$this->SetAutoPageBreak($auto_page_break, $bMargin);
    		// set the starting point for the page content
    		$this->setPageMark();
    	}
		
		public function bgImage($image) {
    		
			$this->bgimage = $image;
			
    	}
    }
}

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pageLayout = array(757, 1118);
$pdf = new MYPDF('p', 'px', $pageLayout, true, 'UTF-8', false);
$pdf->bgImage($bk_image);
// set document information
$pdf->SetCreator('Green Earth Appeal');
$pdf->SetAuthor('Green Earth Appeal');
$pdf->SetTitle('Tree Planting Certificate');
$pdf->SetSubject('Certification of tree planting');
$pdf->SetKeywords('green, earth, appeal, tree, planting, certificate');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(0, 0, 0, true);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
//$pdf->SetFont('times', '', 48);

$fontname1 =  $pdf->addTTFfont($_SERVER["DOCUMENT_ROOT"].'panacea/fonts/Matter-Regular.ttf', 'TrueTypeUnicode', '', 32);
$fontname2 =  $pdf->addTTFfont($_SERVER["DOCUMENT_ROOT"].'panacea/fonts/Matter-Bold.ttf', 'TrueTypeUnicode', '', 32);

// use the font
$pdf->SetFont($fontname1, '', 14, '', false);

// add a page
$pdf->AddPage();
$pdf->SetY(190);
$pdf->SetX(0);
//$cert_client_name = "Nikolai Lind";
$restaurant_top = strtoupper($restaurant);

if ( isset($certificate_text) && !empty($certificate_text) ) {
      $client_name_pdf = "";
} else {
			 // $certificate_2= wordwrap($certificate_mess2, 70, "<br />\n");
			 // $certificate_3= wordwrap($certificate_mess3, 65, "<br />\n");
		 
		     $client_name_pdf = '<p style="font-family:MatterBold; font-weight: bold; font-size: 47px; color:#167d52; margin:0;">K&aelig;re '.ucfirst($cert_client_name).'</p>';
}
$html3 = '<span style="ont-family:MatterRegular; font-size: 14px; color: #167d52">'.date("j F Y").'</span>';
$pdf->writeHTMLCell($w=370, $h=0, $x='40', $y='36', $html3, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->SetFont($fontname2, '', 14, '', false);
$pdf->writeHTMLCell($w=500, $h=0, $x='40', $y='48', $client_name_pdf, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//Close and output PDF document
if ($cert_path) {
    $pdf->Output( $cert_path.$cert_name, 'F');
} else {
    $pdf->Output(dirname(__FILE__).'/../uploads/certs/'.$cert_name, 'F');   
}