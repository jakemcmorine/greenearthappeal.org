<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__).'/tcpdf/config/lang/eng.php');
require_once(dirname(__FILE__).'/tcpdf/tcpdf.php');


// Extend the TCPDF class to create custom Header and Footer
if (!class_exists('MYPDF')) {
    class MYPDF extends TCPDF {
    	//Page header
    	public function Header() {
    		// get the current page break margin
    		$bMargin = $this->getBreakMargin();
    		// get current auto-page-break mode
    		$auto_page_break = $this->AutoPageBreak;
    		// disable auto-page-break
    		$this->SetAutoPageBreak(false, 0);
    		// set bacground image
    		//$img_file = K_PATH_IMAGES.'bg.jpg';
            $img_file = 'libraries/imgs/bg-en3.jpg';
            //$this->Image($img_file, 0, 0, 1200, 1772, '', '', '', false, 300, '', false, false, 0);
            $this->Image($img_file, 0, 0, 757, 1118, '', '', '', false, 300, '', false, false, 0);
            
    		// restore auto-page-break status
    		$this->SetAutoPageBreak($auto_page_break, $bMargin);
    		// set the starting point for the page content
    		$this->setPageMark();
    	}
    }
}

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pageLayout = array(757, 1118);
$pdf = new MYPDF('p', 'px', $pageLayout, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Vinh Nguyen');
$pdf->SetAuthor('Marvin');
$pdf->SetTitle('Certification');
$pdf->SetSubject('');
$pdf->SetKeywords('php programmer, phpcodeteam@gmail.com');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(0, 0, 0, true);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
//$pdf->SetFont('times', '', 48);
$pdf->SetFont('helvetica', '', 40);
// add a page
$pdf->AddPage();
$pdf->SetY(190);
$pdf->SetX(0);

/**
$tbl = <<<EOD
<table cellspacing="0" cellpadding="10" border="0">
    <tr>
        <td rowspan="2" colspan="2">$pct_logo_url</td>
        <td colspan="5"><p style="font-family:Tahoma; font-weight: bold; font-size: 36px; color:#658fca; text-align: center;">{$restaurant}</p></td>
    </tr>
    <tr>
        <td colspan="5"><p style="font-family:Arial; font-weight: bold; font-size: 26px; color:#166734; text-align: center">ha plantado {$total_trees} árboles para compensar la huella de carbono generada por la recolección de su aceite vegetal usado y de sus demás operaciones diarias, a través de un programa ambiental que es operado por The Green Earth Appeal.</p></td>
    </tr>
</table>
EOD;
*/
$restaurant_top = strtoupper($restaurant);

if ( isset($certificate_text) && !empty($certificate_text) ) {
    $pct_header = $certificate_text;
} else {
    $pct_header = '<p style="font-family:Tahoma; font-weight: bold; font-size: 36px; color:#658fca; margin:0; text-align: center;">'.$restaurant_top.'</p>
                <p style="font-family:Tahoma; font-size: 26px; color:green; text-align: center; margin:0">has planted <span style="color:#658fca">'.$total_trees.'</span> trees with Food For Thought, an<br> 
                environmental programme which is operated by<br>
                the Green Earth Appeal</p>
                <p style="font-family:Tahoma; font-weight: bold; font-size: 26px; color:green; text-align: center; margin:0">As a member of Food For Thought <span style="color:#658fca">'.$restaurant.'</span><br>gives diners the opportunity to:</p>';
}

$pdf->writeHTML($pct_header, true, false, true, false, '');

// write footer
//$pdf->SetY(775);
//$pdf->SetX(10);
$pct_footer = '<span style="font-family:Tahoma; font-size: 23px; color:green;">The Green Earth Appeal wishes to thank '.$restaurant.' for their commitment to reduce carbon dioxide levels in the atmosphere and for their support of the Food For Thought programme</span>';
//$pdf->writeHTML($pct_footer, true, false, true, false, '');

$pdf->SetY(890);
$pdf->SetX(320);
//$pdf->SetFont('times', '', 48);
$html3 = '<span style="font-size: 16px; color: #171213">'.date("jS F Y").'</span>';
$pdf->writeHTML($html3, true, false, true, false, '');

$pdf->writeHTMLCell($w=370, $h=0, $x='55', $y='740', $pct_footer, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//Close and output PDF document
if ($cert_path) {
    $pdf->Output( $cert_path.$cert_name, 'F');
} else {
    $pdf->Output(dirname(__FILE__).'/../uploads/certs/'.$cert_name, 'F');   
}