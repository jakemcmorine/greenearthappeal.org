<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__).'/tcpdf/config/lang/eng.php');
require_once(dirname(__FILE__).'/tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
if (!class_exists('MYPDF1')) {
    class MYPDF1 extends TCPDF {
		
		//Page header
    	public function Header() {
    		// get the current page break margin
    		$bMargin = $this->getBreakMargin();
			$this->tcpdflink = false;
    		// get current auto-page-break mode
    		$auto_page_break = $this->AutoPageBreak;
    		// disable auto-page-break
    		$this->SetAutoPageBreak(false, 0);
    		// set bacground image
    		//$img_file = K_PATH_IMAGES.'bg.jpg';
            //$img_file = 'bg.jpg';
            //$this->Image($img_file, 0, 0, 1200, 1772, '', '', '', false, 300, '', false, false, 0);
            //$this->Image($img_file, 0, 0, 757, 1118, '', '', '', false, 300, '', false, false, 0);
            
    		// restore auto-page-break status
    		$this->SetAutoPageBreak($auto_page_break, $bMargin);
    		// set the starting point for the page content
    		$this->setPageMark();
    	}
		public	$last_page_flag = true;	
			public function Close() {
				$this->last_page_flag = true;
				parent::Close();
			}
			// Page footer
			public function Footer() {
				$tpages = $this->getAliasNbPages();
				$pages = $this->getAliasNumPage();
				// Position at 15 mm from bottom
				if($this->last_page_flag){
					$this->SetY(-30);
					$this->SetFont('helvetica', 'B', 9);
					$this->Cell(0, 10, "Hollinwood Business Centre - Albert St - Oldham - OL8 3QL", 0, false, 'C', 0, '', 0, false, 'T', 'M');
					$this->SetY(-20);
					$this->SetFont('helvetica', '', 9);
					// Page number
					//$this->Cell(0, 10, "t: 0208 798 0476 - f: 0871 245 8075 - e: billing@greenearthappeal.org", 0, false, 'C', 0, '', 0, false, 'T', 'M');
					
					if(isset($_SESSION['gocardless_user']) && $_SESSION['gocardless_user'] == "new"){
						$this->writeHTML('<p style="text-align:center;"><b>t:</b> 0208 798 0476 - <b>f:</b> 0871 245 8075 - <b>e:</b> billing@carbonfreedining.org</p>', true, 'C', true, false, '');
					}else{
						$this->writeHTML('<p style="text-align:center;"><b>t:</b> 0208 798 0476 - <b>f:</b> 0871 245 8075 - <b>e:</b> billing@greenearthappeal.org</p>', true, 'C', true, false, '');
					}
				}
			}
    }
    
}

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pageLayout = array(757, 600);
$pageLayout = array(600, 1000);
$pdf = new MYPDF1('p', 'px', $pageLayout, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Atul Rana');
$pdf->SetAuthor('Marvin');
$pdf->SetTitle('Request for Funds');
$pdf->SetSubject('');
$pdf->SetKeywords('php programmer, phpcodeteam@gmail.com');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(0, 60, 0, true);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(TRUE);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
//$pdf->SetFont('times', '', 48);
$pdf->SetFont('helvetica', '', 10);
// add a page
$pdf->AddPage();
//$pdf->SetY(150);
//$pdf->SetX(20);

// create invoice table
// $invoice_number = date('dm').$invoice_id;
//$invoice_name = 'test_invoice.pdf';
$pct_str = '';
// list items for bulk partner
$month_of_plant = date("F", strtotime("last day of previous month") );
$pct_trees = 0;
if ( count($pct_rows) ) {
	//echo "<pre>"; print_r($pct_rows); die("here");
    foreach ($pct_rows AS $item) {
        $item = (array)$item;
		//echo "<pre>"; print_r($item); die("here");
		if(isset($_SESSION['login']['currency'])){
		   switch ( $_SESSION['login']['currency'] ) {
				case 1:
					$currency = '$';
					break;
				case 2:
					$currency = '&pound;';
					break;
				case 3:
					$currency = '€';
					break;
			}
		}
		if(isset($_SESSION['login']['currency'])){
			$unit = $_SESSION['login']['price']/100;
		}else{
			$unit = $item['price']/100;
		}
		
        $pct_price = number_format($item['tree_nums'] * $unit, 2);
		$pct_trees += $item['tree_nums'];
		$item_company_name = stripslashes($item['company']);
		if($item['restaurant_date']!=""){
			$restaurant_date = '('.$item['restaurant_date'].')';
		}else{
			$restaurant_date = '';
		}
        $pct_str .= "<tr>  
                        <td  valign=\"left\" style=\"line-height:2; text-align:left;\">{$item_company_name}&nbsp;{$restaurant_date}</td>
                        <td valign=\"middle\" style=\"line-height:2;\" align=\"right\">{$item['tree_nums']}&nbsp;</td>
                        <td valign=\"middle\" style=\"line-height:2;\" align=\"right\">{$currency}{$pct_price}</td>
                    </tr>";
    }
}

$gocardless_logo = '<img width="150" height="30" src="libraries/imgs/gocardless.jpg" />';

if(isset($gocardless_link) && $gocardless_link){
			if(isset($_SESSION['gocardless_user']) && $_SESSION['gocardless_user'] == "new"){
				
				$gocardless_bank_address = '<td style="text-align:center"><p><i style="font-size:11px;">alternatively, please remit funds to</i><br><i><b>Carbon Free Dining</b></i><br> <i style="font-size:11px;">c/o Lloyds Bank<br>
				AC 84285368 SC 30-98-97<br>IBAN GB56LOYD30989784285368<br>BIC LOYDGB21031</i></p></td>';
				 
			}else{
				$gocardless_bank_address = '<td style="text-align:center"><p><i style="font-size:11px;">alternatively, please remit funds to</i><br><i><b>The Green Earth Appeal</b></i><br> <i style="font-size:11px;">c/o Lloyds TSB, P.O.Box 1000, BX1 1LT<br>
				AC 11418860 SC 77-04-05</i></p></td>';
			}
			$gocardless_url=  '<table>
			<tr>
			   <td style="text-align:center"><b style="text-align:center;font-size:11px;">PAYMENT ADVICE</b></td>
			</tr>
			<tr>
		        <td style="text-align:center"><i><b style="text-align:center;">Click here to</i></b></td>
            </tr>
            <tr>
			  <td style="text-align:center"><a style="color: #000; #4cae4c;display: inline-block;font-size: 14px; text-decoration: none; white-space: nowrap; " href="'.$gocardless_link.'">'.$gocardless_logo.'</a></td>
            </tr>
			<tr>
				'.$gocardless_bank_address.'
			</tr>
            </table>' ; 
	
}else{
	 $gocardless_url=  '<table style="padding:5px 13px;">
			<tr>
			   <td style="text-align:center"><b style="text-align:center;font-size:15px;color: red;">TAKE NO ACTION</b></td>
			</tr>
			<tr>
			   <td><p style="font-size:11px;">This request for funds is for information only, no action is required.</p></td>
			</tr>
			<tr>
			   <td><p style="font-size:11px;">An automatic request for payment has been processed via your requested payment method.</p></td>
			</tr>
            </table>'; 
}

$price_pt = number_format($item2['price'], 2);
$date_text = date("d F Y", strtotime("last day of previous month") );
$pctdate = '<span style="font-size: 16px; color: #171213">'. $date_text .'</span>';
$html3 = '<table>
            <tr>
                <td>Date of Request</td>
                <td>'.$date_text.'</td>
            </tr>
            <tr>
                <td>Request ID</td>
                <td><b>'.$invoice_number.'</b></td>
            </tr>
        </table>';
		
$tbl = <<<EOD
<div>
	<!--h3 style="color:green; font-size:18px; margin:0; padding:0">Green Earth Appeal</h3>
    <b>switchboard</b>&nbsp;&nbsp; 0208 798 0476<br />
    <b>fax &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b> 0871 245 8075<br />
    <b>email </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; billing@greenearthappeal.org <br /><br />
    Hollinwood  Business Centre,<br />
    Albert St,<br />
    Oldham,<br />
    OL8 3QL -->
 
    <div style="margin-top:50px;">
        <h3>Request for Funds</h3>
		<b>$invoice_company</b><br />
			Date of Request &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $date_text <br />
			Request ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>$invoice_number</b>
    </div>
    <div style="margin-top:50px;">
        <i>Reference &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>$ini_first_name</b></i>
    </div>
    <div style="margin-top: 50px;">
        <table align="center" valign="middle">
            <tr>
				<th width="300" align="left"><b>Trees planted in $month_of_plant</b></th>
                <th width="88" align="right"><b>Trees</b></th>
                <th width="157" align="right"><b>Amount</b></th>
            </tr>
            $pct_str
        </table>
    </div>
</div>
EOD;

// expend
$tax = number_format($tax * $item2['price']/100, 2);

$tbl3 = '<table border="0" align="right">
            <tr>
                <td style="border-top: .5px solid gray;" width="75" align="left">Total</td>
                <td style="border-top: .5px solid gray;" width="75">'.$pct_trees.'</td>
                <td style="border-top: .5px solid gray; " align="right" width="155">'.$total.'</td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: .5px solid gray;">&nbsp;</td>
            </tr>
            <tr>
                <td style="border-top: .5px solid gray; border-bottom: .5px solid gray;" align="left"><b>Total Due</b></td>
                <td style="border-top: .5px solid gray; border-bottom: .5px solid gray;"></td>
                <td style="border-top: .5px solid gray; border-bottom: .5px solid gray;" align="right">'.$total.'</td>
            </tr>
        </table><br><br><br><br>';
$payment_btn =	$gocardless_url;
			
$pct_p = '<p style="text-align:center;font-size: 12px;"><i>Please remit funds to \'Green Earth Appeal\' c/o Lloyds TSB, P.O.Box 1000, BX1 1LT<br>
            AC 11418860 SC 77-04-05</i></p>';

$pct_logo = '<img src="libraries/imgs/logo-green1.jpg" />';

//$pdf->writeHTML($tbl, true, false, true, false, '');
 
//$pdf->SetY(875);
//$pdf->SetX(290);
//$pdf->SetFont('times', '', 48);

if(isset($_SESSION['gocardless_user']) && $_SESSION['gocardless_user'] == "new"){
	$Rounded_height = "126";
}else{
	$Rounded_height = "110";
}

$pdf->writeHTMLCell($w=250, $h=0, $x='80', $y='50', $pct_logo, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->writeHTMLCell($w=200, $h=300, $x='332', $y='110', $payment_btn,  $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->SetLineStyle(array('width' => 2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
$pdf->RoundedRect(332,107, 200, $Rounded_height, 5, '1111', '');


//$pdf->writeHTMLCell($w=0, $h=0, $x='350', $y='280', $html3, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->writeHTMLCell($w=400, $h=0, $x='80', $y='120', $tbl, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//$pdf->writeHTMLCell($w=300, $h=0, $x='371', $y='460', $tbl3, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//$pdf->writeHTMLCell($w=300, $h=0, $x='371', $y='0', $tbl3, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//$pdf->writeHTMLCell($w=0, $h=0, $x='40', $y='540', $pct_p, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

// $pdf->writeHTML($tbl3, true, false, true, false, 'R');
// $pdf->writeHTML($pct_p, true, false, true, false, 'C');
$pdf->writeHTMLCell(200, 0, 280, '', $tbl3, 0, 1, 0, true, '', true);
//$pdf->writeHTMLCell(280, 0, 200, '', $payment_btn, 0, 1, 0, true, 'bottom', true);
//$pdf->writeHTML($pct_p, true, false, true, false, '');
//Close and output PDF document
$pdf->Output(dirname(__FILE__).'/../uploads/request_for_funds/'.$invoice_name, 'F');

//============================================================+
// END OF FILE
//============================================================+