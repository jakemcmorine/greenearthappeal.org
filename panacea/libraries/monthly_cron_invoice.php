<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__).'/tcpdf/config/lang/eng.php');
require_once(dirname(__FILE__).'/tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
if (!class_exists('MYPDF1')) {
    class MYPDF1 extends TCPDF {
    	//Page header
    	public function Header() {
    		// get the current page break margin
    		$bMargin = $this->getBreakMargin();
			$this->tcpdflink = false;
    		// get current auto-page-break mode
    		$auto_page_break = $this->AutoPageBreak;
    		// disable auto-page-break
    		$this->SetAutoPageBreak(false, 0);
    		// set bacground image
    		//$img_file = K_PATH_IMAGES.'bg.jpg';
            //$img_file = 'bg.jpg';
            //$this->Image($img_file, 0, 0, 1200, 1772, '', '', '', false, 300, '', false, false, 0);
            //$this->Image($img_file, 0, 0, 757, 1118, '', '', '', false, 300, '', false, false, 0);
            
    		// restore auto-page-break status
    		$this->SetAutoPageBreak($auto_page_break, $bMargin);
    		// set the starting point for the page content
    		$this->setPageMark();
    	}
    }
    
}

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pageLayout = array(757, 600);
$pageLayout = array(800, 660);
$pdf = new MYPDF1('p', 'px', $pageLayout, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Vinh Nguyen');
$pdf->SetAuthor('Marvin');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('');
$pdf->SetKeywords('php programmer, phpcodeteam@gmail.com');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(0, 60, 0, true);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
//$pdf->SetFont('times', '', 48);
$pdf->SetFont('helvetica', '', 12);
// add a page
$pdf->AddPage();
//$pdf->SetY(150);
//$pdf->SetX(20);

// create invoice table
// $invoice_number = date('dm').$invoice_id;
//$invoice_name = 'test_invoice.pdf';
$pct_str = '';
// list items for bulk partner

if ( count($pct_rows) ) {
    foreach ($pct_rows AS $item) {
        $item = (array)$item;
		//echo "<pre>"; print_r($item); die("here");
     
		$unit = $item['price']/100;
        $pct_price = number_format($item['tree_nums'] * $unit, 2);
        $pct_str .= "<tr>
                        <td colspan=\"2\" valign=\"middle\" style=\"height:10px\" align=\"left\">{$item['company']}</td>
                        <td valign=\"middle\" style=\"border-left: .5px solid gray; padding-right:3px\" align=\"right\">{$item['tree_nums']} &nbsp;&nbsp;</td>
                        <td valign=\"middle\" style=\"border-left: .5px solid gray; padding-right:3px\" align=\"right\">{$item['free_trees']} &nbsp;&nbsp;</td>
                        <td valign=\"middle\" style=\"border-left: .5px solid gray; padding-right:3px\" align=\"right\">{$currency}{$unit} &nbsp;&nbsp;</td>
                        <td valign=\"middle\" style=\"border-left: .5px solid gray;\" align=\"right\">{$currency}{$pct_price}</td>
                    </tr>";
    }
    $pct_str .= '<tr>
        <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>';
}

$price_pt = number_format($item2['price'], 2);
$tbl = <<<EOD
<div>
    <h3 style="color:green; font-size:18px; margin:0; padding:0">Green Earth Appeal</h3>
    <br />
    Hollinwood  Business Centre,<br />
    Albert St,<br />
    Oldham,<br />
    OL8 3QL<br /><br />
	<b>switchboard</b> 020 8798 0476 &nbsp;&nbsp; - &nbsp;&nbsp; <b>fax</b> &nbsp; 0871 245 8075<br />
    <b>email </b>accounts@greenearthappeal.org <br /><br />
    
    <div style="margin-top:50px;">
        <h3>INVOICE</h3>
        <b>$invoice_company</b><br />
        <b>$address</b><br />
        <b>$city</b><br />
        <b>$state</b><br />
        <b>$post_code</b><br />
        <!-- <b>RFC: SEN060807L59</b><br /> -->
    </div>
    <div style="margin-top:50px;">
        <i>Reference &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Fruit Tree Planting Donations</b></i>
    </div>
    <div style="margin-top: 50px;">
        <table align="center" valign="middle">
            <tr>
                <th colspan="2" width="30%"><b>Item</b></th>
                <th style="border-left: .5px solid gray;"><b>Units</b></th>
                <th style="border-left: .5px solid gray;"><b>Free Trees</b></th>
                <th style="border-left: .5px solid gray;"><b>Price Per Unit</b></th>
                <th style="border-left: .5px solid gray;"><b>Price &nbsp;&nbsp;</b></th>
            </tr>
            $pct_str
            <tr>
                <td colspan="2" valign="middle" align="left">{$item2['name']}</td>
                <td valign="middle" style="border-left: .5px solid gray;" align="right">{$item2['tree_nums']} &nbsp;&nbsp;</td>
                <td valign="middle" style="border-left: .5px solid gray;" align="right">{$item2['free_trees']} &nbsp;&nbsp;</td>
                <td valign="middle" style="border-left: .5px solid gray;" align="right">{$item2['currency']}{$item2['unit']}  &nbsp;&nbsp;</td>
                <td valign="middle" style="border-left: 0.5px solid gray;" align="right">{$item2['currency']}{$price_pt}</td>
            </tr>
        </table>
    </div>
</div>
EOD;

// expend
$tax = number_format($tax * $item2['price']/100, 2);

$date_text = date("d F Y", strtotime("last day of previous month") );
$pctdate = '<span style="font-size: 16px; color: #171213">'. $date_text .'</span>';
$html3 = '<table>
            <tr>
                <td>Date of Invoice</td>
                <td>'.$pctdate.'</td>
            </tr>
            <tr>
                <td>Invoice Number</td>
                <td><b>'.$invoice_number.'</b></td>
            </tr>
        </table>';

$tbl3 = '<table>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <table>
                        <tr>
                            <td style="border-top: .5px solid gray; width:200px">Sub Total</td>
                            <td style="border-top: .5px solid gray; width: 100px" align="right">'. $item2['currency'].$price_pt .'</td>
                        </tr>
                        <tr>
                            <td style="border-top: .5px solid gray; width:200px">Tax</td>
                            <td style="border-top: .5px solid gray; width: 100px" align="right">'.$item2['currency'].$tax.'</td>
                        </tr>
                        <tr>
                            <td style="border-top: .5px solid gray; width:200px">Total</td>
                            <td style="border-top: .5px solid gray; width: 100px" align="right">'.$total.'</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border-top: .5px solid gray;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="border-top: .5px solid gray; border-bottom: .5px solid gray;"><b>Total Due</b></td>
                            <td style="border-top: .5px solid gray; border-bottom: .5px solid gray;" align="right">'.$total.'</td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>';

$pct_p = '<p style="text-align:center">Please remit funds to \'Green Earth Appeal\'<br />		
            BACS Payment: Sort Code 77-04-05 Account number 11418860<br />
            IBAN: GB84LOYD77040511418860<br />
            Swift: LOYDGB21T12<br />	
            Cheque: Post to address above</p>';

$pct_logo = '<img src="libraries/imgs/logo-green.jpg" />';
//$pdf->writeHTML($tbl, true, false, true, false, '');

//$pdf->SetY(875);
//$pdf->SetX(290);
//$pdf->SetFont('times', '', 48);

$pdf->writeHTMLCell($w=200, $h=0, $x='450', $y='50', $pct_logo, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->writeHTMLCell($w=0, $h=0, $x='420', $y='220', $html3, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->writeHTMLCell($w=600, $h=0, $x='25', $y='20', $tbl, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//$pdf->writeHTMLCell($w=300, $h=0, $x='371', $y='460', $tbl3, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//$pdf->writeHTMLCell($w=300, $h=0, $x='371', $y='0', $tbl3, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//$pdf->writeHTMLCell($w=0, $h=0, $x='40', $y='540', $pct_p, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

$pdf->writeHTML($tbl3, true, false, true, false, 'R');
$pdf->writeHTML($pct_p, true, false, true, false, 'C');
//Close and output PDF document
$pdf->Output(dirname(__FILE__).'/../uploads/invoices/'.$invoice_name, 'F');

//============================================================+
// END OF FILE
//============================================================+