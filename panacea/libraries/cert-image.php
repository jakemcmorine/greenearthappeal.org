<?php
if($bk_image!=""){
		$img_file = 'libraries/imgs/'.$bk_image;
	}else{
		$img_file = 'libraries/imgs/bg-cert.png';
	}
	
// Create Image From Existing File
if($bk_image!=""){
	$pos = strpos($bk_image, "jpg");
	if($pos !=""){
		$png_image = imagecreatefromjpeg($img_file);
	}else{
		$png_image = imagecreatefrompng($img_file);
	}
}else{
	$png_image = imagecreatefrompng($img_file);
}

// Allocate A Bachground Color For The Text - TRANSPARENT
$bg_color = imagecolorallocatealpha($png_image, 255, 255, 255, 127);

// Allocate A Color For The Text - GREEN
$font_color = imagecolorallocate($png_image, 22, 105, 54);

$font_color_light = imagecolorallocate($png_image, 12, 124, 14);

$font_color5 = imagecolorallocate($png_image, 1, 0, 0);

// Set Path to Font File
$font_pathbold = 'libraries/certificate-fonts/tahomabd.ttf';
$font_path = 'libraries/certificate-fonts/Tahoma-Regular-font.ttf';

imagesavealpha($png_image, true);
imagefill($png_image, 0, 0, $bg_color);

// Print Text On Image
$text1 = strtoupper($certificate_mess1);
$text2 = $certificate_mess2;
$text3 = $certificate_mess3;
$text4 = $certificate_mess4;
$text5 = date("jS F Y");

$chklen = strlen($text1);
if ($chklen > 26) {
		$dist = 10;
} else if ($chklen >= 24 && $chklen < 26) {
		$dist = 20;
}else if ($chklen >= 19 && $chklen < 24) {
		$dist = 30;
} else if ($chklen >= 14 && $chklen < 19) {
		$dist = 35;
} else if ($chklen >= 10 && $chklen < 14) {
		$dist = 50;
} else if ($chklen >= 8 && $chklen < 10) {
		$dist = 55;
}else if ($chklen >= 5 && $chklen < 8) {
		$dist = 60;
} else if ($chklen >= 1 && $chklen < 5) {
		$dist = 70;
}else{
	$dist = 10;
}


$text1 = wordwrap($text1, 40, "\n", TRUE);

$text2 = wordwrap($text2, 70, "\n", TRUE);

$text3 = wordwrap($text3, 65, "\n", TRUE);

$text4 = wordwrap($text4, 45, "\n", TRUE);

$backwidth = 1200;
$backwidth1 = 1000;
$posx = 120;

if($create_thumb_certificate !=0){
	$posx1 = 100;
}else{
	$posx1 = 70;
}

$angle = 0;
$size = 24;
$verticaltxtspace = $backwidth - (2 * $posx);       
$verticaltxtspace1 = $backwidth1 - (2 * $posx1);       
$spacepositions = imagettfbbox($size, $angle, $font_path, " ");        
$spacepx = $spacepositions[4] - $spacepositions[0];       


// Split text in lines
$lines = split("[\n]", $text1);       
for($count = 0; $count < count($lines); $count++)
{
	$textpositions = imagettfbbox($size, $angle,$font_path, $lines[$count]);           
	$textpx = $textpositions[2] - $textpositions[0];
	$spaces = (($verticaltxtspace1 - $textpx)/2) / $spacepx;
	
	// Add spaces
	$line = $lines[$count];
	for($i = 0; $i < $spaces; $i++)
	{
		$line = " " . $line;
	}
	$lines[$count] = $line;
}

// Create new text of lines
$text1 = "";
for($count = 0; $count < count($lines); $count++)
{
	$text1 .= $lines[$count] . "\r\n";
}   

/* tex3 */
// Split text in lines
$lines = split("[\n]", $text3);       
for($count = 0; $count < count($lines); $count++)
{
	$textpositions = imagettfbbox($size, $angle,$font_path, $lines[$count]);           
	$textpx = $textpositions[2] - $textpositions[0];
	$spaces = (($verticaltxtspace - $textpx)/2) / $spacepx;
	
	// Add spaces
	$line = $lines[$count];
	for($i = 0; $i < $spaces; $i++)
	{
		if($count == 2){
			 if($i == 5 || $i == 6 || $i == 7){
				$line = "   " . $line;
			 }else{
				 $line = " " . $line;
			 }
		}else{
			$line = " " . $line;
		}
	}
	$lines[$count] = $line;
}

// Create new text of lines
$text3 = "";
for($count = 0; $count < count($lines); $count++)
{
	$text3 .= $lines[$count] . "\r\n";
}   

/* tex2 */
// Split text in lines
$lines = split("[\n]", $text2);       
for($count = 0; $count < count($lines); $count++)
{
	$textpositions = imagettfbbox($size, $angle,$font_path, $lines[$count]);           
	$textpx = $textpositions[2] - $textpositions[0];
	$spaces = (($verticaltxtspace - $textpx)/2) / $spacepx;
	
	// Add spaces
	$line = $lines[$count];
	for($i = 0; $i < $spaces; $i++)
	{
		$line = " " . $line;
	}
	$lines[$count] = $line;
}

// Create new text of lines
$text2 = "";
for($count = 0; $count < count($lines); $count++)
{
	$text2 .= $lines[$count] . "\r\n";
} 


imagettftext($png_image, 36, 0, $dist, 390, $font_color, $font_pathbold, $text1);
imagettftext($png_image, 24, 0, 170, 510, $font_color_light, $font_path, $text2);
imagettftext($png_image, 24, 0, 130, 650, $font_color_light, $font_pathbold, $text3);
imagettftext($png_image, 23, 0, 100, 1350, $font_color_light, $font_path, $text4);
imagettftext($png_image, 16, 0, 566, 1605, $font_color5, $font_path, $text5);

$fname = 'cert/full_'.$cert_save_path.'.jpg';

//Send Image to Browser
//header('Content-type: image/jpeg');
//imagejpeg($png_image);

imagejpeg($png_image, $fname);               //to save image

if($create_thumb_certificate !=0){
	/* to resize image for thumbnail certificate */
	$tmp = imagecreatetruecolor(300, 443); 
	$thumbimage = imagecreatefromjpeg($fname);
	list($width, $height) = getimagesize($fname);
	imagecopyresampled($tmp, $thumbimage, 0, 0, 0, 0, 300, 443, $width, $height); 
	$filename = 'cert/thumb_'.$cert_save_path.'.jpg';
	imagejpeg($tmp, $filename, 100); 
}
// Clear Memory
imagedestroy($png_image);


