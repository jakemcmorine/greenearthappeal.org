<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__).'/tcpdf/config/lang/eng.php');
require_once(dirname(__FILE__).'/tcpdf/tcpdf.php');
require_once(dirname(__FILE__).'/fpdi/fpdi.php');

class PDF extends FPDI {
 /**
 * "Remembers" the template id of the imported page
 */
 var $_tplIdx;
 /**
 * include a background template for every page
 */
 
 var $pdf_path = '';
 
 function Header() {
 if (is_null($this->_tplIdx)) {
	 $pdf_path = dirname(__FILE__).'/../uploads/solicitor/';	 
	 $this->numPages = $this->setSourceFile($pdf_path.'Equitable_Law_Template_Latest.pdf');
	 $this->_tplIdx = $this->importPage(1);
 }
 
 $this->useTemplate($this->_tplIdx);
 $this->SetTextColor(255);
 $this->SetXY(60.5, 24.8);
// $this->Cell(0, 12, "TCPDF and FPDI");
 $this->Cell(0, 12, "");
 }
 
function Footer() {}
}
 
// initiate PDF
$pdf = new PDF();
$pdf->SetMargins(PDF_MARGIN_LEFT, 43, PDF_MARGIN_RIGHT);
$pdf->SetAutoPageBreak(true, 40);
$pdf->setFontSubsetting(false);
$pdf->SetFont('freesans', '', 10);

// $ref_Date = date('l, d F Y', time());
// $our_ref  = "DRJ / Green Earth Appeal";
// $your_ref  = $your_ref;
// add a page
$pdf->AddPage();
$pdf->SetY(40);
$pdf->SetX(11);
$pdf->WriteHTML($pdftext_all_page1);
/* $pdf->writeHTMLCell($w=370, $h=0, $x='142', $y='54', $ref_Date, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->writeHTMLCell($w=370, $h=0, $x='150', $y='62', $our_ref, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->writeHTMLCell($w=370, $h=0, $x='182', $y='70', $your_ref, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true); */
$pdf->endPage();

$pdf->_tplIdx = $pdf->importPage(2);
$pdf->AddPage();

$pdf->SetY(20);
$pdf->SetX(11);
$pdf->WriteHTML($pdftext_all_page2);

$save_path = dirname(__FILE__).'/../uploads/solicitor/';
$newname = 'Equitable_Law_'.$user_id.'.pdf';  
$pdf->Output($save_path.$newname, 'F');