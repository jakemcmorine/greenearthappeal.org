<?php 

$pdftext_all_page1 = '<table width="98%" style="margin-left:10px;">
	<tr><td>&nbsp;</td></tr>
	<tr>
    	<td width="2%">&nbsp;</td>
		<td width="48%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
        <td width="35%">&nbsp;</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
	<tr>
    	<td>&nbsp;</td>
		<td><strong>STRICTLY PRIVATE AND CONFIDENTIAL<br />To Be Opened By Addressee Only</strong></td>
        <td align="right">Date:</td>
        <td align="right" width="37%">[pdftext_sentdate]</td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
		<td></td>
        <td align="right" width="17.6%">Our Ref:</td>
        <td align="right" width="34.5%">DRJ / Green Earth Appeal</td>
    </tr>
	<tr><td>&nbsp;</td></tr>
	 <tr>
    	<td>&nbsp;</td>
		<td>The Sole Director,<br /><strong>The Green Earth Appeal</strong><br />Hollinwood Business Centre,<br />Albert Street,<br />Oldham<br />Lancashire<br />OL8 3QL</td>
        <td align="right" width="18.4%">Your Ref:</td>
        <td align="right" width="33.8%">[your_ref]</td>
    </tr>
	<tr><td>&nbsp;</td></tr>
    <tr>
		<td>&nbsp;</td>
    	<td>And To:-</td>
        <td colspan="2">&nbsp;</td>
    </tr>
	<tr><td>&nbsp;</td></tr>
    <tr>
    	<td>&nbsp;</td>
		<td><i><strong>F.A.O. [pdftext_foa]</strong></i></td>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
		<td>&nbsp;</td>
    	<td>[pdftext_comp]<br />[pdftext_add]<br />[pdftext_city]<br />[pdftext_state]<br />[pdftext_postcode]</td>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
    	<td>&nbsp;</td>
		<td>By email only</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
	<tr>
    	<td>&nbsp;</td>
		<td>Dear Sir(s),</td>
        <td colspan="2">&nbsp;</td>
    </tr>
	<tr><td>&nbsp;</td></tr>
    <tr>
    	<td>&nbsp;</td>
		<td colspan="3"><strong><u>RESULT OF LEGAL DUE DILIGENCE INTO THE GREEN EARTH APPEAL BUSINESS MODEL PLUS SPECIFICS OF YOUR RECENT TRANSACTION</u></strong></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
    	<td>&nbsp;</td>
		<td colspan="3">We are English Solicitors instructed on behalf of The Green Earth Appeal, an English private company limited by guarantee (Company Registration Number 07745907) which we understand operates a not for profit business model, and which is known colloquially as <strong>‘Green Earth Appeal’</strong>.</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
    <tr>
    	<td>&nbsp;</td>
		<td colspan="3">We have verified to our satisfaction that Green Earth Appeal provides a service to their clients which primarily comprises tree planting with the proceeds of payments made to Green Earth Appeal by its clients, with secondary goals of providing education about agroforestry (ensuring maximum yields to the communities who plant the trees) and providing local communities with the infrastructure to continue planting trees in the future (e.g. tools and tree nurseries).</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
    <tr>
    	<td>&nbsp;</td>
		<td colspan="3">We are satisfied that Green Earth Appeal achieves this through an international network of charities and non- government organisations (NGOs) who work directly with these communities in the developing world.</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
    <tr>
    	<td>&nbsp;</td>
		<td colspan="3">We are further satisfied that Green Earth Appeal is a tree planting partner of the United Nations Environment Programme (UNEP) ‘Trillion Tree Campaign’(<a style="color: #c00000;" href="https://www.trilliontreecampaign.org/t/green-earth-appeal">https://www.trilliontreecampaign.org/t/green-earth-appeal</a>)</td>
	</tr>
	</table>';
?>