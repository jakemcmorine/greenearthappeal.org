<?php 		 
	echo $post_json = '{ "id": "webhook event id",  "object":"donation",  "type":"donation.created",  "data": { "id":1,"price":10,"tax":2,"total":12,"giftAid":0,"currency":"GBP" }}'; 
	
	$endpoint = 'https://greenearthappeal.org/panacea/index.php/donation/post_donation_data';
	$ch = @curl_init($endpoint);
	@curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
	@curl_setopt($ch, CURLOPT_POST, true);
	@curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
	@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = @curl_exec($ch);
	$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$curl_errors = curl_error($ch);
	@curl_close($ch);
	echo "<br>";
	if($curl_errors!=""){
		echo "Errors: " . $curl_errors;
	}else{
		echo "<h3>\nResponse: </h3>" . $response;
	}
