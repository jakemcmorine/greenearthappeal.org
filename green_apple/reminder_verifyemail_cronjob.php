<?php 
/* 
Cron job to check email status after one hours and send verify email after every 48 hours 
*/
require_once('../wp-load.php');
global $wpdb;

$tablename = $wpdb->prefix.'optin_users_data';
			
$result = $wpdb->get_results( 'SELECT * FROM '.$tablename.' WHERE `ip_address_step1`!= "" AND email_verified="0" AND unsubscribe="0" AND accept_tc="1"' , OBJECT );

foreach($result as $res){
	
	$current_time = date('Y-m-d H:i:s');
	$total_hours = round((strtotime($current_time) - strtotime($res->to_count_email_hours))/3600);
	
	if($total_hours == '1'){
		
		$verify_email_link = 'https://greenearthappeal.org/free-tree/'. $res->unique_url . '/?verify_email=' . $res->verify_email_address_key;
		$unsubscribe_link = 'https://greenearthappeal.org/free-tree/'. $res->unique_url . '/?unsubscribe_email=' . $res->verify_email_address_key;
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: The Green Organisation <partners@greenearthappeal.org>' . "\r\n";
		
		$message .='Hi '.$res->first_name.','."<br><br>";
		$message .= "Click on the link below to verify your email address:".'<br><br>';
		$message .= "Verify Email Link: ".$verify_email_link.'<br><br>';
		$message .= "Click on the link below if you want to unsubscribe from these emails:".'<br><br>';
		$message .= "Unsubscribe Email Link: ".$unsubscribe_link.'<br><br>';
		$message .= "Thanks".'<br><br>';
		$message .= "The Green Earth Appeal in Partnership with The Green Organisation".'<br><br>';
		@wp_mail($res->email,'Reminder verify your email address', $message, $headers); 
		
		// to update time after 48 hours
		$update = $wpdb->update( 
						$tablename , 
						array( 						
							'to_count_email_hours' => $current_time	
						), 
						array( 'id' => $res->id ));
		echo 'Reminder email sent to '.$res->email.'</br>';
	}
	//echo "<pre>"; print_r($res); die;
}
