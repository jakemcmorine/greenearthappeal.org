<?php
$remoteURL = "https://www.greenearthappeal.org/panacea/cert/full_".$_GET['imgurl'];

// Force download
header("Content-type: application/x-file-to-save"); 
header("Content-Disposition: attachment; filename=".basename($remoteURL));
ob_end_clean();
readfile($remoteURL);
exit;