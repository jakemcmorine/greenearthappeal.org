<?php 



function doReport($mailout) {
	require_once("../../phpsupport/wservices/conf.php");
	
	//Summary
	$s = beginPage();
	$s .= doSummary($cn);
	$summaryPart = $s;
	
	$sql = "SELECT id, isconversion, linktoresp, linktosource, DATE_FORMAT(createdate, '%W %D %M %Y %T') AS dtform FROM t_tracking_heads
			WHERE createdate BETWEEN NOW()-INTERVAL 24 HOUR AND NOW()
			ORDER BY createdate";
	$q = mysql_query($sql, $cn);
	
	while ($r = mysql_fetch_array($q)) {
		//Do Heading
		$s .= "<h3>Track Started: {$r['dtform']}<br />";
		$s .= "Track Ended: " . getTrackEnd($r['id'], $cn) . "</h3>";
		$s .= "<p>";
		$s .= "<strong>Source:</strong> " . getSourceInfo($r['linktosource'], $cn) . "<br />";
		
		if ($r['linktoresp'] == "0") {
			$s .= "<strong>Respondent:</strong> Not Set<br />";
		} else {
			$s .= "<strong>Respondent:</strong> " . getRespondentInfo($r['linktoresp'], $cn) . "<br />";
		}
		if ($r['isconversion'] == "0") {
			$s .= "<strong>Track Converted:</strong> No<br />";
		} else {
			$s .= "<strong>Converted:</strong> Yes<br />";
		}
		if ($r['linktoresp'] == "0") {
			$s .= "<strong>Referrals:</strong> N/A<br />";
		} else {
			$s .= "<strong>Referrals:</strong> " . getRespondentReferralCount($r['linktoresp'], $cn) . "<br />";
		}
		$s .= "</p>";
		//Do Map
		$s .= doMap($r['id'], $cn);
	}
	$s .= endpage();
	if ($mailout == true) {
		$summaryPart .= "<p><strong>THERE IS ADDITIONAL INFORMATION CONTAINED WITHIN THE FILE ATTACHED TO THIS EMAIL</strong></p>";
		doMail($summaryPart, $s);
	} else {
		echo $s;
	}
}

function doMail($mailpart, $filepart) {

	$subject = 		'Green Earth Appeal System Track';
	$bound_text = 	md5(date('r', time()));
	$bound = 		"--".$bound_text . "\r\n";
	$bound_last = 	"--".$bound_text . "--\r\n";
	
	$headers = 		"To: Internal Notifications <internalnotifications@greenearthappeal.org>\r\n";
	$headers .= 	"From: no-reply@greenearthappeal.org\r\n";
	$headers .= 	"MIME-Version: 1.0\r\n";
	$headers .= 	"Content-Type: multipart/mixed; boundary=\"$bound_text\"";
	
	$message = 		$bound;
	$message .= 	"Content-Type: text/html; charset=\"iso-8859-1\"\r\n";
	$message .= 	"Content-Transfer-Encoding: 7bit\r\n\r\n";
	$message .= 	$mailpart;
	
	//Generate The File
	$message .= 	"\r" . $bound;
	$message .= 	"Content-Type: text/html; name=\"Green Earth Appeal System Track.html\"\r\n";
	$message .=		"Content-Transfer-Encoding: base64\r\n";
	$message .=		"Content-disposition: attachment; file=\"Green Earth Appeal System Track.html\"\r\n";
	$message .=		"\r\n" . chunk_split(base64_encode($filepart));
	$message .= 	$bound_last;

	mail("internalnotifications@greenearthappeal.org", $subject, $message, $headers);
}

function doSummary($cn) {
	$s = "<img src=\"http://www.greenearthappeal.org/graphics/email_logo.jpg\" />&nbsp;<br />&nbsp;<br />";
	$s .= "<h1>System Track Report</h1>";
	$s .= "<h2>Period</h2>";
	$frm = mktime(date("H")-24, date("i"), date("s"), date("m"), date("d"), date("y"));
	$s .= "<p><strong>From:</strong> " . date("l jS F Y H:i:s", $frm ) . "<br />";
	$s .= "<strong>To:</strong> " . date("l jS F Y H:i:s") . "</p>";
	$s .= "<h2>Summary</h2>";
	$sql = "SELECT COUNT(*) nf FROM t_respondents WHERE dresponddate BETWEEN NOW()-INTERVAL 24 HOUR AND NOW()";
	$r = mysql_fetch_array(mysql_query($sql, $cn));
	$s .= "<p><strong>Respondents:</strong> {$r['nf']}<br />";
	$sql = "SELECT COUNT(*) nf FROM t_referals WHERE sreferraldate BETWEEN NOW()-INTERVAL 24 HOUR AND NOW()";
	$r = mysql_fetch_array(mysql_query($sql, $cn));
	$s .= "<strong>Referrals:</strong> {$r['nf']}<br />";
	$sql = "SELECT COUNT(*) nf FROM t_tracking_heads WHERE createdate BETWEEN NOW()-INTERVAL 24 HOUR AND NOW() AND isconversion=1";
	$r = mysql_fetch_array(mysql_query($sql, $cn));
	$s .= "<strong>Conversions:</strong> {$r['nf']}</p>&nbsp;<br />";
	return $s;
}

function getRespondentInfo($respID, $cn) {
	$sql = "SELECT stitle, sfirstname, slastname, semail, spostcode, sdayphone FROM t_respondents WHERE id={$respID}";
	$r = mysql_fetch_array(mysql_query($sql, $cn));
	$s = $r['stitle'] . " " . $r['sfirstname'] . " " . $r['slastname'] . " - " . $r['semail'] . " - " . strtoupper($r['spostcode']) . " - " . $r['sdayphone'];
	return $s;
}

function getSourceInfo($sourceID, $cn) {
	$sql = "SELECT sname from t_sponsors WHERE id={$sourceID}";
	$r = mysql_fetch_array(mysql_query($sql, $cn));
	if ($r['sname']=="b-l-a-n-k") {
		return "N/A";
	} else {
		return $r['sname'];
	}
}

function getRespondentReferralCount($respID, $cn) {
	$sql = "SELECT COUNT(*) AS nf FROM t_referals WHERE iparent={$respID}";
	$r = mysql_fetch_array(mysql_query($sql, $cn));
	return $r['nf'];
}

function getTrackEnd($id, $cn) {
	$sql = "SELECT DATE_FORMAT(actiondate, '%W %D %M %Y %T') AS dtended FROM t_tracking_actions WHERE trackparent={$id} ORDER BY trackstep DESC LIMIT 0,1";
	$r = mysql_fetch_array(mysql_query($sql, $cn));
	return $r['dtended'];
}

function doMap($id, $cn) {
	$sql = "SELECT * FROM t_tracking_actions WHERE trackparent = {$id} ORDER BY trackstep";
	$q = mysql_query($sql, $cn);
	$s = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
	while ($r = mysql_fetch_array($q)) {
		$s .= boxitem($r['trackitem'], $r['trackvalue']);
		$s .= boxarrow();
	}
	$s = substr($s, 0, strlen($s)-103);
	$s .= "</table>&nbsp;<br />&nbsp;<br />";
	return $s;
}

function boxitem($text, $resp) {
	if ($resp=="" || $resp == "-") {
		$s = "<td width=\"200\" class=\"actionbox\">{$text}<br /><img src=\"http://www.greenearthappeal.org/graphics/report_props/spacer.gif\" width=\"100\" height=\"1\" /></td>";
	} else {
		$s = "<td width=\"200\" class=\"actionbox\">{$text}<br /><span class=\"actionboxresp\">({$resp})</span><br /><img src=\"http://www.greenearthappeal.org/graphics/report_props/spacer.gif\" width=\"100\" height=\"1\" /></td>";
	}
	return $s;
}

function boxarrow() {
	$s = "<td><img src=\"http://www.greenearthappeal.org/graphics/report_props/arrow.gif\" width=\"30\" height=\"30\" /></td>";
	return $s;
}

function beginPage() {
	$s = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
		<html xmlns=\"http://www.w3.org/1999/xhtml\">
		<head>
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<title>Last 24 Hours</title>
		<style type=\"text/css\">
		<!--
		.actionbox {
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 10px;
			width: 200px;
			border: thin solid #000000;
			text-align:center;
			vertical-align:middle;
		}
		.actionboxresp {
			color:#990000;
		}
		p {
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 10px;
		}
		h3 {
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 14px;
			font-weight:bold;
			color:#990000;
		}
		h2 {
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 14px;
			font-weight:bold;
			color:#000000;
		}
		h1 {
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 16px;
			font-weight:bold;
			color:#990000;
		}
		-->
		</style>
		</head>
		
		<body>";
	return $s;
}

function endpage() {
	$s = "</body></html>";
	return $s;
}
	
