<?php 
require_once("../../phpsupport_sms/cl_database.php");
require_once("../../phpsupport_sms/cl_certs.php");
$cn = new cl_database();
$certs = new cl_certs($cn);
	if (isset($_GET['mobile'])) {
		//if it begins with zero we need to change to 44
		if (substr($_GET['mobile'], 0, 1) == "0") {
			$_GET['mobile'] = "44" . substr($_GET['mobile'], 1, strlen($_GET['mobile'])-1);
		}
	}
	
	if (!isset($_GET['mobile']) || !isset($_GET['code'])) {
   		$certs->outputPGTop();
		$certs->requestMobAndCert("");
	} else {
		if (!isset($_GET['nameoncert'])) {
			//Code and mob is set, check to see if its any good!
			$q = $certs->mobAndCodeAnyGood($_GET['mobile'], $_GET['code']);
			if ($q[0] == false) {
				$certs->outputPGTop();
				$certs->requestMobAndCert("There appears to be a problem with the mobile number or certificate number. Please re-enter them and try again."); 
				$certs->outputPGBottom();
			} else {
				if ($q[1] == false) {
					//Show Details form
					$certs->outputPGTop();
					$certs->requestCertDetails($_GET['mobile'], $_GET['code'], ""); 
					$certs->outputPGBottom();
				} else {
					//Produce certificate
					$certs->produceCertificate($_GET['mobile'], $_GET['code']);
				}
			}
		} else {
			//Save cert detail
			$sql = "UPDATE t_entitlements SET ";
			if (strlen($_GET['nameoncert'])>2) {
				$sql .= "nameoncert='" . $cn->sql_safe($_GET['nameoncert']) . "', ";
				if (strlen($_GET['emailaddress'])>2) {
					$sql .= "emailaddress='" . $cn->sql_safe($_GET['emailaddress']) . "', ";
				}
				$sql .= "listoptin='" . $cn->sql_safe($_GET['maillist']) . "', ";
				$sql = substr($sql, 0, strlen($sql)-2);
				$sql .= " WHERE mobilenumber='" . $cn->sql_safe($_GET['mobile']) . "' AND
						certid='" . $cn->sql_safe($_GET['code']) . "'";
				mysql_query($sql, $cn->getConnection());
				if (mysql_affected_rows($cn->getConnection())) {
					//OK To Continue
					$certs->produceCertificate($_GET['mobile'], $_GET['code']);
				} else {
					$certs->outputPGTop();
					$certs->requestCertDetails($_GET['mobile'], $_GET['code'], "An error occured while trying to save your certificate details. Please try again."); 
					$certs->outputPGBottom();
				}
			}
		}
	}
	?>
    