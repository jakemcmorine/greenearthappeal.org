<?php
defined('P_EXEC') or die('Denied access');

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
if (!class_exists('MYPDF1')) {
    class MYPDF1 extends TCPDF {
    	//Page header
    	public function Header() {
    		// get the current page break margin
    		$bMargin = $this->getBreakMargin();
    		// get current auto-page-break mode
    		$auto_page_break = $this->AutoPageBreak;
    		// disable auto-page-break
    		$this->SetAutoPageBreak(false, 0);
    		// set bacground image
    		//$img_file = K_PATH_IMAGES.'bg.jpg';
            //$img_file = 'bg.jpg';
            //$this->Image($img_file, 0, 0, 1200, 1772, '', '', '', false, 300, '', false, false, 0);
            //$this->Image($img_file, 0, 0, 757, 1118, '', '', '', false, 300, '', false, false, 0);
            
    		// restore auto-page-break status
    		$this->SetAutoPageBreak($auto_page_break, $bMargin);
    		// set the starting point for the page content
    		$this->setPageMark();
    	}
    }
    
}

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pageLayout = array(757, 600);
$pageLayout = array(800, 660);
$pdf = new MYPDF1('p', 'px', $pageLayout, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Vinh Nguyen');
$pdf->SetAuthor('Marvin');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('');
$pdf->SetKeywords('php programmer, phpcodeteam@gmail.com');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(0, 0, 0, true);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
//$pdf->SetFont('times', '', 48);
$pdf->SetFont('helvetica', '', 12);
// add a page
$pdf->AddPage();
//$pdf->SetY(150);
//$pdf->SetX(20);

// create invoice table
$pct_str = '';
foreach ($pct_rows AS $item) {
    $pct_price = (int)$item['client_trees']*.99;
    $pct_price = number_format($pct_price, 2);
    $pct_str .= "<tr>
                    <td colspan=\"2\" valign=\"middle\" style=\"height:10px\" align=\"left\">{$item['name']}</td>
                    <td valign=\"middle\" style=\"border-left: .5px solid gray; padding-right:3px\" align=\"right\">{$item['client_trees']} &nbsp;&nbsp;</td>
                    <td valign=\"middle\" style=\"border-left: .5px solid gray; padding-right:3px\" align=\"right\">\$0.99 &nbsp;&nbsp;</td>
                    <td valign=\"middle\" style=\"border-left: .5px solid gray;\" align=\"right\">\${$pct_price}</td>
                </tr>";
}

// new update for 500 trees
$pct_500 = ($units<500) ? (500-$units) : 0;
if ($pct_500) {
    $pct_price = $pct_500*.99;
    $pct_price = number_format($pct_price, 2);
    $pct_str .= "<tr>
                    <td colspan=\"2\" valign=\"middle\" style=\"height:10px\" align=\"left\">Sonnes Contribution</td>
                    <td valign=\"middle\" style=\"border-left: .5px solid gray; padding-right:3px\" align=\"right\">{$pct_500} &nbsp;&nbsp;</td>
                    <td valign=\"middle\" style=\"border-left: .5px solid gray; padding-right:3px\" align=\"right\">\$0.99 &nbsp;&nbsp;</td>
                    <td valign=\"middle\" style=\"border-left: .5px solid gray;\" align=\"right\">\${$pct_price}</td>
                </tr>";
}

// end new update for 500 trees

$invoice_number = date('md');
$total = number_format(0.99* $trees_of_this_month, 2);
$tbl = <<<EOD
<div>
    <h3 style="color:green; font-size:18px; margin:0; padding:0">Green Earth Appeal</h3>
    <b>swichboard</b> 0161 684 2339<br />
    <b>fax &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b> 0871 245 8075<br />
    <b>email </b>billing@greenearthappeal.org <br /><br />
    Hollinwood  Business Centre,<br />
    Albert St,<br />
    Oldham,<br />
    OL8 3QL.Oldham
    
    <div style="margin-top:50px;">
        <h3>INVOICE</h3>
        <b>Sonne Energeticos</b><br />
        <b>Antiguo Camino Minera del Norte Km 2.5</b><br />
        <b>Col. Centro</b><br />
        <b>Santa Catarina</b><br />
        <b>N.L.</b><br />
        <b>RFC: SEN060807L59</b><br />
    </div>
    <div style="margin-top:50px;">
        <i>Reference &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Fruit Tree Planting Donations</b></i>
    </div>
    <div style="margin-top: 50px;">
        <table align="center" valign="middle">
            <tr>
                <th colspan="2" width="30%"><b>Item</b></th>
                <th style="border-left: .5px solid gray;"><b>Units</b></th>
                <th style="border-left: .5px solid gray;"><b>Price Per Unit</b></th>
                <th style="border-left: .5px solid gray;"><b>Price &nbsp;&nbsp;</b></th>
            </tr>
            $pct_str
            <tr>
                <td colspan="2" valign="middle" align="left">Food For Thought - Trees</td>
                <td valign="middle" style="border-left: .5px solid gray;" align="right">500 &nbsp;&nbsp;</td>
                <td valign="middle" style="border-left: .5px solid gray;" align="right">$0.99  &nbsp;&nbsp;</td>
                <td valign="middle" style="border-left: 0.5px solid gray;" align="right">\$$total</td>
            </tr>
        </table>
    </div>
</div>
EOD;

$pctdate = '<span style="font-size: 16px; color: #171213">'.date("jS F Y").'</span>';
$html3 = '<table>
            <tr>
                <td>Date of Invoice</td>
                <td>'.$pctdate.'</td>
            </tr>
            <tr>
                <td>Invoice Number</td>
                <td><b>'.$invoice_number.'</b></td>
            </tr>
        </table>';

$tbl3 = '<table>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <table>
                        <tr>
                            <td style="border-top: .5px solid gray; width:200px">Total</td>
                            <td style="border-top: .5px solid gray; width: 100px" align="right">$'.$total.'</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border-top: .5px solid gray;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="border-top: .5px solid gray; border-bottom: .5px solid gray;"><b>Total Due</b></td>
                            <td style="border-top: .5px solid gray; border-bottom: .5px solid gray;" align="right">$'.$total.'</td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>';

$pct_p = '<p style="text-align:center">Please remit funds to \'Green Earth Appeal\'<br />		
            BACS Payment: Sort Code 30 96 26 Account number 01331722<br />
            IBAN: GB84LOYD77040511418860<br />
            Swift: LOYDGB21T12<br />	
            Cheque: Post to address above</p>';

$pct_logo = '<img src="imgs/logo-green.png" />';
//$pdf->writeHTML($tbl, true, false, true, false, '');

//$pdf->SetY(875);
//$pdf->SetX(290);
//$pdf->SetFont('times', '', 48);

$pdf->writeHTMLCell($w=200, $h=0, $x='450', $y='50', $pct_logo, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->writeHTMLCell($w=0, $h=0, $x='450', $y='220', $html3, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->writeHTMLCell($w=600, $h=0, $x='25', $y='20', $tbl, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//$pdf->writeHTMLCell($w=300, $h=0, $x='371', $y='460', $tbl3, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//$pdf->writeHTMLCell($w=300, $h=0, $x='371', $y='0', $tbl3, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//$pdf->writeHTMLCell($w=0, $h=0, $x='40', $y='540', $pct_p, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

$pdf->writeHTML($tbl3, true, false, true, false, 'R');
$pdf->writeHTML($pct_p, true, false, true, false, 'C');
//Close and output PDF document
$pdf->Output('../invoices/'.$invoice_name, 'F');

//============================================================+
// END OF FILE
//============================================================+
