<?php
    defined('P_EXEC') or die('Denied access');
    
    class p_config {
        var $date = 1; // a set date of month to send the invoice
        var $optout_days = 7; // should be 7 days
        var $invite_days = 90; // should be 90 days
        var $sonne_email = 'afhuglerv@gruposonne.com'; // email of the sonne partner
    }
?>