<?php
    
	
	define('P_EXEC', true);
    set_time_limit(0);
    require_once('config.php');
    require_once('../database.php');
    
    $config = new p_config();
    $optout_seconds = $config->optout_days*24*60;
    
    //$now = mktime();
	$now = time();
    
    //check if it's enough 7 days
    $query = "SELECT * FROM pct_customers
                WHERE state=1
                AND $now-UNIX_TIMESTAMP(created)>$optout_seconds";
    $result = mysql_query($query) or die(mysql_error);
    while($row = mysql_fetch_assoc($result)) {
        // create the certification and send email
        // output the certificate
        $pct_image_url = '';
        $pct_logo   = 'the-sonne.png';
        $pct_compname = $row['name'];
        $pct_numtrees = 2;
        $pct_numtrees_thou = $pct_numtrees*1000; 
        $pct_id = $row['id'];
        $cert_name = "cert_{$pct_id}.pdf";
        require('cert.php');
        // send the certificate
        $pct_customer_id = $pct_id;
        $to = $row['email'];
        $subject = 'Confirmación de colaboración';
        $msg = file_get_contents('mailtemplate_new.html');
        $msg = str_replace('[name]', $row['firstname'], $msg);
        $msg = str_replace('[accountname]', $row['name'], $msg);
        $msg = str_replace('[numtrees]', 2, $msg);
        $msg = str_replace('[treetext]', 'trees', $msg);
        
        require('PHPMailer_522/test_sendmail_basic.php');
        //$mail->AddAttachment("Marketing_Collateral.zip");      // attachment
        $mail->AddAttachment("../certification/$cert_name"); // attachment
        
        if(!$mail->Send()) {
          //echo "Mailer Error: " . $mail->ErrorInfo;
          echo "Can't send email to the customer {$row['email']}<br />";
          error_log("{$row['email']} :: Mailer Error: " . $mail->ErrorInfo);
        }
        
    }
    // change state
    $query = "UPDATE pct_customers
                SET state=2, count=1
                WHERE state=1
                AND $now-UNIX_TIMESTAMP(created)>$optout_seconds";
    mysql_query($query) or die(mysql_error);
    echo 'success';    
	
	
?>