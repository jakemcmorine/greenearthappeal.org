<?php
define('P_EXEC', true);
define('VIH_LIVE', true);

set_time_limit(0);
require_once('../database.php');
$year = date('Y');
$month = (int)date('m'); 
if ($month == '1') {
    $month = 12;
    $year -= 1;
} else {
    $month = (int)$month - 1;
}
if ($month < 10) $month = '0'.$month;
// the last day of the month
$day = 0;
switch ($month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        $day = 31;
        break;
    case 2:
        $day = 29;
        break;
    default:
        $day = 30;
        break;
}

// month to get invoices from
//$pct_month = "$year-{$month}";
$pct_month = "2017-12";
 $query = "SELECT pcc.compname AS name, SUM(pcr.numtrees) AS nums FROM t_partner_cert_companies AS pcc
            INNER JOIN t_partner_accounts AS pa
            ON pcc.partner_id = pa.id
            INNER JOIN t_partner_cert_requests AS pcr
            ON pcr.custaccountid = pcc.id
            WHERE pa.id=2
            AND pcr.pending_invoices=1
            AND pcr.date LIKE '{$pct_month}%'
            GROUP BY pcr.custaccountid";
$result = mysql_query($query, $db);
$i = 0;
$pct_trees = 0;
$items = '';
while ($row = mysql_fetch_assoc($result)) {
    $i++;
    //echo $i . ' -- '. $row['name']. ' - '. $row['nums']. '<br />';
    $pct_trees += (int)$row['nums'];
    $total_row = number_format((int)$row['nums'] * .99, 2);
    $items .= '<tr>
                <td valign="middle" style="line-height:2">'.$row['name'].'</td>
                <td valign="middle" style="line-height:2;" align="right">'.$row['nums'].'</td>
                <td valign="middle" style="line-height:2" align="right">&pound;'.$total_row.' &nbsp;&nbsp;</td>
            </tr>';
}
//echo 'Trees: '. $pct_trees;
$total = number_format(.99*$pct_trees, 2);


// create the invoice for paper cup

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');


// Extend the TCPDF class to create custom Header and Footer
if (!class_exists('MYPDF1')) {
    class MYPDF1 extends TCPDF {
    	//Page header
    	public function Header() {
    		// get the current page break margin
    		$bMargin = $this->getBreakMargin();
    		// get current auto-page-break mode
    		$auto_page_break = $this->AutoPageBreak;
    		// disable auto-page-break
    		$this->SetAutoPageBreak(false, 0);
    		// set bacground image
    		//$img_file = K_PATH_IMAGES.'bg.jpg';
            //$img_file = 'bg.jpg';
            //$this->Image($img_file, 0, 0, 1200, 1772, '', '', '', false, 300, '', false, false, 0);
            //$this->Image($img_file, 0, 0, 757, 1118, '', '', '', false, 300, '', false, false, 0);
            
    		// restore auto-page-break status
    		$this->SetAutoPageBreak($auto_page_break, $bMargin);
    		// set the starting point for the page content
    		$this->setPageMark();
    	}
    }
    
}

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pageLayout = array(757, 600);
//$pageLayout = array(800, 660);
$pageLayout = array(600, 1000);
$pdf = new MYPDF1('p', 'px', $pageLayout, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Green Earth Appeal');
$pdf->SetAuthor('Green Earth Appeal');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('');
$pdf->SetKeywords('green earth appeal invoice');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(0, 0, 0, true);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
//$pdf->SetFont('times', '', 48);
$pdf->SetFont('helvetica', '', 10);
// add a page
$pdf->AddPage();
//$pdf->SetY(150);
//$pdf->SetX(20);
// neccesiry variables
/*

  $units = 4;
    $invoice_name   = 'invoice_'.date('m').'.pdf';  
*/ 
$units = 4;
//$invoice_name   = 'invoice_'.date('m').'.pdf';  
$invoice_name   = 'invoice_12_2017.pdf';  
// set invoice time is the last day of the month
$invoice_time = mktime(0, 0, 0, $month, $day, $year);
$invoice_number = date('Ymd', $invoice_time);

$tbl = <<<EOD
<div>
    <h3 style="color:green; font-size:18px; margin:0; padding:0">Green Earth Appeal</h3>
    <b>switchboard</b> 0208 798 0476<br />
    <b>fax &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b> 0871 245 8075<br />
    <b>email </b>billing@greenearthappeal.org <br /><br />
    Hollinwood  Business Centre,<br />
    Albert St,<br />
    Oldham,<br />
    OL8 3QL.Oldham
    
    <div style="margin-top:50px;">
        <h3>INVOICE</h3>
        <b>The Printed Cup Company</b><br />
        <b>Unit 3 Mearley Brooke Commercial Centre</b><br />
        <b>Citheroe</b><br />
        <b>Lancs</b><br />
        <b>BB7 1PL</b><br />
    </div>
    <div style="margin-top:50px;">
        <i>Reference &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Sarah</b></i>
    </div>
    <div style="margin-top: 50px;">
        <table align="center" valign="middle" align="left">
            <tr>
                <th width="200"><b>Job</b></th>
                <th></th>
                <th></th>
            </tr>
            $items
        </table>
    </div>
</div>
EOD;
$end_day = 0;
$month = date('m');
$pctdate = '<span style="font-size: 16px; color: #171213">30-November-2017</span>';
$html3 = '<table>
            <tr>
                <td>Date of Invoice</td>
                <td>'.$pctdate.'</td>
            </tr>
            <tr>
                <td>Invoice Number</td>
                <td><b>'.$invoice_number.'</b></td>
            </tr>
        </table>';

$tbl3 = '<table border="0" align="right">
            <tr>
                <td style="border-top: .5px solid gray;" width="60" align="left">Total</td>
                <td style="border-top: .5px solid gray;" width="60">'.$pct_trees.'</td>
                <td style="border-top: .5px solid gray; " align="right" width="155">&pound;'.$total.'</td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: .5px solid gray;">&nbsp;</td>
            </tr>
            <tr>
                <td style="border-top: .5px solid gray; border-bottom: .5px solid gray;" align="left"><b>Total Due</b></td>
                <td style="border-top: .5px solid gray; border-bottom: .5px solid gray;"></td>
                <td style="border-top: .5px solid gray; border-bottom: .5px solid gray;" align="right">&pound;'.$total.'</td>
            </tr>
        </table>';

$pct_p = '<br><br><p style="text-align:center;">Please remit funds to \'Green Earth Appeal\' c/o Lloyds TSB, P.O.Box 1000, BX1 1LT<br>
            <b>AC 11418860 SC 77-04-05</b>';

$pct_logo = '<img src="imgs/logo-green.png" />';
//$pdf->writeHTML($tbl, true, false, true, false, '');

//$pdf->SetY(875);
//$pdf->SetX(290);
//$pdf->SetFont('times', '', 48);
$pdf->writeHTMLCell($w=250, $h=0, $x='350', $y='50', $pct_logo, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->writeHTMLCell($w=0, $h=0, $x='350', $y='220', $html3, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->writeHTMLCell($w=400, $h=0, $x='100', $y='20', $tbl, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

//$pdf->writeHTMLCell($w=300, $h=0, $x='371', $y='460', $tbl3, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//$pdf->writeHTMLCell($w=0, $h=0, $x='40', $y='540', $pct_p, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

//$pdf->writeHTML($html3, true, false, true, false, '');
//$pdf->writeHTML($tbl3, true, false, true, false, 'R');
$pdf->writeHTMLCell(200, 0, 300, '', $tbl3, 0, 1, 0, true, '', true);
$pdf->writeHTML($pct_p, true, false, true, false, '');

//$pdf->writeHTML($pct_logo, true, false, true, false, '');
//Close and output PDF document
$pdf->Output('../invoices/'.$invoice_name, 'F');

/**
 * =============================================================================================
* add the invoice to panacea
*/
// user_id, total, date_created, trees, number, name, 
$panacea_total = number_format($trees_of_this_month * 0.99, 2);
$pa_query = "INSERT INTO pct_invoices SET
  user_id= 803,
  trees = $pct_trees,
  total = '$total',
  name = '$invoice_name',
  `number` = '$pct_month',
  date_created = NOW()";
mysql_query($pa_query, $panacea_db) or die(mysql_error());  

//============================================================+
// END OF FILE
//============================================================+
// send the invoice to papercup

/* if (VIH_LIVE) {
  $to = 'accounts@thepapercupcompany.co.uk';
} else {
  //$to = 'phpcodeteam@gmail.com';
  $to = 'marvin@greenearthappeal.org';
} */
$to = 'mat@greenearthappeal.org';
//$to = 'testing.whizkraft1@gmail.com';
$subject = 'Invoice for The Printed Cup Company ('.$pct_month.')';
$msg = 'Here is the invoice for The Printed Cup Company on '.$pct_month;
require('PHPMailer_522/test_sendmail_basic.php');
//$mail->AddAddress('pccinvoice@greenearthappeal.org');
$mail->AddAttachment("../invoices/$invoice_name");

/*$to = 'Jenny.Woodward@thepapercupcompany.co.uk';
$subject = 'Invoice for The Printed Cup Company ('.$pct_month.')';
$msg = 'Here is the invoice for The Printed Cup Company on '.$pct_month;
require('PHPMailer_522/test_sendmail_basic.php');
$mail->AddAddress('sarah.woodward@thepapercupcompany.co.uk');
$mail->AddAddress('pccinvoice@greenearthappeal.org');
$mail->AddAttachment("../invoices/$invoice_name");*/

if(!$mail->Send()) {
  //echo "Mailer Error: " . $mail->ErrorInfo;
  //echo "Can't send email to the customer {$to}<br />";
  error_log("{$to} :: Mailer Error: " . $mail->ErrorInfo);
}
    
echo 'invoice is sent successfully';
?>