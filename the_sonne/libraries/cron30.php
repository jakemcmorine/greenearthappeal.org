<?php
    define('P_EXEC', true);
    set_time_limit(0);
    require('config.php');
    require('../database.php');
    define('VIH_LIVE', true);
    
    $config = new p_config();
    $invoice_date = $config->date;
    $current_day = date('d');
	//$current_day = 1;
    
    // get invoice month
    $month = (int)date('m');
    $year = date('Y');
    if ($current_day < 10) {
        // invoice is get from previous month
        if ($month == '1') {
            $month = 12;
            $year -= 1;
        } else {
            $month = (int)$month - 1;
        }
    } // invoice is get from this month
    $time = $year.$month;
    $time = (int)$time;

    // check if today is a day to send the invoice
    if (VIH_LIVE) {
       if ($current_day != $invoice_date) {echo 'invalid date'; die;} 
    }

    // create the invoice for the sonne
    // select appropriate customers
    $query = "SELECT * FROM pct_customers
                WHERE state=2";
    $result = mysql_query($query, $db) or die(mysql_error());
    // get number of trees of this month
    $units = 0;
    $pct_rows = array();
    if (mysql_numrows($result)) {
        while($row = mysql_fetch_array($result)) {
            $units += $row['client_trees'];
            $pct_rows[] = $row;
        }
    }
    $trees_of_this_month = ($units>500) ? $units : 500;
    //$customers_number = mysql_numrows($result);    
    // create the invoice
    // $units = $customers_number*2;
    $invoice_name   = 'invoice_'.$month.'.pdf';
    require('invoice.php');
    
    // create the certification for the sonne
    $pct_image_url = '';
    $pct_logo   = 'the-sonne.png';
    $pct_compname = 'Sonne Energeticos S.A de C.V';
    // calculate the number of trees of each restaurant
    $query1 = "SELECT SUM(trees) AS previous_trees FROM pct_trees
                WHERE type='restaurant'
                AND month<>$time";
    $result1 = mysql_query($query1, $db);
    $previous_trees = mysql_fetch_array($result1);
    $previous_trees = $previous_trees[0];
    
    //$pct_numtrees = $previous_trees + $units;
    $pct_numtrees = $previous_trees + $trees_of_this_month;
    $pct_numtrees_thou = $pct_numtrees*1000; 
    $pct_id = 'the_sonne';
    $cert_name = "cert_{$pct_id}.pdf";
    require('cert.php');
    
    
    // send the invoice to the sonne
    if (VIH_LIVE) {
        $to = $config->sonne_email;
    } else {
        $to = 'phpcodeteam@gmail.com';
    }

    $subject = 'Invoice for Sonne Energeticos S.A de C.V';
    $msg = 'email template for the sonne invoice';
    require('PHPMailer_522/test_sendmail_basic.php');
    $mail->AddAddress('marvin@greenearthappeal.org');
    
    //$mail->AddAttachment("Marketing_Collateral.zip");      // attachment
    $mail->AddAttachment("../invoices/$invoice_name");
    
    if(!$mail->Send()) {
      //echo "Mailer Error: " . $mail->ErrorInfo;
      //echo "Can't send email to the customer {$to}<br />";
      error_log("{$to} :: Mailer Error: " . $mail->ErrorInfo);
    }
    
    // send the certification
    $subject = 'Sonne Energeticos S.A de C.V - '.$pct_numtrees.' Trees';
    $msg = 'certification email template for The Sonne';
    
    require('PHPMailer_522/test_sendmail_basic.php');
    if (VIH_LIVE) {
      $mail->AddAddress('marvin@greenearthappeal.org');
    } else {
      $mail->AddAddress('phpcodeteam@gmail.com');
      $mail->AddAddress('marvin@greenearthappeal.org');
    }
    $mail->AddAttachment("../certification/$cert_name"); // attachment
    
    if(!$mail->Send()) {
      //echo "Mailer Error: " . $mail->ErrorInfo;
      echo "Can't send email to the customer {$to}<br />";
      error_log("{$to} :: Mailer Error: " . $mail->ErrorInfo);
    }
    
    /**
     * =============================================================================================
    * add the invoice to panacea
    */
    // user_id, total, date_created, trees, number, name, 
    $panacea_total = number_format($trees_of_this_month * 0.99, 2);
    $pa_query = "INSERT INTO pct_invoices SET
        user_id= 802,
        trees = $trees_of_this_month,
        total = '$panacea_total',
        name = '$invoice_name',
        `number` = '$time',
        date_created = NOW()";
    mysql_query($pa_query, $panacea_db) or die(mysql_error());
    
    /**
     * =============================================================================================
    * create and send the certificate to groups
    */
    $query = "SELECT SUM(c.client_trees) AS trees, c.id AS id, g.id AS gid, g.email AS email, g.name AS name
                FROM pct_customers AS c
                INNER JOIN pct_groups AS g
                ON g.id=c.group_id
                WHERE c.state=2
                GROUP BY g.id";
    $result = mysql_query($query, $db) or die(mysql_error);
    while ($row = mysql_fetch_assoc($result)) {
        // create the certification for each group
        $pct_image_url = '';
        $pct_logo   = 'the-sonne.png';
        $pct_compname = $row['name'];
        // get previous trees
        $query1 = "SELECT SUM(pt.trees) AS previous_trees FROM pct_customers AS pc
                    INNER JOIN pct_groups AS pg
                    ON pc.group_id=pg.id
                    INNER JOIN pct_trees AS pt
                    ON pt.id=pc.id
                    WHERE pt.type='restaurant'
                    AND pg.id={$row['gid']}
                    AND pt.month<>$time";
        $result1 = mysql_query($query1, $db);
        $previous_trees = mysql_fetch_array($result1);
        $previous_trees = $previous_trees[0];
        
        $pct_numtrees = $row['trees'] + $previous_trees;
        $pct_numtrees_thou = $pct_numtrees*1000; 
        $cert_name = 'group_'.$row['gid'].'.pdf';
        require('cert.php');
        
        // send the certification for each group
        $subject = $pct_compname.' Group - '.$pct_numtrees. ' Trees';
        $msg = $pct_compname.' - '.$pct_numtrees. ' Trees'.
                '<p>The Green Earth Appeal</p>
                
                Kind regards,<br /><br />
                Marvin Baker</p>
                <table width="700"  cellpadding="0" cellspacing="0" style="border-width:1px; border-style:solid; border-color:#006600;">
                  <tr>
                    <td><table width="700" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td width="526" style="border-right:#006600; border-right-style:solid; border-right-width:1px; font-family: Arial, Helvetica, sans-serif; font-size: 18px; font-weight: bold;">Marvin Baker</td>
                        <td width="154" rowspan="5" bgcolor="#DFE8BF" style="width:154px"><table width="146" align="center" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="154" height="70"><a href="http://www.greenearthappeal.org"><img src="http://www.greenearthappeal.org/graphics/signaturev2/gea_logo.gif" alt="FOOD FOR THOUGHT" width="148" height="64" border="0" /></a></td>
                          </tr>
                          <tr>
                            <td width="154" height="30" align="center" style="height:30px;"><a href="http://www.greenearthappeal.org"><img src="http://www.greenearthappeal.org/graphics/signaturev2/btn_website.jpg" alt="Visit Our Website" width="146" height="34" border="0" /></a></td>
                          </tr>
                          <tr>
                            <td width="154" height="30" align="center" style="height:30px;"><a href="http://twitter.com/greenearthapp"><img src="http://www.greenearthappeal.org/graphics/signaturev2/btn_twitter.jpg" alt="Follow Us On Twitter" width="146" height="32" border="0" /></a></td>
                          </tr>
                          <tr>
                            <td width="154" height="30" align="center" style="height:30px;"><a href="http://www.facebook.com/pages/Green-Earth-Appeal/113538891999655"><img src="http://www.greenearthappeal.org/graphics/signaturev2/btn_facebook.jpg" alt="Find Us On Facebook" width="146" height="32" border="0" /></a></td>
                          </tr>
                        </table></td>
                      </tr>
                      <tr>
                        <td width="526" height="10" style="border-right:#006600; border-right-style:solid; border-right-width:1px; font-family: Arial, Helvetica, sans-serif; color: #006600; font-size: 11px;">(T) 0161 684 2339&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;(M) 07958 022011&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;(F) 0871 315 1358&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;(E) <a style="color:#006600; text-decoration:none;" href="mailto:marvin@greenearthappeal.org">marvin@greenearthappeal.org</a></td>
                      </tr>
                      <tr>
                        <td width="526" height="10" style="border-right:#006600; border-right-style:solid; border-right-width:1px; font-family: Arial, Helvetica, sans-serif; color: #006600; font-size: 11px;">(W) <a style="color:#006600; text-decoration:none;" href="http://www.greenearthappeal.org">www.greenearthappeal.org</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a style="color:#006600; text-decoration:none;" href="http://twitter.com/greenearthapp">Follow Us On <img src="http://www.greenearthappeal.org/graphics/signaturev2/twitter_small.gif" alt="Twitter" width="55" height="11" border="0" /></a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="http://www.facebook.com/pages/Green-Earth-Appeal/113538891999655" style="color:#006600; text-decoration:none;" >Find Us On <img src="http://www.greenearthappeal.org/graphics/signaturev2/facebook_small.gif" alt="Facebook" width="50" height="10" border="0" /></a></td>
                      </tr>
                      <tr>
                        <td width="526" height="10" style="border-bottom-style:solid; border-bottom-color:#006600; border-bottom-width:1px; border-right:#006600; border-right-style:solid; border-right-width:1px; font-family: Arial, Helvetica, sans-serif; color: #006600; font-size: 11px;" nowrap="nowrap">(P)  Green Earth Appeal, Hollinwood Business Centre, Albert St, Oldham, OL8 3QL.<a href="#"style="color:#006600; text-decoration:none;" ></a></td>
                      </tr>
                      <tr>
                        <td width="526" height="30" bgcolor="#E7EEF7" style="border-right:#006600; border-right-style:solid; border-right-width:1px; fo"><table border="0" cellspacing="3" cellpadding="3">
                          <tr>
                            <td style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; color: #4D7EC1;">
                            <table>
                            	<tr>
                                	<td align="left" valign="top" width="19"><img src="http://www.greenearthappeal.org/graphics/signaturev2/66.gif" alt="&quot;" width="19" height="12" /></td>
                                	<td><span style="font-family: Arial, Helvetica, sans-serif; color: #4D7EC1; font-size: 11px;">On the opening day of the United Nations Climate Change Conference in Copenhagen, we are happy to welcome The Green Earth Appeal as a partner to the Billion Tree Campaign. Trees are a legacy for future generations. - <strong>United Nations Environment Programme</strong></td>
                                    <td align="right" width="19" valign="bottom"><img src="http://www.greenearthappeal.org/graphics/signaturev2/99.gif" alt="&quot;" width="19" height="12" /></td>
                             	</tr>
                             </table>             </td>
                            </tr>
                        </table></td>
                      </tr>
                      <tr bgcolor="#006600">
                        <td height="30" colspan="2" align="center" bgcolor="#006600" style="border-right:#006600; border-right-style:solid; border-right-width:1px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #FFFFFF; font-size: 14px;">PLEASE DON\'T PRINT THIS EMAIL UNLESS YOU REALLY REALLY NEED TO</td>
                      </tr>
                      <tr bgcolor="#006600">
                        <td height="40" colspan="2" align="left" bgcolor="#F7F9EE" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; color:#758831;">This email and any attachments to it may be confidential and are intended solely for the use of the individual to whom it is addressed. Any views or opinions expressed are solely those of the author and do not necessarily represent those of The Green Earth Appeal. If you are not the intended recipient of this email, you must neither take any action based upon its contents, nor copy or show it to anyone. Please contact the sender if you believe you have received this email in error. </td>
                      </tr>
                    </table></td>
                  </tr>
                </table>';
        //$to = $row['email'];
        if (VIH_LIVE) {
          $to = 'marvin@greenearthappeal.org';          
        } else {
          $to = 'sunaroad@gmail.com';
        }

        require('PHPMailer_522/test_sendmail_basic.php');

        $mail->AddAttachment("../certification/$cert_name"); // attachment
        
        if(!$mail->Send()) {
          //echo "Mailer Error: " . $mail->ErrorInfo;
          echo "Can't send email to the customer {$to}<br />";
          error_log("{$to} :: Mailer Error: " . $mail->ErrorInfo);
        }
        
    }
    /*
    //
    $query = "UPDATE pct_customers
                SET count=count+1
                WHERE state=2";

    mysql_query($query, $db) or die(mysql_error);
        */
    
    /**
    * Send the certification to customers on a day of each month
    */
    $query = "SELECT * FROM pct_customers
                WHERE state=2";
    $result = mysql_query($query, $db) or die(mysql_error);
    
    $pct_no_optin = array('ulises.sanchez@mx.mcd.com', 'josefina.flores@mx.mcd.com', 'raul.villegas@mx.mcd.com', 'franceliaayala@lasalitas.com',  'roviedo@chilis.com.mx', 'ncalderon@chilis.com.mx', 'alejandro.leal@ggalimentos.com', 'jose.silva@ggalimentos.com', 'hycalimentosregios@yahoo.com.mx', 'jlmartinez@travelerplaza.com', 'hibarra@grupoculinaria.mx', 'jheredia@jacandray.com', 'andres.nieto@wal-mart.com', 'jbarajas@churchs.com.mx', 'vicentenieto@tiendas-grand.com.mx', 'yerelli.rodriguez@prb.com.mx', 'victoriasmbc@gmail.com', 'gerardo.ayala@s-martmx.com', 'adrianmelena@hotmail.com', 'arturo.lozano@posadas.com', 'enrique@grupopacifica.com', 'enriquecamacho@elpolloloco.com.mx', 'ecavazos@grupoav.com', 'garcia@grupopangea.com');
    
    while ($row = mysql_fetch_assoc($result)) {
      if (! VIH_LIVE) {
        continue;
      }
        // create the certification
        $pct_image_url = '';
        $pct_logo   = 'the-sonne.png';
        $pct_compname = $row['name'];
        // get the number of trees of each restaurant
        $query1 = "SELECT SUM(trees) AS previous_trees FROM pct_trees
                    WHERE type='restaurant'
                    AND id={$row['id']}
                    AND month<>$time";
        $result1 = mysql_query($query1, $db);
        $previous_trees = mysql_fetch_array($result1);
        $previous_trees = $previous_trees[0];
        
        $pct_numtrees = $row['client_trees'] + $previous_trees;
        $pct_numtrees_thou = $pct_numtrees*1000;
        
        $cert_name = 'cert_'.str_replace(' ', '_', $pct_compname).'_'.$row['id'].'.pdf';
        require('cert.php');
        // add trees of this month to database only if it doesn't exists yet
        $row2 = mysql_query("SELECT COUNT(*) FROM pct_trees WHERE month=$time AND type='restaurant' AND id={$row['id']}", $db);
        $row2 = mysql_fetch_array($row2);
        if ($row2[0] < 1) {
            $query2 = "INSERT INTO pct_trees SET
                    id={$row['id']},
                    trees={$row['client_trees']},
                    month=$time,
                    type='restaurant'";
            mysql_query($query2, $db);
        }
        
        // send the certification
        $subject = $row['name'].' - '.$pct_numtrees.' Trees';
        $msg = '<p>Gracias por su constante y dedicada participación en el programa de plantación de árboles de Sonne Energéticos y The Green Earth Appeal.</p>
                <p>Se adjunta el certificado por los '.$pct_numtrees.' árboles que han sido plantados gracias a su participación en esta iniciativa.</p>
                <p>Estos árboles cambian vidas, ya que proporcionan alimentos, forraje para  animales y leña para construcción y combustible. Además, aumentan la producción agrícola, mejoran la infiltración del agua y la recarga de los acuíferos;  al mismo tiempo protegen los suelos de la erosión eólica e hídrica y  compensan la huella de carbono de la recolección del aceite quemado y de sus otras actividades diarias.</p>
                <p>Muchas gracias por su continuo apoyo.</p>
                <p>The Green Earth Appeal</p>
                
                Kind regards,<br /><br />
                Marvin Baker</p>
                <table width="700"  cellpadding="0" cellspacing="0" style="border-width:1px; border-style:solid; border-color:#006600;">
                  <tr>
                    <td><table width="700" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td width="526" style="border-right:#006600; border-right-style:solid; border-right-width:1px; font-family: Arial, Helvetica, sans-serif; font-size: 18px; font-weight: bold;">Marvin Baker</td>
                        <td width="154" rowspan="5" bgcolor="#DFE8BF" style="width:154px"><table width="146" align="center" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="154" height="70"><a href="http://www.greenearthappeal.org"><img src="http://www.greenearthappeal.org/graphics/signaturev2/gea_logo.gif" alt="FOOD FOR THOUGHT" width="148" height="64" border="0" /></a></td>
                          </tr>
                          <tr>
                            <td width="154" height="30" align="center" style="height:30px;"><a href="http://www.greenearthappeal.org"><img src="http://www.greenearthappeal.org/graphics/signaturev2/btn_website.jpg" alt="Visit Our Website" width="146" height="34" border="0" /></a></td>
                          </tr>
                          <tr>
                            <td width="154" height="30" align="center" style="height:30px;"><a href="http://twitter.com/greenearthapp"><img src="http://www.greenearthappeal.org/graphics/signaturev2/btn_twitter.jpg" alt="Follow Us On Twitter" width="146" height="32" border="0" /></a></td>
                          </tr>
                          <tr>
                            <td width="154" height="30" align="center" style="height:30px;"><a href="http://www.facebook.com/pages/Green-Earth-Appeal/113538891999655"><img src="http://www.greenearthappeal.org/graphics/signaturev2/btn_facebook.jpg" alt="Find Us On Facebook" width="146" height="32" border="0" /></a></td>
                          </tr>
                        </table></td>
                      </tr>
                      <tr>
                        <td width="526" height="10" style="border-right:#006600; border-right-style:solid; border-right-width:1px; font-family: Arial, Helvetica, sans-serif; color: #006600; font-size: 11px;">(T) 0161 684 2339&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;(M) 07958 022011&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;(F) 0871 315 1358&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;(E) <a style="color:#006600; text-decoration:none;" href="mailto:marvin@greenearthappeal.org">marvin@greenearthappeal.org</a></td>
                      </tr>
                      <tr>
                        <td width="526" height="10" style="border-right:#006600; border-right-style:solid; border-right-width:1px; font-family: Arial, Helvetica, sans-serif; color: #006600; font-size: 11px;">(W) <a style="color:#006600; text-decoration:none;" href="http://www.greenearthappeal.org">www.greenearthappeal.org</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a style="color:#006600; text-decoration:none;" href="http://twitter.com/greenearthapp">Follow Us On <img src="http://www.greenearthappeal.org/graphics/signaturev2/twitter_small.gif" alt="Twitter" width="55" height="11" border="0" /></a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="http://www.facebook.com/pages/Green-Earth-Appeal/113538891999655" style="color:#006600; text-decoration:none;" >Find Us On <img src="http://www.greenearthappeal.org/graphics/signaturev2/facebook_small.gif" alt="Facebook" width="50" height="10" border="0" /></a></td>
                      </tr>
                      <tr>
                        <td width="526" height="10" style="border-bottom-style:solid; border-bottom-color:#006600; border-bottom-width:1px; border-right:#006600; border-right-style:solid; border-right-width:1px; font-family: Arial, Helvetica, sans-serif; color: #006600; font-size: 11px;" nowrap="nowrap">(P)  Green Earth Appeal, Hollinwood Business Centre, Albert St, Oldham, OL8 3QL.<a href="#"style="color:#006600; text-decoration:none;" ></a></td>
                      </tr>
                      <tr>
                        <td width="526" height="30" bgcolor="#E7EEF7" style="border-right:#006600; border-right-style:solid; border-right-width:1px; fo"><table border="0" cellspacing="3" cellpadding="3">
                          <tr>
                            <td style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; color: #4D7EC1;">
                            <table>
                            	<tr>
                                	<td align="left" valign="top" width="19"><img src="http://www.greenearthappeal.org/graphics/signaturev2/66.gif" alt="&quot;" width="19" height="12" /></td>
                                	<td><span style="font-family: Arial, Helvetica, sans-serif; color: #4D7EC1; font-size: 11px;">On the opening day of the United Nations Climate Change Conference in Copenhagen, we are happy to welcome The Green Earth Appeal as a partner to the Billion Tree Campaign. Trees are a legacy for future generations. - <strong>United Nations Environment Programme</strong></td>
                                    <td align="right" width="19" valign="bottom"><img src="http://www.greenearthappeal.org/graphics/signaturev2/99.gif" alt="&quot;" width="19" height="12" /></td>
                             	</tr>
                             </table>             </td>
                            </tr>
                        </table></td>
                      </tr>
                      <tr bgcolor="#006600">
                        <td height="30" colspan="2" align="center" bgcolor="#006600" style="border-right:#006600; border-right-style:solid; border-right-width:1px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #FFFFFF; font-size: 14px;">PLEASE DON\'T PRINT THIS EMAIL UNLESS YOU REALLY REALLY NEED TO</td>
                      </tr>
                      <tr bgcolor="#006600">
                        <td height="40" colspan="2" align="left" bgcolor="#F7F9EE" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; color:#758831;">This email and any attachments to it may be confidential and are intended solely for the use of the individual to whom it is addressed. Any views or opinions expressed are solely those of the author and do not necessarily represent those of The Green Earth Appeal. If you are not the intended recipient of this email, you must neither take any action based upon its contents, nor copy or show it to anyone. Please contact the sender if you believe you have received this email in error. </td>
                      </tr>
                    </table></td>
                  </tr>
                </table>';
        $to = $row['email'];
        $email_arr = explode(',', $row['email']);
        $to = trim($email_arr[0]);
        if (in_array($to, $pct_no_optin)) {
            $to = 'afhuglerv@gruposonne.com';
        }
        //$to = 'marvin@greenearthappeal.org';

        require('PHPMailer_522/test_sendmail_basic.php');
        /*
        if(count($email_arr)>1) {
            $mail->AddAddress(trim($email_arr[1]));
        }
        */

        $mail->AddAttachment("../certification/$cert_name"); // attachment
        if(!$mail->Send()) {
          echo "Can't send email to the customer {$to}<br />";
          error_log("{$to} :: Mailer Error: " . $mail->ErrorInfo);
        }
    }
    // Add more trees to total trees of the current month to get it up to 500 trees
    if ($units<500) {
        $append_trees = 500 - $units;
        $row3 = mysql_query("SELECT COUNT(*) FROM pct_trees WHERE month=$time AND type='restaurant' AND id=0", $db);
        $row3 = mysql_fetch_array($row3);
        if ($row3[0] < 1) {
            $query3 = "INSERT INTO pct_trees SET
                id=0,
                trees={$append_trees},
                month=$time,
                type='restaurant'";
            mysql_query($query3, $db);
        }
    }
    
    echo 'success';
    
?>