<?php
    session_start();
    // check session and redirect if session existed
    if ($_SESSION['login']) {
        $task = $_POST['task'];
        if ($task == 'logout') {
            $_SESSION['login'] = false;
        } else {
            header('Location: index.php');   
        }
    }
    // check login
    if (isset($_POST['login'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $username = mysql_escape_string($username);
        $password = md5($password);
        // add database
        require('database.php');
        $query = "SELECT * FROM pct_users
                    WHERE username='$username'
                    AND password='$password'";
        $result = mysql_query($query);
        $row = mysql_fetch_array($result);
        // login successfully
        if ($row) {
            $_SESSION['login'] = 1;
            header("Location: index.php");
        } else {
            $error = 'Login Fail. Please try again';
        }
    }
    
?>
<?php require('templates/header.php'); ?>
<div id="maincontent">
    <div>
        <form action="" method="post">
            <fieldset style="width: 400px; margin: 0 auto;">
            	<legend>Login Form</legend>
                <table>
                    <?php
                        if (isset($error) && !empty($error)) : 
                    ?>
                    <tr>
                        <td colspan="2">
                            <p style="color: red;"><?php echo $error; ?></p>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Username: </td>
                        <td><input type="text" name="username" /></td>
                    </tr>
                    <tr>
                        <td>Password: </td>
                        <td><input type="password" name="password" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" name="login" value="Login" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<?php require('templates/footer.php'); ?>        