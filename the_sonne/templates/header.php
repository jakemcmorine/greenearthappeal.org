<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<meta name="author" content="johnn" />
    <link type="image/x-icon" rel="shortcut icon" href="http://www.greenearthappeal.org/plugins/system/jat3/jat3/base-themes/default/images/favicon.ico" />
	<title>Green Earth Appeal and Sonne Energeticos</title>
    <style>
        @font-face {
            font-family: 'QuestrialRegular';
            src: url('http://www.greenearthappeal.org/templates/ja_puresite/fonts/questrialregular/questrial-regular-webfont.eot');
            src: url('http://www.greenearthappeal.org/templates/ja_puresite/fonts/questrialregular/questrial-regular-webfont.eot?#iefix') format('embedded-opentype'),
                 url('http://www.greenearthappeal.org/templates/ja_puresite/fonts/questrialregular/questrial-regular-webfont.woff') format('woff'),
                 url('http://www.greenearthappeal.org/templates/ja_puresite/fonts/questrialregular/questrial-regular-webfont.ttf') format('truetype'),
                 url('http://www.greenearthappeal.org/templates/ja_puresite/fonts/questrialregular/questrial-regular-webfont.svg#QuestrialRegular') format('svg');
            font-weight: normal;
            font-style: normal;
        
        }
        body {
            font-family: Helvetica;
            color: #656667;
        }
        p {
            margin: 5px 0;
        }
        h2, h3 {
            font-family: 'QuestrialRegular',Helvetica, Arial, sans-serif;
        }
        #wrapper {
            width: 960px;
            margin: 0 auto;
        }
        #header, #footer {
            color: #656667;
            text-align: center;
            background:  url("images/bg-body.gif") 0 0 #C9C9C9;
            font-size: 16px;
        }
        #header {
            background: white;
            text-align: left;
            border-bottom: 1px solid #E5E6E7;
            padding-bottom: 10px;
        }
        #footer a {
            text-decoration: none;
        }
        #footer {
            padding: 30px 0;
        }
        #maincontent {
            padding: 15px 10px;
            /*
            border-right: 1px solid black;
            border-left: 1px solid black;
            */
        }
        #maincontent table {
            margin: 0 auto;
        }
        .pct_advertise {
            border-top: 1px solid #E5E6E7;
            margin-top: 10px;
            padding: 10px 0;
        }
        .pct_advertise a {
            text-decoration: none;
        }
        .call_us {
            text-align: center;
            font-size: 22px;
            border-top: 1px solid #E5E6E7;
            padding: 10px 0;
        }
        .call_us a {
            color: green;
            text-decoration: none;
            font-weight: bold;
        }
        /* nivo slider */
        .nivo-controlNav {
            display: none!important;
        }
        .ja-title {
            border-bottom: 1px solid #E5E6E7;
            padding: 35px 35px 20px;
            font-family: 'QuestrialRegular',Helvetica,sans-serif;
            font-size: 17px;
        }
        .ja-title a {
            text-decoration: none;
            color: green;
        }
    </style>
</head>

<body>
    <div id="wrapper">
        <div id="header"> 
            <div style="float: left;"><a href="http://www.greenearthappeal.org/" title="green earth appeal"><img src="images/Partners/logo-trans.png" alt="logo" /></a></div>
            <div style="float: right;"><img src="images/logo.png" alt="logo" /></div>
            <?php
                if ($_SESSION['login']) {
            ?>
                <div style="margin-left: 400px; padding-top: 70px;">
                    <form action="login.php" method="post">
                        <input type="submit" value="Logout" />
                        <input type="hidden" name="task" value="logout" />
                    </form>
                </div>
            <?php 
                }
            ?>
            <div style="clear: both;"></div>
        </div>
        <!-- slider -->
        <div class="slider-wrapper theme-default" style="margin-top: 20px;">
            <div id="slider" class="nivoSlider" style="width: 900px; margin: 0 auto;">
                <img  src="http://www.greenearthappeal.org/images/front/food for thought 17.jpg" style="width: 880px; visibility: hidden; display: inline;" />
            	<img src="http://www.greenearthappeal.org/images/front/food for thought 20.jpg" />
            	<img src="http://www.greenearthappeal.org/images/front/food for thought 25 - Copy.jpg" />
            	<img src="http://www.greenearthappeal.org/images/front/food for thought 21.jpg" />
            	<img src="http://www.greenearthappeal.org/images/front/food for thought 22.jpg">
            	<img src="http://www.greenearthappeal.org/images/front/food for thought 18.jpg">
            	<img src="http://www.greenearthappeal.org/images/front/food for thought 19.jpg">
            	<img src="http://www.greenearthappeal.org/images/front/food for thought 22.jpg">
            </div>
        </div>
        <link rel="stylesheet" href="scripts/themes/default/default.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="scripts/nivo-slider.css" type="text/css" media="screen" />
        <script type="text/javascript" src="scripts/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="scripts/jquery.nivo.slider.js"></script>
        <script type="text/javascript">
        $(window).load(function() {
            $('#slider').nivoSlider();
        });
        </script>
        
        <div class="ja-title">The Green Earth Appeal is a Not-For-Profit Social Enterprise, changing lives of some of the planet's poorest communities through tree planting,&nbsp; in partnership with the United Nations Environment Programme and with the assistance of companies like yours. <a href="http://www.greenearthappeal.org/homepage/what-the-green-earth-appeal-does">Learn More</a></div>