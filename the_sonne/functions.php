<?php

    define(SONNE_URL, 'http://www.greenearthappeal.org/the_sonne/');
    
    function pct_valid_files($files) {
        $valid_files = array();
        $valid_filetypes    = array('image/jpeg', 'image/pjpeg', 'image/gif', 'image/png', 'application/pdf');
        $valid_extensions   = array('jpeg', 'jpg', 'gif', 'png', 'pdf');
        if ($files['name'][0]) {
            for ($i=0; $i<count($files['name']); $i++ ) {
                $file_name  = strtolower($files['name'][$i]);
                $dot_pos    = strrpos($file_name, '.');
                $ext        = substr($file_name, $dot_pos+1);
                if ( in_array($ext, $valid_extensions)
                    && in_array($files['type'][$i], $valid_filetypes)
                    && ($files['size'][$i]<20000000)
                    && $files['error'][$i]==0 ) {
                    $valid_files[] = $i;
                }
            }
        }
        return $valid_files;
    }
    
    function pct_upload_file($files, $valid_files, $upload_dir='attachments') {
        $file_str           = array();
        if (count($valid_files)) {
            // upload files
            foreach ($valid_files AS $v) {
                $v = (int)$v;
                $filename   = strtolower($files['name'][$v]);
                $filepath   = $upload_dir.'/'.$filename;
                $i          = 1;
                while( file_exists($filepath) ) {
                    $filename   = $i.$files['name'][$v];
                    $filepath   = $upload_dir.'/'.$filename;
                    $i++;
                }
                if ( move_uploaded_file($files['tmp_name'][$v], $filepath) ) {
                    $file_str[] = $filename;
                }
            }
        }
        $file_str       = implode(',', $file_str);        
        return $file_str;
    }
    
    function pct_generate_code($length=8, $strength=10) {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength >= 1) {
        $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength >= 2) {
        $vowels .= "AEUY";
        }
        if ($strength >= 4) {
        $consonants .= '23456789';
        }
        if ($strength >= 8 ) {
        $vowels .= '@#$%';
        }
         
        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return str_shuffle($password);
    }
?>