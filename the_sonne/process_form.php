<?php
    session_start();
    $_SESSION['csv_output'] = null;
    $_SESSION['row'] = null;
    $file = $_FILES['csv_file'];
    /**
     *echo '<pre>';
    var_dump($_FILES);
    echo '</pre>'; 
     */
    $error = false;
    if ($file && $file['error']==0) {
        // set file_name
        $filename = $file['name'];
        $filetype = $file['type'];
        
        // check valid file
        /*
        if ($filetype != 'text/csv') {
            $error = true;
        }
        */
        $endpos = strrpos($filename, '.');
        $ext = substr($filename, $endpos+1);
        if (strtolower($ext) != 'csv') {
            $error = true;
        }
        if ($error) {
            echo 'File is not invalid';
            die();
        }
        
        // upload file
        $process = move_uploaded_file($file['tmp_name'], 'csv_upload/'.$filename);
        if ($process) {
            // add csv file to datebase
            
            $row = 1;
            if (($handle = fopen('csv_upload/'.$filename, "r")) !== FALSE) {
                // connect database
                require('database.php');
                // add functions to create code
                require('functions.php');
                // create a csv file
                $fp = fopen('csv_output/'.$filename, 'w') or die('Can not create a csv file');
                // To count number of rows is inserted
                $j = 0;
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    // read file succesfully
                    $num = count($data);
                    //echo "<p> $num fields in line $row: <br /></p>\n";
                    // check account existed
                    $query = "SELECT * FROM pct_customers WHERE email='$data[3]'";
                    $result = mysql_query($query);
                    
                    if (!mysql_numrows($result)) {
                        // add fields to database
                        $code = pct_generate_code();
                        if ($row>2) {
                            $query = "INSERT INTO pct_customers (name, firstname, lastname, email, website, group_id, created, code)
                                            VALUES('$data[0]','$data[1]','$data[2]','$data[3]','$data[4]',".(int)$data[5].",NOW(),'$code' )";
                            if (mysql_query($query)) {
                                $j++;
                                $id = mysql_insert_id();
                                $id = base64_encode($id);
                                $code = base64_encode($code);
                                // create a unique url
                                $url = SONNE_URL."optout.php?id=$id&code=$code";
                                // output a csv file
                                $fields = $data;
                                $fields[] = $url;
                                fputcsv($fp, $fields);
                            } else {
                                die(mysql_error());
                            }
                        } elseif ($row==1) {
                            $fields = $data;
                            $fields[] = 'Unique Url';
                            fputcsv($fp, $fields);
                        } else {
                            $fields = $data;
                            $fields[] = '';
                            fputcsv($fp, $fields);
                        }
                    }
                    $row++;
                }
                if ($j) {
                    // download the csv file
                    $_SESSION['csv_output'] = $filename;
                    $_SESSION['row'] = $j;
                }
                header('Location: index.php');
                fclose($handle);
            }            
        } else {
            // return error
            echo "Can't upload file";
        }
    } else {
        header('Location: index.php');
    }

?>