<?php
    $id = $_GET['id'];
    $code = $_GET['code'];
    $id = (int)base64_decode($id);
    $code = base64_decode($code);
    $code = mysql_escape_string($code);
    $task = $_GET['task'];
    
    // check valid page
    require('database.php');
    $query = "SELECT * FROM pct_customers
                WHERE id=$id
                AND code='$code'";
    $result = mysql_query($query);
    $row = mysql_fetch_assoc($result);
    /*
    echo '<pre>';
    var_dump($row);
    echo '</pre>';
    */
    if (!$row) {
        die('Invalid Page');
    }
    
    $optout_url = "optout.php?id={$_GET['id']}&code={$_GET['code']}&task=optout";
    // optout status
    $optout = false;
    if ($task == 'optout') {
        $query = "UPDATE pct_customers
                    SET state=0, modified=NOW()
                    WHERE id=$id
                    AND code='$code'";
        mysql_query($query) or die(mysql_error());
        $optout = true;
    }
    if ($task == 'optin') {
        $query = "UPDATE pct_customers
                    SET state=2, modified=NOW()
                    WHERE id=$id
                    AND code='$code'";
        mysql_query($query) or die(mysql_error());
        $optin = true;
    }
    
?>
<?php require('templates/header.php'); ?>
<div id="maincontent">
    <div class="upload_form">
        <?php if ($optin) : ?>
            <div><strong>You have opted in successfully</strong></div>
        <?php elseif (!$optout) : ?>
        <div>
            <h3>Bienvenidos al programa Green Earth Appeal</h3>
            <p>En Green Earth Appeal estamos encantados de habernos asociado con Sonne Energ�ticos para ayudar a algunas de las comunidades m�s desfavorecidas  en el mundo, convirti�ndolas en autosuficientes y al mismo tiempo compensando la huella de carbono de su negocio.</p>
            <p>Lamentamos que haya optado por no formar parte de esta valiosa  iniciativa mundial, por lo que antes de que pulse el bot�n de confirmaci�n,  quisi�ramos que por favor se tomara un momento para leer un poco m�s acerca del impacto que cada $15  pesos  tendr�n para ayudar a algunas de las personas m�s pobres de nuestro planeta.</p>
            <p>Cada donaci�n de $15 pesos proveer� un �rbol y contribuir�  a los programas gratuitos de formaci�n a distancia y a los equipos de personas en campo dedicados a entrenar y apoyar a los miembros de las comunidades marginadas a construir una vida mejor para s� mismos.</p>
            <p>�Por qu� plantar �rboles?</p>
            <p>Porque cambian vidas. Los �rboles proporcionan alimentos, forraje para los animales y  madera para combustible y construcci�n. Adem�s aumentan la producci�n agr�cola, mejoran la infiltraci�n del agua y la recarga de los acu�feros. De igual forma, protegen los suelos de la erosi�n e�lica e h�drica; as� tambi�n, absorben el  CO2 de la atmosfera.</p>
            <p>Mediante el apoyo a nuestra asociaci�n con Sonne Energ�ticos, usted estar� dando mayor impulso al compromiso que ya tiene con el medio ambiente,  reciclando su aceite vegetal usado.</p>
            <p>Sonne Energ�ticos est� comprometido a ayudar a estas comunidades necesitadas a convertirse en autosuficientes e igualar� cada $15 pesos  que usted done con $ 15  pesos adicionales, proporcionando cada mes dos �rboles en su nombre.</p>
            <p>Esperamos que le hayamos convencido de los grandes beneficios que podemos lograr con su peque�a pero valiosa contribuci�n y de que va a apoyar esta extraordinaria iniciativa.</p>
            <p>Gracias</p>
            <p>&nbsp;</p>
            <p>Si usted no desea participar, por favor haga clic en el siguiente enlace <a href="<?php echo $optout_url; ?>">de salida</a></p>
        </div>
        <?php else :?>
            <div><strong>Usted ha confirmado que no desea participar</strong></div>
        <?php endif; ?>
    </div>
</div>
<?php require('templates/footer.php'); ?>        