<?php
class eposnowClass{
	
	function __construct() 
	{
		//ePOSnow API keys
		$this->client_id 	 = "2676E173959E43709C901A7DA2ED59C4";
		$this->client_secret = "E724D73F9523429C8937EA5F374813CF";
		
	}
	
	/********************************************* 
		To get The access token 
	**********************************************/
	
	function getAccessToken() // to get product receipts 
	{                      
		$client_id 		= $this->client_id;
		$client_secret 	= $this->client_secret;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://developer.eposnowhq.com/Docs/Base64Encode?secret=".$client_secret."&key=".$client_id);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$headers = array();
		//$headers[] = 'Authorization: Basic '.base64_encode($client_id.':'.$client_secret);
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		$token = json_decode($result,true);
		//echo "<pre>"; print_r($result); 
		return $token;	
	} 
	
	/********************************************* 
	To get Transaction data according to the dates 
	**********************************************/
	
	function getDailyTransactionsByDate( $access_token, $start_date, $end_date ) // to get product receipts 
	{     
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.eposnowhq.com/api/v4/Transaction/GetByDate?startDate='.$start_date.'&endDate='.$end_date);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$headers = array();
		$headers[] = 'Authorization: Basic ' . $access_token;
		$headers[] = 'Content-Type: application/xml';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		return $result;	
	} 
	
	function randomKey( $length )  		// to create random alphanumeric code
	{ 					
		$key='';
		$pool = array_merge(range(0,9),range('A', 'Z'));

		for($i=0; $i < $length; $i++) {
			$key .= $pool[mt_rand(0, count($pool) - 1)];
		}
		return $key;
	}
	
}