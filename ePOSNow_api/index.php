<?php
/**
 * Created by Atul Rana
 * API- ePOSnow API
 * Date: 02/12/2019
 */

require_once("db.php");
require_once("functions.php");

$eposnow = new eposnowClass();

$access_token = $eposnow->getAccessToken();      // to create access token

if(isset($_GET['StartDate']) && !empty($_GET['StartDate'])){
	 $start_date = $_GET['StartDate'];
}else{
	$today = date('Y-m-d');
	$start_date  = date('Y-m-d', strtotime($today .' -1 day'));    //previous day date 
}
if(isset($_GET['EndDate']) && !empty($_GET['EndDate'])){
	 $end_date = $_GET['EndDate'];
}else{
	 $end_date = date('Y-m-d');
}

$result = $eposnow->getDailyTransactionsByDate( $access_token, $start_date, $end_date );

$response = json_decode($result, true);

echo "<h3>Transactions Data: </h3>";
echo "<pre>"; print_r($response);
