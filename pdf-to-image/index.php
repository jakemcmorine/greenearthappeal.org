<?php 

if (!extension_loaded('imagick'))
    echo 'Imagick extension is not installed';

$im = new imagick();
$im->setResolution(300, 300);
$im->readImage("Kingston-Smith.pdf");
$im->setImageColorspace(13); 
$im->setImageCompression(Imagick::COMPRESSION_JPEG); 
$im->setImageCompressionQuality(80);
$im->setImageFormat('jpeg'); 
$im->resizeImage(1199, 1771, imagick::FILTER_LANCZOS, 1);  
$im->writeImage('full_kingston_smith111.jpg');  
$im->resizeImage(300, 443, imagick::FILTER_LANCZOS, 1);  
$im->writeImage('thumb_kingston_smith111.jpg'); 
$im->clear(); 
$im->destroy(); 
return ;