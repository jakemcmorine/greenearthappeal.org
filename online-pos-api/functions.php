<?php
/**
 * Created by Atul.
 * API- Online POS API
 * Date: 09/10/2018
 */
class onlineposClass{
	
	/* to get sale product receipts */
	
	function getPosSalesData($row, $from, $to, $offset) {
		
			$firmaid 		=   $row['firmaid'];
			$auth_token	    =   $row['token'];
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://api.onlinepos.dk/api/exportKoncern?from=".$from."&to=".$to."&offset=".$offset."&json=1");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			$headers = array();
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
			$headers[] = "firmaid: ".$firmaid;
			$headers[] = "token: ".$auth_token;
			$headers[] = "Accept: */*";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			} 
			curl_close ($ch);
			return $result;	
	}

	/* function to create 5 digit random alphanumeric code */
	
	function randomKey($length) {
		
		$key='';
		$pool = array_merge(range(0,9),range('A', 'Z'));

		for($i=0; $i < $length; $i++) {
			$key .= $pool[mt_rand(0, count($pool) - 1)];
		}
		return $key;
	}
	
}