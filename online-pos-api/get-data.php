<?php
/**
 * Created by Atul.
 * API- Online POS API
 * Date: 09/10/2018
 */
require_once("db.php");

require_once("functions.php");

$pos = new onlineposClass();

$sql = "SELECT * FROM pos_api_credentials WHERE active = '1'";

$result = mysql_query($sql);

 if(mysql_num_rows($result) > 0) {
	 
    while($row = mysql_fetch_assoc($result)) {
		// echo "<pre>"; print_r($row); die;
		 
			if(isset($_GET['from']) && !empty($_GET['from'])){
				 $fromdate = $_GET['from'];
				 $from = strtotime($fromdate);
			}else{
				$today = date('Y-m-d');
			    $fromdate = date('Y-m-d', strtotime($today .' -1 day'));    //previous day date
			    $from = strtotime($fromdate);    
			}
			if(isset($_GET['to']) && !empty($_GET['to'])){
				 $to = strtotime($_GET['to']);
			}else{
				 $to = strtotime(date('Y-m-d'));
			}
			
			 $query = "Select * from `pos_api_data` where date = '" . $fromdate . "' AND companyid = '" .  $row['companyid'] . "'";
							 
			 $dbresult = mysql_query($query);
			 
			 $num_rows = mysql_num_rows($dbresult);
			 
			 $random_no = $pos->randomKey(5);
			 
			 if($num_rows > 0){
				 
						  echo "<br/><h4><=========== Data Already Exists For ".$fromdate." (Company ID: ".$row['companyid']."): ============></h4><br/>";
				 
						 $success_msg = $fromdate." - Data Already Exists";
						 
						 $api_logs_query = "INSERT INTO `pos_api_logs`(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $success_msg . "')";
						 
						 mysql_query($api_logs_query);
					 
				}else{
					
					$offset = 0;
					
					for($i=0; $i<=10; $i++){
						
						$offset = $i*1000;
						
						$receipts_res = $pos->getPosSalesData($row, $from, $to, $offset);
												
						$receipts_res_encoded = mysql_real_escape_string($receipts_res);
						
						$Receipts_result = json_decode($receipts_res);
						$error_msg1 = "";
						if(!is_object($Receipts_result)){
							$error_msg = $receipts_res;	       		 //if error exist;
						}
													
						 //echo "<pre>"; print_r($Receipts_result); die("here");
						
						$receipts = (array)$Receipts_result;
						
						
						$nocfd = 0;
						$noreceipts = 0;
						$check_value = array();
						if($row['productid']!=""){
							foreach($receipts['sales'] as $key=>$value){
								$line = $receipts['sales'][$key]->line;
								if($row['companyid'] == $line->firma_id){
									if(!isset( $check_value[$line->chk])){
									  $check_value[$line->chk] = 1;
									  $noreceipts++;
									}
									if($line->product_id == $row['productid']){
											//$nocfd+= $line->amount;
											$nocfd+= 1;
									}
								}
							}
						}
						$random_no = $pos->randomKey(5);
							//echo $noreceipts.' '.$nocfd."<br>";
						if(isset($receipts) && !empty($receipts)){
							
								$sql_query1 = "INSERT INTO `pos_api_data`(`companyid`, `date`, `payload`, `noreceipts`, `nocfd`) VALUES ('" . $row['companyid'] . "','" . $fromdate . "', '" . $receipts_res_encoded . "','" . $noreceipts . "', '" . $nocfd . "')";
								
								 mysql_query($sql_query1);
								 
								 $success_msg = $fromdate." - Data Imported";
								 
								 $api_logs_query = "INSERT INTO `pos_api_logs`(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $success_msg . "')";
								 
								 mysql_query($api_logs_query);
								 
								 echo "<br/><h4><=========== Receipts for ".$fromdate." (Company ID: ".$row['companyid']."): ============></h4><br/>";
								 echo $receipts_res;
								 
						}elseif($error_msg1!=""){
							
							
							 $sql_query = "INSERT INTO `pos_api_logs`(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $error_msg . "')";
							 mysql_query($sql_query);
							 
							 echo 'Error: '.$error_msg;
							 
						}elseif(empty($receipts)){
							
							 if($i==0){     
								 echo "<br/><h4><=========== No Data found for ".$fromdate." (Company ID: ".$row['companyid']."): ============></h4><br/>";
								 
								 $success_msg = $fromdate." - No Data Found";
								 
								 $api_logs_query = "INSERT INTO `pos_api_logs`(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $success_msg . "')";
									 
									mysql_query($api_logs_query);
							 }
							 break;
							 
						}
					}
			}
			
    }
} 
exit;
