<?php
/**
 * Created by Atul.
 * API- CFD Tevalis API
 * Date: 03/12/2018
 */
require_once("db.php");

require_once("functions.php");

$tevalis = new cfdtevalisClass();

$sql = "SELECT * FROM tevalis_api_credentials WHERE active = '1'";

$result = mysql_query($sql);

 if(mysql_num_rows($result) > 0) {
	 
    while($row = mysql_fetch_assoc($result)) {
		// echo "<pre>"; print_r($row); die;
		 
			if(isset($_GET['from']) && !empty($_GET['from'])){
				 $fromdate = $_GET['from'];
				 $from = $fromdate;
			}else{
				$today = date('Y-m-d');
			    $fromdate = date('Y-m-d', strtotime($today .' -1 day'));    //previous day date
			    $from = $fromdate;    
			}
			if(isset($_GET['to']) && !empty($_GET['to'])){
				 $to = $_GET['to'];
			}else{
				 $to = date('Y-m-d');
			}
			
			 $query = "Select * from `tevalis_api_data` where date = '" . $fromdate . "' AND companyid = '" .  $row['companyid'] . "' AND siteid = '" . $row['siteid'] . "'";
							 
			 $dbresult = mysql_query($query);
			 
			 $num_rows = mysql_num_rows($dbresult);
			 
			 $random_no = $tevalis->randomKey(5);
			 
			 if($num_rows > 0){
				 
						  echo "<br/><h4><=========== Data Already Exists For ".$fromdate." (Company ID: ".$row['companyid']."/".$row['siteid']."): ============></h4><br/>";
				 
						 $success_msg = $fromdate." - Data Already Exists";
						 
						 $api_logs_query = "INSERT INTO `tevalis_api_logs`(`pid`, `companyid`, `siteid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $row['siteid'] . "', '" . $success_msg . "')";
						 
						 mysql_query($api_logs_query);
					 
				}else{
					
						$offset = 0;
						$i=0 ;
						
						$receipts_res = $tevalis->getTevalisSalesData($row, $from, $to );
												
						$receipts_res_encoded = mysql_real_escape_string($receipts_res);
						
						$Receipts_result = json_decode($receipts_res);
						
						$receipts = (array)$Receipts_result;
						
						$nocfd = 0;
						$noreceipts = 0;
						if($row['productid']!=""){
							foreach($receipts['transactions'] as $key=>$value){
								$lines = $receipts['transactions'][$key]->BillItemInfos;
								$noreceipts+= 1;
								 if($lines){
									 foreach($lines as $line){
										if($line->ItemCode == $row['productid'] && $line->IsSold == 1){              // Count if IsSold = true only
											   //echo "<pre>"; print_r($lines); 
												$nocfd+= $line->QtySold;
												//$nocfd+= 1; 
										}
									} 
								 }
							}
						}
						$random_no = $tevalis->randomKey(5);
							//echo $noreceipts.' '.$nocfd; die;
						if(isset($receipts) && !empty($receipts)){
							
								$sql_query1 = "INSERT INTO `tevalis_api_data`(`companyid`, `siteid`, `date`, `payload`, `noreceipts`, `nocfd`) VALUES ('" . $row['companyid'] . "','" . $row['siteid'] . "','" . $fromdate . "', '" . $receipts_res_encoded . "','" . $noreceipts . "', '" . $nocfd . "')";
								
								 mysql_query($sql_query1);
								 
								 $success_msg = $fromdate." - Data Imported";
								 
								 $api_logs_query = "INSERT INTO `tevalis_api_logs`(`pid`, `companyid`, `siteid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $row['siteid'] . "', '" . $success_msg . "')";
								 
								 mysql_query($api_logs_query);
								 
								 echo "<br/><h4><=========== Receipts for ".$fromdate." (Company ID: ".$row['companyid']."/".$row['siteid']."): ============></h4><br/>";
								 echo $receipts_res;
								 
						}elseif(isset($Receipts_result->description) && $Receipts_result->description!=""){
							
							 $error_msg = $Receipts_result->description;
							
							 $sql_query = "INSERT INTO `tevalis_api_logs`(`pid`, `companyid`, `siteid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "','" . $row['siteid'] . "', '" . $error_msg . "')";
							 mysql_query($sql_query);
							 
							 echo 'Error: '.$error_msg;
							 
						}elseif(empty($receipts)){
							
							 if($i==0){     
								 echo "<br/><h4><=========== No Data found for ".$fromdate." (Company ID: ".$row['companyid']."/".$row['siteid']."): ============></h4><br/>";
								 
								 $success_msg = $fromdate." - No Data Found";
								 
								 $api_logs_query = "INSERT INTO `tevalis_api_logs`(`pid`, `companyid`, `siteid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $row['siteid'] . "', '" . $success_msg . "')";
									 
									mysql_query($api_logs_query);
							 }
							 break;
							 
						}
			}
			
    }
} 
exit;
