<?php
/**
 * Created by Atul.
 * API- CFD Tevalis API
 * Date: 03/12/2018
 */
class cfdtevalisClass{
	
	/* to get sale product receipts */
	
	function getTevalisSalesData($row, $from, $to) {
		
			$companyid 		=   $row['companyid'];
			$devid 		    =   $row['devid'];
			$guid 			=   $row['guid'];
			$siteid	    	=   $row['siteid'];
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://api.tevalis.com/Sales/GetSalesExport/".$siteid."?StartDate=".$from."%2004:00&EndDate=".$to."%2004:00");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			//curl_setopt($ch, CURLOPT_POST, 1);
			$headers = array();
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
			$headers[] = "CompanyId: ".$companyid;
			$headers[] = "GUID: ".$guid;
			$headers[] = "DevID: ".$devid;
			$headers[] = "Accept: */*";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			} 
			curl_close ($ch);
			return $result;	
	}

	/* function to create 5 digit random alphanumeric code */
	
	function randomKey($length) {
		
		$key='';
		$pool = array_merge(range(0,9),range('A', 'Z'));

		for($i=0; $i < $length; $i++) {
			$key .= $pool[mt_rand(0, count($pool) - 1)];
		}
		return $key;
	}
	
}