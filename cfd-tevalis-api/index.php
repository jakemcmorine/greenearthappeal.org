<?php
function callAPI($method, $url, $data){
   $curl = curl_init();

   switch ($method){
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
         break;
      default:
         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   }

   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'CompanyId: 767',
      'GUID: TH69227',
      'DevID: FOO20181107_160577',
      'Content-Type: application/json'
   ));
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

   // EXECUTE:
   $result = curl_exec($curl);
   if (curl_error($curl)) {
		echo  $error_msg = curl_error($curl);
	}
   
   if(!$result){
	   die("Connection Failure");
	}
	//echo "<pre>"; print_r($result);
   curl_close($curl);
   return $result;
}

$get_data = callAPI('GET', 'https://api.tevalis.com/Sales/GetSalesExport/1754?StartDate=2018-11-30&EndDate=2018-12-03', false);
// $get_data = callAPI('GET', 'https://api.tevalis.com/sites', false);
$response = json_decode($get_data, true);

if(!empty($response)){	
	echo "<pre>"; print_r($response);
}
