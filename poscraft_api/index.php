<?php
/****************************
 * Created by Atul Rana
 * API- POSCrafts API
 * Date: 06/03/2020
****************************/

require_once("db.php");

require_once("functions.php");

$poscraft = new poscraftClass();

$sql = "SELECT * FROM `poscraft_api_credentials` WHERE active = '1'";

if(isset($_GET['StartDate']) && !empty($_GET['StartDate'])){
	
	$start_date = $_GET['StartDate'];
	
}else{
	
	$today = date('Y-m-d');
	$start_date  = date('Y-m-d', strtotime($today .' -1 day'));    //previous day date 
	
}

if(isset($_GET['EndDate']) && !empty($_GET['EndDate'])){
	
	$end_date = $_GET['EndDate'];
	 
}else{
	
	$end_date = date('Y-m-d');
	 
}

$result = mysql_query($sql);

if(mysql_num_rows($result) > 0) {
	 
    while($row = mysql_fetch_assoc($result)) {

		    $query = "Select * from `poscraft_api_data` where date = '" . $start_date . "' AND companyid = '" .  $row['companyid'] . "'";
							 
			$dbresult = mysql_query($query);
			 
			$num_rows = mysql_num_rows($dbresult);
			 
			$random_no = $poscraft->randomKey(5);
			 
			if($num_rows > 0){
			 
					echo "<br/><h4><=========== Data Already Exists For ".$start_date." (Company ID: ".$row['companyid']."): ============></h4><br/>";
			 
					$success_msg = $start_date." - Data Already Exists";
					 
					$api_logs_query = "INSERT INTO `poscraft_api_logs `(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $success_msg . "')";
					 
					mysql_query($api_logs_query);
				 
			}else{
				
					$offset = $i = 0;
					
					$receipts_res = $poscraft->getDailyTransactionsByDate($row, $start_date, $end_date, "report" );
											
					$receipts_res_encoded = mysql_real_escape_string($receipts_res);
					
					$receipts = json_decode($receipts_res, true);
						
					//echo "<pre>"; print_r($receipts); die;	
					$nocfd = $noreceipts = 0;
					$nocfd  =  count($receipts);
					$random_no = $poscraft->randomKey(5);
					
					if(is_array($receipts)){
												
							$sql_query1 = "INSERT INTO `poscraft_api_data` (`companyid`, `date`, `payload`, `noreceipts`, `nocfd`) VALUES ('" . $row['companyid'] . "','" . $start_date . "', '" . $receipts_res_encoded . "','" . $noreceipts . "', '" . $nocfd . "')";
							
							mysql_query($sql_query1);
							 
							$success_msg = $start_date." - Data Imported";
							 
							$api_logs_query = "INSERT INTO `poscraft_api_logs` (`pid`, `companyid`,`logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $success_msg . "')";
							 
							mysql_query($api_logs_query);
							 
							echo "<br/><h4><=========== Receipts for ".$start_date." (Company ID: ".$row['companyid']."): ============></h4><br/>";
							echo $receipts_res;
							 
					}elseif(isset($receipts) && $receipts!=""){
						
						$error_msg = $receipts;
						
						$sql_query = "INSERT INTO `poscraft_api_logs` (`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $error_msg . "')";
						mysql_query($sql_query);
						 
						echo 'Error: '.$error_msg;
						 
					}elseif(empty($receipts)){
						
						if($i == 0){
							
							echo "<br/><h4><=========== No Data found for ".$start_date." (Company ID: ".$row['companyid']."): ============></h4><br/>";
							 
							$success_msg = $start_date." - No Data Found";
							 
							$api_logs_query = "INSERT INTO `poscraft_api_logs` (`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $success_msg . "')";
								 
							mysql_query($api_logs_query);
						}
						break;
						 
					}
			}
	}
 }
 