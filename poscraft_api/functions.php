<?php
class poscraftClass{
	
	/********************************************* 
	To get Transaction data according to the dates 
	**********************************************/
	
	function getDailyTransactionsByDate( $row, $start_date, $end_date, $type ) // to get product receipts 
	{
	
		$email 			=  $row['username'];
		$password   	=  $row['password'];
		$serveraddress  =  $row['serveraddress'];
		
		$ch = curl_init();
		if($type == "summary"){
			curl_setopt($ch, CURLOPT_URL, "http://connect.poscraft.co.uk/api/data/Reports/CarbonFreeSummary?CompanyId=18");
		}else{
			$url = $serveraddress."/api/data/Reports/CarbonFreeReport?CompanyId=".$row['companyid']."&StartDate=".$start_date."&EndDate=".$end_date;
			curl_setopt($ch, CURLOPT_URL, "".$url."");
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 

		$headers = array("Content-Type:application/json", "Accept:application/json");
		$headers[] = "email: ".$email;
		$headers[] = "password: ".$password;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo  'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		return $result;
	} 
	 
	function randomKey( $length )  		// to create random alphanumeric code
	{ 					
		$key = '';
		$pool = array_merge(range(0,9),range('A', 'Z'));

		for($i=0; $i < $length; $i++) {
			$key .= $pool[mt_rand(0, count($pool) - 1)];
		}
		return $key;
	}
	
}