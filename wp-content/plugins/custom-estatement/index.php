<?php
/*
  Plugin Name: eStatement Tree Planting
  Plugin URI: https://www.greenearthappeal.org
  Description: NewDay eStatement Tree Planting
  Version: 1.0
  Author: Atul Rana
  Author URI: https://www.greenearthappeal.org
 */
 
add_action('wp_enqueue_scripts', 'eu_register_script');
function eu_register_script() {
	wp_register_script('custom_jquery', plugin_dir_url( __FILE__ ) . 'js/jquery.min.js', array('jquery'), '0.2', true);
}
// use the registered jquery and style above
add_action('wp_enqueue_scripts', 'eu_enqueue_style');
function eu_enqueue_style(){
	$dir = plugin_dir_url( __FILE__ );
    wp_enqueue_style('custom-style', $dir . 'css/style.css', array(), '0.1.0', 'all');
}

/* load form */
function newday_estatement_form(){
	ob_start();
    include_once "templates/estatement-form.php";
    return ob_get_clean();
}
add_shortcode('newday-estatement-form', 'newday_estatement_form');

add_action( 'wp_ajax_estatement_process', 'estatement_process_callback' );
add_action( 'wp_ajax_nopriv_estatement_process', 'estatement_process_callback' );

/* Insert data in the table newday_estatement_tree_plant_data*/

function estatement_process_callback() {
	
	global $wpdb;
	$result = [];
	$tablename = $wpdb->prefix.'newday_estatement_tree_plant_data';
	 
	if (!filter_var(trim($_POST['email_address']), FILTER_VALIDATE_EMAIL)) {
		$result['status'] = 'Invalidemail';
		echo json_encode($result);
		exit;
    }
	$check_record  = $wpdb->get_row('SELECT * FROM `'.$tablename.'` WHERE email_address = "'.$_POST['email_address'].'"', ARRAY_A);
	 
	if (empty($check_record)) {
		
		$data = array(
			'first_name' 	=> sanitize_text_field( $_POST['first_name'] ), 
			'last_name' 	=> sanitize_text_field( $_POST['last_name'] ),
			'panacea_id' 	=> sanitize_text_field( $_POST['panacea_partner_id'] ),
			'email_address' => sanitize_text_field( $_POST['email_address'] ), 
		);
		
		$wpdb->insert( $tablename, $data); 
		$insert_id = $wpdb->insert_id;	
		
	}else{
		$insert_id = $check_record['id'];
	}		
	
	if ($insert_id) {
		$result['status'] = 'success';
	}else{
		$result['status'] = 'failed';
	}
	
	echo json_encode($result);
	exit;
}