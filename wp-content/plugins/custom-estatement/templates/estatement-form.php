<div class="estatement-registration">
	<div class="screen-reader-response"></div>
	<form action="" method="POST" class="custom-form" id="estatement_form" enctype="multipart/form-data">
		<input type="hidden" name="action" value="estatement_process">
		<input type="hidden" name="panacea_partner_id" value="38121">
		<label>First Name <span class="red" data-tip="Required field">*</span></label> 
		<span class="form-control">
			<input name="first_name" value="" size="40" class="form-control" placeholder="First Name" type="text" required=""/>
		</span>
		<label>Last Name <span class="red" data-tip="Required field">*</span></label> 
		<span class="form-control">
			<input name="last_name" value="" size="40" class="form-control" placeholder="Last Name" type="text" required=""/>
		</span>
		<label>Email Address <span class="red" data-tip="Required field">*</span></label> 
		<span class="form-control">
			<input name="email_address" value="" size="40" class="form-control" placeholder="Email Address" type="email" required=""/>
		</span>
		<div class="privacy_policy_estatement">
			<p>In order to provide you the content requested, we need to store and process your personal data. If you consent to us storing your personal data for this purpose, please tick the checkbox below.<br/><br/>
			<input type="checkbox" name="term_condition" value="1" required=""/>I agree to allow The Green Earth Appeal to store and process my data.<br/><br/>
			You can unsubscribe from these communications at any time. For more information on how to unsubscribe, our privacy practices, and how we are committed to protecting and respecting your privacy, please review our <a target="_blank" href="https://greenearthappeal.org/privacy-policy">Privacy Policy.</a></p>
		</div>
		<div id="success_msg" class="text-center"></div>
		<span class="form-control">
			<center><input value="Submit" class="form-control" type="submit"></center>
		</span>
	</form>
</div>
<script>
var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
jQuery("#estatement_form").submit(function(e) {
	e.preventDefault();
	jQuery("#success_msg").text("Please wait...");
	  jQuery.ajax({
			url: ajaxurl,
			type: "post",
			data: jQuery(this).serialize(),
			success: function(response) {
				var res = JSON.parse(response);
				if(res.status=='success'){
				 // jQuery("#success_msg").text("Record submitted successfully!");
				   window.location = "https://greenearthappeal.org/thank-you-for-using-estatements";	
				/*   setTimeout(function(){ 
					jQuery("#estatement_form").trigger("reset");
				  }, 2000); */
				 // return true;
				}else if(res.status=='Invalidemail'){
					jQuery("#success_msg").text("Invalid email address!");
					return false;
				}else{
					jQuery("#success_msg").text("Something went wrong!");
					return false;
				}
			}
		});	
}); 
</script>