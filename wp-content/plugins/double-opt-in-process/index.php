<?php
/*
  Plugin Name: The Green Organisation Double Opt In process
  Plugin URI: https://greenearthappeal.org/
  Description: Double Opt In process
  Version: 2.0
  Author: Marvin Baker
  Author URI: https://greenearthappeal.org/
 */
 
add_action('wp_enqueue_scripts', 'register_opted_script');
function register_opted_script() {

	//wp_register_script( 'custom_jquery', plugins_url('/js/core.min.js', __FILE__), array('jquery'), '1.1', true );
}
// use the registered jquery and style above
add_action('wp_enqueue_scripts', 'enqueue_opted_style');
function enqueue_opted_style(){
	//wp_enqueue_style( 'new_styles' );
    wp_enqueue_script('custom_jquery');
}


function double_opt_in_process(){
	global $wpdb;
    include_once "templates/double_opt_in_upload.php";
}
add_shortcode('double-opt-in-process', 'double_opt_in_process');

