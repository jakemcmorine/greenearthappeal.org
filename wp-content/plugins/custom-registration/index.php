<?php
/*
  Plugin Name: Custom Registration
  Plugin URI: https://www.greenearthappeal.org
  Description: Tree Granting Application Registration
  Version: 1.0
  Author: Marvin Baker
  Author URI: https://www.greenearthappeal.org
 */
 
  function registration_form($password, $email,$verify_email_address, $website, $first_name, $surname,$address ,$organisation_name,$registration_info,$registration_no,$no_of_trees,$project_fund,$hear_about_us,$search_for,$introduced_you,$others,$type_of_fund){
	  global $reg_errors;
	  
	  ?> 
<style>
.h1_class {
  color: #d5b000;
  font-size: 40px;
  text-align: center;
}
.h2_class {
  font-size: 32px;
  text-align: center;
  text-transform: none;
}
.h3_class {
  text-align: center;
  margin-top: 0px;
  font-size: 28px;
}
.page-id-13859 #content {  padding: 25px 0 60px!important;}
.wh_radio .wpcf7-list-item input {
    float: left;
    margin-top: 0px;
    width: auto;
	margin-right: 10px;
    height: auto !important;
}
.wh_radio .wpcf7-list-item
{
	float:left;
	width:100%;
	
}
.wh_rad .wpcf7-list-item-label {
  float: left;
}
.wh_rad input {
  float: left;
  width:18px!important;
}

.plant-registration input {
    background-color: #f5f5f5;
	border: 1px solid #ccc;
	box-shadow: 0 1px 3px #ddd inset;
	color: #555;
    font-size: 14px;
    height: 45px !important;
    margin-bottom: 0;
    padding: 5px;
    width: 98%;
    max-width: 708.85px;
}
.plant-registration input[type="number"] {
    height: 33px !important;
}
.plant-registration span.wpcf7-list-item {
    display: flex;
    align-items: center;
    justify-content: flex-start;
}
.plant-registration input[type="file"] {
    color: #999;
    font-size: 18px;
    padding-top: 11px;
    width: auto;
    height: auto !important;
    padding-bottom: 11px;
}

div.plant-registration input[type="file"] {
    cursor: pointer;
}
.plant-registration input[type="radio"] {
  box-shadow: none;
  cursor: pointer;
  display: inline;
  margin: 0;
  width: 20px;
  height: auto;
  margin-top: 0px;
  margin-right: 10px;
}

.plant-registration input[type="submit"] {
    background-color: #000;
    border: 2px solid #000;
    box-shadow: none;
    color: #d5b000;
    cursor: pointer;
    font-size: 0.95em;
    font-weight: 600;
    height: 50px;
    margin-top: 10px;
    text-transform: uppercase;
    transition: all 0.3s ease-in-out 0s;
    width: 110px;
}
.plant-registration form {
    background-color: #fcfcfc;
    border: 1px solid #f4f4f4;
    box-shadow: 1px 1px 5px 1px #eee;
    padding: 20px;
}
.plant-registration h5 {
    color: #d5b000;
}
.plant-registration {
    margin: auto;
    width: 80%;
}
.plant-registration  label {
    color: #292929;
    display: block;
    font-weight: bold;
    margin-bottom: 5px;
    margin-top: 20px;
}

.plant-registration select {
    background-color: #f5f5f5;
    border: 2px solid #ccc;
    box-shadow: 0 1px 3px #ddd inset;
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 0px;
    padding: 10px;
    width: 98%;
}
.red{
	color:red;
}
  [data-tip] {
	position:relative;

}
[data-tip]:before {
	content:'';
	/* hides the tooltip when not hovered */
	display:none;
	content:'';
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	border-bottom: 5px solid #1a1a1a;	
	position:absolute;
	top:30px;
	left:35px;
	z-index:8;
	font-size:0;
	line-height:0;
	width:0;
	height:0;
}
[data-tip]:after {
	display:none;
	content:attr(data-tip);
	position:absolute;
	top:35px;
	left:0px;
	padding:5px 8px;
	background:#1a1a1a;
	color:#fff;
	z-index:9;
	font-size: 0.75em;
	height:18px;
	line-height:18px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	white-space:nowrap;
	word-wrap:normal;
}
[data-tip]:hover:before,
[data-tip]:hover:after {
	display:block;
}
.form-control.required {
  border: 1px solid red;
}

@media screen and (max-width:767px)
{
	.plant-registration input[type="file"] {
   
    width: 215px!important;
}
	
}
</style>

		<!--h2 class="post-title entry-title">User/Organisation Registration </h2-->
    	<div class="plant-registration">
		 <div class="screen-reader-response"></div>
		 <form action="" method="POST" class="custom-form" enctype="multipart/form-data">
		 <h5>User Details</h5>
		 <p><label>First Name <span class="red" data-tip="Required field">*</span><br>
			   <span class="form-control"><input name="first_name" value="<?php if (isset( $_POST['first_name'] )) { echo $first_name; }  ?>" size="40" class="form-control <?php if(isset($reg_errors) && $reg_errors->get_error_messages('first_name')){ echo 'required' ; }?>"  placeholder="First Name" type="text" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('first_name')[0])){ echo 'autofocus' ; } ?>/></span>
			   </label>  
			   <span class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('first_name')[0])){ echo  $reg_errors->get_error_messages('first_name')[0] ; } ?></span>
			</p>
			<p><label>Surname <span class="red" data-tip="Required field">*</span><br>
			   <span class="form-control"><input name="surname" value="<?php if (isset( $_POST['surname'] )) { echo $surname; }  ?>" size="40" class="form-control <?php if(isset($reg_errors) && $reg_errors->get_error_messages('surname')){ echo 'required' ; }?>"  placeholder="Surname" type="text" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('surname')[0])){ echo 'autofocus' ; } ?>></span>
			   </label>
				 <span class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('surname')[0])){ echo  $reg_errors->get_error_messages('surname')[0] ; } ?></span>	
			</p>
			<p><label>Email Address <span class="red" data-tip="Required field">*</span><br>
			   <span class="form-control">
			   <input name="email_address"  onkeyup="validateEmail(this.value)" onpaste="return false;" onCopy="return false" id="email"  value="<?php if (isset( $_POST['email_address'] )) { echo $email; }  ?>" class="form-control <?php if(isset($reg_errors) && $reg_errors->get_error_messages('email')){ echo 'required' ; }?>"  placeholder="Email Address" type="email" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('email')[0])){ echo 'autofocus' ; } ?>>
			   </span>
			   </label><span id="verify_error1" class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('email')[0])){ echo  $reg_errors->get_error_messages('email')[0] ; } ?></span>
			</p>
			<p><label>Verify Email Address <span class="red" data-tip="Required field">*</span><br>
			   <span class="form-control">
			   <input onkeyup="blurFunction()" onpaste="return false;" onCopy="return false" id="verify_email" name="verify_email_address"  value="<?php if (isset( $_POST['verify_email_address'] )) { echo $verify_email_address; }  ?>" class="form-control <?php if(isset($reg_errors) && $reg_errors->get_error_messages('verify_email')){ echo 'required' ; }?>"  placeholder="Verify Email Address" type="email" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('verify_email')[0])){ echo 'autofocus' ; } ?>>
			   </span>
			   </label><span class="red" id="verify_error"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('verify_email')[0])){ echo  $reg_errors->get_error_messages('verify_email')[0] ; } ?></span>
			</p>
			<!--p><label>Password <span class="red" data-tip="Required field">*</span><br>
			<span class="form-control">
				<input name="password" value="<?php if (isset( $_POST['password'] )) { echo $password; }  ?>" size="40" class="form-control <?php if(isset($reg_errors) && $reg_errors->get_error_messages('password')){ echo 'required' ; }?>"  placeholder="" type="password">
			</span>
			  </label> <span class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('password')[0])){ echo  $reg_errors->get_error_messages('password')[0] ; } ?></span>
			</p-->
			<input name="password" value="757fdfdf" size="40" class="form-control"  placeholder="" type="hidden">
		 <p>
		  <h5>Organisation Details </h5>
		   <p><label>Name of Organisation <span class="red" data-tip="Required field">*</span><br>
			   <span class="form-control text-181"><input name="organisation_name" value="<?php if (isset( $_POST['organisation_name'] )) { echo $organisation_name; }  ?>" size="40" class="form-control <?php if(isset($reg_errors) && $reg_errors->get_error_messages('organisation_name')){ echo 'required' ; }?>"  placeholder="" type="text" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('organisation_name')[0])){ echo 'autofocus' ; } ?>></span></label>
			   <span class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('organisation_name')[0])){ echo  $reg_errors->get_error_messages('organisation_name')[0] ; } ?></span>
			</p>
			<p><label>Address <span class="red" data-tip="Required field">*</span> <br>
			   <span class="form-control text-181"><input name="address" value="<?php if (isset( $_POST['address'] )) { echo $address; }  ?>" size="40" class="form-control <?php if(isset($reg_errors) && $reg_errors->get_error_messages('address')){ echo 'required' ; }?>"  placeholder="" type="text" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('address')[0])){ echo 'autofocus' ; } ?>></span>
			   </label>
			    <span class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('address')[0])){ echo  $reg_errors->get_error_messages('address')[0] ; } ?></span>
			</p>
			<p><label>Website  <br>
			   <span class="form-control text-181"><input name="website" value="<?php if (isset( $_POST['website'] )) { echo $website; }  ?>" size="40" class="form-control"  placeholder="Website" type="text"></span></label>
			</p>
			<p><label>Logo (upload)(if any)  <br>
			   <span class="form-control"><input name="logo" size="40" class="form-control"  type="file"></span></label>
			</p>
			<!--p><label>Official Registration information (if any)  <br>
			   <span class="form-control text-181"><input name="registration_info" value="<?php if (isset( $_POST['registration_info'] )) { echo $registration_info; }  ?>" size="40" class="form-control"  placeholder="" type="text"></span></label>
			</p>
			<p><label>Registration Number  <br>
			   <span class="form-control text-181"><input name="registration_no" value="<?php if (isset( $_POST['registration_no'] )) { echo $registration_no; }  ?>" size="40" class="form-control"  placeholder="" type="text"></span></label>
			</p-->
			<p><label>
				  Organisation Type<span class="red" data-tip="Required field">*</span><br>
				  <span class="form-control">
					 <select onchange="show_registration(jQuery(this).find(':selected').data('key'),'registration_info')" name="registration_info" class="form-control <?php if(isset($reg_errors) && $reg_errors->get_error_messages('registration_info')){ echo 'required' ; }?>">
					    <option value="">Select</option>
						<option data-key="ngo"  <?php if(isset($_POST['registration_info']) && $_POST['registration_info']=='Non-Governmental Organisation'){ echo 'selected="selected"'; } ?> value="Non-Governmental Organisation">Non-Governmental Organisation(NGO)</option>
						<option data-key="charity" <?php if(isset($_POST['registration_info']) && $_POST['registration_info']=='Charity'){ echo 'selected="selected"'; } ?> value="Charity">Charity</option>
						<option data-key="govtdept" <?php if(isset($_POST['registration_info']) && $_POST['registration_info']=='Government Department'){ echo 'selected="selected"'; } ?> value="Government Department">Government Department</option>
						<option data-key="regcomp" <?php if(isset($_POST['registration_info']) && $_POST['registration_info']=='Registered Company'){ echo 'selected="selected"'; } ?> value="Registered Company">Registered Company</option>
						<option data-key="individual" <?php if(isset($_POST['registration_info']) && $_POST['registration_info']=='Individual'){ echo 'selected="selected"'; } ?> value="Individual">Individual</option>
					 </select>
				  </span>
			   </label>
			    <span class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('registration_info')[0])){ echo  $reg_errors->get_error_messages('registration_info')[0] ; } ?></span>
			</p>
			<p class="ngo_child" <?php if(isset($_POST['registration_info'])){ if($_POST['registration_info']=='Individual' || $_POST['registration_info']=='Government Department'){ echo 'style="display:none!important"'; }else{ echo 'style="display:block!important"'; } }else{ echo 'style="display:none;"'; } ?>>
				<label>Registration Number<br>
				   <span class="form-control text-181"><input name="registration_no" value="<?php if (isset( $_POST['registration_no'] )) { echo $registration_no; }  ?>" size="40" class="form-control"  type="text"></span>
				 </label>
				</p>
			<p class="regis_child" <?php if(isset($_POST['hear_about_us']) && $_POST['hear_about_us']=='Referral'){ echo 'style="display:block"'; }else{ echo 'style="display:none"'; } ?>><label>Registration certificate (upload) <br>
			   <span class="form-control"><input name="reg_certificate" size="40" class="form-control"  type="file"></span></label>
			</p>
			 <h5>Project Summary </h5>
			 <p><label>Number of Trees <span class="red" data-tip="Required field">*</span> <br>
			   <span class="form-control"><input name="no_of_trees" value="<?php if (isset( $_POST['no_of_trees'] )) { echo $no_of_trees; }  ?>" class="form-control <?php if(isset($reg_errors) && $reg_errors->get_error_messages('no_of_trees')){ echo 'required' ; }?>"  type="number" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('no_of_trees')[0])){ echo 'autofocus' ; } ?>></span></label>
			   <span class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('no_of_trees')[0])){ echo  $reg_errors->get_error_messages('no_of_trees')[0] ; } ?></span>
			</p>
			<p><label>Funding from us ($ USD)<span class="red" data-tip="Required field">*</span><br>
			   <span class="form-control"><input name="project_fund" value="<?php if (isset( $_POST['project_fund'] )) { echo $project_fund; }  ?>" class="form-control <?php if(isset($reg_errors) && $reg_errors->get_error_messages('project_fund')){ echo 'required' ; }?>"  type="number" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('project_fund')[0])){ echo  'autofocus' ; } ?>></span></label>
			    <span class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('project_fund')[0])){ echo  $reg_errors->get_error_messages('project_fund')[0] ; } ?></span>
			</p>
			<h5>Help Us Help You </h5>
			<p><label>
				  How did you hear about us? <span class="red" data-tip="Required field">*</span><br>
				  <span class="form-control">
					 <select onchange="showChilds(jQuery(this).find(':selected').data('key'),'hear_about_us')" name="hear_about_us" class="form-control <?php if(isset($reg_errors) && $reg_errors->get_error_messages('hear_about_us')){ echo 'required' ; }?>" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('hear_about_us')[0])){ echo 'autofocus' ; } ?> >
					    <option value="">Select</option>
						<option data-key="search-engine"  <?php if(isset($_POST['hear_about_us']) && $_POST['hear_about_us']=='Search engine'){ echo 'selected="selected"'; } ?> value="Search engine">Search engine</option>
						<option data-key="refferal" <?php if(isset($_POST['hear_about_us']) && $_POST['hear_about_us']=='Referral'){ echo 'selected="selected"'; } ?> value="Referral">Referral</option>
						<option data-key="other" <?php if(isset($_POST['hear_about_us']) && $_POST['hear_about_us']=='Other'){ echo 'selected="selected"'; } ?> value="Other">Other</option>
					 </select>
				  </span>
			   </label>
			   <span class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('hear_about_us')[0])){ echo  $reg_errors->get_error_messages('hear_about_us')[0] ; } ?></span>
			</p>
			<p class="search-engine hear_about_us_child"  <?php if(isset($_POST['hear_about_us']) && $_POST['hear_about_us']=='Search engine'){ echo 'style="display:block"'; }else{ echo 'style="display:none"'; } ?>><label>If Search engine: what did you search for? <span class="red" data-tip="Required field">*</span><br>
			   <span class="form-control text-846"><input  name="search_for" value="<?php if (isset( $_POST['search_for'] )) { echo $search_for; }  ?>" size="40" class="form-control <?php if(isset($reg_errors) && $reg_errors->get_error_messages('search_for')){ echo 'required' ; }?>"  type="text" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('search_for')[0])){ echo  'autofocus' ; } ?>></span></label>
			      <span class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('search_for')[0])){ echo  $reg_errors->get_error_messages('search_for')[0] ; } ?></span>
			</p>
			<p class="refferal hear_about_us_child"  <?php if(isset($_POST['hear_about_us']) && $_POST['hear_about_us']=='Referral'){ echo 'style="display:block"'; }else{ echo 'style="display:none"'; } ?>><label>If Referral: Who introduced you to our organisation? <span class="red" data-tip="Required field">*</span><br>
			   <span class="form-control text-846"><input name="introduced_you" value="<?php if (isset( $_POST['introduced_you'] )) { echo $introduced_you; }  ?>" size="40" class="form-control"  type="text" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('introduced_you')[0])){ echo  'autofocus' ; } ?>></span></label>
			    <span class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('introduced_you')[0])){ echo  $reg_errors->get_error_messages('introduced_you')[0] ; } ?></span>
			</p>
				<p class="other hear_about_us_child" <?php if(isset($_POST['hear_about_us']) && $_POST['hear_about_us']=='Other'){ echo 'style="display:block"'; }else{ echo 'style="display:none"'; } ?>><label>If Other: Please specify  <span class="red" data-tip="Required field">*</span><br>
			   <span class="form-control text-846"><input name="others" value="<?php if (isset( $_POST['others'] )) { echo $others; }  ?>" size="40" class="form-control"  type="text" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('others')[0])){ echo  'autofocus' ; } ?>></span></label>
			   <span class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('others')[0])){ echo  $reg_errors->get_error_messages('others')[0] ; } ?></span>
			</p>
			<h5>Type of funding</h5>
			<p><label>
			 <span class="form-control">
				<span class="wpcf7-form-control-wrap radio-9 wh_rad">
					<span class="wpcf7-form-control wpcf7-radio">
						<span class="wpcf7-list-item first"><input name="type_of_fund" <?php if(isset($_POST['type_of_fund']) && $_POST['type_of_fund']=='either'){ echo 'checked'; } ?> value="either" type="radio"><span class="wpcf7-list-item-label">Either (recommended) </span></span>
						 <span class="wpcf7-list-item last"><input name="type_of_fund" <?php if(isset($_POST['type_of_fund']) && $_POST['type_of_fund']=='public'){ echo 'checked'; } ?> value="public" type="radio"><span class="wpcf7-list-item-label">Public – allowing public to provide funding through crowdfunding platform</span></span>
						 <span class="wpcf7-list-item last"><input name="type_of_fund" <?php if(isset($_POST['type_of_fund']) && $_POST['type_of_fund']=='commercial'){ echo 'checked'; } ?> value="commercial" type="radio"><span class="wpcf7-list-item-label">Commercial – use our commercial partners to provide funding</span></span>
					</span>
				</span>
			</span> 
			</p>
			<h5>Agree to</h5>
			<p><label>
			 <span class="form-control wh_radio">
				<span class="wpcf7-form-control-wrap radio-9">
						<span class="wpcf7-form-control wpcf7-radio">
							 <span class="wpcf7-list-item first"><input name="term_condition" value="1" <?php if(isset($_POST['term_condition']) && $_POST['term_condition']=='1'){ echo 'checked'; } ?> type="checkbox" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('term_condition')[0])){ echo  'autofocus' ; } ?>><span class="wpcf7-list-item-label"><a href="<?php echo esc_url( get_permalink(13915) ); ?>" target="_blank">Terms and Conditions</a></span><span class="red" data-tip="Required field">*</span></span>
							   <span  style="float: left;" class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('term_condition')[0])){ echo  $reg_errors->get_error_messages('term_condition')[0] ; } ?></span>
						   <span class="wpcf7-list-item last">
							<input name="agree_to_use" <?php if(isset($_POST['agree_to_use']) && $_POST['agree_to_use']=='2'){ echo 'checked'; } ?> value="2" type="checkbox" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('agree_to_use')[0])){ echo  'autofocus' ; } ?>><span class="wpcf7-list-item-label">I agree to The Green Earth Appeal using any/all data </span><span class="red" data-tip="Required field">*</span></span>
						   <span style="float: left;margin-left: 26px;" class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('agree_to_use')[0])){ echo  $reg_errors->get_error_messages('agree_to_use')[0] ; } ?></span>
						   <span class="wpcf7-list-item last"><input name="secure_funding" <?php if(isset($_POST['secure_funding']) && $_POST['secure_funding']=='3'){ echo 'checked'; } ?> value="3" type="checkbox" <?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('secure_funding')[0])){ echo  'autofocus' ; } ?>><span class="wpcf7-list-item-label">Once funding is secured we may require further due diligence </span><span class="red" data-tip="Required field">*</span></span>
						   <span style="float: left;margin-left: 26px;" class="red"><?php if(isset($reg_errors) && isset($reg_errors->get_error_messages('secure_funding')[0])){ echo  $reg_errors->get_error_messages('secure_funding')[0] ; } ?></span>
						</span>
					</span>
				</span>
			   </label>
			   </p>
			   <p>
				   <label>
						<input value="Register" id="submit-register" class="form-control" type="submit">
				   </label>
			   </p>
		 </form>
	  </div>
	  <script>
jQuery.noConflict();

function validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

       if (reg.test(emailField) == false) 
        {
			jQuery("#verify_error1").text('Please enter a valid email address.');

        }else{
			jQuery('#verify_error1').text('');
		}
}
function blurFunction(){
	var email = jQuery('#email').val();
	var verify_email = jQuery('#verify_email').val();
	if(email!==verify_email){
		jQuery('#verify_error').html('Email fields do not matched.');
	}else{
		jQuery('#verify_error').html('');
	}
} 
function showChilds(classname,parentname){
	jQuery("."+parentname+"_child").hide();
	/* jQuery("."+parentname+"_child").each(function( index ) {
	jQuery( this ).find('input').prop('required',false);
	}); */
	jQuery("."+classname).show();
	//jQuery("."+classname).find('input').prop('required',true);
	
}
function show_registration(classname,parentname){
	if(classname=='ngo' || classname=='charity' || classname=='regcomp'){
		jQuery(".ngo_child").show();
	}else{
		jQuery(".ngo_child").hide();
	}
	if(classname=='individual'){
		jQuery(".regis_child").hide();
	}
	
	
}
</script>
	  <?php
  }
  
function registration_validation($password, $email,$verify_email_address, $website, $first_name, $surname, $address,$registration_info,$organisation_name,$surname, $no_of_trees, $project_fund, $hear_about_us, $search_for,$introduced_you,$others, $term_condition, $agree_to_use, $secure_funding)  {

	global $reg_errors;

		$reg_errors = new WP_Error;
		  if (empty( $first_name )) {
			$reg_errors->add('first_name', 'First name field is required');
		}
		 if (empty( $surname )) {
			$reg_errors->add('surname', 'Surname field is required');
		}
		 if (empty( $email )) {
			$reg_errors->add('email', 'Email field is required');
		}
		 if (empty( $email )) {
			$reg_errors->add('verify_email', 'Verify email field is required');
		}
		if ($email!==$verify_email_address) {
			$reg_errors->add('verify_email', 'Email fields do not matched.');
		}
		 if (empty( $organisation_name )) {
			$reg_errors->add('organisation_name', 'Organisation name field is required');
		}
		
		 if (empty( $address )) {
			$reg_errors->add('address', 'Address name field is required');
		}
		if (empty( $registration_info )) {
			$reg_errors->add('registration_info', 'Organisation type field is required');
		}
		 if (empty( $no_of_trees )) {
			$reg_errors->add('no_of_trees', 'Trees field is required');
		}
		 if (empty( $project_fund )) {
			$reg_errors->add('project_fund', 'Funding field is required');
		}
		 if (empty( $hear_about_us )) {
			$reg_errors->add('hear_about_us', 'Hear about us field is required');
		}
		 if ($hear_about_us =='Search engine') {
			if (empty( $search_for )) {
				$reg_errors->add('search_for', 'Search engine field is required');
			}
		} elseif($hear_about_us =='Referral'){
			if (empty( $introduced_you )) {
				$reg_errors->add('introduced_you', 'Referral field is required');
			}
		}elseif($hear_about_us =='Other'){
			if (empty( $others )) {
				$reg_errors->add('others', 'Other field is required');
			}
		} 
		
		 if (!isset( $_POST['term_condition']) && empty( $_POST['term_condition'] )) {
			$reg_errors->add('term_condition', 'Term & condition field is required');
		}
		 if (empty( $agree_to_use )) {
			$reg_errors->add('agree_to_use', 'Agree to use field is required');
		}
		 if (empty( $secure_funding )) {
			$reg_errors->add('secure_funding', 'Secure funding field is required');
		}

		/*  if (empty( $password )) {
			$reg_errors->add('password', 'Password field is required');
		}
		if ( 5 > strlen( $password ) ) {
				$reg_errors->add( 'password', 'Password length must be greater than 5' );
			} */
			if ( !is_email( $email ) ) {
			$reg_errors->add( 'email', 'Email is not valid' );
		}
		if ( email_exists( $email ) ) {
			$reg_errors->add( 'email', 'Email Already in use' );
		}


}
function complete_registration() {
	
	//session_start();
	//echo "<pre>"; print_r($_SESSION); die;
    global $reg_errors, $password, $email, $website, $first_name, $surname,$address ,$organisation_name,$registration_info,$registration_no,$no_of_trees,$project_fund,$hear_about_us,$search_for,$introduced_you,$others,$type_of_fund;
		
    if ( 1 > count( $reg_errors->get_error_messages() ) ) {
		$create_random_passs = randomKey(6);
        $userdata = array(
				'user_login'    =>   $email,
				'user_email'    =>   $email,
				'user_pass'     =>   $create_random_passs,
				'user_url'      =>   $website,
				'first_name'    =>   $first_name,
				'last_name'     =>   $surname,
				'nickname'      =>   $first_name
			);
		
        $user = wp_insert_user( $userdata );
		$ranD_key= randomKey(6);
		$activation_key = md5($ranD_key);
		$user_meta_fields =  array(	'organisation_name' => $organisation_name,
								'address' => $address, 
								'website' => $website, 
								'organisation_type' => $registration_info, 
								'registration_no' => $registration_no, 
								'no_of_trees' => $no_of_trees,
								'project_fund' => $project_fund,
								'hear_about_us' => $hear_about_us,
								'search_for' =>  $search_for,
								'introduced_you' => $introduced_you,
								'others' => $others,
								'type_of_fund' => $type_of_fund,
								'logo' => $_SESSION['filename1'],
								'activation_key' => $activation_key,
								'reg_certificate' =>  $_SESSION['filename2']
								);
							
						//echo "<pre>"; print_r($user_meta_fields); die;
						
			foreach($user_meta_fields as $key=>$val){
				  add_user_meta( $user, $key , $val);  //update usermeta
			}		
			
			//$activation_url = 'https://www.greenearthappeal.org/panacea/index.php/user/set_grant_user_password/' . $activation_key;
			 $activation_url = 'https://www.greenearthappeal.org/choose-password/?key=' .$activation_key;
			$user_data= array(
							'user_email'    =>   $email,
							'first_name'    =>   $first_name,
							'last_name'     =>   $surname,
							'user_id'     =>   $user,
							'activation_key'     => $activation_key,
							'organisation_name' => $organisation_name,
							'address' => $address, 
							'website' => $website, 
							'organisation_type' => $registration_info, 
							're_no' => $registration_no, 
							'no_of_trees' => $no_of_trees,
							'project_fund' => $project_fund,
							'hear_about_us' => $hear_about_us,
							'search_for' =>  $search_for,
							'introduced_you' => $introduced_you,
							'others' => $others,
							'type_of_fund' => $type_of_fund,
							'logo' => $_SESSION['filename1'],
							're_certificate' =>  $_SESSION['filename2']
							);
			
			    $panacea_data = http_build_query($user_data);
			    $url ='http://www.greenearthappeal.org/panacea/index.php/user/grant_application_users/?'.$panacea_data;
				$res = file_get_contents($url);
				//echo "<pre>"; print_r($res); die;
				if(!empty($res)){
					 $result = json_decode($res);
					}

		 $plaintext_pass= "";
		 wp_new_user_notification($user,$plaintext_pass,$activation_key,$activation_url,$first_name);
		
        //echo 'Registration complete please check your email.';   
		unset($_SESSION['filename1']); 
		unset($_SESSION['filename2']); 
		 $redirect_url = site_url( '/thankyou-page/?registration=1');
		//wp_redirect( $redirect_url );
		?>
		<script>
		    window.location.href = '<?php echo $redirect_url; ?>';
		</script>
		<?php
    }
}
function randomKey($length) {  //create no of random digit alphanumeric code
			$key='';
			$pool = array_merge(range(0,9),range('A', 'Z'));

			for($i=0; $i < $length; $i++) {
				$key .= $pool[mt_rand(0, count($pool) - 1)];
			}
			return $key;
		}
function wp_new_user_notification( $user_id, $plaintext_pass = '',$activation_key='',$activation_url='', $first_name='') {
        $user = new WP_User($user_id);
  
        $user_login = stripslashes($user->user_login);
        $first_name = stripslashes($first_name);
        $user_email = stripslashes($user->user_email);
	    $headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: Green Earth Appeal Registration <partners@greenearthappeal.org>' . "\r\n";
		$headers .= 'Bcc: marvin@greenearthappeal.org' . "\r\n";
        $message  = 'New user registration'. "<br>";
		//$message .= sprintf(__('Username: %s'), $username) . "<br>";
        $message .= sprintf(__('E-mail: %s'), $user_email) . "<br>";
  
        @wp_mail(get_option('admin_email'), sprintf(__('Green Earth Appeal - New User Registration'), get_option('blogname')), $message,$headers);
		$headers1  = 'MIME-Version: 1.0' . "\r\n";
		$headers1 .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers1 .= 'From: Green Earth Appeal Registration <partners@greenearthappeal.org>' . "\r\n";
		$message1 = "";
		$message1 .='Dear '.ucfirst($first_name).','."<br><br>";
		$message1 .= "Thank you for your interest in the Green Earth Appeal and for registering to use our grant application program.".'<br><br>';
		$message1 .= 'Please confirm your email address and activate your grant account by <a href="'.$activation_url.'">clicking this link </a> or copying the following web address into your browser '.$activation_url.' and choosing a password.'.'<br><br>';
		$message1 .= "Once your account has been activated you will be emailed your account details and further instructions.".'<br><br>';
		$message1 .= "Kind Regards,".'<br><br>';
		$message1 .= "Partnerships Department".'<br><br>';
		$message1 .= "Green Earth Appeal".'<br>';
  
        @wp_mail($user_email, sprintf(__('Green Earth Appeal - Grant Application Confirmation'), get_option('blogname')), $message1,$headers1);
  
    }

function custom_registration_function() {
	

	$password = $email =$verify_email_address = $website = $first_name = $surname =$address  =$organisation_name =$registration_info =$registration_no =$no_of_trees =$project_fund =$hear_about_us =$search_for =$introduced_you = $others = $type_of_fund = "";
	if ( isset($_POST['email_address'] ) ) {
		
		if(!isset($_POST['agree_to_use'])){
			$_POST['agree_to_use'] = "";
		}
		if(!isset($_POST['term_condition'])){
			$_POST['term_condition'] = "";
		}
		if(!isset($_POST['type_of_fund'])){
			$_POST['type_of_fund'] = "";
		}
		if(!isset($_POST['secure_funding'])){
			$_POST['secure_funding'] = "";
		}
		//echo "<pre>"; print_r($_POST); die;
		//session_start();
        registration_validation(
        $_POST['password'],
        $_POST['email_address'],
        $_POST['verify_email_address'],
        $_POST['website'],
        $_POST['first_name'],
        $_POST['surname'],
        $_POST['address'],
        $_POST['registration_info'],
        $_POST['organisation_name'],
        $_POST['surname'],
        $_POST['no_of_trees'],
        $_POST['project_fund'],
        $_POST['hear_about_us'],
        $_POST['search_for'],
        $_POST['introduced_you'],
        $_POST['others'],
        $_POST['term_condition'],
        $_POST['agree_to_use'],
        $_POST['secure_funding']   );
		
		$file_name1="";
		$file_name2="";
       			 if($_FILES['logo']['name']!=''){
					  $filename = $_FILES['logo']['name'];
					  //$filename = date('Ymdhis').$_FILES['logo']['name'];
					  $ext = pathinfo($filename, PATHINFO_EXTENSION);
					  $org_name =  str_replace(" ","-", strtolower($_POST['organisation_name']));
					  $file_name1 = $org_name.'-'.date('Yhs').'.'.$ext;
					  $root = $_SERVER['DOCUMENT_ROOT'].'/wp-content/uploads/plants_trees/' ;
					  $file = $root . basename($file_name1); 
					  $raw_file_name = $_FILES['logo']['tmp_name'];
					  move_uploaded_file($_FILES['logo']['tmp_name'], $file);
				  }
			
			if($_FILES['reg_certificate']['name']!=''){
				  $filename = $_FILES['reg_certificate']['name'];
				  $ext = pathinfo($filename, PATHINFO_EXTENSION);
				  $org_name =  str_replace(" ","-", strtolower($_POST['organisation_name']));
				  $file_name2 = $org_name.'-'.date('Yhs').'2.'.$ext;
				  $root = $_SERVER['DOCUMENT_ROOT'].'/wp-content/uploads/plants_trees/' ;
				  $file = $root . basename($file_name2); 
				  $raw_file_name = $_FILES['reg_certificate']['tmp_name'];
				  move_uploaded_file($_FILES['reg_certificate']['tmp_name'], $file);
			  }
			  $_SESSION['filename1'] = $file_name1;
			  $_SESSION['filename2'] = $file_name2;

	
		 

        // sanitize user form input
        global $password, $email, $verify_email_address, $website, $first_name, $surname, $address,$organisation_name,$registration_info,$registration_no,$no_of_trees,$project_fund, $hear_about_us,$search_for,$introduced_you,$others,$type_of_fund;
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email_address'] );
        $verify_email_address      =   sanitize_email( $_POST['verify_email_address'] );
        $website    =   esc_url( $_POST['website'] );
        $first_name =   sanitize_text_field( $_POST['first_name'] );
        $surname  =   sanitize_text_field( $_POST['surname'] );
        $address   =   sanitize_text_field( $_POST['address'] );
        $organisation_name  =   sanitize_text_field( $_POST['organisation_name'] );
        $registration_info   =   sanitize_text_field( $_POST['registration_info'] );
        $registration_no   =   sanitize_text_field( $_POST['registration_no'] );
        $no_of_trees   =   sanitize_text_field( $_POST['no_of_trees'] );
        $project_fund   =   sanitize_text_field( $_POST['project_fund'] );
        $hear_about_us   =   sanitize_text_field( $_POST['hear_about_us'] );
        $search_for   =   sanitize_text_field( $_POST['search_for'] );
        $introduced_you   =   sanitize_text_field( $_POST['introduced_you'] );
        $others   =   sanitize_text_field( $_POST['others'] );
        $type_of_fund   =   sanitize_text_field( $_POST['type_of_fund'] );
        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
        $password,
        $email,
        $website,
        $first_name,
        $surname
        );
    }
 
    registration_form($password, $email,$verify_email_address, $website, $first_name, $surname,$address ,$organisation_name,$registration_info,$registration_no,$no_of_trees,$project_fund,$hear_about_us,$search_for,$introduced_you,$others,$type_of_fund);
}
 
// Register a new shortcode: [tree_planting_registration]
add_shortcode( 'tree_planting_registration', 'planting_registration' );
 
// The callback function that will replace [book]
function planting_registration() {
    ob_start();
    custom_registration_function();
    return ob_get_clean();
}

