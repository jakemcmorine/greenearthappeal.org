<?php
/**
 * Plugin Name: Custom referer plugin
 * Description: convert Joomla referer module to wp referer plugin
 * Version: 1.0
 * Author: Vinh Nguyen (phpcodeteam@gmail.com)
 * License: GPLv2 or later
 * Text Domain: referer
 */


function output_GEA_sponsored_graphic( $ref ) {
	$mydb = new wpdb('nonoodle_wp955','!LS0-9P71F','nonoodle_gearth','localhost');

	$sql = "SELECT slogo, sname, shome FROM t_sponsors WHERE scode='" . trim( $ref ) . "' LIMIT 0,1";

	$r = $mydb->get_row($sql, ARRAY_A);

	return "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\"><a href=\"{$r['shome']}\" target=\"_blank\"><img src=\"https://www.greenearthappeal.org/graphics/sponsors/{$r['slogo']}\" alt=\"{$r['sname']}\" /></a></td></tr></table>";
}


function output_GEA_sponsored_graphic3( $refid ) {
	$mydb = new wpdb('nonoodle_wp955','!LS0-9P71F','nonoodle_partner','localhost');

	$sql = "SELECT r.restaurant AS sname, r.logo AS slogo, r.website AS shome
        FROM pct_restaurants AS r
        INNER JOIN pct_referer AS re
        ON re.user_id=r.user_id
        WHERE re.code='$refid'";

	$r = $mydb->get_row($sql, ARRAY_A);

	if ( ! $r ) {
		$sql2 = "SELECT p.company AS sname, p.logo AS slogo, p.website AS shome
                    FROM pct_bulk_partners AS p
                    INNER JOIN pct_referer AS re
                    ON re.user_id=p.user_id
                    WHERE re.code='$refid'";
		$r = $mydb->get_row($sql2, ARRAY_A);
		
	}

	if ( $r ) {
		$timestp = time();
		return "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\"><a href=\"{$r['shome']}\" target=\"_blank\"><img src=\"https://www.greenearthappeal.org/panacea/uploads/company/{$r['slogo']}?{$timestp}\" alt=\"{$r['sname']}\" /></a></td></tr></table>";
	} else {
		return output_GEA_sponsored_graphic("b-l-a-n-k");
	}
}


function pct_custom_referer( $atts ) {

	if ( isset( $_GET['ref'] ) ) {
		$cookie_name = "GEAReferrer";
		$cookie_value = $_GET['ref'];
		$output = output_GEA_sponsored_graphic( $cookie_value );

	} elseif ( isset( $_GET['refid'] ) ) {
		$cookie_name = "refid";
		$cookie_value = $_GET['refid'];
		$output = output_GEA_sponsored_graphic3( $cookie_value );

	} elseif ( isset( $_SESSION['refid'] ) ) {
		$cookie_name = 'GEAReferrer';
		$cookie_value = $_SESSION['refid'];
		$output = output_GEA_sponsored_graphic( $cookie_value );

	} elseif ( isset( $_COOKIE['refid'] ) ) {
		$output = output_GEA_sponsored_graphic3( $_COOKIE['refid'] );

	} elseif ( isset( $_COOKIE['GEAReferrer'] ) ) {
		$output = output_GEA_sponsored_graphic( $_COOKIE['GEAReferrer'] );

	} else {
		$cookie_name = 'GEAReferrer';
		$cookie_value = "b-l-a-n-k";
		$output = output_GEA_sponsored_graphic( $cookie_value );
	}

	if (isset($cookie_name, $cookie_value)) {
		$script = "<script type='text/javascript'>function pctSetCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = \"expires=\"+d.toUTCString();
    document.cookie = cname + \"=\" + cvalue + \"; \" + expires;
}
pctSetCookie('{$cookie_name}', '{$cookie_value}', 31);
</script>";
		$output .= $script;
	}


	return $output;
}

add_shortcode( 'custom_referer', 'pct_custom_referer' );
?>