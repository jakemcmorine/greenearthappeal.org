<?php
/**
 * Page Meta Box Options
 *
 * @since 1.0.0
 *
 * @param array $args
 *
 * @return array
 */
function themify_theme_page_meta_box( $args = array() ){
	extract( $args );
	return array(
		// Page Layout
		array(
			'name' 		=> 'page_layout',
			'title'		=> __('Sidebar Option', 'themify'),
			'description'	=> '',
			'type'		=> 'layout',
			'show_title' => true,
			'meta'		=> array(
				array('value' => 'default', 'img' => 'images/layout-icons/default.png', 'selected' => true, 'title' => __('Default', 'themify')),
				array('value' => 'sidebar1', 'img' => 'images/layout-icons/sidebar1.png', 'title' => __('Sidebar Right', 'themify')),
				array('value' => 'sidebar1 sidebar-left', 'img' => 'images/layout-icons/sidebar1-left.png', 'title' => __('Sidebar Left', 'themify')),
				array('value' => 'sidebar-none', 'img' => 'images/layout-icons/sidebar-none.png', 'title' => __('No Sidebar ', 'themify'))
			),
			'default' => 'default'
		),

		// Content Width
		array(
			'name'=> 'content_width',
			'title' => __('Content Width', 'themify'),
			'description' => 'Select "Fullwidth" if the page is to be built with the Builder without the sidebar (it will make the Builder content fullwidth).',
			'type' => 'layout',
			'show_title' => true,
			'meta' => array(
				array(
					'value' => 'default_width',
					'img' => 'themify/img/default.png',
					'selected' => true,
					'title' => __( 'Default', 'themify' )
				),
				array(
					'value' => 'full_width',
					'img' => 'themify/img/fullwidth.png',
					'title' => __( 'Fullwidth', 'themify' )
				)
			),
			'default' => 'default_width'
		),		
		// Hide page title
		array(
			'name' 		=> 'hide_page_title',
			'title'		=> __('Hide Page Title', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Custom menu
		array(
			'name'        => 'custom_menu',
			'title'       => __( 'Custom Menu', 'themify' ),
			'description' => '',
			'type'        => 'dropdown',
			// extracted from $args
			'meta'        => themify_get_available_menus(),
		)
	);
}

/**
 * Query Posts Options
 * @param array $args
 * @return array
 * @since 1.0.0
 */
function themify_theme_query_post_meta_box() {
	$query_post_meta_box = array(
		// Notice
		array(
			'name' => '_query_posts_notice',
			'title' => '',
			'description' => '',
			'type' => 'separator',
			'meta' => array(
				'html' => '<div class="themify-info-link">' . sprintf( __( '<a href="%s">Query Posts</a> allows you to query WordPress posts from any category on the page. To use it, select a Query Category.', 'themify' ), 'http://themify.me/docs/query-posts' ) . '</div>'
			),
		),
		// Post Category
		array(
			'name' 		=> 'query_category',
			'title'		=> __('Post Category', 'themify'),
			'description'	=> __('Select a category or enter multiple category IDs (eg. 2,5,6). Enter 0 to display all categories.', 'themify'),
			'type'		=> 'query_category',
			'meta'		=> array()
		),
		// Query All Post Types
		array(
			'name' => 'query_all_post_types',
			'type' => 'dropdown',
			'title' => __( 'Query All Post Types', 'themify'),
			'meta' =>array(
				array(
				'value' => '',
				'name' => '',
				),
				array(
				'value' => 'yes',
				'name' => 'Yes',
				),
				array(
				'value' => 'no',
				'name' => 'No',
				),
			)
		),
		// Descending or Ascending Order for Posts
		array(
			'name' 		=> 'order',
			'title'		=> __('Order', 'themify'),
			'description'	=> '',
			'type'		=> 'dropdown',
			'meta'		=> array(
				array('name' => __('Descending', 'themify'), 'value' => 'desc', 'selected' => true),
				array('name' => __('Ascending', 'themify'), 'value' => 'asc')
			),
			'default' => 'desc'
		),
		// Criteria to Order By
		array(
			'name' 		=> 'orderby',
			'title'		=> __('Order By', 'themify'),
			'description'	=> '',
			'type'		=> 'dropdown',
			'meta'		=> array(
				array('name' => __('Date', 'themify'), 'value' => 'date', 'selected' => true),
				array('name' => __('Random', 'themify'), 'value' => 'rand'),
				array('name' => __('Author', 'themify'), 'value' => 'author'),
				array('name' => __('Post Title', 'themify'), 'value' => 'title'),
				array('name' => __('Comments Number', 'themify'), 'value' => 'comment_count'),
				array('name' => __('Modified Date', 'themify'), 'value' => 'modified'),
				array('name' => __('Post Slug', 'themify'), 'value' => 'name'),
				array('name' => __('Post ID', 'themify'), 'value' => 'ID'),
				array('name' => __( 'Custom Field String', 'themify' ), 'value' => 'meta_value'),
				array('name' => __( 'Custom Field Numeric', 'themify' ), 'value' => 'meta_value_num')
			),
			'default' => 'date',
			'hide' => 'date|rand|author|title|comment_count|modified|name|ID field-meta-key'
		),
		array(
			'name'			=> 'meta_key',
			'title'			=> __( 'Custom Field Key', 'themify' ),
			'description'	=> '',
			'type'			=> 'textbox',
			'meta'			=> array('size' => 'medium'),
			'class'			=> 'field-meta-key'
		),
		// Post Layout
		array(
			'name' 		=> 'layout',
			'title'		=> __('Post Layout', 'themify'),
			'description'	=> '',
			'type'		=> 'layout',
			'show_title' => true,
			'meta'		=> array(
				array('value' => 'list-post', 'img' => 'images/layout-icons/list-post.png', 'selected' => true, 'title' => __('List Post', 'themify')),
				array('value' => 'grid4', 'img' => 'images/layout-icons/grid4.png', 'title' => __('Grid 4', 'themify')),
				array('value' => 'grid3', 'img' => 'images/layout-icons/grid3.png', 'title' => __('Grid 3', 'themify')),
				array('value' => 'grid2', 'img' => 'images/layout-icons/grid2.png', 'title' => __('Grid 2', 'themify')),
				array('value' => 'grid2-thumb', 'img' => 'images/layout-icons/grid2-thumb.png', 'title' => __('Grid 2 Thumb', 'themify'))
			),
			'default' => 'list-post'
		),
		// Posts Per Page
		array(
			  'name' 		=> 'posts_per_page',
			  'title'		=> __('Posts per page', 'themify'),
			  'description'	=> '',
			  'type'		=> 'textbox',
			  'meta'		=> array('size' => 'small')
			),
		
		// Display Content
		array(
			'name' 		=> 'display_content',
			'title'		=> __('Display Content', 'themify'),
			'description'	=> '',
			'type'		=> 'dropdown',
				'meta'		=> array(
					array( 'name' => __('Full Content', 'themify'), 'value' => 'content' ),
					array( 'name' => __('Excerpt', 'themify'), 'value' => 'excerpt', 'selected' => true ),
					array( 'name' => __('None', 'themify'), 'value' => 'none' )
				),
				'default' => 'excerpt',
		),
		// Featured Image Size
		array(
			'name'	=>	'feature_size_page',
			'title'	=>	__('Image Size', 'themify'),
			'description' => __('Image sizes can be set at <a href="options-media.php">Media Settings</a> and <a href="https://wordpress.org/plugins/regenerate-thumbnails/" target="_blank">Regenerated</a>', 'themify'),
			'type'		 =>	'featimgdropdown',
			'display_callback' => 'themify_is_image_script_disabled'
			),
		array(
			'type' => 'multi',
			'name' => '_post_image_dimensions',
			'title' => __('Image Dimensions', 'themify'),
			'meta' => array(
				'fields' => array(
					// Image Width
					array(
					  'name' 		=> 'image_width',	
					  'label' => __('width', 'themify'),
					  'description' => '', 				
					  'type' 		=> 'textbox',			
					  'meta'		=> array('size'=>'small')			
					),
					// Image Height
					array(
					  'name' 		=> 'image_height',	
					  'label' => __('height', 'themify'),
					  'description' => '', 				
					  'type' 		=> 'textbox',			
					  'meta'		=> array('size'=>'small')			
					),
				),
				'description' => __('Enter height = 0 to disable vertical cropping with img.php enabled', 'themify'), 	
				'before' => '',
				'after' => '',
				'separator' => ''
			)
		),
		// Hide Title
		array(
			'name' 		=> 'hide_title',
			'title'		=> __('Hide Post Title', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Unlink Post Title
		array(
			'name' 		=> 'unlink_title',	
			'title' 		=> __('Unlink Post Title', 'themify'), 	
			'description' => __('Unlink post title (it will display the post title without link)', 'themify'), 				
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'		
		),
		// Hide Post Date
		array(
			'name' 		=> 'hide_date',
			'title'		=> __('Hide Post Date', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Hide Post Meta
		themify_multi_meta_field(),
		// Media Above/Below Title
		array(
			'name' 		=> 'media_position',
			'title'		=> __('Featured Image/Media Position', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'above', 'name' => __('Above Post Title', 'themify')),
				array('value' => 'below', 'name' => __('Below Post Title', 'themify')),
			),
			'default' => 'default'
		),
		// Hide Post Image
		array(
			'name' 		=> 'hide_image',	
			'title' 		=> __('Hide Featured Image', 'themify'), 	
			'description' => '', 				
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Unlink Post Image
		array(
			'name' 		=> 'unlink_image',	
			'title' 		=> __('Unlink Featured Image', 'themify'), 	
			'description' => __('Display the Featured Image without link', 'themify'), 				
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'	
		),
		// Page Navigation Visibility
		array(
			'name' 		=> 'hide_navigation',
			'title'		=> __('Hide Page Navigation', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		)
	);

	return $query_post_meta_box;
}

/**
 * Portfolio Meta Box Options
 * @param array $args
 * @return array
 * @since 1.0.7
 */
function themify_theme_query_portfolio_meta_box(){
	$query_portfolio_meta_box = array(
		// Query Category
		array(
			'name' 		=> 'portfolio_query_category',
			'title'		=> __('Portfolio Category', 'themify'),
			'description'	=> __('Select a portfolio category or enter multiple portfolio category IDs (eg. 2,5,6). Enter 0 to display all portfolio categories.', 'themify'),
			'type'		=> 'query_category',
			'meta'		=> array('taxonomy' => 'portfolio-category')
		),
		// Descending or Ascending Order for Portfolios
		array(
			'name' 		=> 'portfolio_order',
			'title'		=> __('Order', 'themify'),
			'description'	=> '',
			'type'		=> 'dropdown',
			'meta'		=> array(
				array('name' => __('Descending', 'themify'), 'value' => 'desc', 'selected' => true),
				array('name' => __('Ascending', 'themify'), 'value' => 'asc')
			),
			'default' => 'desc'
		),
		// Criteria to Order By
		array(
			'name' 		=> 'portfolio_orderby',
			'title'		=> __('Order By', 'themify'),
			'description'	=> '',
			'type'		=> 'dropdown',
			'meta'		=> array(
				array('name' => __('Date', 'themify'), 'value' => 'date', 'selected' => true),
				array('name' => __('Random', 'themify'), 'value' => 'rand'),
				array('name' => __('Author', 'themify'), 'value' => 'author'),
				array('name' => __('Post Title', 'themify'), 'value' => 'title'),
				array('name' => __('Comments Number', 'themify'), 'value' => 'comment_count'),
				array('name' => __('Modified Date', 'themify'), 'value' => 'modified'),
				array('name' => __('Post Slug', 'themify'), 'value' => 'name'),
				array('name' => __('Post ID', 'themify'), 'value' => 'ID'),
				array('name' => __( 'Custom Field String', 'themify' ), 'value' => 'meta_value'),
				array('name' => __( 'Custom Field Numeric', 'themify' ), 'value' => 'meta_value_num')
			),
			'default' => 'date',
			'hide' => 'date|rand|author|title|comment_count|modified|name|ID field-portfolio-meta-key'
		),
		array(
			'name'			=> 'portfolio_meta_key',
			'title'			=> __( 'Custom Field Key', 'themify' ),
			'description'	=> '',
			'type'			=> 'textbox',
			'meta'			=> array('size' => 'medium'),
			'class'			=> 'field-portfolio-meta-key'
		),
		// Post Layout
		array(
			'name' 		=> 'portfolio_layout',
			'title'		=> __('Portfolio Layout', 'themify'),
			'description'	=> '',
			'type'		=> 'layout',
			'show_title' => true,
			'meta'		=> array(
				array('value' => 'list-post', 'img' => 'images/layout-icons/list-post.png', 'selected' => true),
				array('value' => 'grid4', 'img' => 'images/layout-icons/grid4.png', 'title' => __('Grid 4', 'themify')),
				array('value' => 'grid3', 'img' => 'images/layout-icons/grid3.png', 'title' => __('Grid 3', 'themify')),
				array('value' => 'grid2', 'img' => 'images/layout-icons/grid2.png', 'title' => __('Grid 2', 'themify')),
				array('value' => 'grid2-thumb', 'img' => 'images/layout-icons/grid2-thumb.png', 'title' => __('Grid 2 Thumb', 'themify'))
			),
			'default' => 'list-post'
		),
		// Posts Per Page
		array(
			  'name' 		=> 'portfolio_posts_per_page',
			  'title'		=> __('Portfolios per page', 'themify'),
			  'description'	=> '',
			  'type'		=> 'textbox',
			  'meta'		=> array('size' => 'small')
			),
		
		// Display Content
		array(
			'name' 		=> 'portfolio_display_content',
			'title'		=> __('Display Content', 'themify'),
			'description'	=> '',
			'type'		=> 'dropdown',
				'meta'		=> array(
					array( 'name' => __('Full Content', 'themify'), 'value' => 'content' ),
					array( 'name' => __('Excerpt', 'themify'), 'value' => 'excerpt', 'selected' => true ),
					array( 'name' => __('None', 'themify'), 'value' => 'none' )
				),
				'default' => 'excerpt',
		),
		// Feature gallery or image
		array(
			'name' 		=> 'portfolio_feature',
			'title'		=> __( 'Display Featured Image/Slider', 'themify' ),
			'description' => '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array( 'value' => 'gallery', 'name' => __( 'Gallery', 'themify' ) ),
				array( 'value' => 'image', 'name' => __( 'Image', 'themify' ) )
			),
			'default' => 'gallery'
		),
		// Portfolio Expander
		array(
			'name' 		=> 'portfolio_expander',
			'title'		=> __( 'Enable Portfolio Expander', 'themify' ),
			'description' => '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array( 'value' => 'yes', 'name' => __( 'Yes', 'themify' ) ),
				array( 'value' => 'no', 'name' => __( 'No', 'themify' ) )
			),
			'default' => 'yes'
		),
		// Featured Image Size
		array(
			'name'	=>	'portfolio_feature_size_page',
			'title'	=>	__('Image Size', 'themify'),
			'description' => __('Image sizes can be set at <a href="options-media.php">Media Settings</a> and <a href="https://wordpress.org/plugins/regenerate-thumbnails/" target="_blank">Regenerated</a>', 'themify'),
			'type'		 =>	'featimgdropdown',
			'display_callback' => 'themify_is_image_script_disabled'
			),
		
		// Multi field: Image Dimension
		array(
			'type' => 'multi',
			'name' => '_portfolio_image_dimensions',
			'title' => __('Image Dimensions', 'themify'),
			'meta' => array(
				'fields' => array(
					// Image Width
					array(
					  'name' 		=> 'portfolio_image_width',	
					  'label' => __('width', 'themify'),
					  'description' => '', 				
					  'type' 		=> 'textbox',			
					  'meta'		=> array('size'=>'small')			
					),
					// Image Height
					array(
					  'name' 		=> 'portfolio_image_height',	
					  'label' => __('height', 'themify'),
					  'description' => '', 				
					  'type' 		=> 'textbox',			
					  'meta'		=> array('size'=>'small')			
					),
				),
				'description' => __('Enter height = 0 to disable vertical cropping with img.php enabled', 'themify'), 	
				'before' => '',
				'after' => '',
				'separator' => ''
			)
		),
		// Hide Title
		array(
			'name' 		=> 'portfolio_hide_title',
			'title'		=> __('Hide Portfolio Title', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Unlink Post Title
		array(
			'name' 		=> 'portfolio_unlink_title',	
			'title' 		=> __('Unlink Portfolio Title', 'themify'), 	
			'description' => __('Unlink portfolio title (it will display the post title without link)', 'themify'), 				
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'	
		),
		// Hide Post Date
		array(
			'name' 		=> 'portfolio_hide_date',
			'title'		=> __('Hide Portfolio Date', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Hide Post Meta
		array(
			'name' 		=> 'portfolio_hide_meta_all',	
			'title' 	=> __('Hide Portfolio Meta', 'themify'), 	
			'description' => '', 				
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Hide Post Image
		array(
			'name' 		=> 'portfolio_hide_image',	
			'title' 		=> __('Hide Featured Image', 'themify'), 	
			'description' => '', 				
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Unlink Post Image
		array(
			'name' 		=> 'portfolio_unlink_image',	
			'title' 		=> __('Unlink Featured Image', 'themify'), 	
			'description' => __('Display the Featured Image without link', 'themify'), 				
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'		
		),
		// Page Navigation Visibility
		array(
			'name' 		=> 'portfolio_hide_navigation',
			'title'		=> __('Hide Page Navigation', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		)
	);

	return $query_portfolio_meta_box;
}

/**
 * Display the Query Section metabox
 */
function themify_theme_query_section_meta_box() {
	$query_section_meta_box = array(
	  
		// Query Category
		array(
			'name' 		=> 'section_query_category',
			'title'		=> __('Section Category', 'themify'),
			'description'	=> __('Select a section category or enter multiple section category IDs (eg. 2,5,6). Enter 0 to display all section categories.', 'themify'),
			'type'		=> 'query_category',
			'meta'		=> array('taxonomy' => 'section-category')
		),
		// Descending or Ascending Order for Sections
		array(
			'name' 		=> 'section_order',
			'title'		=> __('Order', 'themify'),
			'description'	=> '',
			'type'		=> 'dropdown',
			'meta'		=> array(
				array('name' => __('Descending', 'themify'), 'value' => 'desc', 'selected' => true),
				array('name' => __('Ascending', 'themify'), 'value' => 'asc')
			),
			'default' => 'desc'
		),
		// Criteria to Order By
		array(
			'name' 		=> 'section_orderby',
			'title'		=> __('Order By', 'themify'),
			'description'	=> '',
			'type'		=> 'dropdown',
			'meta'		=> array(
				array('name' => __('Date', 'themify'), 'value' => 'date', 'selected' => true),
				array('name' => __('Random', 'themify'), 'value' => 'rand'),
				array('name' => __('Author', 'themify'), 'value' => 'author'),
				array('name' => __('Post Title', 'themify'), 'value' => 'title'),
				array('name' => __('Comments Number', 'themify'), 'value' => 'comment_count'),
				array('name' => __('Modified Date', 'themify'), 'value' => 'modified'),
				array('name' => __('Post Slug', 'themify'), 'value' => 'name'),
				array('name' => __('Post ID', 'themify'), 'value' => 'ID'),
				array('name' => __( 'Custom Field String', 'themify' ), 'value' => 'meta_value'),
				array('name' => __( 'Custom Field Numeric', 'themify' ), 'value' => 'meta_value_num')
			),
			'default' => 'date',
			'hide' => 'date|rand|author|title|comment_count|modified|name|ID field-section-meta-key'
		),
		array(
			'name'			=> 'section_meta_key',
			'title'			=> __( 'Custom Field Key', 'themify' ),
			'description'	=> '',
			'type'			=> 'textbox',
			'meta'			=> array('size' => 'medium'),
			'class'			=> 'field-section-meta-key'
		),
		// Posts Per Page
		array(
			  'name' 		=> 'section_posts_per_page',
			  'title'		=> __('Sections per page', 'themify'),
			  'description'	=> '',
			  'type'		=> 'textbox',
			  'meta'		=> array('size' => 'small')
			),
		// Featured Image Size
		array(
			'name'	=>	'section_feature_size_page',
			'title'	=>	__('Image Size', 'themify'),
			'description' => __('Image sizes can be set at <a href="options-media.php">Media Settings</a> and <a href="https://wordpress.org/plugins/regenerate-thumbnails/" target="_blank">Regenerated</a>', 'themify'),
			'type'		 =>	'featimgdropdown',
			'display_callback' => 'themify_is_image_script_disabled'
			),
		// Hide Title
		array(
			'name' 		=> 'section_hide_title',
			'title'		=> __('Hide Section Title', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Hide Subtitle
		array(
			'name' 		=> 'section_hide_subtitle',
			'title'		=> __('Hide Section Subtitle', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Parallax Effect
		array(
			'name' 		=> 'section_parallax_effect',
			'title'		=> __('Parallax Effect', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'effect2', 'name' => __('Background Scrolling', 'themify'), 'selected' => true),
				array('value' => 'effect1',	'name' => __('Overlap Scrolling', 'themify'))
			),
			'default' => 'effect2'
		)
	);

	return $query_section_meta_box;
}

/**
 * Theme Appearance Tab for Themify Custom Panel
 *
 * @since 1.0.0
 *
 * @param array $args
 *
 * @return array
 */
function themify_theme_page_theme_design_meta_box( $args = array() ) {

	/**
	 * backward compatibility with Parallax 2.2.8 or lower
	 * convert the legacy "hide_header" and "hide_footer" options to the new setting
	 */
	global $post;
	if( get_post_meta( $post->ID, 'hide_header', true ) === 'on' ) {
		delete_post_meta( $post->ID, 'hide_header' );
		update_post_meta( $post->ID, 'header_design', 'header-menu-bar' );
	}
	if( get_post_meta( $post->ID, 'hide_footer', true ) === 'on' ) {
		delete_post_meta( $post->ID, 'hide_footer' );
		update_post_meta( $post->ID, 'footer_design', 'none' );
	}

	return array(
		// Notice
		array(
			'name' => '_theme_appearance_notice',
			'title' => '',
			'description' => '',
			'type' => 'separator',
			'meta' => array(
				'html' => '<div class="themify-info-link">' . __( 'The settings here apply on this page only. Leave everything as default will use the site-wide Theme Appearance from the Themify panel > Settings > Theme Settings.', 'themify' ) . '</div>'
			),
		),

		// Header Design
		array(
			'name'        => 'header_design',
			'title'       => __( 'Header Design', 'themify' ),
			'description' => '',
			'type'        => 'layout',
			'show_title'  => true,
			'meta'        => $args['header_design_options'],
			'hide'		  => 'none header-leftpane header-minbar header-rightpane',
			'default' => 'default',
		),
		// Sticky Header
		array(
			'name'        => 'fixed_header',
			'title'       => __( 'Sticky Header', 'themify' ),
			'description' => '',
			'type'		  => 'radio',
			'meta'		  => themify_ternary_options(),
			'class'		  => 'hide-if none header-leftpane header-minbar header-rightpane header-menu-bar',
			'default' => 'default',
		),
        array(
			'name'	=> 'menu_bar_position',
			'title'	=> __('Menu Bar Position', 'themify'),
			'type'	=> 'radio',
			'meta'		=> array(
				array('value' => '', 'name' => __('Default', 'themify')),
				array('value' => 'top', 'name' => __('Top', 'themify')),
				array('value' => 'bottom', 'name' => __('Bottom', 'themify') ),
			),
			'class'		  => 'hide-if none header-horizontal header-boxed-layout header-leftpane header-rightpane header-minbar header-slide-out header-boxed-compact header-overlay header-menu-bar',
			'default' => ''
		),
		// Full Height Header
		array(
			'name'        => 'full_height_header',
			'title'       => __( 'Full Height Header', 'themify' ),
			'description' => __( 'Full height will display the container in 100% viewport height', 'themify' ),
			'type'		  => 'radio',
			'meta'		  => themify_ternary_options(),
			'class'		  => 'hide-if default none header-horizontal header-leftpane header-minbar header-slide-out header-boxed-layout header-boxed-compact header-rightpane header-overlay header-menu-bar',
			'default' => 'default',
		),
		// Header Elements
		array(
			'name' 	=> '_multi_header_elements',	
			'title' => __('Header Elements', 'themify'), 	
			'description' => '',	
			'type' 	=> 'multi',
			'class' => 'hide-if none header-menu-bar',		
			'meta'	=> array(
				'fields' => array(
					// Show Site Logo
					array(
						'name' 	=> 'exclude_site_logo',
						'description' => '',
						'title' => __( 'Site Logo', 'themify' ),
						'type' 	=> 'dropdownbutton',
						'states' => themify_ternary_states( array(
							'icon_no' => THEMIFY_URI . '/img/ddbtn-check.png',
							'icon_yes' => THEMIFY_URI . '/img/ddbtn-cross.png', 
							) ),
						'class' => 'hide-if none header-menu-split',
						'after' => '<div class="clear"></div>',
					),
					// Show Site Tagline
					array(
						'name' 	=> 'exclude_site_tagline',
						'description' => '',
						'title' => __( 'Site Tagline', 'themify' ),
						'type' 	=> 'dropdownbutton',
						'states' => themify_ternary_states( array(
							'icon_no' => THEMIFY_URI . '/img/ddbtn-check.png',
							'icon_yes' => THEMIFY_URI . '/img/ddbtn-cross.png', 
							) ),
						'class' => 'hide-if none',
						'after' => '<div class="clear"></div>',
					),
					// Show Search Form
					array(
						'name' 	=> 'exclude_search_form',
						'description' => '',
						'title' => __( 'Search Form', 'themify' ),
						'type' 	=> 'dropdownbutton',
						'states' => themify_ternary_states( array(
							'icon_no' => THEMIFY_URI . '/img/ddbtn-check.png',
							'icon_yes' => THEMIFY_URI . '/img/ddbtn-cross.png', 
							) ),
						'class' => 'hide-if none',
						'after' => '<div class="clear"></div>',
					),
					// Show RSS Link
					array(
						'name' 	=> 'exclude_rss',
						'description' => '',
						'title' => __( 'RSS Link', 'themify' ),
						'type' 	=> 'dropdownbutton',
						'states' => themify_ternary_states( array(
							'icon_no' => THEMIFY_URI . '/img/ddbtn-check.png',
							'icon_yes' => THEMIFY_URI . '/img/ddbtn-cross.png', 
							) ),
						'class' => 'hide-if none',
						'after' => '<div class="clear"></div>',
					),
					// Show Social Widget
					array(
						'name' 	=> 'exclude_social_widget',
						'description' => '',
						'title' => __( 'Social Widget', 'themify' ),
						'type' 	=> 'dropdownbutton',
						'states' => themify_ternary_states( array(
							'icon_no' => THEMIFY_URI . '/img/ddbtn-check.png',
							'icon_yes' => THEMIFY_URI . '/img/ddbtn-cross.png', 
							) ),
						'class' => 'hide-if none',
						'after' => '<div class="clear"></div>',
					),
					// Show Menu Navigation
					array(
						'name' 	=> 'exclude_menu_navigation',
						'description' => '',
						'title' => __( 'Menu Navigation', 'themify' ),
						'type' 	=> 'dropdownbutton',
						'states' => themify_ternary_states( array(
							'icon_no' => THEMIFY_URI . '/img/ddbtn-check.png',
							'icon_yes' => THEMIFY_URI . '/img/ddbtn-cross.png', 
							) ),
						'class' => 'hide-if none header-menu-split',
						'after' => '<div class="clear"></div>',
					),
					array(
						'name' 	=> 'exclude_cart_icon',
						'description' => '',
						'title' => __( 'Cart Icon', 'themify' ),
						'type' 	=> 'dropdownbutton',
						'states' => themify_ternary_states( array(
							'icon_no' => THEMIFY_URI . '/img/ddbtn-check.png',
							'icon_yes' => THEMIFY_URI . '/img/ddbtn-cross.png', 
							) ),
						'class' => '',
						'after' => '<div class="clear"></div>',
						'display_callback' => 'themify_is_woocommerce_active'
					),
				),
				'description' => '',
				'before' => '',
				'after' => '<div class="clear"></div>',
				'separator' => ''
			)
		),
		// Mobile Menu Style
        array(
			'name'	=> 'mobile_menu_style',
			'title'	=> __('Mobile Menu Style', 'themify'),
			'type'	=> 'radio',
			'meta'		=> array(
				array('value' => '', 'name' => __('Default', 'themify')),
				array('value' => 'slide-menu', 'name' => __('Slide Menu', 'themify')),
				array('value' => 'dropdown-menu', 'name' => __('Dropdown Menu', 'themify') ),
			),
			'class'		  => 'hide-if none header-leftpane header-rightpane header-minbar header-slide-out header-overlay',
			'default' => ''
		),
		// Header Wrap
		array(
			'name'          => 'background_type',
			'title'         => __( 'Header Background Type', 'themify' ),
			'description'   => '',
			'type'          => 'radio',
			'show_title'    => true,
			'meta'          => array(
				array(
					'value'    => 'solid',
					'name'     => __( 'Image Background', 'themify' ),
					'selected' => true
				),
				array(
					'value' => 'transparent',
					'name'  => __( 'Transparent Header', 'themify' ),
				),
				array(
					'value' => 'slider',
					'name' => __( 'Image Slider', 'themify' ),
				),
				array(
					'value' => 'video',
					'name' => __( 'Video Background', 'themify' ),
				),
			),
			'enable_toggle' => true,
			'class'     => 'hide-if none clear header-menu-bar',
			'default' => 'solid',
		),
		// Animated Colors
		array(
			'name' 		=> '_animated_colors',
			'title'		=> __('Animating Colors', 'themify'),
			'description' => sprintf( __('Animating Colors can be configured at <a href="%s">Themify > Settings > Theme Settings</a>', 'themify'), esc_url( add_query_arg( 'page', 'themify', admin_url( 'admin.php' ) ) ) ),
			'type' 		=> 'post_id_info',
			'toggle'	=> 'colors-toggle',
		),
		// Select Background Gallery
		array(
			'name' 		=> 'background_gallery',
			'title'		=> __('Header Slider', 'themify'),
			'description' => '',
			'type' 		=> 'gallery_shortcode',
			'toggle'	=> 'slider-toggle',
			'class'     => 'hide-if none header-menu-bar',
		),
		// Background Mode
		array(
			'name'		=> 'background_mode',
			'title'		=> __('Slider Mode', 'themify'),
			'type'		=> 'radio',
			'meta'		=> array(
				array('value' => 'fullcover', 'selected' => $args['background_mode'] == 'fullcover', 'name' => __('Full Cover', 'themify')),
				array('value' => 'best-fit', 'selected' => $args['background_mode'] == 'best-fit', 'name' => __('Best Fit', 'themify'))
			),
			'enable_toggle'	=> true,
			'toggle'	=> 'enable_toggle_child slider-toggle',
			'class'     => 'hide-if none header-menu-bar',
			'default' => 'fullcover',
		),
		// Background Position
		array(
			'name'		=> 'background_position',
			'title'		=> __('Slider Image Position', 'themify'),
			'type'		=> 'dropdown',
			'meta'		=> array(
				array('value' => '', 'name' => '', 'selected' => true),
				array('value' => 'left-top', 'name' => __('Left Top', 'themify')),
				array('value' => 'left-center', 'name' => __('Left Center', 'themify')),
				array('value' => 'left-bottom', 'name' => __('Left Bottom', 'themify')),
				array('value' => 'right-top', 'name' => __('Right Top', 'themify')),
				array('value' => 'right-center', 'name' => __('Right Center', 'themify')),
				array('value' => 'right-bottom', 'name' => __('Right Bottom', 'themify')),
				array('value' => 'center-top', 'name' => __('Center Top', 'themify')),
				array('value' => 'center-center', 'name' => __('Center Center', 'themify')),
				array('value' => 'center-bottom', 'name' => __('Center Bottom', 'themify'))
			),
			'toggle'	=> 'enable_toggle_child slider-toggle',
			'class'     => 'hide-if none header-menu-bar',
		),
		array(
			'type' => 'multi',
			'name' => '_video_select',
			'title' => __('Header Video', 'themify'),
			'meta' => array(
				'fields' => array(
					// Video File
					array(
						'name' 		=> 'video_file',
						'title' 	=> __('Video File', 'themify'),
						'description' => '',
						'type' 		=> 'video',
						'meta'		=> array(),
					),
				),
				'description' => __('Video format: mp4. Note: video background does not play on mobile, background image will be used as fallback.', 'themify'),
				'before' => '',
				'after' => '',
				'separator' => ''
			),
			'toggle'	=> 'video-toggle',
			'class'     => 'hide-if none header-menu-bar',
		),
		// Background Color
		array(
			'name'        => 'background_color',
			'title'       => __( 'Header Background', 'themify' ),
			'description' => '',
			'type'        => 'color',
			'meta'        => array( 'default' => null ),
			'toggle'      => array( 'solid-toggle', 'slider-toggle', 'video-toggle' ),
			'class'     => 'hide-if none',
			'format'      => 'rgba',
		),
		// Background image
		array(
			'name'        => 'background_image',
			'title'       => '',
			'type'        => 'image',
			'description' => '',
			'meta'        => array(),
			'before'      => '',
			'after'       => '',
			'toggle'      => array( 'solid-toggle', 'video-toggle' ),
			'class'     => 'hide-if none header-menu-bar',
		),
		// Background repeat
		array(
			'name'        => 'background_repeat',
			'title'       => '',
			'description' => __( 'Background Image Mode', 'themify' ),
			'type'        => 'dropdown',
			'meta'        => array(
				array(
					'value' => '',
					'name'  => ''
				),
				array(
					'value' => 'repeat',
					'name'  => __( 'Repeat all', 'themify' )
				),
				array(
					'value' => 'no-repeat',
					'name'  => __( 'No repeat', 'themify' )
				),
				array(
					'value' => 'repeat-x',
					'name'  => __( 'Repeat horizontally', 'themify' )
				),
				array(
					'value' => 'repeat-y',
					'name'  => __( 'Repeat vertically', 'themify' )
				),
			),
			'toggle'      => array( 'solid-toggle', 'video-toggle' ),
			'class'     => 'hide-if none header-menu-bar',
		),
		// Header Slider Auto
		array(
			'name' 		=> 'background_auto',
			'title'		=> __('Autoplay', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',
			'meta'		=> array(
				array('value' => 'yes', 'name' => __('Yes', 'themify'), 'selected' => true),
				array('value' => 'no', 'name' => __('No', 'themify'))
			),
			'toggle'	=> 'slider-toggle',
			'default' => 'yes',
			'class'     => 'hide-if none header-menu-bar',
		),
		// Header Slider Auto Timeout
		array(
			'name' 		=> 'background_autotimeout',
			'title'		=> __('Autoplay Timeout', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',
			'meta'		=> array(
				array('value' => 1, 'name' => __('1 Secs', 'themify')),
				array('value' => 2, 'name' => __('2 Secs', 'themify')),
				array('value' => 3, 'name' => __('3 Secs', 'themify')),
				array('value' => 4, 'name' => __('4 Secs', 'themify')),
				array('value' => 5, 'name' => __('5 Secs', 'themify'), 'selected' => true),
				array('value' => 6, 'name' => __('6 Secs', 'themify')),
				array('value' => 7, 'name' => __('7 Secs', 'themify')),
				array('value' => 8, 'name' => __('8 Secs', 'themify')),
				array('value' => 9, 'name' => __('9 Secs', 'themify')),
				array('value' => 10, 'name' => __('10 Secs', 'themify'))
			),
			'toggle'	=> 'slider-toggle',
			'default' => 5,
			'class'     => 'hide-if none header-menu-bar',
		),
		// Header Slider Transition Speed
		array(
			'name' 		=> 'background_speed',
			'title'		=> __('Transition Speed', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',
			'meta'		=> array(
				array('value' => 1500, 'name' => __('Slow', 'themify')),
				array('value' => 500, 'name' => __('Normal', 'themify'), 'selected' => true),
				array('value' => 300, 'name' => __('Fast', 'themify'))
			),
			'toggle'	=> 'slider-toggle',
			'default' => 500,
			'class'     => 'hide-if none header-menu-bar',
		),
		// Header Slider Wrap
		array(
			'name' 		=> 'background_wrap',
			'title'		=> __('Wrap Slides', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',
			'meta'		=> array(
				array('value' => 'yes', 'name' => __('Yes', 'themify'), 'selected' => true),
				array('value' => 'no', 'name' => __('No', 'themify'))
			),
			'toggle'	=> 'slider-toggle',
			'default' => 'yes',
			'class'     => 'hide-if none header-menu-bar',
		),
		// Hide Slider Controlls
		array(
			'name'        => 'header_hide_controlls',
			'title'       => __( 'Hide Slider Controls', 'themify' ),
			'description' => '',
			'type'        => 'checkbox',
			'toggle'	=> 'slider-toggle',
			'class'     => 'hide-if none header-menu-bar',
		),
		// Header wrap text color
		array(
			'name'        => 'headerwrap_text_color',
			'title'       => __( 'Header Text Color', 'themify' ),
			'description' => '',
			'type'        => 'color',
			'meta'        => array( 'default' => null ),
			'class'     => 'hide-if none',
			'format'      => 'rgba',
		),
		// Header wrap link color
		array(
			'name'        => 'headerwrap_link_color',
			'title'       => __( 'Header Link Color', 'themify' ),
			'description' => '',
			'type'        => 'color',
			'meta'        => array( 'default' => null ),
			'class'     => 'hide-if none',
			'format'      => 'rgba',
		),
		// Footer Separator
		array(
			'name' => 'footer_separator',
			'type' => 'separator',
			'meta' => array('html'=>'<hr />')
		),
		// Footer Design
		array(
			'name'        => 'footer_design',
			'title'       => __( 'Footer Design', 'themify' ),
			'description' => '',
			'type'        => 'layout',
			'show_title'  => true,
			'meta'        => $args['footer_design_options'],
			'hide'		  => 'none',
			'default' => 'default',
		),
		// Footer Elements
		array(
			'name' 	=> '_multi_footer_elements',	
			'title' => __('', 'themify'), 	
			'description' => 'Footer Elements',	
			'type' 	=> 'multi',
			'class' => 'hide-if none',		
			'meta'	=> array(
				'fields' => array(
					// Show Footer Widgets
					array(
						'name' 	=> 'exclude_footer_widgets',
						'description' => '',
						'title' => __( 'Footer Widgets', 'themify' ),
						'type' 	=> 'dropdownbutton',
						'states' => themify_ternary_states( array(
							'icon_no' => THEMIFY_URI . '/img/ddbtn-check.png',
							'icon_yes' => THEMIFY_URI . '/img/ddbtn-cross.png', 
							) ),
						'class' => 'hide-if none',
						'after' => '<div class="clear"></div>',
					),
					// Show Menu Navigation
					array(
						'name' 	=> 'exclude_footer_menu_navigation',
						'description' => '',
						'title' => __( 'Menu Navigation', 'themify' ),
						'type' 	=> 'dropdownbutton',
						'states' => themify_ternary_states( array(
							'icon_no' => THEMIFY_URI . '/img/ddbtn-check.png',
							'icon_yes' => THEMIFY_URI . '/img/ddbtn-cross.png', 
							) ),
						'class' => 'hide-if none',
						'after' => '<div class="clear"></div>',
					),
					// Show Texts
					array(
						'name' 	=> 'exclude_footer_texts',
						'description' => '',
						'title' => __( 'Footer Text', 'themify' ),
						'type' 	=> 'dropdownbutton',
						'states' => themify_ternary_states( array(
							'icon_no' => THEMIFY_URI . '/img/ddbtn-check.png',
							'icon_yes' => THEMIFY_URI . '/img/ddbtn-cross.png', 
							) ),
						'class' => 'hide-if none',
						'after' => '<div class="clear"></div>',
					),
					// Show Back to Top
					array(
						'name' 	=> 'exclude_footer_back',
						'description' => '',
						'title' => __( 'Back to Top Arrow', 'themify' ),
						'type' 	=> 'dropdownbutton',
						'states' => themify_ternary_states( array(
							'icon_no' => THEMIFY_URI . '/img/ddbtn-check.png',
							'icon_yes' => THEMIFY_URI . '/img/ddbtn-cross.png', 
							) ),
						'class' => 'hide-if none',
						'after' => '<div class="clear"></div>',
					),
				),
				'description' => '',
				'before' => '',
				'after' => '<div class="clear"></div>',
				'separator' => ''
			)
		),
		// Footer widget position
		array(
			'name'        => 'footer_widget_position',
			'title'       => __( '', 'themify' ),
			'class' => 'hide-if none',
			'description' => 'Footer Widgets Position',
			'type'        => 'dropdown',
			'meta'        => array(
				array(
					'value' => '',
					'name'  => __( 'Default', 'themify' )
				),
				array(
					'value' => 'bottom',
					'name'  => __( 'After Footer Text', 'themify' )
				),
				array(
					'value' => 'top',
					'name'  => __( 'Before Footer Text', 'themify' )
				)
			),
		),
		
		// Image Filter Separator
		array(
			'name' => 'image_filter_separator',
			'type' => 'separator',
			'meta' => array('html'=>'<hr />')
		),
		// Image Filter
		array(
			'name'        => 'imagefilter_options',
			'title'       => __( 'Image Filter', 'themify' ),
			'description' => '',
			'type'        => 'dropdown',
			'meta'        => array(
				array( 'name' => '', 'value' => 'initial' ),
				array( 'name' => __( 'None', 'themify' ), 'value' => 'none' ),
				array( 'name' => __( 'Grayscale', 'themify' ), 'value' => 'grayscale' ),
				array( 'name' => __( 'Sepia', 'themify' ), 'value' => 'sepia' ),
				array( 'name' => __( 'Blur', 'themify' ), 'value' => 'blur' ),
			),
			'default' => 'initial',
		),
		// Image Hover Filter
		array(
			'name'        => 'imagefilter_options_hover',
			'title'       => __( 'Image Hover Filter', 'themify' ),
			'description' => '',
			'type'        => 'dropdown',
			'meta'        => array(
				array( 'name' => '', 'value' => 'initial' ),
				array( 'name' => __( 'None', 'themify' ), 'value' => 'none' ),
				array( 'name' => __( 'Grayscale', 'themify' ), 'value' => 'grayscale' ),
				array( 'name' => __( 'Sepia', 'themify' ), 'value' => 'sepia' ),
				array( 'name' => __( 'Blur', 'themify' ), 'value' => 'blur' ),
			),
			'default' => 'initial',
		),
		// Image Filter Apply To
		array(
			'name'        => 'imagefilter_applyto',
			'title'       => __( 'Apply Filter To', 'themify' ),
			'description' => sprintf( __( 'Image filters can be set site-wide at <a href="%s" target="_blank">Themify > Settings > Theme Settings</a>', 'themify' ), admin_url( 'admin.php?page=themify#setting-theme_settings' ) ),
			'type'        => 'radio',
			'meta'        => array(
				array( 'value' => 'initial', 'name' => __( 'Theme Default', 'themify' )),
				array( 'value' => 'all', 'name' => __( 'All Images', 'themify' ) ),
				array( 'value' => 'featured-only', 'name' => __( 'Featured Images Only', 'themify' ), )
			),
			'default' => 'initial',
		),

	);
}