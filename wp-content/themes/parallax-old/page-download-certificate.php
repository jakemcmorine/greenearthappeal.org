<?php 	
if(isset($_GET) && $_GET!=""){
	$image_name =  $_GET['cert_name'];
    $file = $_SERVER["DOCUMENT_ROOT"].'/wp-content/themes/parallax/certs/'.$image_name;
	header("Expires: 0"); 
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
	header("Cache-Control: no-store, no-cache, must-revalidate"); 
	header("Cache-Control: post-check=0, pre-check=0", false); 
	header("Pragma: no-cache"); 
	header("Content-type: application/jpg"); 
	header('Content-length: '.filesize($file)); 
	header('Content-disposition: attachment; filename='.basename($file)); 
	readfile($file); 
	exit;
}