<?php
/**
 * Template to display Portfolio module
 * This template file is deprecated and is kept for backward compatibility.
 *
 * @deprecated
 */ 

/**
 * Load the original template for Portfolio module in Builder
 *
 * @since 2.2.4
 */
include trailingslashit( THEMIFY_BUILDER_TEMPLATES_DIR ) . 'template-portfolio.php';