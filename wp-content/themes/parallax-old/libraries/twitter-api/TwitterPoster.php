<?php
# Define constants
 define('TWITTER_USERNAME', 'greenearthapp');
define('CONSUMER_KEY', '81kA6C9nHT6t8lG2ECe0zctiF');
define('CONSUMER_SECRET', '667r36yxE9LLYgZq8Rw4eRFxy4YXz8L1dRHWwdvfU1x9wd2DrO');
define('ACCESS_TOKEN', '133217356-7YeYO0xwe30ANWw3mRwooVueSgDRqvxLkjdK10oj');
define('ACCESS_TOKEN_SECRET', 'Q9cNHBCWmFmOgxysVLhThGyhTUKMIEcLTjbHdByxsesyg');
define('TWEET_LENGTH', 140); 

/* define('TWITTER_USERNAME', 'phpfreak6');
define('CONSUMER_KEY', 'HUaMqWrQWW1RFajqdtn7JMC25');
define('CONSUMER_SECRET', 'DRtO5HJ0yL19HgviMlQZXGuvk4g01NoRw8EzM7VRnLgGo2Wadp');
define('ACCESS_TOKEN', '2428668830-DEPZBkZfNeluW0Rw1vgEwjAvWia71jTPSDQlrqy');
define('ACCESS_TOKEN_SECRET', 'kH8176GpGjINXN0wnJrkkAMPKgQLRhII02OxsZoRhLh45');
define('TWEET_LENGTH', 140); */


class TwitterPoster {

    private static $library = 'TwitterOAuth';
    private static $twitter = NULL;
    private static $DBH = NULL;

    /**
     * connect: Create an object of the Twitter PHP API either TwitterOAuth
     * or twitter-api-php
     * @access private
     */
    private static function connect() {

        if(self::$library == 'TwitterOAuth') {

            include('TwitterOAuth.php');

            # Create the connection
            self::$twitter = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

            # Migrate over to SSL/TLS
            self::$twitter->ssl_verifypeer = true;

        } else {

            include('TwitterAPIExchange.php');

            self::$twitter = new TwitterAPIExchange(array(
                'oauth_access_token' => ACCESS_TOKEN,
                'oauth_access_token_secret' => ACCESS_TOKEN_SECRET,
                'consumer_key' => CONSUMER_KEY,
                'consumer_secret' => CONSUMER_SECRET
            ));

        }

    }

    /**
     * setDatabase: Pass in a PDO connection, if you've already got one
     * @param $database PDO
     */
    public static function setDatabase($database) {
        self::$DBH = $database;
    }

    /**
     * tweet: Post a new status to Twitter
     * @param $message string
     * @access public
     */
    public static function tweet($message = '') {

        if(empty($message)) {
            return;
        }

        # Load the Twitter object
        if(is_null(self::$twitter)) {
            self::connect();
        }

        if(self::$library == 'TwitterOAuth') {
            $response = self::$twitter->post('statuses/update', array('status' => $message));
			//echo "<pre>"; print_r($response); die;
        } else {
            $url = 'https://api.twitter.com/1.1/statuses/update.json';
            $requestMethod = 'POST';
            $postData = array('status' => $message);
            $response = $twitter->buildOauth($url, $requestMethod)->setPostfields($postData)->performRequest();
        }

        return $response;

     }


 }

?>
