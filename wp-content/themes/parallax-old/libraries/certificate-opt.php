<?php
require_once(dirname(__FILE__).'/tcpdf/config/lang/eng.php');
require_once(dirname(__FILE__).'/tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
if (!class_exists('MYPDF')) {
    class MYPDF extends TCPDF {
    	//Page header
    	public function Header() {
    
				// get the current page break margin
				$bMargin = $this->getBreakMargin();
				// get current auto-page-break mode
				$auto_page_break = $this->AutoPageBreak;
				// disable auto-page-break
				$this->SetAutoPageBreak(false, 0);
				$img_file = dirname(__FILE__).'/img/cert-bg.jpg';
				$this->Image($img_file, 0, 0, 757, 1118, '', '', '', false, 300, '', false, false, 0);
				
				// restore auto-page-break status
				$this->SetAutoPageBreak($auto_page_break, $bMargin);
				// set the starting point for the page content
				$this->setPageMark();
			
    	}
			public	$last_page_flag = false;	
			public function Close() {
				$this->last_page_flag = true;
				parent::Close();
			}
			// Page footer
			public function Footer() {
				
				$tpages = $this->getAliasNbPages();
				$pages = $this->getAliasNumPage();
				// Position at 15 mm from bottom
				$this->SetY(-35);
				// Set font
				$this->SetFont('helvetica', 'I', 9);
				// Page number
				if($this->last_page_flag){
					$this->Cell(0, 10, "Please remit funds to 'Green Earth Appeal' c/o Lloyds TSB, P.O.Box 1000, BX1 1LT", 0, false, 'C', 0, '', 0, false, 'T', 'M');
					$this->SetY(-25);
					$this->SetFont('helvetica', 'I', 9);
					// Page number
					$this->Cell(0, 10, "AC 11418860 SC 77-04-05", 0, false, 'C', 0, '', 0, false, 'T', 'M');
				}
			}
		
    }
    
}
$pageLayout = array(800, 660);
$pdf = new MYPDF('p', 'px', $pageLayout, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Vinh Nguyen');
$pdf->SetAuthor('Marvin');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('');
$pdf->SetKeywords('php programmer, phpcodeteam@gmail.com');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(0, 60, 0, true);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(TRUE);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);
$pdf->SetFont('helvetica', '', 10);
// add a page
$pdf->AddPage();

$pct_str = '';

// list items for bulk partner
$pageLayout = array(757, 1118);
$pdf = new MYPDF('p', 'px', $pageLayout, true, 'UTF-8', false);
// set document information
$pdf->SetCreator('Atul Rana');
$pdf->SetAuthor('Marvin');
$pdf->SetTitle('Certification');
$pdf->SetSubject('');
$pdf->SetKeywords('php programmer, php.freak6@gmail.com');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(0, 0, 0, true);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// set font
$pdf->SetFont('helvetica', '', 40);
// add a page
$pdf->AddPage();
$pdf->SetY(190);
$pdf->SetX(0);
if($tree_planted > 0){
	$tree_plant = $tree_planted.' trees';
}else{
	$tree_plant = $tree_planted.' tree';
}
$certificate_mess0 = ucfirst($company_name);
$certificate_mess1 ='have planted '.$tree_planted.' tree this month to demonstrate their
long-term commitment to helping the environment.';
$certificate_mess2 ="Since becoming a";
$certificate_mess3 ='Green Organisation member '.ucfirst($first_name).'
has planted a total of '.$tree_plant.' under the auspices
of Green Earth Appeal and the United Nations Billion Tree Campaign.';
$certificate_mess4 ="The Green Organisation would like to thank";
$certificate_mess4.="<br>".ucfirst($company_name)."<br>";
$certificate_mess4.="for your effective and ongoing commitment to helping
to improve the environment for generations to come.
Congratulations on your achievements thus far.";

	if ( isset($certificate_text) && !empty($certificate_text) ) {
	$certificate_text_new ="Ha plantado 570 trees en alianza con Sonne Energéticos Una
iniciativa ambiental operada por The Green Earth Appeal";
    $pct_header = <<<EOD
<table cellspacing="0" cellpadding="5" border="0">
    <tr>
        <td colspan="5"><p style="font-family:Tahoma; font-size: 28px; color:#08632a; text-align: center; margin:0;">{$certificate_text_new}</p></td>
    </tr>
</table>
EOD;
} else {
				  $certificate_0= wordwrap($certificate_mess0, 70, "<br />\n");
				  $certificate_mess1= wordwrap($certificate_mess1, 70, "<br />\n");
				  $certificate_2= wordwrap($certificate_mess2, 70, "<br />\n");
				  $certificate_3= wordwrap($certificate_mess3, 70, "<br />\n");
		   
				 $pct_header = '<p style="font-family:Tahoma; font-weight: bold; font-size: 38px; color:#94B23C; margin:0; text-align: center;">'.$certificate_0.'</p><p style="font-family:Tahoma; font-weight: bold; font-size: 22px; color:#435C26; text-align: center; margin-top:0">'.$certificate_mess1.'</p>
				<p style="font-family:Tahoma; font-size: 28px; font-weight: bold; color:#94B23C; text-align: center; margin:0">'.$certificate_2.'</p>
				<p style="font-family:Tahoma; font-weight: bold; font-size: 22px; word-spacing: 10px; color:#475C29; text-align: center; margin-top:0">'.$certificate_3.'</p>';
}

$pdf->writeHTML($pct_header, true, false, true, false, '');

$pct_footer = '<span style="font-family:Tahoma; font-size: 16px; font-weight: bold; color:#475C29;">'.$certificate_mess4.'</span>';

$pdf->SetY(890);
$pdf->SetX(320);
$html3 = '<span style="font-size: 16px; color: #171213">'.date("jS F Y").'</span>';
$pdf->writeHTML($html3, true, false, true, false, '');

$pdf->writeHTMLCell($w=370, $h=0, $x='55', $y='740', $pct_footer, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//Close and output PDF document
//$pdf->Output('cert_name.pdf', 'I');	
$pdf->Output(dirname(__FILE__).'/../certs/'.$cert_name, 'F');   


if (extension_loaded('imagick')){   		//extension imagick is installed
		$cert_path = dirname(__FILE__) . '/../certs/';
		$pdf_path = $cert_path.$cert_name;
		$image_url =  dirname(__FILE__) . '/../certs/';
		create_cert_pdf_to_image($pdf_path, $code, $image_url); 
		$Path = $_SERVER["DOCUMENT_ROOT"]."/wp-content/themes/parallax/certs/".$cert_name;
		if (file_exists($Path)){	
			unlink($Path);         //delete certificate pdf
		}
	}
function create_cert_pdf_to_image($pdf,$code,$image_url) {  //create image from certificate pdf
			$im = new imagick();
			$im->setResolution(300, 300);
			$im->readImage($pdf);
			$im->setImageColorspace(13); 
			$im->setImageCompression(Imagick::COMPRESSION_JPEG); 
			$im->setImageCompressionQuality(80);
			$im->setImageFormat('jpeg'); 
			$im->resizeImage(1199, 1771, imagick::FILTER_LANCZOS, 1);  
			$im->writeImage($image_url.'full_'.$code.'.jpg');  
			$im->clear(); 
			$im->destroy(); 
			return ;
		}
//============================================================+
// END OF FILE
//============================================================+
