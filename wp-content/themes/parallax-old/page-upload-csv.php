<?php
/**
* Template Name: Upload CSV
*/

get_header('uploadcsv'); 
global $wpdb;

?>
<link rel='stylesheet' id='upload-css' href='<?php echo site_url(); ?>/wp-content/themes/parallax/opt-css/custom-upload.css' type='text/css' media='all'/>
<body>
	<div class="bs-example">
	 <h3 class="wh_head" class="text-center">The Green Organisation Double Opt In process</h3> 
		<div class="container">
		<h3 class="wh_sub_head" class="text-center">CSV Upload Form</h3> 
		<div class="col-md-12 col-sm-12">
		<div class="container center_div">
			<?php if ( isset($_FILES['csvfile'])){

					$filename = $_FILES['csvfile']['name']; // set your file name 
					$filename = str_replace(" ","_",$filename);
					$filename = basename($filename,".csv");
			
					$csvFile = fopen($_FILES['csvfile']['tmp_name'], 'r');
					
					//skip first line
					fgetcsv($csvFile);
					   //parse data from csv file line by line
					  $count = 1;
					$rand = substr(md5(microtime()),rand(0,9),5);

					$filename = $filename.'_'.$rand.'.csv';
					$filepath = $_SERVER["DOCUMENT_ROOT"].'/optin-csv/'.$filename;
					$fp = fopen($filepath, 'w');
					$count = 1;
					while(($line = fgetcsv($csvFile)) !== FALSE){
						
						$randon_no = substr(md5(microtime()),rand(0,9),6);
						$unique_url = "https://greenearthappeal.org/free-tree/".$randon_no;
						
						$tablename = $wpdb->prefix.'optin_users_data';
						$data = array(
								'first_name' => $line[0], 
								'last_name' => $line[1], 
								'company_name' => $line[2], 
								'email' => $line[3], 
								'website' => $line[4], 
								'unique_url' => $randon_no
								);
								
						$wpdb->insert( $tablename, $data);
						if($count == '1'){
							$rows = array('first_name','last_name','company_name','email','website','unique_url');
							fputcsv($fp, $rows);
							$rows = array($line[0], $line[1], $line[2], $line[3], $line[4], $unique_url);
							fputcsv($fp, $rows);
						}else{
							$rows = array($line[0], $line[1], $line[2], $line[3], $line[4], $unique_url);
							fputcsv($fp, $rows);
						}
						
						
						$count++;
					}
					
					//close opened csv file
					fclose($csvFile);
					
					echo "<div class='alert alert-success'>
						  <strong>Success!</strong> CSV created successfully with unique url's.
						</div>";
									
					?>
					<script>
						window.location.href="https://greenearthappeal.org/optin-csv/<?php echo $filename ?>";
					</script>

				<?php	
		}
		?>
		<div class="plant-registration">
		   <div class="wh_download">
			  <a href="<?php echo site_url(); ?>/optin-csv/sample-csv.csv" class="btn btn-success" role="button">Download Sample CSV</a>
		   </div>
			<form action="" method="POST" action="" enctype="multipart/form-data">
				<div class="form-group">
					<label for="csvfile">Upload CSV file :</label>
					<input type="file" class="form-control" id="csvfile" accept=".csv,.CSV" name="csvfile" required/>
			    </div>
			 <button type="submit" class=" wh_btn btn btn-primary">Upload</button>
			</form> 
		</div>
		</div>
		</div>
		</div>
	</div>
<div class="footer_black"></div>	
</body>
</html>
		
<?php 
//get_footer('optin');
?>