<?php
/**
* Template Name: Free Tree
*/
// get_header('optin'); 
global $wpdb;
session_start();
$page = '';
$code = basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$tablename = $wpdb->prefix.'optin_users_data';
if(!empty($code) && $code!='free-tree'){
			
			$tablename = $wpdb->prefix.'optin_users_data';
			
			$result = $wpdb->get_row( 'SELECT * FROM '.$tablename.' WHERE `unique_url` = "'.$code.'"' , OBJECT );
			if(count($result)>0 && $result->email_verified !='1') { 
			  $person_name =  $result->first_name.' '.$result->last_name;
			?>
		<!DOCTYPE html>
		<html <?php language_attributes(); ?>>
		<head>
		<?php
		/** Themify Default Variables
		 *  @var object */
		global $themify; ?>
		<meta charset="<?php bloginfo( 'charset' ); ?>">				
		<title><?php echo ucfirst($result->company_name); ?> - Tree Planting Certificate</title>
		<link rel='dns-prefetch' href='//www.google.com'/>
		<!-- This site is optimized with the Yoast SEO plugin v7.2 - https://yoast.com/wordpress/plugins/seo/ -->
		<link rel="canonical" href="https://greenearthappeal.org/free-tree"/>
		<meta name="description" content="Thanks to <?php echo ucfirst($person_name); ?> - <?php echo ucfirst($result->company_name); ?> has planted a tree in the developing world with Green Earth Appeal."/>
		<meta property="og:locale" content="en_GB"/>
		<meta property="og:type" content="article"/>
		<meta property="og:title" content="<?php echo ucfirst($person_name); ?> - <?php echo ucfirst($result->company_name); ?> - Green Earth Appeal"/>
		<meta property="og:url" content="https://greenearthappeal.org/free-tree"/>
		<meta property="og:site_name" content="Green Earth Appeal"/>
		<meta property="og:description" content="Thanks to <?php echo ucfirst($person_name); ?> - <?php echo ucfirst($result->company_name); ?> has planted a tree in the developing world with Green Earth Appeal."/>
		<meta property="article:publisher" content="https://www.facebook.com/GreenEarthAppeal/"/>
		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:title" content="Free tree - Green Earth Appeal"/>
		<meta name="twitter:site" content="@greenearthapp"/>
		<meta name="twitter:creator" content="@greenearthapp"/>
		<meta name="twitter:description" content="Thanks to <?php echo ucfirst($person_name); ?> - <?php echo ucfirst($result->company_name); ?> has planted a tree in the developing world with Green Earth Appeal."/>
		<script type='application/ld+json'>{"@context":"https:\/\/schema.org","@type":"Organization","url":"https:\/\/greenearthappeal.org\/","sameAs":["https:\/\/www.facebook.com\/GreenEarthAppeal\/","https:\/\/www.instagram.com\/greenearthappeal\/","https:\/\/www.linkedin.com\/company-beta\/955791\/","https:\/\/www.youtube.com\/user\/GreenEarthAppeal","https:\/\/www.pinterest.co.uk\/greenearthappea","https:\/\/twitter.com\/greenearthapp"],"@id":"#organization","name":"Green Earth Appeal","logo":"https:\/\/greenearthappeal.org\/wp-content\/uploads\/2017\/06\/green_earth_appeal_logo.jpg"}</script>	
			<?php }
			if(count($result)>0 && $result->email_verified =='1'){ 
			  $person_name =  $result->first_name.' '.$result->last_name;
			   $image_url= 'https://www.greenearthappeal.org/wp-content/themes/parallax/certs/full_'.$result->unique_url.'.jpg';
			   $thumb_url= 'https://www.greenearthappeal.org/wp-content/themes/parallax/certs/thumb_'.$result->unique_url.'.jpg';
			   $result->company_name = str_replace("\'","'",$result->company_name);
			   if (extension_loaded('imagick')){  
					$outFile = 'optin-csv/certs/social_'.$result->unique_url.'.jpg';
					$share_file_url = site_url('/'.$outFile);
					$image = new Imagick( $image_url);
					$image->cropImage(1200,800, 0,10);
					$image->writeImage($outFile); 
				  }else{
					  $share_file_url = $thumb_url;
				  }
			?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php
/** Themify Default Variables
 *  @var object */
global $themify; ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">				
<title><?php echo ucfirst($result->company_name); ?> - Tree Planting Certificate</title>
<meta property='og:image'  content="<?php echo $share_file_url; ?>" />
<meta property='og:image:secure_url' content="<?php echo $share_file_url; ?>"/>
<meta property='og:url' content="<?php echo $share_file_url; ?>"/>
<link rel='dns-prefetch' href='//www.google.com'/>
<!-- This site is optimized with the Yoast SEO plugin v7.2 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="https://greenearthappeal.org/free-tree"/>
<meta name="description" content="Thanks to <?php echo ucfirst($person_name); ?> - <?php echo ucfirst($result->company_name); ?> has planted a tree in the developing world with Green Earth Appeal."/>
<meta property="og:locale" content="en_GB"/>
<meta property="og:type" content="article"/>
<meta property="og:title" content="<?php echo ucfirst($person_name); ?> - <?php echo ucfirst($result->company_name); ?> - Green Earth Appeal"/>
<meta property="og:url" content="https://greenearthappeal.org/free-tree"/>
<meta property="og:site_name" content="Green Earth Appeal"/>
<meta property="og:description" content="Thanks to <?php echo ucfirst($person_name); ?> - <?php echo ucfirst($result->company_name); ?> has planted a tree in the developing world with Green Earth Appeal."/>
<meta property="article:publisher" content="https://www.facebook.com/GreenEarthAppeal/"/>
<meta name="twitter:card" content="summary"/>
<meta name="twitter:title" content="Free tree - Green Earth Appeal"/>
<meta name="twitter:site" content="@greenearthapp"/>
<meta name="twitter:creator" content="@greenearthapp"/>
<meta name="twitter:description" content="Thanks to <?php echo ucfirst($person_name); ?> - <?php echo ucfirst($result->company_name); ?> has planted a tree in the developing world with Green Earth Appeal."/>
<script type='application/ld+json'>{"@context":"https:\/\/schema.org","@type":"Organization","url":"https:\/\/greenearthappeal.org\/","sameAs":["https:\/\/www.facebook.com\/GreenEarthAppeal\/","https:\/\/www.instagram.com\/greenearthappeal\/","https:\/\/www.linkedin.com\/company-beta\/955791\/","https:\/\/www.youtube.com\/user\/GreenEarthAppeal","https:\/\/www.pinterest.co.uk\/greenearthappea","https:\/\/twitter.com\/greenearthapp"],"@id":"#organization","name":"Green Earth Appeal","logo":"https:\/\/greenearthappeal.org\/wp-content\/uploads\/2017\/06\/green_earth_appeal_logo.jpg"}</script>	
				
<?php  get_header('optin'); 
	$page = '0';
	}else{
		$page = '1';
		get_header('uploadcsv'); 
	}
}else{
	$page = '1';
	get_header('uploadcsv'); 
}
?>	
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel='stylesheet' id='upload-css' href='<?php echo get_template_directory_uri(); ?>/opt-css/custom-tree-free.css?ver=0223' type='text/css' media='all'/>
<body>
	<div class="bs-example">
	<?php if($page == '1'){  ?>
		<!--h3 class="wh_head" class="text-center">The Green Organisation Double Opt In process</h3-->
		<div class="freetree_head">
			<h2>Plant your free tree</h2>
			<img class="fist_img" src="<?php echo get_template_directory_uri(); ?>/social-images/green_logo.png" alt="Green Earth Appeal" />
			<div class="partnershp_img"><p>In Partnership With</p>
			<img class="secd_img" src="<?php echo get_template_directory_uri(); ?>/social-images/green_org_logo.png" alt="The Green Organisation" /></div>
		</div>
	<?php } 	?>
	 
		<div class="container">
		<div class="col-md-12 col-sm-12">
		<!--h3 class="wh_sub_head" class="text-center">Green Earth Appeal In Partnership With The Green Organisation</h3--> 
		<div class="center_div">
			<?php
		if(!empty($code) && $code!='free-tree'){
			
			$tablename = $wpdb->prefix.'optin_users_data';
			
			$result = $wpdb->get_row( 'SELECT * FROM '.$tablename.' WHERE `unique_url` = "'.$code.'"' , OBJECT );

			$result->company_name = str_replace("\'","'",$result->company_name);
			
			if(count($result)>0 && $result->email_verified =='1'){
				$download_url = 'http://greenearthappeal.org/wp-content/themes/parallax/certs/full_'.$result->unique_url.'.jpg';
				//echo "<pre>"; print_r($result); die;
				//$share_url= 'https://www.greenearthappeal.org/wp-content/themes/parallax/certs/full_'.$result->unique_url.'.jpg';
				$share_url= 'https://www.greenearthappeal.org/free-tree/'.$code;
				
				?>
				<h3 class="wh_sub_head" class="text-center"><?php echo ucfirst($result->company_name); ?> Tree Planting Certificate</h3> 
				<div class="plant-registration">
					 <!--a style="background: none;" href="https://www.greenearthappeal.org/wp-content/themes/parallax/certs/full_<?php echo $result->unique_url.'.jpg';?>" onclick="return popitup(this)"> </a-->
						<img style="border:1px solid #94B23C;" src="https://www.greenearthappeal.org/wp-content/themes/parallax/certs/full_<?php echo $result->unique_url.'.jpg';?>" />
					<div class="col-xs-12 text-center">
						<a class="btn btn_download btn-primary" id="download-image" href="https://greenearthappeal.org/download-certificate?cert_name=<?php echo 'full_'.$result->unique_url.'.jpg';?>" >Click to Download</a>
					</div>
				</div>
				<script>
					function checkOffset() {
						if(jQuery('#social-float').offset().top + jQuery('#social-float').height() >= jQuery('#footerwrap').offset().top - 10)
							jQuery('#social-float').addClass('social_abs');
							jQuery('#social-float').css('position', 'absolute');
						if(jQuery(document).scrollTop() + window.innerHeight < jQuery('#footerwrap').offset().top)
							jQuery('#social-float').removeClass('social_abs'); // restore when you scroll up
							jQuery('#social-float').css('position', 'fixed'); // restore when you scroll up
						//jQuery('#social-float').text(jQuery(document).scrollTop() + window.innerHeight);
					}
					jQuery(document).scroll(function() {
						checkOffset();
					});
					</script>
				<?php 
				
			}elseif(count($result)>0 && $result->opted =='out'){
					echo "<div class='margin_btm alert alert-warning'>
								  <strong>You are opted out.</strong>
								</div>";
			}
			if(count($result)>0 && isset($_GET['unsubscribe_email'])){
				
				$unsubscribe = $wpdb->update( 
							$tablename , 
							array( 			
								'unsubscribe' => 1
							), 
							array( 'id' => $result->id ) 
						);
						if($unsubscribe){
							echo "<div class='margin_btm alert alert-success'>
								  <strong>Unsubscribed from email alerts.</strong>
								</div>";
							$wp_redirect = 'https://greenearthappeal.org/free-tree/'. $code .'';
					?>
					<script>
					setTimeout(function () {
					   window.location.href = '<?php echo $wp_redirect; ?>'; 
					}, 2000); 
					</script>
						<?php }
				
			}
			
			if(count($result)>0 && isset($_GET['verify_email'])){
				
				$verify_email = $_GET['verify_email'];
				$res = $wpdb->get_row( 'SELECT * FROM '.$tablename.' WHERE `verify_email_address_key` = "'.$verify_email.'"' , OBJECT );
					
					
				if(count($res)>0 && $res->email_verified =='0'){
					
					if($res->email_verified!='1'){
						
							$ip_address_step2 = $_SERVER['REMOTE_ADDR'];
							$timestamp_step2 = date('Y-m-d H:i:s');

							$update = $wpdb->update( 
								$tablename , 
								array( 			
									'email_verified' => 1,
									'ip_address_step2' => $ip_address_step2,	
									'timestamp_step2' => $timestamp_step2
								), 
								array( 'id' => $res->id ) 
							);
						if($update){
							
							require('libraries/twitter-api/TwitterPoster.php');
							
							$unique_page_url = 'https://greenearthappeal.org/free-tree/'. $code .'';
							
							if($res->company_twitter_id!="" && $res->personal_twitter_id!=""){
								$tweet_message = "We have planted a tree on the persons behalf @".trim($res->company_twitter_id).' and @'.trim($res->personal_twitter_id);
							}elseif($res->company_twitter_id!=""){
								$tweet_message = "We have planted a tree on the persons behalf @".trim($res->company_twitter_id);
							}elseif($res->personal_twitter_id!="") {
								$tweet_message = "We have planted a tree on the persons behalf @".trim($res->personal_twitter_id);
							}
							
							$tweet_message.= " certificate page: ".$unique_page_url;
							
							$response = TwitterPoster::tweet($tweet_message);
							
							//echo "<pre>"; print_r($response); die;
							if(isset($response['id']) && $response['id']!=""){
								$update_status = $wpdb->update( 
									$tablename , 
									array( 						
										'opted' => 'in'	
									), 
									array( 'unique_url' => $code ));
							}
							
							/* echo "<div class='margin_btm  alert alert-success'>
								  <strong>Thank you for verifying your details.</strong>
								</div>"; */
								echo '<style>.footer_black{float:left;width:100%;height:40px;background:#000;position:absolute;bottom:0px;}</style>';
								$wp_redirect = 'https://greenearthappeal.org/free-tree/'. $code .'';
							?>
							<script>
							/* setTimeout(function () {
							   window.location.href = '<?php echo $wp_redirect; ?>'; 
							}, 2000);  */
							window.location.href = '<?php echo $wp_redirect; ?>'; 
							</script>
							
					<?php 

						}
					} ?>
					
					
				<?php }elseif($res->email_verified=='1'){

					echo "<div class='margin_btm  alert alert-success'>
								  <strong>Email address already verified.</strong>
								</div>";
						echo '<style>.footer_black{float:left;width:100%;height:40px;background:#000;position:absolute;bottom:0px;}</style>';
					
					$wp_redirect = 'https://greenearthappeal.org/free-tree/'. $code .'';
					?>
					<script>
					setTimeout(function () {
					   window.location.href = '<?php echo $wp_redirect; ?>'; 
					}, 2000); 
					</script>
					
			<?php 	}else{
					echo "<div class='margin_btm alert alert-warning'>
						  <strong>Wrong Activation key!</strong>
						</div>";
						echo '<style>.footer_black{float:left;width:100%;height:40px;background:#000;position:absolute;bottom:0px;}</style>';
				}	
			}
			if(count($result)>0 && $result->ip_address_step1==""){
				 if ( isset($_POST['email'])){
							//echo "<pre>"; print_r($_POST); die;
							$first_name = stripslashes($_POST['first_name']);
							$last_name  = stripslashes($_POST['last_name']);
							$email 		= $_POST['email'];
							$website 	= $_POST['website'];
							$company_name = stripslashes($_POST['company_name']);
							$company_twitter_id = $_POST['company_twitter_id'];
							$personal_twitter_id = $_POST['personal_twitter_id'];
							$terms = $_POST['terms'];
							
							$ip_address_step1 = $_SERVER['REMOTE_ADDR'];
					
							$timestamp_step1 = date('Y-m-d H:i:s');

								$update = $wpdb->update( 
									$tablename , 
									array( 						
										'first_name' => $first_name,	
										'last_name' => $last_name,	
										'email' => $email,	
										'company_name' => $company_name,	
										'website' => $website,	
										'company_twitter_id' => $company_twitter_id,
										'personal_twitter_id' => $personal_twitter_id,
										'ip_address_step1' => $ip_address_step1,	
										'timestamp_step1' => $timestamp_step1
									), 
									array( 'unique_url' => $code ) 
								);
								if($terms=='1'){
									
									$verify_email_address = substr(md5(microtime()),rand(0,9),10);
									
									$update_verify_email_address = $wpdb->update( 
									$tablename , 
									array( 						
										'verify_email_address_key' => $verify_email_address,
										'to_count_email_hours' => $timestamp_step1,
										'accept_tc' => '1'	
									), 
									array( 'unique_url' => $code ));
									
									$cert_name = 'The_Green_Earth_Appeal_Certificate_'. $code . '.pdf';
									
									$tree_planted = '1';
									
									require('libraries/certificate-opt.php');
									
									$verify_email_link = 'https://greenearthappeal.org/free-tree/'. $code . '/?verify_email=' . $verify_email_address . '';
									$subject = "Verify your email address - Plant a Free Tree";
									$headers  = 'MIME-Version: 1.0' . "\r\n";
									$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
									$headers .= 'From: The Green Organisation <partners@greenearthappeal.org>' . "\r\n";
									$message = "";
									$message .='<p style="line-height:1.625;font-size:14px;margin:0;color:#222!important;">Hi '.$result->first_name.',</p>'."<br><br>";
									$message .= "<p style='line-height:1.625;font-size:14px;margin:0;color:#222!important;'>Thank you for requesting to plant a free fruit tree in the developing world - you are one step closer to making a huge difference!</p>".'<br>';
									$message .= "<p style='line-height:1.625;font-size:14px;margin:0;color:#222!important;'>To plant your tree - click on the link below to verify your email address:</p>".'<br>';
									$message .= "<p style='line-height:1.625;font-size:14px;margin:0;color:#222!important;'>".$verify_email_link.'</p><br>';
									$message .= "<p style='line-height:1.625;font-size:14px;margin:0;color:#222!important;'>Keep up the great work,</p>".'<br>';
									$message .= "<p style='line-height:1.625;font-size:14px;margin:0;color:#222!important;'>The Green Earth Appeal in Partnership with The Green Organisation</p>".'<br>';
									$message .= "<img style='width: 376px;' src='https://greenearthappeal.org/wp-content/themes/parallax/green_earth_appeal_green_organisation_logo.png' alt='The Green Organisation'>";
									@wp_mail($email, $subject, $message, $headers); 
									
									if($update==1){
										echo "<div class='margin_btm alert alert-success'>
												<strong>Success!</strong> Record updated successfully.Please check Inbox to verify your email address.
											</div>";
										echo '<style>.footer_black{float:left;width:100%;height:40px;background:#000;position:absolute;bottom:0px;}</style>';											
										}
									
								}else{
									// mark as opted out
									$update = $wpdb->update( 
									$tablename , 
									array( 						
										'opted' => 'out'	
									), 
									array( 'unique_url' => $code ));
									if($update==1){
										echo "<div class='margin_btm  alert alert-success'>
												<strong>Success!</strong> You are marked as opted out.
											</div>";
										echo '<style>.footer_black{float:left;width:100%;height:40px;background:#000;position:absolute;bottom:0px;}</style>';
									}
								}
								
								$result = $wpdb->get_row( 'SELECT * FROM '.$tablename.' WHERE `unique_url` = "'.$code.'"' , OBJECT );

				} 
			if(count($result)>0 && $result->ip_address_step1==""){
			?>
				<div class="plant-registration">
					<form action="" method="POST" id="form_opted_in" action="" enctype="multipart/form-data">
						 <div class="form-group">
							<label for="first_name">First Name <span class="red" data-tip="Required field">*</span></label>
								<input type="text" class="form-control" id="first_name" value="<?php echo esc_attr( $result->first_name ); ?>" name="first_name" required>
						  </div>
						   <div class="form-group">
							<label for="last_name">Last Name <span class="red" data-tip="Required field">*</span></label>
								<input type="text" class="form-control" id="last_name" value="<?php echo esc_attr( $result->last_name ); ?>" name="last_name" required>
						  </div>
						   <div class="form-group">
							<label for="company_name">Company Name <span class="red" data-tip="Required field">*</span></label>
								<input type="text" class="form-control" id="company_name" value="<?php echo esc_attr( $result->company_name ); ?>" name="company_name" required>
						  </div>
						  <div class="form-group">
							<label for="email">Email Address <span class="red" data-tip="Required field">*</span></label>
								<input type="email" class="form-control" id="email" value="<?php echo esc_attr( $result->email ); ?>" name="email" required>
						  </div>
						  <div class="form-group">
							<label for="email">Website </label>
								<input type="url" class="form-control" id="website" value="<?php echo esc_attr( $result->website ); ?>" name="website">
						  </div>
						  <div class="form-group">
							<label for="company_twitter_id">Company Twitter ID </label>
								<input type="text" class="form-control" id="company_twitter_id" value="<?php echo esc_attr( $result->company_twitter_id ); ?>" name="company_twitter_id">
						  </div>
						  <div class="form-group">
							<label for="personal_twitter_id">Personal Twitter ID </label>
								<input type="text" class="form-control" id="personal_twitter_id" value="<?php echo esc_attr( $result->personal_twitter_id ); ?>" name="personal_twitter_id">
						  </div>
						  <div class="checkbox">
							<label><input id="terms" value="1" name="terms" type="checkbox" required> <b>Accept <a href="#"> Terms and Conditions</a></b></label>
						  </div>
						 <button type="submit" id="update_submit_button" class="btn btn-primary">Update</button>
						 <div id="term_condition_check">
						 </div>
					</form> 
				</div>
			<?php 	}		
			}elseif(count($result)==0){
				echo "<div class='margin_btm alert alert-warning'>
						  <strong>No Result Found!</strong>
						</div>";
				echo '<style>.footer_black{float:left;width:100%;height:40px;background:#000;position:absolute;bottom:0px;}</style>';	
			} 
		} ?>
		</div>
		</div>
		</div>
		<?php if($share_url!=""){ ?>
			<div class="social-float-parent">
						<div id="social-float">
							<div id="share-buttons">
								<p>Share your certificate</p>
									<!-- Facebook -->
									<a href="http://www.facebook.com/sharer.php?u=<?php echo $share_url; ?>" target="_blank">
										<img src="<?php echo get_template_directory_uri(); ?>/social-images/facebook.png" alt="Facebook" />
									</a>
									
									<!-- Google+ -->
									<a href="https://plus.google.com/share?url=<?php echo $share_url; ?>" target="_blank">
										<img src="<?php echo get_template_directory_uri(); ?>/social-images/google.png" alt="Google" />
									</a>
									
									<!-- LinkedIn -->
									<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $share_url; ?>" target="_blank">
										<img src="<?php echo get_template_directory_uri(); ?>/social-images/linkedin.png" alt="LinkedIn" />
									</a>
									<a href="https://pinterest.com/pin/create/bookmarklet/?url=<?php echo $share_url; ?>" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/social-images/pinterest.png" alt="Pinterest" />
									</a>
									
									<!-- Twitter -->
									<a href="https://twitter.com/share?url=<?php echo $share_url; ?>&amp;text=Certificate%20url" target="_blank">
										<img src="<?php echo get_template_directory_uri(); ?>/social-images/twitter.png" alt="Twitter" />
									</a>
								</div>
						</div>
				</div>
		<?php } ?>
		
		
	</div>
</body>
</html>	
<script>

jQuery(document).ready(function () {
	 jQuery("#email").prop('required',true);
	jQuery( "#update_submit_button" ).click(function(e) {
		 e.preventDefault();
		 if(jQuery('#first_name').val()==""){
			  var html = "<div style='margin-top: 15px;' class='alert alert-danger'><strong>First name field is required.</strong></div>";
				jQuery("#term_condition_check").html(html);
				return false;
		 }else if(jQuery('#last_name').val()==""){
			 	 var html = "<div style='margin-top: 15px;' class='alert alert-danger'><strong>Last name field is required.</strong></div>";
				jQuery("#term_condition_check").html(html);
				return false;
		 }else if(jQuery('#company_name').val()==""){
			  var html = "<div style='margin-top: 15px;' class='alert alert-danger'><strong>Company name field is required.</strong></div>";
				jQuery("#term_condition_check").html(html);
				return false;
		 }else if(jQuery('#email').val()==""){
			 var html = "<div style='margin-top: 15px;' class='alert alert-danger'><strong>Email field is required.</strong></div>";
				jQuery("#term_condition_check").html(html);
				return false;
		 }
		 if(jQuery('input[name="terms"]').is(':checked'))
			{	
			    jQuery("#term_condition_check").css('display',"none");
				jQuery("#form_opted_in").submit();
			}else{
				 var html = "<div style='margin-top: 15px;' class='alert alert-danger'><strong>To plant your free tree you must accept our terms and conditions.</strong></div>";
				jQuery("#term_condition_check").html(html);
				return false;
				
			}
			
		
		
	});

});
</script>
<?php 
if($page=='1'){
	echo '<div class="footer_black"></div>';
}else{
	get_footer();
}
?>