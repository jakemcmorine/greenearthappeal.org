<?php
/**
* Template Name: Delete Client
*/
$delete_key = basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)); 
get_header(); 
if(!empty($delete_key) && $delete_key!='delete-client'){
		$url ='https://www.greenearthappeal.org/panacea/index.php/welcome/get_delete_imarcro_client/'.$delete_key;
		$imaro_client = file_get_contents($url);
		$imaro_client = json_decode($imaro_client);
		$hubspot_contact_id = $imaro_client->hubspot_contact_id;
		//echo "<pre>"; print_r($imaro_client); echo "</pre>";
	if(!empty($imaro_client)){
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<div class="bs-example">
		<div class="container">
			<div class="col-md-12 col-sm-12">
				<div class="center_div">
					<h3 class="wh_sub_head">Delete Certificate data</h3>
					<?php 
					if(isset($_POST['delete_key'])){
						$delete_key = $_POST['delete_key'];
						
						if(isset($_POST['hubspot_contact_id'])){
							
								$contact_id = $_POST['hubspot_contact_id'];
								$hapikey = "5bd190b1-2ae5-462d-bd7e-91250b2edbf1";        //live
								$delete_contact_url = 'https://api.hubapi.com/contacts/v1/contact/vid/' .$contact_id .'?hapikey=' . $hapikey;
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL, $delete_contact_url);
								curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								$response = curl_exec($ch);
								$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
								$curl_errors = curl_error($ch);
								$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
								@curl_close($ch);
								/* echo "curl Errors: " . $curl_errors;
								echo "\nStatus code: " . $status_code;
								echo "\nResponse: " . $response; */
						}
						
						$delete_script ='https://www.greenearthappeal.org/panacea/index.php/welcome/delete_imarcro_client_from_database/'.$delete_key;
						$delete_script_data = file_get_contents($delete_script);
						$delete_script_data = json_decode($delete_script_data);
						//echo "<pre>"; print_r($delete_script_data); echo "</pre>";
						if(!empty($delete_script_data)){
							echo "<p class='text-success' style='font-size: 18px;' align='center'>Data removed successfully!</p>";
						}
					}else{
					
					?>
					<div class="plant-registration">
						<div class="col-xs-12 text-center">
							<button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">Click here to Delete</button>
						</div>		
					</div>
					<?php 	} ?>
				</div>
			</div>
	    </div>
	</div>
	<div class="modal fade" id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			 <div class="modal-content">
				<form action="" method="POST">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<p>Are you sure you want to delete?</p>
					</div>
					<input type="hidden" name="delete_key" value="<?php echo $delete_key; ?>">
					<input type="hidden" name="hubspot_contact_id" value="<?php echo $hubspot_contact_id; ?>">
					<div class="modal-footer">
						 <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
						 <button type="submit" class="btn btn-success">Yes</button>
					</div>
				</form>
			 </div>
		</div>
	</div>				
<?php
	}else{
		
		echo "<div style='text-align: center;padding: 24px 25px;' class='alert alert-warning'>
				    <strong>No Result Found!</strong>
				</div>";
	}
	
}else{
		echo "<div style='text-align: center;padding: 24px 25px;' class='alert alert-warning'>
				    <strong>No Result Found!</strong>
				</div>";
	}  ?>
<?php get_footer(); ?>
<style>
.custom_footermenu{
	display:none;
}
.btn-group-lg > .btn, .btn-lg {
    padding: 10px 16px;
    font-size: 13px !important;
    text-transform: uppercase !important;
	margin-top: 20px !important;
}

.container {
    position: relative;
    width: 100%;
    height: 0;
    padding-bottom: 16.25%;
}

.plant-registration a.btn_download {margin: 20px auto 0;float: none;display: inherit;height: auto;text-align: center;padding: 5px 0;border-radius: 4px; position:relative; z-index:9;}
/* .plant-registration .btn_download {
    background-color: #8B0000;
    border: 2px solid #8B0000;
    box-shadow: none;
    color: #ffff;
    cursor: pointer;
    font-size: .95em;
    font-weight: 600;
    height: 40px;
    margin-top: 30px;
    text-transform: uppercase;
    transition: all .3s ease-in-out 0s;
    width: 208px;
    line-height: 25px;
} */
.wh_sub_head {
	float: left;
	width: 100%;
	font-family: Josefin Sans;
	padding-bottom: 18px;
	text-align: center;
	margin-top: 8px;
	padding-top: 25px;
	font-size: 27px;
font-weight: 500;
}
.social-widget,  #searchform-wrap, .social-links.horizontal, #searchform{
	display:none !important;
}
/* #main-nav-wrap ul{margin:0; padding:0;}
#main-nav-wrap ul li {list-style:none; float:left; display:inline-block;}
#main-nav-wrap ul li a{color: #000;
display: block;
padding: .6em 25px;
margin: 0;
text-decoration: none;
-moz-transition: all .2s ease-in-out;
-webkit-transition: all .2s ease-in-out;
transition: all .2s ease-in-out;}
#main-nav-wrap ul li a:hover {color:#ed1e24;} */


@media screen and (max-width:767px)
{
	.wh_sub_head  {font-size: 18px !important;padding-top: 0!important;}
		#share-buttons p { font-size:8px; width:50px;} 
	   .plant-registration a.btn_download {
		font-size: 12px; 
		padding:2px 0;
		width: 150px;
	}
}
</style>

<!--div class="footer_black"></div-->
</body>
</html>