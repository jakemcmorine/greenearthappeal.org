<?php
/** Themify Default Variables
    @var object */
	global $themify; ?>

	<?php themify_layout_after(); //hook ?>
    </div>
	<!-- /body -->

	<?php if( 'yes' != $themify->hide_footer ) : ?>
		<div id="footerwrap">

			<?php themify_footer_before(); // hook ?>
			<footer id="footer" class="pagewidth">
				<?php themify_footer_start(); // hook ?>	

				<?php get_template_part( 'includes/footer-widgets'); ?>

				<p class="back-top"><a href="#header"><?php _e('Back to top', 'themify'); ?></a></p>
			
				<?php if (function_exists('wp_nav_menu')) {
					wp_nav_menu(array('theme_location' => 'footer-nav' , 'fallback_cb' => '' , 'container'  => '' , 'menu_id' => 'footer-nav' , 'menu_class' => 'footer-nav')); 
				} ?>

				<div class="footer-text clearfix">
					<?php themify_the_footer_text(); ?>
					<?php themify_the_footer_text('right'); ?>
				</div>
				<!-- /footer-text --> 
				<?php themify_footer_end(); // hook ?>
			</footer>
			<!-- /#footer --> 
			<?php themify_footer_after(); // hook ?>
		</div>
		<!-- /#footerwrap -->
	<?php endif; // hide_footer check ?>
	
</div>
<!-- /#pagewrap -->

<?php  
$page_id = get_the_ID();
 if($page_id=='13891' && $_GET['registration']==1){ 
 ?>
	 <div class="modal fade" id="google_review_model">
		<div class="modal-dialog modal-sm">
		  <div class="modal-content">
		  <form id="review_form" action="" method="POST">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<p>Will you provide a review of our organisation?</p>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			  <button type="submit" name="review_submit" class="btn btn-primary">Yes</button>
			</div>
			</form>
		  </div>
		</div>
	</div>
	 <style>
	 .modal-content {
	  margin-top: 122px!important;
	}
	
	 </style>
	 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	 <?php
	 global $wpdb;
	$stat_res = $wpdb->get_row("SELECT form_type FROM wpiq_grant_review_statistics ORDER BY id DESC LIMIT 1");

	$form_type = $stat_res->form_type;
	 if($form_type == 1){
		 $form = 2;
		 $review_url = "https://bit.ly/CFD-Reviews";
	 }else{
		 $form = 1;
		 $review_url = "https://bit.ly/GEA-Reviewss";
	 }
		 $wpdb->insert('wpiq_grant_review_statistics', array(
							'form_type' => $form
						   ));
		?>
	<script>
		jQuery(document).ready(function(){
			jQuery('#review_form').attr('action', '<?php echo $review_url;?>');
		setTimeout(function(){
			jQuery("#google_review_model").modal("show");
		}, 4000);
	});
	</script>

<?php } ?>
<?php
/**
 *  Stylesheets and Javascript files are enqueued in theme-functions.php
 */
?>
<?php themify_body_end(); // hook ?>
<!-- wp_footer -->
<?php wp_footer(); ?>
</body>
</html>