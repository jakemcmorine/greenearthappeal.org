<?php
/**
* Template Name: Certificate
*/
error_reporting(0);
$code = basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

if(!empty($code) && $code!='certificate'){
					
		$url ='https://www.greenearthappeal.org/panacea/index.php/welcome/get_imarcro_client_certificate_page/'.$code.'/1';     // value 1 for imacroclient
		$imaro_client = file_get_contents($url);
		$imaro_client = json_decode($imaro_client);
		
		//echo "<pre>"; print_r($imaro_client); die;
		
		$certificate_page_url 		 = 'https://greenearthappeal.org/certificate/'.$imaro_client->certificate_key;
		$certificate_delete_page_url = "https://greenearthappeal.org/delete-client/".$imaro_client->delete_key;
		
		if(!empty($imaro_client)){
			
			$api_url ='https://www.greenearthappeal.org/panacea/index.php/welcome/get_initiative_clients_for_cfd/?email='.$imaro_client->email_address;
			$res = file_get_contents($api_url);	
			$result = json_decode($res);
			
			if(strpos($result->company_number, "linkedin") !== false){       //show error page if wrong company number entered
			
				  global $wp_query;
				  $wp_query->set_404();
				  status_header( 404 );
				  get_template_part( 404 ); exit();
			}

			if($result->company_number != "" && $result->company_number != "0"){         //if company number exist
				  $result->code = $result->company_number;
			}elseif($imaro_client->company_number != ""){
				 $result->code = $imaro_client->company_number;
			}
			 $image_url= 'https://www.greenearthappeal.org/panacea/cert/full_'.$result->code.'.jpg';
			 if (@file_get_contents($image_url, 0, NULL, 0, 1)) {
						   "Exist";
				}else{
					  $result->code = str_replace("&","and",$result->code);
				}
			 $result->code = str_replace("amp;","",$result->code);
			 $result->code = str_replace(";","",$result->code);
			 $result->company = str_replace("\'","'",$result->company);
			
		  if(!empty($result)){
			  $person_name =  ucwords(strtolower($result->first_name.' '.$result->last_name));
			  $image_url= 'https://www.greenearthappeal.org/panacea/cert/full_'.$result->code.'.jpg';
			  //echo "<pre>"; print_r($result); die;
			  $thumb_url= 'https://www.greenearthappeal.org/panacea/cert/thumb_'.$result->code.'.jpg';
			  
			  $share_file_url = $thumb_url;
			// to make company name in first case upper if all the letter are upper
			 if(isset($result->company)){
					$array = explode(' ', $result->company);
					$count = count($array);
					$i=0;
					 foreach ($array as $testcase) {
						if (ctype_upper($testcase)) {
							 $i++;
						}
					} 
					$company_name = $result->company;
					if($count == $i){
					  $company_name = strtolower($result->company);
					  $company_name = ucwords($company_name);
					} 
			 }else{
					$company_name = $result->company;
			 }
				?>
				<script>
					var _hsq = window._hsq = window._hsq || [];
					_hsq.push(["identify",{
						email: '<?php echo $imaro_client->email_address; ?>'
					}]);
				</script>
<!DOCTYPE html>
<html <?php language_attributes(); ?>  prefix="og: http://ogp.me/ns#">
<head>
	<?php
	/** Themify Default Variables
	 *  @var object */
	global $themify; ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="robots" content="noindex, nofollow">
	<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">      
	<title><?php echo ucwords($person_name); ?> &#8211; Green Earth Appeal</title>
	
	<meta property='og:image'  content="<?php echo $share_file_url; ?>" />
	<meta property='og:image:secure_url' content="<?php echo $share_file_url; ?>"/>
	<meta property='og:url' content="<?php echo $share_file_url; ?>"/>
	
	<!-- This site is optimized with the Yoast SEO plugin v7.2 - https://yoast.com/wordpress/plugins/seo/ -->
	<meta name="description" content="Thanks to <?php echo ucfirst($person_name); ?> - <?php echo ucfirst($result->company); ?> has planted a tree in the developing world with Green Earth Appeal."/>
	<link rel="canonical" href="https://greenearthappeal.org/certificate"/>
	<meta property="og:locale" content="en_GB"/>
	<meta property="og:type" content="article"/>
	<meta property="og:title" content="<?php echo ucfirst($person_name); ?> - <?php echo ucfirst($result->company); ?> - Green Earth Appeal"/>
	<meta property="og:description" content="Thanks to <?php echo ucfirst($person_name); ?> - <?php echo ucfirst($result->company); ?> has planted a tree in the developing world with Green Earth Appeal."/>
	<meta property="og:url" content="https://greenearthappeal.org/certificate"/>
	<meta property="og:site_name" content="Green Earth Appeal"/>
	<meta name="twitter:card" content="summary_large_image"/>
	<meta name="twitter:description" content="Thanks to <?php echo ucfirst($person_name); ?> - <?php echo ucfirst($result->company); ?> has planted a tree in the developing world with Green Earth Appeal."/>
	<script type='application/ld+json'>{"@context":"https:\/\/schema.org","@type":"Organization","url":"https:\/\/greenearthappeal.org\/","sameAs":[],"@id":"#organization","name":"Green Earth Appeal","logo":"https:\/\/greenearthappeal.org\/wp-content\/uploads\/2017\/11\/Asset-1.png"}</script>

	<!-- / Yoast SEO plugin. -->
	<!-- wp_head -->
	<?php wp_head(); ?>
	<link rel='stylesheet' id='upload-css' href='<?php echo get_stylesheet_directory_uri(); ?>/cert.css' type='text/css' media='all'/>
</head>
<?php 	

	/*  to create contact on hubspot */ 	
   $hapikey 	=  "5bd190b1-2ae5-462d-bd7e-91250b2edbf1";        //client key
	if($imaro_client->date_birth == "-"){
		$imaro_client->date_birth = "";
	}
	if($imaro_client->linkedin_profile_url == "-"){
		$imaro_client->linkedin_profile_url = "";
	}
	if($imaro_client->twitter1 == "-"){
		$imaro_client->twitter1 = "";
	}
	if($imaro_client->twitter2 == "-"){
		$imaro_client->twitter2 = "";
	}
	if($imaro_client->twitter3 == "-"){
		$imaro_client->twitter3 = "";
	}
	if($imaro_client->phone_number == "-" || $imaro_client->phone_number == "0" ){
		$imaro_client->phone_number = "";
	}
	if($imaro_client->hs_contact_owner_key == "abcde12345" || $imaro_client->hs_contact_owner_key == "hgudk85496" ){
			if($imaro_client->hs_contact_owner_key == "abcde12345" ){
				$hubspot_owner_id = "32668037";     //richard
			}
			if($imaro_client->hs_contact_owner_key == "hgudk85496" ){
				$hubspot_owner_id = "32668118";     //Brad
			}
	}else{
		$hubspot_owner_id = "";   
	}
	
	$arr = array(
		'properties' => array(
			array(
				'property' => 'email',
				'value' => $imaro_client->email_address
			),
			array(
				'property' => 'firstname',
				'value' => $result->first_name
			),
			array(
				'property' => 'lastname',
				'value' => $result->last_name
			),
			array(
				'property' => 'country',
				'value' => $imaro_client->country
			),
			array(
				'property' => 'city',
				'value' => $imaro_client->city
			),
			array(
				'property' => 'jobtitle',
				'value' => $imaro_client->job_title
			),
			array(
				'property' => 'linkedin_search_id',
				'value' => $imaro_client->search_id
			),
			array(
				'property' => 'company_number',
				'value' => $imaro_client->company_number
			),
			array(
				'property' => 'li_profile_website_url',
				'value' => $imaro_client->website
			),
			array(
				'property' => 'custom_lifecycle_stage',
				'value' => 'Certificate Opened'                 
			),
			array(
				'property' => 'lifecyclestage',
				'value' => ''                 
			),
			array(
				'property' => 'company',
				'value' => $company_name
			),
			array(
				'property' => 'phone',
				'value' =>  $imaro_client->phone_number
			),
			array(
				'property' => 'date_of_birth',
				'value' => $imaro_client->date_birth
			),
			array(
				'property' => 'linkedin_profile_url',
				'value' =>$imaro_client->linkedin_profile_url
			),
			array( 
				'property' => 'twitter_url_1',
				'value' => $imaro_client->twitter1                 
			),
			array(
				'property' => 'twitter_url_2',
				'value' => $imaro_client->twitter2             
			),
			array(
				'property' => 'twitter_url_3',
				'value' => $imaro_client->twitter3               
			),
			array(
				'property' => 'certificate_url',
				'value' => $certificate_page_url               
			),
			array(
				'property' => 'certificate_delete_url',
				'value' => $certificate_delete_page_url               
			),	
			array(
				'property' => 'hubspot_owner_id',
				'value' => $hubspot_owner_id               
			)									
		)
	);
	if($imaro_client->create_hs_immediately != "1"){
		
		// echo "<pre>"; print_r($arr); die;
		$user_email_address = $imaro_client->email_address;

		$check_email = 'https://api.hubapi.com/contacts/v1/contact/email/' .$user_email_address .'/profile?hapikey=' . $hapikey;
		$ch_email = curl_init($check_email);
		curl_setopt($ch_email, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch_email, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch_email, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch_email);
		$status_code = @curl_getinfo($ch_email, CURLINFO_HTTP_CODE);
		$curl_errors = curl_error($ch_email);
		curl_close($ch_email);
		$res = json_decode($response);
		//echo "<pre>"; print_r($response); die;
		/* to check if any user deleted from CFD HS after complete process */
		//$check_migrated = $wpdb->get_row( $wpdb->prepare("SELECT * FROM cfwp_hubspot_migrate_contacts WHERE `email` = %s", $user_email_address));
		
		//if($res->message == "contact does not exist" && $imaro_client->hubspot_contact_id=="" && !isset($check_migrated)){
		if(isset($res->message) && $res->message == "contact does not exist" && $imaro_client->hubspot_contact_id==""){
				
				$response_data  = gea_create_hubspot_contact_using_api($arr);
				if(isset($response_data->vid)){      //to save the contactid for delete record
					 $contact_id  = $response_data->vid;
					 $api_url ='https://www.greenearthappeal.org/panacea/index.php/welcome/update_certificate_hubspot_contactid/'.$contact_id.'/'.$code;
					 $res22 = file_get_contents($api_url);	
					 $result222 = json_decode($res22);
				}	
				$company_arr = array(
					'properties' => array(
					array(
						'name' => 'name',
						'value' => $company_name
					),
					array(
						'name' => 'description',
						'value' => $company_name
					)
				));
				$companyId  = create_gea_hs_company($company_arr);
				$add_contact_status = gea_add_contact_to_company($companyId, $contact_id);	

				/* if($add_contact_status =='200'){

				}	 */			
		
		}elseif(isset($res->message) && $res->message != "contact does not exist" && $imaro_client->hubspot_contact_id==""){
			if(isset($res->vid)){      
					 $contact_id  = $res->vid;
					 $api_url ='https://www.greenearthappeal.org/panacea/index.php/welcome/update_certificate_hubspot_contactid/'.$contact_id.'/'.$code;
					 $res22 = file_get_contents($api_url);	
					 $result222 = json_decode($res22);
			
			}
			
		}	  
	}
	get_header('cert'); 
?>
	<div class="bs-example">
		<!--h3 class="wh_head" class="text-center">Green Earth Appeal Certificate</h3--> 
		<div class="pagewidth ">
				<?php 
					//echo "<pre>"; print_r($result); die;	
					$share_url= 'https://www.greenearthappeal.org/certificate/'.$code;
					
					$image_url= 'https://www.greenearthappeal.org/panacea/cert/full_'.$result->code.'.jpg';
					$cert_random_key = randomKeyGen('8');
						
				?>
					<h3 class="wh_sub_head" class="text-center"><?php echo ucwords(strtolower($person_name)); ?></h3> 
					<?php /* if($imaro_client->total_trees > 1){
						 $trees = $imaro_client->total_trees.' trees';      //for update trees acc. to company number
					}else{
						 $trees = $imaro_client->total_trees.' tree';
					} */ 
					
					if($imaro_client->tree_nums > 1){
						 $trees = $imaro_client->tree_nums.' trees';
					}else{
						 $trees = $imaro_client->tree_nums.' tree';
					} 
					?>
					<h2 style="text-align: center; font-size: 24px; font-weight: 400;">Has planted a total <?php echo $trees; ?> through Green Earth Appeal</h2>
					<div class="wh_row">
						<div class="video_data half_sEC">
							<div id="cp_widget_97af3f67-ac92-4f88-8361-67db641eab49">Loading....</div>
							<script type="text/javascript">
								var cpo = []; cpo["_object"] ="cp_widget_97af3f67-ac92-4f88-8361-67db641eab49"; cpo["_fid"] = "AICAle-zl0A-";

								var _cpmp = _cpmp || []; _cpmp.push(cpo);

								(function() { var cp = document.createElement("script"); cp.type = "text/javascript";

								cp.async = true; cp.src = "//www.cincopa.com/media-platform/runtime/libasync.js";

								var c = document.getElementsByTagName("script")[0];

								c.parentNode.insertBefore(cp, c); })();
							</script>
							<div class="social-float-parent dsktp">
								<div id="social-float">
										<div id="share-buttons"><p>Share your certificate</p>
											<!-- Facebook -->
											<a href="http://www.facebook.com/sharer.php?u=<?php echo $share_url; ?>" target="_blank">
													<img src="<?php echo get_stylesheet_directory_uri(); ?>/social-images/facebook.png?ver=001" alt="Facebook" />
												</a>
												
												<!-- LinkedIn -->
												<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $share_url; ?>" target="_blank">
													<img loading="lazy" src="<?php echo get_stylesheet_directory_uri(); ?>/social-images/linkedin.png?ver=001" alt="LinkedIn" data-lazy-src="<?php echo get_stylesheet_directory_uri(); ?>/social-images/linkedin.png?ver=001" class="lazyloaded" data-was-processed="true">
												</a>
												
												<!-- Twitter -->
												<a href="https://twitter.com/share?url=<?php echo $share_url; ?>&amp;text=We+just+planted+a+tree+with+%40co2freeding+%23carbonfreedining" target="_blank">
													<img loading="lazy" src="<?php echo get_stylesheet_directory_uri(); ?>/social-images/twitter.png?ver=001" alt="Twitter" data-lazy-src="<?php echo get_stylesheet_directory_uri(); ?>/social-images/twitter.png?ver=001" class="lazyloaded" data-was-processed="true">
												</a>
										</div>
								</div>
						 </div>
						</div>
						<div class="plant-registration half_sEC">
							<img src="https://www.greenearthappeal.org/panacea/cert/full_<?php echo $result->code.'.jpg?ver='.$cert_random_key; ?>" />
							<div class="social-float-parent mb">
								<div id="social-float">
									<div id="share-buttons"><p>Share your certificate</p>
										<!-- Facebook -->
										<a href="http://www.facebook.com/sharer.php?u=<?php echo $share_url; ?>" target="_blank">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/social-images/facebook.png?ver=001" alt="Facebook" />
										</a>
										<!-- LinkedIn -->
										<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $share_url; ?>" target="_blank">
											<img loading="lazy" src="<?php echo get_stylesheet_directory_uri(); ?>/social-images/linkedin.png?ver=001" alt="LinkedIn" data-lazy-src="<?php echo get_stylesheet_directory_uri(); ?>/social-images/linkedin.png?ver=001" class="lazyloaded" data-was-processed="true">
										</a>
										<!-- Twitter -->
										<a href="https://twitter.com/share?url=<?php echo $share_url; ?>&amp;text=We+just+planted+a+tree+with+%40co2freeding+%23carbonfreedining" target="_blank">
											<img loading="lazy" src="<?php echo get_stylesheet_directory_uri(); ?>/social-images/twitter.png?ver=001" alt="Twitter" data-lazy-src="<?php echo get_stylesheet_directory_uri(); ?>/social-images/twitter.png?ver=001" class="lazyloaded" data-was-processed="true">
										</a>
									</div>
								</div>
							</div>
							<div class="col-xs-12 text-center">
								<a class="btn btn_download btn-primary" download="certificate.jpg" href="https://www.greenearthappeal.org/download-cert-image.php?imgurl=<?php echo $result->code.'.jpg'; ?>" >Click to Download</a>
							</div>		
						</div>
					</div>	
			</div>
		</div>
		<?php
				}else{
					get_header(); 
					echo "<div style='text-align: center;padding: 24px 25px;' class='alert alert-warning'>
							  <strong>No Result Found!</strong>
							</div>";
				}

			}else{
				get_header(); 
				echo "<div style='text-align: center;padding: 24px 25px;' class='alert alert-warning'>
							<strong>No Result Found!</strong>
						</div>";
			} 
		}else{
				get_header(); 
				echo "<div style='text-align: center;padding: 24px 25px;' class='alert alert-warning'>
							<strong>No Result Found!</strong>
						</div>";
			} 
		?>

	<script>
		jQuery(document).scroll(function() {
			checkOffset();
		});
	</script>

<?php get_footer(); ?>
<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4641198.js"></script>
<!-- End of HubSpot Embed Code -->
</body>
</html>