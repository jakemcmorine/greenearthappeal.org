<?php

$base_url = get_template_directory_uri() . '/builder-layouts/';

return array(
	array(
		"title" => "Home",
		"data"  => "home.zip",
		"thumb" => $base_url . "home.jpg"
	),
	array(
		"title" => "Home 2",
		"data"  => "home-2.zip",
		"thumb" => $base_url . "home-2.jpg"
	),
	array(
		"title" => "Home 3",
		"data"  => "home-3.zip",
		"thumb" => $base_url . "home-3.jpg"
	),
	array(
		"title" => "Home 4",
		"data"  => "home-4.zip",
		"thumb" => $base_url . "home-4.jpg"
	),
);