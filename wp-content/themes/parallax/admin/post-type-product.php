<?php
/**
 * Product Meta Box Options
 * @param array $args
 * @return array
 * @since 1.0.0
 */
function themify_theme_product_meta_box() {
	$team_meta_box = array(
		// Content Width
		array(
			'name'=> 'content_width',
			'title' => __('Content Width', 'themify'),
			'description' => '',
			'type' => 'layout',
			'show_title' => true,
			'meta' => array(
				array(
					'value' => 'default_width',
					'img' => 'themify/img/default.png',
					'selected' => true,
					'title' => __( 'Default', 'themify' )
				),
				array(
					'value' => 'full_width',
					'img' => 'themify/img/fullwidth.png',
					'title' => __( 'Fullwidth', 'themify' )
				)
			),
			'default' => 'full_width'
		),
		// Featured Image Size
		array(
			'name'	=>	'feature_size',
			'title'	=>	__('Image Size', 'themify'),
			'description' => __('Image sizes can be set at <a href="options-media.php">Media Settings</a> and <a href="https://wordpress.org/plugins/regenerate-thumbnails/" target="_blank">Regenerated</a>', 'themify'),
			'type'		 =>	'featimgdropdown',
			'display_callback' => 'themify_is_image_script_disabled'
		),
		// Image Dimensions
		array(
			'type' => 'multi',
			'name' => 'image_dimensions',
			'title' => __('Image Dimension', 'themify'),
			'meta' => array(
				'fields' => array(
					// Image Width
					array(
						'name' => 'image_width',	
						'label' => __('width', 'themify'), 
						'description' => '',
						'type' => 'textbox',			
						'meta' => array('size'=>'small'),
						'before' => '',
						'after' => '',
					),
					// Image Height
					array(
						'name' => 'image_height',
						'label' => __('height', 'themify'),
						'type' => 'textbox',						
						'meta' => array('size'=>'small'),
						'before' => '',
						'after' => '',
					)
				),
				'description' => __('Enter height = 0 to disable vertical cropping with img.php enabled', 'themify'), 	
				'before' => '',
				'after' => '',
				'separator' => ''
			)
		),
			// Image Filter
			array(
					'name'        => 'imagefilter_options',
					'title'       => __( 'Image Filter', 'themify' ),
					'description' => '',
					'type'        => 'dropdown',
					'meta'        => array(
						array( 'name' => '', 'value' => 'initial' ),
						array( 'name' => __( 'None', 'themify' ), 'value' => 'none' ),
						array( 'name' => __( 'Grayscale', 'themify' ), 'value' => 'grayscale' ),
						array( 'name' => __( 'Sepia', 'themify' ), 'value' => 'sepia' ),
						array( 'name' => __( 'Blur', 'themify' ), 'value' => 'blur' ),
					),
					'default' => 'initial'
			),
			// Image Hover Filter
			array(
				'name'        => 'imagefilter_options_hover',
				'title'       => __( 'Image Hover Filter', 'themify' ),
				'description' => '',
				'type'        => 'dropdown',
				'meta'        => array(
					array( 'name' => '', 'value' => 'initial' ),
					array( 'name' => __( 'None', 'themify' ), 'value' => 'none' ),
					array( 'name' => __( 'Grayscale', 'themify' ), 'value' => 'grayscale' ),
					array( 'name' => __( 'Sepia', 'themify' ), 'value' => 'sepia' ),
					array( 'name' => __( 'Blur', 'themify' ), 'value' => 'blur' ),
				),
				'default' => 'initial'
			),
			// Image Filter Apply To
			array(
				'name'        => 'imagefilter_applyto',
				'title'       => __( 'Apply Filter To', 'themify' ),
				'description' => sprintf( __( 'Theme Default = can be set in <a href="%s" target="_blank">Themify > Settings > Theme Settings</a>', 'themify' ), admin_url( 'admin.php?page=themify#setting-theme_settings' ) ),
				'type'        => 'radio',
				'meta'        => array(
					array( 'value' => 'initial', 'name' => __( 'Theme Default', 'themify' ), 'selected' => true ),
					array( 'value' => 'featured-only', 'name' => __( 'Featured Images Only', 'themify' ), ),
					array( 'value' => 'all', 'name' => __( 'All Images', 'themify' ) )
				),
				'default' => 'initial'
			),
		// Team Title
		array(
			'name' 		=> 'team_title',	
			'title' 	=> __('Team Member Position', 'themify'), 	
			'description' => '',
			'type' 		=> 'textbox',			
			'meta'		=> array()			
		),
		// Skills
		array(
			'name' 		=> 'skills',
			'title' 	=> __('Skill Set', 'themify'),
			'description' => '',
			'type' 		=> 'textarea'
		),
		// Social links
		array(
			'name' 		=> 'social',
			'title' 	=> __('Social Links', 'themify'),
			'description' => '',
			'type' 		=> 'textarea'
		),
		// Shortcode ID
		array(
			'name' 		=> 'post_id_info',	
			'title' 	=> __('Shortcode ID', 'themify'),
			'description' => __('To show this use [team id="%s"]', 'themify'),
			'type' 		=> 'post_id_info'
		),
		// External Link
		array(
			'name' 		=> 'external_link',	
			'title' 		=> __('External Link', 'themify'), 	
			'description' => __('Link Featured Image and Post Title to external URL', 'themify'), 				
			'type' 		=> 'textbox',			
			'meta'		=> array()
		),
		// Lightbox Link
		themify_lightbox_link_field(),
	);

	return $team_meta_box;
}