/* Themify Custom Panel UI enhancements */
(function($){

	'use strict';
	
	$(document).ready(function(){
		/**
		 * The Transparent Header style should not be available when using Header Left Pane, Right Pane, or Min Bar layouts
		 */
		$( 'input[name="header_design"]' ).on( 'change', function() {
			var val = $( this ).val();
			if( val == 'header-minbar' || val == 'header-leftpane' || val == 'header-rightpane' ) {
				$( '#background_type-transparent, label[for="background_type-transparent"]' ).hide();
			} else {
				$( '#background_type-transparent, label[for="background_type-transparent"]' ).show();
			}
		} ).trigger( 'change' );
	});

})(jQuery);