<?php
/**
 * Section Meta Box Options
 * @param array $args
 * @return array
 * @since 1.0.0
 */
function themify_theme_section_meta_box() {
	$section_meta_box = array(
		// Section Width
		array(
			'name' 		=> 'section_width',
			'title' 	=> __('Section Width', 'themify'),
			'description' => '',
			'type' 		=> 'layout',
			'show_title' => true,
			'meta'		=> array(
				array('value' => 'default', 'img' => 'images/layout-icons/default.png', 'selected' => true, 'title' => __('Default', 'themify')),
				array('value' => 'fullwidth', 'img' => 'images/layout-icons/slider-image-only.png', 'title' => __('Fullwidth', 'themify'))
			),
			'default' => 'default'
		),
		// Section Type
		array(
			'name'		=> 'section_type',
			'title'		=> __('Section Type', 'themify'),
			'description' => '',
			'type'		=> 'layout',
			'show_title' => true,
			'meta'		=> array(
				array('value' => 'default', 'img' => 'images/layout-icons/default.png', 'selected' => true, 'title' => __('Default', 'themify')),
				array('value' => 'message', 'img' => 'images/layout-icons/type-text.png', 'selected' => true, 'title' => __('Message', 'themify')),
				array('value' => 'video', 'img' => 'images/layout-icons/type-video.png', 'title' => __('Video', 'themify')),
				array('value' => 'slider', 'img' => 'images/layout-icons/type-slider.png', 'title' => __('Slider', 'themify'))
			),
			'enable_toggle' => true,
			'default' => 'default'
		),
		// Video URL
		array(
			'name' 		=> 'video_url',
			'title' 		=> __('Video URL', 'themify'),
			'description' => __('Video embed URL such as YouTube or Vimeo video url (<a href="https://themify.me/docs/video-embeds">details</a>).', 'themify'),
			'type' 		=> 'textbox',
			'meta'		=> array(),
			'toggle'	=> 'video-toggle',
		),
		// Slider Gallery
		array(
			'name' 		=> 'slider_gallery',
			'title' 	=> __('Slider Gallery', 'themify'),
			'description' => '',
			'type' 		=> 'gallery_shortcode',
			'toggle'	=> 'slider-toggle'
		),
			// Image Filter
			array(
				'name'        => 'imagefilter_options',
				'title'       => __( 'Image Filter', 'themify' ),
				'description' => '',
				'type'        => 'dropdown',
				'meta'        => array(
					array( 'name' => '', 'value' => 'initial' ),
					array( 'name' => __( 'None', 'themify' ), 'value' => 'none' ),
					array( 'name' => __( 'Grayscale', 'themify' ), 'value' => 'grayscale' ),
					array( 'name' => __( 'Sepia', 'themify' ), 'value' => 'sepia' ),
					array( 'name' => __( 'Blur', 'themify' ), 'value' => 'blur' ),
				),
				'default' => 'initial'
			),
			// Image Hover Filter
			array(
				'name'        => 'imagefilter_options_hover',
				'title'       => __( 'Image Hover Filter', 'themify' ),
				'description' => '',
				'type'        => 'dropdown',
				'meta'        => array(
					array( 'name' => '', 'value' => 'initial' ),
					array( 'name' => __( 'None', 'themify' ), 'value' => 'none' ),
					array( 'name' => __( 'Grayscale', 'themify' ), 'value' => 'grayscale' ),
					array( 'name' => __( 'Sepia', 'themify' ), 'value' => 'sepia' ),
					array( 'name' => __( 'Blur', 'themify' ), 'value' => 'blur' ),
				),
				'default' => ''
			),
			// Image Filter Apply To
			array(
				'name'        => 'imagefilter_applyto',
				'title'       => __( 'Apply Filter To', 'themify' ),
				'description' => sprintf( __( 'Theme Default = can be set in <a href="%s" target="_blank">Themify > Settings > Theme Settings</a>', 'themify' ), admin_url( 'admin.php?page=themify#setting-theme_settings' ) ),
				'type'        => 'radio',
				'meta'        => array(
					array( 'value' => 'initial', 'name' => __( 'Theme Default', 'themify' ), 'selected' => true ),
					array( 'value' => 'featured-only', 'name' => __( 'Featured Images Only', 'themify' ), ),
					array( 'value' => 'all', 'name' => __( 'All Images', 'themify' ) )
				),
				'default' => 'initial'
			),
		// Hide Section Title
		array(
			'name' 		=> 'hide_section_title',
			'title' 		=> __('Hide Section Title', 'themify'),
			'description' => '',
			'type' 		=> 'dropdown',
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Hide Section Subtitle
		array(
			'name' 		=> 'hide_section_subtitle',
			'title' 		=> __('Hide Section Subtitle', 'themify'),
			'description' => '',
			'type' 		=> 'dropdown',
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'yes', 'name' => __('Yes', 'themify')),
				array('value' => 'no',	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Separator
		array(
			'name' => 'separator_font',
			'title' => '', 
			'description' => '',
			'type' => 'separator',
			'meta' => array('html'=>'<h4>'.__('Section Font').'</h4><hr class="meta_fields_separator"/>'),
		),
		// Multi field: Font
		array(
			'type' => 'multi',
			'name' => 'multi_font',
			'title' => __('Font', 'themify'),
			'meta' => array(
				'fields' => array(
					// Font size
					array(
						'name' => 'font_size',	
						'label' => '',
						'description' => '',
						'type' => 'textbox',			
						'meta' => array('size'=>'small'),
						'before' => '',
						'after' => ''
					),
					// Font size unit
					array(
						'name' 	=> 'font_size_unit',	
						'label' => '',
						'type' 	=> 'dropdown',	
						'meta'	=> array(
							array('value' => 'px', 'name' => __('px', 'themify'), 'selected' => true),
							array('value' => 'em', 'name' => __('em', 'themify'))
						),
						'before' => '',
						'after' => '',
						'default' => 'px'
					),
					// Font family
					array(
						'name' 	=> 'font_family',	
						'label' => '',
						'type' 	=> 'dropdown',	
						'meta'	=> array_merge( themify_get_web_safe_font_list(), themify_get_google_web_fonts_list() ),
						'before' => '',
						'after' => '',
					),
				),
				'description' => '',	
				'before' => '',
				'after' => '',
				'separator' => ''
			)
		),
		// Font Color
		array(
			'name' => 'font_color',
			'title' => __('Font Color', 'themify'), 
			'description' => '',
			'type' => 'color',
			'meta' => array('default'=>null),
		),
		// Link Color
		array(
			'name' => 'link_color',
			'title' => __('Link Color', 'themify'), 
			'description' => '',
			'type' => 'color',
			'meta' => array('default'=>null),
		),
		// Separator
		array(
			'name' => 'separator',
			'title' => '', 
			'description' => '',
			'type' => 'separator',
			'meta' => array('html'=>'<h4>'.__('Section Background').'</h4><hr class="meta_fields_separator"/>'),
		),
		// Background Color
		array(
			'name' => 'background_color',
			'title' => __('Background Color', 'themify'), 
			'description' => '',
			'type' => 'color',
			'meta' => array('default'=>null),
		),
		// Backgroud image
		array(
			'name' 	=> 'background_image',	
			'title' => '',
			'type' 	=> 'image',
			'description' => '',	
			'meta'	=> array(),
			'before' => '',
			'after' => ''
		),
		// Background repeat
		array(
			'name' 		=> 'background_repeat',
			'title'		=> __('Background Repeat', 'themify'),
			'description'	=> '',
			'type' 		=> 'dropdown',			
			'meta'		=> array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => 'repeat', 'name' => __('Repeat', 'themify')),
				array('value' => 'fullcover', 'name' => __('Fullcover', 'themify'))
			),
			'default' => 'default'
		),
		// Header wrap text color
		array(
			'name'        => 'headerwrap_text_color',
			'title'       => __( 'Header Text Color', 'themify' ),
			'description' => '',
			'type'        => 'color',
			'meta'        => array( 'default' => null ),
			'class'     => 'hide-if none',
		),
		// Header wrap link color
		array(
			'name'        => 'headerwrap_link_color',
			'title'       => __( 'Header Link Color', 'themify' ),
			'description' => '',
			'type'        => 'color',
			'meta'        => array( 'default' => null ),
			'class'     => 'hide-if none',
		),
	);

	/** Portfolio Meta Box Options */
	$portfolio_meta_box = array(
		// Content Width
		array(
			'name'=> 'content_width',
			'title' => __('Content Width', 'themify'),
			'description' => '',
			'type' => 'layout',
			'show_title' => true,
			'meta' => array(
				array(
					'value' => 'default_width',
					'img' => 'themify/img/default.png',
					'selected' => true,
					'title' => __( 'Default', 'themify' )
				),
				array(
					'value' => 'full_width',
					'img' => 'themify/img/fullwidth.png',
					'title' => __( 'Fullwidth', 'themify' )
				)
			),
			'default' => 'default_width'
		),
		// Gallery Shortcode
		array(
			'name' 		=> 'gallery_shortcode',	
			'title' 	=> __('Image Slider', 'themify'),
			'description' => __( 'This gallery will show as an image slider instead of the featured image.', 'themify' ),			
			'type' 		=> 'gallery_shortcode'
		),
		// Featured Image Size
		array(
			'name'	=>	'feature_size',
			'title'	=>	__('Image Size', 'themify'),
			'description' => __('Image sizes can be set at <a href="options-media.php">Media Settings</a> and <a href="https://wordpress.org/plugins/regenerate-thumbnails/" target="_blank">Regenerated</a>', 'themify'),
			'type'		 =>	'featimgdropdown',
			'display_callback' => 'themify_is_image_script_disabled'
		),
		// Multi field: Image Dimension
		array(
			'type' => 'multi',
			'name' => 'image_dimensions',
			'title' => __('Image Dimension', 'themify'),
			'meta' => array(
				'fields' => array(
					// Image Width
					array(
						'name' => 'image_width',	
						'label' => __('width', 'themify'), 
						'description' => '',
						'type' => 'textbox',			
						'meta' => array('size'=>'small'),
						'before' => '',
						'after' => '',
					),
					// Image Height
					array(
						'name' => 'image_height',
						'label' => __('height', 'themify'),
						'type' => 'textbox',						
						'meta' => array('size'=>'small'),
						'before' => '',
						'after' => '',
					)
				),
				'description' => __('Enter height = 0 to disable vertical cropping with img.php enabled', 'themify'), 	
				'before' => '',
				'after' => '',
				'separator' => ''
			),
			'toggle'	=> array('media-image-toggle')
		),
			// Image Filter
			array(
				'name'        => 'imagefilter_options',
				'title'       => __( 'Image Filter', 'themify' ),
				'description' => '',
				'type'        => 'dropdown',
				'meta'        => array(
					array( 'name' => '', 'value' => 'initial' ),
					array( 'name' => __( 'None', 'themify' ), 'value' => 'none' ),
					array( 'name' => __( 'Grayscale', 'themify' ), 'value' => 'grayscale' ),
					array( 'name' => __( 'Sepia', 'themify' ), 'value' => 'sepia' ),
					array( 'name' => __( 'Blur', 'themify' ), 'value' => 'blur' ),
				),
				'default' => 'initial'
			),
			// Image Hover Filter
			array(
				'name'        => 'imagefilter_options_hover',
				'title'       => __( 'Image Hover Filter', 'themify' ),
				'description' => '',
				'type'        => 'dropdown',
				'meta'        => array(
					array( 'name' => '', 'value' => 'initial' ),
					array( 'name' => __( 'None', 'themify' ), 'value' => 'none' ),
					array( 'name' => __( 'Grayscale', 'themify' ), 'value' => 'grayscale' ),
					array( 'name' => __( 'Sepia', 'themify' ), 'value' => 'sepia' ),
					array( 'name' => __( 'Blur', 'themify' ), 'value' => 'blur' ),
				),
				'default' => 'initial'
			),
			// Image Filter Apply To
			array(
				'name'        => 'imagefilter_applyto',
				'title'       => __( 'Apply Filter To', 'themify' ),
				'description' => sprintf( __( 'Theme Default = can be set in <a href="%s" target="_blank">Themify > Settings > Theme Settings</a>', 'themify' ), admin_url( 'admin.php?page=themify#setting-theme_settings' ) ),
				'type'        => 'radio',
				'meta'        => array(
					array( 'value' => 'initial', 'name' => __( 'Theme Default', 'themify' ), 'selected' => true ),
					array( 'value' => 'featured-only', 'name' => __( 'Featured Images Only', 'themify' ), ),
					array( 'value' => 'all', 'name' => __( 'All Images', 'themify' ) )
				),
				'default' => 'initial'
			),
		// Hide Title
		array(
			"name" 		=> "hide_post_title",
			"title"		=> __('Hide Post Title', 'themify'),
			"description"	=> "",
			"type" 		=> "dropdown",			
			"meta"		=> array(
				array("value" => "default", "name" => "", "selected" => true),
				array("value" => "yes", 'name' => __('Yes', 'themify')),
				array("value" => "no",	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Unlink Post Title
		array(
			"name" 		=> "unlink_post_title",
			"title" 		=> __('Unlink Post Title', 'themify'), 	
			"description" => __('Unlink post title (it will display the post title without link)', 'themify'), 				
			"type" 		=> "dropdown",			
			"meta"		=> array(
				array("value" => "default", "name" => "", "selected" => true),
				array("value" => "yes", 'name' => __('Yes', 'themify')),
				array("value" => "no",	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Hide Post Date
		array(
			"name" 		=> "hide_post_date",
			"title"		=> __('Hide Post Date', 'themify'),
			"description"	=> "",
			"type" 		=> "dropdown",			
			"meta"		=> array(
				array("value" => "default", "name" => "", "selected" => true),
				array("value" => "yes", 'name' => __('Yes', 'themify')),
				array("value" => "no",	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Hide Post Meta
		array(
			"name" 		=> "hide_post_meta",
			"title"		=> __('Hide Post Meta', 'themify'),
			"description"	=> "",
			"type" 		=> "dropdown",			
			"meta"		=> array(
				array("value" => "default", "name" => "", "selected" => true),
				array("value" => "yes", 'name' => __('Yes', 'themify')),
				array("value" => "no",	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Hide Post Image
		array(
			"name" 		=> "hide_post_image",
			"title" 		=> __('Hide Featured Image', 'themify'), 	
			"description" => "", 				
			"type" 		=> "dropdown",			
			"meta"		=> array(
				array("value" => "default", "name" => "", "selected" => true),
				array("value" => "yes", 'name' => __('Yes', 'themify')),
				array("value" => "no",	'name' => __('No', 'themify'))
			),
			'default' => 'default'		
		),
		// Unlink Post Image
		array(
			"name" 		=> "unlink_post_image",
			"title" 		=> __('Unlink Featured Image', 'themify'), 	
			"description" => __('Display the Featured Image without link', 'themify'), 				
			"type" 		=> "dropdown",			
			"meta"		=> array(
				array("value" => "default", "name" => "", "selected" => true),
				array("value" => "yes", 'name' => __('Yes', 'themify')),
				array("value" => "no",	'name' => __('No', 'themify'))
			),
			'default' => 'default'
		),
		// Video URL
		array(
			'name' 		=> 'video_url',
			'title' 		=> __('Video URL', 'themify'),
			'description' => __('Video embed URL such as YouTube or Vimeo video url (<a href="https://themify.me/docs/video-embeds">details</a>)', 'themify'),
			'type' 		=> 'textbox',
			'meta'		=> array(),
		),
		// External Link
		array(
			'name' 		=> 'external_link',	
			'title' 		=> __('External Link', 'themify'), 	
			'description' => __('Link Featured Image and Post Title to external URL', 'themify'), 				
			'type' 		=> 'textbox',			
			'meta'		=> array()
		),
		// Lightbox Link
		themify_lightbox_link_field(),
		array(
			'name' 	=> '_multi_layout',
			'type' => 'multi',
			'title' => __('Layout', 'themify'),
			'meta' => array(
				'fields' => array(
					// Image Width
					array(
					  'name' 		=> 'hide_header',
					  'label' => __('Exclude Header', 'themify'),
					  'description' => '',
					  'type' 		=> 'checkbox',
					  'meta'		=> array('size'=>'small'),
					  'before' => '<div>',
					  'after' => '</div>',
					),
					// Image Height
					array(
					  'name' 		=> 'hide_footer',
					  'label' => __('Exclude Footer', 'themify'),
					  'description' => '',
					  'type' 		=> 'checkbox',
					  'meta'		=> array('size'=>'small'),
					  'before' => '<div>',
					  'after' => '</div>',
					),
				),
				'description' => '',
				'before' => '',
				'after' => '',
				'separator' => ''
			)
		),
		// Shortcode ID
		array(
			'name' 		=> '_post_id_info',	
			'title' 	=> __('Shortcode ID', 'themify'),
			'description' => __('To show this use [portfolio id="%s"]', 'themify'),
			'type' 		=> 'post_id_info'
		)
	);

	return $section_meta_box;
}