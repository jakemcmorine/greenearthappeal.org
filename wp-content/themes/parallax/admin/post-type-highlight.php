<?php
/**
 * Highlight Meta Box Options
 * @param array $args
 * @return array
 * @since 1.0.0
 */
function themify_theme_highlight_meta_box() {
	$highlight_meta_box = array(
		// Icon/Image Select
		array(
			'name'		=> 'highlight_type',
			'title'		=> __('Highlight Type', 'themify'),
			'description' => '',
			'type'		=> 'radio',
			'meta'		=> array(
				array('value' => 'icon', 'selected' => true, 'name' => __('Icon', 'themify')),
				array('value' => 'image', 'name' => __('Image', 'themify')),
			),
			'enable_toggle'	=> true
		),
		// Featured Image Size
		array(
			'name'	=>	'feature_size',
			'title'	=>	__('Image Size', 'themify'),
			'description' => __('Image sizes can be set at <a href="options-media.php">Media Settings</a> and <a href="https://wordpress.org/plugins/regenerate-thumbnails/" target="_blank">Regenerated</a>', 'themify'),
			'type'		 =>	'featimgdropdown',
			'toggle'	=> 'image-toggle',
			'display_callback' => 'themify_is_image_script_disabled'
		),
		// Image Dimensions
		array(
			'type' => 'multi',
			'name' => 'image_dimensions',
			'title' => __('Image Dimension', 'themify'),
			'meta' => array(
				'fields' => array(
					// Image Width
					array(
						'name' => 'image_width',	
						'label' => __('width', 'themify'), 
						'description' => '',
						'type' => 'textbox',			
						'meta' => array('size'=>'small'),
						'before' => '',
						'after' => '',
					),
					// Image Height
					array(
						'name' => 'image_height',
						'label' => __('height', 'themify'),
						'type' => 'textbox',						
						'meta' => array('size'=>'small'),
						'before' => '',
						'after' => '',
					)
				),
				'description' => __('Enter height = 0 to disable vertical cropping with img.php enabled', 'themify'), 	
				'before' => '',
				'after' => '',
				'separator' => ''
			),
			'toggle'	=> 'image-toggle'
		),
			// Image Filter
			array(
					'name'        => 'imagefilter_options',
					'title'       => __( 'Image Filter', 'themify' ),
					'description' => '',
					'type'        => 'dropdown',
					'meta'        => array(
						array( 'name' => '', 'value' => 'initial' ),
						array( 'name' => __( 'None', 'themify' ), 'value' => 'none' ),
						array( 'name' => __( 'Grayscale', 'themify' ), 'value' => 'grayscale' ),
						array( 'name' => __( 'Sepia', 'themify' ), 'value' => 'sepia' ),
						array( 'name' => __( 'Blur', 'themify' ), 'value' => 'blur' ),
					),
					'default' => 'initial'
			),
			// Image Hover Filter
			array(
					'name'        => 'imagefilter_options_hover',
					'title'       => __( 'Image Hover Filter', 'themify' ),
					'description' => '',
					'type'        => 'dropdown',
					'meta'        => array(
						array( 'name' => '', 'value' => 'initial' ),
						array( 'name' => __( 'None', 'themify' ), 'value' => 'none' ),
						array( 'name' => __( 'Grayscale', 'themify' ), 'value' => 'grayscale' ),
						array( 'name' => __( 'Sepia', 'themify' ), 'value' => 'sepia' ),
						array( 'name' => __( 'Blur', 'themify' ), 'value' => 'blur' ),
					),
					'default' => 'initial'
			),
			// Image Filter Apply To
			array(
					'name'        => 'imagefilter_applyto',
					'title'       => __( 'Apply Filter To', 'themify' ),
					'description' => sprintf( __( 'Theme Default = can be set in <a href="%s" target="_blank">Themify > Settings > Theme Settings</a>', 'themify' ), admin_url( 'admin.php?page=themify#setting-theme_settings' ) ),
					'type'        => 'radio',
					'meta'        => array(
						array( 'value' => 'initial', 'name' => __( 'Theme Default', 'themify' ), 'selected' => true ),
						array( 'value' => 'featured-only', 'name' => __( 'Featured Images Only', 'themify' ), ),
						array( 'value' => 'all', 'name' => __( 'All Images', 'themify' ) )
					),
					'default' => 'initial'
			),
		// Icon
		array(
			'name' 		=> 'icon',
			'title' 		=> __('Icon', 'themify'),
			'type' 		=> 'fontawesome',
			'meta'		=> array(),
			'toggle'	=> 'icon-toggle'
		),
		// Icon Color
		array(
			'name'		=> 'icon_color',
			'title'		=> __('Icon Color', 'themify'),
			'description' => __('Defaults to Bar Color if blank', 'themify'),
			'type'		=> 'color',
			'meta'		=> array('default' => null),
			'toggle'	=> 'icon-toggle'
		),
		// Icon Background Color
		array(
			'name'		=> 'icon_background',
			'title'		=> __('Icon Background Color', 'themify'),
			'description' => __('Defaults to transparent if blank', 'themify'),
			'type'		=> 'color',
			'meta'		=> array('default'=>null),
			'toggle'	=> 'icon-toggle'
		),
		// Bar Percentage
		array(
			'name' 		=> 'bar_percentage',
			'title' 	=> __('Bar Percentage', 'themify'),
			'description' => __('Enter a value from 0 to 100', 'themify'),
			'type' 		=> 'textbox',
			'meta'		=> array()
		),
		// Bar Color
		array(
			'name' => 'bar_color',
			'title' => __('Bar Color', 'themify'),
			'description' => sprintf( __('You can set up the default color at <a href="%s">Styling &gt; Backgrounds</a>.', 'themify'), admin_url('admin.php?page=themify#styling') ),
			'type' => 'color',
			'meta' => array('default' => null)
		),
		// External Link
		array(
			'name' 		=> 'external_link',	
			'title' 		=> __('External Link', 'themify'), 	
			'description' => __('Link Featured Image and Post Title to external URL', 'themify'), 				
			'type' 		=> 'textbox',			
			'meta'		=> array()
		),
		// Lightbox Link
		themify_lightbox_link_field(),
		// Shortcode ID
		array(
			'name' 		=> 'post_id_info',	
			'title' 	=> __('Shortcode ID', 'themify'),
			'description' => __('To show this use [highlight id="%s"]', 'themify'),
			'type' 		=> 'post_id_info'
		)
	);

	return $highlight_meta_box;
}