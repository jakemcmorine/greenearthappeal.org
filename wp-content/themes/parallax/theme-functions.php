<?php
/*
 * To add custom PHP functions to the theme, create a child theme (https://themify.me/docs/child-theme) and add it to the child theme functions.php file. 
 * They will be added to the theme automatically.
 */

/* prevent update checks from wp.org repository */
add_theme_support( 'themify-exclude-theme-from-wp-update' );

/////// Actions ////////
add_action( 'wp_head', 'themify_viewport_tag' );
add_action( 'wp_enqueue_scripts', 'themify_theme_enqueue_scripts', 11 );

// Register Custom Menu Function - Action
add_action( 'init', 'themify_register_custom_nav');

/////// Filters ////////
add_filter('themify_default_post_layout_condition', 'themify_theme_default_post_layout_condition', 12);
add_filter('themify_default_post_layout', 'themify_theme_default_post_layout', 12);
add_filter('themify_default_layout_condition', 'themify_theme_default_layout_condition', 12);
add_filter('themify_default_layout', 'themify_theme_default_layout', 12);

// Set class on body tag according to layout width
add_filter( 'body_class', 'themify_theme_body_class', 99 );

function themify_theme_default_post_layout_condition($condition) {
	return $condition || is_tax('portfolio-category');
}

function themify_theme_default_post_layout() {
	global $themify;
	// get default layout
	$class = $themify->post_layout;
	if('portfolio' === $themify->query_post_type) {
		$class = themify_check('portfolio_layout') ? themify_get('portfolio_layout') : themify_get('setting-default_post_layout');
	} elseif ( is_tax('portfolio-category') || is_post_type_archive( 'portfolio' ) ) {
		$class = themify_check('setting-default_portfolio_index_post_layout')? themify_get('setting-default_portfolio_index_post_layout') : 'list-post';
	}
	return $class;
}

/**
 * Changes condition to filter layout class
 * @param bool $condition
 * @return bool
 */
function themify_theme_default_layout_condition($condition) {
	global $themify;
	// if layout is not set or is the home page and front page displays is set to latest posts 
	return $condition || (is_home() && 'posts' === get_option('show_on_front')) || '' != $themify->query_category || is_tax('portfolio-category') || is_singular('portfolio');
}
/**
 * Returns modified layout class
 * @param string $class Original body class
 * @return string
 */
function themify_theme_default_layout($class) {
	// get default layout
	if (is_tax('portfolio-category')) {
		$class = themify_check('setting-default_portfolio_index_layout')? themify_get('setting-default_portfolio_index_layout') : 'sidebar-none';
	}
	else{
	    global $themify;
	    $class = $themify->layout;
	}
	return $class;
}

/**
 * Enqueue Stylesheets and Scripts
 */
function themify_theme_enqueue_scripts(){

	// Get theme version for Themify theme scripts and styles
	$theme_version = wp_get_theme()->display('Version');
	$header = themify_theme_get_header_design();

	///////////////////
	//Enqueue styles
	///////////////////
	
	// Themify base styling
	themify_enque_style( 'theme-style', THEME_URI . '/style.css' ,null,$theme_version );

	// Themify Media Queries CSS
	themify_enque_style( 'themify-media-queries', THEME_URI . '/media-queries.css',null,$theme_version);

	// Themify Header Layout CSS
	if( $header != '' && $header !== 'default' && $header !== 'header-none'  && $header !== 'header-block'  ) {
		themify_enque_style( 'parallax-header-layouts', THEME_URI . '/styles/' . $header . '.css', array( 'theme-style' ),$theme_version );
	}
	
	// Themify child base styling
	if( is_child_theme() ) {
		themify_enque_style( 'theme-style-child', get_stylesheet_uri() , null, $theme_version );
	}

	///////////////////
	//Enqueue scripts
	///////////////////

	//wp_enqueue_script( 'themify-infinitescroll', THEME_URI.'/js/jquery.infinitescroll.min.js', array('imagesloaded'), '2.0b2', true );
	wp_enqueue_script( 'themify-backstretch', themify_enque(THEMIFY_URI.'/js/backstretch.themify-version.js'), null, THEMIFY_VERSION, true );

	// Check scrolling and transition effect
	$scrollingEffect = true;
	$scrollingEffectType = themify_get('section_parallax_effect') != '' ? 
		themify_get('section_parallax_effect') : 'effect2';

	if( ( themify_get('setting-scrolling_effect_mobile_exclude') !== 'on' && themify_is_touch() ) 
		|| themify_check('setting-scrolling_effect_all_disabled') ) {
		$scrollingEffect = false;
	}

	// Themify Auto Iframe Height
	wp_enqueue_script( 'theme-auto-iframe-height',	THEME_URI . '/js/jquery.iframe-auto-height.min.js', null, '2.0.0', true );

	// Slide mobile navigation menu
	wp_enqueue_script( 'slide-nav', themify_enque(THEMIFY_URI . '/js/themify.sidemenu.js'), null, THEMIFY_VERSION, true );

	// Themify internal scripts
	wp_enqueue_script( 'theme-script',	themify_enque(THEME_URI . '/js/themify.script.js'), array('themify-backstretch'), $theme_version, true );

	//Themify Gallery
	wp_enqueue_script( 'themify-gallery', themify_enque(THEMIFY_URI . '/js/themify.gallery.js'), null, THEMIFY_VERSION, true );

	//Inject variable values in gallery script
	wp_localize_script( 'theme-script', 'themifyScript', array(
		'themeURI' => THEME_URI,
		'lightbox' => themify_lightbox_vars_init(),
		'fixedHeader' => themify_theme_fixed_header(),
		'sticky_header'=>themify_theme_sticky_logo(),
		'autoInfinite' 	=> ! themify_get( 'setting-autoinfinite' )? 'auto':'',
		'ajaxurl'	=> admin_url( 'admin-ajax.php' ),
		'load_nonce' => wp_create_nonce( 'theme_load_nonce' ),
		'scrollingEffect' => $scrollingEffect,
		'scrollingEffectType' => $scrollingEffectType, // Overlapping section effect | background scrolling
		'scrollingEasing' => (!themify_get('setting-scrolling_effect_easing'))? 'linear' : themify_get('setting-scrolling_effect_easing'),
		'chart' => apply_filters('themify_chart_init_vars', array(
			'trackColor' => 'rgba(0,0,0,.1)',
			'scaleColor' => false,
			'lineCap' => 'butt',
			'rotate' => 0,
			'size' => 175,
			'lineWidth' => 3,
			'animate' => 2000
		)),
		'headerScroll' => apply_filters( 'themify_parallax_header_scroll', array(
			'adjust' => 0,
			'ratio' => -0.3
		))
	));
	
	// Collect page variables or use defaults
	$page_id = get_queried_object_id();
	if( $page_id != null || themify_is_shop() ) {
		$slider_play = themify_get( 'background_auto', themify_check('setting-footer_slider_auto') ? themify_get('setting-footer_slider_auto') : 'yes' );
		$slider_autoplay = themify_get( 'background_autotimeout', themify_check('setting-footer_slider_autotimeout') ? themify_get('setting-footer_slider_autotimeout') : 5 );
		$slider_speed = themify_get( 'background_speed', themify_check('setting-footer_slider_speed') ? themify_get('setting-footer_slider_speed') : 500 );
		$slider_wrap = themify_get( 'background_wrap', 'yes' );
		$slider_background_mode = themify_get( 'background_mode', 'cover' );
		$slider_background_position = themify_get( 'background_position', 'center-center' );
	} else {
		$slider_play = 'yes';
		$slider_autoplay = 5;
		$slider_speed = 500;
		$slider_wrap = 'yes';
		$slider_background_mode = 'cover';
		$slider_background_position = 'center-center';
	}

	// Header gallery
	wp_enqueue_script( 'themify-header-slider', themify_enque(THEME_URI . '/js/themify.header-slider.js'), null, $theme_version, true );

	//Inject variable values in gallery script
	wp_localize_script( 'themify-header-slider', 'themifyVars', array(
			'play'					=> $slider_play,
			'autoplay'				=> $slider_autoplay,
			'speed'					=> $slider_speed,
			'wrap'					=> $slider_wrap,
			'backgroundMode'		=> $slider_background_mode,
			'backgroundPosition'	=> $slider_background_position
		)
	);
	
	// Section slider
	wp_enqueue_script( 'slider-section', themify_enque(THEME_URI . '/js/themify.slidersection.js'), null, $theme_version, true );
	
	//Inject variable values in section slider script
	wp_localize_script( 'slider-section', 'themifySectionVars', array(
			'play'		=> $slider_play,
			'autoplay'	=> $slider_autoplay,
			'speed'		=> $slider_speed,
			'wrap'		=> $slider_wrap
		)
	);
}

/**
 * Load Google fonts used by the theme
 *
 * @return array
 */
function themify_theme_google_fonts( $fonts ) {
	/* translators: If there are characters in your language that are not supported by Sanchez, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Sanchez font: on or off', 'themify' ) ) {
		$fonts['sanchez'] = 'Sanchez:400,400italic';
	}
	/* translators: If there are characters in your language that are not supported by Poppins, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Poppins font: on or off', 'themify' ) ) {
		$fonts['poppins'] = 'Poppins:400,300,500,600,700';
	}
	/* translators: If there are characters in your language that are not supported by Muli, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Muli font: on or off', 'themify' ) ) {
		$fonts['muli'] = 'Muli:400,400italic,300,300italic';
	}
	/* translators: If there are characters in your language that are not supported by Crete Round, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Crete Round font: on or off', 'themify' ) ) {
		$fonts['crete-round'] = 'Crete+Round';
	}
	/* translators: If there are characters in your language that are not supported by Vidaloka, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Vidaloka font: on or off', 'themify' ) ) {
		$fonts['vidaloka'] = 'Vidaloka';
	}
	/* translators: If there are characters in your language that are not supported by Alice, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Alice font: on or off', 'themify' ) ) {
		$fonts['alice'] = 'Alice';
	}

	return $fonts;
}
add_filter( 'themify_google_fonts', 'themify_theme_google_fonts' );

///////////////////////////////////////
// Build Write Panels
///////////////////////////////////////
if ( ! function_exists( 'themify_theme_init_types' ) ) {
	/**
	 * Initialize custom panel with its definitions
	 * Custom panel definitions are located in admin/post-type-TYPE.php
	 * @since 1.0.0
	 */
	function themify_theme_init_types() {
		// Load required files for post, page and custom post types where it applies
		foreach ( array( 'post', 'page', 'section', 'portfolio', 'highlight', 'team', 'product' ) as $type ) {
			require_once( "admin/post-type-$type.php" );
		}
	}
}

function themify_theme_setup_metaboxes( $meta_boxes, $post_type ) {

	if( ! in_array( $post_type, array( 'post', 'page', 'section', 'portfolio', 'highlight', 'team', 'product' ) ) ) {
		return $meta_boxes;
	}

	themify_theme_init_types();
	
	/**
	 * Options for header design
	 * @since 1.0.0
	 * @var array
	 */
	$header_design_options = themify_theme_header_design_options();
	
	/**
	 * Options for footer design
	 * @since 1.0.0
	 * @var array
	 */
	$footer_design_options = themify_theme_footer_design_options();

	$entry_id = isset( $_GET['post'] ) ? $_GET['post'] : null;

	if ( $entry_id ) {
		$background_mode = get_post_meta( $entry_id,'background_mode', true );
	}
	if ( empty( $background_mode ) ) {
		$background_mode = 'fullcover';
	}


	if( $post_type == 'post' ) {
		$theme_metaboxes = array(
			array(
				'name'    => __( 'Post Options', 'themify' ),
				'id'      => 'post-options',
				'options' => themify_theme_post_meta_box( array(
				) ),
				'pages'   => 'post'
			),
			array(
				'name'    => __( 'Page Appearance', 'themify' ),
				'id'      => 'post-theme-design',
				'options' => themify_theme_page_theme_design_meta_box( array(
					'header_design_options' => $header_design_options,
					'footer_design_options' => $footer_design_options,
					'background_mode'		=> $background_mode
				) ),
				'pages'   => 'post'
			),
		);
	} elseif( $post_type == 'page' ) {
		$theme_metaboxes = array(
			array(
				'name'    => __( 'Page Options', 'themify' ),
				'id'      => 'page-options',
				'options' => themify_theme_page_meta_box( array(
					'header_design_options' => $header_design_options,
					'footer_design_options' => $footer_design_options,
				) ),
				'pages'   => 'page'
			),
			array(
				'name'    => __( 'Page Appearance', 'themify' ),
				'id'      => 'page-theme-design',
				'options' => themify_theme_page_theme_design_meta_box( array(
					'header_design_options' => $header_design_options,
					'footer_design_options' => $footer_design_options,
					'background_mode'		=> $background_mode
				) ),
				'pages'   => 'page'
			),
			array(
				'name'		=> __('Query Posts', 'themify'),	
				'id' => 'query-posts',
				'options'	=> themify_theme_query_post_meta_box(), 		
				'pages'	=> 'page'
			),
			array(
				'name'		=> __('Query Portfolios', 'themify'),	
				'id' => 'query-portfolio',
				'options'	=> themify_theme_query_portfolio_meta_box(), 		
				'pages'	=> 'page'
			),
		);
		if( ! ( isset( $GLOBALS['themify']->deprecated_section ) && $GLOBALS['themify']->deprecated_section ) ) {
			$theme_metaboxes[] = array(
				'name'		=> __('Query Sections', 'themify'),	
				'id' => 'query-section',
				'options'	=> themify_theme_query_section_meta_box(), 		
				'pages'	=> 'page'
			);
		}
	} elseif( $post_type == 'section' ) {
		$theme_metaboxes = array(
			array(
				'name'		=> __('Section Options', 'themify'),			// Name displayed in box
				'id' => 'section-options',
				'options'	=> themify_theme_section_meta_box(), 	// Field options
				'pages'	=> 'section'					// Pages to show write panel
			),
		);
	} elseif( $post_type == 'portfolio' ) {
		$theme_metaboxes = array(
			array(
				'name'		=> __('Portfolio Options', 'themify'),
				'id' => 'portfolio-options',
				'options'	=> themify_theme_portfolio_meta_box(),
				'pages'	=> 'portfolio'
			),
			array(
				'name'		=> __('Portfolio Styles', 'themify'),
				'id' => 'portfolio-styles',
				'options'	=> themify_theme_page_theme_design_meta_box( array(
					'header_design_options' => $header_design_options,
					'footer_design_options' => $footer_design_options,
					'background_mode'		=> $background_mode
				) ),
				'pages'	=> 'portfolio'
			),
		);
	} elseif( $post_type == 'highlight' ) {
		$theme_metaboxes = array(
			array(
				'name'	=> __('Highlight Options', 'themify'),
				'id' => 'highlight-options',
				'options' => themify_theme_highlight_meta_box(),
				'pages'	=> 'highlight'
			),
			array(
				'name'		=> __('Highlight Styles', 'themify'),
				'id' => 'highlight-styles',
				'options'	=> themify_theme_page_theme_design_meta_box( array(
					'header_design_options' => $header_design_options,
					'footer_design_options' => $footer_design_options,
					'background_mode'		=> $background_mode
				) ),
				'pages'	=> 'highlight'
			),
		);
	} elseif( $post_type == 'team' ) {
		$theme_metaboxes = array(
			array(
				'name'	=> __('Team Options', 'themify'),	
				'id' => 'team-options',
				'options' => themify_theme_team_meta_box(),
				'pages'	=> 'team'
			),
			array(
				'name'		=> __('Team Styles', 'themify'),
				'id' => 'team-styles',
				'options'	=> themify_theme_page_theme_design_meta_box( array(
					'header_design_options' => $header_design_options,
					'footer_design_options' => $footer_design_options,
					'background_mode'		=> $background_mode
				) ),
				'pages'	=> 'team'
			),
		);
	} elseif( $post_type == 'product' ) {
		$theme_metaboxes = array(
			array(
				'name'		=> __('Product Header', 'themify'),
				'id' => 'product-styles',
				'options'	=> themify_theme_page_theme_design_meta_box( array(
					'header_design_options' => $header_design_options,
					'footer_design_options' => $footer_design_options,
					'background_mode'		=> $background_mode
				) ),
				'pages'	=> 'product'
			)
		);
	}

	return isset( $theme_metaboxes ) ? array_merge( $theme_metaboxes, $meta_boxes ) : $meta_boxes;
}
add_filter( 'themify_metabox/fields/themify-meta-boxes', 'themify_theme_setup_metaboxes', 10, 2 );


function themify_theme_header_background( $area = 'header', $classes = '' ) {
	$class = '';
	$style = '';
	$image = null;
	$repeat = '';
	$data_bg = '';
	$nobg = '';
	$area_wrap = themify_get( 'background_type' );
	$image_meta = '';
	$background_gallery = '';

	if ( 'header' === $area && in_array( $area_wrap, array( 'video', 'slider' ) ) ) {
                $post_id = get_the_ID();
		$back_key = 'styling-background-header_wrap_background-background_image-value-value';
		$mode_key = 'styling-background-header_wrap_background-background_mode-value-value';
		$nobg_key = 'styling-background-header_wrap_background-background_image-value-none';
		$header_customizer = json_decode( get_theme_mod( 'headerwrap_background' ) );

		if ( isset( $header_customizer->src ) && '' != $header_customizer->src ) {
			$image = $header_customizer->src;
			$repeat = isset( $header_customizer->style ) ? $header_customizer->style : 'fullcover';
		} elseif ( themify_check( $back_key ) ) {
			$image = themify_get( $back_key );
			$repeat = themify_check( $mode_key )? themify_get( $mode_key ): 'fullcover';
		}
		$nobg = themify_get( $nobg_key ) || ( isset( $header_customizer->noimage ) && 'noimage' == $header_customizer->noimage );

		if ( is_page() || is_singular( 'post' ) || is_singular( 'portfolio' ) ) {
			global $themify;

			if ( is_page() ) {
				$post_id = $themify->page_id;
			} else {
				$post_id = get_the_ID();
			}

			$image_meta = get_post_meta($post_id, 'background_image', true);
			if ( $image_meta ) {
				$image = $image_meta;
				$repeat_meta = get_post_meta($post_id, 'background_repeat', true);
				if ( $repeat_meta ) {
					$repeat = $repeat_meta;
				}
			}
			
			if ( $area_wrap === 'slider') {
                            $background_color = get_post_meta( $post_id, 'background_color', true );
                            if($background_color != '' ){
                                $data_bg .= "data-bgcolor='$background_color'";
                            }
			}
                        elseif ( 'video' === $area_wrap) {
                            $video = get_post_meta( $post_id, 'video_file', true );
                            if($video!=''){
                                $data_bg .= " data-fullwidthvideo='$video'";
                            }
			}
			$background_gallery = get_post_meta( $post_id, 'background_gallery', true );
			if ( $gallery = ( ( $area_wrap === 'slider' ) || ( $area_wrap == '' && $background_gallery != '' ) ) ) {
				$repeat = 'fullcover header-gallery';
			} elseif ( $nobg ) {
				// Checks $image_meta because $image might have been set in customizer and we don't need to check that.
				if ( is_singular() && $image_meta ) {
					$data_bg .= " data-bg='$image'";
				}
			} elseif ( $image ) {
				$data_bg .= " data-bg='$image'";
			}

			
		}
	}

	if ( $repeat || $classes ) {
		$class = "class='$repeat $classes'";
	}

	if ( !empty( $style ) ) {
		$style = 'style="' . $style . '"';
	}
	
	echo "$data_bg $class $style";
}

/**
 * Returns options for header design. Used in custom panel and theme settings.
 *
 * @since 1.0.0
 *
 * @return array List of header design options.
 */
function themify_theme_header_design_options() {
	return apply_filters( 'themify_theme_header_design_options', array(
		array(
			'value' => 'default',
			'img'   => 'images/layout-icons/default.png',
			'title' => __( 'Default', 'themify' ),
			'selected' => true,
		),
		array(
			'value' => 'header-block',
			'img'   => 'images/layout-icons/header-block.png',
			'title' => __( 'Header Block', 'themify' ),
		),
		array(
			'value' => 'header-horizontal',
			'img'   => 'images/layout-icons/header-horizontal.png',
			'title' => __( 'Header Horizontal', 'themify' ),
		),
		array(
			'value' => 'header-boxed-layout',
			'img'   => 'images/layout-icons/header-boxed-layout.png',
			'title' => __( 'Header Boxed Layout', 'themify' ),
		),
		array(
			'value' => 'header-leftpane', // hides sticky header
			'img'   => 'images/layout-icons/header-leftpanel.png',
			'title' => __( 'Header Left Pane', 'themify' ),
		),
		array(
			'value' => 'header-rightpane', // hides sticky header
			'img'   => 'images/layout-icons/header-rightpanel.png',
			'title' => __( 'Header Right Pane ', 'themify' ),
		),
		array(
			'value' => 'header-minbar', // hides sticky header
			'img'   => 'images/layout-icons/header-minbar.png',
			'title' => __( 'Header Min Bar', 'themify' ),
		),
		array(
			'value' => 'header-slide-out', // hides sticky header
			'img'   => 'images/layout-icons/header-slide-out.png',
			'title' => __( 'Header Slide Out', 'themify' ),
		),
		array(
			'value' => 'header-boxed-compact', // hides sticky header
			'img'   => 'images/layout-icons/header-boxed-compact.png',
			'title' => __( 'Header Boxed Compact', 'themify' ),
		),
		array(
			'value' => 'header-overlay', // hides sticky header
			'img'   => 'images/layout-icons/header-overlay.png',
			'title' => __( 'Header Overlay ', 'themify' ),
		),
		array(
			'value' => 'header-menu-bar', // hides sticky header
			'img'   => 'images/layout-icons/header-menu-bar.png',
			'title' => __( 'Menu Bar ', 'themify' ),
		),
		array(
			'value' => 'none', // hides sticky header
			'img'   => 'images/layout-icons/none.png',
			'title' => __( 'No Header ', 'themify' ),
		)
	));
}

/**
 * Returns options for footer design. Used in custom panel and theme settings.
 *
 * @since 1.0.0
 *
 * @return array List of footer design options.
 */
function themify_theme_footer_design_options() {
	return apply_filters( 'themify_theme_footer_design_options', array(
		array(
			'value' => 'default',
			'img'   => 'images/layout-icons/default.png',
			'title' => __( 'Theme Default', 'themify' ),
			'selected' => true,
		),
		array(
			'value' => 'footer-block',
			'img'   => 'images/layout-icons/footer-block.png',
			'title' => __( 'Footer Block', 'themify' ),
		),
		array(
			'value' => 'footer-left-col',
			'img'   => 'images/layout-icons/footer-left-col.png',
			'title' => __( 'Footer Left Column', 'themify' ),
		),
		array(
			'value' => 'footer-right-col',
			'img'   => 'images/layout-icons/footer-right-col.png',
			'title' => __( 'Footer Right Column', 'themify' ),
		),
		array(
			'value' => 'footer-horizontal-left',
			'img'   => 'images/layout-icons/footer-horizontal-left.png',
			'title' => __( 'Footer Horizontal Left', 'themify' ),
		),
		array(
			'value' => 'footer-horizontal-right',
			'img'   => 'images/layout-icons/footer-horizontal-right.png',
			'title' => __( 'Footer Horizontal Right', 'themify' ),
		),
		array(
			'value' => 'none',
			'img'   => 'images/layout-icons/none.png',
			'title' => __( 'No Footer ', 'themify' ),
		)
	)); // default, footer-left-col, footer-right-col, footer-horizontal-left, footer-horizontal-right
}

function themify_theme_get_header_design() {
	$header = themify_area_design( 'header', array(
		'default' => '',
		'values'  => wp_list_pluck( themify_theme_header_design_options(), 'value' ),
	) );
	return 'none' == $header ? 'header-none' : $header;
}

/**
 * Logic for fixed header. Checks, if it applies, custom fields first and then theme settings.
 *
 * @since 1.0.0
 *
 * @return string
 */
function themify_theme_fixed_header() {
        static $fixed = NULL;
        if(is_null($fixed)){
			
            $header = themify_area_design( 'header', array(
                    'values'  => wp_list_pluck( themify_theme_header_design_options(), 'value' ), ) );
            if ( in_array( $header, array( 'header-leftpane', 'header-rightpane', 'header-minbar', 'none' ) )) {
                    $fixed = ''; 
                    return $fixed;
            }
			
			//Removing the below code for optimization
			/* if ( $header == 'header-block' && themify_get_menu_bar_position() == 'top' ) {
                    $fixed = ''; 
                    return $fixed;
            } */
            if ( is_singular( array( 'post', 'page', 'portfolio' ) ) ) {
				 
                    $fixed_header_field = themify_get( 'fixed_header' );
                    if ( 'yes' == $fixed_header_field ) {
                        $fixed = 'fixed-header';
                        return $fixed;
                    } elseif ( 'no' == $fixed_header_field ) {
                            $fixed = '';
							
							return $fixed;
                    }
            }
            $fixed = themify_check( 'setting-fixed_header_disabled' ) ? '' : 'fixed-header';
			
		}
		
        return $fixed;
}

/* Sticky Header logo customizer */
function themify_theme_sticky_logo(){
     if(themify_theme_fixed_header()){
        global $themify_customizer;
        $logo = json_decode($themify_customizer->get_cached_mod('sticky_header_imageselect'));
        return isset($logo->src) && '' != $logo->src?$logo:false;
     }
     else{
         return false;
     }
}

/**
 * Function that checks if meta data exists and retrieves it, otherwise checks theme setting and retrieves it instead.
 * If it still doesn't exist, uses the default provided.
 *
 * @since 1.0.0
 *
 * @param string $meta
 * @param string $default
 * @param string $theme_setting
 *
 * @return mixed
 */
function themify_theme_get( $meta, $default = '', $theme_setting = '' ) {
	global $themify;
	$post_type = '';

	// If it's a singular view or a query page, try to get the post meta data first
	if ( themify_is_query_page() ) {
		// Let's check now prefixing with post type since it's a query post type page
			// Check without checking for custom post type
			$value = get_post_meta( $themify->page_id, $meta, true );
		if ( '' != $themify->query_post_type && 'post' != $themify->query_post_type ) {
			$post_type = $themify->query_post_type . '_';
			$cpt_meta = $post_type . $meta;
			$value = get_post_meta( $themify->page_id, $cpt_meta, true );
		} else {
		}
		if ( $value && '' != $value && 'default' != $value ) {
			return $value;
		}

	} elseif ( is_singular() ) {
		// Check first without checking for custom post type
		$value = get_post_meta( get_the_ID(), $meta, true );

		if ( $value && '' != $value && 'default' != $value ) {
			return $value;
		}
		// Let's check now prefixing with post type
		if ( ( 'post' != get_post_type() && 'page' != get_post_type() ) ) {
			$post_type = get_post_type() . '_';
		}
		$cpt_meta = $post_type . $meta;
		$value = get_post_meta( get_the_ID(), $cpt_meta, true );
		if ( $value && '' != $value && 'default' != $value ) {
			return $value;
		}
	} elseif( themify_is_shop() ) {
		return themify_get( $meta, $default );
	}

	// If there is no post meta data or is '' (default), prepare to fetch theme setting
	if ( empty( $theme_setting ) ) {
		if ( themify_is_query_page() && '' != $themify->query_post_type && 'post' != $themify->query_post_type ) {
			$post_type = $themify->query_post_type . '_';
		} elseif ( is_singular() && ( 'post' != get_post_type() && 'page' != get_post_type() ) ) {
			$post_type = get_post_type() . '_';
		}
		$theme_setting = 'setting-' . $post_type . $meta;
	}

	// Check theme setting (if there's a special setting like for portfolios it will be checked)
	if ( themify_check( $theme_setting ) ) {
		return themify_get( $theme_setting );
	}

	// Prepare to check non special setting stripping out the post type from setting key
	if ( 'post' != $post_type ) {
		$theme_setting = str_replace( $post_type, '', $theme_setting );
	}
	// Check regular setting (like portfolios that rely in default layouts setting)
	if ( themify_check( $theme_setting ) ) {
		return themify_get( $theme_setting );
	}
	// No luck so return default
	return $default;
}

if ( ! function_exists( 'themify_theme_body_class' ) ) {
	/**
	 * Adds body classes for special theme features.
	 *
	 * @since 1.0.0
	 *
	 * @param $classes
	 *
	 * @return array
	 */
	function themify_theme_body_class( $classes ) {
		
		$header = themify_theme_get_header_design();
		$classes[] = $header;
		$classes[] = themify_theme_fixed_header() ? 'fixed-header' : 'no-fixed-header';
		 
		if ( ! in_array( $header, array( 'header-leftpane', 'header-rightpane', 'header-minbar', 'header-none' ) ) ) {	
			// Add transparent-header class to body if user selected it in custom panel
			if ( ( is_single() || is_page() || themify_is_shop() ) && 'transparent' == themify_get( 'background_type' ) ) {
				$classes[] = 'transparent-header';
			}
		}
		if ( 'header-block' == $header && 'yes' == themify_theme_get( 'full_height_header' ) ) {
			$classes[] = 'header-full-height';
		}
		
		// Footer Design
		$footer = themify_area_design( 'footer', array(
			'default' => '',
			'values'  => wp_list_pluck( themify_theme_footer_design_options(), 'value' ),
		) );
		$classes[] = 'none' == $footer ? 'footer-none' : $footer;
		
		// Exclusion classes
		if ( ! themify_theme_show_area( 'site_logo' ) ) {
			$classes[] = 'logo-off';
		}
		if ( ! themify_theme_show_area( 'site_tagline' ) ) {
			$classes[] = 'tagline-off';
		}
		if ( ! themify_theme_show_area( 'social_widget' ) ) {
			$classes[] = 'social-widget-off';
		}
		if ( ! themify_theme_show_area( 'rss' ) ) {
			$classes[] = 'rss-off';
		}
		if ( ! themify_theme_show_area( 'search_form' ) ) {
			$classes[] = 'search-off';
		}
		if ( ! themify_theme_show_area( 'menu_navigation' ) ) {
			$classes[] = 'menu-navigation-off';
		}
		if ( ! themify_theme_show_area( 'header_widgets' ) ) {
			$classes[] = 'header-widgets-off';
		}
		if ( ! themify_theme_show_area( 'footer' ) ) {
			$classes[] = 'footer-off';
		}
		if ( ! themify_theme_show_area( 'footer_widgets' ) ) {
			$classes[] = 'footer-widgets-off';
		}
		if ( ! themify_theme_show_area( 'footer_menu_navigation' ) ) {
			$classes[] = 'footer-menu-navigation-off';
		}

		return $classes;
	}
}

if ( ! function_exists( 'themify_theme_show_area' ) ) {
	/**
	 * Checks whether to show a certain layout area or not.
	 *
	 * @param string $area
	 *
	 * @return bool
	 */
	function themify_theme_show_area( $area = '' ) {
		$show = true;
		switch ( $area ) {
			case 'header':
				$show = 'none' == themify_area_design( $area, array( 'values'  => wp_list_pluck( themify_theme_header_design_options(), 'value' ) ) ) ? false : true;
				break;
			case 'footer':
				$show = 'none' == themify_area_design( $area, array( 'values'  => wp_list_pluck( themify_theme_footer_design_options(), 'value' ) ) ) ? false : true;
				break;
			default:
				if ( ( is_singular() && ! is_attachment() ) || themify_is_shop() ) {
					$exclude = themify_get( 'exclude_' . $area );
					if ( 'yes' == $exclude ) {
						$show = false;
					} elseif ( 'no' == $exclude ) {
						$show = true;
					} else {
						$show = themify_check( 'setting-exclude_' . $area ) ? false : true;
					}
				} elseif ( themify_check( 'setting-exclude_' . $area ) ) {
					$show = false;
				}
				break;
		}

		return apply_filters( "themify_theme_show_{$area}", $show );
	}
}

if ( ! function_exists( 'themify_theme_do_not_exclude_all' ) ) {
	/**
	 * Checks if all the elements in a certain area have been excluded.
	 *
	 * @since 1.0.0
	 *
	 * @param string $area
	 *
	 * @return bool
	 */
	function themify_theme_do_not_exclude_all( $area = 'header' ) {
		if ( 'mobile-menu' == $area ) {
			return themify_theme_show_area( 'search_form' ) || themify_theme_show_area( 'social_widget' ) || themify_theme_show_area( 'header_widgets' ) || themify_theme_show_area( 'rss' ) || themify_theme_show_area( 'menu_navigation' );
		} elseif ( 'header' == $area ) {
			return themify_theme_show_area( 'site_logo' ) || themify_theme_show_area( 'site_tagline' ) || themify_theme_show_area( 'search_form' ) || themify_theme_show_area( 'social_widget' ) || themify_theme_show_area( 'header_widgets' ) || themify_theme_show_area( 'rss' ) || themify_theme_show_area( 'menu_navigation' );
		} elseif ( 'footer' == $area ) {
			return themify_theme_show_area( 'footer_site_logo' ) || themify_theme_show_area( 'footer_menu_navigation' ) || themify_theme_show_area( 'footer_texts' ) || themify_theme_show_area( 'footer_widgets' ) || themify_theme_show_area( 'footer_back' );
		}
		return false;
	}
}

/**
 * Add viewport tag for responsive layouts
 */
function themify_viewport_tag(){
	echo "\n".'<meta name="viewport" content="width=device-width, initial-scale=1">'."\n";
}

if ( ! function_exists( 'themify_theme_custom_post_css' ) ) {
	/**
	 * Outputs custom post CSS at the end of a post
	 * @since 1.0.0
	 */
	function themify_theme_custom_post_css() {
		global $themify;
		if (is_singular() && in_array( get_post_type(), array( 'post', 'page', 'portfolio','highlight','team') ) ) {
			$post_id = get_the_ID();
			if ( is_page() ) {
				if ( is_post_type_archive( 'product' ) ) {
					$entry_id = '.post-type-archive-product';
				} else {
					$entry_id = '.page-id-' . $post_id;
				}
			} else {
				$entry_id = '.postid-' . $post_id;
			}
			$headerwrap = $entry_id . ' #headerwrap';
			$site_logo = $entry_id . ' #site-logo';
			$site_description = $entry_id . ' #site-description';
			$main_nav = $entry_id . ' #main-nav';
			$social_widget = $entry_id . ' .social-widget';
			$css = array();
			$style = '';
			$rules = array(
				$headerwrap => array(),
				"$entry_id #site-logo span:after, $entry_id #headerwrap #searchform, $entry_id #main-nav .current_page_item a, $entry_id #main-nav .current-menu-item a" => array(
					array(
						'prop' => 'border-color',
						'key'  => 'headerwrap_text_color'
					),
				),
			);
			
			$header_type = themify_get( 'background_type' );
			if ( 'transparent' != $header_type ) {
				$rules = array(
					"$entry_id #site-logo span:after, $entry_id #headerwrap #searchform, $entry_id #main-nav .current_page_item a, $entry_id #main-nav .current-menu-item a" => array(
							array(
								'prop' => 'border-color',
								'key'  => 'headerwrap_text_color'
							),
					),
				);
				if ( 'solid' === $header_type || !$header_type || 'video' === $header_type ) {
					$rules[$headerwrap] = array(
						array(
							'prop' => 'background',
							'key'  => 'background_color'
						),
						array(
							'prop' => 'background-image',
							'key'  => 'background_image'
						),
						array(
							'prop' => 'background-repeat',
							'key'  => 'background_repeat',
							'dependson' => array(
								'prop' => 'background-image',
								'key'  => 'background_image'
							),
						),
					);
				}
			}
			
			// Body background color, image and image repeat is not influenced by presets.
			$body_bg_color_image = array(
				array(
					'prop' => 'background-color',
					'key'  => 'body_background_color'
				),
				array(
					'prop' => 'background-image',
					'key'  => 'body_background_image'
				),
				array(
					'prop' => 'background-repeat',
					'key'  => 'body_background_repeat',
					'dependson' => array(
						'prop' => 'background-image',
						'key'  => 'body_background_image'
					),
				),
			);		

			$rules["$headerwrap, $site_logo, $site_description"] = array(
				array(
					'prop' => 'color',
					'key'  => 'headerwrap_text_color'
				),
			);

			$rules["$site_logo a, $site_description a, $social_widget a, $main_nav > li > a"] = array(
				array(
					'prop' => 'color',
					'key'  => 'headerwrap_link_color'
				),
			);

			foreach ( $rules as $selector => $property ) {
				foreach ( $property as $val ) {
					$prop = $val['prop'];
					$key = $val['key'];

					if( ! themify_check( $key ) ) {
						continue;
					}

					$plain_key = themify_get( $key );
					if ( is_array( $key ) ) {
						if ( $prop == 'font-size' && themify_check( $key[0] ) ) {
							$css[$selector][$prop] = $prop . ': ' . themify_get( $key[0] ) . themify_get( $key[1] );
						}
					} elseif ( $prop == 'background-image' && 'default' != $plain_key ) {
						$css[$selector][$prop] = $prop .': url(' . $plain_key . ')';
					} elseif ( $prop == 'background-repeat' && 'fullcover' == $plain_key ) {
						if ( isset( $val['dependson'] ) ) {
							if ( $val['dependson']['prop'] == 'background-image' && ( themify_check( $val['dependson']['key'] ) && 'default' != themify_get( $val['dependson']['key'] ) ) ) {
								if( $selector === '.skin-styles' ) {
									$css['.iphone:before'] = $css[$selector];
									$css['.iphone:before']['background-size'] = 'background-size: cover; content: ""';
								}
								$css[$selector]['background-size'] = 'background-size: cover; background-attachment:fixed;';
							}
						} else {
							$css[$selector]['background-size'] = 'background-size: cover; background-attachment:fixed;';
						}
					} elseif ( themify_check( $key ) && 'default' != $plain_key ) {
						if ( $prop == 'color' || stripos( $prop, 'color' ) ) {
							$css[$selector][$prop] = $prop . ': ' . themify_get_color( $key );
						}
						else {
							$css[$selector][$prop] = $prop .': '. $plain_key;
						}
					}  else {
						$css[$selector][$prop] = $prop .': '. $plain_key;
					}
				}
				if ( ! empty( $css[$selector] ) ) {
					$style .= "$selector {\n\t" . implode( ";\n\t", $css[$selector] ) . "\n}\n";
				}
			}

			if ( '' != $style ) {
				echo "\n<!-- Entry Style -->\n<style>\n$style</style>\n<!-- End Entry Style -->\n";
			}
		}
	}
	add_action( 'wp_head', 'themify_theme_custom_post_css', 77 );
}


/**
 * Custom Post Type Background Gallery
***************************************************************************/
class Themify_Background_Gallery {

	/**
	 * Custom post type key
	 * @var String
	 */
	static $cpt = 'background-gallery';

	function __construct(){}

	function create_controller(){
		/** ID of default background gallery
		 * @var String|Number */
		$bggallery_id = $this->get_bggallery_id();
		$bggallery_order = $this->get_bggallery_order();
		$size = $this->get_bggallery_size();
		$bggallery_enable = themify_check( 'background_type' ) && themify_get( 'background_type' ) === 'slider';

		// If we still don't have a background gallery ID, do nothing.
		if( !$bggallery_id || 'default' === $bggallery_id || !$bggallery_enable ) return;

		$images = get_posts(array(
			'post__in' => $bggallery_id,
			'post_type' => 'attachment',
			'post_mime_type' => 'image',
			'numberposts' => -1,
			'orderby' => $bggallery_order,
			'order' => 'ASC'
		));

		if($images){
			$hide_controlls = themify_check( 'header_hide_controlls' ) ? 'class="tf-hide"' : '';
			echo '
				<div id="gallery-controller" ' . $hide_controlls . '>
				<div class="slider">
					<ul class="slides clearfix">';
			foreach( $images as $image ){
				// Get large size for background
				$image_data = wp_get_attachment_image_src( $image->ID, apply_filters( 'themify_theme_background_gallery_image_size', $size ) );
				echo '<li data-bg="',$image_data[0],'"><span class="slider-dot"></span></li>';
			}
			echo '		</ul>
						<div class="carousel-nav-wrap">
							<a href="#" class="carousel-prev" style="display: block; ">&lsaquo;</a>
							<a href="#" class="carousel-next" style="display: block; ">&rsaquo;</a>
						</div>

				</div>
			</div>
			<!-- /gallery-controller -->';
		}
	}

	/**
	 * Displays a list of current background galleries
	 * @return array of name/value arrays
	 */
	function get_backgrounds() {
		$bgs = get_posts( array(
			'post_type' => self::$cpt,
			'orderby' => 'title',
			'order' => 'ASC',
			'numberposts' => -1
		));
		$backgrounds = array();
		$backgrounds[] = array( 'name' => '', 'value' => 'default');
		foreach($bgs as  $background){
			$backgrounds[] = array(
				'name' => $background->post_title,
				'value' => $background->ID
			);
		}
		return $backgrounds;
	}

	/**
	 * Return the background gallery ID if one of the following is found:
	 * - bg gallery defined in theme settings
	 * - bg gallery defined in Themify custom panel, either in post or page
	 * @return String|Mixed Background Gallery ID or 'default'
	 */
	function get_bggallery_id() {
		$gallery_raw = themify_get( 'background_gallery' );

		$sc_gallery = preg_replace( '#\[gallery(.*)ids="([0-9|,]*)"(.*)\]#i', '$2', $gallery_raw );

		$image_ids = explode( ',', str_replace( ' ', '', $sc_gallery ) );

		return $image_ids;
	}
	
	function get_bggallery_order() {
		$sc_order = false;

		$gallery_raw = themify_get( 'background_gallery' );

		if ( strpos( $gallery_raw, 'orderby' ) !== false ) {
			$sc_order = preg_replace('#\[gallery(.*)orderby="([a-zA-Z0-9]*)"(.*)\]#i', '$2', $gallery_raw);
		}

		return $sc_order ? $sc_order : 'post__in';
	}
	
	function get_bggallery_size() {
		$size = false;

		$gallery_raw = themify_get( 'background_gallery' );

		if ( strpos( $gallery_raw, 'size' ) !== false ) {
			$size = preg_replace('#\[gallery(.*)size="([a-zA-Z0-9]*)"(.*)\]#i', '$2', $gallery_raw);
		}

		return $size ? $size : 'large';
	}
}
// Start Background Gallery
global $themify_bg_gallery;
$themify_bg_gallery = new Themify_Background_Gallery();

/* Custom Write Panels
/***************************************************************************/

if ( ! function_exists('themify_get_google_web_fonts_list') ) {
	/**
	 * Returns a list of Google Web Fonts
	 * @return array
	 * @since 1.0.0
	 */
	function themify_get_google_web_fonts_list() {
		$google_fonts_list = array(
			array('value' => '', 'name' => ''),
			array(
				'value' => '',
				'name' => '--- '.__('Google Fonts', 'themify').' ---'
			)
		);
		foreach( themify_get_google_font_lists() as $font ) {
			$google_fonts_list[] = array(
				'value' => $font,
				'name' => $font
			);
		}
		return apply_filters('themify_get_google_web_fonts_list', $google_fonts_list);
	}
}

if ( ! function_exists('themify_get_web_safe_font_list') ) {
	/**
	 * Returns a list of web safe fonts
	 * @return array
	 * @since 1.0.0
	 */
	function themify_get_web_safe_font_list($only_names = false) {
		$web_safe_font_names = array(
			'Arial, Helvetica, sans-serif',
			'Verdana, Geneva, sans-serif',
			'Georgia, \'Times New Roman\', Times, serif',
			'\'Times New Roman\', Times, serif',
			'Tahoma, Geneva, sans-serif',
			'\'Trebuchet MS\', Arial, Helvetica, sans-serif',
			'Palatino, \'Palatino Linotype\', \'Book Antiqua\', serif',
			'\'Lucida Sans Unicode\', \'Lucida Grande\', sans-serif'
		);

		if( ! $only_names ) {
			$web_safe_fonts = array(
				array('value' => 'default', 'name' => '', 'selected' => true),
				array('value' => '', 'name' => '--- '.__('Web Safe Fonts', 'themify').' ---')
			);
			foreach( $web_safe_font_names as $font ) {
				$web_safe_fonts[] = array(
					'value' => $font,
					'name' => str_replace( '\'', '"', $font )
				);
			}
		} else {
			$web_safe_fonts = $web_safe_font_names;
		}

		return apply_filters( 'themify_get_web_safe_font_list', $web_safe_fonts );
	}
}

// Return Google Web Fonts list
$google_fonts_list = themify_get_google_web_fonts_list();
// Return Web Safe Fonts list
$fonts_list = themify_get_web_safe_font_list();

///////////////////////////////////////
// Setup Write Panel Options
///////////////////////////////////////

/** Definition for tri-state hide meta buttons
 *  @var array */
$states = array(
	array(
		'name' => __('Hide', 'themify'),
		'value' => 'yes',
		'icon' => THEMIFY_URI . '/img/ddbtn-check.svg',
		'title' => __('Hide this meta', 'themify')
	),
	array(
		'name' => __('Do not hide', 'themify'),
		'value' => 'no',
		'icon' => THEMIFY_URI . '/img/ddbtn-cross.svg',
		'title' => __('Show this meta', 'themify')
	),
	array(
		'name' => __('Theme default', 'themify'),
		'value' => '',
		'icon' => THEMIFY_URI . '/img/ddbtn-blank.png',
		'title' => __('Use theme settings', 'themify'),
		'default' => true
	)
);
	
/* 	Custom Functions
/***************************************************************************/

	///////////////////////////////////////
	// Enable WordPress feature image
	///////////////////////////////////////
	add_theme_support( 'post-thumbnails' );

	///////////////////////////////////////
	// Setup content width for media
	///////////////////////////////////////
	if ( ! isset( $content_width ) ) {
		$content_width = 978;
	}
		
	/**
	 * Register Custom Menu Function
	 */
	function themify_register_custom_nav() {
		if (function_exists('register_nav_menus')) {
			register_nav_menus( array(
				'main-nav' => __( 'Main Navigation', 'themify' ),
				'footer-nav' => __( 'Footer Navigation', 'themify' ),
			) );
		}
	}
	
	/**
	 * Default Main Nav Function
	 */
	function themify_default_main_nav() {
		echo '<ul id="main-nav" class="main-nav clearfix">';
		wp_list_pages('title_li=');
		echo '</ul>';
	}

	/**
	 * Sets custom menu selected in page custom panel as navigation, otherwise sets the default.
	 */
	function themify_theme_menu_nav(){
		global $themify;
		if ( $custom_menu = themify_get( 'custom_menu' ) ) {
			$themify->custom_menu = $custom_menu;
		} else {
			$themify->custom_menu = '';
		}
		if('' != $themify->custom_menu){
			wp_nav_menu(array('menu' => $themify->custom_menu, 'fallback_cb' => 'themify_default_main_nav' , 'container'  => '' , 'menu_id' => 'main-nav' , 'menu_class' => 'main-nav'));
		} else {
			wp_nav_menu(array('theme_location' => 'main-nav' , 'fallback_cb' => 'themify_default_main_nav' , 'container'  => '' , 'menu_id' => 'main-nav' , 'menu_class' => 'main-nav'));
		}
	}

if ( ! function_exists( 'themify_theme_maybe_hide_shop_title' ) ) {
	/**
	 * Hide the page title if it's the shop page and user choosed to hide it.
	 * @param bool $show_title
	 * @return bool
	 */
	function themify_theme_maybe_hide_shop_title( $show_title ) {
		$hide_page_title = get_post_meta( get_option( 'woocommerce_shop_page_id' ) , 'hide_page_title', true );
		if ( themify_is_shop() && 'yes' == $hide_page_title ) {
			return false;
		}
		return $show_title;
	}
	add_filter('woocommerce_show_page_title', 'themify_theme_maybe_hide_shop_title');
}
	
	/**
	 * Checks if the browser is a mobile device
	 * @return bool 
	 */
	function themify_is_mobile(){
		return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
	}

	///////////////////////////////////////
	// Register Sidebars
	///////////////////////////////////////
	if ( function_exists('register_sidebar') ) {
		register_sidebar(array(
			'name' => __('Sidebar', 'themify'),
			'id' => 'sidebar-main',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="widgettitle">',
			'after_title' => '</h4>',
		));
		register_sidebar(array(
			'name' => __('Social Widget', 'themify'),
			'id' => 'social-widget',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<strong class="widgettitle">',
			'after_title' => '</strong>',
		));
	}

	///////////////////////////////////////
	// Footer Sidebars
	///////////////////////////////////////
	themify_register_grouped_widgets();

// Exclude CPT for sidebar
add_filter( 'themify_exclude_CPT_for_sidebar', 'themify_CPT_exclude_sidebar' );

if( ! function_exists('themify_CPT_exclude_sidebar') ) {
	/**
	 * Exclude Custom Post Types
	 */
	function themify_CPT_exclude_sidebar($CPT = array()) {
		
		$parallax = array('portfolio', 'team');
		
		if(empty($CPT)){
			$CPT = array('post', 'page', 'attachment', 'tbuilder_layout', 'tbuilder_layout_part', 'section');
		}
		
		if(themify_is_woocommerce_active()){
			$parallax[] = 'product';
		}

		$CPT = array_merge($CPT, $parallax);

		return $CPT;
	}
}

if( ! function_exists('themify_theme_comment') ) {
	/**
	 * Custom Theme Comment
	 * @param object $comment Current comment.
	 * @param array $args Parameters for comment reply link.
	 * @param int $depth Maximum comment nesting depth.
	 * @since 1.0.0
	 */
	function themify_theme_comment($comment, $args, $depth) {
	   $GLOBALS['comment'] = $comment; 
	   ?>

		<li id="comment-<?php comment_ID() ?>">
			<p class="comment-author"> <?php echo get_avatar($comment,$size='48'); ?> <?php printf('<cite>%s</cite>', get_comment_author_link()) ?><br />
				<small class="comment-time"><?php comment_date( apply_filters( 'themify_comment_date', '' ) ); ?> @ <?php comment_time( apply_filters( 'themify_comment_time', '' ) ); ?>
				<?php edit_comment_link( __('Edit', 'themify'),' [',']') ?>
				</small> </p>
			<div class="commententry">
				<?php if ($comment->comment_approved == '0') : ?>
				<p><em>
					<?php _e('Your comment is awaiting moderation.', 'themify') ?>
					</em></p>
				<?php endif; ?>
				<?php comment_text() ?>
			</div>
			<p class="reply">
				<?php comment_reply_link(array_merge( $args, array('add_below' => 'comment', 'depth' => $depth, 'reply_text' => __( 'Reply', 'themify' ), 'max_depth' => $args['max_depth']))) ?>
			</p>
	<?php
	}
}

if ( ! function_exists( 'themify_theme_section_category_classes' ) ) {
	/**
	 * Outputs Section post category classes
	 * @param $post_id int Entry ID to retrieve terms from
	 * @return string
	 */
	function themify_theme_section_category_classes($post_id) {
		$sectioncats = get_the_terms($post_id, 'section-category');
		$categoryclass = '';
		foreach($sectioncats as $sectioncat) {
			$categoryclass .= ' section-category-' . $sectioncat->term_id . ' section-category-' . $sectioncat->slug;
		}
		return $categoryclass;
	}
}

function themify_get_menu_bar_position() {
	$position = themify_get( 'setting-menu_bar_position', 'top' );
	if( is_singular() && ( $single_value = themify_get( 'menu_bar_position' ) ) ) {
		$position = $single_value;
	}

	return $position;
}

function themify_theme_layout_body_classes( $classes ) {

	global $themify;
	$header = themify_theme_get_header_design();
	
	$classes[] = ( $themify->hide_header == 'yes' ) ? 'no-header' : 'with-header';
	$classes[] = ( $themify->hide_footer == 'yes' ) ? 'no-footer' : 'with-footer';
	
	if(themify_get_menu_bar_position() == 'top' ) {
		$classes[] = 'nav-bar-top';
	}
	
	if (!in_array( $header, array( 'header-leftpane', 'header-rightpane', 'header-minbar', 'header-slide-out', 'header-overlay', 'none' ) ) &&  (themify_get_mobile_menu_style() == 'dropdown-menu') ) {
		$classes[] = 'dropdown-menu-style';
	}

	// Image Filters
	$filter = '';
	$filter_hover = '';
	$apply_to = '';

	if ( is_page() || themify_is_shop() ) {

		if ( $do_filter = themify_get( 'imagefilter_options' ) ) {
			if ( 'initial' != $do_filter ) {
				$filter = 'filter-' . $do_filter;
			}
		}

		if ( $do_hover_filter = themify_get( 'imagefilter_options_hover' ) ) {
			if ( 'initial' != $do_hover_filter ) {
				$filter_hover = 'filter-hover-' . $do_hover_filter;
			}
		}

		if ( $apply_here = themify_get('imagefilter_applyto') ) {
			if ( 'initial' != $apply_here ) {
				$apply_to = 'filter-' . $apply_here;
			}
		}

	} elseif ( is_singular() ) {

		if ( $do_filter = themify_get( 'imagefilter_options' ) ) {
			if ( 'initial' != $do_filter ) {
				$filter = 'filter-' . $do_filter;
			}
		}

		if ( $do_hover_filter = themify_get( 'imagefilter_options_hover' ) ) {
			if ( 'initial' != $do_hover_filter ) {
				$filter_hover = 'filter-hover-' . $do_hover_filter;
			}
		}

		if ( $apply_here = themify_get('imagefilter_applyto') ) {
			if ( 'initial' != $apply_here ) {
				$apply_to = 'filter-' . $apply_here;
			}
		}

	}

	if ( '' == $filter ) {
		if ( $do_filter = themify_get( 'setting-imagefilter_options' ) ) {
				$filter = 'filter-' . $do_filter;
		}
	}

	if ( '' == $filter_hover ) {
		if ( $do_hover_filter = themify_get( 'setting-imagefilter_options_hover' ) ) {
				$filter_hover = 'filter-hover-' . $do_hover_filter;
		} else {
				$filter_hover = 'filter-hover-none';
		}
	}

	if ( '' == $apply_to ) {
		if ( '' != $filter || '' != $filter_hover ) {
			if ( 'allimages' == themify_get('setting-imagefilter_applyto') ) {
					$apply_to = 'filter-all';
			} else {
					$apply_to = 'filter-featured-only';
			}
		}
	}

	$classes[] = $filter;
	$classes[] = $filter_hover;
	$classes[] = $apply_to;
	return $classes;
}

add_filter( 'body_class', 'themify_theme_layout_body_classes' );

/**
 * Removes Sections meta box from Menus screen.
 *
 * @since 2.0.0
 */
function themify_theme_unregister_section_menu() {
	remove_meta_box('section-menu', 'nav-menus', 'side');
}

/**
 * Check if there are entries of section post type and unregister section post type otherwise.
 *
 * @since 2.0.0
 */
function themify_theme_unregister_section() {
	$section = get_posts(
		array(
			 'post_type' => 'section',
			 'posts_per_page' => 1
		)
	);
	if ( ! $section ) {
		// Unregister section post type and taxonomy
		global $wp_post_types, $wp_taxonomies, $themify;

		/* flag: section post type is deprecated */
		$themify->deprecated_section = true;

		if ( isset( $wp_post_types[ 'section' ] ) ) {
			unset( $wp_post_types[ 'section' ] );
		}
		if ( isset( $wp_taxonomies[ 'section-category' ] ) ) {
			unset( $wp_taxonomies[ 'section-category' ] );
		}

		// Remove meta box in menu screen
		add_action( 'admin_init', 'themify_theme_unregister_section_menu', 12 );
	}

}

// Once WP is loaded, check if we need to remove section post type and taxonomy
add_action( 'wp_loaded', 'themify_theme_unregister_section' );

/**
 * Check if there are entries of highlight post type and unregisters post type otherwise.
 *
 * @since 1.8.1
 */
function themify_theme_unregister_highlights() {
	$highlight = get_posts(
		array(
			 'post_type' => 'highlight',
			 'posts_per_page' => 1
		)
	);
	if ( ! $highlight ) {
		// Unregister highlight post type and taxonomy
		global $wp_post_types, $wp_taxonomies;
		if ( isset( $wp_post_types[ 'highlight' ] ) ) {
			unset( $wp_post_types[ 'highlight' ] );
		}
		if ( isset( $wp_taxonomies[ 'highlight-category' ] ) ) {
			unset( $wp_taxonomies[ 'highlight-category' ] );
		}
	}

}
add_action( 'wp_loaded', 'themify_theme_unregister_highlights' );

/**
 * If there's a background image set in the page, don't show the one set in Customizer.
 *
 * @since 1.7.2
 *
 * @param $css
 *
 * @return mixed
 */
function themify_theme_customizer_styling( $css ) {
	global $themify;
	$image = '';

	if ( is_page() ) {
		$post_id = $themify->page_id;
	} else {
		$post_id = get_the_ID();
	}

	$image_meta = get_post_meta( $post_id, 'background_image', true );
	if ( $image_meta ) {
		$image       = $image_meta;
		$repeat_meta = get_post_meta( $post_id, 'background_repeat', true );
		if ( $repeat_meta ) {
			$repeat = $repeat_meta;
		}
	}

	$pattern = '/#headerwrap(.*?)\n(.*?)(background-image(.*?);)/i';
	$replace = "#headerwrap { ";

	if ( $gallery = ( ( get_post_meta( $post_id, 'background_type', true ) == 'slider' ) || ( get_post_meta( $post_id, 'background_type', true ) == '' && get_post_meta( $post_id, 'background_gallery', true ) != '' ) ) ) {
		$css = preg_replace( $pattern, $replace, $css );
	} elseif ( $image ) {
		$css = preg_replace( $pattern, $replace, $css );
	}

	return $css;
}
add_filter( 'themify_customizer_styling', 'themify_theme_customizer_styling' );

/**
 * Set the fixed-header selector for the scroll highlight script
 *
 * @since 1.7.9
 */
function themify_theme_scroll_highlight_vars( $vars ) {
	$vars['fixedHeaderSelector'] = '#headerwrap.fixed-header';
	return $vars;
}
add_filter( 'themify_builder_scroll_highlight_vars', 'themify_theme_scroll_highlight_vars' );

if ( ! function_exists( 'themify_update_shop_options' ) ) {
	/**
	 * Hide header or footer for shop page when user choose to hide it.
	 *
	 * @since 1.8.7
	 */
	function themify_update_shop_options() {
		// Check if WC is active and this is a WC-managed page
		if ( !themify_is_woocommerce_active() || !function_exists('is_woocommerce') || !is_woocommerce() ) return;

		global $themify;

		$woo_hide_header = get_post_meta(get_option('woocommerce_shop_page_id'), 'hide_header', true);

		if ( themify_is_shop() && $woo_hide_header == 'on' ) {
			$themify->hide_header = 'yes';
		}

		$woo_hide_footer = get_post_meta(get_option('woocommerce_shop_page_id'), 'hide_footer', true);

		if ( themify_is_shop() && $woo_hide_footer == 'on' ) {
			$themify->hide_footer = 'yes';
		}
	}
	add_action( 'themify_body_start','themify_update_shop_options' );
}

/**
 * Handle Builder's JavaScript fullwidth rows, forces fullwidth rows if sidebar is disabled
 *
 * @return bool
 */
function themify_theme_fullwidth_layout( $support ) {
	global $themify;

	/* if Content Width option is set to Fullwidth, do not use JavaScript */
	if( themify_get( 'content_width' ) == 'full_width' ) {
		return true;
	}

	/* using sidebar-none layout, force fullwidth rows using JavaScript */
	if( $themify->layout == 'sidebar-none' ) {
		return false;
	}

	return true;
}
add_filter( 'themify_builder_fullwidth_layout_support', 'themify_theme_fullwidth_layout' );

/**
 * Load custom script file for Themify Custom Panel
 *
 * @since 2.1.6
 */
function themify_theme_custom_panel_scripts() {
	wp_enqueue_script( 'themify-custom-panel', trailingslashit( THEME_URI ) . 'admin/js/themify-custom-panel.js', array( 'themify-metabox' ), null, true );
}
add_action( 'themify_metabox_enqueue_assets', 'themify_theme_custom_panel_scripts' );

/**
 * Determines the side where sidemenu panel shows, based on selected header layout
 *
 * @since 1.8.3
 */
function themify_sidemenu_data() {
	$header = themify_theme_get_header_design();
	$side = in_array( $header, array( 'header-minbar', 'header-leftpane' ) ) ? 'left' : 'right';
	$hideon = in_array( $header, array( 'header-minbar', 'header-slide-out', 'header-overlay' ) ) ? 9999 : themify_get_option( 'setting-mobile_menu_trigger_point', 1200 );

	echo "data-side='{$side}' data-hideon='{$hideon}'";
}
if (is_admin()) {
	add_action('admin_enqueue_scripts', 'themify_admin_script_style');
	function themify_admin_script_style() {
		wp_enqueue_script('themify-admin-script', themify_enque(THEME_URI . '/admin/js/admin-script.js'));
	}
}

// Portfolio comments filter
function portfolio_comments_open( $open, $post_id ) {
	$post = get_post( $post_id );
	! empty( $post ) && 'portfolio' == $post->post_type
		&& themify_check( 'setting-portfolio_comments' ) && ( $open = true );
	return $open;
}
add_filter( 'comments_open', 'portfolio_comments_open', 10, 2 );


/**
 * Creates module for Mobile Menu style
 */
function themify_get_mobile_menu_style() {
	$mobile_menu_style = themify_get( 'setting-mobile_menu_style', 'slide-menu' );
	if( is_singular() && ( $single_value = themify_get( 'mobile_menu_style' ) ) ) {
		$mobile_menu_style = $single_value;
	}
	return $mobile_menu_style;
}
/*Temprorary code 21.09.19*/
if ( ! function_exists( 'themify_enque_style' ) ){
    function themify_enque_style($handle, $src = '', $deps = array(), $ver = false, $media = 'all' ){
	wp_enqueue_style($handle, themify_enque($src), $deps, $ver, $media);
    }
}