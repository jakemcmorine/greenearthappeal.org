<?php
/** Themify Default Variables
 @var object */
	global $themify; 
	if(themify_theme_show_area( 'footer_widgets' )){
		$footer_position = themify_get('footer_widget_position');
		if(!$footer_position){
			$footer_position = themify_get('setting-footer_widget_position');
		}
	}
	else{
		$footer_position = false;
	}
?>
	<?php themify_layout_after(); //hook ?>
    </div>
	<!-- /body -->

	<?php if ( 'yes' != $themify->hide_footer ) : ?>
		<div id="footerwrap" <?php themify_theme_header_background( 'footer' ); ?>>

			<?php themify_footer_before(); // hook ?>
			<footer id="footer" class="pagewidth">
				<?php themify_footer_start(); // hook ?>

				<?php if ( themify_theme_show_area( 'footer_back' ) ) : ?>
					<p class="back-top"><a href="#header"><span><?php _e('Back to top', 'themify'); ?></span></a></p>
				<?php endif; // exclude footer back ?>
			
				<div class="main-col first clearfix">
					<div class="footer-left-wrap first">
						<?php if ( themify_theme_show_area( 'footer_menu_navigation' ) ) : ?>
							<?php if (function_exists('wp_nav_menu')) {
								wp_nav_menu(array('theme_location' => 'footer-nav' , 'fallback_cb' => '' , 'container'  => '' , 'menu_id' => 'footer-nav' , 'menu_class' => 'footer-nav')); 
							} ?>
						<?php endif; // exclude menu navigation ?>
					</div>
					<div class="footer-right-wrap">

						<?php if ($footer_position!=='top') : ?>
							<div class="footer-text clearfix">
								<?php if ( themify_theme_show_area( 'footer_texts' ) ) : ?>
									<?php themify_the_footer_text(); ?>
									<?php themify_the_footer_text('right'); ?>
								<?php endif; ?>
							</div>
							<!-- /footer-text --> 
						<?php endif;?>
					</div>
				</div>
				
				<?php if (themify_theme_show_area( 'footer_widgets' )) : ?>
					<?php if($footer_position==='top'):?>
						<div class="section-col clearfix">
							<div class="footer-widgets-wrap">
								<?php get_template_part( 'includes/footer-widgets'); ?>
								<!-- /footer-widgets -->
							</div>
						</div>
						<div class="footer-text clearfix">
							<?php if ( themify_theme_show_area( 'footer_texts' ) ) : ?>
								<?php themify_the_footer_text(); ?>
								<?php themify_the_footer_text('right'); ?>
							<?php endif; ?>
						</div>
						<!-- /footer-text --> 
					<?php else:?>
						<div class="section-col clearfix">
							<div class="footer-widgets-wrap">
								<?php get_template_part( 'includes/footer-widgets'); ?>
								<!-- /footer-widgets -->
							</div>
						</div>
					<?php endif;?>
				<?php endif;?>
				
				<?php themify_footer_end(); // hook ?>
				
			</footer>
			<!-- /#footer --> 
			
			<?php themify_footer_after(); // hook ?>
			
		</div>
		<!-- /#footerwrap -->
	<?php endif; // exclude footer ?>
	
</div>
<!-- /#pagewrap -->

<?php
/**
 *  Stylesheets and Javascript files are enqueued in theme-functions.php
 */
?>

<?php themify_body_end(); // hook ?>
<!-- wp_footer -->
<?php wp_footer(); ?>

</body>
</html>