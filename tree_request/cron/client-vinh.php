<?php

set_time_limit(0);

$api_url = "https://www.tpcc.co/api/v1/trees/";
//$api_url = "http://ami3.co.uk/api/v1/trees/";

$api_key = "?api=5RI6j46g";
$get_data = true;
$is_live = true;

require_once("db.php");
require_once("functions.php");
$newobj = new cronClass();

if ($get_data) {

    $data = file_get_contents($api_url . "users" . $api_key);
    //$data = file_get_contents('users_data.txt');
    //$users = json_decode($data);

    $data1 = file_get_contents($api_url . "users/updates" . $api_key);
    //$data1 = file_get_contents('users_update_data2.js');
    //$users_up = json_decode($data1);
    
    $users = json_decode($data1);
    $users_up = json_decode($data);

}

error_log("New Users => " . $data . " \ndate: " . date("Y-m-d H:i:s", time()) . " \n\n", 3, "newusers");
error_log("Update Users => " . $data1 . " \ndate: " . date("Y-m-d H:i:s", time()) . " \n\n", 3, "updateusers");


// update users
if (isset($users_up) && count($users_up) > 0) {
    foreach ($users_up as $user_update) {
        if ($user_update->account_number != 0 && $user_update->account_number != '') {
            $sql = "SELECT companycode FROM t_partner_cert_companies WHERE comp_id = '" . $user_update->account_number . "'";
            $r = mysql_fetch_array(mysql_query($sql));
            $comp_code = $r[0];

			if ($comp_code) {
				$logo = $comp_code . ".png";
	            $artwork = $user_update->account_number . "_art.png";
    	        file_put_contents("../../graphics/sponsors/" . $logo, base64_decode($user_update->logo));
        	    file_put_contents("../company_artwork/" . $artwork, base64_decode($user_update->artwork));

            	$sql = "UPDATE `t_partner_cert_companies` SET `compname` = '" . addslashes($user_update->name) . "', `compaddress1` = '" . addslashes($user_update->address1) . "', `compaddress2` = '" . addslashes($user_update->address2) . "', `compaddress3` = '" . addslashes($user_update->address3) . "', `comppostcode` = '" . $user_update->postcode . "', `compweb` = '" . $user_update->website . "', `contacttitle` = '" . $user_update->title . "', `contactforename` = '" . $user_update->forename . "', `contactsurname` = '" . $user_update->surname . "', `contacttelephone` = '" . $user_update->telephone . "', `contactemail` = '" . $user_update->email . "', `contactnotes` = '" . $user_update->notes . "', `companylogo` = '" . $logo . "', `updated_date` = '" . $user_update->updated_at . "', `partner_id` = '2',  `companyartwork` =  '" . $artwork . "', `facebook_url` = '" . $user_update->facebook . "', `twitter_url` = '" . $user_update->twitter . "' WHERE `comp_id` = '" . $user_update->account_number . "'";
	            mysql_query($sql);

    	        $newobj->updateSponsors($comp_code, $user_update->name, "", $user_update->website);
			}
        } // if ends 
    } // foreach ends 
} // if ends


$last_id = 0;
$new = 0;
// add_users
if (isset($users) && count($users) > 0) {
    foreach ($users as $k => $user) {
    
        if ($user->account_number != '') {
            $sql_client = "SELECT * FROM `t_partner_cert_companies` WHERE `comp_id` =  '" . $user->account_number . "'";
            $query_client = mysql_query($sql_client);
            $client_inf = mysql_fetch_array($query_client);

            $count_client = mysql_num_rows($query_client);
            
            $today = date('Y-m-d H:i:s');
			function randomKey($length) {  //create 5  digit random alphanumeric code
					$key='';
					$pool = array_merge(range(0,9),range('A', 'Z'));

					for($i=0; $i < $length; $i++) {
						$key .= $pool[mt_rand(0, count($pool) - 1)];
					}
					return $key;
				}

				$alphpnumeric = randomKey(5);
            
            if ($count_client <= 0) {
                $comp_code = $newobj->generateLinkCode();

                $logo = $comp_code . ".png";
                //$logo = $user->account_number.".png";
                $artwork = $user->account_number . "_art.png";
                file_put_contents("../../graphics/sponsors/" . $logo, base64_decode($user->logo));
                file_put_contents("../company_artwork/" . $artwork, base64_decode($user->artwork));

                $sql = "INSERT INTO `t_partner_cert_companies`(`comp_id`, `compname`, `compaddress1`, `compaddress2`, `compaddress3`, `comppostcode`, `compweb`, `contacttitle`, `contactforename`, `contactsurname`, `contacttelephone`, `contactemail`, `contactnotes`, `companylogo`, `added_date`, `partner_id`,  `companyartwork`, `facebook_url`, `twitter_url`, `companycode`, `5_digit_code`) VALUES ('" . $user->account_number . "','" . addslashes($user->name) . "','" . addslashes($user->address1) . "','" . addslashes($user->address2) . "','" . addslashes($user->address3) . "','" . $user->postcode . "','" . $user->website . "','" . $user->title . "','" . $user->forename . "','" . $user->surname . "','" . $user->telephone . "','" . $user->email . "','" . $user->notes . "','" . $logo . "', '" . $user->created_at . "', '2', '" . $artwork . "','" . $user->facebook . "','" . $user->twitter . "', '" . $comp_code . "', '" . $alphpnumeric . "')";
                mysql_query($sql);
                $last_id = mysql_insert_id();

                $newobj->updateSponsors($comp_code, $user->name, $logo, $user->website);
                $sql2 = "INSERT INTO `t_partner_cert_requests`(`custaccountid`, `numtrees`, `date`) VALUES ('" . $last_id . "','" . $user->trees_today . "', '" . $today . "')";
                mysql_query($sql2);
                $new = 1;
            } // if new user insert query
            else {
                $last_id = $client_inf['id'];
                $sql2 = "INSERT INTO `t_partner_cert_requests`(`custaccountid`, `numtrees`, `date`) VALUES ('" . $last_id . "','" . $user->trees_today . "', '" . $today . "')";
                mysql_query($sql2);
                $new = 0;
            }


            if ($last_id > 0) {
                $sql = "SELECT companycode, compname, partner_id FROM t_partner_cert_companies WHERE id=" . $last_id;
                $r = mysql_fetch_array(mysql_query($sql));
                $pct_linkcode = $r[0];
                $pct_compname = $r[1];
                $pct_id = $last_id;
                $cp_id = $r['partner_id'];

                // create partner ticker image for total trees 
                $totalPartnerSum = 0;
                $sqlCount = "SELECT id FROM t_partner_cert_companies WHERE partner_id={$cp_id}";
                $c = mysql_query($sqlCount);
                while ($cnt = mysql_fetch_array($c)) {
                    $tr_total = $newobj->treetotal($cnt['id']);
                    $totalPartnerSum = $totalPartnerSum + $tr_total;
                }

                $newobj->createCounterImageForPartner($totalPartnerSum, $cp_id);
                $newobj->createCounterImage2($pct_id);

                // get number of trees
                $sql = "SELECT SUM(numtrees) FROM t_partner_cert_requests WHERE custaccountid=$pct_id";
                $r2 = mysql_fetch_array(mysql_query($sql));
                $pct_numtrees = $r2[0];
                $pct_numtrees_thou = number_format($pct_numtrees * 1000);

                // get logo partner
                $sql2 = 'SELECT ts.slogo AS slogo FROM t_partner_accounts AS tpa
									INNER JOIN t_sponsors AS ts
									ON tpa.sponsor_id=ts.id
									WHERE tpa.id=' . $r[2];
                $result = mysql_fetch_array(mysql_query($sql2));
                $pct_logo = $result[0];
                ob_clean();
                ob_start();
                require_once('cert2.php');

                $pageLayout = array(757, 1118);
                $pdf = new MYPDF('p', 'px', $pageLayout, true, 'UTF-8', false);

                // set document information
                $pdf->SetCreator('Thakur Yogita');
                $pdf->SetAuthor('Marvin');
                $pdf->SetTitle('Certification');
                $pdf->SetSubject('');
                $pdf->SetKeywords('php programmer, thakuryogita@gmail.com');

                // set header and footer fonts
                $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

                // set default monospaced font
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                //set margins
                //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                $pdf->SetMargins(0, 0, 0, true);
                $pdf->SetHeaderMargin(0);
                $pdf->SetFooterMargin(0);

                // remove default footer
                $pdf->setPrintFooter(false);

                //set auto page breaks
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

                //set image scale factor
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

                //set some language-dependent strings
                $pdf->setLanguageArray($l);

                // ---------------------------------------------------------
                // set font
                //$pdf->SetFont('times', '', 48);
                $pdf->SetFont('helvetica', '', 40);
                // add a page
                $pdf->AddPage();
                $pdf->SetY(150);
                $pdf->SetX(20);


                $pct_image_url = '';
                $ds = DIRECTORY_SEPARATOR;
                if (file_exists('..' . $ds . 'graphics' . $ds . 'sponsors' . $ds . $pct_logo)) {
                    $pct_image_url = '../../graphics/sponsors/' . $pct_logo;
                    $pct_image_url = '<img src="' . $pct_image_url . '" title="logo" />';
                }
                $tbl = '
						<table cellspacing="0" cellpadding="10" border="0">
							<tr>
								<td rowspan="2" colspan="2">' . $pct_image_url . '</td>
								<td colspan="5"><p style="font-family:Tahoma; font-weight: bold; font-size: 36px; color:#658fca; text-align: center;">' . $pct_compname . '</p></td>
							</tr>
							<tr>
								<td colspan="5"><p style="font-family:Arial; font-weight: bold; font-size: 26px; color:#166734; text-align: center">have planted ' . $pct_numtrees . ' trees to offset the carbon footprint of ' . $pct_numtrees_thou . ' paper cups produced for their customers, through an environmental programme which is operated by The Green Earth Appeal.</p></td>
							</tr>
						</table>';

                $pdf->writeHTML($tbl, true, false, true, false, '');

                $pdf->SetY(875);
                $pdf->SetX(290);
                //$pdf->SetFont('times', '', 48);
                $html3 = '<span style="font-size: 16px; color: #171213">' . date("jS F Y") . '</span>';
                $pdf->writeHTML($html3, true, false, true, false, '');
                //Close and output PDF document
                $ff = '../certification/cert_' . $pct_id . '.pdf';
                $pdf->Output($ff, 'F');
                ///
                ob_end_clean();
                $newobj->doInformationMail2($new, $last_id, $user->trees_today, true);
                error_log("New Success! for: " . $last_id . " = trees: " . $user->trees_today . " date: " . date("Y-m-d H:i:s", time()) . " \n", 3, "error_log.log");
            }  // if
            else {
                $newobj->doInformationMail2($new, $last_id, $user->trees_today);
                error_log("New Error! for: " . $user->account_number . " = trees: " . $user->trees_today . " date: " . date("Y-m-d H:i:s", time()) . " \n", 3, "error_log.log");
            }
        } // if
    } // foreach ends 
    
    echo $totalPartnerSum; die;
} // if ends 
?>