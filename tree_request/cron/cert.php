<?php
require_once('../tcpdf/config/lang/eng.php');
require_once('../tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
	//Page header
	public function Header() {
		// get the current page break margin
		$bMargin = $this->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $this->AutoPageBreak;
		// disable auto-page-break
		$this->SetAutoPageBreak(false, 0);
		// set bacground image
		//$img_file = K_PATH_IMAGES.'bg.jpg';
        $img_file = '../bg.jpg';
;
        //$this->Image($img_file, 0, 0, 1200, 1772, '', '', '', false, 300, '', false, false, 0);
        $this->Image($img_file, 0, 0, 757, 1118, '', '', '', false, 300, '', false, false, 0);
        
		// restore auto-page-break status
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$this->setPageMark();
	}
}

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pageLayout = array(757, 1118);
$pdf = new MYPDF('p', 'px', $pageLayout, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Vinh Nguyen');
$pdf->SetAuthor('Marvin');
$pdf->SetTitle('Certification');
$pdf->SetSubject('');
$pdf->SetKeywords('php programmer, phpcodeteam@gmail.com');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(0, 0, 0, true);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
//$pdf->SetFont('times', '', 48);
$pdf->SetFont('helvetica', '', 40);
// add a page
$pdf->AddPage();
$pdf->SetY(150);
$pdf->SetX(20);
/*
// Print a text
$arr = array('jpg', 'jpeg', 'gif', 'png');
$pct_image_url = '';
//$pct_image_url = '../graphics/sponsors/'.$pct_linkcode.'.'.'png';
//$pct_image_url = '<img src="'.$pct_image_url.'" title="logo" />';
$ds = DIRECTORY_SEPARATOR;
foreach ($arr AS $type) {
    if (file_exists('..'.$ds.'graphics'.$ds.'sponsors'.$ds.$pct_linkcode.'.'.$type)) {
        $pct_image_url = '../graphics/sponsors/'.$pct_linkcode.'.'.$type;
        $pct_image_url = '<img src="'.$pct_image_url.'" title="logo" />';
    }
}
*/

$pct_image_url = '';
$ds = DIRECTORY_SEPARATOR;
if (file_exists('..'.$ds.'graphics'.$ds.'sponsors'.$ds.$pct_logo)) {
    $pct_image_url = '../graphics/sponsors/'.$pct_logo;
    $pct_image_url = '<img src="'.$pct_image_url.'" title="logo" />';
}
$tbl = <<<EOD
<table cellspacing="0" cellpadding="10" border="0">
    <tr>
        <td rowspan="2" colspan="2">$pct_image_url</td>
        <td colspan="5"><p style="font-family:Tahoma; font-weight: bold; font-size: 36px; color:#658fca; text-align: center;">$pct_compname</p></td>
    </tr>
    <tr>
        <td colspan="5"><p style="font-family:Arial; font-weight: bold; font-size: 26px; color:#166734; text-align: center">have planted $pct_numtrees trees to offset the carbon footprint of $pct_numtrees_thou paper cups produced for their customers, through an environmental programme which is operated by The Green Earth Appeal.</p></td>
    </tr>
</table>
EOD;
$pdf->writeHTML($tbl, true, false, true, false, '');

$pdf->SetY(875);
$pdf->SetX(290);
//$pdf->SetFont('times', '', 48);
$html3 = '<span style="font-size: 16px; color: #171213">'.date("jS F Y").'</span>';
$pdf->writeHTML($html3, true, false, true, false, '');
//Close and output PDF document
$ff = '../certification/cert_'.$pct_id.'.pdf';
$pdf->Output($ff, 'F');

//============================================================+
// END OF FILE
//============================================================+