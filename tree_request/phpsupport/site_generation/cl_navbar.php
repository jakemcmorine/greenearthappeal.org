<?php

class cl_navbar {

	var $path_to_graphics;
	var $in_sub_subectory;
	var $current_page;
	
	function cl_navbar($path_to_graphics, $in_sub_subdirectory, $current_page) {
		$this->path_to_graphics = $path_to_graphics;
		$this->in_sub_subdirectory = $in_sub_subdirectory;
		$this->current_page = $current_page;
		$this->dowork();
	}
	
	function dowork() {
	
		//Graphics
		$homeg 			= ($this->current_page <> "/index.php") 				? "off/home.jpg" 			: "on/home.jpg";
		$whyplantg 		= ($this->current_page <> "/why_plant_trees.php") 		? "off/whyplantrees.jpg" 	: "on/whyplantrees.jpg";
		$makepledgeg	= ($this->current_page <> "/make_the_pledges.php") 		? "off/makethepledges.jpg" 	: "on/makethepledges.jpg";
		$faqg			= ($this->current_page <> "/faq.php") 					? "off/faq.jpg" 			: "on/faq.jpg";
		$whoweareg		= ($this->current_page <> "/about_us.php") 				? "off/aboutus.jpg" 		: "on/aboutus.jpg";
        $foodg 		    = ($this->current_page <> "/foodforthought.php") 		? "off/foodforthought.jpg" 	: "on/foodforthought.jpg";
		$contactg 		= ($this->current_page <> "/contact_us.php") 			? "off/contactus.jpg" 		: "on/contactus.jpg";
		
		//Targets
		$homeu 			= ($this->in_sub_subdirectory == false) 					? "index.php" 					: "../index.php";
		$whyplantu 		= ($this->in_sub_subdirectory == false) 					? "why_plant_trees.php" 		: "../why_plant_trees.php";
		$makepledgeu	= ($this->in_sub_subdirectory == false) 					? "carbon_reduction/index.php" 	: "../carbon_reduction/index.php";
		$faqu			= ($this->in_sub_subdirectory == false)						? "faq.php" 					: "../faq.php";
		$whoweareu		= ($this->in_sub_subdirectory == false) 					? "about_us.php" 				: "../about_us.php";
		$foodu 		    = ($this->in_sub_subdirectory == false) 					? "http://foodforthought.greenearthappeal.org/" 			: "http://foodforthought.greenearthappeal.org/";
		$contactu 		= ($this->in_sub_subdirectory == false) 					? "contact_us.php" 				: "../contact_us.php";
	
		?><table border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td><a href="<?php echo $homeu; ?>"><img src="<?php echo $this->path_to_graphics; ?>nav/<?php echo $homeg; ?>" alt="Home" name="btn_home" width="109" height="45" id="btn_home" onmouseover="MM_swapImage('btn_home','','<?php echo $this->path_to_graphics; ?>nav/over/home.jpg',1)" onmouseout="MM_swapImgRestore()" border="0" /></a></td>
			<td><img src="<?php echo $this->path_to_graphics; ?>nav/off/seperator.jpg" width="1" height="45" /></td>
			<td><a href="<?php echo $makepledgeu; ?>"><img src="<?php echo $this->path_to_graphics; ?>nav/<?php echo $makepledgeg; ?>" alt="Make The Pledges" name="btn_makethepledge" width="178" height="45" id="btn_makethepledge" onmouseover="MM_swapImage('btn_makethepledge','','<?php echo $this->path_to_graphics; ?>nav/over/makethepledges.jpg',1)" onmouseout="MM_swapImgRestore()" border="0" /></a></td>
			<td><img src="<?php echo $this->path_to_graphics; ?>nav/off/seperator.jpg" width="1" height="45" /></td>
            <td><a href="<?php echo $whyplantu; ?>"><img src="<?php echo $this->path_to_graphics; ?>nav/<?php echo $whyplantg; ?>" alt="Why Plant Trees?" name="btnwhyplant" width="171" height="45" id="btnwhyplant" onmouseover="MM_swapImage('btnwhyplant','','<?php echo $this->path_to_graphics; ?>nav/over/whyplantrees.jpg',1)" onmouseout="MM_swapImgRestore()" border="0" /></a></td>
			<td><img src="<?php echo $this->path_to_graphics; ?>nav/off/seperator.jpg" width="1" height="45" /></td>
			<td><a href="<?php echo $foodu; ?>" target="blank"><img src="<?php echo $this->path_to_graphics; ?>nav/<?php echo $foodg; ?>" alt="Food For Thought" name="btn_links" width="172" height="45" id="btn_links" onmouseover="MM_swapImage('btn_links','','<?php echo $this->path_to_graphics; ?>nav/over/foodforthought.jpg',1)" onmouseout="MM_swapImgRestore()" border="0" /></a></td>
            <td><img src="<?php echo $this->path_to_graphics; ?>nav/off/seperator.jpg" width="1" height="45" /></td>
			<td><a href="<?php echo $faqu; ?>"><img src="<?php echo $this->path_to_graphics; ?>nav/<?php echo $faqg; ?>" alt="Frequently Asked Questions" name="btn_faq" width="98" height="45" id="btn_faq" onmouseover="MM_swapImage('btn_faq','','<?php echo $this->path_to_graphics; ?>nav/over/faq.jpg',1)" onmouseout="MM_swapImgRestore()" border="0" /></a></td>
			<td><img src="<?php echo $this->path_to_graphics; ?>nav/off/seperator.jpg" width="1" height="45" /></td>
			<td><a href="<?php echo $whoweareu; ?>"><img src="<?php echo $this->path_to_graphics; ?>nav/<?php echo $whoweareg; ?>" alt="About Us" name="btn_whoweare" width="126" height="45" id="btn_whoweare" onmouseover="MM_swapImage('btn_whoweare','','<?php echo $this->path_to_graphics; ?>nav/over/aboutus.jpg',1)" onmouseout="MM_swapImgRestore()" border="0" /></a></td>
			<td><img src="<?php echo $this->path_to_graphics; ?>/nav/off/seperator.jpg" width="1" height="45" /></td>
			<td><a href="<?php echo $contactu; ?>"><img src="<?php echo $this->path_to_graphics; ?>nav/<?php echo $contactg; ?>" alt="Contact Us" name="btn_contactus" width="139" height="45" id="btn_contactus" onmouseover="MM_swapImage('btn_contactus','','<?php echo $this->path_to_graphics; ?>nav/over/contactus.jpg',1)" onmouseout="MM_swapImgRestore()" border="0" /></a></td>
		  </tr>
		</table><?php
	}	
}
?>
