<?php

class cl_page {
	var $path_to_scripts;
	var $called_by;
	var $referrer;
	var $PATH_TO_GRAPHICS;
	
	
	function cl_page($path_to_scripts, $called_by, $path_to_graphics) {
		
		$this->PATH_TO_GRAPHICS = $path_to_graphics;
		
		require_once("cl_referrer.php");
		$this->referrer = new cl_referrer($this->PATH_TO_GRAPHICS);
		
		
		if (isset($_GET['ref'])) {
			$this->referrer->setReferrer($_GET['ref']);
		} else {
			if (!isset($_SESSION['refid'])) {
				$_SESSION['refid'] = 1;
			}
		}
	
		
		$this->path_to_scripts = $path_to_scripts;
		$this->called_by = $called_by;
	}
	
	function pg_top($in_sub_subdirectory, $title, $metadecr, $metakey, $shownav, $makesound = false) {
		if ($in_sub_subdirectory == true) {
			$top = "../";
		} else {
			$top = "";
		}
		
		if ($makesound == true) {
			$headfla = "greenearthappeal.swf";
		} else {
			$headfla = "greenearthappeal_nosound.swf";
		}
		?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="<?php echo $metadecr; ?>" />
        <meta name="keywords" content="<?php echo $metakey; ?>" />
        <title><?php echo $title; ?></title>
        <script type="text/javascript" src="<?php echo $top; ?>js_swfobj/swfobject.js"></script>
        <script type="text/javascript" src="<?php echo $top; ?>js_dw/dw_imgswap.js"></script>
        <script type="text/javascript" src="<?php echo $top; ?>carbon_reduction/dojo/dojo.js" djConfig="parseOnLoad:true, isDebug:false"></script>
        <script type="text/javascript" src="<?php echo $top; ?>carbon_reduction/js_support/user_stats.js"></script>
		<script type="text/javascript" src="<?php echo $top; ?>carbon_reduction/js_support/valid_email.js"></script>
        <script type="text/javascript" src="<?php echo $top; ?>carbon_reduction/js_support/valid_phone.js"></script>
        <script type="text/javascript" src="<?php echo $top; ?>carbon_reduction/js_support/slide_order.js"></script>
        <script type="text/javascript" src="<?php echo $top; ?>carbon_reduction/js_support/user_detail.js"></script>
        <script type="text/javascript" src="<?php echo $top; ?>carbon_reduction/js_support/user_referrals.js"></script>
        <script type="text/javascript" src="<?php echo $top; ?>carbon_reduction/js_support/valid_misc.js"></script>
        <link href="<?php echo $this->PATH_TO_GRAPHICS; ?>favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<link href="<?php echo $this->PATH_TO_GRAPHICS; ?>questionnaire.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $this->PATH_TO_GRAPHICS;; ?>sitestyles.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $this->PATH_TO_GRAPHICS; ?>specialinstances.css" rel="stylesheet" type="text/css" />
        </head>
        <script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script type="text/javascript">
		try {
		var pageTracker = _gat._getTracker("UA-12304193-1");
		pageTracker._trackPageview();
		} catch(err) {}</script>
        <body onload="MM_preloadImages('<?php echo $this->PATH_TO_GRAPHICS; ?>nav/over/home.jpg','<?php echo $this->PATH_TO_GRAPHICS; ?>nav/over/whyplantrees.jpg','<?php echo $this->PATH_TO_GRAPHICS; ?>nav/over/makethepledges.jpg','<?php echo $this->PATH_TO_GRAPHICS; ?>nav/over/faq.jpg','<?php echo $this->PATH_TO_GRAPHICS; ?>nav/over/aboutus.jpg','<?php echo $this->PATH_TO_GRAPHICS; ?>nav/over/links.jpg','<?php echo $this->PATH_TO_GRAPHICS; ?>nav/over/contactus.jpg','<?php echo $this->PATH_TO_GRAPHICS; ?>plantfreetrees/over.jpg')">
        <table width="1006" border="0" align="center" cellpadding="3" cellspacing="0">
          <tr>
            <td width="226"><img src="<?php echo $this->PATH_TO_GRAPHICS; ?>greenearthlogo_box.jpg" width="226" height="201" alt="Green Earth Appeal - Zero Carbon at Zero Cost" /></td>
            <td><div id="flashcontent">
  <img src="<?php echo $this->PATH_TO_GRAPHICS; ?>header_anim/background.jpg" />
</div><script type="text/javascript">
   var so = new SWFObject("<?php echo $this->PATH_TO_GRAPHICS; ?>header_anim/<?php echo $headfla; ?>", "mymovie", "767", "200", "8", "#FFFFFF");
   so.write("flashcontent");
</script></td>
          </tr>
          <?php
		  if ($shownav == true) {
		  	?><tr><td colspan="2"><?php
		  	require_once($this->path_to_scripts . "site_generation/cl_navbar.php");
			$nav = new cl_navbar($this->PATH_TO_GRAPHICS, $in_sub_subdirectory, $this->called_by);
			?></td></tr><?php
		  } ?>
          <tr>
    		<td colspan="2" valign="top" class="contentsection"><?php
		  	
	}
	
	function pg_end($in_sub_subdirectory, $shownav) {
		if ($in_sub_subdirectory == false) {
			$home = "index.php";
			$pledge = "carbon_reduction/index.php";
			$contact = "contact_us.php";
			$sponsored= "linkimgs/green_christmas_appeal_sponsor_logo.gif";
			$endorsed= "../linkimgs/plantforplanted_endorsed.gif";
			$privacypolicy= "privacy_policy.php";
		} else {
			$home = "../index.php";
			$pledge = "../carbon_reduction/index.php";
			$contact = "../contact_us.php";
			$sponsored= "../linkimgs/green_christmas_appeal_sponsor_logo.gif";
			$endorsed= "../linkimgs/plantforplanted_endorsed.gif";
			$privacypolicy= "../privacy_policy.php";
		}
			
		?></tr><?php
		if ($shownav==true) {
		  ?>
          <tr bgcolor="#E6E6E6">
            <td height="85" colspan="2">
            <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="basetext">
              <tr>
                <td width="33%" valign="middle" align="left">Copyright 2010 Green Earth Appeal<br />
                	<span class="privpol">Read our: <a href="<?php echo $privacypolicy; ?>">Privacy Policy</a></span></td>
                <td width="33%" valign="middle" align="center"><a href="http://www.greenchristmasappeal.org" target="_blank"><a href="http://www.unep.org/BILLIONTREECAMPAIGN/" target="_blank"><img src="<?php echo $endorsed; ?>" height="58" border="0"/></a></td>
                <td width="33%" valign="middle" align="right"><strong><a href="<?php echo $home; ?>">HOME</a> | <a href="<?php echo $pledge; ?>">MAKE THE PLEDGES</a> | <a href="<?php echo $contact; ?>">CONTACT US</a></strong></td>
              </tr>
            </table></td>
          </tr><?php
		}
		?>
        </table><?php
	}

	function linkToPledges($in_sub_subdirectory) {
		if ($in_sub_subdirectory == true) { $top = "../"; } else { $top = ""; }
		?><div id="Get10TreesFree" class="floatright">
        <a href="carbon_reduction/index.php">
        <img id="10freetrees" src="<?php echo $this->PATH_TO_GRAPHICS; ?>plantfreetrees/off.jpg" alt="Plant 10 Trees Free" onmouseover="MM_swapImage('10freetrees','','<?php echo $this->PATH_TO_GRAPHICS; ?>plantfreetrees/over.jpg',1)" onmouseout="MM_swapImgRestore()" border="0"/>
        
        </a>
        <br />
        <?php 
		$this->referrer->sponsorBox($_SESSION['refid'], $in_sub_subdirectory);
		?>
        </div><?php
	}
}
?>	