<?php
  class cl_image_functions {
      
      /* 
      COPYRIGHT NOTICE : 
        Copyright (C) 2011 Phil Mellor t/a Codigo Solutions. All rights reserved.
        This class may NOT be re-distributed, edited or published prior consent.
        For further information contact copyright@codigosolutions.com
      */
        
      
      var $image;           /*!< Holder for image */
      var $imageW;          /*!< Image width */
      var $imageH;          /*!< Image height */ 
      var $imageO;          /*!< Image orientation */ 
      var $imageF;          /*!< Image format JPEG/GIF/PNG */ 
      var $imageResized;    /*!< Holder for resized image */  
      
      function cl_image_functions() {
          //PURPOSE: Provide Image Support
          
          //Do memory limit raising
          $this->increase_mem_limit();
      }
      
      function increase_mem_limit() {
          //PURPOSE: Increases memory limit to max available on hosting platform
          @ini_set('memory_limit', '32M');
          @ini_set('memory_limit', '64M');
          @ini_set('memory_limit', '128M');
          @ini_set('memory_limit', '256M');
          @ini_set('memory_limit', '512M');
          @ini_set('memory_limit', '640M');
          @ini_set('memory_limit', '1024M');
      }
      
      function load($filename) {
          //PURPOSE: Load Image Into Memory
          //Load Image
          $image_info = getimagesize($filename);
          
          //Get Image Format & Load It
          $this->image_type = $image_info[2];
          if( $this->image_type == IMAGETYPE_JPEG ) {
             $this->imageF = ("JPEG");
             $this->image = imagecreatefromjpeg($filename);
          } elseif( $this->image_type == IMAGETYPE_GIF ) {
             $this->imageF = ("GIF");  
             $this->image = imagecreatefromgif($filename);
          } elseif( $this->image_type == IMAGETYPE_PNG ) {
              $this->imageF = ("PNG");  
             $this->image = imagecreatefrompng($filename);
          }
          
          //Get Sizes
          $this->imageW = imagesx($this->image);
          $this->imageH = imagesy($this->image);
          
          //Get Orientation (Portrait/Landscape)
          if ($this->imageH > $this->imageW) {
              $this->imageO = "P";
          } else {
              $this->imageO = "L";
          }
          
      }
      
      function getImageType() {
          if ($this->imageF=="JPEG") {
              return ".jpg";
          } else {
            return "." . strtolower($this->imageF);
          }
      }
      
      function doResize($maxWidth, $maxHeight) {
           //PURPOSE: Pubilc - Perform resize operation 
           if ($this->imageO == "P") {
               $this->_resizeToHeight($maxHeight);
           } else {
               $this->_resizeToWidth($maxWidth); 
           }
      }
      
      function _resizeToWidth($width) {
          //PURPOSE Resize according to width
          $ratio = $this->imageW / $this->imageH;
          $height = $width / $ratio;
          $this->_resize($width, $height);
      }
      
      function _resizeToHeight($height) {
          //PURPOSE Resize according to height 
          $ratio = $this->imageH / $this->imageW; 
          $width = $height / $ratio;
          $this->_resize($width, $height);
      }
      
      function _resize($width, $height) {
          //PURPOSE: Perform Resize Action
          $new_image = imagecreatetruecolor($width, $height);
          imagecopyresampled($new_image, $this->image, 0,0,0,0, $width, $height, $this->imageW, $this->imageH);
          $this->imageResized = $new_image;
      }
      
      function output($filename=null, $destroy=false) {
          //PURPOSE Output new image
          $result = false;
          if ($filename==null) {
             if ($this->_outputToScreen()) { $result = true; }
          } else {
              $filename = $this->_outputToFile($filename);
              $result = $filename;
          }
          
          //Destroy Resized Image as its been used
          imagedestroy($this->imageResized);
          if ($destroy) {
              //Destroy the original
              imagedestroy($this->image);
          }
          
          return $result;
      }
      
      function _outputToScreen() {
          //PURPOSE: Output the image to screen
          if ($this->imageF=="JPEG") {
              header('Content-type: image/jpeg');
              imagejpeg($this->imageResized);
          } elseif ($this->imageF=="GIF") { 
              header('Content-type: image/gif');
              imagegif($this->imageResized);
          } elseif ($this->imageF=="PNG") { 
              header('Content-type: image/png');
              imagepng($this->imageResized);
          }
          return true; 
      }
      
      function _outputToFile($filename) {
         //PURPOSE: Output the image to file
         $result = false;
          if ($this->imageF=="JPEG") {
              if (imagejpeg($this->imageResized, $filename . ".jpg", 100)) { return $filename . ".jpg";  }
          } elseif ($this->imageF=="GIF") { 
              if (imagegif($this->imageResized, $filename . ".gif")) { return $filename . ".gif";  }
          } elseif ($this->imageF=="PNG") { 
              if (imagepng($this->imageResized, $filename . ".png", 9)) { return $filename . ".png";  } 
          } 
      }
      
      function _watermark() {
          //PURPOSE Watermark image with text/logo
          //TODO
      }  
      
  }
?>
