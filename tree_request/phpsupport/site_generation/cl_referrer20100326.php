<?php
class cl_referrer {

	var $db;
	var $PATH_TO_GRAPHICS;
	
	function cl_referrer($path_to_graphics) {
		require_once("cl_db.php");
		$this->PATH_TO_GRAPHICS = $path_to_graphics;
		$this->db = new cl_db();
	}
	
	function setReferrer($pRef) {
		//Clean String
		$pRef = $this->db->cleanString($pRef);
		
		//Do query
		$sql = "SELECT id FROM t_sponsors WHERE scode='{$pRef}'";
		$q = mysql_query($sql, $this->db->cn);
		if (mysql_num_rows($q)>0) {
			$r = mysql_fetch_array($q);
			$_SESSION['refid'] = $r['id'];
			$this->incrementHit($r['id']);
		} else {
			$_SESSION['refid'] = 0;
			$this->incrementHit(0);
		}
	}
	
	function reverseReferrer($pID) {
		//Clean String
		$pID = $this->db->cleanString($pID);
		
		//Do Query
		$sql = "SELECT scode FROM t_sponsors WHERE id='{$pID}'";
		return mysql_fetch_array(mysql_query($sql, $this->db->cn));
	}
	
	
	
	function sponsorBox($pID, $is_in_subdirectory) {
		//Clean String
		$pID = $this->db->cleanString($pID);
	
		//Do query
		$sql = "SELECT sname, slogo, shome, slogotext FROM t_sponsors WHERE id='{$pID}' LIMIT 0,1";
		$q = mysql_query($sql, $this->db->cn);
		if (mysql_num_rows($q)>0) {
			$r = mysql_fetch_array($q);
			?>
			<table class="sponsorbox" width="326" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="3">
			  <tr>
				<td align="left"><img src="<?php echo $this->PATH_TO_GRAPHICS; ?>sponsors/sponsored_by_green.gif" alt="Sponsored By" width="97" height="10" /></td>
			  </tr>
			  <tr>
				<td align="center"><a href="<?php echo $r['shome']; ?>" target="_blank"><img src="<?php echo $this->PATH_TO_GRAPHICS; ?>sponsors/<?php echo $r['slogo']; ?>" alt="<?php echo $r['sname']; ?>" border="0" /></a></td>
			  </tr>
              <?php if (strlen($r['slogotext'])>1) {
			  	?>
                <tr>
					<td align="center"><?php echo $r['slogotext']; ?></td>
			    </tr>
                <?php
			} ?>
			</table></td>
			  </tr>
			</table>
			<?php
		} else {
			//Default to Green Christmas Appeal
			$sql = "SELECT sname, slogo, shome FROM t_sponsors WHERE id='1' LIMIT 0,1";
			$q = mysql_query($sql, $this->db->cn);
			$r = mysql_fetch_array($q);
			?>
			<table class="sponsorbox" width="326" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="3">
			  <tr>
				<td align="left"><img src="<?php echo $this->PATH_TO_GRAPHICS; ?>sponsors/sponsored_by_green.gif" alt="Sponsored By" width="97" height="10" /></td>
			  </tr>
			  <tr>
				<td align="center"><a href="<?php echo $r['shome']; ?>" target="_blank"><img src="<?php $this->PATH_TO_GRAPHICS; ?>sponsors/<?php echo $r['slogo']; ?>" alt="<?php echo $r['sname']; ?>" border="0" /></a></td>
			  </tr>
			</table></td>
			  </tr>
			</table>
			<?php
		}
	}
	
	function FPsponsor($pID) {
		//Clean String
		$pID = $this->db->cleanString($pID);
	
		//Do query
		$sql = "SELECT sname, slogo, shome, slogotext FROM t_sponsors WHERE id='{$pID}' LIMIT 0,1";
		$q = mysql_query($sql, $this->db->cn);
		if (mysql_num_rows($q)>0) {
			$r = mysql_fetch_array($q);
			?>
            <div class="sponsorbox">
            <p><img src="http://www.greenearthappeal.org/graphics/sponsors/sponsored_by_green.gif" alt="Sponsored By" /></p>
            <p><a href="<?php echo $r['shome']; ?>" target="_blank"><img src="<?php echo $this->PATH_TO_GRAPHICS . $r['slogo']; ?>" alt="<?php echo $r['sname']; ?>" border="0"/></a></p>
            <?php if (strlen($r['slogotext'])>1) {
			  	?>
                <p><?php echo $r['slogotext']; ?></p>
                <?php
			} ?>
            </div>
			<?php
		} 
	}
	
	function incrementHit($iSponsorID) {
		$sql = "UPDATE t_sponsors_stats SET hits=hits+1 WHERE dpoint=CURDATE() AND sid={$iSponsorID}";
		mysql_query($sql, $this->db->cn);
		if (mysql_affected_rows($this->db->cn)<1) {
			$sql = "INSERT INTO t_sponsors_stats SET hits=1, dpoint=CURDATE(), sid={$iSponsorID}";
			mysql_query($sql, $this->db->cn);
		}
	}

} // End Class
?>