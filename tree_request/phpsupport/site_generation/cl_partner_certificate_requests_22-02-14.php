<?php

class cl_partner_certificate_requests {

    var $cn;
    var $cn_state = 0;
    var $p;
    var $s;
    var $g;
    var $f;
    var $path_to_scripts;
    var $debug = false;
    
    var $file_max_size = 5242880;
    var $file_allowable_types = "gif|jpeg|jpg|pjeg|png";
    var $file_max_width_px = "200";
    var $file_max_height_px = "200";
    var $update_notice = ""; 

    function cl_partner_certificate_requests($p, $s, $g, $f, $path_to_scripts) {
        
        if ($this->debug) {
           error_reporting(E_ALL);
           ini_set('display_errors', '1'); 
        }
        
        $this->p = $p;
        $this->s = $s;
        $this->g = $g;
        $this->f = $f;
        $this->path_to_scripts = $path_to_scripts;

        require_once($this->path_to_scripts . "/site_generation/cl_db.php");
        $this->cn = new cl_db();
        
        if ($s==null && $p==null) {
            $this->top(false);
            $this->loginform(null, null);
            $this->bottom();
        } else {
            if (isset($p['uname']) && isset($p['pword'])) {
                if ($this->validateUser($p['uname'], $p['pword'])) {
                    $_SESSION['u'] = $p['uname'];
                    $this->s = $p['uname'];
                    $this->top(true);
                    ?><h1>Welcome</h1>
                    <p>Thanks for signing into Green Earth Appeal's Corporate Partner Tree Request System.</p>
                    <?php if ($this->update_notice=="") {
                        ?><p style="color:#006600; font-weight:bold;">System is operating normally.</p><?php
                    } else {
                        ?><p style="color:#990000; font-weight:bold;"><?php echo $this->update_notice; ?></p><?php
                    } ?>
                    <p><a href="index.php"><img src="imgs/btn_continue.png" alt="Continue"></a></p><?php  
                    $this->bottom(); 
                } else {
                    $this->top(false); 
                    $this->loginform($p['uname'], $p['pword'], "Credentials rejected - please try again.");
                    $this->bottom();  
                }
            } else {
                $done = false;
                if (isset($this->p['a'])) {
                    switch ($this->p['a']) {
                        case "bulk.approve":
							$this->top(true);
                            $this->makeBulkOrder();
                            $this->bottom();
                            $done = true;
                            break;
						case "bulk":
							$this->top(true);
                            $this->showListFromFile();
                            $this->bottom();
                            $done = true;
                            break;	
						case "company":
                            $this->top(true);
                            $this->addUpdatePartnerCustomer($this->f);
                            $this->bottom();
                            $done = true;
                            break;
                        case "tree.request":
                            $this->top(true); 
                            $this->attachTreeRequest();
							$this->createCounterImage();
                            $this->bottom();
                            $done = true;
                            break;
                    }
                }

                if (isset($this->g['a'])) {
                    switch ($this->g['a']) {
                        case "bulk.upload":
                            $this->top(true);
                            $this->uploadCSV(); 
                            $this->bottom(); 
                            $done = true;
                            break;
						case "company.add":
                            $this->top(true);
                            $this->customerForm(); 
                            $this->bottom(); 
                            $done = true;
                            break;
                        case "company.edit":
                            if ($this->checkChangesAllowed($this->s['u'], $this->g['id'])) {
                                $this->top(true); 
                                $sql = "SELECT * FROM t_partner_cert_companies WHERE id=" . $this->cn->safe($g['id']);
                                $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
                                $this->customerForm($r);
                                $this->bottom();
                                $done=true; 
                            } else {
                                //Naughty - attempt to break security
                                die();
                            }
                            break;
                        case "company.order":
                            $this->top(true); 
                            $this->treeForm(null, $this->g['id']);
                            $this->bottom();
                            $done=true;
                            break;
                        case "randcode":
                            echo $this->generateLinkCode();
                            break;
                        case "logout":
                            $this->top(false);
                            unset($this->s['u']);
                            unset($_SESSION['u']);
                            session_destroy();
                            $this->loginform("", "", "You have been logged out.");   
                            $this->bottom();
                            $done=true;
                        default:
                            //echo "DEFAULT HIT";
                            break;
                    }
                }

                if (!$done) {
                    $this->top(true);
                    $this->listPartnersCustomers();  
                    $this->bottom(); 
                }
            }
        }

        if ($this->debug) {
            echo "<hr /><h3>GET:</h3>";
            var_dump($this->g);
            echo "<hr /><h3>POST:</h3>";
            var_dump($this->p);
            echo "<hr /><h3>SESSION:</h3>";
            var_dump($this->s);
            echo "<hr /><h3>FILES:</h3>";
            var_dump($this->f);
        }
    }

    function top($loggedin=false) {
        ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Green Earth Appeal</title>
        <style type="text/css">
        <!--
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            
        }
        #content{
            width:780px;
            margin-left: auto;
            margin-right:auto;
            margin-top:20px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #333333;
        }
        .odd {
            background-color: #CDE0CD;
        }
        .even {
            background-color: #EFF5EF;
        }
        .header {
            background-color: #5A965A;
            font-weight: bold;
            color: #FFFFFF;
        }
        .formpart {
            width: 500px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
        }
        .formparttitle {
            width: 280px; 
        }
        .loginpart {
            width: 300px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
        }
        .loginparttitle {
            width: 100px; 
        }
        -->
        </style>
        </head>

        <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="imgs/logo.png" width="204" height="96" /></td>
            <td align="right" style="padding-right:20px;"><img src="imgs/title.png" width="538" height="29" /><br />
              <br />
        <?php if ($loggedin) { ?><table cellpadding="0" cellspacing="0">
          <tr><td><a href="index.php"><img src="imgs/main_page.png" width="94" height="28" border="0" /></a></td>
        <td><a href="index.php?a=logout"><img src="imgs/logout.png" width="77" height="28" /></a></td>
        </tr></table><?php } ?>
          </tr>
        </table>
        <div id="content">
        <?php
    }
    
    function bottom() {
        ?></div>
        </body>
        </html><?php
    }

    function loginform ($username, $password, $error=null) {
        ?><h1>Please Log In</h1>
        <?php if ($error<>null) { echo $error; } ?>
        <form name="front-door" method="POST" action="index.php">
        <table>
            <tr>
                <td class="loginparttitle">Username</td>
                <td><input class="loginpart" type="text" name="uname" value="<?php if ($username<>null) { echo $username; } ?>" />
            </tr>
            <tr>
                <td>Password</td>
                <td><input class="loginpart" type="password" name="pword" value="<?php if ($password<>null) { echo $password; } ?>" />
            </tr>
            <tr>
                <td></td>
                <td><input type="image" src="imgs/btn_login.png" name="button" value="Log In" />
            </tr>
        </table>
        </form>
        <?php
    }

    function validateUser($username, $password) {
        $sql = "SELECT COUNT(*) FROM t_partner_accounts WHERE account_email=" . $this->cn->safe($username) . " AND account_password=MD5(" . $this->cn->safe($password) . ")";
        $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
        if ($r[0]==1) {
            return true;
        } else {
            return false;
        }
    }

    function changePassword($username, $password, $old, $new, $repeated) {
        $error = "";
        if (strlen($new)<6) {
            $error = "New password was too short. Please choose a longer one.";
        } else {
            if ($new <> $repeated) {
                $error = "New password and repeated passwords were not the same";
            } else {
                if (!$this->validateuser($username, $password)) {
                    $error = "Existing password was incorrect";
                } else {
                    $sql = "UPDATE t_partner_accounts SET account_password=MD5(" . $this->cn->safe($new) . ")";
                    mysql_query($sql, $this->cn->get());
                    if (mysql_affected_rows($this->cn->get())>0) {
                        return array(true, null);
                    } else {
                        return array(false, $error);
                    }
                }
            }
        }
    }


	function makeBulkOrder(){
		$done = 0; $pr ='';
		foreach($this->p['vals'] as $kval => $cval){
			$rec_id = $cval['id'];
			$cust_id = $cval['clid'];
			$trees_num = $cval['trees_ord'];
			
             $sql = "SELECT COUNT(*) FROM t_partner_cert_requests WHERE custaccountid=" . $this->cn->safe($rec_id);
             $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
             if ($r[0]>0) { $new = false; } else { $new = true; }
			 
             $sqlIns = "INSERT INTO t_partner_cert_requests SET numtrees=" . $this->cn->safe($trees_num) . ", custaccountid=" . $this->cn->safe($rec_id). ", date=NOW()";
             if ($this->debug) { echo "<br>" . $sqlIns; }
             mysql_query($sqlIns, $this->cn->get());
             if (mysql_affected_rows($this->cn->get())>0) {
                 //Success
				 $done ++;
                 
                 if ($cust_id > 0) {
                    $sql = "SELECT companycode, compname, partner_id FROM t_partner_cert_companies WHERE id=" . $this->cn->safe($rec_id); 
                    $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
                    $pct_linkcode = $r[0];
                    $pct_compname = $r[1];
                    $pct_id = $rec_id;
                    
                    // get number of trees
                    $sql = "SELECT SUM(numtrees) FROM t_partner_cert_requests WHERE custaccountid=$pct_id";
                    $r2 = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
                    $pct_numtrees = $r2[0];
                    $pct_numtrees_thou = number_format($pct_numtrees*1000);

                    // get logo partner
                    $sql2   = 'SELECT ts.slogo AS slogo FROM t_partner_accounts AS tpa
                                INNER JOIN t_sponsors AS ts
                                ON tpa.sponsor_id=ts.id
                                WHERE tpa.id='.$r[2];
                    $result = mysql_fetch_array(mysql_query($sql2, $this->cn->get()));
                    $pct_logo = $result[0];
                    ob_start();
                    //require_once('cert.php');
                    ob_end_clean();
                    $this->doInformationMail($new, $rec_id, $trees_num, true);
                 } else {
                    $this->doInformationMail($new, $rec_id, $trees_num);   
                 }                 
                
                $pr = '<p>The bulk orders have been placed with Green Earth Appeal on behalf of your customer.</p>';
                
             } else {
                 
                $pr ='<p>The order for trees could not be saved. Please go back and try again.</p>';
               
             } 
			 }
			 
    	      if (mysql_affected_rows($this->cn->get())>0) { ?>
				<h1>Success</h1>
                <?php echo $pr; ?>
                <p><a href="index.php">Back to Main Page</a></p>
		<?php  } else {  ?>
        		<h1>Error</h1>
                <?php echo $pr; ?>
                <p><a href="javascript:history.go(-1)">Go Back</a></p>
        <?php } // else ends 
		
	} // func ends 


	function showListFromFile(){
		$errors = array();
		if (!isset($this->f['csv_file']) || $this->f['csv_file']=="") { array_push($errors, "CSV File is missing"); }
		if ($this->f['csv_file'] != '') {
			if ($this->f["csv_file"]["error"] == 0){
				
				$csv_name = basename($this->f["csv_file"]["name"]);
				$csv_type = $this->f["csv_file"]["type"];
				$csv_size = $this->f["csv_file"]["size"];
				$csv_temp = $this->f["csv_file"]["tmp_name"];
				$file_dir = 'upload';
				$file_path = $file_dir.'/'.$csv_name;
				//if ($csv_type == 'application/vnd.ms-excel'){ array_push($errors, "File should be in CSV format"); }
				move_uploaded_file($csv_temp, $file_path);
				
				// reading file
				$csv_data = $this->readCSV($file_path);
				//echo "<pre>"; print_r($csv_data); ?>
				
				
                <table width="100%" cellspacing="0">
                    <tbody>
                    <form action="index.php" name="bulk_order" method="post">
                    	<tr height="20">
                    		<td width="50%">
                    			<h1>Bulk Orders</h1>
                    		</td><td width="50%" align="right">&nbsp;<!--<a href="index.php?a=company.add"><img border="0" src="imgs/btn_approve.png"></a>--></td>
                        </tr>
                		<tr><td colspan="2">
                        <table width="780" cellspacing="0" cellpadding="10">
                        <tr class="header"><td>Client ID</td><td>Company Name</td><td>Company Code</td><td align="center">Trees Ordered</td></tr>
				<?php
				$cnt =0; $i =0;
				foreach($csv_data as $kdata => $cdata){
					$clid = $cdata[0];
					$trees_ordered = $cdata[1];
					if(trim($clid) != '') {
						$sql = "SELECT id, comp_id, compname, companycode FROM t_partner_cert_companies WHERE comp_id='" . $clid. "'"; 
						$row_data = mysql_query($sql, $this->cn->get());
				?>
						<?php
                        while ($rows = mysql_fetch_array($row_data)) { ?>
							<input type="hidden" name="vals[<?php echo $cnt; ?>][clid]" value="<?php echo $clid; ?>"  />
                        <input type="hidden" name="vals[<?php echo $cnt; ?>][id]" value="<?php echo $rows['id'] ?>"  />
                        <input type="hidden" name="vals[<?php echo $cnt; ?>][trees_ord]" value="<?php echo $trees_ordered; ?>"  />
                        <input type="hidden" name="a" value="bulk.approve"  />
                       <?php     if ($i%2 == 0) { $class="odd"; } else { $class="even"; }
                            ?><tr class="<?php echo $class; ?>"><td width="20%"><?php echo $rows['comp_id']; ?></td><td width="50%"><?php echo $rows['compname']; ?></td>                         <td width="15%"><?php echo $rows['companycode']; ?></td><td width="15%" align="center"><?php echo $trees_ordered; ?></td>
                            </tr><?php
                        }
                        ?>
			<?php	
					} // if id exists
					$cnt++;
					$i++;
				} // foeach ends  ?>
                </table>
                		</td>
                	</tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr><td colspan="2" align="center"><input type="image" src="imgs/btn_approve.png" height="46" width="780" value="Approve" /></td></tr>
                </form>
                </tbody>
                </table>
			<?php }
		}
		
	} // func ends 



	function readCSV($csvFile){
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}



    function addUpdatePartnerCustomer($files) {
        //Do checks
        $errors = array();
        if (!isset($this->p['compname']) || $this->p['compname']=="") { array_push($errors, "Company Name is missing"); }
        if (!isset($this->p['compaddress1']) || $this->p['compaddress1']=="") { array_push($errors, "Company address missing"); }
        if (!isset($this->p['comppostcode']) || $this->p['comppostcode']=="") { array_push($errors, "Company postcode is missing"); }
        if (!isset($this->p['contactforename']) || $this->p['contactforename']=="") { array_push($errors, "Contact forename is missing"); }
        if (!isset($this->p['contactsurname']) || $this->p['contactsurname']=="") { array_push($errors, "Contact surname is missing"); }
        if (!isset($this->p['contactemail']) || $this->p['contactemail']=="") { array_push($errors, "Contact email is missing"); }
        if (!isset($this->p['contacttelephone']) || $this->p['contacttelephone']=="") { array_push($errors, "Contact telephone is missing"); }

        if (isset($this->p['id'])) {
            //This is an update - check changes are allowed
            if ($this->checkChangesAllowed($this->s['u'], $this->p['id'])) {
                $sql = "UPDATE t_partner_cert_companies SET compname=" . $this->cn->safe($this->p['compname']) . ", compaddress1=" . $this->cn->safe($this->p['compaddress1']) . ",
                        compaddress2=" . $this->cn->safe($this->p['compaddress2']) . ", compaddress3=" . $this->cn->safe($this->p['compaddress3']) . ",
                        comppostcode=" . $this->cn->safe($this->p['comppostcode']) . ", contacttitle=" . $this->cn->safe($this->p['contactitle']) . ",
                        contactforename=" . $this->cn->safe($this->p['contactforename']) . ", contactsurname=" . $this->cn->safe($this->p['contactsurname']) . ",
                        contacttelephone=" . $this->cn->safe($this->p['contacttelephone']) . ", contactemail=" . $this->cn->safe($this->p['contactemail']) . ",
                        compweb=" . $this->cn->safe($this->p['compweb']) . ", contactnotes=" . $this->cn->safe($this->p['contactnotes']) . ",
                        facebook_url =" .$this->cn->safe($this->p['fburl']). ", twitter_url =" .$this->cn->safe($this->p['twurl']). ", comp_id =" .$this->cn->safe($this->p['comp_id']). ", updated_date=NOW() WHERE id=" . $this->cn->safe($this->p['id']);
                if ($this->debug) { echo $sql; }
                mysql_query($sql, $this->cn->get());
                if (mysql_affected_rows($this->cn->get())>0) {
                    //Get The companies linkcode
                    $sql = "SELECT companycode FROM t_partner_cert_companies WHERE id=" . $this->cn->safe($this->p['id']); 
                    $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
                    $linkcode = $r[0];
                    
                    if (isset($this->f['artwork']['name']) && $this->f['artwork']['name']<>"") {
                     //Get any existing filename
                     $sql = "SELECT companyartwork FROM t_partner_cert_companies WHERE companycode='{$linkcode}'";
                     $q = mysql_query($sql, $this->cn->get());
                     if (mysql_num_rows($q)>0) {
                         $r = mysql_fetch_array($q);
                         if ($r[0]<>"") { $old_name = $r[0]; } else { $old_name=""; }
                     } else {
                         $old_name="";
                     }                     
                     //We don't resize or anything with this file - its artwork
                     if ($this->f['artwork']['size'] < $this->file_max_size) {
                         //Get file suffix
                         $parts = explode(".", $this->f['artwork']['name']);
                         $suffix = $parts[count($parts)-1];
                           if (move_uploaded_file($this->f['artwork']['tmp_name'], "company_artwork/" . $linkcode . "." . $suffix)) {
                                 $sql = "UPDATE t_partner_cert_companies SET companyartwork=" . $this->cn->safe($linkcode . "." . $suffix) . " WHERE companycode='{$linkcode}'";
                                 if ($this->debug) {echo $sql; }  
                                 mysql_query($sql, $this->cn->get());
                             }
                         
                     }
                     if ($old_name<>"" && $old_name<>$linkcode . "." . $suffix) {
                         if ($this->debug) { echo "<br />Deleting file {$old_name}"; }
                         unlink("company_artwork/" . $old_name);
                     }
                     
                 }
                    //Are there images attached?
                    if (isset($this->f['file']['name']) && $this->f['file']['name']<>"") {
                        if ($this->debug) { echo "<br/>File set"; } 
                        //Resize and save image, keeping the orginal
                        if ($this->f['file']['error']>0) {
                            //There was an error with the uploaded file, but record was saved.
                            ?><h1>Partial Success</h1>
                            <p>New account has been added however an error prevented the file from being uploaded properly.</p><?php
                        } else {
                            if ($this->debug) { echo "<br/>File - no error"; }   
                            if (!$this->_helper_filetypeallowed($this->f['file']['type'])) {
                                //File type not allowed by system
                                ?><h1>Error</h1>
                                <p>The file was of a type not recognised. Please go back to the main page and edit the company using a jpg/gif or png image.</p>
                                <p><a href="index.php">Back to Main Page</a></p>
                                <?php  
                            } else {
                                if ($this->debug) { echo "<br/>File type allowed"; }   
                                if ($this->f['file']['size'] > $this->file_max_size) {
                                    //File is too big
                                    ?><h1>Error</h1>
                                    <p>The file size was too big to continue. Please go back to the main page and edit the company using a smaller file size.</p>
                                    <p><a href="index.php">Back to Main Page</a></p>
                                    <?php 
                                } else {
                                    if ($this->debug) { echo "<br/>File size fine"; }   
                                    //Checks done, so on with the show
                                    $type = explode("/", $this->f['file']['type']);
                                    $type = $type[1];
                                    if (!move_uploaded_file($this->f['file']['tmp_name'], "temp/" . $linkcode . "." . $type)) { echo "Could not move file"; }    
                                    //Now do resize
                                    require_once($this->path_to_scripts . "/site_generation/cl_image_functions.php");
                                    $imgc = new cl_image_functions();
                                    $imgc->load("temp/" . $linkcode . "." . $type);
                                    $imgc->doResize($this->file_max_width_px, $this->file_max_height_px);
                                    $filename = $imgc->output("../graphics/sponsors/" . $linkcode, true); 
                                    //Update sponsor main table
                                    if ($this->updateSponsors($linkcode, $this->p['compname'], $filename, $this->p['compweb'])>0) {
                                        //Updated/Added
                                        ?><h1>Success</h1>
                                        <p>The account has been updated. The account's URL is <a href="http://www.greenearthappeal.org/?ref=<?php echo $linkcode; ?>">http://www.greenearthappeal.org/?ref=<?php echo $linkcode; ?></a></p>
                                        <p><a href="index.php">Back to Main Page</a></p><?php
                                    } else {
                                        //Something Broke
                                        ?><h1>Partial Success</h1>
                                        <p>The accounthas been added however an error prevented a changes being made to GreenEarthAppeal.org</p>
                                        <p><a href="index.php">Back to Main Page</a></p><?php
                                    }                                                 
                                }
                            }
                        }
                    } else {
                        $this->updateSponsors($linkcode, $this->p['compname'], "", $this->p['compweb']);  
                        //Do we set to the default logo to the account top>
                        $sql = "SELECT slogo FROM t_sponsors WHERE scode='{$linkcode}'";
                        $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
                        if ($r[0]=="") {
                            //Get Account Top 
                            $sql = "SELECT sponsor_id FROM t_partner_accounts WHERE account_email=" . $this->cn->safe($this->s['u']);
                            $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
                            $sql = "SELECT slogo FROM t_sponsors WHERE id='{$r[0]}'";
                            $q = mysql_query($sql, $this->cn->get());
                            if (mysql_num_rows($q)>0) {
                                $r = mysql_fetch_array($q);
                                if ($r[0]<>"") {
                                    $this->updateSponsors($linkcode, $this->p['compname'], $r[0], $this->p['compweb']); 
                                }
                            }
                        } else {
                            //Do not change logo
                        }
                        ?><h1>Success</h1>
                        <p>The account has been updated. The account's URL is <a href="http://www.greenearthappeal.org/?ref=<?php echo $linkcode; ?>">http://www.greenearthappeal.org/?ref=<?php echo $linkcode; ?></a></p>
                        <p><a href="index.php">Back to Main Page</a></p><?php
                    }

                } else {
                    ?><h1>Error</h1>
                    <p>The account could not be updated. Please go back and try again.</p>
                    <p><a href="javascript:history.go(-1)">Click here to to back</a></p><?php
                }
            } else {
                die("Authentication Error");
            }
        } else {
             $linkcode = $this->generateLinkCode();
             $sql = "INSERT INTO t_partner_cert_companies SET compname=" . $this->cn->safe($this->p['compname']) . ", compaddress1=" . $this->cn->safe($this->p['compaddress1']) . ",
                        compaddress2=" . $this->cn->safe($this->p['compaddress2']) . ", compaddress3=" . $this->cn->safe($this->p['compaddress3']) . ",
                        comppostcode=" . $this->cn->safe($this->p['comppostcode']) . ", contacttitle=" . $this->cn->safe($this->p['contactitle']) . ",
                        contactforename=" . $this->cn->safe($this->p['contactforename']) . ", contactsurname=" . $this->cn->safe($this->p['contactsurname']) . ",
                        contacttelephone=" . $this->cn->safe($this->p['contacttelephone']) . ", contactemail=" . $this->cn->safe($this->p['contactemail']) . ",
                        compweb=" . $this->cn->safe($this->p['compweb']) . ", contactnotes=" . $this->cn->safe($this->p['contactnotes']) . ",						facebook_url =" .$this->cn->safe($this->p['fburl']). ", twitter_url =" .$this->cn->safe($this->p['twurl']). ", comp_id =" .$this->cn->safe($this->p['comp_id']). ", updated_date=NOW(), added_date=NOW(), partner_id=" . $this->getPartnerID() . ", companycode='{$linkcode}'";
             if ($this->debug) { echo $sql; }           
             mysql_query($sql, $this->cn->get());
             if (mysql_affected_rows($this->cn->get())>0) {
                 
                 if (isset($this->f['artwork']['name']) && $this->f['artwork']['name']<>"") {
                     //Get any existing filename
                     $sql = "SELECT companyartwork FROM t_partner_cert_companies WHERE companycode='{$linkcode}'";
                     $q = mysql_query($sql, $this->cn->get());
                     if (mysql_num_rows($q)>0) {
                         $r = mysql_fetch_array($q);
                         if ($r[0]<>"") { $old_name = $r[0]; } else { $old_name=""; }
                     } else {
                         $old_name="";
                     }                     
                     //We don't resize or anything with this file - its artwork
                     if ($this->f['artwork']['size'] < $this->file_max_size) {
                         //Get file suffix
                         $parts = explode(".", $this->f['artwork']['name']);
                         $suffix = $parts[count($parts)-1];
                           if (move_uploaded_file($this->f['artwork']['tmp_name'], "company_artwork/" . $linkcode . "." . $suffix)) {
                                 $sql = "UPDATE t_partner_cert_companies SET companyartwork=" . $this->cn->safe($linkcode . "." . $suffix) . " WHERE companycode='{$linkcode}'";
                                 if ($this->debug) {echo $sql; }  
                                 mysql_query($sql, $this->cn->get());
                             }
                     }
                     if ($old_name<>"" && $old_name<>$linkcode . "." . $suffix) {
                         unlink("company_artwork/" . $old_name);
                     }
                 }
                  if (isset($this->f['file']['name']) && $this->f['file']['name']<>"") {
                        if ($this->debug) { echo "<br/>File set"; } 
                        //Resize and save image, keeping the orginal
                        if ($this->f['file']['error']>0) {
                            //There was an error with the uploaded file, but record was saved.
                            ?><h1>Partial Success</h1>
                            <p>New account has been added however an error prevented the file from being uploaded properly.</p>
                            <p><a href="index.php">Back to Main Page</a></p><?php
                        } else {
                            if ($this->debug) { echo "<br/>File - no error"; }   
                            if (!$this->_helper_filetypeallowed($this->f['file']['type'])) {
                                //File type not allowed by system
                                ?><h1>Error</h1>
                                <p>The file was of a type not recognised. Please go back to the main page and edit the company using a jpg/gif or png image.</p>
                                <p><a href="index.php">Back to Main Page</a></p>
                                <?php    
                            } else {
                                if ($this->debug) { echo "<br/>File type allowed"; }   
                                if ($this->f['file']['size'] > $this->file_max_size) {
                                    //File is too big
                                    ?><h1>Error</h1>
                                    <p>The file size was too big to continue. Please go back to the main page and edit the company using a smaller file size.</p>
                                    <p><a href="index.php">Back to Main Page</a></p>
                                    <?php   
                                } else {
                                    if ($this->debug) { echo "<br/>File size fine"; }   
                                    //Checks done, so on with the show
                                    $type = explode("/", $this->f['file']['type']);
                                    $type = $type[1];
                                    if (!move_uploaded_file($this->f['file']['tmp_name'], "temp/" . $linkcode . "." . $type)) { echo "Could not move file"; }
                                    //Now do resize
                                    require_once($this->path_to_scripts . "/site_generation/cl_image_functions.php");
                                    $imgc = new cl_image_functions();
                                    $imgc->load("temp/" . $linkcode . "." . $type);
                                    $imgc->doResize($this->file_max_width_px, $this->file_max_height_px);
                                    $filename = $imgc->output("../graphics/sponsors/" . $linkcode, true); 
                                    //Update sponsor main table
                                    if ($this->updateSponsors($linkcode, $this->p['compname'], $filename, $this->p['compweb'])>0) {
                                        //Updated/Added
                                        ?><h1>Success</h1>
                                        <p>New account has been added. The account's URL is <a href="http://www.greenearthappeal.org/?ref=<?php echo $linkcode; ?>">http://www.greenearthappeal.org/?ref=<?php echo $linkcode; ?></a></p>
                                        <p><a href="index.php">Back to Main Page</a></p><?php
                                    } else {
                                        //Something Broke
                                        ?><h1>Partial Success</h1>
                                        <p>New account has been added however an error prevented a new referrer code being added to GreenEarthAppeal.org</p>
                                        <p><a href="index.php">Back to Main Page</a></p><?php
                                    }
                                                                       
                                }
                            }
                        }
                        //
                        //
                        return true;
                    } else {
                        //Do we set to the default logo to the account top>
                        $sql = "SELECT slogo FROM t_sponsors WHERE scode='{$linkcode}'";
                        $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
                        if ($r[0]=="") {
                            //Get Account Top 
                            $sql = "SELECT sponsor_id FROM t_partner_accounts WHERE account_email=" . $this->cn->safe($this->s['u']);
                            $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
                            $sql = "SELECT slogo FROM t_sponsors WHERE id='{$r[0]}'";
                            $q = mysql_query($sql, $this->cn->get());
                            if (mysql_num_rows($q)>0) {
                                $r = mysql_fetch_array($q);
                                if ($r[0]<>"") {
                                    $this->updateSponsors($linkcode, $this->p['compname'], $r[0], $this->p['compweb']);
                                }
                            }
                        } else {
                            //Do not change logo
                        }
                        
                        ?><h1>Success</h1>
                        <p>New account has been added. The account's URL is <a href="http://www.greenearthappeal.org/?ref=<?php echo $linkcode; ?>">http://www.greenearthappeal.org/?ref=<?php echo $linkcode; ?></a></p>
                        <p><a href="index.php">Back to Main Page</a></p><?php
                        return true;
                    }
             } else {
                 return false;
             }
        }
    }
    
    function updateSponsors($scode, $sname, $slogo, $shome) {
        //Check Logo has no path
        $slogo = str_replace("../graphics/sponsors/", "", $slogo);
        //Check homepage has http:// or https://
        if (substr($shome,0,7) != "http://" && substr($shome,0,8) != "https://") {
            $shome = "http://" . $shome;
        }
        $sql = "UPDATE t_sponsors SET scode=" . $this->cn->safe($scode) . ", sname=" . $this->cn->safe(ucwords(strtolower($sname))) . ", slogotext=" . $this->cn->safe(ucwords(strtolower($sname))) . ", shome=" . $this->cn->safe($shome);
                if ($slogo<>"") { $sql .= ", slogo=" . $this->cn->safe($slogo);  }
                $sql .= " WHERE scode=" . $this->cn->safe($scode);
        mysql_query($sql, $this->cn->get());
        if (mysql_affected_rows($this->cn->get()) < 1) {
             $sql = "INSERT INTO t_sponsors SET scode=" . $this->cn->safe($scode) . ", sname=" . $this->cn->safe($sname) . ", slogotext=" . $this->cn->safe($sname) . ", shome=" . $this->cn->safe($shome);
                if ($slogo<>"") { $sql .= ", slogo=" . $this->cn->safe($slogo);  } 
             mysql_query($sql, $this->cn->get());
             if (mysql_affected_rows($this->cn->get())<1) {
                 return 0;
             } else {
                 return 2;
             }   
        } else {
            return 1;
        }
    }
    
    function _helper_filetypeallowed($uploaded_type) {
        $types = explode("|", $this->file_allowable_types);
        $uploaded_type = str_replace("image/", "", $uploaded_type);
        if (in_array($uploaded_type, $types)) {
            return true;
        } else {
            return false;
        }
        
    }
    
    function generateLinkCode()  {
        $alphas = array_merge(range('a', 'z'), range('0', '9'));
        $found = true;
        $max = count($alphas);
        while ($found) {
             $code = "";
             for ($i=0; $i<21; $i++) {
                 $code .= $alphas[rand(0, $max-1)];
             }
             //Check code not used
             $sql = "SELECT COUNT(*) FROM t_sponsors WHERE scode='{$code}'";
             $r = mysql_query($sql, $this->cn->get());
             if ($r[0]==0) {
                 return $code;
             } 
        }
        
    }

    function listPartnersCustomers() {
		//$onlienpath = 'http://localhost/tree_request/';
		$onlienpath = 'http://www.greenearthappeal.org/tree_request/';
		$onlinefolder = 'counter/partner/';
        ?>
        <table width="100%" cellspacing="0" cellspacing="0">
        <tr height="20"><td colspan="3"><h1>Customers</h1></td><td align="right"><a href="index.php?a=company.add"><img src="imgs/btn_addnew.gif" border="0"></a></td><td align="right"><a href="index.php?a=bulk.upload"><img src="imgs/bulk.png" border="0"></a></td><td width="10"></td></tr>
        <?php
        $sql = "SELECT id FROM t_partner_accounts WHERE account_email=" . $this->cn->safe($this->s['u']);
        if ($this->debug) { echo $sql; }
        $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
        $totalPartnerSum = 0;
		$sqlCount = "SELECT id FROM t_partner_cert_companies WHERE partner_id={$r['id']}";
		$c = mysql_query($sqlCount, $this->cn->get());
		while ($cnt = mysql_fetch_array($c)) {
			$totalPartnerSum = $totalPartnerSum + $this->treetotal($cnt['id']);
		}
		$this->createCounterImageForPartner($totalPartnerSum, $r['id']);
		?>
		<tr height="30"><td colspan="6">Total trees planted for your account: <?php echo $totalPartnerSum; ?></td></tr>
        <tr height="30"><td>Your ticker url </td><td colspan="5"><?php echo $onlienpath. $onlinefolder. 'ticker_'.$r['id'].'.png'; ?></td></tr>
        <tr height="30"><td>Your ticker image </td><td colspan="5"><a target="_blank" href="<?php echo $onlienpath. $onlinefolder. 'ticker_'.$r['id'].'.png'; ?>">View Image</a></td></tr>
		</table>
        
        <?php 
		$sql = "SELECT id, comp_id, compname, comppostcode FROM t_partner_cert_companies WHERE partner_id={$r['id']} ORDER BY compname";
        $q = mysql_query($sql, $this->cn->get());
        ?><table width="900" cellspacing="0" cellpadding="10">
        <tr class="header"><td>Client ID</td><td>Company Name</td><td>Postcode</td><td align="center">Trees Planted</td><td></td><td></td></tr><?php
        $i =0;
        while ($r = mysql_fetch_array($q)) {
            if ($i==0) { $class="odd"; } else { $class="even"; }
            ?><tr class="<?php echo $class; ?>"><td width="200"><?php echo $r['comp_id']; ?></td><td width="580"><?php echo $r['compname']; ?></td><td width="50"><?php echo $r['comppostcode']; ?></td>
                <td width="100" align="center"><?php echo $this->treetotal($r['id']); ?></td>
                <td width="50"><a href="index.php?a=company.edit&id=<?php echo $r['id']; ?>"><img src="imgs/btn_edit.png" border="0"></a></td>
                <td width="50"><a href="index.php?a=company.order&id=<?php echo $r['id']; ?>"><img src="imgs/btn_order.png" border="0"></a></td>
            </tr><?php
            $i++;
            if ($i==2) { $i=0; }
        }
        ?></table><?php
    }

    function getPartnerID() {
        $sql = "SELECT id FROM t_partner_accounts WHERE account_email=" . $this->cn->safe($this->s['u']);
        $q = mysql_query($sql, $this->cn->get());
        if (mysql_num_rows($q)<1) {
            die("Authentication Error");
        } else {
            $r = mysql_fetch_array($q);
            return $r[0];
        }
    }

    function checkChangesAllowed($username, $id) {
        $sql = "SELECT COUNT(*) FROM t_partner_cert_companies INNER JOIN t_partner_accounts ON t_partner_cert_companies.partner_id = t_partner_accounts.id
                WHERE t_partner_accounts.account_email=" . $this->cn->safe($username) . " AND t_partner_cert_companies.id=" . $this->cn->safe($id);
        $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
        if ($r[0]>0) {
            return true;
        } else {
            return false;
        }
    }
	
	
	function uploadCSV(){ ?>
		<h1>Upload CSV file for Bulk Orders</h1>
        <form action="index.php" name="bulk-upload" method="post" enctype="multipart/form-data"><table>
            <tr><td class="formparttitle">Select File:</td><td><input class="formpart" type="file" name="csv_file"  /></td></tr>
            <tr><td>&nbsp;<input type="hidden" name="a" value="bulk" /></td></tr>
            <tr><td colspan="2"><input type="image" src="imgs/btn_file_wide.png" height="46" width="780" value="Save" /></td></tr>
        </table></form>
	<?php } // func ends 
	

    function customerForm($r=null) {
        if ($r==null) { $r = array(); }
        if (isset($r['id'])) { ?><h1>Edit Customer</h1><?php } else { ?><h1>Add New Customer</h1><?php }
        ?><form action="index.php" name="partner-customer" method="post" enctype="multipart/form-data"><table>
            <tr><td colspan="2"><h2>Company Details</h2></td></tr>
            <tr><td class="formparttitle">Client ID:</td><td><input class="formpart" type="text" name="comp_id" value="<?php if (isset($r['comp_id'])) { echo $r['comp_id']; } ?>" /></td></tr>
            <tr><td class="formparttitle">Name:</td><td><input class="formpart" type="text" name="compname" value="<?php if (isset($r['compname'])) { echo $r['compname']; } ?>" /></td></tr>
            <tr><td>Address:</td><td><input class="formpart" type="text" name="compaddress1" value="<?php if (isset($r['compaddress1'])) { echo $r['compaddress1']; } ?>" /></td></tr>
            <tr><td></td><td><input class="formpart" type="text" name="compaddress2" value="<?php if (isset($r['compaddress2'])) { echo $r['compaddress2']; } ?>" /></td></tr>
            <tr><td></td><td><input class="formpart" type="text" name="compaddress3" value="<?php if (isset($r['compaddress3'])) { echo $r['compaddress3']; } ?>" /></td></tr>
            <tr><td>Postcode:</td><td><input class="formpart" type="text" name="comppostcode" value="<?php if (isset($r['comppostcode'])) { echo $r['comppostcode']; } ?>" /></td></tr>
            <tr><td>Web&nbsp;Address:</td><td><input class="formpart" type="text" name="compweb" value="<?php if (isset($r['compweb'])) { echo $r['compweb']; } ?>" /></td></tr>

            <tr><td colspan="2"><br /><h2>Contact Details</h2></td></tr>
            <tr><td>Contact Title</td><td><select class="formpart" name="contactitle">
                <?php
                $opts = array();
                array_push($opts, array("Select Title...", "Select Title..."));
                array_push($opts, array("Mr", "Mr"));
                array_push($opts, array("Mrs", "Mrs"));
                array_push($opts, array("Miss", "Miss"));
                array_push($opts, array("Ms", "Ms"));
                if (isset($r['contacttitle'])) { $sel = $r['contacttitle']; } else { $sel = ""; }
                $this->defaultSelected($opts, $sel);
                ?></select></td></tr>
            <tr><td>Forename:</td><td><input class="formpart" type="text" name="contactforename" value="<?php if (isset($r['contactforename'])) { echo $r['contactforename']; } ?>" /></td></tr>
            <tr><td>Surname:</td><td><input class="formpart" type="text" name="contactsurname" value="<?php if (isset($r['contactsurname'])) { echo $r['contactsurname']; } ?>" /></td></tr>
            <tr><td>Telephone:</td><td><input class="formpart" type="text" name="contacttelephone" value="<?php if (isset($r['contacttelephone'])) { echo $r['contacttelephone']; } ?>" /></td></tr>
            <tr><td>Email:</td><td><input class="formpart" type="text" style="width: 500px;" name="contactemail" value="<?php if (isset($r['contactemail'])) { echo $r['contactemail']; } ?>" /></td></tr>
            <tr><td colspan="2"><br /><h2>Social</h2></td></tr>
            <tr><td class="formparttitle">Facebook url:</td><td><input class="formpart" type="text" name="fburl" value="<?php if (isset($r['facebook_url'])) { echo $r['facebook_url']; } ?>" /></td></tr>
            <tr><td class="formparttitle">Twitter url:</td><td><input class="formpart" type="text" name="twurl" value="<?php if (isset($r['twitter_url'])) { echo $r['twitter_url']; } ?>" /></td></tr>
            <tr><td colspan="2"><br /><h2>Uploads</h2></td></tr> 
            <tr><td>Logo</td><td><input class="formpart" style="width: 500px;" type="file" name="file" id="file" /><?php if (isset($r['companycode'])) { ?><br /><img src="../graphics/sponsors/<?php echo $this->getImage($r['companycode']); ?>" alt="" /><?php } ?></td></tr>
            <tr><td>Product Artwork</td><td><input class="formpart" style="width:500px" type="file" name="artwork" id="artwork" /></td></tr>
            <tr><td colspan="2"><br />Notes</td></tr>
            <tr><td colspan="2"><textarea cols="90" rows="10" name="contactnotes" style="width:780px;"><?php if (isset($r['contactnotes'])) { echo $r['contactnotes']; } ?></textarea></td></tr>
            <tr><td colspan="2"><input type="hidden" name="a" value="company" /><?php if (isset($r['id'])) { ?><input type="hidden" value="<?php echo $r['id']; ?>" name="id" /><?php } ?></td></tr>
            <tr><td colspan="2"><input type="image" src="imgs/btn_save_wide.png" height="46" width="780" value="Save" /></td></tr>
        </table></form><?php
    }
    
    function getImage($custcode) {
        $sql = "SELECT slogo FROM t_sponsors WHERE scode=" . $this->cn->safe($custcode);
        $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
        return $r[0];
    }

    function treeForm($r=null, $customer_id=0) {
        if ($r==null) { $r = array(); }
        if ($this->debug) { echo "<br />treeForm called for custid {$customer_id}"; }
        if ($this->checkChangesAllowed($this->s['u'], $customer_id)) {
            $sql = "SELECT compname, compaddress1, compaddress2, compaddress3, comppostcode FROM t_partner_cert_companies WHERE id=" . $this->cn->safe($customer_id);
            $cust_details = mysql_fetch_array(mysql_query($sql, $this->cn->get()));

            ?><h1>Order Trees For Customer</h1>
            <form action="index.php" name="partner-customer" method="post" enctype="multipart/form-data">
                <table>
                     <tr height="30">
                        <td class="formparttitle">Your ticker url: </td>
                        <td>http://localhost/tree_request/counter/ticker_<?php echo $customer_id; ?>.png</a></td>
                    </tr>
                     <tr height="30">
                        <td class="formparttitle">Your ticker image: </td>
                        <td><a target="_blank" href="counter/ticker_<?php echo $customer_id; ?>.png">View Image</a></td>
                    </tr>
                    <tr>
                        <td class="formparttitle">Customer</td>
                        <td><?php echo $cust_details['compname']; ?><br /><?php echo $this->_displayAddress($cust_details['compaddress1'], $cust_details['compaddress2'], $cust_details['compaddress3'], $cust_details['comppostcode'], "<br />");  ?></td>
                    </tr>
                    <tr>
                        <td class="formparttitle">Number Of Trees</td>
                        <td><input type="text" class="formpart" name="numtrees" value="<?php if (isset($r['numtrees'])) { echo $r['numtrees']; } ?>" /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="hidden" name="a" value="tree.request" /><input type="hidden" name="custid" value="<?php echo $customer_id; ?>" />
                        <input type="image" src="imgs/btn_order_wide.png" value="Order Trees" /></td>
                    </tr>
                </table>
            </form><?php
        } else {
            die("Authorisation Error");
        }
    }

   function _displayAddress($address1, $address2, $address3, $comppostcode, $seperator) {
       $s = "";
       if ($address1<>"") { $s .= $address1 . $seperator; }
       if ($address2<>"") { $s .= $address2 . $seperator; }
       if ($address3<>"") { $s .= $address3 . $seperator; }
       if ($comppostcode<>"") { $s .= $comppostcode . $seperator; }
       $sep_len = strlen($seperator);
       return substr($s, 0, (strlen($s)-$sep_len));
   }

    function defaultSelected($options, $selected) {
        $max = count($options);
        for ($i=0; $i<$max; $i++) {
            if ($options[$i][0]==$selected) {
               ?><option selected="selected" value="<?php echo $options[$i][0]; ?>"><?php echo $options[$i][1]; ?></option><?php
            } else {
                ?><option value="<?php echo $options[$i][0]; ?>"><?php echo $options[$i][1]; ?></option><?php
            }
        }
    }

    function connect() {
        //Connect to db, use when nessecary to save server load.
        if ($this->cn_state == 0) {
            require_once($this->path_to_scripts . "site_generation/cl_db.php");
            $this->cn = new cl_db();
        }
    }

    function attachTreeRequest() {
         //Check stuff in the stack
         $passed = true;
         if (!isset($this->p['numtrees']) || $this->p['numtrees']=="" || $this->p['numtrees']=="0" || !is_numeric($this->p['numtrees'])) { $passed = false; }
         if (!isset($this->p['custid']) || $this->p['custid']=="" || $this->p['custid']=="0") { $passed = false; }
         if (!$this->checkChangesAllowed($this->s['u'], $this->p['custid'])) { $passed = false; } 
         
         if ($passed==false) {
             return false;
         } else {
             $sql = "SELECT COUNT(*) FROM t_partner_cert_requests WHERE custaccountid=" . $this->cn->safe($this->p['custid']);
             $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
             if ($r[0]>0) { $new = false; } else { $new = true; }
             $sql = "INSERT INTO t_partner_cert_requests SET numtrees=" . $this->cn->safe($this->p['numtrees']) . ", custaccountid=" . $this->cn->safe($this->p['custid']). ", date=NOW()";
             if ($this->debug) { echo "<br>" . $sql; }
             mysql_query($sql, $this->cn->get());
             if (mysql_affected_rows($this->cn->get())>0) {
                 //Success
                 
                 // phpcodeteam@gmail.com create pdf certification
                 if ($this->p['custid'] > 0) {
                    $sql = "SELECT companycode, compname, partner_id FROM t_partner_cert_companies WHERE id=" . $this->cn->safe($this->p['custid']); 
                    $r = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
                    $pct_linkcode = $r[0];
                    $pct_compname = $r[1];
                    $pct_id = $this->p['custid'];
                    
                    // get number of trees
                    $sql = "SELECT SUM(numtrees) FROM t_partner_cert_requests WHERE custaccountid=$pct_id";
                    $r2 = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
                    $pct_numtrees = $r2[0];
                    $pct_numtrees_thou = number_format($pct_numtrees*1000);

                    // get logo partner
                    $sql2   = 'SELECT ts.slogo AS slogo FROM t_partner_accounts AS tpa
                                INNER JOIN t_sponsors AS ts
                                ON tpa.sponsor_id=ts.id
                                WHERE tpa.id='.$r[2];
                    $result = mysql_fetch_array(mysql_query($sql2, $this->cn->get()));
                    $pct_logo = $result[0];
                    ob_start();
                    //require_once('cert.php');
                    ob_end_clean();
                    $this->doInformationMail($new, $this->p['custid'], $this->p['numtrees'], true);
                 } else {
                    $this->doInformationMail($new, $this->p['custid'], $this->p['numtrees']);   
                 }
                 
                 
                 ?><h1>Success</h1>
                <p>An order for <?php echo $this->p['numtrees']; ?> tree<?php if ($this->p['numtrees']>1) { echo "s"; } ?> has been placed with Green Earth Appeal on behalf of your customer.</p>
                <p><a href="index.php">Back to Main Page</a></p><?php
             } else {
                 ?><h1>Error</h1>
                <p>The order for trees could not be saved. Please go back and try again.</p>
                <p><a href="javascript:history.go(-1)">Go Back</a></p><?php
             }
         }  
    }
    
    function treetotal($custacid) {
        $sql = "SELECT Sum(numtrees) FROM t_partner_cert_requests WHERE custaccountid=" . $this->cn->safe($custacid);
        $q = mysql_query($sql, $this->cn->get());
        if (mysql_num_rows($q)<1) {
            return 0;
        } else {
            $r = mysql_fetch_array($q);
            if ($r[0]<>0) { return $r[0]; } else { return 0; }
        }
        
    }
    
    function doInformationMail($isnew, $account_id, $num_trees, $pdf=false) {
        //Get Details
        if ($num_trees > 1) { $treetext="trees"; } else { $treetext="tree"; }
        $sql = "SELECT * FROM t_partner_cert_companies WHERE id=" . $this->cn->safe($account_id);
        $customer = mysql_fetch_array(mysql_query($sql, $this->cn->get()));
        $sql = "SELECT * FROM t_partner_accounts WHERE id=" . $customer['partner_id'];
        $account = mysql_fetch_array(mysql_query($sql, $this->cn->get())); 
		$tickerurl = 'http://greenearthappeal.org/tree_request/counter/ticker_'.$account_id.'.png';
		
        //Email Marvin
        $msg = file_get_contents("notify_marvin.html");
        $msg = str_replace("[account]", $account['account_name'], $msg); 
        $msg = str_replace("[numtrees]", $num_trees, $msg); 
        $msg = str_replace("[custname]", $customer['compname'], $msg);
        $msg = str_replace("[treetext]", $treetext, $msg); 
		$msg = str_replace("[tickerurl]", $tickerurl, $msg); 
		
        $this->sendHTMLmail("marvin@greenearthappeal.org", $msg, "Notification: Partner ordering trees on behalf of customer", false, $pdf);
        //$this->sendHTMLmail("thakuryogita@gmail.com", $msg, "Notification: Partner ordering trees on behalf of customer", false, $pdf);
        $msg = "";
        
        //Email Account Owner
        if (isset($account['account_email']) && $account['account_email']<>"") {
            $msg = file_get_contents("notify_customer.html");
            $msg = str_replace("[name]", $account['account_person'], $msg); 
            $msg = str_replace("[numtrees]", $num_trees, $msg); 
            $msg = str_replace("[custname]", $customer['compname'], $msg); 
            $msg = str_replace("[treetext]", $treetext, $msg);  
			$msg = str_replace("[tickerurl]", $tickerurl, $msg); 
            $this->sendHTMLmail($account['account_email'], $msg, "Confirmation of tree order for " . $customer['compname'], false, $pdf);
            //$this->sendHTMLmail('thakuryogita@gmail.com', $msg, "Confirmation of tree order for " . $customer['compname'], false, $pdf);
            $msg = "";
        }
        
        //Email Account Named
        if (isset($customer['contactemail']) && $customer['contactemail']<>"") {  
            if ($isnew) {
                $msg = file_get_contents("mailtemplate_new.html");
            } else {
                $msg = file_get_contents("mailtemplate_reorder.html");
            }
            $name = $this->_helperNameType($customer['contacttitle'], $customer['contactforename'],$customer['contactsurname']); 
            $msg = str_replace("[name]", $name, $msg); 
            $msg = str_replace("[numtrees]", $num_trees, $msg); 
            $msg = str_replace("[accountname]", $account['account_name'], $msg);
            $msg = str_replace("[linkcode]", $customer['companycode'], $msg); 
            $msg = str_replace("[treetext]", $treetext, $msg);  
            if (!$isnew) {
                $msg = str_replace("[treetotal]", $this->treetotal($account_id), $msg);
            }
			$msg = str_replace("[tickerurl]", $tickerurl, $msg); 
            $this->sendHTMLmail($customer['contactemail'], $msg, "Confirmation - we're planting " . $num_trees . " " . $treetext, false, $pdf);
			//$this->sendHTMLmail('thakuryogita@gmail.com', $msg, "Confirmation - we're planting " . $num_trees . " " . $treetext, false, $pdf);
            $msg = "";
        }
    }
    
    function sendHTMLmail($to, $msg, $subject, $attach_marketing=false, $pdf = false) {
        $from = "marvin@greenearthappeal.org";
        $headers = "From: {$from}\r\n";
        $headers .= "Reply-To: marvin@greenearthappeal.com\r\n";
        $headers .= "Return-path: <{from}>\r\n";
        if (!$attach_marketing) {
            if ($pdf) {
                // phpcodeteam@gmail.com new method
                $pct_customer_id = $this->p['custid'];
                //if ($pct_customer_id) die();
                require('PHPMailer_522/test_sendmail_basic.php');
            } else {
                $from = "marvin@greenearthappeal.org";
                $headers = "From: {$from}\r\n";
                $headers .= "Reply-To: marvin@greenearthappeal.com\r\n";
                $headers .= "Return-path: <{from}>\r\n";
                $headers .= "Content-Type: text/html\r\n";
                if ($this->debug) { echo "<hr />EMAIL: {$to} SUBJECT: {$subject}<br />{$msg}"; }
                @mail ($to, $subject, $msg, $headers, "-f " . $from);
            }
            
        } else {
            $random_hash = md5(date('r', time())); 
            $headers .= "Content-Type: multipart/mixed; boundary=\"PHP-mixed-" . $random_hash . "\"";
            $attachment = chunk_split(base64_encode(file_get_contents('Marketing_Collateral.zip')));
            
            ob_start();
            ?> 
            --PHP-mixed-<?php echo $random_hash; ?>  
            Content-Type: multipart/alternative; boundary="PHP-alt-<?php echo $random_hash; ?>" 

            --PHP-alt-<?php echo $random_hash; ?>  
            Content-Type: text/plain; charset="iso-8859-1" 
            Content-Transfer-Encoding: 7bit

            --PHP-alt-<?php echo $random_hash; ?>  
            Content-Type: text/html; charset="iso-8859-1" 
            Content-Transfer-Encoding: 7bit

            <?php echo $msg; ?>

            --PHP-alt-<?php echo $random_hash; ?>-- 

            --PHP-mixed-<?php echo $random_hash; ?>  
            Content-Type: application/zip; name="Marketing Collateral.zip"  
            Content-Transfer-Encoding: base64  
            Content-Disposition: attachment  

            <?php echo $attachment; ?> 
            --PHP-mixed-<?php echo $random_hash; ?>-- 

            <?php 
            $message = ob_get_clean();
            mail($to, $subject, $message, $headers, "-f" . $from);
            echo $message; 
        }
    }
    
    function _helperNameType($title, $forename, $surname) {
        if (isset($title) && isset($surname) && $title<>"" && $surname<>"") {
            return $title . " " . $surname;
        } else {
            if (isset($forename) && $forename<>"") {
                return $forename;  
            } else {
                return "Sir/Madam";
            }
        }
    }
	
	
	function createCounterImageForPartner($cnt,$prId){
		 
		  //Set the Content Type
		  @header('Content-type: image/png');
	
		  // Create Image From Existing File
		  $png_image = imagecreatefrompng('imgs/counter_img.png');
	
		  // Allocate A Bachground Color For The Text - TRANSPARENT
		  $bg_color = imagecolorallocatealpha($png_image ,22, 105, 54, 127);
	
		  // Allocate A Color For The Text - GREEN
		  $font_color = imagecolorallocate($png_image, 22, 105, 54);
	
		  // Set Path to Font File
		  $font_path = 'counter/fonts/HelveticaLTStd-Bold.ttf';
		  
		  // Set Number Text to Be Printed On Image
		  $text = $cnt;
		  $chklen =  strlen($text);
		  if($chklen == 1) {
			 $dist = '134';	  
		  } else if($chklen == 2){
			  $dist = 106;
		  } else if($chklen == 3){
			  $dist = 80;
		  } else if($chklen == 4){
			  $dist = 56;
		  } else if($chklen == 5){
			  $dist = 28;
		  }
	
		  imagesavealpha($png_image, true);
		  imagefill($png_image, 0, 0, $bg_color); 
	
		  // Print Text On Image
		  imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);
	
		  $fname = 'counter/partner/ticker_'.$prId.'.png';
		  
		  // Send Image to Browser
		  imagepng($png_image,$fname);
			  
		  // Clear Memory
		  imagedestroy($png_image);
		  
	} // func ends 
	
	
	function createCounterImage(){
		  //Set the Content Type
		  @header('Content-type: image/png');
	
		  // Create Image From Existing File
		  $png_image = imagecreatefrompng('imgs/counter_img.png');
	
		  // Allocate A Bachground Color For The Text - TRANSPARENT
		  $bg_color = imagecolorallocatealpha($png_image ,22, 105, 54, 127);
	
		  // Allocate A Color For The Text - GREEN
		  $font_color = imagecolorallocate($png_image, 22, 105, 54);
	
		  // Set Path to Font File
		  $font_path = 'counter/fonts/HelveticaLTStd-Bold.ttf';
	
		  $totalNumTrees = $this->treetotal($this->p['custid']);
		  
		  // Set Number Text to Be Printed On Image
		  $text = $totalNumTrees;
		  $chklen =  strlen($text);
		  if($chklen == 1) {
			 $dist = '134';	  
		  } else if($chklen == 2){
			  $dist = 106;
		  } else if($chklen == 3){
			  $dist = 80;
		  } else if($chklen == 4){
			  $dist = 56;
		  } else if($chklen == 5){
			  $dist = 28;
		  }
	
		  imagesavealpha($png_image, true);
		  imagefill($png_image, 0, 0, $bg_color); 
	
		  // Print Text On Image
		  imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);
	
		  $fname = 'counter/ticker_'.$this->p['custid'].'.png';
		  
		  // Send Image to Browser
		  imagepng($png_image,$fname);
			  
		  // Clear Memory
		  imagedestroy($png_image);
		  
	} // func ends 

}
?>