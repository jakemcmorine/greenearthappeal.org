<?php
class cl_referrer {

	var $db;
	var $PATH_TO_GRAPHICS;
	
	function cl_referrer($path_to_graphics) {
		require_once("cl_db.php");
		$this->PATH_TO_GRAPHICS = $path_to_graphics;
		$this->db = new cl_db();
	}
	
	function setReferrer($sponsorCode, $isNew) {
		//Clean String
		$sponsorCode = $this->db->cleanString($sponsorCode);
		
		//Do query
		$sql = "SELECT COUNT(*) AS nf FROM t_sponsors WHERE scode='{$sponsorCode}'";
		$r = mysql_fetch_array(mysql_query($sql, $this->db->cn));
		if ($r['nf']==0) {
			$_SESSION['refid'] = "b-l-a-n-k";
		}
		if($isNew) { $this->incrementHit($_SESSION['refid']); }

	}	
	
	function sponsorBox($sponsorCode, $is_in_subdirectory) {
		//Clean String
		$sponsorCode = $this->db->cleanString($sponsorCode);
	
		//Do query
		$sql = "SELECT sname, slogo, shome, slogotext FROM t_sponsors WHERE scode='{$sponsorCode}' LIMIT 0,1";
		$q = mysql_query($sql, $this->db->cn);
		if (mysql_num_rows($q)>0) {
			$r = mysql_fetch_array($q);
			?>
			<table class="sponsorbox" width="326" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="3">
			  <tr>
				<td align="left"><img src="<?php echo $this->PATH_TO_GRAPHICS; ?>sponsors/working_in_partnership_with.gif" alt="Working In Partnership With" /></td>
			  </tr>
			  <tr>
				<td align="center"><a href="<?php echo $r['shome']; ?>" target="_blank"><img src="<?php echo $this->PATH_TO_GRAPHICS; ?>sponsors/<?php echo $r['slogo']; ?>" alt="<?php echo $r['sname']; ?>" border="0" /></a></td>
			  </tr>
              <?php if (strlen($r['slogotext'])>1) {
			  	?>
                <tr>
					<td align="center"><?php echo $r['slogotext']; ?></td>
			    </tr>
                <?php
			} ?>
			</table></td>
			  </tr>
			</table>
			<?php
		} else {
			//Default to UNEP
			$sql = "SELECT sname, slogo, shome FROM t_sponsors WHERE id='0' LIMIT 0,1";
			$q = mysql_query($sql, $this->db->cn);
			$r = mysql_fetch_array($q);
			?>
			<table class="sponsorbox" width="326" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="3">
			  <tr>
				<td align="left"><img src="<?php echo $this->PATH_TO_GRAPHICS; ?>sponsors/working_in_partnership_with.gif" alt="Working In Partnership With" /></td>
			  </tr>
			  <tr>
				<td align="center"><a href="<?php echo $r['shome']; ?>" target="_blank"><img src="<?php $this->PATH_TO_GRAPHICS; ?>sponsors/<?php echo $r['slogo']; ?>" alt="<?php echo $r['sname']; ?>" border="0" /></a></td>
			  </tr>
			</table></td>
			  </tr>
			</table>
			<?php
		}
	}
	
	function FPsponsor($sponsorCode) {
	
		//Clean String
		$sponsorCode = $this->db->cleanString($sponsorCode);
	
		//Do query
		$sql = "SELECT sname, slogo, shome, slogotext FROM t_sponsors WHERE scode='{$sponsorCode}' LIMIT 0,1";
		
		$q = mysql_query($sql, $this->db->cn);
		if (mysql_num_rows($q)>0) {
			$r = mysql_fetch_array($q);
			?>
            <div class="sponsorbox">
            <p><img src="http://www.greenearthappeal.org/graphics/sponsors/working_in_partnership_with.gif" alt="Working In Partnership With" /></p>
            <p><a href="<?php echo $r['shome']; ?>" target="_blank"><img src="<?php echo $this->PATH_TO_GRAPHICS . $r['slogo']; ?>" alt="<?php echo $r['sname']; ?>" border="0"/></a></p>
            <?php if (strlen($r['slogotext'])>1) {
			  	?>
                <p><?php echo $r['slogotext']; ?></p>
                <?php
			} ?>
            </div>
			<?php
		} 
	}
	
	function incrementHit($sponsorCode) {
		$sponsorCode = $this->db->cleanString($sponsorCode);
		$sql = "UPDATE  t_sponsors
				INNER JOIN t_sponsors_stats ON t_sponsors.id = t_sponsors_stats.sid
				SET t_sponsors_stats.hits=t_sponsors_stats.hits + 1
				WHERE t_sponsors_stats.dpoint=CURDATE() AND t_sponsors.scode='{$sponsorCode}';";
		mysql_query($sql, $this->db->cn);
		if (mysql_affected_rows($this->db->cn)<1) {
			$sql = "INSERT INTO t_sponsors_stats (t_sponsors_stats.sid, t_sponsors_stats.dpoint, t_sponsors_stats.hits ) 
					VALUES ((SELECT id FROM t_sponsors WHERE scode='{$sponsorCode}' LIMIT 0,1), CURDATE(), 1);";
			mysql_query($sql, $this->db->cn);
		}
	}

} // End Class
?>