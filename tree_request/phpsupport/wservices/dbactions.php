<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

function pushNewRespondent($title, $fname, $lname, $adr1, $adr2, $adr3, $town, $county, $postcode,
							$eadr, $dayphone, $evephone, $mobnum, $ipaddress, $cn) {
	//PURPOSE: Push information about respondent into database
	//RETURNS: Record id number
	
	//Clean the user provided info prior to insert
	$title = cleanString($title);
	$fname = cleanString($fname);
	$lname = cleanString($lname);
	$adr1 = cleanString($adr1);
	$adr2 = cleanString($adr2);
	$adr3 = cleanString($adr3);
	$town = cleanString($town);
	$county = cleanString($county);
	$postcode = cleanString($postcode);
	$eadr = cleanString($eadr);
	$dayphone = cleanString($dayphone);
	$evephone = cleanString($evephone);
	$mobnum = cleanString($mobnum);
	$ipaddress = cleanString($ipaddress);
	
	//Create Query
	$sql = "INSERT into t_respondents SET
				stitle='{$title}', 
				sfirstname='{$fname}', 
				slastname='{$lname}', 
				sadr1='{$adr1}', 
				sadr2='{$adr2}', 
				sadr3='{$adr3}', 
				stown='{$town}', 
				scounty='{$county}', 
				spostcode='{$postcode}', 
				semail='{$eadr}', 
				sdayphone='{$dayphone}', 
				sevephone='{$evephone}', 
				smobile='{$mobnum}', 
				dresponddate=NOW(),
				sipaddress='{$ipaddress}'";	
	//Do query 
	if (!mysql_query($sql, $cn)) {
		//Query Failed
		die("ERROR");
	} else {
		if (mysql_affected_rows($cn)<1) {
			//Query Did Not Effect Any Rows
			die ("ERROR");
		} else {
			//Query Was Good
			echo mysql_insert_id($cn);
		}
	}
} //End Function

function pushReferral($referredBy, $name, $email, $cn) {
	//PURPOSE: 	Push information about referral into database
	//RETURNS: 	Int 0=previously sent
	//			Int 1=inserted
	//			Int 2=error
	
	//Clean the user provided info prior to insert
	$name = cleanString($name);
	$email = cleanString($email);
	$referredBy = cleanString($referredBy);
	
	$sql = "SELECT COUNT(*) AS nf FROM t_referals WHERE semail='{$email}'";
	$r = mysql_fetch_array(mysql_query($sql, $cn));
	if ($r['nf'] > 0) {
		//Previously Exists
		echo "0";
	} else {
		//Is OK to insert
		$sql = "INSERT INTO t_referals SET
					iparent='{$referredBy}',
					sname='{$name}',
					semail='{$email}',
					sreferraldate=NOW()";

		mysql_query($sql, $cn);
		if (mysql_affected_rows($cn)>0) {
			echo "1";
		} else {
			echo "2";
		}
	}
} //End Function

function pushPledge($respondent, $cn) {
	//PURPOSE: 	Push information about someone's pledge
	//RETURNS: 	Str(32) Of Pledge Key OR False
	
	//Clean the user provided info prior to insert
	$respondent = cleanString($respondent);
	
	//Generate Unique Key
	$s = date("l-jS-F-Y/H:i:s:u/e/O") . "/" . time();
	$key = md5($s);
	
	//Insert To Database
	$sql = "INSERT INTO t_pledges SET
			spledgekey='{$key}',
			spledgerespondent='{$respondent}'";
	mysql_query($sql, $cn);
	
	if (mysql_affected_rows($cn)>0) {
		echo $key;
	} else {
		echo "0";
	}
}
	

function pushPledgeItem($pledgeKey, $pledgeType, $cn) {
	//PURPOSE: 	Push information about someone's pledge item
	//RETURNS: 	Boolean
	
	//Clean the user provided info prior to insert
	$pledgeKey = cleanString($pledgeKey);
	$pledgeType = cleanString($pledgeType);
	
	//Insert The Data
	$sql = "INSERT INTO t_pledge_items SET
				spledgeparent='{$pledgeKey}',
				ipledgeoption='{$pledgeType}',
				ipledgedate=NOW(),
				ipledgesent=0,
				ipledgefilled=0";
	mysql_query($sql, $cn); 
	if (mysql_affected_rows($cn)>0) {
		echo "true";
	} else {
		echo "false";
	}
}

function updatePledgeSent($id, $cn) {
	//PURPOSE: 	Update pledge sent flag
	//RETURNS: 	Null
	
	//Not nessecary to clean data here
	
	//Do Update
	$sql = "UPDATE t_pledges_items SET ipledgesent WHERE id={$id}";
	mysql_query($sql, $cn);
}

function getRespondentInfo($id, $cn) {
	//PURPOSE: 	Get Information About A Respondent
	//RETURNS: 	Array(Mixed)
	
	//Clean the user provided info prior to insert
	$id = cleanString($id);
	
	//Get The Data
	$id = 
	$sql = "SELECT stitle, sfirstname, slastname, spostcode, sdayphone, semail FROM t_respondents WHERE id='{$id}'";
	$r = mysql_fetch_array(mysql_query($sql, $cn));
	return $r;
}

function getRespondentPledges($id, $cn) {
	//PURPOSE: 	Get Information About A Respondent
	//RETURNS: 	Array(WithLink-Str, NoLink-Str)
	
	//Clean the user provided info prior to insert
	$id = cleanString($id);
	
	//Set the vars
	$linked = "";
	$nolinks = "";
	$ids = array();
	
	//Get The Data 
	$sql = "SELECT t_pledge_items.ipledgeoption, t_pledgeoptions.id, t_pledgeoptions.spledgename, t_pledgeoptions.spledgeurl, t_pledge_items.id as pid
					FROM t_pledges 
					INNER JOIN t_pledge_items ON t_pledge_items.spledgeparent = spledgekey 
					INNER JOIN t_pledgeoptions ON t_pledgeoptions.id = t_pledge_items.ipledgeoption 
					INNER JOIN t_respondents ON t_pledges.spledgerespondent = t_respondents.id
					WHERE t_respondents.id='{$id}'";
	$q = mysql_query($sql, $cn);
	while ($r = mysql_fetch_array($q)) {
		$linked .= "<a href=\"{$r['spledgeurl']}\" target=\"_blank\">{$r['spledgename']}</a><br />";
		$nolinks .= $r['spledgename'] . "<br />";
		array_push($ids, $r['pid']);
	}
	$linked = substr($linked, 0, strlen($linked)-6);
	$nolinks = substr($nolinks, 0, strlen($nolinks)-6);
	$ret = array();
	array_push($ret, $linked);
	array_push($ret, $nolinks);
	array_push($ret, $ids);
	return $ret;
}

function updatePledgeSend($aritems, $cn) {
	//PURPOSE: 	Sets Pledge Sent Flag as true
	//RETURNS: 	Null
	
	$max = count($aritems);
	for ($i=0; $i<$max; $i++) {
		$sql = "UPDATE t_pledge_items SET ipledgesent=1 WHERE id='{$aritems[$i]}'";
		mysql_query($sql, $cn);
	}
}

function getTrackingItem($cn) {
	$sql = "INSERT INTO t_tracking_heads SET createdate=NOW()";
	mysql_query($sql, $cn);
	$id = mysql_insert_id($cn);
	return $id;
}

function pushTrackingItem($id, $type, $item, $value, $step, $cn) {

	$id = cleanString($id);
	$type = cleanString($type);
	$item = cleanString($item);
	$value = cleanString($value);
	$step = cleanString($step);
	
	$sql = "INSERT INTO t_tracking_actions SET
			actiondate=NOW(),
			trackparent='{$id}',
			tracktype='{$type}', 
			trackitem='{$item}',
			trackvalue='{$value}',
			trackstep='{$step}'";
	mysql_query($sql, $cn);
}

function linkTrackingItem($id, $resp, $cn) {

	$id = cleanString($id);
	$resp = cleanString($resp);
	
	$sql = "UPDATE t_tracking_heads SET 
				linktoresp='{$resp}'
			WHERE id='{$id}'";
	mysql_query($sql, $cn);
} 

function linkTrackingItemSource($id, $sourceid, $cn) {

	$id = cleanString($id);
	$sourceid = cleanString($sourceid);
	
	$sql = "UPDATE t_tracking_heads SET 
				linktosource='{$sourceid}'
			WHERE id='{$id}'";
	mysql_query($sql, $cn);
} 

function convertTrackingItem($id, $cn) {

	$id = cleanString($id);
	
	$sql = "UPDATE t_tracking_heads SET 
				isconversion='1'
			WHERE id='{$id}'";
	mysql_query($sql, $cn);
} 
?>