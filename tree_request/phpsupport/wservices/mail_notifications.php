<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

/*
function respondent_welcome($title, $firstname, $lastname, $eadr) {
	//Prepare array of items to be replaced
	$replace = array (	"[PERSON TITLE]"	 				=>	$title,
						"[PERSON LAST NAME]"				=>	$lastname
						);
						
	//Load the file and do the replacement
	$text = file_get_contents("../../public_html/carbon_reduction/mail_templates/respondent_welcome.php", false);
	$text = strtr($text, $replace);
	
	$subject = 		'greenearthappeal.org Welcome Note';
	
	$headers = 		"To: {$firstname} {$lastname} <{$eadr}>\r\n";
	$headers .= 	"From: no-reply@greenearthappeal.org\r\n";
	$headers .= 	"Content-type: text/html; charset=iso-8859-1\r\n";
	
	$message =		"\r\n" . $text . "\r\n";
	
	@mail($eadr, $subject, $message, $headers);
}
*/

function respondent_three_pledge($title, $firstname, $lastname, $eadr, $text) {
	//Prepare array of items to be replaced
	$replace = array (	
						"[PERSON FIRST NAME]"				=>	$firstname
						);
						
	//do the replacement
	$text = strtr($text, $replace);
	
	$subject = 		'Thank you for your pledges';
	
	$headers = 		"To: {$firstname} {$lastname} <{$eadr}>\r\n";
	$headers .= 	"From: no-reply@greenearthappeal.org\r\n";
	$headers .= 	"Content-type: text/html; charset=iso-8859-1\r\n";
	
	$message =		"\r\n" . $text . "\r\n";

	mail($eadr, $subject, $message, $headers);
}

function respondent_two_pledge($title, $firstname, $lastname, $eadr, $text) {
	//Prepare array of items to be replaced
	$replace = array (	
						"[PERSON FIRST NAME]"				=>	$firstname
						);
						
	//Load the file and do the replacement
	$text = strtr($text, $replace);
	
	$subject = 		'Thank you for your pledges';
	
	$headers = 		"To: {$firstname} {$lastname} <{$eadr}>\r\n";
	$headers .= 	"From: no-reply@greenearthappeal.org\r\n";
	$headers .= 	"Content-type: text/html; charset=iso-8859-1\r\n";
	
	$message =		"\r\n" . $text . "\r\n";
	
	@mail($eadr, $subject, $message, $headers);
}

function respondent_referral($firstname, $lastname, $recipient_name, $recipient_eadr, $text) {
	//Prepare array of items to be replaced
	$replace = array (	"[RECIPIENT NAME]"					=>	$recipient_name,
						"[PERSON FIRST NAME]"				=>	$firstname,
						"[PERSON LAST NAME]"				=>	$lastname
						);
						
	//Load the file and do the replacement
	$text = strtr($text, $replace);
	
	$subject = 		"From {$firstname} {$lastname}";
	
	$headers = 		"To: {$recipient_name} <{$recipient_eadr}>\r\n";
	$headers .= 	"From: no-reply@greenearthappeal.org\r\n";
	$headers .= 	"Content-type: text/html; charset=iso-8859-1\r\n";
	
	$message =		"\r\n" . $text . "\r\n";

	@mail($recipient_eadr, $subject, $message, $headers);
}

function internal_notice($title, $firstname, $lastname, $postcode, $eadr, $day_phone, $pledges, $text) {
	//Prepare array of items to be replaced
	$replace = array (	"[PERSON TITLE]"					=>	$title,
						"[PERSON FIRST NAME]"				=>	$firstname,
						"[PERSON LAST NAME]"				=>	$lastname,
						"[PERSON POSTCODE]"					=>	$postcode,
						"[PERSON EMAIL ADDRESS]"			=>	$eadr,
						"[PERSON DAYTIME PHONE]"			=>	$day_phone,
						"[PLEDGES]"							=>	$pledges,
						);
						
	//Load the file and do the replacement
	$text = strtr($text, $replace);
	
	$to =  	 		$eadr;
	$subject = 		'greenearthappeal.org New Pledge Notice';
	
	$headers = 		"To: Internal Notifications <internalnotifications@greenearthappeal.org>\r\n";
	$headers .= 	"From: no-reply@greenearthappeal.org\r\n";
	$headers .= 	"Content-type: text/html; charset=iso-8859-1\r\n";
	
	$message =		"\r\n" . $text . "\r\n";

	@mail("internalnotifications@greenearthappeal.org", $subject, $message, $headers);
}

function ecotricity_notice($title, $firstname, $lastname, $adr1, $adr2, $adr3, $town, $county, $postcode, $eadr, $day_phone,$mobile, $tariff, $prepay, $supply, $paymethod, $paywhen, $prevpay, $prevpayperiod, $ipaddress, $text) {
	//Prepare array of items to be replaced
	$dt = date("r");
	$replace = array (	"[Date]" 						=>	$dt,
						"[Title]"						=>	$title, 
						"[First Name]"					=>	$firstname,
						"[Last Name]"					=>	$lastname, 
						"[Address Line 1]"				=>	$adr1,
						"[Address Line 2]"				=>	$adr2,
						"[Address Line 3]"				=>	$adr3,
						"[Town]"						=>	$town, 
						"[County]"						=>	$county,
						"[Postcode]"					=>	$postcode,
						"[E-mail Address]"				=>	$eadr,
						"[Daytime Telephone Number]"	=>	$day_phone, 
						"[Mobile Telephone Number]"		=>	$mobile,
						"[Current Tariff]"				=>	$tariff, 
						"[Prepayment]"					=>	$prepay,
						"[Supplier Number]"				=>	$supply,
						"[Method] "						=>	$paymethod, 
						"[CDate]"						=>	$paywhen,
						"[Approx Payment]"				=>	$prevpay,
						"[Period]"						=>	$prevpayperiod,
						"[IP ADDRESS]"					=>	$ipaddress
						);

	//Load the file and do the replacement
	$text = strtr($text, $replace);
	//echo $text;
	$to =  	 		$eadr;
	$subject = 		'Ecotricity Supply Switch Application Form';
	
	$headers = 		"To: Ecotricity Notifications <ecotricitynotifications@greenearthappeal.org>\r\n";
	$headers .= 	"From: no-reply@greenearthappeal.org\r\n";
	$headers .= 	"Content-type: text/html; charset=iso-8859-1\r\n";
	
	$message =		"\r\n" . $text . "\r\n";

	@mail("ecotricitynotifications@greenearthappeal.org", $subject, $message, $headers);
}

function contact_form_fill($name, $email, $telephone, $message, $text) {
	//Prepare array of items to be replaced
	$dt = date("r");
	$replace = array (	"[DATE]" 					=> $dt,
						"[NAME]"					=>	$name,
						"[EMAIL]"					=>	$email,
						"[TELEPHONE]"				=>	$telephone,
						"[MESSAGE]"					=>	$message
						);
						
	//do the replacement
	$text = strtr($text, $replace);
	
	$subject = 		'Green Earth Appeal Contact Form';
	
	$headers = 		"To: Internal Notifications <internalnotifications@greenearthappeal.org>\r\n";
	$headers .= 	"From: no-reply@greenearthappeal.org\r\n";
	$headers .= 	"Content-type: text/html; charset=iso-8859-1\r\n";
	
	$message =		"\r\n" . $text . "\r\n";

	mail("internalnotifications@greenearthappeal.org ", $subject, $message, $headers);
}

function pledge2_later($firstname, $lastname, $email, $link, $text) {
	//Prepare array of items to be replaced
	$dt = date("r");
	$replace = array (	"[PERSON FIRST NAME]"					=>	$firstname,
						"[PLEDGE-LINK]"							=>	$link
						);
						
	//do the replacement
	$text = strtr($text, $replace);
	
	$subject = 		'Green Earth Appeal Pledge Link';
	
	$headers = 		"To: {$firstname} {$lastname} <{$email}>\r\n";
	$headers .= 	"From: no-reply@greenearthappeal.org\r\n";
	$headers .= 	"Content-type: text/html; charset=iso-8859-1\r\n";
	
	$message =		"\r\n" . $text . "\r\n";

	mail($email, $subject, $message, $headers);
}
