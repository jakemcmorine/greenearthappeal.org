<?php
$data_string = array ( "account_number" => "22944", "name" => "test","address1" => "test1","address2" => "test2","address3" => "test3","postcode" => "WD6 1JF","title" => "Ms","forename" => "Jane","surname" => "Smith","telephone" => "07123 345 678","email" => "jane@bbc.co.uk","logo" => "base64_string","artwork" => "base64_string","facebook" => "","twitter" => "","created_at" => "2014-07-08 15:33:50","updated_at" => "2014-07-08 15:42:14","trees" => 4 );

$data = '{"account_number":"10960","name":"Lavazza Coffee (UK) Ltd","address1":"2nd Floor, 36 windsor Street, ","address2":", Uxbridge","address3":"Middlesex","postcode":"UB8 1AB","title":"","forename":"Lucinda","surname":"Brett","telephone":"01895 209 757","email":"l.brett@lavazza-coffee.co.uk","logo":"","trees":"1"}';
$data_string = json_decode($data);

$token = $_GET['token'];


// set up the curl resource
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://192.168.0.2/tree_request/api/order_trees.php?access_token=$token");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($data_string)                                                                       
)); 
// execute the request

$output = curl_exec($ch);

// output the profile information - includes the header

echo($output) . PHP_EOL;

// close curl resource to free up system resources
curl_close($ch);
?>
