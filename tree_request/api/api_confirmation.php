<?php
	require_once("db.php");
	
	define('JPATH_BASE', dirname(__FILE__) );
	require_once(JPATH_BASE.'/tcpdf/config/lang/eng.php');
	require_once(JPATH_BASE.'/tcpdf/tcpdf.php');
	
	if (!class_exists('MYPDF')) {
		class MYPDF extends TCPDF {
			//Page header
			public function Header() {
				// get the current page break margin
				$bMargin = $this->getBreakMargin();
				// get current auto-page-break mode
				$auto_page_break = $this->AutoPageBreak;
				// disable auto-page-break
				$this->SetAutoPageBreak(false, 0);
				// set bacground image
				//$img_file = K_PATH_IMAGES.'bg.jpg';
				$img_file = 'http://www.greenearthappeal.org/tree_request/api/bg-en3.jpg';
				$this->Image($img_file, 0, 0, 757, 1118, '', '', '', false, 300, '', false, false, 0);
				
				// restore auto-page-break status
				$this->SetAutoPageBreak($auto_page_break, $bMargin);
				// set the starting point for the page content
				$this->setPageMark();
			}
		}
	}
	
	//Featch records from database
	$query_orders = "SELECT * FROM t_partner_cert_companies WHERE added_date >= SUBDATE( NOW(), INTERVAL 24 HOUR)";
	//$query_orders = "SELECT * FROM t_partner_cert_companies WHERE added_date > DATE_SUB(NOW(), INTERVAL 24 HOUR) AND added_date <= NOW()";
	
	$result = mysql_query($query_orders);
	
	while($rowVal = mysql_fetch_array($result, MYSQL_ASSOC))
	{
		$name = $rowVal['contactforename'];
		$address1 = $rowVal['compaddress1'];
		$address2 = $rowVal['compaddress2'];
		$address3 = $rowVal['compaddress3'];
		$postcode = $rowVal['comppostcode'];
		$title = $rowVal['contacttitle'];
		$surname = $rowVal['contactsurname'];
		$telephone = $rowVal['contacttelephone'];
		$email = $rowVal['contactemail'];
		$website = $rowVal['compweb'];
                if($website =='http://www.maisonliedberg.com/')
                    $website = 'Maison Liedberg';
		elseif($website =='http://www.footlightstheatre.co.uk/')
                    $website = 'Footlights';
		else
                    $website = $website;
		$account_id = 0;
	
		$pageLayout = array(757, 1118);
		$pdf = new MYPDF('p', 'px', $pageLayout, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator('Green Earth Appeal');
		$pdf->SetAuthor('Green Earth Appeal');
		$pdf->SetTitle('Certification');
		$pdf->SetSubject('');
		$pdf->SetKeywords('green earth appeal certification');

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(0, 0, 0, true);
		$pdf->SetHeaderMargin(0);
		$pdf->SetFooterMargin(0);

		// remove default footer
		$pdf->setPrintFooter(false);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		//set some language-dependent strings
		$pdf->setLanguageArray($l);

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 40);
		// add a page
		$pdf->AddPage();
		$pdf->SetY(190);
		$pdf->SetX(0);
		$restaurant = '';
		$client_trees = '';
		$cirtificate_top = strtoupper($title ." ". $name ." ". $surname);

		$pct_header = '<p style="font-family:Tahoma; font-weight: bold; font-size: 36px; color:green; margin:0; text-align: center;">'.$cirtificate_top.'</p>
						<p style="font-family:Tahoma; font-size: 26px; color:green; text-align: center; margin:0">has planted a tree to offset the purchase made from '.$website.',  through <br /> an
						environmental programme which is operated by<br>
						the Green Earth Appeal</p>';

		$pdf->writeHTML($pct_header, true, false, true, false, '');


		$pct_footer = '<span style="font-family:Tahoma; font-size: 23px; color:#067902;">The Green Earth Appeal wishes to thank '.$cirtificate_top.' for their commitment to reduce carbon dioxide levels in the atmosphere.</span>';
		//$pct_footer = '';

		$pdf->SetY(890);
		$pdf->SetX(320);

		$html3 = '<span style="font-size: 16px; color: #0a5f28">'.date("jS F Y").'</span>';
		$pdf->writeHTML($html3, true, false, true, false, '');

		$pdf->writeHTMLCell($w=370, $h=0, $x='55', $y='740', $pct_footer, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

		//Close and output PDF document
		$rendom_file = mt_rand();
		$cert_name = "certificate_".$rendom_file.".pdf";
		$pdf->Output(dirname(__FILE__).'/uploads/certs/'.$cert_name, 'F');
		//Send certificate mail to user
		$emailSubject = 'Your purchase from '.$website;
		$emailBody = '<p>Dear '.$name.'</p>';
		$emailBody .= '<p>Thank you for offsetting the carbon footprint of your recent purchase from '.$website.'.</p>';
		$emailBody .= '<p>'.$website.' is one of our partners and on your behalf we will plant a single fruit tree in the developing world.</p>';
		$emailBody .= '<p>Please find attach a confirmation certificate.</p>';
		$emailBody .= '<p>Kind Regards.</p>';
		$emailBody .= '<p>The Green Earth Appeal.</p>';
		$emailBody .= file_get_contents('libraries/signature.htm');
		error_reporting(E_STRICT);
		require_once('../PHPMailer_522/class.phpmailer.php');
		$mail             = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPDebug  = 0;
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = "ssl";
		$mail->Host       = "host.server.com";
		$mail->Port       = 465;
		$mail->Username   = "vinhnd@greenearthappeal.org";
		$mail->Password   = "hT7t3dKf-";
		$mail->SetFrom('thankyou@greenearthappeal.org', 'Green Earth Appeal');
		$mail->AddReplyTo("thankyou@greenearthappeal.org","Green Earth Appeal");
		$mail->Subject    = $emailSubject;

		$mail->MsgHTML($emailBody);
		$mail->AddAddress($email);
		$mail->AddAttachment(dirname(__FILE__).'/uploads/certs/'.$cert_name);
		if(!$mail->Send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
		} 
		echo "mail sent";
	}