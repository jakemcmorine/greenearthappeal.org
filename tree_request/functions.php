<?php

class cronClass{

function generateLinkCode()  {
	$alphas = array_merge(range('a', 'z'), range('0', '9'));
	$found = true;
	$max = count($alphas);
	while ($found) {
		 $code = "";
		 for ($i=0; $i<21; $i++) {
			 $code .= $alphas[rand(0, $max-1)];
		 }
		 //Check code not used
		 $sql = "SELECT COUNT(*) FROM t_sponsors WHERE scode='{$code}'";
		 $r = mysql_query($sql);
		 if ($r[0]==0) {
			 return $code;
		 } 
	}
	
} // func ends


function updateSponsors($scode, $sname, $slogo, $shome) {
        //Check Logo has no path
        /*
		$slogo = str_replace("../graphics/sponsors/", "", $slogo);
        //Check homepage has http:// or https://
        if (substr($shome,0,7) != "http://" && substr($shome,0,8) != "https://") {
            $shome = "http://" . $shome;
        }
		*/
        $sql = "UPDATE t_sponsors SET scode=" . $scode . ", sname=" . ucwords(strtolower($sname)) . ", slogotext=" . ucwords(strtolower($sname)) . ", shome=" . $shome;
                if ($slogo<>"") { $sql .= ", slogo=" . $slogo;  }
                $sql .= " WHERE scode=" . $scode;
        mysql_query($sql);
        if (mysql_affected_rows() < 1) {
             $sql = "INSERT INTO t_sponsors SET scode=" . $scode . ", sname=" . $sname . ", slogotext=" . $sname . ", shome=" . $shome;
                if ($slogo<>"") { $sql .= ", slogo=" . $slogo;  } 
             mysql_query($sql);
             if (mysql_affected_rows()<1) {
                 return 0;
             } else {
                 return 2;
             }   
        } else {
            return 1;
        }
    } // ends
	
	
	function checkChangesAllowed($username, $id) {
        $sql = "SELECT COUNT(*) FROM t_partner_cert_companies INNER JOIN t_partner_accounts ON t_partner_cert_companies.partner_id = t_partner_accounts.id
                WHERE t_partner_accounts.account_email=" . $username . " AND t_partner_cert_companies.id=" . $id;
        $r = mysql_fetch_array(mysql_query($sql));
        if ($r[0]>0) {
            return true;
        } else {
            return false;
        }
    }

	function createCounterImageForPartner($cnt,$prId){
		 
		  //Set the Content Type
		  @header('Content-type: image/png');
	
		  // Create Image From Existing File
		  $png_image = imagecreatefrompng('imgs/counter_img.png');
	
		  // Allocate A Bachground Color For The Text - TRANSPARENT
		  $bg_color = imagecolorallocatealpha($png_image ,22, 105, 54, 127);
	
		  // Allocate A Color For The Text - GREEN
		  $font_color = imagecolorallocate($png_image, 22, 105, 54);
	
		  // Set Path to Font File
		  $font_path = 'counter/fonts/HelveticaLTStd-Bold.ttf';
		  
		  // Set Number Text to Be Printed On Image
		  $text = $cnt;
		  $chklen =  strlen($text);
		  if($chklen == 1) {
			 $dist = '134';	  
		  } else if($chklen == 2){
			  $dist = 106;
		  } else if($chklen == 3){
			  $dist = 80;
		  } else if($chklen == 4){
			  $dist = 56;
		  } else if($chklen == 5){
			  $dist = 28;
		  }
	
		  imagesavealpha($png_image, true);
		  imagefill($png_image, 0, 0, $bg_color); 
	
		  // Print Text On Image
		  imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);
	
		  $fname = 'counter/partner/ticker_'.$prId.'.png';
		  
		  // Send Image to Browser
		  imagepng($png_image,$fname);
			  
		  // Clear Memory
		  imagedestroy($png_image);
		  
	} // func ends 
	
	
	function createCounterImage2($cid){
		  //Set the Content Type
		  @header('Content-type: image/png');
	
		  // Create Image From Existing File
		  $png_image = imagecreatefrompng('imgs/counter_img.png');
	
		  // Allocate A Bachground Color For The Text - TRANSPARENT
		  $bg_color = imagecolorallocatealpha($png_image ,22, 105, 54, 127);
	
		  // Allocate A Color For The Text - GREEN
		  $font_color = imagecolorallocate($png_image, 22, 105, 54);
	
		  // Set Path to Font File
		  $font_path = 'counter/fonts/HelveticaLTStd-Bold.ttf';
	
		  $totalNumTrees = $this->treetotal($cid);
		  
		  // Set Number Text to Be Printed On Image
		  $text = $totalNumTrees;
		  $chklen =  strlen($text);
		  if($chklen == 1) {
			 $dist = '134';	  
		  } else if($chklen == 2){
			  $dist = 106;
		  } else if($chklen == 3){
			  $dist = 80;
		  } else if($chklen == 4){
			  $dist = 56;
		  } else if($chklen == 5){
			  $dist = 28;
		  }
	
		  imagesavealpha($png_image, true);
		  imagefill($png_image, 0, 0, $bg_color); 
	
		  // Print Text On Image
		  imagettftext($png_image, 36, 0, $dist, 40, $font_color, $font_path, $text);
	
		  $fname = 'counter/ticker_'.$cid.'.png';
		  
		  // Send Image to Browser
		  imagepng($png_image,$fname);
			  
		  // Clear Memory
		  imagedestroy($png_image);
		  
	} // func ends 
	
	


function treetotal($custacid) {
        $sql = "SELECT Sum(numtrees) FROM t_partner_cert_requests WHERE custaccountid=" . $custacid;
        $q = mysql_query($sql);
        if (mysql_num_rows($q)<1) {
            return 0;
        } else {
            $r = mysql_fetch_array($q);
            if ($r[0]<>0) { return $r[0]; } else { return 0; }
        }
        
    }
	
	function doInformationMail2($isnew, $account_id, $num_trees, $pdf=false) {
        //Get Details
        if ($num_trees > 1) { $treetext="trees"; } else { $treetext="tree"; }
        $sql = "SELECT * FROM t_partner_cert_companies WHERE id=" . $account_id;
        $customer = mysql_fetch_array(mysql_query($sql));
		
        $sql = "SELECT * FROM t_partner_accounts WHERE id=" . $customer['partner_id'];
        $account = mysql_fetch_array(mysql_query($sql)); 
		
		$tickerurl = 'http://greenearthappeal.org/tree_request/counter/ticker_'.$account_id.'.png';
		
        //Email Marvin
        $msg = file_get_contents("notify_marvin.html");
        $msg = str_replace("[account]", $account['account_name'], $msg); 
        $msg = str_replace("[numtrees]", $num_trees, $msg); 
        $msg = str_replace("[custname]", $customer['compname'], $msg);
        $msg = str_replace("[treetext]", $treetext, $msg); 
		$msg = str_replace("[tickerurl]", $tickerurl, $msg); 
		
        //$this->sendHTMLmail2($account_id, "marvin@greenearthappeal.org", $msg, "Notification: Partner ordering trees on behalf of customer", false, $pdf);
        $this->sendHTMLmail2($account_id, "marvin@greenearthappeal.org", $msg, "Notification: Partner ordering trees on behalf of customer", false, $pdf);
        $msg = "";
        
        //Email Account Owner
        if (isset($account['account_email']) && $account['account_email']<>"") {
            $msg = file_get_contents("notify_customer.html");
            $msg = str_replace("[name]", $account['account_person'], $msg); 
            $msg = str_replace("[numtrees]", $num_trees, $msg); 
            $msg = str_replace("[custname]", $customer['compname'], $msg); 
            $msg = str_replace("[treetext]", $treetext, $msg);  
			$msg = str_replace("[tickerurl]", $tickerurl, $msg); 
            //$this->sendHTMLmail2($account_id, $account['account_email'], $msg, "Confirmation of tree order for " . $customer['compname'], false, $pdf);
            $this->sendHTMLmail2($account_id, 'marvin@greenearthappeal.org', $msg, "Confirmation of tree order for " . $customer['compname'], false, $pdf);
			//$this->sendHTMLmail2($account_id, 'marvin@greenearthappeal.org', $msg, "Confirmation of tree order for " . $customer['compname'], false, $pdf);
            $msg = "";
        }
        
        //Email Account Named
        if (isset($customer['contactemail']) && $customer['contactemail']<>"") {  
            if ($isnew) {
                $msg = file_get_contents("mailtemplate_new.html");
            } else {
                $msg = file_get_contents("mailtemplate_reorder.html");
            }
            $name = $this->_helperNameType($customer['contacttitle'], $customer['contactforename'],$customer['contactsurname']); 
            $msg = str_replace("[name]", $name, $msg); 
            $msg = str_replace("[numtrees]", $num_trees, $msg); 
            $msg = str_replace("[accountname]", $account['account_name'], $msg);
            $msg = str_replace("[linkcode]", $customer['companycode'], $msg); 
            $msg = str_replace("[treetext]", $treetext, $msg);  
            if (!$isnew) {
                $msg = str_replace("[treetotal]", $this->treetotal($account_id), $msg);
            }
			$msg = str_replace("[tickerurl]", $tickerurl, $msg); 
            //$this->sendHTMLmail2($account_id,$customer['contactemail'], $msg, "Confirmation - we're planting " . $num_trees . " " . $treetext, false, $pdf);
		    $this->sendHTMLmail2($account_id, "marvin@greenearthappeal.org", $msg, "Confirmation - we're planting " . $num_trees . " " . $treetext, false, $pdf);
			//$this->sendHTMLmail2($account_id, "marvin@greenearthappeal.org", $msg, "Confirmation - we're planting " . $num_trees . " " . $treetext, false, $pdf);
            $msg = "";
        }
    }
    
    function sendHTMLmail2($pct_customer_id, $to, $msg, $subject, $attach_marketing=false, $pdf = false) {
        $from = "marvin@greenearthappeal.org";
        $headers = "From: {$from}\r\n";
        $headers .= "Reply-To: marvin@greenearthappeal.com\r\n";
        $headers .= "Return-path: <{from}>\r\n";
        if (!$attach_marketing) {
            if ($pdf) {
                // $pct_customer_id = $this->p['custid'];
                //if ($pct_customer_id) die();
                require('PHPMailer_522/sendmail.php');
            } else {
                /*$from = "marvin@greenearthappeal.org";
                $headers = "From: {$from}\r\n";
                $headers .= "Reply-To: marvin@greenearthappeal.com\r\n";
                $headers .= "Return-path: <{from}>\r\n";
                $headers .= "Content-Type: text/html\r\n";
                if ($this->debug) { echo "<hr />EMAIL: {$to} SUBJECT: {$subject}<br />{$msg}"; }
                @mail ($to, $subject, $msg, $headers, "-f " . $from);*/

                smtp_sendmail($to, $subject, $msg);
            }
            
        } else {
            smtp_sendmail($to, $subject, $msg, 'Marketing_Collateral.zip');

            /*$random_hash = md5(date('r', time())); 
            $headers .= "Content-Type: multipart/mixed; boundary=\"PHP-mixed-" . $random_hash . "\"";
            $attachment = chunk_split(base64_encode(file_get_contents('Marketing_Collateral.zip')));
            ob_clean();
            ob_start();
            ?> 
            --PHP-mixed-<?php echo $random_hash; ?>  
            Content-Type: multipart/alternative; boundary="PHP-alt-<?php echo $random_hash; ?>" 

            --PHP-alt-<?php echo $random_hash; ?>  
            Content-Type: text/plain; charset="iso-8859-1" 
            Content-Transfer-Encoding: 7bit

            --PHP-alt-<?php echo $random_hash; ?>  
            Content-Type: text/html; charset="iso-8859-1" 
            Content-Transfer-Encoding: 7bit

            <?php echo $msg; ?>

            --PHP-alt-<?php echo $random_hash; ?>-- 

            --PHP-mixed-<?php echo $random_hash; ?>  
            Content-Type: application/zip; name="Marketing Collateral.zip"  
            Content-Transfer-Encoding: base64  
            Content-Disposition: attachment  

            <?php echo $attachment; ?> 
            --PHP-mixed-<?php echo $random_hash; ?>-- 

            <?php 
            $message = ob_get_clean();
            mail($to, $subject, $message, $headers, "-f" . $from);
            //echo $message; */
        }
    }
	

	function _helperNameType($title, $forename, $surname) {
        if (isset($title) && isset($surname) && $title<>"" && $surname<>"") {
            return $title . " " . $surname;
        } else {
            if (isset($forename) && $forename<>"") {
                return $forename;  
            } else {
                return "";
				//return "Sir/Madam";
            }
        }
    }


    function smtp_sendmail($to, $subject, $msg, $attachment = false) {
        require_once('../PHPMailer_522/class.phpmailer.php');

        $mail             = new PHPMailer();

        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
        $mail->Port       = 465;                   // set the SMTP port for the GMAIL server
        $mail->Username   = "partners@greenearthappeal.org";  // GMAIL username
        $mail->Password   = "P8%k<g_39x,#";            // GMAIL password

        $mail->SetFrom('marvin@greenearthappeal.org', 'Marvin Baker');

        $mail->AddReplyTo("marvin@greenearthappeal.org", "Marvin Baker");

        $mail->Subject    = $subject;

        //$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

        $mail->MsgHTML($msg);
        //$address = "phpcodeteam@gmail.com";
        $mail->AddAddress($to);

        if ($attachment) {
            $mail->AddAttachment($attachment); // attachment    
        }
        

        if (!$mail->Send()) {
          echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "mail sent";
        }
    }
	
	
	
	
	
}
?>