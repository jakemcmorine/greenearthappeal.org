<?php

require_once('PHPMailer_522/class.phpmailer.php');

$mail             = new PHPMailer(); // defaults to using php "mail()"

$mail->IsSendmail(); // telling the class to use SendMail transport
/*
$body             = file_get_contents('contents.html');
$body             = preg_replace('/[\]/','',$body);
*/
$mail->SetFrom('marvin@greenearthappeal.com',"Marvin Baker");
$mail->AddReplyTo('marvin@greenearthappeal.com',"Marvin Baker");

$address = $to;
$mail->AddAddress($address);

$mail->Subject    = $subject;

$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

$mail->MsgHTML($msg);

//$mail->AddAttachment("images/phpmailer.gif");      // attachment
$mail->AddAttachment("certification/cert_{$pct_customer_id}.pdf"); // attachment

if(!$mail->Send()) {
  echo "Mailer Error: " . $mail->ErrorInfo;
} else {
  echo "Message sent!";
}

?>