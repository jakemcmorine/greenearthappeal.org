<?php
/**
 * Created by Atul.
 * API- Lightspeed API
 * Date: 08/01/2018
 */
require_once("db.php");

require_once("functions.php");

$ls = new lightspeedClass();

$sql = "SELECT * FROM ls_api_credentials WHERE active = '1'";

$result = mysql_query($sql);

 if(mysql_num_rows($result) > 0) {
	 
    while($row = mysql_fetch_assoc($result)) {
		 
		 $token_result = $ls->get_authtoken($row);
		 
		 $token_res = json_decode($token_result);
		 
		if(isset($token_res->token) && $token_res->token!=""){
			
			if(isset($_GET['date']) && !empty($_GET['date'])){
				 $today = $_GET['date'];
			}else{
				$current_date = date('Y-m-d');
			    $today = date('Y-m-d', strtotime($current_date .' -1 day'));    //previous day date
			}
			
			 $query = "Select * from `ls_api_data` where date = '" . $today . "' AND unique_id = '" .  $row['seqno'] . "'";
							 
			 $dbresult = mysql_query($query);
			 
			 $num_rows = mysql_num_rows($dbresult);
			 
			 if($num_rows > 0){
				 
						  echo "<br/><h4><=========== Data Already Exists For ".$today." (Company ID: ".$row['companyid']."): ============></h4><br/>";
				 
						 $success_msg = $today." - Data Already Exists";
						 
						 $api_logs_query = "INSERT INTO `ls_api_logs`(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $success_msg . "')";
						 
						 mysql_query($api_logs_query);
					 
				}else{
					$offset = 0;
					
					for($i=0; $i<=100; $i++){
						
						$offset = $i*100;
						
						$receipts_res = $ls->getReceiptsByDate($row, $token_res->token, $today, $offset);
						
						$receipts_res_encoded = mysql_real_escape_string($receipts_res);
						
						$Receipts_result = json_decode($receipts_res);
						
						//echo "<pre>"; print_r($Receipts_result); die;
						$receipts = $Receipts_result->results;
						$nocfd = $cfdtotaldonation = $noreceipts = 0;
						
						if($row['productid'] != ""){
							$product_ids = array_map('trim', explode(',', $row['productid']));
							foreach($receipts as $key=>$value){
								$items = $receipts[$key]->items;
								$noreceipts+= 1;
								if(!empty($items)){
									foreach($items as $k=>$v){
										if($product_ids){
											if (in_array($items[$k]->productId, $product_ids)){
												$nocfd+= $items[$k]->amount;
												$cfdtotaldonation+= $items[$k]->totalPrice; 
											}  
										}
									}
								} 
							}
						}
						
						 $random_no = $ls->randomKey(5);
						 
						if(isset($Receipts_result->results) && !empty($Receipts_result->results)){
							
								$cfdtotaldonation = number_format($cfdtotaldonation, 2, '.', ''); 
							
								//$sql_query1 = "INSERT INTO `ls_api_data`(`companyid`, `unique_id`, `date`, `payload`, `noreceipts`, `nocfd`, `cfd_total_donation_amount`) VALUES ('" . $row['companyid'] . "','" . $row['seqno'] . "','" . $today . "', '" . $receipts_res_encoded . "','" . $noreceipts . "', '" . $nocfd . "', '" . $cfdtotaldonation . "')";
								
								if($nocfd == 0){
									$processed = 1;
								}else{
									$processed = 0;
								}
								
								echo  $sql_query1 = "INSERT INTO `ls_api_data`(`companyid`, `unique_id`, `date`, `payload`, `noreceipts`, `nocfd`, `cfd_total_donation_amount`,`processed`) VALUES ('" . $row['companyid'] . "','" . $row['seqno'] . "','" . $today . "', '','" . $noreceipts . "', '" . $nocfd . "', '" . $cfdtotaldonation . "', '" . $processed . "')";
								
								 mysql_query($sql_query1);
								 
								 $success_msg = $today." - Data Imported";
								 
								 $api_logs_query = "INSERT INTO `ls_api_logs`(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $success_msg . "')";
								 
								 mysql_query($api_logs_query);
								 
								 echo "<br/><h4><=========== Receipts for ".$today." (Company ID: ".$row['companyid']."): ============></h4><br/>";
								 //echo $receipts_res;
								 
						}elseif(isset($Receipts_result->description) && $Receipts_result->description!=""){
							
							 $error_msg = $Receipts_result->description;
							
							 $sql_query = "INSERT INTO `ls_api_logs`(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $error_msg . "')";
							 mysql_query($sql_query);
							 
							 echo 'Error: '.$error_msg;
							 
						}elseif(empty($Receipts_result->results)){
							 if($i==0){     
								 echo "<br/><h4><=========== No Data found for ".$today." (Company ID: ".$row['companyid']."): ============></h4><br/>";
								 
								 $success_msg = $today." - No Data Found";
								 
								 $api_logs_query = "INSERT INTO `ls_api_logs`(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $success_msg . "')";
									 
									mysql_query($api_logs_query);
							 }
							 break;
							 
						}
					}
			}
			
		}elseif(isset($token_res->description) && $token_res->description!=""){
			
			 $error_msg = $token_res->description;
			 	
			$random_no = $ls->randomKey(5);		
			
			 $sql_query = "INSERT INTO `ls_api_logs`(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $error_msg . "')";
             mysql_query($sql_query);
			 
			 echo 'Error: '.$error_msg;
			
		}
			
    }
} 
exit;
