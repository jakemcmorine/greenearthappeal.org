<?php
class lightspeedClass{
	
	function get_authtoken($row) {                      // to get authtoken and create connection with lightspeed
			$server_address =   $row['serveraddress'];
			$deviceid       =   $row['deviceid'];
			$username       =   $row['username'];
			$password       =   $row['password'];
			$company_id     =   $row['companyid'];
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $server_address."/PosServer/rest/token");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"companyId\": \"$company_id\",\n \"deviceId\": \"$deviceid\",\n  \"password\": \"$password\",\n  \"username\": \"$username\"\n}");
			curl_setopt($ch, CURLOPT_POST, 1);

			$headers = array();
			$headers[] = "Content-Type: application/json";
			$headers[] = "Accept: */*";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			//echo $result . "<br>";
			/* if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			} */
			curl_close ($ch);
			return $result;
	}
	function getReceiptsByDate($row, $auth_token, $today, $offset) {                      // to get product receipts 
			$server_address =   $row['serveraddress'];
			$ch = curl_init();
			$headers = array();
			$headers[] = "Content-Type: application/json";
			$headers[] = "X-Auth-Token: ".$auth_token;
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $server_address."/PosServer/rest/financial/receipt?date=".$today."&amount=100&offset=".$offset."");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			curl_close ($ch);
			return $result;	
	}
	
	function randomKey($length) { 					 //create 5  digit random alphanumeric code
		$key='';
		$pool = array_merge(range(0,9),range('A', 'Z'));

		for($i=0; $i < $length; $i++) {
			$key .= $pool[mt_rand(0, count($pool) - 1)];
		}
		return $key;
	}
	

}