<?php
/**
 * Created by Atul Rana
 * API- Lightspeed API
 * Date: 05/11/2019
 */
$client_id = "greenEarthAppealProd-4829-42b9-8697-6664b7e26f1f";
$client_secret = "3b91477-fe20-4abe-9f45-65c2dba9d1f7";

if(isset($_REQUEST['code'])){
	
	$code = $_REQUEST['code'];
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://nightswatch.ikentoo.com/oauth/token');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "code=$code&grant_type=authorization_code&client-id=$client_id&redirect_uri=https://greenearthappeal.org/iKentoo_api/auth.php");
    curl_setopt($ch, CURLOPT_POST, 1);

	$headers = array();
	$headers[] = 'Authorization: Basic '.base64_encode($client_id.':'.$client_secret);
	$headers[] = 'Content-Type: application/x-www-form-urlencoded';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close ($ch);
	$result = json_decode($result,true);
	echo "<h3>Access token: </h3>".$result['access_token'];
	
}else{
	
	echo '<a href="https://nightswatch.ikentoo.com/oauth/authorize/?client_id='.$client_id.'&response_type=code&state=1234zyx&redirect_uri=https://greenearthappeal.org/iKentoo_api/auth.php">Authorize</>'; 
	
}
