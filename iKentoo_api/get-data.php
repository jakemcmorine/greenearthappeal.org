<?php
/**
 * Created by Atul Rana
 * API- Lightspeed API
 * Date: 18/11/2019
 */
 
require_once("db.php");

require_once("functions.php");

$ikentoo = new ikentooClass();

$sql = "SELECT * FROM ikt_api_credentials WHERE active = '1'";

$result = mysql_query($sql);

 if(mysql_num_rows($result) > 0) {
	 
    while($row = mysql_fetch_assoc($result)) {
		// echo "<pre>"; print_r($row); die;
		
			if(isset($_GET['date']) && !empty($_GET['date'])){
				 $fromdate = $_GET['date'];
			}else{
				$fromdate = date('Y-m-d', strtotime(date('Y-m-d') .' -1 day'));    //previous day date   
			}
		 
			 $query = "Select * from `ikt_api_data` where date = '" . $fromdate . "' AND companyid = '" .  $row['companyid'] . "'";
							 
			 $dbresult = mysql_query($query);
			 
			 $num_rows = mysql_num_rows($dbresult);
			 
			 $random_no = $ikentoo->randomKey(5);
			 
			 if($num_rows > 0){
				 
						  echo "<br/><h4><=========== Data Already Exists For ".$fromdate." (Company ID: ".$row['companyid']."): ============></h4><br/>";
				 
						 $success_msg = $fromdate." - Data Already Exists";
						 
						 $api_logs_query = "INSERT INTO `ikt_api_logs`(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $success_msg . "')";
						 
						 mysql_query($api_logs_query);
					 
				}else{
					
						$offset = 0;
						$i=0 ;
												
						$receipts_res = $ikentoo->getDailyFinancialsByDate( $fromdate, $row['companyid'], $row['access_token']);
												
						$receipts_res_encoded = mysql_real_escape_string($receipts_res);
						
						$receipts = json_decode($receipts_res, true);
																
						$nocfd = $cfdtotaldonation = $noreceipts = 0;
						
						if($row['productid']!=""){
							foreach($receipts['sales'] as $key=>$value){
								$lines = $receipts['sales'][$key]['salesLines'];
								$noreceipts+= 1;
								 if($lines){
									 foreach($lines as $line){
										if($line['sku'] == $row['productid']){              // Count if IsSold = true only
											   //echo "<pre>"; print_r($lines); 
												$nocfd+= $line['quantity'];
												$cfdtotaldonation+= $line['menuListPrice'];
												//$nocfd+= 1; 
										}
									} 
								 }
							}
						}
						$random_no = $ikentoo->randomKey(5);
							//echo $noreceipts.' '.$nocfd; die;
						if(!empty($receipts) && !isset($receipts['error'])){
							
								$cfdtotaldonation = number_format($cfdtotaldonation, 2, '.', ''); 
							
								 $sql_query1 = "INSERT INTO `ikt_api_data`(`companyid`, `date`, `payload`, `noreceipts`, `nocfd`, `cfd_total_donation_amount`) VALUES ('" . $row['companyid'] . "','" . $fromdate . "', '" . $receipts_res_encoded . "','" . $noreceipts . "', '" . $nocfd . "', '" . $cfdtotaldonation . "')";
								
								 mysql_query($sql_query1);
								 
								 $success_msg = $fromdate." - Data Imported";
								 
								 $api_logs_query = "INSERT INTO `ikt_api_logs`(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "','" . $success_msg . "')";
								 
								 mysql_query($api_logs_query);
								 
								 echo "<br/><h4><=========== Receipts for ".$fromdate." (Company ID: ".$row['companyid']."): ============></h4><br/>";
								 echo $receipts_res;
								 
						}elseif(isset($receipts['error']) && $receipts['error']!=""){
							
							 $error_msg = $receipts['error'];
							 $sql_query = "INSERT INTO `ikt_api_logs`(`pid`, `companyid`,`logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $error_msg . "')";
							 mysql_query($sql_query);
							 
							 echo 'Error: '.$error_msg;
							 
						}elseif(empty($receipts)){
							
							 if($i==0){     
								 echo "<br/><h4><=========== No Data found for ".$fromdate." (Company ID: ".$row['companyid']."): ============></h4><br/>";
								 
								 $success_msg = $fromdate." - No Data Found";
								 
								 $api_logs_query = "INSERT INTO `ikt_api_logs`(`pid`, `companyid`, `logentry`) VALUES ('" . $random_no . "','" . $row['companyid'] . "', '" . $success_msg . "')";
									 
									mysql_query($api_logs_query);
							 }
							 break;
							 
						}
			}
			
    }
} 
exit;
