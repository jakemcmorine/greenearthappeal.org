<?php
class ikentooClass{
	
	function __construct() 
	{
		//API keys
		$this->client_id 	 = "greenEarthAppealProd-4829-42b9-8697-6664b7e26f1f";
		$this->client_secret = "3b91477-fe20-4abe-9f45-65c2dba9d1f7";
	}
	
	function getDailyFinancialsByDate( $today, $businessLocationId , $access_token) // to get product receipts 
	{                      
		$client_id 		= $this->client_id;
		$client_secret 	= $this->client_secret;

		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, 'https://api.ikentoo.com/f/finance/'.$businessLocationId.'/dailyFinancials?includeConsumers=true&include=consumer&date='.$today);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$headers = array();
		$headers[] = 'Authorization: Bearer ' . $access_token;
		$headers[] = 'Accept: application/json';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		return $result;	
	}
	
	function randomKey( $length )  		// to create random alphanumeric code
	{ 					
		$key='';
		$pool = array_merge(range(0,9),range('A', 'Z'));

		for($i=0; $i < $length; $i++) {
			$key .= $pool[mt_rand(0, count($pool) - 1)];
		}
		return $key;
	}
	
}