<?php
/**
 * Created by Atul.
 * API- Outlook meeting data
 * Date: 12/02/2019
 */
require_once("db.php");

$post = json_decode(file_get_contents('php://input'), true);

$current_dt = date("Y-m-d H:i:s");

//echo "<pre>"; print_r($post); die;

if($post){
	
	$client_id = $post['client_id'];
	
	if($post['state'] == "0" || $post['state'] == "1"){
		
		 $meeting_info    = $post['meeting_info'];
		 $meeting_org     = $post['meeting_organiser'];
		 $meeting_invites = $post['invites'];
				
	     $info_query = "INSERT INTO `mfc_meeting_info` (`client_id`, `meeting_id`, `subject`, `location`, `type`, `state`, `start_date`, `end_date`) VALUES ('".$post['client_id']."','".$post['meeting_id']."','".$meeting_info['subject']."','".$meeting_info['location']."','".$meeting_info['type']."','".$post['state']."','".$meeting_info['start_date']."','".$meeting_info['end_date']."')";
						 
		 mysql_query($info_query);
		
		$info_id  = mysql_insert_id();
		
		$org_query = "INSERT INTO `mfc_meeting_organiser` (`client_id`, `meeting_info_id`, `first_name`, `last_name`, `email_address`, `title`, `company`) VALUES ('".$post['client_id']."','".$info_id."','".$meeting_org['first_name']."','".$meeting_org['last_name']."','".$meeting_org['email_address']."','".$meeting_org['title']."','".$meeting_org['company']."')";
		
		 mysql_query($org_query);
		
		$org_id  = mysql_insert_id();
		
		if($meeting_invites){
			
			foreach($meeting_invites as $invite){
				
			$inv_query = "INSERT INTO `mfc_meeting_invites` (`client_id`, `organiser_id`, `first_name`, `last_name`, `email_address`, `title`, `company`) VALUES ('".$post['client_id']."','".$org_id."','".$invite['first_name']."','".$invite['last_name']."','".$invite['email_address']."','".$invite['title']."','".$invite['company']."')";
							 
				mysql_query($inv_query);
			}
			
		}
			 
		echo "<h4>Record inserted!</h4>";
		
	}elseif($post['state'] == "2"){
		
		/* $insert_query = "INSERT INTO `mfc_meeting_info` (`client_id`, `meeting_id`, `subject`, `location`, `type`, `start_date`, `end_date`) VALUES ('".$post['client_id']."','".	$post['meeting_id']."','".$post['subject']."','".$post['location']."','".$post['type']."','".$post['start_date']."','".$post['end_date']."')";
						 
		mysql_query($insert_query);
			 
		echo "<h4>Record Cancelled!</h4>"; */
		
	}
	 
}
exit;
