<?php 		 

	$post_data = array(
			   'state' => "0",                                //0,1,2
			   'client_id'    => 'A78FSR454',
			   'meeting_id'   => '4354353535435',
					'meeting_info' => array(
					'subject'      => 'First meeting',
					'location'     => 'Manchester',
					'type'    	   => 'One off',                     //One off/Recurring
					'start_date'   => '2019-02-11 11:02:09',
					'end_date'     => '2019-03-12 12:02:25',
				),
			 'meeting_organiser' => array(
				'first_name' 		=> 'atulrana',
				'last_name'  		=> 'rana1',
				'email_address'     => 'apitester@gmail.com',
				'title'   		 	=> 'Outlook Data',
				'company'   		=> 'API outlook Company1',
			),
			 'invites' => array(
				array('first_name' 		=> 'invite1 first',
					'last_name'  		=> 'invite1 last',
					'email_address'     => 'apitester1@gmail.com',
					'title'   		 	=> 'Outlook Data1',
					'company'   		=> 'API outlook Company1',
				),array(
					'first_name' 		=> 'invite2 first',
					'last_name'  		=> 'invite2 last',
					'email_address'     => 'apitester2@gmail.com',
					'title'   		 	=> 'Outlook Data2',
					'company'   		=> 'API outlook Company2',
			)));
				
	/*  $post_data =  array(
			   'state'				=> "2",
			   'client_id'   	    => 'A78FSR454',
			   'meeting_id'   		=> '4354353535435',
			   'cancellation_date'  => '2019-03-12 12:02:25'
			   ); */
	//echo "<pre>"; print_r($post_data); 
	echo $post_json = json_encode($post_data); //die;

	$username = "atul";
	$password = "password";
	
	$header = array();
	$header[] = 'Content-type: application/json';
	$header[] = 'Authorization:'.$username;
	
	$endpoint = 'https://www.greenearthappeal.org/outlook-meeting-api/index.php';                //end point url
	$ch = @curl_init($endpoint);
	@curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
	@curl_setopt($ch, CURLOPT_POST, true);
	@curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
	@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = @curl_exec($ch);
	$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$curl_errors = curl_error($ch);
	@curl_close($ch);
	echo "<br>";
	if($curl_errors!=""){
		
		echo "Errors: " . $curl_errors;
		
	}else{
		
		echo "<h3>\nResponse: </h3>" . $response;
		
	}
